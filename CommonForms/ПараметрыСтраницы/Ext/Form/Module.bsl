﻿
#Область ПрограммныйИнтерфейс


&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	ЭтотОбъект.ОриентацияСтраницы_ = ?(ЗначениеЗаполнено(Параметры.ПараметрыСтраницы.ОриентацияСтраницы), Параметры.ПараметрыСтраницы.ОриентацияСтраницы, ОриентацияСтраницы.Портрет);
	ЭтотОбъект.АвтоМасштаб_ = ?(ЗначениеЗаполнено(Параметры.ПараметрыСтраницы.АвтоМасштаб), Параметры.ПараметрыСтраницы.АвтоМасштаб, "Нет");
	ЭтотОбъект.МасштабПечати_ = ?(ЗначениеЗаполнено(Параметры.ПараметрыСтраницы.МасштабПечати), Параметры.ПараметрыСтраницы.МасштабПечати, 100);
	ЭтотОбъект.ЭкземпляровНаСтранице_ = ?(ЗначениеЗаполнено(Параметры.ПараметрыСтраницы.ЭкземпляровНаСтранице), Параметры.ПараметрыСтраницы.ЭкземпляровНаСтранице, 1);
	ЭтотОбъект.ПолеСлева_ = ?(ЗначениеЗаполнено(Параметры.ПараметрыСтраницы.ПолеСлева), Параметры.ПараметрыСтраницы.ПолеСлева, 10);
	ЭтотОбъект.ПолеСправа_ = ?(ЗначениеЗаполнено(Параметры.ПараметрыСтраницы.ПолеСправа), Параметры.ПараметрыСтраницы.ПолеСправа, 10);
	ЭтотОбъект.ПолеСверху_ = ?(ЗначениеЗаполнено(Параметры.ПараметрыСтраницы.ПолеСверху), Параметры.ПараметрыСтраницы.ПолеСверху, 10);
	ЭтотОбъект.ПолеСнизу_ = ?(ЗначениеЗаполнено(Параметры.ПараметрыСтраницы.ПолеСнизу), Параметры.ПараметрыСтраницы.ПолеСнизу, 10);
	ЭтотОбъект.РазмерКолонтитулаСверху_ = ?(ЗначениеЗаполнено(Параметры.ПараметрыСтраницы.РазмерКолонтитулаСверху), Параметры.ПараметрыСтраницы.РазмерКолонтитулаСверху, 10);
	ЭтотОбъект.РазмерКолонтитулаСнизу_ = ?(ЗначениеЗаполнено(Параметры.ПараметрыСтраницы.РазмерКолонтитулаСнизу), Параметры.ПараметрыСтраницы.РазмерКолонтитулаСнизу, 10);
	ЭтотОбъект.РазмерСтраницы_ = ?(ЗначениеЗаполнено(Параметры.ПараметрыСтраницы.РазмерСтраницы), Параметры.ПараметрыСтраницы.РазмерСтраницы, Неопределено);
	ЭтотОбъект.КлючУникальностиНастроекПользователя = Параметры.КлючУникальностиНастроекПользователя;
	
	Если ЗначениеЗаполнено(Параметры.ПараметрыСтраницы.ЧередованиеРасположенияСтраниц) Тогда
		Если Параметры.ПараметрыСтраницы.ЧередованиеРасположенияСтраниц = ЧередованиеРасположенияСтраниц.ЗеркальноСверху Тогда
			ЧередованиеРасположенияСтраниц_ = "Зеркально сверху";
		ИначеЕсли Параметры.ПараметрыСтраницы.ЧередованиеРасположенияСтраниц = ЧередованиеРасположенияСтраниц.ЗеркальноСлева Тогда
			ЧередованиеРасположенияСтраниц_ = "Зеркально слева";
		ИначеЕсли Параметры.ПараметрыСтраницы.ЧередованиеРасположенияСтраниц = ЧередованиеРасположенияСтраниц.НеИспользовать Тогда
			ЧередованиеРасположенияСтраниц_ = "Не использовать";
		Иначе
			ЧередованиеРасположенияСтраниц_ ="Авто";
		КонецЕсли;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(Параметры.ПараметрыСтраницы.ДвусторонняяПечать) Тогда
		Если Параметры.ПараметрыСтраницы.ДвусторонняяПечать = ТипДвустороннейПечати.ПереворотВверх Тогда
			ДвусторонняяПечать_ = "Переворот вверх";
		ИначеЕсли Параметры.ПараметрыСтраницы.ДвусторонняяПечать = ТипДвустороннейПечати.ПереворотВлево Тогда
			ДвусторонняяПечать_ = "Переворот влево";
		Иначе
			ДвусторонняяПечать_ = "Нет";
		КонецЕсли;
	КонецЕсли;

	ЭтотОбъект.ЧерноБелаяПечать_ = ?(ЗначениеЗаполнено(Параметры.ПараметрыСтраницы.ЧерноБелаяПечать), Параметры.ПараметрыСтраницы.ЧерноБелаяПечать, Ложь);
		
	
	СписокПараметров_ = Новый Структура("АвтоМасштаб, ДвусторонняяПечать, МасштабПечати, ОриентацияСтраницы, 
	| ПолеСверху, ПолеСлева, ПолеСнизу, ПолеСправа, РазмерКолонтитулаСверху, РазмерКолонтитулаСнизу, 
	| РазмерСтраницы, ЧередованиеРасположенияСтраниц, ЧерноБелаяПечать, ЭкземпляровНаСтранице");
	
	Для Каждого Параметр_ Из СписокПараметров_ Цикл
		Ключ_ = Параметр_.Ключ;
		ЭтотОбъект.ПараметрыСтраницыИзМакета.Добавить(Параметры.ПараметрыСтраницыИзМакета[Ключ_],  Ключ_ + "_");
	КонецЦикла;
	

КонецПроцедуры

&НаКлиенте
Процедура ОК(Команда)
	СохранитьНастройки(ЭтотОбъект.КлючУникальностиНастроекПользователя);
	
	Параметры_ = Новый Структура;
	
	Параметры_.Вставить("ОриентацияСтраницы", ?(ЭтотОбъект.ОриентацияСтраницы_ = "Ландшафт", ОриентацияСтраницы.Ландшафт, ОриентацияСтраницы.Портрет));
	Параметры_.Вставить("АвтоМасштаб", ЭтотОбъект.АвтоМасштаб_);
	Параметры_.Вставить("МасштабПечати", ЭтотОбъект.МасштабПечати_);
	Параметры_.Вставить("ЭкземпляровНаСтранице", ЭтотОбъект.ЭкземпляровНаСтранице_);
	
	Параметры_.Вставить("ПолеСлева", ЭтотОбъект.ПолеСлева_);
	Параметры_.Вставить("ПолеСправа", ЭтотОбъект.ПолеСправа_);
	Параметры_.Вставить("ПолеСверху", ЭтотОбъект.ПолеСверху_);
	Параметры_.Вставить("ПолеСнизу", ЭтотОбъект.ПолеСнизу_);
	
	Параметры_.Вставить("РазмерКолонтитулаСверху", ЭтотОбъект.РазмерКолонтитулаСверху_);
	Параметры_.Вставить("РазмерКолонтитулаСнизу", ЭтотОбъект.РазмерКолонтитулаСнизу_);
	Параметры_.Вставить("РазмерСтраницы", ЭтотОбъект.РазмерСтраницы_);
	
	ЧередованиеСтраниц_ = ЧередованиеРасположенияСтраниц.Авто;
	
	Если ЭтотОбъект.ЧередованиеРасположенияСтраниц_ = "Зеркально сверху" Тогда
		ЧередованиеСтраниц_ = ЧередованиеРасположенияСтраниц.ЗеркальноСверху;
	ИначеЕсли ЭтотОбъект.ЧередованиеРасположенияСтраниц_ = "Зеркально слева" Тогда
		ЧередованиеСтраниц_ = ЧередованиеРасположенияСтраниц.ЗеркальноСлева;
	ИначеЕсли ЭтотОбъект.ЧередованиеРасположенияСтраниц_ = "Не использовать" Тогда
		ЧередованиеСтраниц_ = ЧередованиеРасположенияСтраниц.НеИспользовать;
	КонецЕсли;
	Параметры_.Вставить("ЧередованиеРасположенияСтраниц", ЧередованиеСтраниц_);
	
	Двусторонняя_ = ТипДвустороннейПечати.Нет;
	Если ЭтотОбъект.ДвусторонняяПечать_ = "Переворот вверх" Тогда
		Двусторонняя_ = ТипДвустороннейПечати.ПереворотВверх;
	ИначеЕсли ЭтотОбъект.ДвусторонняяПечать_ = "Переворот влево" Тогда
		Двусторонняя_ = ТипДвустороннейПечати.ПереворотВлево;
	КонецЕсли;
	Параметры_.Вставить("ДвусторонняяПечать", Двусторонняя_);
	
	Параметры_.Вставить("ЧерноБелаяПечать", ЭтотОбъект.ЧерноБелаяПечать_);
	ЭтотОбъект.Закрыть(Параметры_);
КонецПроцедуры

&НаСервере
Процедура СохранитьНастройки(КлючУникальности)
	СписокРеквизитов_ = ЭтотОбъект.ПолучитьРеквизиты();
	
	Для Каждого Реквизит_ Из СписокРеквизитов_ Цикл
		ИмяРеквизита_ = Реквизит_.Имя;
		ОбщегоНазначения.ХранилищеНастроекДанныхФормСохранить(КлючУникальности, ИмяРеквизита_, ЭтотОбъект[ИмяРеквизита_]);
		
	КонецЦикла;
	
КонецПроцедуры


&НаСервере
Процедура ЗагрузитьРеквизитыИзНастроек(КлючУникальности)
	СписокРеквизитов_ = ЭтотОбъект.ПолучитьРеквизиты();
	
	Для Каждого Реквизит_ Из СписокРеквизитов_ Цикл
		ИмяРеквизита_ = Реквизит_.Имя ;
		ЗначениеНастройки_ = ОбщегоНазначения.ХранилищеНастроекДанныхФормЗагрузить(КлючУникальности, ИмяРеквизита_);
		Если ЗначениеНастройки_ <> Неопределено Тогда
			ЭтотОбъект[ИмяРеквизита_] = ЗначениеНастройки_;
		КонецЕсли;
		
	КонецЦикла;
КонецПроцедуры



&НаКлиенте
Процедура Отмена(Команда)
	Закрыть();
КонецПроцедуры

&НаКлиенте
Процедура Справка(Команда)
	ЭтотОбъект.ОткрытьСправкуФормы();
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	ЗагрузитьРеквизитыИзНастроек(ЭтотОбъект.КлючУникальностиНастроекПользователя);
КонецПроцедуры

&НаКлиенте
Процедура СбросПараметров(Команда)
	УдалитьПараметры(ЭтотОбъект.КлючУникальностиНастроекПользователя);
	Для каждого Параметр_ Из ЭтотОбъект.ПараметрыСтраницыИзМакета Цикл
		Ключ_ = Параметр_.Представление;
		ЭтотОбъект[Ключ_] = Параметр_.Значение;
	КонецЦикла;

КонецПроцедуры


&НаСервере
Процедура УдалитьПараметры(КлючУникальности)
		СписокРеквизитов_ = ЭтотОбъект.ПолучитьРеквизиты();
	
	Для Каждого Реквизит_ Из СписокРеквизитов_ Цикл
		ИмяРеквизита_ = Реквизит_.Имя;
		ОбщегоНазначения.ХранилищеНастроекДанныхФормУдалить(КлючУникальности, ИмяРеквизита_, ИмяПользователя());
	КонецЦикла;
КонецПроцедуры

#КонецОбласти