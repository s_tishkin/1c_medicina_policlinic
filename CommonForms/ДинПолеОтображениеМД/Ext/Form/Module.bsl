﻿#Область ПрограммныйИнтерфейс

&НаСервере
Процедура УстановитьПараметрыИнициализации(ПараметрыИн)
	ДинамическоеПолеФормы.УстановитьРеквизит(ЭтотОбъект, ПолучитьДинПоле(), "ПараметрыИнициализации", ПараметрыИн);
КонецПроцедуры

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	ДинПоле_ = ПолучитьДинПоле();
	ПараметрыПоля_ = ДинамическоеПолеФормы.ПолучитьРеквизит(ЭтотОбъект, ДинПоле_, "ПараметрыИнициализации");
	Если ПараметрыПоля_ = Неопределено Тогда
		УстановитьПараметрыИнициализации(Параметры);
		ПараметрыПоля_ = ДинамическоеПолеФормы.ПолучитьРеквизит(ЭтотОбъект, ДинПоле_, "ПараметрыИнициализации");
	КонецЕсли;
	
	ДинПолеОтображениеМД.ПриСозданииНаСервере(ЭтотОбъект, ДинПоле_, ПараметрыПоля_);
КонецПроцедуры

&НаКлиенте
Процедура ТабличноеПолеОбработкаРасшифровки(Элемент, Расшифровка, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	ДинПолеОтображениеМД.ОбработкаРасшифровки(
		ЭтотОбъект, 
		ПолучитьДинПоле(), 
		Расшифровка
	);
КонецПроцедуры

&НаКлиенте
Процедура ПолеДокумента_ДокументСформирован(Элемент)
	ДинПолеОтображениеМД.ПолеДокумента_ДокументСформирован(
		ЭтотОбъект, 
		ПолучитьДинПоле(), 
		Элемент
	);
КонецПроцедуры

&НаКлиенте
Процедура ПолеДокумента_ПриНажатии(Элемент, ДанныеСобытия, СтандартнаяОбработка)
	ДинПолеОтображениеМД.ПолеДокумента_ПриНажатии(
		ЭтотОбъект, 
		ПолучитьДинПоле(), 
		Элемент, 
		ДанныеСобытия, 
		СтандартнаяОбработка
	);
КонецПроцедуры

&НаКлиенте
Процедура ОбработчикКоманды(Команда)
	Параметры_ = Неопределено;
	ВызовСервераНеНужен_ = ДинПолеОтображениеМД.ОбработчикКоманды(
		ЭтотОбъект, 
		ПолучитьДинПоле(), 
		Команда, 
		Параметры_
	);
	
	Если Истина <> ВызовСервераНеНужен_ Тогда
		ОбработчикКомандыНаСервере(Команда.Имя, Параметры_);
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ОбработчикКомандыНаСервере(ИмяКоманды, Параметры)
	ДинПолеОтображениеМД.ОбработчикКомандыНаСервере(
		ЭтотОбъект, 
		ПолучитьДинПоле(), 
		Команды.Найти(ИмяКоманды), 
		Параметры
	);
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция ПолучитьДинПоле()
	Возврат "ДинПолеОтображениеМД";
КонецФункции

#КонецОбласти


