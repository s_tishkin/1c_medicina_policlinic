﻿
#Область ПрограммныйИнтерфейс


////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ И ФУНКЦИИ ОБЩЕГО НАЗНАЧЕНИЯ

// Процедура помещает результаты подбора в хранилище.
//
&НаСервере
Процедура ПоместитьПлатежиВХранилище() 
	
	Платежи = ТаблицаОстатковРасчетов.Выгрузить(, "Выбран, Сумма, Соглашение, Заказ, ВалютаВзаиморасчетов, СтатьяДвиженияДенежныхСредств, Партнер, Контрагент");
	МассивУдаляемыхСтрок = Новый Массив;
	Для Каждого СтрокаТаблицы Из Платежи Цикл
		Если Не СтрокаТаблицы.Выбран Тогда
			МассивУдаляемыхСтрок.Добавить(СтрокаТаблицы);
		КонецЕсли;
	КонецЦикла;
	
	Для Каждого СтрокаТаблицы Из МассивУдаляемыхСтрок Цикл
		Платежи.Удалить(СтрокаТаблицы);
	КонецЦикла;
	
	Платежи.Колонки.Добавить("УникальныйИдентификаторУслуги", Новый ОписаниеТипов("УникальныйИдентификатор"));
	
	Для Каждого строкаТЗ_ Из Платежи Цикл
		Если ТипЗнч(строкаТЗ_.Заказ) = Тип("Строка") Тогда
			строкаТЗ_.УникальныйИдентификаторУслуги = Новый УникальныйИдентификатор(строкаТЗ_.Заказ);
			строкаТЗ_.Заказ = Неопределено;
		КонецЕсли;
	КонецЦикла;

	АдресПлатежейВХранилище = ПоместитьВоВременноеХранилище(Платежи, УникальныйИдентификатор);
	
КонецПроцедуры // ПоместитьПлатежиВХранилище()


// Функция получает текст запроса по остаткам расчетов с клиентами.
//
// Возвращаемое значение:
//	Запрос - Запрос по остаткам расчетов с клиентами.
//
&НаСервере
Функция ТекстЗапросаПоОстаткамРасчетовСКлиентами()
	
	ТекстЗапроса = 
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	РасчетыСКлиентами.ЗаказКлиента КАК ЗаказКлиента,
	|	РасчетыСКлиентами.Валюта КАК Валюта
	|ПОМЕСТИТЬ ВТ
	|ИЗ
	|	РегистрНакопления.РасчетыСКлиентами КАК РасчетыСКлиентами
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.АналитикаУчетаПоПартнерам КАК АналитикаПоПартнерам
	|		ПО РасчетыСКлиентами.АналитикаУчетаПоПартнерам = АналитикаПоПартнерам.КлючАналитики
	|ГДЕ
	|	АналитикаПоПартнерам.Организация = &Организация
	|	И (АналитикаПоПартнерам.Партнер = &Партнер
	|			ИЛИ &Партнер = НЕОПРЕДЕЛЕНО)
	|	И (АналитикаПоПартнерам.Контрагент = &Контрагент
	|			ИЛИ &Контрагент = НЕОПРЕДЕЛЕНО)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ВЫБОР
	|		КОГДА ТаблицаПлатежей.Сумма ЕСТЬ NULL 
	|				ИЛИ ТаблицаПлатежей.Сумма = 0
	|			ТОГДА ЛОЖЬ
	|		ИНАЧЕ ИСТИНА
	|	КОНЕЦ КАК Выбран,
	|	ЕСТЬNULL(ТаблицаПлатежей.Сумма, 0) КАК Сумма,
	|	&СтатьяДвиженияДенежныхСредств КАК СтатьяДвиженияДенежныхСредств,
	|	&ВалютаДокумента КАК ВалютаДокумента,
	|	ВЫБОР
	|		КОГДА РасчетыСКлиентами.ЗаказКлиента ССЫЛКА Справочник.СоглашенияСКлиентами
	|			ТОГДА ВЫРАЗИТЬ(РасчетыСКлиентами.ЗаказКлиента КАК Справочник.СоглашенияСКлиентами)
	|		ИНАЧЕ НЕОПРЕДЕЛЕНО
	|	КОНЕЦ КАК Соглашение,
	|	РасчетыСКлиентами.ЗаказКлиента КАК Заказ,
	|	РасчетыСКлиентами.Валюта КАК ВалютаВзаиморасчетов,
	|	АналитикаПоПартнерам.Партнер КАК Партнер,
	|	АналитикаПоПартнерам.Контрагент КАК Контрагент,
	|	ВЫБОР
	|		КОГДА РасчетыСКлиентами.СуммаОстаток > 0
	|			ТОГДА РасчетыСКлиентами.СуммаОстаток
	|		ИНАЧЕ 0
	|	КОНЕЦ КАК ДолгПартнера,
	|	ВЫБОР
	|		КОГДА РасчетыСКлиентами.КОплатеОстаток > 0
	|				И НЕ &ТолькоБезусловнаяЗадолженность
	|			ТОГДА РасчетыСКлиентами.КОплатеОстаток
	|		ИНАЧЕ 0
	|	КОНЕЦ КАК КОплате,
	|	ВЫБОР
	|		КОГДА РасчетыСКлиентами.СуммаОстаток < 0
	|			ТОГДА -РасчетыСКлиентами.СуммаОстаток
	|		ИНАЧЕ 0
	|	КОНЕЦ КАК НашДолг
	|ИЗ
	|	РегистрНакопления.РасчетыСКлиентами.Остатки(
	|			,
	|			(ЗаказКлиента, Валюта) В
	|				(ВЫБРАТЬ РАЗЛИЧНЫЕ
	|					ВТ.ЗаказКлиента,
	|					ВТ.Валюта
	|				ИЗ
	|					ВТ КАК ВТ)) КАК РасчетыСКлиентами
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.АналитикаУчетаПоПартнерам КАК АналитикаПоПартнерам
	|		ПО (АналитикаПоПартнерам.Организация = &Организация)
	|			И (АналитикаПоПартнерам.Партнер = &Партнер)
	|			И (АналитикаПоПартнерам.Контрагент = &Контрагент)
	|		ЛЕВОЕ СОЕДИНЕНИЕ ТаблицаПлатежей КАК ТаблицаПлатежей
	|		ПО РасчетыСКлиентами.ЗаказКлиента = ТаблицаПлатежей.Заказ
	|			И РасчетыСКлиентами.Валюта = ТаблицаПлатежей.Валюта
	|ГДЕ
	|	(НЕ &ДебиторскаяЗадолженность
	|				И РасчетыСКлиентами.СуммаОстаток < 0
	|			ИЛИ &ДебиторскаяЗадолженность
	|				И РасчетыСКлиентами.КОплатеОстаток > 0
	|				И НЕ &ТолькоБезусловнаяЗадолженность
	|			ИЛИ &ДебиторскаяЗадолженность
	|				И РасчетыСКлиентами.СуммаОстаток > 0)
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ВЫБОР
	|		КОГДА ТаблицаПлатежей.Сумма ЕСТЬ NULL 
	|				ИЛИ ТаблицаПлатежей.Сумма = 0
	|			ТОГДА ЛОЖЬ
	|		ИНАЧЕ ИСТИНА
	|	КОНЕЦ,
	|	ЕСТЬNULL(ТаблицаПлатежей.Сумма, 0),
	|	&СтатьяДвиженияДенежныхСредств,
	|	&ВалютаДокумента,
	|	ВЫБОР
	|		КОГДА РасчетыСКлиентами.ЗаказКлиента ССЫЛКА Справочник.СоглашенияСКлиентами
	|			ТОГДА ВЫРАЗИТЬ(РасчетыСКлиентами.ЗаказКлиента КАК Справочник.СоглашенияСКлиентами)
	|		ИНАЧЕ НЕОПРЕДЕЛЕНО
	|	КОНЕЦ,
	|	РасчетыСКлиентами.ЗаказКлиента,
	|	РасчетыСКлиентами.Валюта,
	|	АналитикаПоПартнерам.Партнер,
	|	АналитикаПоПартнерам.Контрагент,
	|	0,
	|	0,
	|	СУММА(РасчетыСКлиентами.Сумма)
	|ИЗ
	|	РегистрНакопления.РасчетыСКлиентами КАК РасчетыСКлиентами
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.АналитикаУчетаПоПартнерам КАК АналитикаПоПартнерам
	|		ПО РасчетыСКлиентами.АналитикаУчетаПоПартнерам = АналитикаПоПартнерам.КлючАналитики
	|		ЛЕВОЕ СОЕДИНЕНИЕ ТаблицаПлатежей КАК ТаблицаПлатежей
	|		ПО РасчетыСКлиентами.ЗаказКлиента = ТаблицаПлатежей.Заказ
	|			И РасчетыСКлиентами.Валюта = ТаблицаПлатежей.Валюта
	|ГДЕ
	|	РасчетыСКлиентами.ЗаказКлиента В(&УИДыУслугБезОтметки)
	|	И РасчетыСКлиентами.ВидДвижения = ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход)
	|
	|СГРУППИРОВАТЬ ПО
	|	ВЫБОР
	|		КОГДА ТаблицаПлатежей.Сумма ЕСТЬ NULL 
	|				ИЛИ ТаблицаПлатежей.Сумма = 0
	|			ТОГДА ЛОЖЬ
	|		ИНАЧЕ ИСТИНА
	|	КОНЕЦ,
	|	ЕСТЬNULL(ТаблицаПлатежей.Сумма, 0),
	|	ВЫБОР
	|		КОГДА РасчетыСКлиентами.ЗаказКлиента ССЫЛКА Справочник.СоглашенияСКлиентами
	|			ТОГДА ВЫРАЗИТЬ(РасчетыСКлиентами.ЗаказКлиента КАК Справочник.СоглашенияСКлиентами)
	|		ИНАЧЕ НЕОПРЕДЕЛЕНО
	|	КОНЕЦ,
	|	РасчетыСКлиентами.ЗаказКлиента,
	|	РасчетыСКлиентами.Валюта,
	|	АналитикаПоПартнерам.Партнер,
	|	АналитикаПоПартнерам.Контрагент
	|
	|ИМЕЮЩИЕ
	|	СУММА(РасчетыСКлиентами.Сумма) > 0";
	
	Возврат ТекстЗапроса;
	
КонецФункции // ТекстЗапросаПоОстаткамРасчетовСКлиентами()

// Процедура заполняет таблицу платежей.
//
&НаСервере
Процедура ЗаполнитьТаблицуПоРасчетамСПартнерами()
	
	УстановитьПривилегированныйРежим(Истина);
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Организация", Организация);
	Запрос.УстановитьПараметр("ВалютаДокумента", Валюта);
	Запрос.УстановитьПараметр("СтатьяДвиженияДенежныхСредств", СтатьяДвиженияДенежныхСредств);
	Запрос.УстановитьПараметр("Контрагент", Контрагент);
	
	Если ТипЗнч(Контрагент) = Тип("СправочникСсылка.Картотека")
		 И ЭтотОбъект.ХозяйственнаяОперация = ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.ВозвратОплатыКлиенту")
	Тогда
		// Для пациента получим список услуг не требующих отметки исполнения.
		// Для таких услуг можно делать возврат денежных средств не смотря на статус Выполнена.
		ЗапросУсл_ = Новый Запрос;
		ЗапросУсл_.Текст =
		"ВЫБРАТЬ
		|	ПРЕДСТАВЛЕНИЕ(СтатусыУслуг.УникальныйИдентификаторУслуги) КАК УникальныйИдентификаторУслуги
		|ИЗ
		|	РегистрСведений.СтатусыУслуг КАК СтатусыУслуг
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.Номенклатура КАК СправочникНоменклатура
		|		ПО СтатусыУслуг.Номенклатура = СправочникНоменклатура.Ссылка
		|ГДЕ
		|	СтатусыУслуг.Пациент = &Пациент
		|	И СправочникНоменклатура.НеТребуетОтметкиИсполнения";
		
		ЗапросУсл_.УстановитьПараметр("Пациент", Контрагент);
		УИДыСтрокойТЗ_ = ЗапросУсл_.Выполнить().Выгрузить();
		УИДыСтрокой_ = УИДыСтрокойТЗ_.ВыгрузитьКолонку("УникальныйИдентификаторУслуги");
		Запрос.УстановитьПараметр("УИДыУслугБезОтметки", УИДыСтрокой_);
	Иначе
		Запрос.УстановитьПараметр("УИДыУслугБезОтметки", Новый Массив);
	КонецЕсли;
	
	Если ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.СписаниеДебиторскойЗадолженности
		ИЛИ ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.СписаниеКредиторскойЗадолженности
		ИЛИ ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.АвансовыйОтчет
	Тогда
		Запрос.УстановитьПараметр("Партнер", Неопределено);
	Иначе
		Запрос.УстановитьПараметр("Партнер", Партнер);
	КонецЕсли;
	
	МассивРасчетыСПоставщиками = Новый Массив;
	МассивРасчетыСПоставщиками.Добавить(Перечисления.ХозяйственныеОперации.ОплатаПоставщику);
	МассивРасчетыСПоставщиками.Добавить(Перечисления.ХозяйственныеОперации.ВозвратДенежныхСредствОтПоставщика);
	МассивРасчетыСПоставщиками.Добавить(Перечисления.ХозяйственныеОперации.ВозвратТоваровПоставщику);
	МассивРасчетыСПоставщиками.Добавить(Перечисления.ХозяйственныеОперации.СписаниеДебиторскойЗадолженности);
	МассивРасчетыСПоставщиками.Добавить(Перечисления.ХозяйственныеОперации.СписаниеКредиторскойЗадолженности);
	МассивРасчетыСПоставщиками.Добавить(Перечисления.ХозяйственныеОперации.ВыдачаДенежныхСредствВДругуюОрганизацию);
	МассивРасчетыСПоставщиками.Добавить(Перечисления.ХозяйственныеОперации.ПеречислениеДенежныхСредствВДругуюОрганизацию);
	МассивРасчетыСПоставщиками.Добавить(Перечисления.ХозяйственныеОперации.АвансовыйОтчет);
	ЕстьРасчетыСПоставщиками = МассивРасчетыСПоставщиками.Найти(ХозяйственнаяОперация) <> Неопределено;
	
	МассивРасчетыСКлиентами = Новый Массив;
	МассивРасчетыСКлиентами.Добавить(Перечисления.ХозяйственныеОперации.ПоступлениеОплатыОтКлиента);
	МассивРасчетыСКлиентами.Добавить(Перечисления.ХозяйственныеОперации.ВозвратОплатыКлиенту);
	МассивРасчетыСКлиентами.Добавить(Перечисления.ХозяйственныеОперации.ВозвратТоваровОтКлиента);
	МассивРасчетыСКлиентами.Добавить(Перечисления.ХозяйственныеОперации.СписаниеДебиторскойЗадолженности);
	МассивРасчетыСКлиентами.Добавить(Перечисления.ХозяйственныеОперации.СписаниеКредиторскойЗадолженности);
	ЕстьРасчетыСКлиентами = МассивРасчетыСКлиентами.Найти(ХозяйственнаяОперация) <> Неопределено;
	
	МассивДебиторскаяЗадолженность = Новый Массив;
	МассивДебиторскаяЗадолженность.Добавить(Перечисления.ХозяйственныеОперации.СписаниеДебиторскойЗадолженности);
	МассивДебиторскаяЗадолженность.Добавить(Перечисления.ХозяйственныеОперации.ПоступлениеОплатыОтКлиента);
	МассивДебиторскаяЗадолженность.Добавить(Перечисления.ХозяйственныеОперации.ВозвратТоваровОтКлиента);
	МассивДебиторскаяЗадолженность.Добавить(Перечисления.ХозяйственныеОперации.ВозвратДенежныхСредствОтПоставщика);
	Запрос.УстановитьПараметр("ДебиторскаяЗадолженность", МассивДебиторскаяЗадолженность.Найти(ХозяйственнаяОперация) <> Неопределено);
	
	МассивТолькоБезусловнаяЗадолженность = Новый Массив;
	МассивТолькоБезусловнаяЗадолженность.Добавить(Перечисления.ХозяйственныеОперации.СписаниеДебиторскойЗадолженности);
	МассивТолькоБезусловнаяЗадолженность.Добавить(Перечисления.ХозяйственныеОперации.СписаниеКредиторскойЗадолженности);
	МассивТолькоБезусловнаяЗадолженность.Добавить(Перечисления.ХозяйственныеОперации.ВозвратОплатыКлиенту);
	МассивТолькоБезусловнаяЗадолженность.Добавить(Перечисления.ХозяйственныеОперации.ВозвратДенежныхСредствОтПоставщика);
	Запрос.УстановитьПараметр("ТолькоБезусловнаяЗадолженность", МассивТолькоБезусловнаяЗадолженность.Найти(ХозяйственнаяОперация) <> Неопределено);
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	РасшифровкаПлатежа.Соглашение КАК Соглашение,
	|	РасшифровкаПлатежа.Заказ КАК Заказ,
	|	РасшифровкаПлатежа.ВалютаВзаиморасчетов КАК Валюта,
	|	РасшифровкаПлатежа.Сумма КАК Сумма
	|
	|ПОМЕСТИТЬ ТаблицаПлатежей
	|ИЗ
	|	&РасшифровкаПлатежа КАК РасшифровкаПлатежа
	|;
	|///////////////////////////////////////////////////////////////////////////////
	|
	|//ТекстЗапросаРасчетыСКлиентами
	|
	|//ТекстОбъединитьВсе
	|
	|//ТекстЗапросаРасчетыСПоставщиками
	|
	|УПОРЯДОЧИТЬ ПО
	|	Контрагент
	|";
	
	ЗапросГотов_ = Ложь;
	Если ЕстьРасчетыСКлиентами Тогда
		ТекстЗапроса = СтрЗаменить(
			ТекстЗапроса,
			"//ТекстЗапросаРасчетыСКлиентами",
			ТекстЗапросаПоОстаткамРасчетовСКлиентами()
		);
		ЗапросГотов_ = Истина;
	КонецЕсли;

	Если ЕстьРасчетыСКлиентами И ЕстьРасчетыСПоставщиками Тогда
		ТекстЗапроса = СтрЗаменить(ТекстЗапроса, "//ТекстОбъединитьВсе", "ОБЪЕДИНИТЬ ВСЕ");
		ЗапросГотов_ = Истина;
	КонецЕсли;

	Запрос.Текст = ТекстЗапроса;
	
	Если ЗначениеЗаполнено(АдресПлатежейВХранилище) Тогда
		РасшифровкаПлатежа = ПолучитьИзВременногоХранилища(АдресПлатежейВХранилище);
	Иначе
		РасшифровкаПлатежа = ТаблицаОстатковРасчетов.Выгрузить(,).СкопироватьКолонки();
	КонецЕсли;
	РасшифровкаПлатежа.Свернуть("Соглашение, Заказ, ВалютаВзаиморасчетов", "Сумма");
	Запрос.УстановитьПараметр("РасшифровкаПлатежа", РасшифровкаПлатежа);
	
	Если ЗапросГотов_ Тогда
		ТаблицаОстатковРасчетов.Загрузить(Запрос.Выполнить().Выгрузить());
	КонецЕсли;
	
	Если РасшифровкаПлатежа.Количество() = 0 Тогда
		СуммаКРаспределению = СуммаДокумента;
	Иначе
		СуммаКРаспределению = 0;
	КонецЕсли;
	
	СоответствиеВалютаКурс = Новый Соответствие;
	
	Для Каждого СтрокаТаблицы Из ТаблицаОстатковРасчетов Цикл
		
		Если Не СтрокаТаблицы.Выбран Тогда
			
			Если СтрокаТаблицы.КОплате <> 0 Тогда
				СтрокаТаблицы.Сумма = СтрокаТаблицы.КОплате;
				
			ИначеЕсли СтрокаТаблицы.НашДолг <> 0 Тогда
				СтрокаТаблицы.Сумма = СтрокаТаблицы.НашДолг;
				
			ИначеЕсли СтрокаТаблицы.ДолгПартнера <> 0 Тогда
				СтрокаТаблицы.Сумма = СтрокаТаблицы.ДолгПартнера;
				
			КонецЕсли;
			
			Если Валюта <> СтрокаТаблицы.ВалютаВзаиморасчетов Тогда
				
				Коэффициенты = СоответствиеВалютаКурс.Получить(СтрокаТаблицы.ВалютаВзаиморасчетов);
				Если Коэффициенты = Неопределено Тогда
					Коэффициенты = МодульВалютногоУчета.ПолучитьКоэффициентыПересчетаВалюты(Валюта, СтрокаТаблицы.ВалютаВзаиморасчетов, ТекущаяДатаСеанса());
					СоответствиеВалютаКурс.Вставить(СтрокаТаблицы.ВалютаВзаиморасчетов, Коэффициенты);
				КонецЕсли;
					
				СтрокаТаблицы.Сумма = ?(Коэффициенты.КоэффициентПересчетаВВалютуВзаиморасчетов <> 0, СтрокаТаблицы.Сумма / Коэффициенты.КоэффициентПересчетаВВалютуВзаиморасчетов, 0);
				
			КонецЕсли;
			
		КонецЕсли;
		
		Если СуммаКРаспределению > 0 Тогда
			
			СтрокаТаблицы.Выбран = Истина;
			Если СтрокаТаблицы.Сумма > СуммаКРаспределению Тогда
				СтрокаТаблицы.Сумма = СуммаКРаспределению;
			КонецЕсли;
			СуммаКРаспределению = СуммаКРаспределению - СтрокаТаблицы.Сумма;
			
		КонецЕсли;
		
	КонецЦикла;
	
	ЗаполнитьЗаказСтрокой();
	
КонецПроцедуры // ЗаполнитьТаблицуПоРасчетамСПартнерами()

Процедура ЗаполнитьЗаказСтрокой()

	Запрос_ = Новый Запрос;
	Запрос_.Текст =
	"ВЫБРАТЬ
	|	СтатусыУслуг.МедицинскаяКарта,
	|	СтатусыУслуг.Номенклатура,
	|	ДанныеПациентовСрезПоследних.Фамилия,
	|	ДанныеПациентовСрезПоследних.Имя,
	|	ДанныеПациентовСрезПоследних.Отчество,
	|	ДанныеПациентовСрезПоследних.ДатаРождения,
	|	СтатусыУслуг.Номенклатура.Артикул КАК Артикул,
	|	СтатусыУслуг.Соглашение,
	|	СтатусыУслуг.Заказ,
	|	СтатусыУслуг.Заказ.Дата КАК ДатаЗаказа
	|ИЗ
	|	РегистрСведений.СтатусыУслуг КАК СтатусыУслуг
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.ДанныеПациентов.СрезПоследних КАК ДанныеПациентовСрезПоследних
	|		ПО СтатусыУслуг.Пациент = ДанныеПациентовСрезПоследних.Пациент
	|ГДЕ
	|	СтатусыУслуг.УникальныйИдентификаторУслуги = &УникальныйИдентификаторУслуги";
	
	Для Каждого строкаТЗ_ Из ЭтотОбъект.ТаблицаОстатковРасчетов Цикл
		
		Если ТипЗнч(строкаТЗ_.Заказ) = Тип("Строка") и ЗначениеЗаполнено(строкаТЗ_.Заказ) Тогда
			УникальныйИдентификаторУслуги_ = Новый УникальныйИдентификатор(строкаТЗ_.Заказ);
			
			Запрос_.УстановитьПараметр("УникальныйИдентификаторУслуги", УникальныйИдентификаторУслуги_);
			Выборка_ = Запрос_.Выполнить().Выбрать();
			Если Выборка_.Следующий() Тогда
				
				строкаТЗ_.Соглашение = Выборка_.Соглашение;
				строкаТЗ_.ДатаЗаказа = Выборка_.ДатаЗаказа;
				
				строкаТЗ_.ЗаказСтрокой = ПроведениеСервер.ПредставлениеРасчетногоДокумента(Выборка_);
				
			КонецЕсли;
			
		Иначе
			строкаТЗ_.ЗаказСтрокой = Строка(строкаТЗ_.Заказ);
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

// Функция возвращает таблицу реквизитов, зависимых от хозяйственной операции.
//
&НаСервереБезКонтекста
Функция ПолучитьМассивыРеквизитов(ХозяйственнаяОперация, МассивВсехРеквизитов, МассивРеквизитовОперации) Экспорт
	
	МассивВсехРеквизитов = Новый Массив;
	МассивВсехРеквизитов.Добавить("Партнер");
	МассивВсехРеквизитов.Добавить("Контрагент");
	МассивВсехРеквизитов.Добавить("ТаблицаОстатковРасчетов.Соглашение");
	МассивВсехРеквизитов.Добавить("ТаблицаОстатковРасчетов.Заказ");
	МассивВсехРеквизитов.Добавить("ТаблицаОстатковРасчетов.КОплате");
	МассивВсехРеквизитов.Добавить("ТаблицаОстатковРасчетов.НашДолг");
	МассивВсехРеквизитов.Добавить("ТаблицаОстатковРасчетов.ДолгПартнера");
	МассивВсехРеквизитов.Добавить("ТаблицаОстатковРасчетов.Партнер");
	
	Если ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ПоступлениеОплатыОтКлиента
		ИЛИ ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратТоваровОтКлиента
	Тогда
		МассивРеквизитовОперации = Новый Массив;
		МассивРеквизитовОперации.Добавить("Партнер");
		МассивРеквизитовОперации.Добавить("Контрагент");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Соглашение");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Заказ");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.КОплате");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.ДолгПартнера");
		
	ИначеЕсли ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратОплатыКлиенту Тогда
		МассивРеквизитовОперации = Новый Массив;
		МассивРеквизитовОперации.Добавить("Партнер");
		МассивРеквизитовОперации.Добавить("Контрагент");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Соглашение");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Заказ");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.НашДолг");
		
	ИначеЕсли ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ОплатаПоставщику
		ИЛИ ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратТоваровПоставщику
	Тогда
		МассивРеквизитовОперации = Новый Массив;
		МассивРеквизитовОперации.Добавить("Партнер");
		МассивРеквизитовОперации.Добавить("Контрагент");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Соглашение");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Заказ");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.КОплате");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.НашДолг");
		
	ИначеЕсли ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВыдачаДенежныхСредствВДругуюОрганизацию
		ИЛИ ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ПеречислениеДенежныхСредствВДругуюОрганизацию
	Тогда
		МассивРеквизитовОперации = Новый Массив;
		МассивРеквизитовОперации.Добавить("Контрагент");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Заказ");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.КОплате");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.НашДолг");	
	
	ИначеЕсли ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратДенежныхСредствОтПоставщика Тогда
		МассивРеквизитовОперации = Новый Массив;
		МассивРеквизитовОперации.Добавить("Партнер");
		МассивРеквизитовОперации.Добавить("Контрагент");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Соглашение");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Заказ");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.ДолгПартнера");
		
	ИначеЕсли ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.СписаниеДебиторскойЗадолженности Тогда
		МассивРеквизитовОперации = Новый Массив;
		МассивРеквизитовОперации.Добавить("Контрагент");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Партнер");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Соглашение");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Заказ");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.ДолгПартнера");
		
	ИначеЕсли ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.СписаниеКредиторскойЗадолженности Тогда
		МассивРеквизитовОперации = Новый Массив;
		МассивРеквизитовОперации.Добавить("Контрагент");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Партнер");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Соглашение");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Заказ");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.НашДолг");	
		
	ИначеЕсли ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.АвансовыйОтчет Тогда	
		МассивРеквизитовОперации = Новый Массив;
		МассивРеквизитовОперации.Добавить("Контрагент");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Партнер");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Соглашение");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.Заказ");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.КОплате");
		МассивРеквизитовОперации.Добавить("ТаблицаОстатковРасчетов.НашДолг");	
		
	КонецЕсли;
	
КонецФункции // ПолучитьМассивыРеквизитов()

// Процедура устанавливает видимость реквизитов в зависимости от хозяйственной операции.
//
&НаСервере
Процедура УстановитьВидимость()
	
	Перем МассивВсехРеквизитов;
	Перем МассивРеквизитовОперации;
	
	ПолучитьМассивыРеквизитов(
		ХозяйственнаяОперация, 
		МассивВсехРеквизитов, 
		МассивРеквизитовОперации
	);
	ДенежныеСредстваСервер.УстановитьВидимостьЭлементовПоМассиву(
		Элементы,
		МассивВсехРеквизитов,
		МассивРеквизитовОперации
	);
	
	Элементы.Контрагент.ТолькоПросмотр = (ХозяйственнаяОперация <> Перечисления.ХозяйственныеОперации.АвансовыйОтчет);
	
	ДенежныеСредстваСервер.УстановитьЗаголовокКолонкиЗаказ(ХозяйственнаяОперация, Элементы.ТаблицаОстатковРасчетовЗаказ);
	
	Если ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВыдачаДенежныхСредствВДругуюОрганизацию
		ИЛИ ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ПеречислениеДенежныхСредствВДругуюОрганизацию
	Тогда
		Заголовок = "Подбор по расчетам между организациями";
	КонецЕсли;
	
КонецПроцедуры // УстановитьВидимость()

// Процедура рассчитывает общую сумму выбранных платежей.
//
&НаКлиенте
Процедура РассчитатьСуммуПлатежей()
	
	СуммаПлатежей = 0;
	Для Каждого СтрокаТаблицы Из ТаблицаОстатковРасчетов Цикл
		
		Если СтрокаТаблицы.Выбран Тогда
			СуммаПлатежей = СуммаПлатежей + СтрокаТаблицы.Сумма;
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры // РассчитатьСуммуПлатежей()

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ

// Процедура - обработчик события "ПриСозданииНаСервере".
//
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	Организация = Параметры.Организация;
	ХозяйственнаяОперация = Параметры.ХозяйственнаяОперация;
	
	Если ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВыдачаДенежныхСредствВДругуюОрганизацию
		ИЛИ ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ПеречислениеДенежныхСредствВДругуюОрганизацию
	Тогда
		Партнер = Справочники.Партнеры.НашеПредприятие;
	Иначе
		Партнер = Параметры.Партнер;
	КонецЕсли;
	
	Контрагент = Параметры.Контрагент;
	Если Контрагент = Неопределено Тогда
		Контрагент = Справочники.Контрагенты.ПустаяСсылка();
	КонецЕсли;
	
	Валюта = Параметры.Валюта;
	СуммаДокумента = Параметры.СуммаДокумента;
	СтатьяДвиженияДенежныхСредств = Параметры.СтатьяДвиженияДенежныхСредств;
	АдресПлатежейВХранилище = Параметры.АдресПлатежейВХранилище;
	
	ЗаполнитьТаблицуПоРасчетамСПартнерами();
	ТаблицаОстатковРасчетов.Сортировать("ДатаЗаказа УБЫВ");
	УстановитьВидимость();
	
КонецПроцедуры // ПриСозданииНаСервере()

// Процедура - обработчик события "ПриОткрытии".
//
&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	РассчитатьСуммуПлатежей();
	
КонецПроцедуры // ПриОткрытии()

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ДЕЙСТВИЯ КОМАНДНЫХ ПАНЕЛЕЙ ФОРМЫ

// Процедура вызывается при нажатии на кнопку "ОК" 
&НаКлиенте
Процедура ПеренестиВДокументВыполнить()

	ПоместитьПлатежиВХранилище();
	Закрыть(КодВозвратаДиалога.OK);
	
	Структура = Новый Структура("АдресПлатежейВХранилище, ХозяйственнаяОперация", АдресПлатежейВХранилище, ХозяйственнаяОперация);
	ОповеститьОВыборе(Структура);
	
КонецПроцедуры // ПеренестиВДокументВыполнить()

// Процедура вызывается при нажатии на кнопку "Выбрать платежи".
//
&НаКлиенте
Процедура ВыбратьПлатежиВыполнить()

	Для Каждого СтрокаТаблицы Из ТаблицаОстатковРасчетов Цикл
		СтрокаТаблицы.Выбран = Истина;
	КонецЦикла;
	РассчитатьСуммуПлатежей();
	
КонецПроцедуры // ВыбратьПлатежиВыполнить()

// Процедура вызывается при нажатии на кнопку "Исключить платежки".
//
&НаКлиенте
Процедура ИсключитьПлатежиВыполнить()

	Для Каждого СтрокаТаблицы Из ТаблицаОстатковРасчетов Цикл
		СтрокаТаблицы.Выбран = Ложь
	КонецЦикла;
	РассчитатьСуммуПлатежей();
	
КонецПроцедуры // ИсключитьПлатежиВыполнить()

// Процедура вызывается при нажатии на кнопку "Выбрать выделенные платежи".
//
&НаКлиенте
Процедура ВыбратьВыделенныеПлатежи(Команда)
	
	МассивСтрок = Элементы.ТаблицаОстатковРасчетов.ВыделенныеСтроки;
	Для Каждого НомерСтроки Из МассивСтрок Цикл
		СтрокаТаблицы = ТаблицаОстатковРасчетов.НайтиПоИдентификатору(НомерСтроки);
		Если СтрокаТаблицы <> Неопределено Тогда
			СтрокаТаблицы.Выбран = Истина;
		КонецЕсли;
	КонецЦикла;
	РассчитатьСуммуПлатежей();
	
КонецПроцедуры // ВыбратьВыделенныеПлатежи()

// Процедура вызывается при нажатии на кнопку "Исключить выделенные платежи".
//
&НаКлиенте
Процедура ИсключитьВыделенныеПлатежи(Команда)
	
	МассивСтрок = Элементы.ТаблицаОстатковРасчетов.ВыделенныеСтроки;
	Для Каждого НомерСтроки Из МассивСтрок Цикл
		СтрокаТаблицы = ТаблицаОстатковРасчетов.НайтиПоИдентификатору(НомерСтроки);
		Если СтрокаТаблицы <> Неопределено Тогда
			СтрокаТаблицы.Выбран = Ложь;
		КонецЕсли;
	КонецЦикла;
	РассчитатьСуммуПлатежей();
	
КонецПроцедуры // ИсключитьВыделенныеПлатежи()

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ ПОЛЕЙ ФОРМЫ

// Процедура - обработчик события "ПриИзменении" поля "Партнер".
//
&НаКлиенте
Процедура ПартнерПриИзменении(Элемент)
	
	ЗаполнитьТаблицуПоРасчетамСПартнерами();
	
КонецПроцедуры // ПартнерПриИзменении()

// Процедура - обработчик события "ПриИзменении" поля "Контрагент".
//
&НаКлиенте
Процедура КонтрагентПриИзменении(Элемент)
	
	ЗаполнитьТаблицуПоРасчетамСПартнерами();
	
КонецПроцедуры // КонтрагентПриИзменении()

// Процедура - обработчик события "ПриОкончанииРедактирования" таблицы "ТаблицаОстатковРасчетов".
//
&НаКлиенте
Процедура ТаблицаОстатковРасчетовПриОкончанииРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования)
	
	РассчитатьСуммуПлатежей();
	
КонецПроцедуры // ТаблицаОстатковРасчетовПриОкончанииРедактирования()

// Процедура - обработчик события "Выбор" таблицы "ТаблицаОстатковРасчетов".
//
&НаКлиенте
Процедура ТаблицаОстатковРасчетовВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	Если Поле.Имя = "ТаблицаОстатковРасчетовЗаказ" Тогда
		СтрокаТаблицы = Элементы.ТаблицаОстатковРасчетов.ТекущиеДанные;
		Если СтрокаТаблицы <> Неопределено
			И ЗначениеЗаполнено(СтрокаТаблицы.Заказ)
			И ТипЗнч(СтрокаТаблицы.Заказ) <> Тип("Строка")
		Тогда
			ПоказатьЗначение(, СтрокаТаблицы.Заказ);
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры // ТаблицаОстатковРасчетовВыбор()



#КонецОбласти