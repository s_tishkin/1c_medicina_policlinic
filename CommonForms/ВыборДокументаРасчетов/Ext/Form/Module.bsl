﻿
#Область ПрограммныйИнтерфейс


///////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ ПОЛЕЙ ФОРМЫ

// Процедура - обработчик события "Выбор" табличного поля.
//
&НаКлиенте
Процедура СписокВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	ОповеститьОВыборе(Элементы.Список.ТекущиеДанные.Ссылка);
	
КонецПроцедуры // СписокВыбор()




#КонецОбласти