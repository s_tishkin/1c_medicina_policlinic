﻿&НаКлиенте
Перем СоздатьНовыйШаблон, СоздатьНовыйНумератор;

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	СоответствиеШаблонов_ = ЗаполнитьСоотвествиеМедицинскаяКартаШаблон();
	ЗаписатьСоотвествиеШаблонов(СоответствиеШаблонов_);
	
	СоответствиеНумераторов_ = ЗаполнитьСоотвествиеШаблонНумератор();
	ЗаписатьСоотвествиеНумераторов(СоответствиеНумераторов_);

	ПолучитьПараметрыШаблоновНаСервере();
	
	ИсходныйШаблонНомера = Запись.ШаблонНомера;
	ИсходныйНумератор = Нумератор;
	Если ИсходныйШаблонНомера <> Справочники.Удалить_ШаблоныНомеровМедицинскихКарт.ПустаяСсылка() Тогда
		ЗаполнитьШаблонНомера(Запись.ШаблонНомера);
	Иначе
		ЭтотОбъект.Элементы.РедактироватьШаблонНомера.Доступность = Ложь;
	КонецЕсли;
	Если Нумератор <> Справочники.Нумераторы.ПустаяСсылка() Тогда
		ЗаполнитьНумератор();
	КонецЕсли;
	ЗаполнитьПримерНомера();
	ЭтотОбъект.Элементы.РедактироватьНумератор.Доступность = Ложь;
КонецПроцедуры

#Область ВспомогательныеМетоды

Функция ПолучитьПараметрыШаблоновНаСервере()
	Перем Справочник, МакетXML, МакетТаблица, СтрокаМакета, Ошибка;
	Перем ПараметрыШаблона;
	
	Попытка
	
		Справочник = Справочники.Удалить_ШаблоныНомеровМедицинскихКарт;
	
		МакетXML = Справочник.ПолучитьМакет("ПараметрыШаблонов").ПолучитьТекст();
		МакетТаблица = ОбщегоНазначения.ПрочитатьXMLВТаблицу(МакетXML).Данные;
		
		ПараметрыШаблона = Новый Соответствие();
		СтруктураПараметров_ = Новый Структура();
		
		Для Каждого СтрокаМакета Из МакетТаблица Цикл
			СтруктураПараметров_.Вставить(СтрокаМакета.Имя, СтрокаМакета.ПримерДанных);
		КонецЦикла;
	
	Исключение
		
		Ошибка = ПодробноеПредставлениеОшибки(ИнформацияОбОшибке());
		__ПРОВЕРКА__(Ложь, "ПолучитьПараметрыШаблонов: Неожиданная ошибка: " + Ошибка);
		ВызватьИсключение;
		
	КонецПопытки;
	
	СтруктураПараметров = СтруктураПараметров_;
КонецФункции

#КонецОбласти

&НаКлиенте
Процедура ПриОткрытии(Отказ)
		ИсходныйШаблонНомера = Запись.ШаблонНомера;
		ИсходныйНумератор = Нумератор;
	КонецПроцедуры

&НаКлиенте
Процедура ПередЗаписью(Отказ, ПараметрыЗаписи)
	Если ЭтотОбъект.Элементы.ЗаписатьШаблонНомера.Видимость Тогда 
		Отказ = ЗаписатьШаблонНомераНаКлиенте();
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	НачатьТранзакцию(РежимУправленияБлокировкойДанных.Управляемый);
	
	НаборЗаписей_ = РегистрыСведений.Удалить_ШаблоныНомеровМедицинскихКарт.СоздатьНаборЗаписей();
	НаборЗаписей_.Отбор.ТипМедицинскойКарты.Установить(Запись.ТипМедицинскойКарты);
	НаборЗаписей_.Отбор.Период.Установить(Запись.Период);
	НаборЗаписей_.Прочитать();
	
	Если НаборЗаписей_.Количество() = 1 Тогда
		НаборЗаписей_.Очистить();
		НаборЗаписей_.Записать(Истина);
	КонецЕсли;
	
	ЗафиксироватьТранзакцию();

КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	СоответствиеШаблонов_ = ЗаполнитьСоотвествиеМедицинскаяКартаШаблон();
	ЗаписатьСоотвествиеШаблонов(СоответствиеШаблонов_);
	
	СоответствиеНумераторов_ = ЗаполнитьСоотвествиеШаблонНумератор();
	ЗаписатьСоотвествиеНумераторов(СоответствиеНумераторов_);
	ЗаполнитьШаблонНомера(Запись.ШаблонНомера);
	//ЗаполнитьНумератор();
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	ИсходныйШаблонНомера = Запись.ШаблонНомера;
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ШаблонНомераСоздание(Элемент, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
КонецПроцедуры

&НаКлиенте
Процедура ШаблонНомераПриИзменении(Элемент)
	ЗаполнитьШаблонНомера(Запись.ШаблонНомера);
	ИсходныйНумератор = Нумератор;
	ЗаполнитьНумератор();
	ЭтотОбъект.Элементы.РедактироватьНумератор.Доступность = Ложь;
	ЭтотОбъект.Элементы.СоздатьНумератор.Доступность = Ложь;
	ЗаполнитьПримерНомера();
	
КонецПроцедуры

&НаКлиенте
Процедура НумераторСоздание(Элемент, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
КонецПроцедуры

&НаКлиенте
Процедура НумераторПриИзменении(Элемент)
	ЗаполнитьНумератор();
	ЗаполнитьПримерНомера();
КонецПроцедуры

&НаКлиенте
Процедура ФорматнаяСтрокаНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	Конструктор = Новый КонструкторФорматнойСтроки(ЭтотОбъект.ФорматнаяСтрока);
	Конструктор.ДоступныеТипы = Новый ОписаниеТипов("Число");
	
	ОписаниеОповещения_ = Новый ОписаниеОповещения("ПослеВыбораФорматнойСтроки", ЭтотОбъект);
	Конструктор.Показать(ОписаниеОповещения_);
	
КонецПроцедуры

#Область ВспомогательныеМетоды

&НаКлиенте
Процедура ПослеВыбораФорматнойСтроки(Текст, ДополнительныеПараметры) Экспорт
	
	Если Текст <> Неопределено Тогда
		ЭтотОбъект.ФорматнаяСтрока = Текст;
		ЗаполнитьПримерНомера();
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

&НаКлиенте
Процедура ФорматнаяСтрокаПриИзменении(Элемент)
	ЗаполнитьПримерНомера();
КонецПроцедуры

&НаКлиенте
Процедура ФорматПрефиксаНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	ФорматНачалоВыбора("ФорматПрефикса");
	ЗаполнитьПримерНомера();
	СтандартнаяОбработка = Ложь;
КонецПроцедуры

&НаКлиенте
Процедура ФорматПрефиксаПриИзменении(Элемент)
	ЗаполнитьПримерНомера();
КонецПроцедуры

&НаКлиенте
Процедура ФорматСуффиксаНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	ФорматНачалоВыбора("ФорматСуффикса");
	ЗаполнитьПримерНомера();
	СтандартнаяОбработка = Ложь;
КонецПроцедуры

&НаКлиенте
Процедура ФорматСуффиксаПриИзменении(Элемент)
	ЗаполнитьПримерНомера();
КонецПроцедуры

&НаКлиенте
Процедура ФорматНачалоВыбора(ИмяРеквизита)
	
	ПараметрыОткрытия_ = Новый Структура;
	ПараметрыОткрытия_.Вставить("Элемент", "Префикс");
	ПараметрыОткрытия_.Вставить("Формат", ЭтотОбъект[ИмяРеквизита]);
	
	ДополнительныеПараметры = Новый Структура("ИмяРеквизита", ИмяРеквизита);
	ОписаниеОповещения_ = Новый ОписаниеОповещения("ПослеРедактированияФорматнойСтроки", ЭтотОбъект, ДополнительныеПараметры);
	
	ОткрытьФорму("Справочник.Удалить_ШаблоныНомеровМедицинскихКарт.Форма.ВводШаблона",
				ПараметрыОткрытия_,
				ЭтотОбъект,,,,
				ОписаниеОповещения_,
				РежимОткрытияОкнаФормы.БлокироватьОкноВладельца
			);
	
КонецПроцедуры

#Область ВспомогательныеМетоды

&НаКлиенте
Процедура ПослеРедактированияФорматнойСтроки(Результат, ДополнительныеПараметры) Экспорт
	
	Если Результат <> Неопределено Тогда
		ЭтотОбъект[ДополнительныеПараметры.ИмяРеквизита] = Результат.Формат;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

&НаКлиенте
Процедура НачальныйНомерПриИзменении(Элемент)
	ЗаполнитьПримерНомера();
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура СоздатьШаблонНомера(Команда)
	ЭтотОбъект.Элементы.ФорматПрефикса.Доступность = Истина;
	ЭтотОбъект.Элементы.ФорматСуффикса.Доступность = Истина;
	ЭтотОбъект.Элементы.Нумератор.Доступность = Истина;
	ЭтотОбъект.Элементы.НаименованиеШаблонаНомера.Видимость = Истина;
	ЭтотОбъект.Элементы.ШаблонНомера.Видимость = Ложь;
	ЭтотОбъект.Элементы.РедактироватьШаблонНомера.Видимость = Ложь;
	ЭтотОбъект.Элементы.СоздатьШаблонНомера.Видимость = Ложь;
	ЭтотОбъект.Элементы.ЗаписатьШаблонНомера.Видимость = Истина;
	ЭтотОбъект.Элементы.ОтменитьШаблонНомера.Видимость = Истина;
	НаименованиеШаблонаНомера = "";
	
	ЭтотОбъект.Элементы.СоздатьНумератор.Видимость = Истина;
	ЭтотОбъект.Элементы.СоздатьНумератор.Доступность = Истина;
	ЭтотОбъект.Элементы.РедактироватьНумератор.Доступность = Истина;
	
	СоздатьНовыйШаблон = Истина;
КонецПроцедуры

&НаКлиенте
Процедура РедактироватьШаблонНомера(Команда)
	ЭтотОбъект.Элементы.ФорматПрефикса.Доступность = Истина;
	ЭтотОбъект.Элементы.ФорматСуффикса.Доступность = Истина;
	ЭтотОбъект.Элементы.ШаблонНомера.Доступность = Ложь;
	ЭтотОбъект.Элементы.Нумератор.Доступность = Истина;
	ЭтотОбъект.Элементы.РедактироватьШаблонНомера.Видимость = Ложь;
	ЭтотОбъект.Элементы.СоздатьШаблонНомера.Видимость = Ложь;
	ЭтотОбъект.Элементы.ЗаписатьШаблонНомера.Видимость = Истина;
	ЭтотОбъект.Элементы.ОтменитьШаблонНомера.Видимость = Истина;
	
	ЭтотОбъект.Элементы.СоздатьНумератор.Доступность = Истина;
	ЭтотОбъект.Элементы.РедактироватьНумератор.Доступность = Истина;
	
	СоздатьНовыйШаблон = Ложь;
КонецПроцедуры

&НаКлиенте
Процедура ЗаписатьШаблонНомера(Команда)
	ЗаписатьШаблонНомераНаКлиенте();
КонецПроцедуры

&НаКлиенте
Процедура ОтменитьРедактированиеШаблонаНомера(Команда)
	ЭтотОбъект.Элементы.ШаблонНомера.Видимость = Истина;
	ЭтотОбъект.Элементы.НаименованиеШаблонаНомера.Видимость = Ложь;
	ЭтотОбъект.Элементы.ОтменитьШаблонНомера.Видимость = Ложь;
	ЭтотОбъект.Элементы.ЗаписатьШаблонНомера.Видимость = Ложь;
	ЭтотОбъект.Элементы.СоздатьШаблонНомера.Видимость = Истина;
	ЭтотОбъект.Элементы.РедактироватьШаблонНомера.Видимость = Истина;
	
	ЗаполнитьШаблонНомера(Запись.ШаблонНомера);
	ЭтотОбъект.Элементы.ШаблонНомера.Доступность = Истина;
	ЭтотОбъект.Элементы.ФорматПрефикса.Доступность = Ложь;
	ЭтотОбъект.Элементы.ФорматСуффикса.Доступность = Ложь;
	ЭтотОбъект.Элементы.Нумератор.Доступность = Ложь;
	ЭтотОбъект.Элементы.РедактироватьНумератор.Доступность = Ложь;
	ЭтотОбъект.Элементы.СоздатьНумератор.Доступность = Ложь;
	НаименованиеШаблонаНомера = Строка(Запись.ШаблонНомера);
	ЗаполнитьПримерНомера();
	
	Если ЭтотОбъект.Элементы.ОтменитьНумератор.Видимость Тогда
		ОтменитьРедактированиеНумератораНаКлиенте();
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура СоздатьНумератор(Команда)
	НаименованиеНумератора = "";
	ЭтотОбъект.Элементы.НаименованиеНумератора.Видимость = Истина;
	ЭтотОбъект.Элементы.Нумератор.Видимость = Ложь;
	
	ЭтотОбъект.Элементы.НачальныйНомер.Доступность = Истина;
	ЭтотОбъект.Элементы.Периодичность.Доступность = Истина;
	ЭтотОбъект.Элементы.ИспользоватьПропущенные.Доступность = Истина;
	ЭтотОбъект.Элементы.ФорматнаяСтрока.Доступность = Истина;
	НаименованиеНумератора = "";
	
	ЭтотОбъект.Элементы.СоздатьНумератор.Видимость = Ложь;
	ЭтотОбъект.Элементы.РедактироватьНумератор.Видимость = Ложь;
	ЭтотОбъект.Элементы.ЗаписатьНумератор.Видимость = Истина;
	ЭтотОбъект.Элементы.ОтменитьНумератор.Видимость = Истина;
	СоздатьНовыйНумератор = Истина;
КонецПроцедуры

&НаКлиенте
Процедура РедактироватьНумератор(Команда)
	ЭтотОбъект.Элементы.НаименованиеНумератора.Видимость = Ложь;
	ЭтотОбъект.Элементы.Нумератор.Видимость = Истина;
	ЭтотОбъект.Элементы.Нумератор.Доступность = Ложь;
	ЭтотОбъект.Элементы.НачальныйНомер.Доступность = Истина;
	ЭтотОбъект.Элементы.Периодичность.Доступность = Истина;
	ЭтотОбъект.Элементы.ИспользоватьПропущенные.Доступность = Истина;
	ЭтотОбъект.Элементы.ФорматнаяСтрока.Доступность = Истина;
	
	ЭтотОбъект.Элементы.СоздатьНумератор.Видимость = Ложь;
	ЭтотОбъект.Элементы.РедактироватьНумератор.Видимость = Ложь;
	ЭтотОбъект.Элементы.ЗаписатьНумератор.Видимость = Истина;
	ЭтотОбъект.Элементы.ОтменитьНумератор.Видимость = Истина;
	СоздатьНовыйНумератор = Ложь;
КонецПроцедуры

#Область ВспомогательныеМетоды

&НаСервере
Процедура РедактироватьНумераторНаСервере(Нумератор)
	Объект = Нумератор.ПолучитьОбъект();
	Объект.НачальныйНомер = НачальныйНомер;
	Объект.Периодичность = Периодичность;
	Объект.ИспользоватьПропущенные = ИспользоватьПропущенные;
	Объект.ФорматнаяСтрока = ФорматнаяСтрока;
	Объект.Записать();
КонецПроцедуры

#КонецОбласти

&НаКлиенте
Процедура ЗаписатьНумератор(Команда)
	ЗаписатьНумераторНаКлиенте();
КонецПроцедуры

#Область ВспомогательныеМетоды

&НаКлиенте
Функция ЗаписатьНумераторНаКлиенте()
	Если СоздатьНовыйНумератор Тогда
		Если НаименованиеНумератора = "" Тогда
			СообщенияПользователю.Показать("Общие_НеЗаданоНаименованиеНумератора");
			Возврат Истина;
		КонецЕсли;

		НовыйНумератор = СоздатьНумераторНаСервере();
		ЭтотОбъект.Элементы.СоздатьНумератор.Видимость = Ложь;
		ЭтотОбъект.Элементы.РедактироватьНумератор.Видимость = Истина;
		ЭтотОбъект.Элементы.РедактироватьНумератор.Доступность = Истина;
		ЭтотОбъект.Элементы.НаименованиеНумератора.Видимость = Ложь;
		ЭтотОбъект.Элементы.Нумератор.Видимость = Истина;
		Нумератор = НовыйНумератор;
		
	Иначе
		РедактироватьНумераторНаСервере(Нумератор);
		Если НаименованиеНумератора = "" Тогда
			НаименованиеНумератора = Строка(Нумератор);
		КонецЕсли;
	КонецЕсли;
	
	ЭтотОбъект.Элементы.ОтменитьНумератор.Видимость = Ложь;
	ЭтотОбъект.Элементы.НачальныйНомер.Доступность = Ложь;
	ЭтотОбъект.Элементы.Периодичность.Доступность = Ложь;
	
	ЭтотОбъект.Элементы.ИспользоватьПропущенные.Доступность = Ложь;
	ЭтотОбъект.Элементы.ФорматнаяСтрока.Доступность = Ложь;
	ЭтотОбъект.Элементы.ЗаписатьНумератор.Видимость = Ложь;
	СоздатьНовыйНумератор = Ложь;
	
	Если ЭтотОбъект.Элементы.ОтменитьШаблонНомера.Видимость = Ложь Тогда
		ЭтотОбъект.Элементы.Нумератор.Доступность = Ложь;
		ЭтотОбъект.Элементы.РедактироватьНумератор.Доступность = Ложь;
		ЭтотОбъект.Элементы.СоздатьНумератор.Доступность = Ложь;
	Иначе
		ЭтотОбъект.Элементы.Нумератор.Доступность = Истина;
		ЭтотОбъект.Элементы.РедактироватьНумератор.Видимость = Истина;

	КонецЕсли;
	Возврат Ложь;
КонецФункции

&НаСервере
Функция СоздатьНумераторНаСервере()
	НовыйЭлемент = Справочники.Нумераторы.СоздатьЭлемент();
	НовыйЭлемент.Наименование = НаименованиеНумератора;
	НовыйЭлемент.НачальныйНомер = НачальныйНомер;
	НовыйЭлемент.Периодичность = Периодичность;
	НовыйЭлемент.ИспользоватьПропущенные = ИспользоватьПропущенные;
	НовыйЭлемент.ФорматнаяСтрока = ФорматнаяСтрока;
	НовыйЭлемент.Записать();
	
	СоответствиеНумераторов_ = ЗаполнитьСоотвествиеШаблонНумератор();
	ЗаписатьСоотвествиеНумераторов(СоответствиеНумераторов_);
	
	Возврат НовыйЭлемент.Ссылка;
КонецФункции

#КонецОбласти

&НаКлиенте
Процедура ОтменитьРедактированиеНумератора(Команда)
	ОтменитьРедактированиеНумератораНаКлиенте();
	ЭтотОбъект.Элементы.Нумератор.Доступность = Истина;
	ЗаполнитьПримерНомера();
КонецПроцедуры

#Область ВспомогательныеМетоды

&НаКлиенте
Процедура ОтменитьРедактированиеНумератораНаКлиенте()
	ЭтотОбъект.Элементы.ОтменитьНумератор.Видимость = Ложь;
	ЭтотОбъект.Элементы.ЗаписатьНумератор.Видимость = Ложь;
	ЭтотОбъект.Элементы.СоздатьНумератор.Видимость = Истина;
	ЭтотОбъект.Элементы.РедактироватьНумератор.Видимость = Истина;
	
	ЭтотОбъект.Элементы.НаименованиеНумератора.Видимость = Ложь;
	ЭтотОбъект.Элементы.Нумератор.Видимость = Истина;
	ЗаполнитьНумератор();

	ЭтотОбъект.Элементы.НачальныйНомер.Доступность = Ложь;
	ЭтотОбъект.Элементы.Периодичность.Доступность = Ложь;
	ЭтотОбъект.Элементы.ИспользоватьПропущенные.Доступность = Ложь;
	ЭтотОбъект.Элементы.ФорматнаяСтрока.Доступность = Ложь;
	НаименованиеНумератора = Строка(Нумератор);
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура ЗаписатьСоотвествиеНумераторов(Соответствие)
	Для каждого Соот_ Из Соответствие Цикл
		Элемент_ = ШаблонНумератор.Добавить();
		ЗаполнитьЗначенияСвойств(Элемент_, Соот_);
	КонецЦикла;
КонецПроцедуры

&НаСервере
Функция ЗаполнитьСоотвествиеШаблонНумератор()
	Запрос_ = Новый Запрос();
	Запрос_.Текст = 
		"ВЫБРАТЬ
		|	СправочникНумераторы.Ссылка КАК Нумератор,
		|	КОЛИЧЕСТВО(ШаблоныНомеровМедицинскихКарт.Нумератор) КАК КоличествоШаблоновНомера
		|ИЗ
		|	Справочник.Нумераторы КАК СправочникНумераторы
		|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.Удалить_ШаблоныНомеровМедицинскихКарт КАК ШаблоныНомеровМедицинскихКарт
		|		ПО СправочникНумераторы.Ссылка = ШаблоныНомеровМедицинскихКарт.Нумератор
		|
		|СГРУППИРОВАТЬ ПО
		|	СправочникНумераторы.Ссылка";
	Результат_ = Запрос_.Выполнить().Выбрать();
	Соответствие_ = Новый Массив();
	Пока Результат_.Следующий() Цикл
		Стр = Новый Структура();
		Стр.Вставить("Нумератор", Результат_.Нумератор);
		Стр.Вставить("КоличествоШаблоновНомера", Результат_.КоличествоШаблоновНомера);
		Соответствие_.Добавить(Стр);
	КонецЦикла;
	Возврат Соответствие_;
КонецФункции

&НаСервере
Процедура ЗаполнитьШаблонНомера(ШаблонНомера)
	Если ШаблонНомера <> Справочники.Удалить_ШаблоныНомеровМедицинскихКарт.ПустаяСсылка() Тогда
		Запись.ШаблонНомера = ШаблонНомера;
		Объект = ШаблонНомера.ПолучитьОбъект();
		Нумератор = Объект.Нумератор;
		ФорматПрефикса = Объект.ФорматПрефикса;
		ФорматСуффикса = Объект.ФорматСуффикса;
		НаименованиеШаблонаНомера = Строка(Запись.ШаблонНомера);
		ЭтотОбъект.Элементы.СоздатьШаблонНомера.Доступность = Истина;
		ЭтотОбъект.Элементы.РедактироватьШаблонНомера.Доступность = Истина;
	
		Если ШаблонНомераИспользуетсяДляОдногоТипаМедКарт(Запись.ШаблонНомера, Запись.ТипМедицинскойКарты) Тогда
			ЭтотОбъект.Элементы.СоздатьШаблонНомера.Видимость = Истина;
			ЭтотОбъект.Элементы.РедактироватьШаблонНомера.Видимость = Истина;
			ЭтотОбъект.Элементы.НаименованиеШаблонаНомера.Видимость = Ложь;
		Иначе
			ЭтотОбъект.Элементы.СоздатьШаблонНомера.Видимость = Истина;
			ЭтотОбъект.Элементы.РедактироватьШаблонНомера.Видимость = Ложь;
			ЭтотОбъект.Элементы.НаименованиеШаблонаНомера.Видимость = Ложь;
		КонецЕсли
	
	Иначе
		ЭтотОбъект.Элементы.РедактироватьШаблонНомера.Доступность = Ложь;
		ФорматПрефикса = "";
		ФорматСуффикса = "";
	КонецЕсли
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьНумератор()
	Если Нумератор <> Справочники.Нумераторы.ПустаяСсылка() Тогда
		НачальныйНомер = Нумератор.НачальныйНомер;
		Периодичность =Нумератор.Периодичность;
		ИспользоватьПропущенные = Нумератор.ИспользоватьПропущенные;
		ФорматнаяСтрока = Нумератор.ФорматнаяСтрока;
		НаименованиеНумератора = Строка(Нумератор);
		//ЭтотОбъект.Элементы.СоздатьНумератор.Доступность = Истина;
		//ЭтотОбъект.Элементы.РедактироватьНумератор.Доступность = Истина;
		
		Если НумераторИспользуетсяДляОдногоШаблонаНомера(Нумератор, Запись.ШаблонНомера) Тогда
			ЭтотОбъект.Элементы.СоздатьНумератор.Видимость = Истина;
			ЭтотОбъект.Элементы.РедактироватьНумератор.Видимость = Истина;
			ЭтотОбъект.Элементы.РедактироватьНумератор.Доступность = Истина;
			ЭтотОбъект.Элементы.НаименованиеНумератора.Видимость = Ложь;
		Иначе
			ЭтотОбъект.Элементы.СоздатьНумератор.Видимость = Истина;
			ЭтотОбъект.Элементы.РедактироватьНумератор.Видимость = Ложь;
			ЭтотОбъект.Элементы.НаименованиеНумератора.Видимость = Ложь;
		КонецЕсли
	Иначе
		//ЭтотОбъект.Элементы.СоздатьНумератор.Доступность = Ложь;
		ЭтотОбъект.Элементы.РедактироватьНумератор.Доступность = Ложь;
		НачальныйНомер = "";
		Периодичность = "";
		ИспользоватьПропущенные = Ложь;
		ФорматнаяСтрока = "";
	КонецЕсли
КонецПроцедуры

Функция ШаблонНомераИспользуетсяДляОдногоТипаМедКарт(ШаблонНомера, ТипМедицинскойКарты)
	Пар_ = Новый Структура();
	
	Пар_.Вставить("ШаблонНомера", ШаблонНомера);
	Шаблон_ = ТипМедицинскойКартыШаблон.НайтиСтроки(Пар_);
	Если Шаблон_[0].КоличествоТиповМедицинскихКарт > 1 ИЛИ (Шаблон_[0].КоличествоТиповМедицинскихКарт = 1 И ШаблонНомера <> ИсходныйШаблонНомера) Тогда
		Возврат Ложь;
	КонецЕсли;
	Возврат Истина;
КонецФункции

&НаКлиенте
Функция ЗаписатьШаблонНомераНаКлиенте()
	Если ЭтотОбъект.Элементы.ЗаписатьНумератор.Видимость Тогда 
		Отказ = ЗаписатьНумераторНаКлиенте();
		Если Отказ Тогда 
			Возврат Отказ;
		КонецЕсли;
		ЭтотОбъект.Элементы.Нумератор.Доступность = Ложь;
	КонецЕсли;
	
	
	Если СоздатьНовыйШаблон Тогда
		Если НаименованиеШаблонаНомера = "" Тогда
			СообщенияПользователю.Показать("Общие_НеЗаданоНаименованиеШаблона");
			Возврат Истина;
		КонецЕсли;
		НовыйШаблон = СоздатьШаблонНомераНаСервере();
		ЭтотОбъект.Элементы.СоздатьШаблонНомера.Видимость = Ложь;
		ЭтотОбъект.Элементы.РедактироватьШаблонНомера.Доступность = Истина;
		ЭтотОбъект.Элементы.РедактироватьШаблонНомера.Видимость = Истина;
		ЭтотОбъект.Элементы.НаименованиеШаблонаНомера.Видимость = Ложь;
		ЭтотОбъект.Элементы.ШаблонНомера.Видимость = Истина;
		Запись.ШаблонНомера = НовыйШаблон;
	Иначе
		РедактироватьШаблонНомераНаСервере(Запись.ШаблонНомера);
		Если НаименованиеШаблонаНомера = "" Тогда
			НаименованиеШаблонаНомера = Строка(Запись.ШаблонНомера);
		КонецЕсли;
	КонецЕсли;
	ЭтотОбъект.Элементы.ОтменитьШаблонНомера.Видимость = Ложь;
	ЭтотОбъект.Элементы.ФорматПрефикса.Доступность = Ложь;
	ЭтотОбъект.Элементы.ФорматСуффикса.Доступность = Ложь;
	ЭтотОбъект.Элементы.ШаблонНомера.Доступность = Истина;
	ЭтотОбъект.Элементы.Нумератор.Доступность = Ложь;
	ЭтотОбъект.Элементы.ЗаписатьШаблонНомера.Видимость = Ложь;
	ЭтотОбъект.Элементы.РедактироватьШаблонНомера.Видимость = Истина;
	СоздатьНовыйШаблон = Ложь;
	
	
	
	Если НумераторИспользуетсяДляОдногоШаблонаНомера(Нумератор, Запись.ШаблонНомера) Тогда
		ЭтотОбъект.Элементы.СоздатьНумератор.Видимость = Ложь;
		ЭтотОбъект.Элементы.РедактироватьНумератор.Видимость = Истина;
	Иначе
		ЭтотОбъект.Элементы.СоздатьНумератор.Видимость = Истина;
		ЭтотОбъект.Элементы.РедактироватьНумератор.Видимость = Ложь;
	КонецЕсли;
	ЭтотОбъект.Элементы.СоздатьНумератор.Доступность = Ложь;
	ЭтотОбъект.Элементы.РедактироватьНумератор.Доступность = Ложь;
	Возврат Ложь;
КонецФункции

Функция НумераторИспользуетсяДляОдногоШаблонаНомера(Нумератор, ШаблонНомера)
	Пар_ = Новый Структура();
	
	Пар_.Вставить("Нумератор", Нумератор);
	Шаблон_ = ШаблонНумератор.НайтиСтроки(Пар_);
	Если Шаблон_[0].КоличествоШаблоновНомера > 1 ИЛИ (Шаблон_[0].КоличествоШаблоновНомера = 1 И Нумератор <> ИсходныйНумератор) Тогда
		Возврат Ложь;
	КонецЕсли;
	Возврат Истина;
КонецФункции

&НаСервере
Процедура РедактироватьШаблонНомераНаСервере(ШаблонНомера)
	Объект = ШаблонНомера.ПолучитьОбъект();
	Объект.Нумератор = Нумератор;
	Объект.ФорматПрефикса = ФорматПрефикса;
	Объект.ФорматСуффикса = ФорматСуффикса;
	Объект.Записать();
КонецПроцедуры

&НаСервере
Функция СоздатьШаблонНомераНаСервере()
	НовыйЭлемент = Справочники.Удалить_ШаблоныНомеровМедицинскихКарт.СоздатьЭлемент();
	НовыйЭлемент.Наименование = НаименованиеШаблонаНомера;
	НовыйЭлемент.Нумератор = Нумератор;
	НовыйЭлемент.ФорматПрефикса = ФорматПрефикса;
	НовыйЭлемент.ФорматСуффикса = ФорматСуффикса;
	НовыйЭлемент.Записать();
	
	СоответствиеШаблонов_ = ЗаполнитьСоотвествиеМедицинскаяКартаШаблон();
	ЗаписатьСоотвествиеШаблонов(СоответствиеШаблонов_);
	
	Возврат НовыйЭлемент.Ссылка;
КонецФункции

&НаСервере
Функция ЗаполнитьСоотвествиеМедицинскаяКартаШаблон()
	Запрос_ = Новый Запрос();
	Запрос_.Текст = 
		"ВЫБРАТЬ
		|	СправочникШаблоныНомеровМедицинскихКарт.Ссылка КАК ШаблонНомера,
		|	КОЛИЧЕСТВО(ШаблоныНомеровМедицинскихКарт.ТипМедицинскойКарты) КАК КоличествоТиповМедицинскихКарт
		|ИЗ
		|	Справочник.Удалить_ШаблоныНомеровМедицинскихКарт КАК СправочникШаблоныНомеровМедицинскихКарт
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.Удалить_ШаблоныНомеровМедицинскихКарт КАК ШаблоныНомеровМедицинскихКарт
		|		ПО СправочникШаблоныНомеровМедицинскихКарт.Ссылка = ШаблоныНомеровМедицинскихКарт.ШаблонНомера
		|
		|СГРУППИРОВАТЬ ПО
		|	СправочникШаблоныНомеровМедицинскихКарт.Ссылка,
		|	СправочникШаблоныНомеровМедицинскихКарт.Наименование";
	Результат_ = Запрос_.Выполнить().Выбрать();
	Соответствие_ = Новый Массив();
	Пока Результат_.Следующий() Цикл
		Стр = Новый Структура();
		Стр.Вставить("ШаблонНомера", Результат_.ШаблонНомера);
		Стр.Вставить("КоличествоТиповМедицинскихКарт", Результат_.КоличествоТиповМедицинскихКарт);
		Соответствие_.Добавить(Стр);
	КонецЦикла;
	Возврат Соответствие_;
КонецФункции

Процедура ЗаписатьСоотвествиеШаблонов(Соответствие)
	Для каждого Соот_ Из Соответствие Цикл
		Элемент_ = ТипМедицинскойКартыШаблон.Добавить();
		ЗаполнитьЗначенияСвойств(Элемент_, Соот_);
	КонецЦикла;
КонецПроцедуры

Процедура ЗаполнитьПримерНомера()
	Префикс_ = "";
	Суффикс_ = "";
	Если ФорматнаяСтрока <> "" Тогда
		ПримерНомера = Префикс_ + Формат(НачальныйНомер, ФорматнаяСтрока) + Суффикс_;
	Иначе
		ПримерНомера = Префикс_ + НачальныйНомер + Суффикс_;
	КонецЕсли;

КонецПроцедуры

#КонецОбласти