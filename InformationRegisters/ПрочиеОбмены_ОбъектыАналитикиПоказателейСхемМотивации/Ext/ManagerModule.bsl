﻿
#Область ПрограммныйИнтерфейс

// Функция возвращает список узлов, для которого применим данный регистр
// Возвращает:
//   Список значений, список узлов.
Функция ПолучитьСписокДоступныхУзловРегистра() Экспорт
	
	// доступные для регистра виды
	ФильтрТипов = ПолучитьФильтрОтбора();
	
	СтруктураФильтра = Новый Структура("ТипКонфигурацииПриемника", ФильтрТипов);
	
	Ответ = ПрочиеОбмены_ПланыОбмена.ДополнительныеРегистры_ПолучитьУзлыОбмена(СтруктураФильтра);
	
	Возврат Ответ;
КонецФункции // ПолучитьСписокДоступныхУзловРегистра()

// Функция проверяет - доступен ли данному узлу этот регистр
// Возвращает:
//   Булево.
Функция РегистрДоступенДляУзла(ТипКонфигурацииПриемника) Экспорт
	
	// доступные для регистра виды
	ФильтрТипов = ПолучитьФильтрОтбора();
	
	Ответ = НЕ (ФильтрТипов.Найти(ТипКонфигурацииПриемника) = Неопределено);
	
	Возврат Ответ;
	
КонецФункции // РегистрДоступенДляУзла()

// Функция формирует массив отбора
//
Функция ПолучитьФильтрОтбора() Экспорт
	
	ПеречислениеМенеджер = Перечисления.ПрочиеОбмены_КонфигурацииПолучатели;
	
	// доступные для регистра виды
	ФильтрТипов = Новый Массив;
	ФильтрТипов.Добавить(ПеречислениеМенеджер.ЗКБУ10);
	ФильтрТипов.Добавить(ПеречислениеМенеджер.ЗУП);
	ФильтрТипов.Добавить(ПеречислениеМенеджер.ЗКБУ10Мед);
	
	Возврат ФильтрТипов;
	
КонецФункции // ПолучитьФильтрОтбора()




#КонецОбласти