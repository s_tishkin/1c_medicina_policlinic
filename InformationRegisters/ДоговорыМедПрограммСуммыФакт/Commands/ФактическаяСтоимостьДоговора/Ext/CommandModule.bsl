﻿
#Область ПрограммныйИнтерфейс


&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	ПараметрыФормы = Новый Структура(
		"Отбор,РежимРаботыИзПанелиНавигации", Новый Структура("Договор", ПараметрКоманды), Истина
	);
	ОткрытьФорму("РегистрСведений.ДоговорыМедПрограммСуммыФакт.ФормаСписка",
		ПараметрыФормы, ПараметрыВыполненияКоманды.Источник,
		ПараметрыВыполненияКоманды.Уникальность, ПараметрыВыполненияКоманды.Окно,
		ПараметрыВыполненияКоманды.НавигационнаяСсылка
	);
КонецПроцедуры


#КонецОбласти