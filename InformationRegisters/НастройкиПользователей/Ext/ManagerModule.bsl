﻿
#Область ПрограммныйИнтерфейс

#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////
 // Процедура: ПервоначальноеЗаполнение
 //   Выполняет первоначальное заполнение регистра значениями по умолчанию.
 //
 // Параметры:
 //   ЗаменятьСуществующие
 //     При установке в значение Истина, существующие записи удаляются.
 //     Значение по умолчанию: Ложь.
 ///
Процедура ЗаполнениеНастроекПользователей(Пользователь = Неопределено, ЗаменятьСуществующие = Ложь) Экспорт
	Перем НаборЗаписей, Запрос, Выборка, Запись, Период;
	
	НаборЗаписей = РегистрыСведений.НастройкиПользователей.СоздатьНаборЗаписей();
	Если ЗаменятьСуществующие Тогда
		Если Пользователь <> Неопределено Тогда
			НаборЗаписей.Отбор.Пользователь.Установить(Пользователь);
		КонецЕсли;
		НаборЗаписей.Записать();
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	СписокНастроек.Настройка,
		|	СписокНастроек.Пользователь,
		|	СписокНастроек.Пользователь.ИдентификаторПользователяИБ КАК ИдентификаторПользователяИБ,
		|	СписокНастроек.ЗначениеПоУмолчанию
		|ИЗ
		|	(ВЫБРАТЬ
		|		СписокНастроек.Ссылка КАК Настройка,
		|		СписокНастроек.ЗначениеПоУмолчанию КАК ЗначениеПоУмолчанию,
		|		Пользователи.Ссылка КАК Пользователь
		|	ИЗ
		|		ПланВидовХарактеристик.НастройкиПользователей КАК СписокНастроек,
		|		Справочник.Пользователи КАК Пользователи
		|	ГДЕ
		|		СписокНастроек.УровеньДоступаКНастройкам В (ЗНАЧЕНИЕ(Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя))) КАК СписокНастроек
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.НастройкиПользователей.СрезПоследних КАК РегистрНастроек
		|		ПО (РегистрНастроек.Настройка = СписокНастроек.Настройка)
		|			И (РегистрНастроек.Пользователь = СписокНастроек.Пользователь)
		|ГДЕ
		|	РегистрНастроек.Настройка ЕСТЬ NULL "
	;
	
	Если Пользователь <> Неопределено Тогда
		Запрос.Текст = Запрос.Текст + 
					"И СписокНастроек.Пользователь = &Пользователь";
		Запрос.УстановитьПараметр("Пользователь", Пользователь);
	КонецЕсли;
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Период = НачалоДня(ТекущаяДатаСеанса());
	Пока Выборка.Следующий() Цикл
		НаборЗаписей.Отбор.Пользователь.Установить(Выборка.Пользователь);
		НаборЗаписей.Отбор.Настройка.Установить(Выборка.Настройка);
		Запись = НаборЗаписей.Добавить();
		Запись.Значение = Выборка.ЗначениеПоУмолчанию;
		Если НЕ (ОбщиеМеханизмы.ЭтоСеансФоновогоЗадания() ИЛИ
						ОбщиеМеханизмы.ЭтоСеансWSConnection())
		Тогда
			Попытка
				Запись.Назначил = Пользователи.ТекущийПользователь();
			Исключение
				Запись.Назначил = Выборка.Пользователь;
			КонецПопытки;
		КонецЕсли;
		Запись.Настройка = Выборка.Настройка;
		Запись.Период = Период;
		Запись.Пользователь = Выборка.Пользователь;
		НаборЗаписей.Записать();
		НаборЗаписей.Очистить();
	КонецЦикла;
КонецПроцедуры

#КонецЕсли

#КонецОбласти