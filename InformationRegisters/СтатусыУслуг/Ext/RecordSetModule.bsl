﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

Процедура ПередЗаписью(Отказ, Замещение)
	Если ОбменДанными.Загрузка = Истина Тогда
		Возврат;
	КонецЕсли;
	
	Если ЭтотОбъект.Количество() = 0 Тогда
		// Удаляют записи. Услуги УИД которых присутствует в регистре запретим удалять.
		УИД_ = ЭтотОбъект.Отбор.УникальныйИдентификаторУслуги.Значение;
		Запрос_ = Новый Запрос;
		Запрос_.Текст =
			"ВЫБРАТЬ ПЕРВЫЕ 1
			|	РасчетыСКлиентами.Сумма
			|ИЗ
			|	РегистрНакопления.РасчетыСКлиентами КАК РасчетыСКлиентами
			|ГДЕ
			|	РасчетыСКлиентами.ЗаказКлиента = &ЗаказКлиента";
		Запрос_.УстановитьПараметр("ЗаказКлиента", Строка(УИД_));
		
		ИсключаемыеДокументы_ = Неопределено;
		Если ЭтотОбъект.ДополнительныеСвойства.Свойство("СтатусыУслугИсключитьДокументыПриПроверке", ИсключаемыеДокументы_) Тогда
			Запрос_.Текст = Запрос_.Текст + "
				| И НЕ РасчетыСКлиентами.Регистратор В (&Документы)";
				
			Запрос_.УстановитьПараметр("Документы", ИсключаемыеДокументы_);
		КонецЕсли;
		
		Выборка_ = Запрос_.Выполнить().Выбрать();
		Если Выборка_.Следующий() Тогда
			Отказ = Истина;
			Инф_ = УправлениеЗаказамиУслугами.ПолучитьСтатусУслуги(УИД_, Истина);
			Сообщить("Нельзя удалить услугу по которой производились расчеты: " + Инф_.Номенклатура);
			Возврат;
		КонецЕсли;
	КонецЕсли;
	
	Для Каждого Строка_ Из ЭтотОбъект Цикл
		
		Если ЗначениеЗаполнено(Строка_.МедицинскаяКарта) Тогда
			
			ПациентКарты_ = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Строка_.МедицинскаяКарта, "Пациент");
			Если Не ЗначениеЗаполнено(Строка_.Пациент) Тогда
				Строка_.Пациент = ПациентКарты_;
			ИначеЕсли ПациентКарты_ <> Строка_.Пациент Тогда
				Отказ = Истина;
				СообщенияПользователю.Показать(
					"ОформлениеЗаказов_ПациентКартыИПациентВРеквизитеНеСовпадают",
					Новый Структура("ПациентКарты,Пациент", Строка_.МедицинскаяКарта.Пациент, Строка_.Пациент)
				);
				Возврат;
			КонецЕсли;
			
		КонецЕсли;
		
		Если Не ЗначениеЗаполнено(Строка_.ИсточникФинансирования) И ЗначениеЗаполнено(Строка_.Соглашение) Тогда
			ИсточникФинансирования_ = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Строка_.Соглашение, "ИсточникФинансирования");
			Строка_.ИсточникФинансирования = ИсточникФинансирования_;
		КонецЕсли;

		Если Не ЗначениеЗаполнено(Строка_.Договор) И ЗначениеЗаполнено(Строка_.Соглашение) Тогда
			Договор_ = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Строка_.Соглашение, "Договор");
			Строка_.Договор = Договор_;
		КонецЕсли;
		
		Если Строка_.Количество = 0 Тогда
			Строка_.Количество = 1;
		КонецЕсли;
		
		Если ЗначениеЗаполнено(Строка_.Заказ) Тогда
			Строка_.ДатаЗаказа = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Строка_.Заказ, "Дата");
		ИначеЕсли ЗначениеЗаполнено(Строка_.Назначение) Тогда
			Строка_.ДатаЗаказа = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Строка_.Назначение, "Дата");
		ИначеЕсли ЗначениеЗаполнено(Строка_.МедицинскийДокумент) Тогда
			Строка_.ДатаЗаказа = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Строка_.МедицинскийДокумент, "Дата");
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

Процедура ПриЗаписи(Отказ, Замещение)
	Если ОбменДанными.Загрузка = Истина Тогда
		Возврат;
	КонецЕсли;
	
	Если ЭтотОбъект.Количество() = 0 Тогда
		// Удалена запись регистра, удалим соответствующие ей записи в СтатусыБиоматериала и в CDAДокументыНазначенныхУслуг.
		ОтборУИ = Неопределено;
		Для каждого ЭлементОтбора Из ЭтотОбъект.Отбор Цикл
			Если ЭлементОтбора.Использование И ЭлементОтбора.Имя = "УникальныйИдентификаторУслуги" Тогда
				ОтборУИ = ЭлементОтбора.Значение;
				Прервать;
			КонецЕсли;
		КонецЦикла;
		
		Если ОтборУИ <> Неопределено Тогда
			// блокируем данные от их изменения
			РегистрыСведений.СтатусыБиоматериала.ЗаблокироватьДанные(ОтборУИ);
			// удаление данных
			Если ЭтотОбъект.Количество() = 0 Тогда
				РегистрыСведений.СтатусыБиоматериала.УдалитьСтатусБиоматериала(ОтборУИ);
			КонецЕсли;
			
			// Удалим записи из регистра CDAДокументыНазначенныхУслуг
			РегистрыСведений.CDAДокументыНазначенныхУслуг.УдалитьCDAДокумент(ОтборУИ);

			// Обновим состояние стандарта, если услуга в него входит
			ПроверитьИОбновитьСостояниеСтандартаМедПомощи(ОтборУИ);

		КонецЕсли;
	Иначе
		// Обновим состояние стандарта, если услуга в него входит
		ПроверитьИОбновитьСостояниеСтандартаМедПомощи(ЭтотОбъект[0].УникальныйИдентификаторУслуги);
	КонецЕсли;
	
КонецПроцедуры

Процедура ПроверитьИОбновитьСостояниеСтандартаМедПомощи(УникальныйИдентификаторУслуги)

	Запрос_ = Новый Запрос;
	Запрос_.Текст =
	"ВЫБРАТЬ
	|	РегистрСведенийСтандартыМедицинскойПомощиПациентов.Стандарт,
	|	РегистрСведенийСтандартыМедицинскойПомощиПациентов.Состояние,
	|	УидСтандартаМедПомощи.УИДСтандарта,
	|	РегистрСведенийСтатусыУслуг.Номенклатура,
	|	СУММА(1) КАК Количество
	|ПОМЕСТИТЬ втВыполненныеУслугиСтандарта
	|ИЗ
	|	РегистрСведений.УслугиСтандартовМедицинскойПомощи КАК УидСтандартаМедПомощи
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.УслугиСтандартовМедицинскойПомощи КАК УидУслугСтандартаМедПомощи
	|		ПО УидСтандартаМедПомощи.УИДСтандарта = УидУслугСтандартаМедПомощи.УИДСтандарта
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.СтатусыУслуг КАК РегистрСведенийСтатусыУслуг
	|		ПО (УидУслугСтандартаМедПомощи.УИДУслуги = РегистрСведенийСтатусыУслуг.УникальныйИдентификаторУслуги)
	|			И (РегистрСведенийСтатусыУслуг.СтатусУслуги В (&ВыполненныеСтатусы))
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.СтандартыМедицинскойПомощиПациентов КАК РегистрСведенийСтандартыМедицинскойПомощиПациентов
	|		ПО УидСтандартаМедПомощи.УИДСтандарта = РегистрСведенийСтандартыМедицинскойПомощиПациентов.УИДСтандарта
	|ГДЕ
	|	УидСтандартаМедПомощи.УИДУслуги = &УИДУслуги
	|
	|СГРУППИРОВАТЬ ПО
	|	РегистрСведенийСтандартыМедицинскойПомощиПациентов.Стандарт,
	|	РегистрСведенийСтандартыМедицинскойПомощиПациентов.Состояние,
	|	УидСтандартаМедПомощи.УИДСтандарта,
	|	РегистрСведенийСтатусыУслуг.Номенклатура
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	СправочникСтандартыМедицинскойПомощиПроцедуры.Номенклатура,
	|	СправочникСтандартыМедицинскойПомощиПроцедуры.Количество,
	|	ЕСТЬNULL(втВыполненныеУслугиСтандарта.Количество, 0) КАК ВыполненноеКоличество
	|ИЗ
	|	Справочник.СтандартыМедицинскойПомощи.Процедуры КАК СправочникСтандартыМедицинскойПомощиПроцедуры
	|		ЛЕВОЕ СОЕДИНЕНИЕ втВыполненныеУслугиСтандарта КАК втВыполненныеУслугиСтандарта
	|		ПО СправочникСтандартыМедицинскойПомощиПроцедуры.Номенклатура = втВыполненныеУслугиСтандарта.Номенклатура
	|ГДЕ
	|	СправочникСтандартыМедицинскойПомощиПроцедуры.Ссылка В
	|			(ВЫБРАТЬ
	|				втВыполненныеУслугиСтандарта.Стандарт
	|			ИЗ
	|				втВыполненныеУслугиСтандарта)
	|	И СправочникСтандартыМедицинскойПомощиПроцедуры.Количество <> ЕСТЬNULL(втВыполненныеУслугиСтандарта.Количество, 0)
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	втВыполненныеУслугиСтандарта.УИДСтандарта
	|ИЗ втВыполненныеУслугиСтандарта КАК втВыполненныеУслугиСтандарта";
	
	ВыполненныеСтатусы_ = Новый Массив;
	ВыполненныеСтатусы_.Добавить(ПредопределенноеЗначение("Перечисление.СтатусыУслуг.Выполнена"));
	ВыполненныеСтатусы_.Добавить(ПредопределенноеЗначение("Перечисление.СтатусыУслуг.ВключенаВРеестр"));
	
	Запрос_.УстановитьПараметр("УИДУслуги", УникальныйИдентификаторУслуги);
	Запрос_.УстановитьПараметр("ВыполненныеСтатусы", ВыполненныеСтатусы_);
	
	МассивРезультатов_ = Запрос_.ВыполнитьПакет();
	НеВыполненныеУслуги_ = МассивРезультатов_[1].Выбрать();
	ДанныеСтандарта_ = МассивРезультатов_[2].Выбрать();
	
	Если ДанныеСтандарта_.Следующий() Тогда
		// Услуга входит в стандарт
		РегистрыСведений.СтандартыМедицинскойПомощиПациентов.УстановкаТранзакционнойБлокировки(ДанныеСтандарта_.УИДСтандарта);

		Запись_ = ПолучитьЗапись(ДанныеСтандарта_.УИДСтандарта);
		
		Если НеВыполненныеУслуги_.Следующий() Тогда
			// Не все услуги стандарта выполнены
			Если Запись_.Состояние = ПредопределенноеЗначение("Перечисление.СтатусыУслуг.Выполнена") Тогда
				РегистрыСведений.СтандартыМедицинскойПомощиПациентов.УстановитьСостояниеСтандарта(
						ДанныеСтандарта_.УИДСтандарта, ПредопределенноеЗначение("Перечисление.СтатусыУслуг.Заказана"),
						Ложь, Запись_.Регистратор
				);
			КонецЕсли;
		Иначе
			// Все услуги стандарта выполнены
			Если Запись_.Состояние = ПредопределенноеЗначение("Перечисление.СтатусыУслуг.Заказана") Тогда
				РегистрыСведений.СтандартыМедицинскойПомощиПациентов.УстановитьСостояниеСтандарта(
						ДанныеСтандарта_.УИДСтандарта, ПредопределенноеЗначение("Перечисление.СтатусыУслуг.Выполнена"),
						Ложь, Запись_.Регистратор
				);
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

Функция ПолучитьЗапись(УИДСтандарта)
	Запрос_ = Новый Запрос;
	Запрос_.Текст =
	"ВЫБРАТЬ
	|	СтандартыМедицинскойПомощиПациентов.Регистратор,
	|	СтандартыМедицинскойПомощиПациентов.Состояние
	|ИЗ
	|	РегистрСведений.СтандартыМедицинскойПомощиПациентов КАК СтандартыМедицинскойПомощиПациентов
	|ГДЕ
	|	СтандартыМедицинскойПомощиПациентов.УИДСтандарта = &УИДСтандарта";
	
	Запрос_.УстановитьПараметр("УИДСтандарта", УИДСтандарта);
	
	Выборка_ = Запрос_.Выполнить().Выбрать();
	
	Выборка_.Следующий();
	
	Возврат Выборка_;
КонецФункции

/// Устанавливает режим изменения статуса услуги
// Параметры:
Процедура УстановитьРежимИзмененияСтатуса() Экспорт
	ЭтотОбъект.ДополнительныеСвойства.Вставить("РежимИзмененияСтатуса");
КонецПроцедуры

#КонецОбласти

#КонецЕсли