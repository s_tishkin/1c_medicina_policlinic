﻿
#Область ПрограммныйИнтерфейс


&НаКлиенте
Процедура СписокВыборЗначения(Элемент, Значение, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;

	Если Элементы.Список.ТекущиеДанные <> Неопределено Тогда
		Закрыть(Элементы.Список.ТекущиеДанные.ИдентификаторБиоматериала);
	КонецЕсли;
КонецПроцедуры


#КонецОбласти