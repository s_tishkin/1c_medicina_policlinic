﻿
#Область ПрограммныйИнтерфейс

#Если Не ТолстыйКлиентУправляемоеПриложение Или Сервер Тогда
	
////Процедура УстановитьСтатусБиоматериала
// Если в регистре нет записей для данного УИД и Ключа строки состава услуги
// то создаются записи, иначе меняется статус на указанный в параметре.
//
///
Процедура УстановитьСтатусБиоматериала(
							УникальныйИдентификаторУслуги,
							КлючСтрокиСоставаУслуги = 1,
							СтатусБиоматериала,
							ДатаУстановкиСтатуса = Неопределено,
							СведенияОБиоматериале = Неопределено,
							ИдентификаторБиоматериала = Неопределено
							) Экспорт
	
	Если ДатаУстановкиСтатуса = Неопределено Тогда
		ДатаСтатуса_ = ТекущаяДатаСеанса();
	Иначе
		ДатаСтатуса_ = ДатаУстановкиСтатуса;
	КонецЕсли;
	
	НаборЗаписей_ = РегистрыСведений.СтатусыБиоматериала.СоздатьНаборЗаписей();
	НаборЗаписей_.Отбор.УникальныйИдентификаторУслуги.Установить(УникальныйИдентификаторУслуги);
	НаборЗаписей_.Прочитать();
	Если НаборЗаписей_.Количество() = 0 Тогда
		НоваяЗапись_ = НаборЗаписей_.Добавить();
		НоваяЗапись_.УникальныйИдентификаторУслуги = УникальныйИдентификаторУслуги;
		НоваяЗапись_.КлючСтрокиСоставаУслуги = 0;
		НоваяЗапись_.СтатусБиоматериала = СтатусБиоматериала;
		
		НоваяЗапись_ = НаборЗаписей_.Добавить();
		НоваяЗапись_.УникальныйИдентификаторУслуги = УникальныйИдентификаторУслуги;
		НоваяЗапись_.КлючСтрокиСоставаУслуги = КлючСтрокиСоставаУслуги;
		НоваяЗапись_.СтатусБиоматериала = СтатусБиоматериала;
		НоваяЗапись_.ДатаУстановкиСтатуса = ДатаСтатуса_;
		НоваяЗапись_.СведенияОБиоматериале = СведенияОБиоматериале;
		НоваяЗапись_.ИдентификаторБиоматериала = ИдентификаторБиоматериала;
	Иначе
		// обновим/дополним данные по ключу
		НайденаЗаписьПоКлючу = Ложь;
		ИндексНулевойКлюч = 0;
		ПредыдущийСтатус = СтатусБиоматериала;
		СтатусыБиоматериалаРазличны = Ложь;
		Для каждого Запись_ Из НаборЗаписей_ Цикл
			Если Запись_.КлючСтрокиСоставаУслуги = 0 Тогда
				ИндексНулевойКлюч = НаборЗаписей_.Индекс(Запись_);
				Продолжить;
			КонецЕсли;
			Если Запись_.КлючСтрокиСоставаУслуги = КлючСтрокиСоставаУслуги Тогда
				НайденаЗаписьПоКлючу = Истина;;
				Запись_.СтатусБиоматериала = СтатусБиоматериала;
				Запись_.ДатаУстановкиСтатуса = ДатаСтатуса_;
				Запись_.СведенияОБиоматериале = СведенияОБиоматериале;
				Запись_.ИдентификаторБиоматериала = ИдентификаторБиоматериала;
			ИначеЕсли Запись_.КлючСтрокиСоставаУслуги <> 0 И 
				 ПредыдущийСтатус <> Запись_.СтатусБиоматериала
			Тогда
				СтатусыБиоматериалаРазличны = Истина;
				ПредыдущийСтатус = Запись_.СтатусБиоматериала;
			КонецЕсли;
		КонецЦикла;
		Если НайденаЗаписьПоКлючу = Ложь Тогда
			НоваяЗапись_ = НаборЗаписей_.Добавить();
			НоваяЗапись_.УникальныйИдентификаторУслуги = УникальныйИдентификаторУслуги;
			НоваяЗапись_.КлючСтрокиСоставаУслуги = КлючСтрокиСоставаУслуги;
			НоваяЗапись_.СтатусБиоматериала = СтатусБиоматериала;
			НоваяЗапись_.ДатаУстановкиСтатуса = ДатаСтатуса_;
			НоваяЗапись_.СведенияОБиоматериале = СведенияОБиоматериале;
			НоваяЗапись_.ИдентификаторБиоматериала = ИдентификаторБиоматериала;
		КонецЕсли;
		
		// обновим нулевой статус
		ЗаписьНулевойКлюч_ = НаборЗаписей_.Получить(ИндексНулевойКлюч);
		Если СтатусыБиоматериалаРазличны Тогда
			ЗаписьНулевойКлюч_.СтатусБиоматериала = Перечисления.СтатусыБиоматериала.ПустаяСсылка();
		Иначе
			ЗаписьНулевойКлюч_.СтатусБиоматериала = СтатусБиоматериала;
		КонецЕсли;
	КонецЕсли;
	НаборЗаписей_.Записать(Истина);
	
КонецПроцедуры

// Добавляет новую запись в регистр. Заполняет ее значениями из другой записи с таким же идентификатором биоматериала.
//
// Параметры:
//  <Параметр1>  - <Тип.Вид> - <описание параметра>
//                 <продолжение описания параметра>
//  <Параметр2>  - <Тип.Вид> - <описание параметра>
//                 <продолжение описания параметра>
//
Процедура КопироватьСтатусБиоматериалаПоИдентификаторуБиоматериала(УникальныйИдентификаторУслуги, КлючСтрокиСоставаУслуги, ИдентификаторБиоматериала) Экспорт
	// Найдем запись регистра с заданным идентификатором биоматериала
	Запрос_ = Новый Запрос;
	Запрос_.Текст =
	"ВЫБРАТЬ
	|	СтатусыБиоматериала.СтатусБиоматериала,
	|	СтатусыБиоматериала.СведенияОБиоматериале,
	|	СтатусыБиоматериала.ДатаУстановкиСтатуса
	|ИЗ
	|	РегистрСведений.СтатусыБиоматериала КАК СтатусыБиоматериала
	|ГДЕ
	|	СтатусыБиоматериала.ИдентификаторБиоматериала = &ИдентификаторБиоматериала";
	
	Запрос_.УстановитьПараметр("ИдентификаторБиоматериала", ИдентификаторБиоматериала);
	Выборка_ = Запрос_.Выполнить().Выбрать();
	Выборка_.Следующий();
	
	УстановитьСтатусБиоматериала(
								УникальныйИдентификаторУслуги,
								КлючСтрокиСоставаУслуги,
								Выборка_.СтатусБиоматериала,
								Выборка_.ДатаУстановкиСтатуса,
								Выборка_.СведенияОБиоматериале,
								ИдентификаторБиоматериала
						);
КонецПроцедуры

////Процедура УдалитьСтатусБиоматериала
//       удаляет с регистра статусов биоматериала все записи по УИ
//
///
Процедура УдалитьСтатусБиоматериала(Знач УникальныйИдентификаторУслуги) Экспорт
	
	НаборЗаписей_ = РегистрыСведений.СтатусыБиоматериала.СоздатьНаборЗаписей();
	НаборЗаписей_.Отбор.УникальныйИдентификаторУслуги.Установить(УникальныйИдентификаторУслуги);
	НаборЗаписей_.Записать(Истина);
	
КонецПроцедуры

/// Функция РазрешеноУдалениеСтатусаБиоматериала
//      Проверяет возожность удаления статуса биоматериала
//
// Параметры:
//    УникальныйИдентификаторУслуги
//         уникальный идентификатор услуги.
//
// Возврат:
//     Неопределено - нет записи в регистре, Истина - удаление разрешено, Ложь - удаление разрешено.
///
Функция РазрешеноУдалениеСтатусаБиоматериала(Знач УникальныйИдентификаторУслуги) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	СтатусыБиоматериала.СтатусБиоматериала
		|ИЗ
		|	РегистрСведений.СтатусыБиоматериала КАК СтатусыБиоматериала
		|ГДЕ
		|	СтатусыБиоматериала.УникальныйИдентификаторУслуги = &УникальныйИдентификаторУслуги
		|	И СтатусыБиоматериала.КлючСтрокиСоставаУслуги <> 0";
	
	Запрос.УстановитьПараметр("УникальныйИдентификаторУслуги", УникальныйИдентификаторУслуги);
	РезультатЗапроса = Запрос.Выполнить();
	
	Если РезультатЗапроса.Пустой() Тогда
		Возврат Неопределено;
	КонецЕсли;
	
	РольУдаленияДоступна = Пользователи.РолиДоступны("ПравоПринудительногоУдаленияСтатусаБиоматериала");
	
	Если РольУдаленияДоступна Тогда
		Возврат Истина;
	КонецЕсли;
	
	Выгрузка = РезультатЗапроса.Выгрузить();
	
	Если Выгрузка.Количество() = 1 И
		 Выгрузка.Получить(0).СтатусБиоматериала = Перечисления.СтатусыБиоматериала.ОжиданиеРегистрации
	Тогда
		Возврат Истина;
	Иначе
		Возврат Ложь;
	КонецЕсли;
	
КонецФункции

// блокирует исключительно регистр по уникальному идентифкиатору
Процедура ЗаблокироватьДанные(Знач УникальныйИдентификаторУслуги) Экспорт
	
	БлокировкаДанных = Новый БлокировкаДанных;
	ЭлементБлокировки = БлокировкаДанных.Добавить("РегистрСведений.СтатусыБиоматериала");
	ЭлементБлокировки.Режим = РежимБлокировкиДанных.Исключительный;
	ЭлементБлокировки.УстановитьЗначение("УникальныйИдентификаторУслуги", УникальныйИдентификаторУслуги);
	БлокировкаДанных.Заблокировать();
	
КонецПроцедуры

////
 // Процедура: ПроверитьНаличиеЗарегистрированногоСтатусаБиоматериала
 //   Проверяет имеют ли услуги статус биоматериала "Зарегистрирован".
 //   При наличии формирует сообщение и присваивает переменной Отказ истину.
 //
 // Параметры:
 //   ДокументОбъект (ДокументОбъект)
 //     Описание.
 //   ЛабМедицинскиеУслуги (ТаблицаЗначений, ТЧ)
 //     ТаблицаЗначений с 2мя колонками УИД и Номенклатура
 // Возврат:
 //   
///
Процедура ПроверитьНаличиеЗарегистрированногоСтатусаБиоматериала(ДокументОбъект, ЛабМедицинскиеУслуги, Отказ) Экспорт
	
	РольУдаленияДоступна_ = Пользователи.РолиДоступны("ПравоПринудительногоУдаленияСтатусаБиоматериала");
	
	Если РольУдаленияДоступна_ Тогда
		Возврат;
	КонецЕсли;

	НоменклатураПоУИД_ = Новый Соответствие;
	СписокУИД_ = Новый Массив;
	Для Каждого строкаТЧ_ Из ЛабМедицинскиеУслуги Цикл
		СписокУИД_.Добавить(строкаТЧ_.УникальныйИдентификаторУслуги);
		НоменклатураПоУИД_.Вставить(строкаТЧ_.УникальныйИдентификаторУслуги, строкаТЧ_.Номенклатура);
	КонецЦикла;
	
	// Проверим статус по регистру
	Запрос_ = Новый Запрос;
	Запрос_.Текст =
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	СтатусыБиоматериала.УникальныйИдентификаторУслуги,
	|	СтатусыБиоматериала.СтатусБиоматериала
	|ИЗ
	|	РегистрСведений.СтатусыБиоматериала КАК СтатусыБиоматериала
	|ГДЕ
	|	СтатусыБиоматериала.УникальныйИдентификаторУслуги В(&СписокУИД)
	|	И СтатусыБиоматериала.КлючСтрокиСоставаУслуги <> 0";
	Запрос_.УстановитьПараметр("СписокУИД", СписокУИД_);
	Выборка_ = Запрос_.Выполнить().Выбрать();
	
	СписокУИДДляСообщения_ = Новый Массив;
	СписокУИДСИсключениемДляСообщения_ = Новый Массив;
	ДелатьЗапросВЛИС_ = Истина;
	ДляЗапросаВЛИС_ = Новый Массив;
	Пока Выборка_.Следующий() Цикл
		Если Выборка_.СтатусБиоматериала = ПредопределенноеЗначение("Перечисление.СтатусыБиоматериала.Зарегистрирован") Тогда
			Отказ = Истина;
			// Если есть причина для отказа, не будем выполнять долгий запрос в ЛИС для поиска других зарегистрированных.
			ДелатьЗапросВЛИС_ = Ложь;
			СписокУИДДляСообщения_.Добавить(Выборка_.УникальныйИдентификаторУслуги);
		ИначеЕсли Выборка_.СтатусБиоматериала = ПредопределенноеЗначение("Перечисление.СтатусыБиоматериала.ОжиданиеРегистрации") Тогда
			ДляЗапросаВЛИС_.Добавить(Выборка_.УникальныйИдентификаторУслуги);
		КонецЕсли;
	КонецЦикла;
	
	// Проверим статус в ЛИС для услуг которые потенциально могли изменить свой статус, но информация
	// об этом еще не получена.
	Если ДелатьЗапросВЛИС_ И ДляЗапросаВЛИС_.Количество() > 0 Тогда
		
		// Разобьем услуги по узлам в которые необходимо отправить запрос
		УслугиУзлыТЗ_ = ОбщиеМеханизмыHL7.ПолучитьУзлыДляЛабораторныхУслуг(ДляЗапросаВЛИС_);
		УзлыТЗ_ = УслугиУзлыТЗ_.Скопировать();
		УзлыТЗ_.Свернуть("Узел");
		
		// В каждый узел отправим запрос со спиcком соответствующих узлу услуг
		Для Каждого УзелСтрокаТЗ_ Из УзлыТЗ_ Цикл
			Узел_ = УзелСтрокаТЗ_.Узел;
			УслугиУзла_ = УслугиУзлыТЗ_.НайтиСтроки(Новый Структура("Узел", Узел_));
			Услуги_ = УслугиУзлыТЗ_.Скопировать(УслугиУзла_).ВыгрузитьКолонку("УникальныйИдентификаторУслуги");
		
			UID_list_ = "";
			Для Каждого УИД_ Из Услуги_ Цикл
				Если ЗначениеЗаполнено(UID_list_) Тогда
					UID_list_ = UID_list_ + ",";
				КонецЕсли;
				UID_list_ = UID_list_ + Строка(УИД_);
			КонецЦикла;
		
			Попытка
			
				Ответ_ = ОбщиеМеханизмыHL7.IsOrdersActivated(UID_list_,Узел_);
			
			Исключение
				Ответ_ = Неопределено;
				Отказ = Истина;
				ОбщиеМеханизмы.ОбработатьОшибку(
						ИнформацияОбОшибке(),Истина,ДокументОбъект.Метаданные(), ДокументОбъект.Ссылка
				);
				СписокУИДСИсключениемДляСообщения_ = Услуги_;
				Прервать;
			КонецПопытки;
		
			Если Ответ_ <> Неопределено Тогда
			
				ОтветМассивом_ = СтроковыеФункцииКлиентСервер.РазложитьСтрокуВМассивПодстрок(Ответ_);
				
				Врем_ = 0;
				Пока Врем_ < Услуги_.Количество() Цикл
					Знач_ = Булево(ОтветМассивом_[Врем_]);
					Если Знач_ = Истина Тогда
						Отказ = Истина;
						СписокУИДДляСообщения_.Добавить(Услуги_[Врем_]);
					КонецЕсли;
					Врем_ = Врем_ + 1;
				КонецЦикла;
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	
	УслугиДляСообщения_ = "";
	УслугиСУИДДляСообщения_ = "";
	Для Каждого УИД_ Из СписокУИДДляСообщения_ Цикл
		Номенклатура_ = НоменклатураПоУИД_.Получить(УИД_);
		Если УслугиДляСообщения_ <> "" Тогда
			УслугиДляСообщения_ = УслугиДляСообщения_ + ", ";
			УслугиСУИДДляСообщения_ = УслугиСУИДДляСообщения_ + ", ";
		КонецЕсли;
		
		Реквизиты_ = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(Номенклатура_, "Наименование, Артикул");
		УслугиДляСообщения_ = УслугиДляСообщения_ + Реквизиты_.Артикул + " " + Реквизиты_.Наименование;
		УслугиСУИДДляСообщения_ = УслугиСУИДДляСообщения_ + Реквизиты_.Артикул + " " + Реквизиты_.Наименование + " (" + УИД_ + ")";
	КонецЦикла;
	
	Если ЗначениеЗаполнено(УслугиДляСообщения_) Тогда
		СообщенияПользователю.Показать(
				"Услуги_ДляСледующихУслугЗарегистрированБиоматериалИхОтменаНевозможна",
				Новый Структура("Список", УслугиДляСообщения_));
				
		Текст_ = СообщенияПользователю.Получить(
				"Услуги_ДляСледующихУслугЗарегистрированБиоматериалИхОтменаНевозможна",
				Новый Структура("Список", УслугиСУИДДляСообщения_));
		
		ЗаписьЖурналаРегистрации(
			"Проверка статуса биоматериала",
			УровеньЖурналаРегистрации.Предупреждение,
			?(ДокументОбъект <> Неопределено, ДокументОбъект.Метаданные(), Неопределено),
			ДокументОбъект.Ссылка,
			Текст_
		);
	КонецЕсли;
	
	УслугиСИсключениемДляСообщения_ = "";
	УслугиСУИДСИсключениемДляСообщения_ = "";
	Для Каждого УИД_ Из СписокУИДСИсключениемДляСообщения_ Цикл
		Номенклатура_ = НоменклатураПоУИД_.Получить(УИД_);
		Если УслугиСИсключениемДляСообщения_ <> "" Тогда
			УслугиСИсключениемДляСообщения_ = УслугиСИсключениемДляСообщения_ + ", ";
			УслугиСУИДСИсключениемДляСообщения_ = УслугиСУИДСИсключениемДляСообщения_ + ", ";
		КонецЕсли;
		
		Реквизиты_ = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(Номенклатура_, "Наименование, Артикул");
		УслугиСИсключениемДляСообщения_ = УслугиСИсключениемДляСообщения_ + Реквизиты_.Артикул + " " + Реквизиты_.Наименование;
		УслугиСУИДСИсключениемДляСообщения_ = УслугиСУИДСИсключениемДляСообщения_ + Реквизиты_.Артикул + " " + Реквизиты_.Наименование + " (" + УИД_ + ")";
	КонецЦикла;
	
	Если ЗначениеЗаполнено(УслугиСИсключениемДляСообщения_) Тогда
		СообщенияПользователю.Показать(
				"Услуги_ДляСлеующихУслугНеУдалосьОпределитьСтатусБиоматериалИхОтменаНевозможна",
				Новый Структура("Список", УслугиСИсключениемДляСообщения_));
				
		Текст_ = СообщенияПользователю.Получить(
				"Услуги_ДляСлеующихУслугНеУдалосьОпределитьСтатусБиоматериалИхОтменаНевозможна",
				Новый Структура("Список", УслугиСУИДСИсключениемДляСообщения_));
		
		ЗаписьЖурналаРегистрации(
			"Проверка статуса биоматериала",
			УровеньЖурналаРегистрации.Предупреждение,
			?(ДокументОбъект <> Неопределено, ДокументОбъект.Метаданные(), Неопределено),
			ДокументОбъект.Ссылка,
			Текст_
		);
	КонецЕсли;

КонецПроцедуры




#КонецЕсли

#КонецОбласти