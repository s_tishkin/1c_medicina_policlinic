﻿
#Область ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////
// События формы
//

////
 // Процедура: Форма_ПриСозданииНаСервере
 //   Обработчик события формы ПриСозданииНаСервере.
 //   Устанавливает текущее соглашение из параметров,
 //   Заполняет таблицу набора записей.
 ///
&НаСервере
Процедура Форма_ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("Соглашение") Тогда
		Если ТипЗнч(Параметры.Соглашение) = Тип("СправочникСсылка.СоглашенияСКлиентами") Тогда
			ЭтотОбъект.Соглашение = Параметры.Соглашение;
			ЭтотОбъект.ДоговорКонтрагента = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Параметры.Соглашение, "Договор");
			СоглашениеПоМедКартам = ?(Не Соглашение.Типовое, Соглашение.СоглашениеПоМедКартам, Ложь);
		ИначеЕсли ТипЗнч(Параметры.Соглашение) = Тип("СправочникСсылка.ДоговорыКонтрагентов") Тогда
			ЭтотОбъект.ДоговорКонтрагента = Параметры.Соглашение;
			СоглашениеПоМедКартам = Истина;
		КонецЕсли;
	КонецЕсли;
	
	УстановитьОтборыСписка();
	
	// Сообщение
	Сообщение = СообщенияПользователю.Получить("Соглашения_СоглашениеНеПоМедицинскимКартам");
	ЭтотОбъект.Элементы.ТекстСообщения.Заголовок = Сообщение;
	
КонецПроцедуры

////
 // Процедура: ПриОткрытии
 //   Обработчик ПриОткрытии формы. 
 //   Вызывает процедуру изменения видимости элементов.
 //   
 ///
&НаКлиенте
Процедура ПриОткрытии(Отказ)
	УстановитьВидимостьЭлементов();
КонецПроцедуры

////
 // Процедура: ОбработкаОповещения
 //   Обработчик оповещения формы. Случжит для отслеживания изменения реквизита
 //   СоглашениеПоТипамМедКарт у владельца формы.
 //   
 //   
 ///
&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)

	Если ИмяСобытия = ОповещенияФорм.ИзмененыДанныеВФормеОбъекта() И Источник = ЭтотОбъект.ВладелецФормы Тогда
		УстановитьВидимостьЭлементов();
	КонецЕсли;

КонецПроцедуры

&НаСервере
Процедура ПриЗагрузкеДанныхИзНастроекНаСервере(Настройки)
	
	НастройкаПорядокСортировки = Настройки.Получить("ПорядокСортировки");
	Если НЕ НастройкаПорядокСортировки = Неопределено Тогда
		МассивПорядок = СтроковыеФункцииКлиентСервер.РазложитьСтрокуВМассивПодстрок(НастройкаПорядокСортировки);
		Для каждого ПолеСортировки Из МассивПорядок Цикл
			СтрокаСортироки_ = СокрЛП(ПолеСортировки);
			ПозицияРазделителя = Найти(СтрокаСортироки_, " ");
			ИмяПоля = Лев(СтрокаСортироки_, ПозицияРазделителя - 1);
			ТипПорядка = СтрЗаменить(СтрокаСортироки_, ИмяПоля + " ", "");
			Если ПустаяСтрока(ИмяПоля) ИЛИ ПустаяСтрока(ТипПорядка) Тогда
				Продолжить;
			КонецЕсли;
			ЭлементСортировки = ЭтотОбъект.Список.Порядок.Элементы.Добавить(Тип("ЭлементПорядкаКомпоновкиДанных"));
			ЭлементСортировки.Использование = Истина;
			ЭлементСортировки.Поле = Новый ПолеКомпоновкиДанных(ИмяПоля);
			ЭлементСортировки.ТипУпорядочивания = ?(ТипПорядка = "Возр",
													НаправлениеСортировкиКомпоновкиДанных.Возр,
													НаправлениеСортировкиКомпоновкиДанных.Убыв
												);
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	
	ЭтотОбъект.ПорядокСортировки = "";
	Для каждого ЭлементПорядка Из ЭтотОбъект.Список.Порядок.Элементы Цикл
		Если ЭлементПорядка.Использование = Истина Тогда
			Если НЕ ПустаяСтрока(ПорядокСортировки) тогда
				ПорядокСортировки = ПорядокСортировки + ",";
			КонецЕсли;
			ПорядокСортировки = ПорядокСортировки + Строка(ЭлементПорядка.Поле) + " " +
								?(ЭлементПорядка.ТипУпорядочивания = НаправлениеСортировкиКомпоновкиДанных.Возр, "Возр", "Убыв");
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры


////////////////////////////////////////////////////////////////////
// Вспомогательные метода
//


&НаКлиенте
Процедура ПросмотрПоследнейЗагрузки(Команда)
	ОткрытьФорму("РегистрСведений.ПрикрепленныеПациентыСоглашенийНеЗагруженные.ФормаСписка");
КонецПроцедуры

////
 // Процедура: УстановитьВидимостьЭлементов
 //   Для соглашения реквизит которого СоглашениеПоМедКартам = Истина,
 //   делает видимым список, иначе скрывает список и показывает сообщение.
 //   Значение реквизита берет непосредственно из соглашения или
 //   из реквизита формы владельца.
 ///
&НаКлиенте
Процедура УстановитьВидимостьЭлементов()
	Если ЗначениеЗаполнено(Соглашение) Тогда
		Если ЭтотОбъект.ВладелецФормы <> Неопределено 
			И ЭтотОбъект.ВладелецФормы.ИмяФормы = "Справочник.СоглашенияСКлиентами.Форма.ФормаЭлемента"
		Тогда
			Условие = ?(Не ЭтотОбъект.ВладелецФормы.Объект.Типовое, 
							(ЭтотОбъект.ВладелецФормы.Объект.СоглашениеПоМедКартам = Истина), Ложь);
		Иначе
			Условие = СоглашениеПоМедКартам;
		КонецЕсли;
		ЭтотОбъект.Элементы.ДоговорКонтрагента.Видимость = Условие = Истина;
		ЭтотОбъект.Элементы.Список.Видимость = Условие = Истина;
		ЭтотОбъект.Элементы.ТекстСообщения.Видимость = Не Условие = Истина;
	Иначе
		ЭтотОбъект.Элементы.СписокСоглашение.Видимость = Истина;
		ЭтотОбъект.Элементы.ТекстСообщения.Видимость = Ложь;
		Условие = Ложь;
	КонецЕсли;

	ЭтотОбъект.Элементы.Соглашение.Видимость = Условие = Истина;
	Если ЗначениеЗаполнено(Соглашение) Тогда
		ЭтотОбъект.ПоложениеКоманднойПанели = ?(Условие,ПоложениеКоманднойПанелиФормы.Авто,ПоложениеКоманднойПанелиФормы.Нет);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ДоговорКонтрагентаПриИзменении(Элемент)
	УстановитьОтборыСписка();
КонецПроцедуры

&НаСервере
Процедура УстановитьОтборыСписка()
	
	Если ЗначениеЗаполнено(ЭтотОбъект.Соглашение) Тогда
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
													ЭтотОбъект.Список,
													"Соглашение",
													ЭтотОбъект.Соглашение,
													ВидСравненияКомпоновкиДанных.Равно
												);
	ИначеЕсли ЗначениеЗаполнено(ЭтотОбъект.ДоговорКонтрагента) Тогда
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(
													ЭтотОбъект.Список,
													"Соглашение",
													ПолучитьСоглашенияДоговора(ЭтотОбъект.ДоговорКонтрагента),
													ВидСравненияКомпоновкиДанных.ВСписке
												);
	Иначе
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ПолучитьСоглашенияДоговора(Знач Договор)
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	СоглашенияСКлиентами.Ссылка
		|ИЗ
		|	Справочник.СоглашенияСКлиентами КАК СоглашенияСКлиентами
		|ГДЕ
		|	СоглашенияСКлиентами.Договор = &Договор";
	
	Запрос.УстановитьПараметр("Договор", Договор);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();
	
	Результат = Новый Массив;
	Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
		Результат.Добавить(ВыборкаДетальныеЗаписи.Ссылка);
	КонецЦикла;
	
	Возврат Результат;
	
КонецФункции

&НаКлиенте
Процедура СоглашениеПриИзменении(Элемент)
	УстановитьОтборыСписка();
КонецПроцедуры

&НаКлиенте
Процедура РазрешитьОбслуживаниеБезОплаты(Команда)
	
	ОповещениеЗавершения_ = Новый ОписаниеОповещения("ПослеВводаДаты", ЭтотОбъект);
	ПоказатьВводДаты(ОповещениеЗавершения_,
					ОбщегоНазначенияКлиент.ДатаСеанса() + 3*24*60*60,
					СообщенияПользователю.Получить("Общие_ВведитеДатуОкончанияОбслуживанияБезОплаты"),
					ЧастиДаты.Дата
				);
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеВводаДаты(Дата, ДополнительныеПараметры) Экспорт
	
	РазрешитьОбслуживаниеБезОплатыНаСервере(Дата);
	ОповеститьОбИзменении(Тип("РегистрСведенийКлючЗаписи.ПрикрепленныеПациентыСоглашений"));
	
КонецПроцедуры

&НаСервере
Процедура РазрешитьОбслуживаниеБезОплатыНаСервере(Знач ДатаОграничения)
	
	ВыделенныеЗаписи = ЭтотОбъект.Элементы.Список.ВыделенныеСтроки;
	
	Для каждого Запись_ Из ВыделенныеЗаписи Цикл
		
		НаборЗаписей_ = РегистрыСведений.ПрикрепленныеПациентыСоглашений.СоздатьНаборЗаписей();
		НаборЗаписей_.Отбор.Период.Установить(Запись_.Период);
		НаборЗаписей_.Отбор.Соглашение.Установить(Запись_.Соглашение);
		НаборЗаписей_.Отбор.Фамилия.Установить(Запись_.Фамилия);
		НаборЗаписей_.Отбор.Имя.Установить(Запись_.Имя);
		НаборЗаписей_.Отбор.Отчество.Установить(Запись_.Отчество);
		НаборЗаписей_.Отбор.ДатаРождения.Установить(Запись_.ДатаРождения);
		НаборЗаписей_.Прочитать();
		
		Для каждого Запись_ Из НаборЗаписей_ Цикл
			Запись_.ДатаЗавершенияОбслуживанияБезОплаты = ДатаОграничения;
		КонецЦикла;
		
		НаборЗаписей_.Записать();
		
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура СписокПослеУдаления(Элемент)
	ПересчитатьСтоимостьДоговора();
КонецПроцедуры

&НаСервере
Процедура ПересчитатьСтоимостьДоговора()
	Если ЗначениеЗаполнено(ЭтотОбъект.ДоговорКонтрагента) Тогда
	
		ВидДоговора_ = ОбщиеМеханизмы.ЗначениеРеквизитаОбъекта(ЭтотОбъект.ДоговорКонтрагента, "ВидДоговора");
		
		Если ВидДоговора_ = ПредопределенноеЗначение("Перечисление.ВидыДоговоров.РасчетыПоПрикрепленнымПациентам") Тогда
			РегистрыСведений.ДоговорыМедПрограммСуммы.ВыполнитьРасчетСуммыДоговора(
						ЭтотОбъект.ДоговорКонтрагента, ЭтотОбъект.Соглашение);
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ОчиститьВсеДанные(Команда)
	ОчиститьВсеДанныеНаСервере(ЭтотОбъект.Соглашение);
	ОповеститьОбИзменении(Тип("РегистрСведенийКлючЗаписи.ПрикрепленныеПациентыСоглашений"));
КонецПроцедуры

&НаСервереБезКонтекста
Процедура ОчиститьВсеДанныеНаСервере(Знач Соглашение)
	Набор_ = РегистрыСведений.ПрикрепленныеПациентыСоглашений.СоздатьНаборЗаписей();
	Набор_.Отбор.Соглашение.Установить(Соглашение);
	Набор_.Записать();
КонецПроцедуры

#КонецОбласти