﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Заполняет доли цен в многоэтапной услуге
Процедура ЗаполнитьДолиЦенЭтаповМногоэтапнойУслуги(Знач Замещать = Ложь,
													Знач Спецификация = Неопределено,
													Знач Организация = Неопределено,
													Знач Номенклатура = Неопределено
	) Экспорт
	
	ТекущаяДата_ = КонецДня(ТекущаяДатаСеанса());
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	СпецификацииМедицинскихУслуг.Ссылка КАК СпецификацияМедицинскойУслуги,
		|	СпецификацииМедицинскихУслуг.Номенклатура,
		|	ТиповыеСоглашенияОрганизаций.Организация,
		|	ТиповыеСоглашенияОрганизаций.ТиповоеСоглашение,
		|	ЦеныНоменклатурыПоСоглашениям.Цена,
		|	СУММА(ВЫБОР
		|			КОГДА ЦеныНоменклатурыВСоставеМногоэтапной.ИзмененоПользователем = ИСТИНА
		|				ТОГДА 1
		|			ИНАЧЕ 0
		|		КОНЕЦ) КАК КоличествоИзмененных
		|ИЗ
		|	Справочник.Номенклатура КАК СправочникНоменклатура
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.СпецификацииМедицинскихУслуг КАК СпецификацииМедицинскихУслуг
		|		ПО (СпецификацииМедицинскихУслуг.Номенклатура = СправочникНоменклатура.Ссылка)
		|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.СпецификацииМедицинскихУслуг.СоставУслуги КАК СпецификацииМедицинскихУслугСоставУслуг
		|		ПО (СпецификацииМедицинскихУслуг.Ссылка = СпецификацииМедицинскихУслугСоставУслуг.Ссылка)
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЦеныНоменклатурыПоСоглашениям КАК ЦеныНоменклатурыПоСоглашениям
		|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ (ВЫБРАТЬ
		|				СоглашенияСКлиентами.Организация КАК Организация,
		|				МИНИМУМ(СоглашенияСКлиентами.Ссылка) КАК ТиповоеСоглашение
		|			ИЗ
		|				Справочник.СоглашенияСКлиентами КАК СоглашенияСКлиентами
		|			ГДЕ
		|				СоглашенияСКлиентами.Типовое = ИСТИНА
		|				И СоглашенияСКлиентами.ИсточникФинансирования = ЗНАЧЕНИЕ(Справочник.ИсточникиФинансирования.ПЛТ)
		|				И СоглашенияСКлиентами.Статус = ЗНАЧЕНИЕ(Перечисление.СтатусыСоглашенийСКлиентами.Действует)
		|			
		|			СГРУППИРОВАТЬ ПО
		|				СоглашенияСКлиентами.Организация) КАК ТиповыеСоглашенияОрганизаций
		|			ПО ЦеныНоменклатурыПоСоглашениям.Соглашение = ТиповыеСоглашенияОрганизаций.ТиповоеСоглашение
		|		ПО СправочникНоменклатура.Ссылка = ЦеныНоменклатурыПоСоглашениям.Номенклатура
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЦеныНоменклатурыВСоставеМногоэтапной КАК ЦеныНоменклатурыВСоставеМногоэтапной
		|		ПО (СпецификацииМедицинскихУслуг.Ссылка = ЦеныНоменклатурыВСоставеМногоэтапной.СпецификацияМедицинскойУслуги)
		|			И (ТиповыеСоглашенияОрганизаций.Организация = ЦеныНоменклатурыВСоставеМногоэтапной.Организация)
		|			И (СпецификацииМедицинскихУслугСоставУслуг.Номенклатура = ЦеныНоменклатурыВСоставеМногоэтапной.НоменклатураСоставаУслуги)
		|ГДЕ
		|	СправочникНоменклатура.ВидМедицинскойУслуги = ЗНАЧЕНИЕ(Перечисление.ВидыМедицинскихУслуг.Многоэтапная)
		|	И НЕ ЦеныНоменклатурыПоСоглашениям.Номенклатура ЕСТЬ NULL
		|	И (&ПропускатьОтборПоОрганизацие
		|			ИЛИ ТиповыеСоглашенияОрганизаций.Организация = &Организация)
		|	И (&ПропускатьОтборПоСпецификацие
		|			ИЛИ СпецификацииМедицинскихУслуг.Ссылка = &Спецификация)
		|	И (&ПропускатьОтборПоНоменклатуре
		|			ИЛИ СправочникНоменклатура.Ссылка В (&Номенклатура)
		|			ИЛИ СпецификацииМедицинскихУслугСоставУслуг.Номенклатура В (&Номенклатура))
		|
		|СГРУППИРОВАТЬ ПО
		|	СпецификацииМедицинскихУслуг.Ссылка,
		|	СпецификацииМедицинскихУслуг.Номенклатура,
		|	ТиповыеСоглашенияОрганизаций.Организация,
		|	ТиповыеСоглашенияОрганизаций.ТиповоеСоглашение,
		|	ЦеныНоменклатурыПоСоглашениям.Цена
		|
		|ИМЕЮЩИЕ
		|	СУММА(ВЫБОР
		|			КОГДА &Замещать
		|				ТОГДА 0
		|			КОГДА ЦеныНоменклатурыВСоставеМногоэтапной.ИзмененоПользователем = ИСТИНА
		|				ТОГДА 1
		|			ИНАЧЕ 0
		|		КОНЕЦ) = 0";
	
	Запрос.УстановитьПараметр("ДатаСреза", ТекущаяДата_);
	Запрос.УстановитьПараметр("Замещать", Замещать);
	Запрос.УстановитьПараметр("Спецификация", Спецификация);
	Запрос.УстановитьПараметр("ПропускатьОтборПоСпецификацие", НЕ ЗначениеЗаполнено(Спецификация));
	Запрос.УстановитьПараметр("Организация", Организация);
	Запрос.УстановитьПараметр("ПропускатьОтборПоОрганизацие", НЕ ЗначениеЗаполнено(Организация));
	Запрос.УстановитьПараметр("Номенклатура", Номенклатура);
	Запрос.УстановитьПараметр("ПропускатьОтборПоНоменклатуре", НЕ ЗначениеЗаполнено(Номенклатура));
	
	РезультатЗапроса = Запрос.Выполнить();
	
	Выборка = РезультатЗапроса.Выбрать();
	
	Пока Выборка.Следующий() Цикл
		
		НаборЗаписей = РегистрыСведений.ЦеныНоменклатурыВСоставеМногоэтапной.СоздатьНаборЗаписей();
		НаборЗаписей.Отбор.СпецификацияМедицинскойУслуги.Установить(Выборка.СпецификацияМедицинскойУслуги);
		НаборЗаписей.Отбор.Организация.Установить(Выборка.Организация);
		
		ЦенаВсегоСостава = 0;
		Для Каждого СтрокаСоставУслуги Из Выборка.СпецификацияМедицинскойУслуги.СоставУслуги Цикл
			СтрокаНабора = НаборЗаписей.Добавить();
			СтрокаНабора.Номенклатура                  = Выборка.Номенклатура;
			СтрокаНабора.СпецификацияМедицинскойУслуги = Выборка.СпецификацияМедицинскойУслуги;
			СтрокаНабора.Организация                   = Выборка.Организация;
			СтрокаНабора.НоменклатураСоставаУслуги     = СтрокаСоставУслуги.Номенклатура;
			СтрокаНабора.КлючСтроки                    = СтрокаСоставУслуги.КлючСтроки;
			СтрокаНабора.Количество                    = СтрокаСоставУслуги.Количество;
			
			ЦенаНоменклатурыСостава_ = РаботаССоглашениями.ПолучитьЦенуНоменклатуры(СтрокаСоставУслуги.Номенклатура, Выборка.ТиповоеСоглашение);
			СтрокаНабора.Цена = СтрокаНабора.Количество * ?(ЦенаНоменклатурыСостава_ = Неопределено, 0, ЦенаНоменклатурыСостава_);
			ЦенаВсегоСостава = ЦенаВсегоСостава + СтрокаНабора.Цена;
		КонецЦикла;
		
		ОбщаяДоля = 1;
		ОбщаяЦена = Выборка.Цена;
		Для каждого СтрокаНабора Из НаборЗаписей Цикл
			Если ЦенаВсегоСостава = 0 Тогда
				ДоляПлан = 1/НаборЗаписей.Количество();
			Иначе
				ДоляПлан = СтрокаНабора.Цена/ЦенаВсегоСостава;
			КонецЕсли;
			СтрокаНабора.ДоляЦены = ДоляПлан;
			СтрокаНабора.ЦенаВСоставе = СтрокаНабора.ДоляЦены * Выборка.Цена;
			
			Если ОбщаяДоля - СтрокаНабора.ДоляЦены < 0 ИЛИ НаборЗаписей.Количество() - 1 = НаборЗаписей.Индекс(СтрокаНабора) Тогда
				СтрокаНабора.ДоляЦены = ОбщаяДоля;
				СтрокаНабора.ЦенаВСоставе = ОбщаяЦена;
				ОбщаяДоля = 0;
			Иначе
				ОбщаяДоля = ОбщаяДоля - СтрокаНабора.ДоляЦены;
				ОбщаяЦена = ОбщаяЦена - СтрокаНабора.ЦенаВСоставе;
			КонецЕсли;
		КонецЦикла;
		
		НаборЗаписей.Записать();
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли