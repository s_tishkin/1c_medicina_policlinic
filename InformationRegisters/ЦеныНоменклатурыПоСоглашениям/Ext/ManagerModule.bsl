﻿
#Область ПрограммныйИнтерфейс

////
 // Процедура: ЗаполнитьЦеныПоВсемСоглашениям
 ///
Процедура ЗаполнитьЦеныПоВсемСоглашениям(ДатаСрезаЦен = Неопределено) Экспорт
	Цены_ = ПолучитьЦеныНоменклатуры(,, ДатаСрезаЦен);
	Соглашения_ = ПолучитьСоглашения();
	Для Каждого Соглашение_ Из  Соглашения_ Цикл
		ЗаполнитьЦеныПоСоглашению(Соглашение_, Цены_);	
	КонецЦикла;
КонецПроцедуры

////
 // Процедура: ЗаполнитьЦеныПоСоглашению
 //
 // Параметры:
 //   Соглашение
 //	  Цены
 ///
Процедура ЗаполнитьЦеныПоСоглашению(Соглашение, Цены = Неопределено) Экспорт
	Если ЗаполнятьЦеныПоСоглашению(Соглашение) Тогда
		Цены_ = Неопределено;
		НоменклатураУслугСоглашения_ = ПолучитьНоменклатуруУслугСоглашения(Соглашение);
		Если Цены = Неопределено Тогда
			Цены_ = ПолучитьЦеныНоменклатуры(ПолучитьВидыЦенСоглашения(Соглашение), НоменклатураУслугСоглашения_);
		Иначе
			Цены_ = ФильтроватьЦеныНоменклатуры(Цены, НоменклатураУслугСоглашения_);
		КонецЕсли;
		
		ЦеныНоменклатуры_ = Новый Соответствие;
		ЗаполнитьЦеныПоВидуЦенСоглашения(Соглашение, Цены_, ЦеныНоменклатуры_);
		ЗаполнитьЦеныПоМедПрограммам(Соглашение, Цены_, ЦеныНоменклатуры_);
		ЗаполнитьЦеныПоЦеновымГруппам(Соглашение, Цены_, ЦеныНоменклатуры_);
		ЗаполнитьЦеныПоУслугам(Соглашение, Цены_, ЦеныНоменклатуры_);
		
		Записи_ = РегистрыСведений.ЦеныНоменклатурыПоСоглашениям.СоздатьНаборЗаписей();	
		Записи_.Отбор.Соглашение.Установить(Соглашение.Ссылка);
		Для Каждого ЦенаНоменклатуры_ Из ЦеныНоменклатуры_ Цикл
			Запись_ = Записи_.Добавить();
			Запись_.Соглашение = Соглашение.Ссылка;
			Запись_.Номенклатура = ЦенаНоменклатуры_.Ключ;
			Запись_.Цена = ЦенаНоменклатуры_.Значение;
		КонецЦикла;
		Записи_.Записать();
	Иначе
		Записи_ = РегистрыСведений.ЦеныНоменклатурыПоСоглашениям.СоздатьНаборЗаписей();	
		Записи_.Отбор.Соглашение.Установить(Соглашение.Ссылка);
		Записи_.Записать();
	КонецЕсли;
КонецПроцедуры

////
 // Функция: ПолучитьВидыЦенСоглашения
 //
 // Параметры:
 //   Соглашение
 //
 // Возврат: {Массив}
 ///
Функция ПолучитьВидыЦенСоглашения(Соглашение)
	ВидыЦен_ = Новый Массив;
	ВидыЦен_.Добавить(Соглашение.ВидЦен);
	Для Каждого Услуга_ Из Соглашение.МедицинскиеУслуги Цикл
		Если ЗначениеЗаполнено(Услуга_.ВидЦены) Тогда
			АлгоритмыДляКоллекций.ДобавитьУникальное(ВидыЦен_, Услуга_.ВидЦены);
		КонецЕсли;
	КонецЦикла;
	Для Каждого ЦеноваяГруппа_ Из Соглашение.ЦеновыеГруппы Цикл
		Если ЗначениеЗаполнено(ЦеноваяГруппа_.ВидЦен) Тогда
			АлгоритмыДляКоллекций.ДобавитьУникальное(ВидыЦен_, ЦеноваяГруппа_.ВидЦен);	
		КонецЕсли;
	КонецЦикла;
	Для Каждого МедПрограмма_ Из Соглашение.МедицинскиеПрограммы Цикл
		Если ЗначениеЗаполнено(МедПрограмма_.ВидЦен) Тогда
			АлгоритмыДляКоллекций.ДобавитьУникальное(ВидыЦен_, МедПрограмма_.ВидЦен);
		КонецЕсли;
	КонецЦикла;
	Возврат ВидыЦен_;
КонецФункции

////
 // Функция: ПолучитьЦеныНоменклатуры
 //
 // Возврат: {ТаблицаЗначений}
 ///
Функция ПолучитьЦеныНоменклатуры(ВидыЦен = Неопределено, Номенклатура = Неопределено, ДатаСрезаЦен = Неопределено)
	Запрос_ = Новый Запрос(
		"ВЫБРАТЬ
		|	Номенклатура.Ссылка
		|ПОМЕСТИТЬ Номенклатура
		|ИЗ
		|	Справочник.Номенклатура КАК Номенклатура
		|ГДЕ
		|	ВЫБОР
		|			КОГДА &ВсяНоменклатура = ИСТИНА
		|				ТОГДА ИСТИНА
		|			ИНАЧЕ Номенклатура.Ссылка В (&Номенклатура)
		|		КОНЕЦ
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ЦеныНоменклатуры.Номенклатура,
		|	ЦеныНоменклатуры.Цена,
		|	ЦеныНоменклатуры.ВидЦены
		|ИЗ
		|	РегистрСведений.ЦеныНоменклатуры.СрезПоследних(
		|			&Дата,
		|			ВЫБОР
		|					КОГДА &ВсеВидыЦен = ИСТИНА
		|						ТОГДА ИСТИНА
		|					ИНАЧЕ ВидЦены В (&ВидыЦен)
		|				КОНЕЦ
		|				И Номенклатура В
		|					(ВЫБРАТЬ
		|						Номенклатура.Ссылка
		|					ИЗ
		|						Номенклатура КАК Номенклатура)) КАК ЦеныНоменклатуры
		|ГДЕ
		|	ЦеныНоменклатуры.Цена > 0"
	);
	Запрос_.УстановитьПараметр("ВсеВидыЦен", ВидыЦен = Неопределено);
	Запрос_.УстановитьПараметр("ВидыЦен", ?(ВидыЦен = Неопределено, Новый Массив, ВидыЦен));
	Запрос_.УстановитьПараметр("Дата", ?(ДатаСрезаЦен = Неопределено, КонецДня(ТекущаяДатаСеанса()), ДатаСрезаЦен));
	Запрос_.УстановитьПараметр("ВсяНоменклатура", Номенклатура = Неопределено);
	Запрос_.УстановитьПараметр(
		"Номенклатура", ?(Номенклатура = Неопределено, Новый Массив, Номенклатура)
	);
	Результат_ = Запрос_.Выполнить();
	Возврат Результат_.Выгрузить();
КонецФункции

////
 // Функция: ФильтроватьЦеныНоменклатуры
 //
 // Параметры:
 //   Цены
 //   Номенклатура
 //
 // Возврат: {ТаблицаЗначений}
 ///
Функция ФильтроватьЦеныНоменклатуры(Цены, Номенклатура = Неопределено)
	Если Номенклатура = Неопределено Тогда
		Возврат Цены;	
	КонецЕсли;
	Запрос_ = Новый Запрос(
		"ВЫБРАТЬ
		|	Цены.Номенклатура,
		|	Цены.Цена,
		|	Цены.ВидЦены
		|ПОМЕСТИТЬ Цены
		|ИЗ
		|	&Цены КАК Цены
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	Цены.Номенклатура,
		|	Цены.Цена,
		|	Цены.ВидЦены
		|ИЗ
		|	Цены КАК Цены
		|ГДЕ
		|	Цены.Номенклатура В(&Номенклатура)"
	);
	Запрос_.УстановитьПараметр("Цены", Цены);
	Запрос_.УстановитьПараметр("Номенклатура", Номенклатура);
	Результат_ = Запрос_.Выполнить();
	Возврат Результат_.Выгрузить();
КонецФункции

////
 // Процедура: ЗаполнитьЦеныПоУслугам
 //
 // Параметры:
 //   Соглашение
 //	  Цены
 //   ЦеныНоменклатуры
 ///
Процедура ЗаполнитьЦеныПоУслугам(Соглашение, Цены, ЦеныНоменклатуры)
	Для Каждого Услуга_ Из Соглашение.МедицинскиеУслуги Цикл
		Если Услуга_.Цена > 0 Тогда
			ЦеныНоменклатуры.Вставить(Услуга_.Номенклатура, Услуга_.Цена);
		Иначе
			Отбор_ = Новый Структура("Номенклатура, ВидЦены", Услуга_.Номенклатура, Услуга_.ВидЦены);
			Услуги_ = Цены.НайтиСтроки(Отбор_);
			Если Услуги_.Количество() > 0 Тогда
				ЦеныНоменклатуры.Вставить(Услуги_[0].Номенклатура, Услуги_[0].Цена);
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

////
 // Процедура: ЗаполнитьЦеныПоЦеновымГруппам
 //
 // Параметры:
 //   Соглашение
 //	  Цены
 //   ЦеныНоменклатуры
 ///
Процедура ЗаполнитьЦеныПоЦеновымГруппам(Соглашение, Цены, ЦеныНоменклатуры)
	Для Каждого ЦеноваяГруппа_ Из Соглашение.ЦеновыеГруппы Цикл
		ЗаполнитьЦеныПоВидуЦен(ЦеноваяГруппа_.ВидЦен, Цены, ЦеныНоменклатуры);
	КонецЦикла;
КонецПроцедуры

////
 // Процедура: ЗаполнитьЦеныПоМедПрограммам
 //
 // Параметры:
 //   Соглашение
 //	  Цены
 //   ЦеныНоменклатуры
 ///
Процедура ЗаполнитьЦеныПоМедПрограммам(Соглашение, Цены, ЦеныНоменклатуры)
	Запрос_ = Новый Запрос;
	Запрос_.Текст =
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	МедицинскиеПрограммыМедицинскиеУслуги.Номенклатура,
	|	СоглашенияСКлиентамиМедПрограммы.ВидЦен
	|ИЗ
	|	Справочник.СоглашенияСКлиентами.МедицинскиеПрограммы КАК СоглашенияСКлиентамиМедПрограммы
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.МедицинскиеПрограммы.МедицинскиеУслуги КАК МедицинскиеПрограммыМедицинскиеУслуги
	|		ПО СоглашенияСКлиентамиМедПрограммы.МедицинскаяПрограмма = МедицинскиеПрограммыМедицинскиеУслуги.Ссылка
	|ГДЕ
	|	СоглашенияСКлиентамиМедПрограммы.Ссылка = &Соглашение";
	
	Запрос_.УстановитьПараметр("Соглашение", Соглашение.Ссылка);
	Выборка_ = Запрос_.Выполнить().Выбрать();
	Пока Выборка_.Следующий() Цикл
		Отбор_ = Новый Структура("Номенклатура, ВидЦены", Выборка_.Номенклатура, Выборка_.ВидЦен);
		Услуги_ = Цены.НайтиСтроки(Отбор_);
		Если Услуги_.Количество() > 0 Тогда
			ЦеныНоменклатуры.Вставить(Услуги_[0].Номенклатура, Услуги_[0].Цена);
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

////
 // Процедура: ЗаполнитьЦеныПоВидуЦенСоглашения
 //
 // Параметры:
 //   Соглашение
 //	  Цены
 //   ЦеныНоменклатуры
 ///
Процедура ЗаполнитьЦеныПоВидуЦенСоглашения(Соглашение, Цены, ЦеныНоменклатуры)
	ЗаполнитьЦеныПоВидуЦен(Соглашение.ВидЦен, Цены, ЦеныНоменклатуры);
КонецПроцедуры

////
 // Процедура: ЗаполнитьЦеныПоВидуЦен
 //
 // Параметры:
 //   ВидЦены
 //	  Цены
 //   ЦеныНоменклатуры
 ///
Процедура ЗаполнитьЦеныПоВидуЦен(ВидЦены, Цены, ЦеныНоменклатуры)
	Отбор_ = Новый Структура("ВидЦены", ВидЦены);
	Услуги_ = Цены.НайтиСтроки(Отбор_);
	Для Каждого Услуга_ Из Услуги_ Цикл
		ЦеныНоменклатуры.Вставить(Услуга_.Номенклатура, Услуга_.Цена);
	КонецЦикла;
КонецПроцедуры

////
 // Функция: ПолучитьСоглашения
 //
 // Параметры:
 //   ВидыЦен
 //
 // Возврат: {ТаблицаЗначений}
 ///
Функция ПолучитьСоглашения(ВидыЦен = Неопределено)
	Запрос_ = Новый Запрос(
		"ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	СоглашенияСКлиентами.Ссылка
		|ПОМЕСТИТЬ Соглашения
		|ИЗ
		|	Справочник.СоглашенияСКлиентами КАК СоглашенияСКлиентами
		|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.СоглашенияСКлиентами.МедицинскиеУслуги КАК СоглашенияСКлиентамиМедицинскиеУслуги
		|		ПО СоглашенияСКлиентами.Ссылка = СоглашенияСКлиентамиМедицинскиеУслуги.Ссылка
		|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.СоглашенияСКлиентами.ЦеновыеГруппы КАК СоглашенияСКлиентамиЦеновыеГруппы
		|		ПО СоглашенияСКлиентами.Ссылка = СоглашенияСКлиентамиЦеновыеГруппы.Ссылка
		|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.СоглашенияСКлиентами.МедицинскиеПрограммы КАК СоглашенияСКлиентамиМедицинскиеПрограммы
		|		ПО СоглашенияСКлиентами.Ссылка = СоглашенияСКлиентамиМедицинскиеПрограммы.Ссылка
		|ГДЕ
		|	ВЫБОР
		|			КОГДА &ВсеВидыЦен = ИСТИНА
		|				ТОГДА ИСТИНА
		|			КОГДА СоглашенияСКлиентами.ВидЦен В (&ВидыЦен)
		|				ТОГДА ИСТИНА
		|			КОГДА СоглашенияСКлиентамиМедицинскиеУслуги.ВидЦены В (&ВидыЦен)
		|				ТОГДА ИСТИНА
		|			КОГДА СоглашенияСКлиентамиЦеновыеГруппы.ВидЦен В (&ВидыЦен)
		|				ТОГДА ИСТИНА
		|			КОГДА СоглашенияСКлиентамиМедицинскиеПрограммы.ВидЦен В (&ВидыЦен)
		|				ТОГДА ИСТИНА
		|			ИНАЧЕ ЛОЖЬ
		|		КОНЕЦ
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	Соглашения.Ссылка,
		|	СоглашенияСКлиентами.ПометкаУдаления,
		|	СоглашенияСКлиентами.Статус КАК Статус,
		|	СоглашенияСКлиентами.Соглашение,
		|	СоглашенияСКлиентами.СегментНоменклатуры,
		|	СоглашенияСКлиентами.ВидЦен,
		|	СоглашенияСКлиентами.ХозяйственнаяОперация,
		|	СоглашенияСКлиентами.ЦеновыеГруппы.(
		|		ВидЦен
		|	),
		|	СоглашенияСКлиентами.МедицинскиеПрограммы.(
		|		ВидЦен
		|	),
		|	СоглашенияСКлиентами.МедицинскиеУслуги.(
		|		Номенклатура,
		|		ВидЦены,
		|		Цена
		|	)
		|ИЗ
		|	Соглашения КАК Соглашения
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.СоглашенияСКлиентами КАК СоглашенияСКлиентами
		|		ПО Соглашения.Ссылка = СоглашенияСКлиентами.Ссылка"
	);
	Запрос_.УстановитьПараметр("ВсеВидыЦен", ?(ВидыЦен = Неопределено, Истина, Ложь));
	Запрос_.УстановитьПараметр("ВидыЦен", ?(ВидыЦен = Неопределено, Новый Массив, ВидыЦен));
	Результат_ = Запрос_.Выполнить();
	Возврат Результат_.Выгрузить();
КонецФункции

////
 // Процедура: ЗаполнитьЦеныПоВидамЦен
 //
 // Параметры:
 //   ВидыЦен
 ///
Процедура ЗаполнитьЦеныПоВидамЦен(Номенклатура, ВидыЦен) Экспорт
	Соглашения_ = ПолучитьСоглашения(ВидыЦен);
	Если Соглашения_.Количество() < Номенклатура.Количество() Тогда
		Цены_ = ПолучитьЦеныНоменклатуры(ВидыЦен);
		Для Каждого Соглашение_ Из Соглашения_ Цикл
			ЗаполнитьЦеныПоСоглашению(Соглашение_, Цены_);	
		КонецЦикла;
	Иначе
		Соглашения_ = ПолучитьСоглашения();
		Цены_ = ПолучитьЦеныНоменклатуры(, Номенклатура);
		ЦеныПоСоглашениям_ = ОбщиеМеханизмы.СоздатьТаблицу("Номенклатура,ЦеныПоСоглашениям");
		Для Каждого Элемент_ Из Номенклатура Цикл
			ЦеныПоСоглашениям_.Добавить().Номенклатура = Элемент_;			
		КонецЦикла;
		Для Каждого Соглашение_ Из Соглашения_ Цикл
			ЗаполнитьЦеныНоменклатурыПоСоглашению(Соглашение_, Цены_, ЦеныПоСоглашениям_);
		КонецЦикла;
		
		Для Каждого Элемент_ Из ЦеныПоСоглашениям_ Цикл
			ЗаписатьЦеныНоменклатурыПоСоглашениям(Элемент_.Номенклатура, Элемент_.ЦеныПоСоглашениям);
		КонецЦикла;
	КонецЕсли;
КонецПроцедуры

////
 // Функция: ПолучитьНоменклатуруУслугСоглашения
 //
 // Параметры:
 //   Соглашение
 //
 // Возврат: {Массив, Неопределено}
 ///
Функция ПолучитьНоменклатуруУслугСоглашения(Соглашение)
	НоменклатураУслуг_ = Неопределено;
	
	Если ЗначениеЗаполнено(Соглашение.СегментНоменклатуры) Тогда
		НоменклатураУслуг_ = СегментыСервер.МассивЭлементов(Соглашение.СегментНоменклатуры);
	КонецЕсли;
	
	Если Соглашение.ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.РеализацияПоМедПрограмме Тогда
		Если НоменклатураУслуг_ = Неопределено Тогда
			НоменклатураУслуг_ = Новый Массив;	
		КонецЕсли;
		Для Каждого Услуга_ Из Соглашение.МедицинскиеУслуги Цикл
			АлгоритмыДляКоллекций.ДобавитьУникальное(НоменклатураУслуг_, Услуга_.Номенклатура);	
		КонецЦикла;
	КонецЕсли;
	
	Возврат НоменклатураУслуг_;
КонецФункции

Процедура ЗаполнитьЦеныНоменклатурыПоСоглашению(Соглашение, Цены, ЦеныПоСоглашениям)
	Если ЗаполнятьЦеныПоСоглашению(Соглашение) Тогда
		НоменклатураУслугСоглашения_ = ПолучитьНоменклатуруУслугСоглашения(Соглашение);
		Цены_ = ФильтроватьЦеныНоменклатуры(Цены, НоменклатураУслугСоглашения_);
		ЦеныНоменклатуры_ = Новый Соответствие;
		ЗаполнитьЦеныПоВидуЦенСоглашения(Соглашение, Цены_, ЦеныНоменклатуры_);
		ЗаполнитьЦеныПоМедПрограммам(Соглашение, Цены_, ЦеныНоменклатуры_);
		ЗаполнитьЦеныПоЦеновымГруппам(Соглашение, Цены_, ЦеныНоменклатуры_);
		ЗаполнитьЦеныПоУслугам(Соглашение, Цены_, ЦеныНоменклатуры_);
		Для Каждого Элемент_ Из ЦеныПоСоглашениям Цикл
			Цена_ = ЦеныНоменклатуры_.Получить(Элемент_.Номенклатура);
			Если Цена_ <> Неопределено Тогда
				Если Элемент_.ЦеныПоСоглашениям = Неопределено Тогда
					Элемент_.ЦеныПоСоглашениям = ОбщиеМеханизмы.СоздатьТаблицу("Соглашение,Цена");
				КонецЕсли;
				
				Запись_ = Элемент_.ЦеныПоСоглашениям.Добавить();
				Запись_.Соглашение = Соглашение.Ссылка;
				Запись_.Цена = Цена_;
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
КонецПроцедуры

Процедура ЗаписатьЦеныНоменклатурыПоСоглашениям(Номенклатура, Цены)
	Записи_ = РегистрыСведений.ЦеныНоменклатурыПоСоглашениям.СоздатьНаборЗаписей();	
	Записи_.Отбор.Номенклатура.Установить(Номенклатура);
	Если Цены <> Неопределено Тогда
		Для Каждого Элемент_ Из Цены Цикл
			Запись_ = Записи_.Добавить();
			Запись_.Номенклатура = Номенклатура;
			Запись_.Соглашение = Элемент_.Соглашение;
			Запись_.Цена = Элемент_.Цена;
		КонецЦикла;
	КонецЕсли;
	Записи_.Записать();
КонецПроцедуры

Функция ЗаполнятьЦеныПоСоглашению(Соглашение)
	Действует_ = ПредопределенноеЗначение("Перечисление.СтатусыСоглашенийСКлиентами.Действует");
	Заполнять_ = ?(Соглашение.ПометкаУдаления = Истина, Ложь, Истина);
	Заполнять_ = ?(Соглашение.Статус = Действует_, Заполнять_, Ложь);
	Если Заполнять_ = Истина Тогда
		Заполнять_ = ?(Соглашение.ВидЦен <> Соглашение.Соглашение.ВидЦен, Истина, Ложь);
		Заполнять_ = ?(Соглашение.ЦеновыеГруппы.Количество() > 0, Истина, Заполнять_);
		Заполнять_ = ?(Соглашение.МедицинскиеПрограммы.Количество() > 0, Истина, Заполнять_);
		Заполнять_ = ?(Соглашение.МедицинскиеУслуги.Количество() > 0, Истина, Заполнять_);
		Заполнять_ = ?(ЗначениеЗаполнено(Соглашение.СегментНоменклатуры), Истина, Заполнять_);
	КонецЕсли;
	Возврат Заполнять_;
КонецФункции

// <Описание функции>
//
// Параметры:
//  ДатаПроверки  - Дата - дата среза регистра цен.
//
// Возвращаемое значение:
//   Булево   - Истина - если на указанную дату существуют заданные цены.
//
Функция СуществуютЦеныНаДату(Знач ДатаПроверки) Экспорт
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ ПЕРВЫЕ 1
		|	ЦеныНоменклатуры.Регистратор
		|ИЗ
		|	РегистрСведений.ЦеныНоменклатуры КАК ЦеныНоменклатуры
		|ГДЕ
		|	ЦеныНоменклатуры.Период = &Период";
	
	Запрос.УстановитьПараметр("Период", ДатаПроверки);
	
	РезультатЗапроса = Запрос.Выполнить();
	
	Возврат НЕ РезультатЗапроса.Пустой();
	
КонецФункции // СуществуютЦеныНаДату()


#КонецОбласти