﻿
#Область ПрограммныйИнтерфейс


&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	Если Параметры.РежимРаботыИзПанелиНавигации И Параметры.Отбор.Свойство("Договор") Тогда
		Элементы.Договор.Видимость = Ложь;
		ЭтотОбъект.РежимРаботыПоДоговору = Истина;
		ЭтотОбъект.Договор = Параметры.Отбор.Договор;
	КонецЕсли;

	Если Параметры.РежимРаботыИзПанелиНавигации И Параметры.Отбор.Свойство("Соглашение") Тогда
		Элементы.Договор.Видимость = Ложь;
		Элементы.Соглашение.Видимость = Ложь;
		ЭтотОбъект.РежимРаботыПоДоговору = Ложь;
		ЭтотОбъект.Соглашение = Параметры.Отбор.Соглашение;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(ЭтотОбъект.Договор) Тогда
		ЗаполнитьСуммуИтого();
	Иначе
		Элементы.СуммаИтого.Видимость = Ложь;
		Элементы.ФормаВыполнитьРасчет.Видимость = Ложь;
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьСуммуИтого()

	Если ЭтотОбъект.РежимРаботыПоДоговору Тогда
		ЭтотОбъект.СуммаИтого = РегистрыСведений.ДоговорыМедПрограммСуммы.ПолучитьСуммуДоговора(ЭтотОбъект.Договор);
	Иначе
		ЭтотОбъект.СуммаИтого = РегистрыСведений.ДоговорыМедПрограммСуммы.ПолучитьСуммуСоглашения(
										ЭтотОбъект.Договор, ЭтотОбъект.Соглашение
									);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ВыполнитьРасчет(Команда)
	ПоказатьВопрос(
		Новый ОписаниеОповещения("ВыполнитьРасчетПослеВопроса", ЭтотОбъект),
		"Текущие данные будут очищены и заполнены заново. Продолжить?",
		РежимДиалогаВопрос.ДаНет
	);
КонецПроцедуры

&НаКлиенте
////
 // Процедура: ВыполнитьРасчетПослеВопроса
 //   Вызывается после ответа на вопрос пользователем
 //
 // Параметры:
 //   П1 (Тип)
 //     Описание
 //   П2 (Тип)
 //     Описание
///
Процедура ВыполнитьРасчетПослеВопроса(Результат, Параметры) Экспорт
	Если Результат <> КодВозвратаДиалога.Да Тогда
		Возврат;
	КонецЕсли;
	
	ВыполнитьРасчетНаСервере();
	Элементы.Список.Обновить();
КонецПроцедуры


&НаСервере
Процедура ВыполнитьРасчетНаСервере()
	РегистрыСведений.ДоговорыМедПрограммСуммы.ВыполнитьРасчетСуммыДоговора(ЭтотОбъект.Договор);
	ЗаполнитьСуммуИтого();
КонецПроцедуры

&НаКлиенте
Процедура СписокПриАктивизацииСтроки(Элемент)
	Если Элементы.Список.ТекущиеДанные <> Неопределено Тогда
		ТекДанные_ = Элементы.Список.ТекущиеДанные;
		СписокПрикрепленных.Параметры.УстановитьЗначениеПараметра("Соглашение",ТекДанные_.Соглашение);
		СписокПрикрепленных.Параметры.УстановитьЗначениеПараметра("Скидка", ТекДанные_.Скидка);
		СписокПрикрепленных.Параметры.УстановитьЗначениеПараметра("Наценка", ТекДанные_.Наценка);
		СписокПрикрепленных.Параметры.УстановитьЗначениеПараметра("СтоимостьМП", ТекДанные_.СтоимостьМедицинскойПрограммы);
	Иначе
		СписокПрикрепленных.Параметры.УстановитьЗначениеПараметра("Соглашение",Неопределено);
		СписокПрикрепленных.Параметры.УстановитьЗначениеПараметра("Скидка", 0);
		СписокПрикрепленных.Параметры.УстановитьЗначениеПараметра("Наценка", 0);
		СписокПрикрепленных.Параметры.УстановитьЗначениеПараметра("СтоимостьМП",0);
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ОбновитьНаСервере()
	Если ЗначениеЗаполнено(ЭтотОбъект.Договор) Тогда
		ЗаполнитьСуммуИтого();
		Элементы.Список.Обновить();
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура Обновить(Команда)
	ОбновитьНаСервере();
КонецПроцедуры




#КонецОбласти