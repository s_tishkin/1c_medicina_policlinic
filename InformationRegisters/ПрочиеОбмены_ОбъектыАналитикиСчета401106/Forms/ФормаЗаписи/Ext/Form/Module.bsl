﻿
#Область ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

// Процедура - обработчик события "ПриСозданииНаСервере" формы.
//
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Объект = РеквизитФормыВЗначение("Запись");
	ИмяРегистра = Метаданные.НайтиПоТипу(ТипЗнч(Объект)).Имя;
	
	ЗаполнитьСпискиВыбора();
	
КонецПроцедуры // ПриСозданииНаСервере()

// Процедура - обработчик события "ПриОткрытии" формы.
//
&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	// установим список и видимость
	УстановитьВидимость();
	ОписаниеОперации = ПолучитьОписаниеХозОперации(Запись.УзелОбмена, Запись.ВидОперации);
	
КонецПроцедуры // ПриОткрытии()

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ПОЛЕЙ ФОРМЫ

// Процедура - обработчик события "ПриИзменении" поля "УзелОбмена".
//
&НаКлиенте
Процедура УзелОбменаПриИзменении(Элемент)
	
	ЗаполнитьСпискиВыбора();
	ПроверитьКорректность();
	УстановитьВидимость();
	
КонецПроцедуры // УзелОбменаПриИзменении()

// Процедура - обработчик события "ПриИзменении" поля "ВидОперации".
//
&НаКлиенте
Процедура ВидОперацииПриИзменении(Элемент)
	
	ЗаполнитьСпискиВыбора();
	ПроверитьКорректность();
	УстановитьВидимость();
	ОписаниеОперации = ПолучитьОписаниеХозОперации(Запись.УзелОбмена, Запись.ВидОперации);
	
КонецПроцедуры // ВидОперацииПриИзменении()

// Процедура - обработчик события "ПриИзменении" поля "Организация".
//
&НаКлиенте
Процедура ОрганизацияПриИзменении(Элемент)
	
	ПроверитьКорректность();
	УстановитьВидимость();
	
КонецПроцедуры // ОрганизацияПриИзменении()

////////////////////////////////////////////////////////////////////////////////
// ОБЩИЕ ПРОЦЕДУРЫ И ФУНКЦИИ (НА СЕРВЕРЕ)

// Функция формирует список организаций узла
// Параметры:
//   ТекущийУзел - ссылка ПрочиеОбмены_УзлыОбмена
// Возвращает:
//   Список значений, организации узла.
&НаСервереБезКонтекста
Функция ПолучитьСпискиОрганизаций(ТекущийУзел)
	
	// установим список и видимость
	Возврат ПрочиеОбмены_ПланыОбмена.ДополнительныеРегистры_ПолучитьОрганизацииУзлаОбмена(ТекущийУзел);
	
КонецФункции // ПолучитьСпискиОрганизаций()

// Функция формирует список хозопераций узла
// Параметры:
//   Режим - число
// Возвращает:
//   Список значений, перечисления.ПрочиеОбмены_ХозяйственныеОперацииБГУ.
&НаСервереБезКонтекста
Функция ПолучитьСпискиХозОпераций(Режим = 0)
	
	ПеречислениеМенеджер = Перечисления.ПрочиеОбмены_ХозяйственныеОперацииБГУ;
	Ответ = Новый СписокЗначений();
	
	Если Режим = 0 Тогда
		// доступные в форме операции
		//Ответ.Добавить(Переч.ВыпускПродукции_ВыпускПродукции);
		
		Ответ.Добавить(ПеречислениеМенеджер.ПоступлениеПрочее_БезвозмездноеПолучениеМежбюджетное);
		Ответ.Добавить(ПеречислениеМенеджер.ПоступлениеПрочее_БезвозмездноеПолучениеПрочее);
		Ответ.Добавить(ПеречислениеМенеджер.ПоступлениеПрочее_ПоступлениеИзлишков);
		
		Ответ.Добавить(ПеречислениеМенеджер.Списание_НаСебестоимость);
		Ответ.Добавить(ПеречислениеМенеджер.Списание_НаСобственныеНужды);
		Ответ.Добавить(ПеречислениеМенеджер.Списание_НедостачиСверхНормы);
		Ответ.Добавить(ПеречислениеМенеджер.Списание_ПоВетхости);
		
		Ответ.Добавить(ПеречислениеМенеджер.ВозвратПоставщику_СчетРазницы);
		
	ИначеЕсли Режим = 1 Тогда //операции с КБК
		
		Ответ.Добавить(ПеречислениеМенеджер.ПоступлениеПрочее_БезвозмездноеПолучениеМежбюджетное);
		Ответ.Добавить(ПеречислениеМенеджер.ПоступлениеПрочее_БезвозмездноеПолучениеПрочее);
		Ответ.Добавить(ПеречислениеМенеджер.ПоступлениеПрочее_ПоступлениеИзлишков);
			
		Ответ.Добавить(ПеречислениеМенеджер.Списание_НаСобственныеНужды);
		Ответ.Добавить(ПеречислениеМенеджер.Списание_НедостачиСверхНормы);
		Ответ.Добавить(ПеречислениеМенеджер.Списание_ПоВетхости);
		
		Ответ.Добавить(ПеречислениеМенеджер.ВозвратПоставщику_СчетРазницы);
		
	ИначеЕсли Режим = 2 Тогда //операции с КОСГУ
	
		Ответ.Добавить(ПеречислениеМенеджер.ВозвратПоставщику_СчетРазницы);
		
	ИначеЕсли Режим = 3 Тогда //операции со счетом
	
		Ответ.Добавить(ПеречислениеМенеджер.ВозвратПоставщику_СчетРазницы);
		
	КонецЕсли;
	
	Возврат Ответ;
	
КонецФункции // ПолучитьСпискиХозОпераций()

// Функция формирует список счетов ЕПСБУ
// Параметры:
//   ТекущийУзел - ссылка ПрочиеОбмены_УзлыОбмена.
//   ТекущаяОперация - Перечисления.ПрочиеОбмены_ХозяйственныеОперацииБГУ
// Возвращает:
//   Список значений, ПрочиеОбмены_ОбъектыВнешнихПрограмм.
&НаСервереБезКонтекста
Функция ПолучитьСпискиСчетовОтклонения(ТекущийУзел, ТекущаяОперация)
	
	// счета для ВозвратаПоставщику
	Ответ = Новый СписокЗначений();
	Если НЕ (ПолучитьСпискиХозОпераций(3).НайтиПоЗначению(ТекущаяОперация) = Неопределено) ТОгда
		
		СписокКодов = Новый СписокЗначений;
		
		Если ЭтоПлатформа77(ТекущийУзел) Тогда
			СписокКодов.Добавить("401.01");
			СписокКодов.Добавить("106.04.1.ПР");
			СписокКодов.Добавить("106.04.2.ПР");
			
		ИначеЕсли ТекущийУзел.ТипКонфигурацииПриемника = Перечисления.ПрочиеОбмены_КонфигурацииПолучатели.ББУ8 Тогда
			СписокКодов.Добавить("401.01");
			СписокКодов.Добавить("106.04.ПР");
			
		Иначе //БГУ
			СписокКодов.Добавить("401.10");
			СписокКодов.Добавить("401.20");
			СписокКодов.Добавить("109.61");
			
		КонецЕсли;	
		
		СтруктураФильтра = Новый Структура("УзелОбмена,КодОбъекта", ТекущийУзел, СписокКодов);
		
		МассивСчетов = ПрочиеОбмены_ПланыОбмена.ПоискСоответствий_ПолучитьСоответствиеОбъекта(
			Перечисления.ПрочиеОбмены_ТипыОбъектовОбмена.ПланСчетов,
			СтруктураФильтра,
			Ложь,
			Истина);
			
		Ответ.ЗагрузитьЗначения(МассивСчетов);
	КонецЕсли;	
	
	Возврат Ответ;
	
КонецФункции // ПолучитьСпискиСчетовОтклонения()

// Функция формирует список узлов
// Параметры:
//   ТекУзел - ссылка ПрочиеОбмены_УзлыОбмена
// Возвращает:
//   Список значений, перечисления.ПрочиеОбмены_ХозяйственныеОперацииБГУ.
&НаСервереБезКонтекста
Функция ПолучитьСпискиУзлов()
	
	РегистрМенеджер = РегистрыСведений.ПрочиеОбмены_ОбъектыАналитикиСчета401106;
	
	Возврат РегистрМенеджер.ПолучитьСписокДоступныхУзловРегистра();
	
КонецФункции // ПолучитьСпискиУзлов()

// Функция формирует представление операции
// Параметры:
//   ТекущийУзел - ссылка ПрочиеОбмены_УзлыОбмена.
//   ТекущаяОперация - Перечисления.ПрочиеОбмены_ХозяйственныеОперацииБГУ
// Возвращает:
//   строка.
&НаСервереБезКонтекста
Функция ПолучитьОписаниеХозОперации(ТекущийУзел, ТекущаяОперация)
	
	ПеречислениеМенеджер = Перечисления.ПрочиеОбмены_ХозяйственныеОперацииБГУ;
	
	Ответ = Новый СписокЗначений();
	
	// доступные в форме операции
	//Ответ.Добавить(Переч.ВыпускПродукции_ВыпускПродукции);
	Если ТекущийУзел.ТипКонфигурацииПриемника = Перечисления.ПрочиеОбмены_КонфигурацииПолучатели.БГУ8 Тогда
		
		СчетСтрока1 = "401.10";
		СчетСтрока2 = "109.61";
		СчетСтрока3 = "401.20";
		
	Иначе
		
		СчетСтрока1 = "401.01";
		СчетСтрока2 = "106.04.ПР";
		СчетСтрока3 = "401.01";
		
	КонецЕсли;	
	
	Ответ.Добавить(ПеречислениеМенеджер.ПоступлениеПрочее_БезвозмездноеПолучениеМежбюджетное,
		"Документ Поступление материалов прочее: КТ, счет %1");
	Ответ.Добавить(ПеречислениеМенеджер.ПоступлениеПрочее_БезвозмездноеПолучениеПрочее,
		"Документ Поступление материалов прочее: КТ, счет %1");
	Ответ.Добавить(ПеречислениеМенеджер.ПоступлениеПрочее_ПоступлениеИзлишков,
		"Документ Поступление материалов прочее: КТ, счет %1");
	
	Ответ.Добавить(ПеречислениеМенеджер.Списание_НаСебестоимость,
		"Документ Списание материалов: ДТ, счет %2
		|Документ Выпуск продукции: КТ, счет %2");
	Ответ.Добавить(ПеречислениеМенеджер.Списание_НаСобственныеНужды,
		"Документ Списание материалов: ДТ, счет %3");
	Ответ.Добавить(ПеречислениеМенеджер.Списание_НедостачиСверхНормы,
		"Документ Списание материалов: ДТ, счет %1");
	Ответ.Добавить(ПеречислениеМенеджер.Списание_ПоВетхости,
		"Документ Списание материалов: ДТ, счет %3");
	
	Ответ.Добавить(ПеречислениеМенеджер.ВозвратПоставщику_СчетРазницы,
		"Документ Возврат поставщику: ДТ, счет разницы (по умолчанию - %3)");
	
	ОтветСтр = "";
	
	Поз = Ответ.НайтиПоЗначению(ТекущаяОперация);
	
	Если Не Поз = Неопределено Тогда
		ОтветСтр = Поз.Представление;
	КонецЕсли;
	ОтветСтр = СтрЗаменить(ОтветСтр, "%1", СчетСтрока1);
	ОтветСтр = СтрЗаменить(ОтветСтр, "%2", СчетСтрока2);
	ОтветСтр = СтрЗаменить(ОтветСтр, "%3", СчетСтрока3);
	
	Возврат ОтветСтр;
	
КонецФункции // ПолучитьОписаниеХозОперации()

// Процедура получает списки значений
//
&НаСервере
Процедура ЗаполнитьСпискиВыбора()
	
	ВременныйСписок = ПолучитьСпискиУзлов();
	ЗаполнитьСписокВыбора(ВременныйСписок, Элементы.УзелОбмена);
	
	ВременныйСписок = ПолучитьСпискиОрганизаций(Запись.УзелОбмена);
	ЗаполнитьСписокВыбора(ВременныйСписок, Элементы.Организация);

	ВременныйСписок = ПолучитьСпискиХозОпераций(0);
	ЗаполнитьСписокВыбора(ВременныйСписок, Элементы.ВидОперации);
	
	ВременныйСписок = ПолучитьСпискиСчетовОтклонения(Запись.УзелОбмена, Запись.ВидОперации);
	ЗаполнитьСписокВыбора(ВременныйСписок, Элементы.СчетОтклонения);
	
КонецПроцедуры // ЗаполнитьСпискиВыбора()

// Процедура заполняет список выбора элемента
// Параметры:
//   СписокИсточник - список значений-источник.
//   ЭлементПриемник - элемент формы, приемник
&НаСервере
Процедура ЗаполнитьСписокВыбора(СписокИсточник, ЭлементПриемник)
	
	ЭлементПриемник.СписокВыбора.Очистить();
	Для Каждого стр Из СписокИсточник цикл
		ЭлементПриемник.СписокВыбора.Добавить(стр.Значение);
	КонецЦикла;
	
КонецПроцедуры // ЗаполнитьСписокВыбора()

// Функция определяет - узел - база ББУ77? 
// Параметры:
//   ТекущийУзел - ссылка ПрочиеОбмены_УзлыОбмена
// Возвращает:
//   булево.
&НаСервереБезКонтекста
Функция ЭтоПлатформа77(ТекущийУзел)
	
	Возврат ПрочиеОбмены_ПланыОбмена.ЭтоББУ77(ТекущийУзел);
	
КонецФункции // ЭтоПлатформа77()

// процедура проверяет корректность заполнения реквизитов
//
&НаСервере
Процедура ПроверитьКорректность()
	
	Объект = РеквизитФормыВЗначение("Запись");
	ИмяРегистра = Метаданные.НайтиПоТипу(ТипЗнч(Объект)).Имя;
	
	СтруктураИзмерений = Новый Структура("УзелОбмена,Организация,ВидОперации",
		ПолучитьСпискиУзлов(),
		ПолучитьСпискиОрганизаций(Запись.УзелОбмена),
		ПолучитьСпискиХозОпераций(0));
		
	СтруктураРесурсов = Новый Структура("НаправленияДеятельности,КБК,КОСГУ,СчетОтклонения",
		"",
		"",
		"",
		ПолучитьСпискиСчетовОтклонения(Запись.УзелОбмена, Запись.ВидОперации));
	
	ПрочиеОбмены_ПланыОбмена.ДополнительныеРегистры_ПроверитьКорректностьЗаписиРегистра(ИмяРегистра, Запись, СтруктураИзмерений, СтруктураРесурсов);
	
КонецПроцедуры // ПроверитьКорректность()

////////////////////////////////////////////////////////////////////////////////
// ОБЩИЕ ПРОЦЕДУРЫ И ФУНКЦИИ (НА КЛИЕНТЕ)

// Процедура устанавливает видимость
//
&НаКлиенте
Процедура УстановитьВидимость()
	
	// управление динамическими страницами результата
	СтруктураПрохода = Новый Структура("СтраницыКБК,СтраницыКОСГУ,СтраницыСчетОтклонения",
	    Новый Структура("ИмяСтраницы,ТипЗапроса", "СтраницаКБК", 1),
		Новый Структура("ИмяСтраницы,ТипЗапроса", "СтраницаКОСГУ", 2),
		Новый Структура("ИмяСтраницы,ТипЗапроса", "СтраницаСчетОтклонения", 3));
		
	Для Каждого стр Из СтруктураПрохода Цикл
		
		ИмяСтраниц = стр.Ключ;
		ИмяСтраницы = стр.Значение.ИмяСтраницы;
		Парам = стр.Значение.ТипЗапроса;
		
		Если (ПолучитьСпискиХозОпераций(Парам).НайтиПоЗначению(Запись.ВидОперации) = Неопределено) Тогда
			ИмяСтраницы = ИмяСтраницы + "Авто";
		КонецЕсли;	
	
		ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
			Элементы,
			ИмяСтраниц,
			"ТекущаяСтраница",
			Элементы[ИмяСтраницы]);
			
	КонецЦикла;
	
	// название полей
	стрНадпись = ?(ЭтоПлатформа77(Запись.УзелОбмена), "Виды приносящей доход деятельности", "Направления деятельности");
	
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
		Элементы, "НаправленияДеятельности", "Заголовок", стрНадпись);
		
	// названия элементов
	СтруктураАналоговИмен = СинонимыББУБГУ(Запись.УзелОбмена);
	СтруктураИменЭлементов = Новый Структура("КОСГУ,КБК");
	Для каждого ОчереднойЭлемент Из СтруктураИменЭлементов Цикл
		
		Для каждого Аналог Из СтруктураАналоговИмен Цикл
			Если Найти(ОчереднойЭлемент.Ключ, Аналог.Ключ) > 0 Тогда
				// имя элемента начинается на тип ББУ
				ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы,
					ОчереднойЭлемент.Ключ, "Заголовок", Аналог.Значение);
				// декорация
				ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы,
					"Декорация" + ОчереднойЭлемент.Ключ, "Заголовок" , "" + Аналог.Значение + " определяется автоматически");
				Прервать;	
			КонецЕсли;
		КонецЦикла;
		
	КонецЦикла;
	
КонецПроцедуры // УстановитьВидимость()

// Функция получает основные синонимы аналитики ББУ-БГУ 
// Параметры:
//   ТекущийУзел - ссылка ПрочиеОбмены_УзлыОбмена
// Возвращает:
//   структура. Ключ - типовое имя ББУ, значение - синоним БГУ.
&НаСервереБезКонтекста
Функция СинонимыББУБГУ(ТекущийУзел)
	
	СтруктураИмен = ПрочиеОбмены_ПланыОбмена.ПолучитьСписокСинонимовПеречислений(ТекущийУзел);
	Возврат СтруктураИмен;
	
КонецФункции // СинонимыББУБГУ()



#КонецОбласти