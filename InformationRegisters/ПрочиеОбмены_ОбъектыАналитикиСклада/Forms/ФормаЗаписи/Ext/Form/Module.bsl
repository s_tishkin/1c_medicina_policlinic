﻿
#Область ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

// Процедура - обработчик события "ПриСозданииНаСервере" формы.
//
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Объект = РеквизитФормыВЗначение("Запись");
	ИмяРегистра = Метаданные.НайтиПоТипу(ТипЗнч(Объект)).Имя;
	
	ЗаполнитьСпискиВыбора();
	
КонецПроцедуры // ПриСозданииНаСервере()

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ПОЛЕЙ ФОРМЫ

// Процедура - обработчик события "ПриИзменении" поля "УзелОбмена".
//
&НаКлиенте
Процедура УзелОбменаПриИзменении(Элемент)
	
	ЗаполнитьСпискиВыбора();	
	ПроверитьКорректность();
	
КонецПроцедуры // УзелОбменаПриИзменении()

// Процедура - обработчик события "ПриИзменении" поля "Организация".
//
&НаКлиенте
Процедура ОрганизацияПриИзменении(Элемент)
	
	ПроверитьКорректность();
	
КонецПроцедуры // ОрганизацияПриИзменении()

////////////////////////////////////////////////////////////////////////////////
// ОБЩИЕ ПРОЦЕДУРЫ И ФУНКЦИИ (НА СЕРВЕРЕ)

// Функция формирует список организаций узла
// Параметры:
//   ТекущийУзел - ссылка ПрочиеОбмены_УзлыОбмена
// Возвращает:
//   Список значений, организации узла.
&НаСервереБезКонтекста
Функция ПолучитьСпискиОрганизаций(ТекущийУзел)
	
	// установим список и видимость
	Возврат ПрочиеОбмены_ПланыОбмена.ДополнительныеРегистры_ПолучитьОрганизацииУзлаОбмена(ТекущийУзел);
	
КонецФункции // ПолучитьСпискиОрганизаций()

// Функция формирует список узлов
// Параметры:
//   ТекУзел - ссылка ПрочиеОбмены_УзлыОбмена
// Возвращает:
//   Список значений, перечисления.ПрочиеОбмены_ХозяйственныеОперацииБГУ.
&НаСервереБезКонтекста
Функция ПолучитьСпискиУзлов()
	
	РегистрМенеджер = РегистрыСведений.ПрочиеОбмены_ОбъектыАналитикиСклада;
	
	Возврат РегистрМенеджер.ПолучитьСписокДоступныхУзловРегистра();
	
КонецФункции // ПолучитьСпискиУзлов()

// Процедура получает списки значений
//
&НаСервере
Процедура ЗаполнитьСпискиВыбора()
	
	ВременныйСписок = ПолучитьСпискиУзлов();
	ЗаполнитьСписокВыбора(ВременныйСписок, Элементы.УзелОбмена);
	
	ВременныйСписок = ПолучитьСпискиОрганизаций(Запись.УзелОбмена);
	ЗаполнитьСписокВыбора(ВременныйСписок, Элементы.Организация);

КонецПроцедуры // ЗаполнитьСпискиВыбора()

// Процедура заполняет список выбора элемента
// Параметры:
//   СписокИсточник - список значений-источник.
//   ЭлементПриемник - элемент формы, приемник
&НаСервере
Процедура ЗаполнитьСписокВыбора(СписокИсточник, ЭлементПриемник)
	
	ЭлементПриемник.СписокВыбора.Очистить();
	Для каждого стр Из СписокИсточник цикл
		ЭлементПриемник.СписокВыбора.Добавить(стр.Значение);
	КонецЦикла;
	
КонецПроцедуры // ЗаполнитьСписокВыбора()

// процедура проверяет корректность заполнения реквизитов
//
&НаСервере
Процедура ПроверитьКорректность()
	
	Объект = РеквизитФормыВЗначение("Запись");
	ИмяРегистра = Метаданные.НайтиПоТипу(ТипЗнч(Объект)).Имя;
	
	СтруктураИзмерений = Новый Структура("УзелОбмена, Организация",
		ПолучитьСпискиУзлов(),
		ПолучитьСпискиОрганизаций(Запись.УзелОбмена));
	СтруктураРесурсов = Новый Структура("ЦМО");
	
	ПрочиеОбмены_ПланыОбмена.ДополнительныеРегистры_ПроверитьКорректностьЗаписиРегистра(ИмяРегистра, Запись, СтруктураИзмерений, СтруктураРесурсов);
	
КонецПроцедуры // ПроверитьКорректность()






#КонецОбласти