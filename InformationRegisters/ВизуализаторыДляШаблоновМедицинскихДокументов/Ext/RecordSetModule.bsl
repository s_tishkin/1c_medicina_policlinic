﻿#Область ОбработчикиСобытий

////
 // Процедура: ПередЗаписью
 //   Обработчик события ПередЗаписью набора записей.
 //   Проверяет наличие одного основного визуализатора.
 ///
Процедура ПередЗаписью(Отказ, Замещение)
	
	// TODO: Рассмотреть другие варианты записи набора.
	Если Истина
		И ЭтотОбъект.Отбор.Визуализатор.Использование = Ложь
		И ЭтотОбъект.Отбор.ШаблонМедицинскогоДокумента.Использование = Истина
	Тогда
		// Считаем количество основных визуализаторов.
		КоличествоОсновных_ = 0;
		Для Каждого Запись_ Из ЭтотОбъект Цикл
			Если Запись_.Основной Тогда
				КоличествоОсновных_ = КоличествоОсновных_ + 1;
			КонецЕсли;
		КонецЦикла;
		ШМДОбъект = ЭтотОбъект.Отбор.ШаблонМедицинскогоДокумента.Значение.ПолучитьОбъект();
		Если Ложь
			Или (Замещение И КоличествоОсновных_ <> 1 И ШМДОбъект <> Неопределено) // ШМДОбъект <> Неопределено случай удаления объекта из иб
			Или (Не Замещение И КоличествоОсновных_ <> 0) // Считаем, что основной уже в базе.
		Тогда
			Отказ = Истина;
			Сообщение_ = Новый СообщениеПользователю;
			Сообщение_.Текст = НСтр("ru = 'Ровно один визуализатор должен быть основным.'");
			Сообщение_.Сообщить();
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти