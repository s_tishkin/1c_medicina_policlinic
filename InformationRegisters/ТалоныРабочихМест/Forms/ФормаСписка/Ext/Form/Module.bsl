﻿
#Область ПрограммныйИнтерфейс


&НаКлиенте
Процедура РазрешитьИзменения(Команда)
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
		Элементы, Элементы.Список.Имя, "ТолькоПросмотр", Ложь
	);
КонецПроцедуры


#КонецОбласти