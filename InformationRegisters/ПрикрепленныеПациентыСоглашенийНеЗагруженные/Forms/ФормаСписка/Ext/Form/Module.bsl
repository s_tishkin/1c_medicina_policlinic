﻿
#Область ПрограммныйИнтерфейс


&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// Максимальный период прикреплений
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	МАКСИМУМ(ОбщиеДанныеЗагрузки.Период) КАК Период
		|ИЗ
		|	(ВЫБРАТЬ
		|		ПрикрепленныеПациентыСоглашенийСрезПоследних.Период КАК Период
		|	ИЗ
		|		РегистрСведений.ПрикрепленныеПациентыСоглашений.СрезПоследних(, ) КАК ПрикрепленныеПациентыСоглашенийСрезПоследних
		|	
		|	ОБЪЕДИНИТЬ ВСЕ
		|	
		|	ВЫБРАТЬ
		|		ПрикрепленныеПациентыСоглашенийНеЗагруженные.Период
		|	ИЗ
		|		РегистрСведений.ПрикрепленныеПациентыСоглашенийНеЗагруженные.СрезПоследних(, ) КАК ПрикрепленныеПациентыСоглашенийНеЗагруженные) КАК ОбщиеДанныеЗагрузки";
		
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда
		ЭтотОбъект.Период = Выборка.Период;
	КонецЕсли;
	
	// Количество загруженных и не загруженных записей в последнюю загрузку
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	СУММА(ВЫБОР
		|			КОГДА ОбщиеДанныеЗагрузки.Загружен = ИСТИНА
		|				ТОГДА 1
		|			ИНАЧЕ 0
		|		КОНЕЦ) КАК КоличествоЗагруженных,
		|	СУММА(ВЫБОР
		|			КОГДА ОбщиеДанныеЗагрузки.Загружен = ЛОЖЬ
		|				ТОГДА 1
		|			ИНАЧЕ 0
		|		КОНЕЦ) КАК КоличествоНЕЗагруженных
		|ИЗ
		|	(ВЫБРАТЬ
		|		ИСТИНА КАК Загружен
		|	ИЗ
		|		РегистрСведений.ПрикрепленныеПациентыСоглашений КАК ПрикрепленныеПациентыСоглашенийСрезПоследних
		|	ГДЕ
		|		ПрикрепленныеПациентыСоглашенийСрезПоследних.Период = &Период
		|	
		|	ОБЪЕДИНИТЬ ВСЕ
		|	
		|	ВЫБРАТЬ
		|		ЛОЖЬ
		|	ИЗ
		|		РегистрСведений.ПрикрепленныеПациентыСоглашенийНеЗагруженные КАК ПрикрепленныеПациентыСоглашенийНеЗагруженные
		|	ГДЕ
		|		ПрикрепленныеПациентыСоглашенийНеЗагруженные.Период = &Период) КАК ОбщиеДанныеЗагрузки";
		
	Запрос.УстановитьПараметр("Период", ЭтотОбъект.Период);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда
		ЗаполнитьЗначенияСвойств(ЭтотОбъект, Выборка);
	КонецЕсли;
	
	// Общие данные не загруженных записей
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	ПрикрепленныеПациентыСоглашенийНеЗагруженные.МенеджерЗагрузки,
	|	ПрикрепленныеПациентыСоглашенийНеЗагруженные.ЭтоОткрепление
	|ИЗ
	|	РегистрСведений.ПрикрепленныеПациентыСоглашенийНеЗагруженные КАК ПрикрепленныеПациентыСоглашенийНеЗагруженные";
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда
		ЗаполнитьЗначенияСвойств(ЭтотОбъект, Выборка);
	КонецЕсли;
	
КонецПроцедуры


#КонецОбласти