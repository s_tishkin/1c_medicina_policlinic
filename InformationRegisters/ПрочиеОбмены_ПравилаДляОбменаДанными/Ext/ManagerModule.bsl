﻿
#Область ПрограммныйИнтерфейс

#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

// загружает зачитанные правила конвертации объектов из ИБ для плана обмена
//
Процедура ЗагрузитьПравила(Отказ, Запись, АдресВременногоХранилища = "", ИмяФайлаПравил = "") Экспорт
	
	// проверка заполнения обязательныех полей записи
	ВыполнитьПроверкуЗаполненияПолей(Отказ, Запись);
	
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	// получаем двоичные данные правил из файла или макета конфигурации
	Если Запись.ИсточникПравил = Перечисления.ИсточникиПравилДляОбменаДанными.МакетКонфигурации Тогда
		
		ДвоичныеДанные = ПолучитьДвоичныеДанныеИзМакетаКонфигурации(Отказ, Запись.УзелОбмена, Запись.ИмяМакетаПравил);
		
	Иначе
		
		ДвоичныеДанные = ПолучитьИзВременногоХранилища(АдресВременногоХранилища);
		
	КонецЕсли;
	
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	// получаем имя временного файла в локальной ФС на сервере
	ИмяВременногоФайла = ПолучитьИмяВременногоФайла("xml");
	
	// получаем файл правил для зачитки
	ДвоичныеДанные.Записать(ИмяВременногоФайла);
	
	Если Запись.ВидПравил = Перечисления.ВидыПравилДляОбменаДанными.ПравилаКонвертацииОбъектов Тогда
		
		// зачитываем правила конвертации
		КонвертацияОбъектовИнформационныхБаз = Обработки.КонвертацияОбъектовИнформационныхБаз.Создать();
		
		// свойства обработки
		КонвертацияОбъектовИнформационныхБаз.ИмяПланаОбменаВРО = "";//Запись.ИмяПланаОбмена;
		
		// методы обработки
		ПравилаЗачитанные = КонвертацияОбъектовИнформационныхБаз.ПолучитьСтруктуруПравилОбмена(ИмяВременногоФайла);
		
		ИнформацияОПравилах = КонвертацияОбъектовИнформационныхБаз.ПолучитьИнформациюОПравилах();
		
		Если КонвертацияОбъектовИнформационныхБаз.ФлагОшибки() Тогда
			Отказ = Истина;
		КонецЕсли;
		
	// Иначе // ПравилаРегистрацииОбъектов
	//	
	//	// зачитываем правила регистрации
	//	ЗагрузкаПравилРегистрации = Обработки.ЗагрузкаПравилРегистрацииОбъектов.Создать();
	//	
	//	// свойства обработки
	//	ЗагрузкаПравилРегистрации.ИмяПланаОбменаДляЗагрузки = Запись.ИмяПланаОбмена;
	//	
	//	// методы обработки
	//	ЗагрузкаПравилРегистрации.ЗагрузитьПравила(ИмяВременногоФайла);
	//	
	//	ПравилаЗачитанные = ЗагрузкаПравилРегистрации.ПравилаРегистрацииОбъектов;
	//	
	//	ИнформацияОПравилах = ЗагрузкаПравилРегистрации.ПолучитьИнформациюОПравилах();
	//	
	//	Если ЗагрузкаПравилРегистрации.ФлагОшибки Тогда
	//		Отказ = Истина;
	//	КонецЕсли;
	Иначе
		Отказ = Истина;
	КонецЕсли;
	
	// удаляем временный файл правил
	УдалитьВременныйФайл(ИмяВременногоФайла);
	
	Если Не Отказ Тогда
		
		Запись.ПравилаXML          = Новый ХранилищеЗначения(ДвоичныеДанные, Новый СжатиеДанных());
		Запись.ПравилаЗачитанные   = Новый ХранилищеЗначения(ПравилаЗачитанные);
		
		Запись.ИнформацияОПравилах = ИнформацияОПравилах;
		
		Запись.ИмяФайлаПравил = ИмяФайлаПравил;
		
		Запись.ПравилаЗагружены = Истина;
		
		Запись.ИмяПланаОбменаИзПравил = "";//Запись.ИмяПланаОбмена;
		
	КонецЕсли;
	
КонецПроцедуры // ЗагрузитьПравила()

// Получает зачитанные правила конвертации объектов из ИБ для плана обмена
//
// Параметры:
//  УзелОбмена - узел обмена
// 
// Возвращаемое значение:
//  ПравилаЗачитанные - ХранилищеЗначения - зачитанные правила конвертации объектов.
//  Неопределено - Если правила конвертации не были загружены в базу для плана обмена.
//
Функция ПолучитьЗачитанныеПравилаКонвертацииОбъектов(Знач УзелОбмена) Экспорт
	
	// возвращаемое значение функции
	ПравилаЗачитанные = Неопределено;
	
	ТекстЗапроса = 
	"ВЫБРАТЬ
	|	ПрочиеОбмены_ПравилаДляОбменаДанными.ПравилаЗачитанные КАК ПравилаЗачитанные
	|ИЗ
	|	РегистрСведений.ПрочиеОбмены_ПравилаДляОбменаДанными КАК ПрочиеОбмены_ПравилаДляОбменаДанными
	|ГДЕ
	|	ПрочиеОбмены_ПравилаДляОбменаДанными.УзелОбмена = &УзелОбмена
	|	И ПрочиеОбмены_ПравилаДляОбменаДанными.ВидПравил = ЗНАЧЕНИЕ(Перечисление.ВидыПравилДляОбменаДанными.ПравилаКонвертацииОбъектов)
	|	И ПрочиеОбмены_ПравилаДляОбменаДанными.ПравилаЗагружены";
	
	Запрос = Новый Запрос;
	Запрос.Текст = ТекстЗапроса;
	Запрос.УстановитьПараметр("УзелОбмена", УзелОбмена);
	
	Результат = Запрос.Выполнить();
	
	Если Не Результат.Пустой() Тогда
		
		Выборка = Результат.Выбрать();
		Выборка.Следующий();
		
		ПравилаЗачитанные = Выборка.ПравилаЗачитанные;
		
	КонецЕсли;
	
	Возврат ПравилаЗачитанные;
	
КонецФункции // ПолучитьЗачитанныеПравилаКонвертацииОбъектов()

// Получает сохраненные правила конвертации объектов из ИБ для плана обмена
//
Функция ПолучитьТекущиеПравилаКонвертацииОбъектов(Знач УзелОбмена) Экспорт
	
	// возвращаемое значение функции
	ПравилаЗачитанные = Неопределено;
	
	ТекстЗапроса = 
	"ВЫБРАТЬ
	|	ПрочиеОбмены_ПравилаДляОбменаДанными.ПравилаXML КАК ПравилаЗачитанные
	|ИЗ
	|	РегистрСведений.ПрочиеОбмены_ПравилаДляОбменаДанными КАК ПрочиеОбмены_ПравилаДляОбменаДанными
	|ГДЕ
	|	ПрочиеОбмены_ПравилаДляОбменаДанными.УзелОбмена = &УзелОбмена
	|	И ПрочиеОбмены_ПравилаДляОбменаДанными.ВидПравил = ЗНАЧЕНИЕ(Перечисление.ВидыПравилДляОбменаДанными.ПравилаКонвертацииОбъектов)
	|	И ПрочиеОбмены_ПравилаДляОбменаДанными.ПравилаЗагружены";
	
	Запрос = Новый Запрос;
	Запрос.Текст = ТекстЗапроса;
	Запрос.УстановитьПараметр("УзелОбмена", УзелОбмена);
	
	Результат = Запрос.Выполнить();
	
	Если Не Результат.Пустой() Тогда
		
		Выборка = Результат.Выбрать();
		Выборка.Следующий();
		
		ПравилаЗачитанные = Выборка.ПравилаЗачитанные;
		
	КонецЕсли;
	
	Возврат ПравилаЗачитанные;
	
КонецФункции // ПолучитьТекущиеПравилаКонвертацииОбъектов()

// Получает сохраненные правила конвертации объектов из ИБ для плана обмена
//
Функция ПолучитьСтруктуруПравилКонвертации(Отказ, УзелОбмена) Экспорт
	
	ИмяМакета = Справочники.ПрочиеОбмены_УзлыОбмена.ПолучитьИмяМакетаПоУмолчанию(УзелОбмена);
	
	ДвоичныеДанные = ПолучитьДвоичныеДанныеИзМакетаКонфигурации(Отказ, УзелОбмена, ИмяМакета);
	
	Если Отказ Тогда
		Возврат Неопределено;
	КонецЕсли;
	
	// получаем имя временного файла в локальной ФС на сервере
	ИмяВременногоФайла = ПолучитьИмяВременногоФайла("xml");
	
	// получаем файл правил для зачитки
	ДвоичныеДанные.Записать(ИмяВременногоФайла);
	
	// зачитываем правила конвертации
	КонвертацияОбъектовИнформационныхБаз = Обработки.КонвертацияОбъектовИнформационныхБаз.Создать();
		
	// свойства обработки
	КонвертацияОбъектовИнформационныхБаз.ИмяПланаОбменаВРО = "";//Запись.ИмяПланаОбмена;
		
	// методы обработки
	ПравилаЗачитанные = КонвертацияОбъектовИнформационныхБаз.ПолучитьСтруктуруПравилОбмена(ИмяВременногоФайла);

	Если КонвертацияОбъектовИнформационныхБаз.ФлагОшибки() Тогда
		Отказ = Истина;
	КонецЕсли;

	// удаляем временный файл правил
	УдалитьВременныйФайл(ИмяВременногоФайла);
	
	Возврат ПравилаЗачитанные;
	
КонецФункции // ПолучитьСтруктуруПравилКонвертации()

// Получает сохраненные правила конвертации объектов из ИБ для плана обмена
//
Процедура ЗагрузитьИнформациюОПравилах(Отказ, АдресВременногоХранилища, СтрокаИнформацииОПравилах) Экспорт
	
	Перем ВидПравил;
	
	СтрокаИнформацииОПравилах = "";
	
	ДвоичныеДанные = ПолучитьИзВременногоХранилища(АдресВременногоХранилища);
	
	// получаем имя временного файла в локальной ФС на сервере
	ИмяВременногоФайла = ПолучитьИмяВременногоФайла("xml");
	
	// получаем файл правил для зачитки
	ДвоичныеДанные.Записать(ИмяВременногоФайла);
	
	ОпределитьВидПравилДляОбменаДанными(ВидПравил, ИмяВременногоФайла, Отказ);
	
	Если Отказ Тогда
		Возврат;
	КонецЕсли;
	
	Если ВидПравил = Перечисления.ВидыПравилДляОбменаДанными.ПравилаКонвертацииОбъектов Тогда
		
		// зачитываем правила конвертации
		КонвертацияОбъектовИнформационныхБаз = Обработки.КонвертацияОбъектовИнформационныхБаз.Создать();
		
		КонвертацияОбъектовИнформационныхБаз.ЗагрузитьПравилаОбмена(ИмяВременногоФайла, "XMLФайл",, Истина);
		
		Если КонвертацияОбъектовИнформационныхБаз.ФлагОшибки() Тогда
			Отказ = Истина;
		КонецЕсли;
		
		Если Не Отказ Тогда
			СтрокаИнформацииОПравилах = КонвертацияОбъектовИнформационныхБаз.ПолучитьИнформациюОПравилах();
		КонецЕсли;
		
	Иначе // ПравилаРегистрацииОбъектов
		Отказ = Истина;
		
		//// зачитываем правила регистрации
		//ЗагрузкаПравилРегистрации = Обработки.ЗагрузкаПравилРегистрацииОбъектов.Создать();
		//
		//ЗагрузкаПравилРегистрации.ЗагрузитьПравила(ИмяВременногоФайла, Истина);
		//
		//Если ЗагрузкаПравилРегистрации.ФлагОшибки Тогда
		//	Отказ = Истина;
		//КонецЕсли;
		//
		//Если Не Отказ Тогда
		//	СтрокаИнформацииОПравилах = ЗагрузкаПравилРегистрации.ПолучитьИнформациюОПравилах();
		//КонецЕсли;
		
	КонецЕсли;
	
	// удаляем временный файл правил
	УдалитьВременныйФайл(ИмяВременногоФайла);
	
КонецПроцедуры // ЗагрузитьИнформациюОПравилах()

// Получает сохраненные правила конвертации объектов из ИБ для плана обмена
//
Функция ПолучитьДвоичныеДанныеИзМакетаКонфигурации(Отказ, УзелОбмена, ИмяМакета)
	
	// получаем имя временного файла в локальной ФС на сервере
	ИмяВременногоФайла = ПолучитьИмяВременногоФайла("xml");
	
	ПланОбменаМенеджер = Справочники.ПрочиеОбмены_УзлыОбмена;
	
	// получаем макет типовых правил
	Попытка
		МакетПравил = ПланОбменаМенеджер.ПолучитьМакет(ИмяМакета);
	Исключение
		Параметры_ = Новый Структура();
		Параметры_.Вставить("ИмяМакета",ИмяМакета);
		Параметры_.Вставить("ИмяПланаОбмена",УзелОбмена);
		СтрокаСообщения = СообщенияПользователю.Получить("Общие_ОшибкаПолученияМакетаКонфигурации",Параметры_);
		ОбменДаннымиСервер.СообщитьОбОшибке(СтрокаСообщения, Отказ);
		
		Возврат Неопределено;
		
	КонецПопытки;
	
	МакетПравил.Записать(ИмяВременногоФайла);
	
	ДвоичныеДанные = Новый ДвоичныеДанные(ИмяВременногоФайла);
	
	// удаляем временный файл правил
	УдалитьВременныйФайл(ИмяВременногоФайла);
	
	Возврат ДвоичныеДанные;
	
КонецФункции // ПолучитьДвоичныеДанныеИзМакетаКонфигурации()

// Удаляет временный файл
//
Процедура УдалитьВременныйФайл(ИмяВременногоФайла)
	
	Попытка
		Если Не ПустаяСтрока(ИмяВременногоФайла) Тогда
			УдалитьФайлы(ИмяВременногоФайла);
		КонецЕсли;
	Исключение
	КонецПопытки;
	
КонецПроцедуры // УдалитьВременныйФайл()

// Проверяет заполнение
//
Процедура ВыполнитьПроверкуЗаполненияПолей(Отказ, Запись)
	
	Если Не ЗначениеЗаполнено(Запись.УзелОбмена) Тогда
		
		НСтрока = СообщенияПользователю.Получить("Общие_НеЗаданПланОбмена");
		
		ОбменДаннымиСервер.СообщитьОбОшибке(НСтрока, Отказ);
		
	ИначеЕсли Запись.ИсточникПравил = Перечисления.ИсточникиПравилДляОбменаДанными.МакетКонфигурации
		    И ПустаяСтрока(Запись.ИмяМакетаПравил) Тогда
		
		НСтрока = СообщенияПользователю.Получить("Общие_НеУказаныТиповыеПравила");
		
		ОбменДаннымиСервер.СообщитьОбОшибке(НСтрока, Отказ);
		
	КонецЕсли;
	
КонецПроцедуры // ВыполнитьПроверкуЗаполненияПолей()

// Определяет вид правил
//
Процедура ОпределитьВидПравилДляОбменаДанными(ВидПравил, ИмяФайла, Отказ)
	
	// открываем файл для чтения
	Попытка
		Правила = Новый ЧтениеXML();
		Правила.ОткрытьФайл(ИмяФайла);
		Правила.Прочитать();
	Исключение
		Правила = Неопределено;
		
		НСтрока = СообщенияПользователю.Получить("Общие_ОшибкаРазбораXMLФайла");
		ОбменДаннымиСервер.СообщитьОбОшибке(НСтрока, Отказ);
		Возврат;
	КонецПопытки;
	
	Если Правила.ТипУзла = ТипУзлаXML.НачалоЭлемента Тогда
		
		Если Правила.ЛокальноеИмя = "ПравилаОбмена" Тогда
			
			ВидПравил = Перечисления.ВидыПравилДляОбменаДанными.ПравилаКонвертацииОбъектов;
			
		//ИначеЕсли Правила.ЛокальноеИмя = "ПравилаРегистрации" Тогда
		//	
		//	ВидПравил = Перечисления.ВидыПравилДляОбменаДанными.ПравилаРегистрацииОбъектов;
			
		Иначе
			
			НСтрока = СообщенияПользователю.Получить("Общие_ОшибкаФорматаПравил");
			ОбменДаннымиСервер.СообщитьОбОшибке(НСтрока, Отказ);
		КонецЕсли;
		
	Иначе
		
		НСтрока = СообщенияПользователю.Получить("Общие_ОшибкаФорматаПравил");
		ОбменДаннымиСервер.СообщитьОбОшибке(НСтрока, Отказ);
	КонецЕсли;
	
	Правила.Закрыть();
	Правила = Неопределено;
	
КонецПроцедуры // ОпределитьВидПравилДляОбменаДанными()

// Процедура добавляет запись в регистр по переданным значениям структуры
//
Процедура ДобавитьЗапись(СтруктураЗаписи) Экспорт
	
	ОбменДаннымиСервер.ДобавитьЗаписьВРегистрСведений(СтруктураЗаписи, "ПрочиеОбмены_ПравилаДляОбменаДанными");
	
КонецПроцедуры // ДобавитьЗапись()

#КонецЕсли

#КонецОбласти