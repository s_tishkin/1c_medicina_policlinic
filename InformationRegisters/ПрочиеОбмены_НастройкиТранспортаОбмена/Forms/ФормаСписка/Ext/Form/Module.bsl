﻿
#Область ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

// Процедура - обработчик события "ПриСозданииНаСервере" формы.
//
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("Отбор") И Параметры.Отбор.Свойство("Узел") Тогда

		Узел = Параметры.Отбор.Узел;
		
		ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список, "Узел", Узел, ЗначениеЗаполнено(Узел));
				
		Параметры.Отбор.Удалить("Узел");
				
		Список.Группировка.Элементы.Очистить();
		
		Элементы.Список.ПодчиненныеЭлементы.Узел.Видимость = Ложь;
		
	КонецЕсли;
	
КонецПроцедуры //ПриСозданииНаСервере() 


#КонецОбласти