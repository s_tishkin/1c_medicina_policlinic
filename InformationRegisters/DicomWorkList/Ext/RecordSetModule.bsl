﻿#Область ОбработчикиСобытий

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)

	Интерфейс_ = ОбщиеМеханизмыDICOMКлиентСервер.ПолучитьИнтерфейсDICOM();
	Если Интерфейс_ <> Неопределено Тогда
		Для Каждого Запись_ Из ЭтотОбъект Цикл
			Запись_.RequestedProcedurePriority = "LOW";
			Запись_.StudyInstanceUID = Интерфейс_.GenerateUniqueIdentifier("1.2.276.0.7230010.3.1.2");
			Запись_.УникальныйИдентификаторИсследования = Новый УникальныйИдентификатор;
			#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
			Попытка
				Запись_.Ответственный = Пользователи.ТекущийПользователь();
			Исключение
			КонецПопытки;
			#КонецЕсли
			Запись_.RequestedProcedureID = Запись_.УникальныйИдентификаторИсследования; 
			Запись_.ScheduledProcedureStepID = "0";
			Запись_.СозданФайл = Истина;
		КонецЦикла;
	КонецЕсли;
КонецПроцедуры

Процедура ПередЗаписью(Отказ, Замещение)
	Для Каждого Запись_ Из ЭтотОбъект Цикл
		Запись_.ИмяФайла = Запись_.StudyInstanceUID + ".wl";
		
		Если Запись_.Выполнена Или Запись_.ДобавленоИзMPPS Тогда
			Запись_.СозданФайл = Ложь;
		КонецЕсли;
	КонецЦикла;
	
	Если Замещение Тогда
		// Найдем удаляемые записи
		БДЗаписи_ = РегистрыСведений.DicomWorkList.СоздатьНаборЗаписей();
		
		Для Каждого ЭлементОтбора_ Из ЭтотОбъект.Отбор Цикл
			ЗаполнитьЗначенияСвойств(БДЗаписи_.Отбор[ЭлементОтбора_.Имя], ЭлементОтбора_);
		КонецЦикла;
		
		БДЗаписи_.Прочитать();
		
		ЗаписываемыеЗаписи_ = ЭтотОбъект.Выгрузить();
		
		Для Каждого БДЗапись_ Из БДЗаписи_ Цикл
			Отбор_ = Новый Структура("УникальныйИдентификаторУслуги, КлючСтрокиСоставаУслуги, Выполнена", 
								БДЗапись_.УникальныйИдентификаторУслуги, БДЗапись_.КлючСтрокиСоставаУслуги, Ложь);
			
			ЗаписываемыеНайденные_ = ЗаписываемыеЗаписи_.НайтиСтроки(Отбор_);
			Если (ЗаписываемыеНайденные_.Количество() = 0 Или ЗаписываемыеНайденные_[0].СозданФайл = Ложь)
				И БДЗапись_.СозданФайл = Истина
			Тогда

				Каталог_ = ОбщиеМеханизмыDICOM.ПолучитьКаталогФайловWorkList();

				Если ЗначениеЗаполнено(Каталог_) Тогда
					УдалитьФайлы(Каталог_, БДЗапись_.ИмяФайла);
				Иначе
					Отказ = Истина;
				КонецЕсли;
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;

КонецПроцедуры

Процедура ПриЗаписи(Отказ, Замещение)
	
	Для Каждого Запись_ Из ЭтотОбъект Цикл
		Данные_ = Новый Структура;
		Для Каждого Ресурс_ Из Метаданные.РегистрыСведений.DicomWorkList.Ресурсы Цикл
			Данные_.Вставить(Ресурс_.Имя, Запись_[Ресурс_.Имя]);
		КонецЦикла;
		
		Данные_.Вставить("ИмяФайла", Запись_.ИмяФайла);
		
		Если Запись_.СозданФайл Тогда
			ОбщиеМеханизмыDICOM.ЗаписатьЭлементWorkList(Данные_, Отказ);
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти