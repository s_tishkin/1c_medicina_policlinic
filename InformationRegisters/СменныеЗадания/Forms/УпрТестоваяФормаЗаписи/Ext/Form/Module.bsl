﻿
#Область ПрограммныйИнтерфейс


&НаКлиенте
Процедура Команда1(Команда)
	
	Если ЭтотОбъект.Реквизит1 = "" Тогда 
		ЭтотОбъект.Запись.УникальныйИдентификаторУслуги = 
			Новый УникальныйИдентификатор;
	Иначе
		ЭтотОбъект.Запись.УникальныйИдентификаторУслуги = 
			Новый УникальныйИдентификатор(ЭтотОбъект.Реквизит1);
	КонецЕсли;
			
			
				
				
КонецПроцедуры

&НаКлиенте
Процедура КомандаСохранить(Команда)
	 Сохранить();
КонецПроцедуры

&НаСервере
Процедура Сохранить()
	
	//Объект = ДанныеФормыВЗначение(ЭтотОбъект.Запись, Тип("РегистрСведений.СтатусыУслуг"));
	
	МенеджерЗаписи = РегистрыСведений.СменныеЗадания.СоздатьМенеджерЗаписи();
	ЗаполнитьЗначенияСвойств(МенеджерЗаписи,Запись);
	МенеджерЗаписи.Записать();
	
	ЭтотОбъект.Модифицированность = Ложь;

КонецПроцедуры



#КонецОбласти