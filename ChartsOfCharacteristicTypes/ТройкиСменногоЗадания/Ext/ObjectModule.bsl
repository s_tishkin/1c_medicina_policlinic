﻿#Область ПрограммныйИнтерфейс


Процедура ПередЗаписью(Отказ)
	Имя_ = "";
	Для Каждого ЭлементСписка Из ЭтотОбъект.Тройки Цикл
		Имя_ = ?(Имя_ = "", "", Имя_ + "; ") + Строка(ЭлементСписка.Врач)+" " +Строка(ЭлементСписка.СМР);
		Имя_ = СокрЛП(Имя_);
	КонецЦикла;
	ЭтотОбъект.Наименование = ?(Имя_ = "", "Нет сотрудников", Имя_);
	
КонецПроцедуры


#КонецОбласти