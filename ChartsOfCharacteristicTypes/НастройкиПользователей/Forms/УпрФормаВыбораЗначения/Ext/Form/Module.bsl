﻿
#Область ПрограммныйИнтерфейс


&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	ЭтотОбъект.рф_СписокЗначений = Параметры.пф_СписокЗначений;
КонецПроцедуры

&НаКлиенте
Процедура КомандаОК(Команда)
	ЭтотОбъект.Закрыть(ЭтотОбъект.рф_СписокЗначений);
КонецПроцедуры

&НаКлиенте
Процедура КомандаОтмена(Команда)
	ЭтотОбъект.Закрыть(Неопределено);
КонецПроцедуры


#КонецОбласти