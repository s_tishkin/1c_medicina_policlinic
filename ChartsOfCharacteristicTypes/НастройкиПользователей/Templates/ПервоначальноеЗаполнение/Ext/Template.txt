﻿<Items
	Description="ПервоначальныеЗначенияНастроек"
	Columns="Имя,УровеньДоступаКНастройкам,ЗначениеПоУмолчанию,Отключен">
	
	<!-- Настройки АРМ Врача -->
	
	<Item
		Имя="УслугаРазборБольногоНаВрачебнойКомиссии"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.Система"
		ЗначениеПоУмолчанию="Справочник.Номенклатура.ПустаяСсылка"
		Отключен = "Истина"
	/>
	
	
	<!-- Настройки ДругиеНастройки -->
	
	<!-- Основные значения для подстановки в документы -->
	
	<Item
		Имя="ОсновнаяОрганизация"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.Система"
		ЗначениеПоУмолчанию="Справочник.Организации.ПустаяСсылка"
		Отключен = "Ложь"
	/>
	
	
	<!-- Настройки Регистратуры -->

	<Item
		Имя="АвтоЗакрытиеОбращения"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.Система"
		ЗначениеПоУмолчанию="10"
		Отключен = "Истина"
	/>
	
	<Item
		Имя="ВозрастДляПредупреждения"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="99"
		Отключен = "Ложь"
	/>
	
	<Item
		Имя="ВозрастОбязательногоЗаконногоПредставителя"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.Система"
		ЗначениеПоУмолчанию="14"
		Отключен = "Ложь"
	/>
	
	<Item
		Имя="ВремяДействияЗаписиОЗаконномПредставителе"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.Система"
		ЗначениеПоУмолчанию="30"
		Отключен = "Ложь"
	/>
	
	<Item
		Имя="ВремяДоЗакрытияНевыполненныхЗаказовПациентов"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.Система"
		ЗначениеПоУмолчанию="30"
		Отключен = "Ложь"
	/>
	
	<Item
		Имя="ДоступныеИсточникиФинансирования"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="ПланВидовХарактеристик.СпискиНастроек.СписокДоступныхИсточниковФинансированияПоУмолчанию"
		Отключен = "Ложь"
	/>
	
	<Item
		Имя="МаксимальныйВозрастПациента"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.Система"
		ЗначениеПоУмолчанию="110"
		Отключен = "Ложь"
	/>

	<Item
		Имя="МестоЖительстваПоУмолчанию"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.Система"
		ЗначениеПоУмолчанию=""
		Отключен = "Ложь"
	/>
	
	<Item
		Имя="НачальныйПериодНумерацииМедицинскихКарт"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.Система"
		ЗначениеПоУмолчанию="20050101"
		Отключен = "Ложь"
	/>
	
	<Item
		Имя="ТипКарты"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="ПланВидовХарактеристик.СпискиНастроек.СписокТиповМедицинскихКартПоУмолчанию"
		Отключен = "Ложь"
	/>
	
	<Item
		Имя="Регистратура_ВремяНаПоискДубликатовЗаказанныхУслуг"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.Система"
		ЗначениеПоУмолчанию="1"
		Отключен = "Ложь"
	/>
	
	<Item
		Имя="Подразделения"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="ПланВидовХарактеристик.СпискиНастроек.СписокДоступныхПодразделенийПоУмолчанию"
		Отключен = "Ложь"
	/>
	
	<Item
		Имя="МаксимальноеКоличествоПовторногоЗаказаУслуги"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.Система"
		ЗначениеПоУмолчанию="30"
		Отключен = "Ложь"
	/>

	<Item
		Имя="СрокУведомленияОВизите"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.Система"
		ЗначениеПоУмолчанию="3"
		Отключен = "Ложь"
	/>

	<Item
		Имя="ИсточникФинансированияПоУмолчанию"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию=""
		Отключен = "Ложь"
	/>
	
	<!-- Настройки Защита персональных данных -->
	
	<Item
		Имя="КоличествоДнейДействияСогласияНаОбработкуПД"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.Система"
		ЗначениеПоУмолчанию="365"
		Отключен = "Ложь"
	/>
	
	<Item
		Имя="УведомлениеОбОтсутствииСогласияНаОбработкуПД"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="Истина"
		Отключен = "Ложь"
	/>

	<!-- Настройки взаимодействий -->
	
	<Item
		Имя="ПолучатьСМС"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="Ложь"
		Отключен = "Ложь"
	/>

		<!-- Сменное задание. Настройки контроля исполнения -->
	
	<Item
		Имя="СменноеЗаданиеВрач"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="Справочник.Сотрудники.ПустаяСсылка"
		Отключен = "Ложь"
	/>
	
	<Item
		Имя="СменноеЗаданиеВрачОтветственныйЗаНазначение"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="Справочник.Сотрудники.ПустаяСсылка"
		Отключен = "Ложь"
	/>
	
	<Item
		Имя="СменноеЗаданиеСМП"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="Справочник.Сотрудники.ПустаяСсылка"
		Отключен = "Ложь"
	/>
	
	<Item
		Имя="СменноеЗаданиеИспользоватьДатуПланирования"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="Ложь"
		Отключен = "Ложь"
	/>
	
	<Item
		Имя="СменноеЗаданиеОтметкаИсполненияДаннымиПланирования"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="Ложь"
		Отключен = "Ложь"
	/>
	
	<Item
		Имя="СменноеЗаданиеТипСменногоЗадания"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="Рабочее место"
		Отключен = "Ложь"
	/>
	
	<Item
		Имя="СменноеЗаданиеДатаВыполнения"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="00010101"
		Отключен = "Ложь"
	/>
	
	<Item
		Имя="СменноеЗаданиеСписокКабинетов"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="ПланВидовХарактеристик.СпискиНастроек.СменноеЗаданиеСписокКабинетов"
		Отключен = "Ложь"
	/>
	
	<Item
		Имя="СменноеЗаданиеГруппаЗаменяемости"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="ПланВидовХарактеристик.СпискиНастроек.СменноеЗаданиеГруппаЗаменяемости"
		Отключен = "Ложь"
	/>
	
	<Item
		Имя="СменноеЗаданиеСписокВрачей"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="ПланВидовХарактеристик.БригадыСменногоЗадания.БригадаПоУмолчанию"
		Отключен = "Ложь"
	/>
	
	<Item
		Имя="СменноеЗаданиеСписокСМП"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="ПланВидовХарактеристик.БригадыСменногоЗадания.БригадаПоУмолчанию"
		Отключен = "Ложь"
	/>
	
	<Item
		Имя="СменноеЗаданиеКоллекцияТроек"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="ПланВидовХарактеристик.ТройкиСменногоЗадания.ТройкаПоУмолчанию"
		Отключен = "Ложь"
	/>

	<Item
		Имя="СменноеЗаданиеСтоматология"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="Ложь"
		Отключен = "Ложь"
	/>

	<Item
		Имя="СменноеЗаданиеПодборУслугПоСпецификациям"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="Истина"
		Отключен = "Ложь"
	/>

	<Item
		Имя="СменноеЗаданиеВремяВыполненияПриема"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="1200"
		Отключен = "Ложь"
	/>

		<!-- Просмотр занятости. -->

	<Item
		Имя="ПросмотрЗанятостиНачалоШкалыВремени"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="00010101090000"
		Отключен = "Ложь"
	/>

	<Item
		Имя="ПросмотрЗанятостиОкончаниеШкалыВремени"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="00010101180000"
		Отключен = "Ложь"
	/>

	<Item
		Имя="ПросмотрЗанятостиПериодАвтообновления"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="0"
		Отключен = "Ложь"
	/>

	<Item
		Имя="ПросмотрЗанятостиПериодШкалыВремени"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="Перечисление.Периодичность.ОдинЧас"
		Отключен = "Ложь"
	/>

	<Item
		Имя="ПросмотрЗанятостиРазмерЯчейкиБезВремени"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="6"
		Отключен = "Ложь"
	/>

	<Item
		Имя="ПросмотрЗанятостиШаблонПредставленияПациента"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="#ФамилияИнициалы#,#Пол#,#Возраст#,#Исполнитель#"
		Отключен = "Ложь"
	/>

	<Item
		Имя="ПросмотрЗанятостиШиринаКолонокРабочихМест"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="20"
		Отключен = "Ложь"
	/>

	<Item
		Имя="ПросмотрЗанятостиШагСетки"
		УровеньДоступаКНастройкам="Перечисление.УровниДоступаКНастройкам.ОграничениеПользователя"
		ЗначениеПоУмолчанию="00010101000000"
		Отключен = "Ложь"
	/>

</Items>
