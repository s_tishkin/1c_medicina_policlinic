﻿
#Область ПрограммныйИнтерфейс

////
// Осуществляет поиск бригады сменного задания, табличная часть которой состоит из 
//  сотрудников и ролей, переданных в таблице значений.
//
// Параметры:
//  СписокБригады	 - ТаблицаЗначений - Список сотрудников бригады и их ролей, 
//   для которых производится поиск бригады
//  СоздаватьПриОтсутствии	 - Булево - Определяет, создавать ли новый элемент, если поиск завершился неудачей.
// 
// Возвращаемое значение:
//  ПланВидовХарактеристикСсылка.БригадыСменногоЗадания - Ссылка на найденный элемент или пустая ссылка
///
Функция НайтиБригаду(СписокБригады, СоздаватьПриОтсутствии = Ложь) Экспорт
	
	Если Не ТипЗнч(СписокБригады) = Тип("ТаблицаЗначений") Тогда
		ВызватьИсключение("Несоответствие типов: Параметр 1.");
	КонецЕсли;
	
	Запрос_ = Новый Запрос;
	Запрос_.Текст =
		"ВЫБРАТЬ
		|	БригадыСотрудниковСписокБригады.Ссылка КАК Ссылка,
		|	БригадыСотрудниковСписокБригады.Сотрудник КАК Сотрудник,
		|	БригадыСотрудниковСписокБригады.Роль
		|ИЗ
		|	ПланВидовХарактеристик.БригадыСменногоЗадания.СписокБригады КАК БригадыСотрудниковСписокБригады
		|
		|УПОРЯДОЧИТЬ ПО
		|	Сотрудник
		|ИТОГИ
		|	КОЛИЧЕСТВО(РАЗЛИЧНЫЕ Сотрудник)
		|ПО
		|	Ссылка";
	Выборка_ = Запрос_.Выполнить().Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
	Пока Выборка_.Следующий() Цикл
		Если Не Выборка_.Сотрудник = СписокБригады.Количество() Тогда
			Продолжить;
		КонецЕсли;
		Флаг_ = Истина;
		ВложеннаяВыборка_ = Выборка_.Выбрать();
		Пока Флаг_ И ВложеннаяВыборка_.Следующий() Цикл
			МассивСтрок_ = СписокБригады.НайтиСтроки(
				Новый Структура("Сотрудник, Роль", ВложеннаяВыборка_.Сотрудник, ВложеннаяВыборка_.Роль)
			);
			Если МассивСтрок_.Количество() = 0 Тогда
				Флаг_ = Ложь;
			КонецЕсли;
		КонецЦикла;
		Если Флаг_ = Истина Тогда
			Возврат Выборка_.Ссылка;
		КонецЕсли;
	КонецЦикла;
	
	Если Не СоздаватьПриОтсутствии = Истина Или СписокБригады.Количество() = 0 Тогда
		Возврат ПланыВидовХарактеристик.БригадыСменногоЗадания.ПустаяСсылка();
	КонецЕсли;
	
	Возврат СоздатьБригаду(СписокБригады);
	
КонецФункции

////
// Создает новый объект плана видов характеристик БригадыСменногоЗадания.
//
// Параметры:
//  СписокБригады - ТаблицаЗначений - Таблица значений с колонками Сотрудник, Роль.
// 
// Возвращаемое значение:
//  ПланВидовХарактеристикСсылка.БригадыСменногоЗадания - Ссылка на новый элемент плана.
//
Функция СоздатьБригаду(СписокБригады)
	
	Объект_ = ПланыВидовХарактеристик.БригадыСменногоЗадания.СоздатьЭлемент();
	Объект_.ТипЗначения = Новый ОписаниеТипов("СправочникСсылка.Сотрудники");
	Для Каждого ЭлементСписка_ Из СписокБригады Цикл
		СтрокаТЧ = Объект_.СписокБригады.Добавить();
		ЗаполнитьЗначенияСвойств(СтрокаТЧ, ЭлементСписка_);
	КонецЦикла;
	Объект_.Записать();
	
	Возврат Объект_.Ссылка;
КонецФункции

#КонецОбласти