﻿
#Область ПрограммныйИнтерфейс


////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ КОМАНД ФОРМЫ

// Процедура - обработчик команды "Выбрать".
//
&НаКлиенте
Процедура Выбрать(Команда)
	
	Если ПроверитьЗаполнение() Тогда
		Параметр = Новый Структура("СтатьяРасходов, АналитикаРасходов", СтатьяРасходов, АналитикаРасходов);
		Закрыть(Параметр);
	КонецЕсли;
	
КонецПроцедуры // Выбрать()


#КонецОбласти