﻿
#Область ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ И ФУНКЦИИ ОБЩЕГО НАЗНАЧЕНИЯ

// Процедура устанавливает текущую страницу для вида операции документа.
//
&НаСервере
Процедура УстановитьТекущуюСтраницу()

	ЭлементСтраница = Элементы.СтраницаНаНаправленияДеятельности;
	Если ЭлементСтраница <> Неопределено Тогда
		Элементы.ГруппаСтраницы.ТекущаяСтраница = ЭлементСтраница;
	КонецЕсли;

КонецПроцедуры // УстановитьТекущуюСтраницу()

// Процедура производит заполнение списка типов значений.
//
// Параметры:
//	ВыбранныйТипЗначения - Тип - Тип аналитики.
//
&НаСервере
Процедура УстановитьТипЗначения(ВыбранныйТипЗначения)

	СписокТиповЗначений = Новый СписокЗначений;
	СписокТиповЗначений.Добавить("СправочникСсылка.МаркетинговыеМероприятия");
	СписокТиповЗначений.Добавить("СправочникСсылка.Номенклатура");
	СписокТиповЗначений.Добавить("СправочникСсылка.Партнеры");
	СписокТиповЗначений.Добавить("СправочникСсылка.Организации");
	СписокТиповЗначений.Добавить("СправочникСсылка.ФизическиеЛица");
	СписокТиповЗначений.Добавить("СправочникСсылка.СтруктураПредприятия");

	Для Каждого ЭлементСписка Из СписокТиповЗначений Цикл
		Если ВыбранныйТипЗначения.СодержитТип(Тип(ЭлементСписка.Значение)) Тогда
			ТипЗначения = ЭлементСписка.Значение;
			Прервать;
		КонецЕсли;
	КонецЦикла;
		
КонецПроцедуры // УстановитьТипЗначения()

// В процедуре выполняется установка свойств элементов формы.
//
&НаСервере
Процедура УправлениеЭлементамиФормы()

	Элементы.ТипЗначенияНаРасходыБудущихПериодов.ТолькоПросмотр = Ложь;

КонецПроцедуры // УправлениеЭлементамиФормы()

///////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

// Процедура - обработчик события "ПриСозданииНаСервере" формы.
//
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		УстановитьТекущуюСтраницу();
		УправлениеЭлементамиФормы();
	КонецЕсли;
	
	ЗаполнитьСписокВыбораКорСчета();
	
	// СтандартныеПодсистемы.ЗапретРедактированияРеквизитовОбъектов
	ЗапретРедактированияРеквизитовОбъектов.ЗаблокироватьРеквизиты(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ЗапретРедактированияРеквизитовОбъектов
	
КонецПроцедуры // ПриСозданииНаСервере()

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	// СтандартныеПодсистемы.ЗапретРедактированияРеквизитовОбъектов
	ЗапретРедактированияРеквизитовОбъектов.ЗаблокироватьРеквизиты(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ЗапретРедактированияРеквизитовОбъектов
	
КонецПроцедуры

// Процедура - обработчик события "ПриЧтенииНаСервере" формы.
//
&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	УстановитьТипЗначения(Объект.ТипЗначения);
	УстановитьТекущуюСтраницу();
	УправлениеЭлементамиФормы();
	
КонецПроцедуры // ПриЧтенииНаСервере()

// Процедура - обработчик события "ПередЗаписьюНаСервере" формы.
//
&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект)
	
	Если Не ПустаяСтрока(ТипЗначения) Тогда
		ТекущийОбъект.ТипЗначения = Новый ОписаниеТипов(ТипЗначения);
	КонецЕсли;
	
КонецПроцедуры // ПередЗаписьюНаСервере()

// Процедура - обработчик события "ОбработкаПроверкиЗаполненияНаСервере" формы.
//
&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	Если ПустаяСтрока(ТипЗначения) Тогда
		ТекстСообщения_ = СообщенияПользователю.Получить("Общие_НичегоНеВыбраноВПолеАналитикаРасходов");
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
			ТекстСообщения_,
			, // ОбъектИлиСсылка
			"ТипЗначения",
			, // ПутьКДанным
			Отказ
		);
	КонецЕсли;
	
КонецПроцедуры // ОбработкаПроверкиЗаполненияНаСервере()

///////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ ПОЛЕЙ ФОРМЫ


// Процедура - обработчик события "ПриИзменении" поля "СтатьяРасходов" формы.
//
&НаСервере
Процедура СтатьяРасходовПриИзмененииСервер()
	
	Если ЗначениеЗаполнено(Объект.СтатьяРасходов) Тогда
		ТипЗначенияСтатьиРасходов = ПланыВидовХарактеристик.СтатьиРасходов.ПолучитьРеквизитыСтатьиРасходов(Объект.СтатьяРасходов).ТипЗначения;
		УстановитьТипЗначения(ТипЗначенияСтатьиРасходов);
	КонецЕсли;
	УправлениеЭлементамиФормы();
	
КонецПроцедуры // СтатьяРасходовПриИзменении()

// Процедура - обработчик события "ПриИзменении" поля "СтатьяРасходов" формы.
//
&НаКлиенте
Процедура СтатьяРасходовПриИзменении(Элемент)
	
	СтатьяРасходовПриИзмененииСервер();
	
КонецПроцедуры // СтатьяРасходовПриИзменении()

&НаСервере
Процедура ЗаполнитьСписокВыбораКорСчета()

	СписокВыбора = Элементы.КорреспондирующийСчет.СписокВыбора;
	СписокВыбора.Очистить();
	СписокВыбора.Добавить("25", "Общепроизводственные расходы (25)");
	СписокВыбора.Добавить("26", "Общехозяйственные расходы (26)");
	СписокВыбора.Добавить("44.01", "Издержки обращения (44.01)");
	СписокВыбора.Добавить("44.02", "Коммерческие расходы (44.02)");
	СписокВыбора.Добавить("91.02", "Прочие расходы (91.02)");
	
КонецПроцедуры

// Обработчик команды, создаваемой подсистемой "Запрет редактирования реквизитов объектов".
//
&НаКлиенте
Процедура Подключаемый_РазрешитьРедактированиеРеквизитовОбъекта(Команда)
	
	ОбщиеМеханизмыКлиент.РазрешитьРедактированиеРеквизитовОбъекта(ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти