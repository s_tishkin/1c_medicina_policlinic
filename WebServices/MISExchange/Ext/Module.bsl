﻿
#Область ПрограммныйИнтерфейс


&НаСервере
Функция GetDepartmentList(ИдентификаторОрганизации = "")
	Организация = Неопределено;
	Если ЗначениеЗаполнено(ИдентификаторОрганизации) Тогда
		Организация = Справочники.Организации.ПолучитьСсылку(Новый УникальныйИдентификатор(ИдентификаторОрганизации));
	КонецЕсли;

	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	Отделения.Ссылка КАК Отделение,
	|	Отделения.Наименование КАК Наименование,
	|	Отделения.Владелец КАК Владелец
	|ИЗ
	|	Справочник.СтруктураПредприятия КАК Отделения
	|ГДЕ
	|	НЕ Отделения.ПометкаУдаления";
	
	Если ЗначениеЗаполнено(Организация) Тогда
		Запрос.Текст = Запрос.Текст + "
		|	И Отделения.Владелец = &Организация";
		Запрос.УстановитьПараметр("Организация", Организация);
	КонецЕсли;
	
	ТипСписокОтделений = ФабрикаXDTO.Тип("MISExchange", "DepartmentList");
	ТипОтделение = ТипСписокОтделений.Свойства.Получить("Department").Тип;
	СписокОтделений = ФабрикаXDTO.Создать(ТипСписокОтделений);
	Отделения = СписокОтделений.ПолучитьСписок("Department");
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		ОтделениеXDTO = ФабрикаXDTO.Создать(ТипОтделение);
		ОтделениеXDTO.Ref = Строка(Выборка.Отделение.УникальныйИдентификатор());
		ОтделениеXDTO.Description = Выборка.Наименование;
		ОтделениеXDTO.Owner = Строка(Выборка.Владелец.УникальныйИдентификатор());
		Отделения.Добавить(ОтделениеXDTO);
	КонецЦикла;
КонецФункции

&НаСервере
Функция GetOrganizationList()
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	Организации.Ссылка КАК Организация,
	|	Организации.Наименование КАК Наименование,
	|	Организации.НаименованиеПолное КАК НаименованиеПолное,
	|	Организации.НаименованиеСокращенное КАК НаименованиеСокращенное,
	|	Организации.ИНН КАК ИНН,
	|	Организации.КПП КАК КПП
	|ИЗ
	|	Справочник.Организации КАК Организации
	|ГДЕ
	|	(НЕ Организации.ПометкаУдаления)";
	
	ТипСписокОрганизаций = ФабрикаXDTO.Тип("MISExchange", "OrganizationList");
	ТипОрганизация = ТипСписокОрганизаций.Свойства.Получить("Organization").Тип;
	СписокОрганизаций = ФабрикаXDTO.Создать(ТипСписокОрганизаций);
	Организации = СписокОрганизаций.ПолучитьСписок("Organization");
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		ОрганизацияXDTO = ФабрикаXDTO.Создать(ТипОрганизация);
		ОрганизацияXDTO.Ref = Строка(Выборка.Организация.УникальныйИдентификатор());
		ОрганизацияXDTO.Description = Выборка.Наименование;
		Если ЗначениеЗаполнено(Выборка.НаименованиеПолное) Тогда
			ОрганизацияXDTO.NameFull = Выборка.НаименованиеПолное;
		КонецЕсли;

		Если ЗначениеЗаполнено(Выборка.НаименованиеСокращенное) Тогда
			ОрганизацияXDTO.NameShort = Выборка.НаименованиеСокращенное;
		КонецЕсли;

		Если ЗначениеЗаполнено(Выборка.ИНН) Тогда
			ОрганизацияXDTO.INN = Выборка.ИНН;
		КонецЕсли;

		Если ЗначениеЗаполнено(Выборка.КПП) Тогда
			ОрганизацияXDTO.KPP = Выборка.КПП;
		КонецЕсли;
		
		Организации.Добавить(ОрганизацияXDTO);
	КонецЦикла;
	
	Возврат СписокОрганизаций;
КонецФункции


#КонецОбласти