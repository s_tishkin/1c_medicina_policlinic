﻿
&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	Если ПараметрКоманды = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	ПараметрыОткрытия_ = Новый Структура(
		"ЗначенияЗаполнения",
		Новый Структура(
			"Пациент,",
			ПараметрКоманды
		)
	);
	
	ОткрытьФорму("Документ.ОбъединениеПациентов.ФормаОбъекта",ПараметрыОткрытия_);
	
КонецПроцедуры
