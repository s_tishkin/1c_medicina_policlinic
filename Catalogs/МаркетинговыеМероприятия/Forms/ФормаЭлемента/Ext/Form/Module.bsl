﻿
#Область ПрограммныйИнтерфейс

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// СтандартныеПодсистемы.ВерсионированиеОбъектов
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ И ФУНКЦИИ ОБЩЕГО НАЗНАЧЕНИЯ

&НаСервереБезКонтекста
Функция ПартнерКонтактногоЛица(КонтактноеЛицо)

	Возврат КонтактноеЛицо.Владелец;

КонецФункции

&НаКлиенте
// Создать взаимодействие по выбранному контакту
Процедура СоздатьВзаимодействие(ИмяФормы)
	
	Основание = Новый Структура();
	Основание.Вставить("Предмет", Объект.Ссылка);
	Основание.Вставить("Контакт",
						?(ЗначениеЗаполнено(Элементы.ПартнерыИКонтактныеЛица.ТекущиеДанные.КонтактноеЛицо),
						Элементы.ПартнерыИКонтактныеЛица.ТекущиеДанные.КонтактноеЛицо,
						Элементы.ПартнерыИКонтактныеЛица.ТекущиеДанные.Партнер));
	ОткрытьФорму(ИмяФормы,
				Новый Структура("Основание", Основание),
				ЭтотОбъект,,,,,
				РежимОткрытияОкнаФормы.БлокироватьОкноВладельца
			);
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ КОМАНД ФОРМЫ

&НаКлиенте
Процедура ПозвонитьВыполнить()

	СоздатьВзаимодействие("Документ.ТелефонныйЗвонок.ФормаОбъекта");

КонецПроцедуры

&НаКлиенте
Процедура НаписатьПисьмоВыполнить()

	СоздатьВзаимодействие("Документ.ЭлектронноеПисьмоИсходящее.ФормаОбъекта");

КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ УПРАВЛЕНИЯ ФОРМЫ

&НаКлиенте
Процедура КонтактноеЛицоПриИзменении(Элемент)

	ТекущиеДанные = Элементы.ПартнерыИКонтактныеЛица.ТекущиеДанные;
	Если НЕ ЗначениеЗаполнено(ТекущиеДанные.Партнер) Тогда
		ТекущиеДанные.Партнер = ПартнерКонтактногоЛица(ТекущиеДанные.КонтактноеЛицо);
	КонецЕсли;

КонецПроцедуры

&НаКлиенте
Процедура КомментарииНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ОбщегоНазначенияКлиент.ПоказатьФормуРедактированияКомментария(Элемент.ТекстРедактирования, ЭтотОбъект, "Объект.Комментарий");
	
КонецПроцедуры

#КонецОбласти