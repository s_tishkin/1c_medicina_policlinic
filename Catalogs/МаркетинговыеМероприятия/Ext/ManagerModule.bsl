﻿
#Область ПрограммныйИнтерфейс

// Возвращает контакты участников мероприятия.
//
// Возвращаемое значение:
// 		Тип(Массив) - массив контактов.
Функция ПолучитьКонтакты(Ссылка) Экспорт
	
	Возврат СделкиСервер.ПолучитьУчастниковПоТабличнойЧастиПредметаВзаимодействия(Ссылка);
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// СтандартныеПодсистемы.ВерсионированиеОбъектов

// Определяет настройки объекта для подсистемы ВерсионированиеОбъектов.
//
// Параметры:
//  Настройки - Структура - настройки подсистемы.
Процедура ПриОпределенииНастроекВерсионированияОбъектов(Настройки) Экспорт

КонецПроцедуры

// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов

#КонецОбласти