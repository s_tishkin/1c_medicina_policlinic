﻿
#Область ПрограммныйИнтерфейс


&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	Параметры_ = Новый Структура;
	Параметры_.Вставить("КлючНазначенияИспользования", "ДляЗаказов");
	Параметры_.Вставить("ТекМедКарта", ВыбратьЗаполненное(Параметры.Пациент, Справочники.МедицинскиеКарты.ПустаяСсылка()));
	Параметры_.Вставить("РежимВыбораСтрок", "Одиночный");
	Параметры_.Вставить("Избранное", Ложь);
	Параметры_.Вставить("Префикс", "СеткаВыборНоменклатуры");
	Параметры_.Вставить("ФильтрПоРабочимМестам", Параметры.РабочееМесто);
	Параметры_.Вставить("ДатаСрезаСостояния", Параметры.Дата);
	
	ДинПолеПодборНоменклатурыУстановитьПараметрыИнициализации(Параметры_);
ДинПолеПодборНоменклатурыПриСозданииНаСервере(Отказ, СтандартнаяОбработка);
КонецПроцедуры

////
 //	Процедура: ПодборНоменклатуры_КлиентскийОбработчик
 //     Клиенский обработчик динамического поля подбора номенклатуры.
 ///
&НаКлиенте
Процедура ДинамическоеПолеФормы_КлиентскийОбработчик(
					Элемент, 
					Арг1 = Неопределено, 
					Арг2 = Неопределено, 
					Арг3 = Неопределено, 
					Арг4 = Неопределено)
					
       ДинамическоеПолеФормы.ОбработатьСобытие(
              ЭтотОбъект, Элемент, "ДинамическоеПолеФормы_КлиентскийОбработчик", Арг1, Арг2, Арг3, Арг4
       );
КонецПроцедуры

////
 //	Процедура: ПодборНоменклатуры_СерверныйОбработчик
 //     Серверный обработчик динамического поля подбора номенклатуры.
 ///
//&НаСервере
//Процедура ДинамическоеПолеФормы_СерверныйОбработчик(Элемент)
//      ДинамическоеПолеФормы.ОбработатьСобытие(ЭтотОбъект, Элемент);
// КонецПроцедуры

////
 //	Процедура: ДинамическоеПолеФормы_ОбработкаВыбора
 //     Обработчик выбора услуги в динамическом поле подбора номенклатуры.
 //     Добавляет услугу в таблицу заказанных услуг.
 //
 //	Параметры:
 //   Элемент {ЭлементФормы}
 //     Элемент формы, который инициировал событие.
 //   ВыбраннаяСтрока {СправочникСсылка.Номенклатура}
 //     Значение выбранной строки.
 //   Поле {ПолеФормы}
 //     Активное поле.
 //   СтандартнаяОбработка {Булево}
 //     Признак выполнения стандартной обработки.
 //   ПараметрыДляЗаказа {Структура}
 //     Структура, которая содержит значения: ИсточникФинансирования, Договор, Цена,
 //     ИсполнениеУслуги, СостояниеУслуги.
 ///
&НаКлиенте
Процедура ДинамическоеПолеФормы_ОбработкаВыбора(Результат, ДополнительныеПараметры) Экспорт
	МассивПараметровЗаказа = Неопределено;
	ДополнительныеПараметры.Свойство("МассивПараметровЗаказа", МассивПараметровЗаказа);
	__ПРОВЕРКА__(
		МассивПараметровЗаказа.Количество() = 1, "Должна быть выбрана одну услуга"
	);
	ЭтотОбъект.Закрыть(МассивПараметровЗаказа[0]);
КонецПроцедуры

// Событие динамического поля для снятия выделения номенклатуры в подборе.
//
&НаКлиенте 
Процедура ОбработкаСнятияВыделенияПодбораНоменклатуры(ДинПоле) Экспорт
	ЭтотОбъект.ПодключитьОбработчикОжидания("СнятьВыделениеПодбораНоменклатуры", 0.1, Истина);	
КонецПроцедуры

#КонецОбласти

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	// Вставить содержимое обработчика.
ДинПолеПодборНоменклатурыОбработкаОповещения(ИмяСобытия, Параметр, Источник);
КонецПроцедуры
#Область ДинПолеПодборНоменклатуры
&НаСервере
Процедура ДинПолеПодборНоменклатурыУстановитьПараметрыИнициализации(ПараметрыИн)
	ДинамическоеПолеФормы.УстановитьРеквизит(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), "ПараметрыИнициализации", ПараметрыИн);
КонецПроцедуры

&НаСервере
Процедура ДинПолеПодборНоменклатурыПриСозданииНаСервере(Отказ, СтандартнаяОбработка) Экспорт
	ДинПоле = ДинПолеПодборНоменклатурыПолучитьДинПоле();
	ПараметрыПоля = ДинамическоеПолеФормы.ПолучитьРеквизит(ЭтотОбъект, ДинПоле, "ПараметрыИнициализации");
	Если ПараметрыПоля = Неопределено Тогда
		ДинПолеПодборНоменклатурыУстановитьПараметрыИнициализации(Параметры);	
	КонецЕсли;
	ПараметрыПоля = ДинамическоеПолеФормы.ПолучитьРеквизит(ЭтотОбъект, ДинПоле, "ПараметрыИнициализации");
	ДинПолеПодборНоменклатуры.ПриСозданииНаСервере(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), ПараметрыПоля);
КонецПроцедуры

&НаКлиенте
Процедура ДинПолеПодборНоменклатурыОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	Если ИмяСобытия = "СнятьВыделениеПодбораНоменклатуры" Тогда
		ДинПолеПодборНоменклатуры.СнятьВыделениеПодбораНоменклатуры(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле());
	КонецЕсли;
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция ДинПолеПодборНоменклатурыПолучитьДинПоле()
	Возврат "ДинПолеПодборНоменклатуры";
КонецФункции

&НаКлиенте
Процедура ДинПолеПодборНоменклатурыОбработчикПриИзменении(Элемент)
	Если ДинПолеПодборНоменклатуры.ОбработчикПриИзменении(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), Элемент) <> Истина Тогда
		ДинПолеПодборНоменклатурыОбработчикПриИзмененииНаСервере(Элемент.Имя);
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ДинПолеПодборНоменклатурыОбработчикПриИзмененииНаСервере(ИмяЭлемента)
	ДинПолеПодборНоменклатуры.ОбработчикПриИзмененииНаСервере(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), ИмяЭлемента);
КонецПроцедуры

&НаКлиенте
Процедура ДинПолеПодборНоменклатурыОбработчикНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	ДинПолеПодборНоменклатуры.ОбработчикНачалоВыбора(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), Элемент, ДанныеВыбора, СтандартнаяОбработка);
КонецПроцедуры

&НаКлиенте
Процедура ДинПолеПодборНоменклатурыОбработчикОткрытие(Элемент, СтандартнаяОбработка)
	Если ДинПолеПодборНоменклатуры.ОбработчикОткрытие(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), Элемент, СтандартнаяОбработка) <> Истина Тогда
		ДинПолеПодборНоменклатурыОбработчикОткрытиеНаСервере(Элемент.Имя, СтандартнаяОбработка);
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ДинПолеПодборНоменклатурыОбработчикОткрытиеНаСервере(ИмяЭлемента, СтандартнаяОбработка)
	ДинПолеПодборНоменклатуры.ОбработчикОткрытиеНаСервере(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), ИмяЭлемента, СтандартнаяОбработка);
КонецПроцедуры

&НаКлиенте
Процедура ДинПолеПодборНоменклатурыОбработчикАвтоПодбор(Элемент, Текст, ДанныеВыбора, ПараметрыПолученияДанных, Ожидание, СтандартнаяОбработка)
	Если ДинПолеПодборНоменклатуры.ОбработчикАвтоПодбор(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), Элемент, Текст, ДанныеВыбора, ПараметрыПолученияДанных, Ожидание, СтандартнаяОбработка) <> Истина Тогда
		ДинПолеПодборНоменклатурыОбработчикАвтоПодборНаСервере(Элемент.Имя, Текст, ДанныеВыбора, Ожидание, СтандартнаяОбработка);
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ДинПолеПодборНоменклатурыОбработчикАвтоПодборНаСервере(ИмяЭлемента, Текст, ДанныеВыбора, Ожидание, СтандартнаяОбработка)
	ДинПолеПодборНоменклатуры.ОбработчикАвтоПодборНаСервере(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), ИмяЭлемента, Текст, ДанныеВыбора, Ожидание, СтандартнаяОбработка);
КонецПроцедуры

&НаКлиенте
Процедура ДинПолеПодборНоменклатурыОбработчикСписокВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	ДинПолеПодборНоменклатуры.ОбработчикСписокВыбор(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка);
КонецПроцедуры

&НаКлиенте
Процедура ДинПолеПодборНоменклатурыОбработчикСписокПриАктивизацииСтроки(Элемент)
	ДинПолеПодборНоменклатуры.ОбработчикСписокПриАктивизацииСтроки(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ДинПолеПодборНоменклатурыОбработчикКоманд(Команда)
	Если ДинПолеПодборНоменклатуры.ОбработчикКоманд(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), Команда) <> Истина Тогда
		ДинПолеПодборНоменклатурыОбработчикКомандНаСервере(Команда.Имя);
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ДинПолеПодборНоменклатурыОбработчикКомандНаСервере(ИмяКоманды)
	ДинПолеПодборНоменклатуры.ОбработчикКомандНаСервере(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), ИмяКоманды);
КонецПроцедуры

&НаСервере
Процедура ДинПолеПодборНоменклатурыОбработатьСобытиеНаСервере(ИмяСобытия, Параметр1 = Неопределено, Параметр2 = Неопределено, Параметр3 = Неопределено, Параметр4 = Неопределено, Параметр5 = Неопределено, Параметр6 = Неопределено, Параметр7 = Неопределено)
	ДинПолеПодборНоменклатуры.ОбработчикСобытияНаСервере(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), ИмяСобытия, Параметр1, Параметр2, Параметр3, Параметр4, Параметр5, Параметр6, Параметр7);
КонецПроцедуры

&НаКлиенте
Процедура ДинПолеПодборНоменклатурыОбработатьОповещение(Результат, ДополнительныеПараметры) Экспорт
	ДинПолеПодборНоменклатурыОбработатьСобытиеНаСервере(ДополнительныеПараметры.ИмяСобытия);
КонецПроцедуры



#КонецОбласти



