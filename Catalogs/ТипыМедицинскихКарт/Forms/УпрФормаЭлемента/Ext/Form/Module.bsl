﻿
#Область ПрограммныйИнтерфейс

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	ЭтотОбъект.УсловияМедПомощиАмбулаторноПоликлиническая = Перечисления.УсловияМедицинскойПомощи.АмбулаторноПоликлиническая;
	ЭтотОбъект.УсловияМедПомощиСтационарная = Перечисления.УсловияМедицинскойПомощи.Стационарная;
	
	СтационарныеУсловия_ = ОбщиеМеханизмыПовтИсп.ИспользоватьСтационарныеУсловия();
	Если СтационарныеУсловия_ = Ложь Тогда
		Элементы.УсловияОказанияПомощи.Видимость = Ложь;
	КонецЕсли;
	УправлениеДоступностьюПоУсловиюОказанияПомощи(
						Объект, Элементы, 
						УсловияМедПомощиАмбулаторноПоликлиническая,
						УсловияМедПомощиСтационарная);
						
	СформироватьСписокОграниченияПоПолу();
	
	Элементы.ИспользоватьВредности.Видимость = ПолучитьФункциональнуюОпцию("ИспользоватьВредныеФакторы");
	
	// Подсистема Архивирование
	АрхивированиеСервер.ФормаЭлементаПриСозданииНаСервере(ЭтотОбъект, Объект[Архивирование.ИмяРеквизита()], "Архивирование_Статус");
	// конец Подсистема Архивирование
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	// Подсистема Архивирование
	Архивирование.УстановитьВидимостьЭлементовАрхивности(ЭтотОбъект, Объект[Архивирование.ИмяРеквизита()]);
	// конец Подсистема Архивирование
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	// Подсистема Архивирование
	Если ИмяСобытия = Архивирование.ИмяОповещения() Тогда
		Если ТипЗнч(Параметр) = Тип("Массив") Тогда
			Поиск_ = Параметр.Найти(Объект.Ссылка);
			Если Поиск_ <> Неопределено Тогда
				Архивирование.УстановитьВидимостьЭлементовАрхивности(ЭтотОбъект, Объект[Архивирование.ИмяРеквизита()]);
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;
	// конец Подсистема Архивирование
	
КонецПроцедуры

////
 // Процедура: СформироватьСписокОграниченияПоПолу
 //   Предназначена для формирования списка выбора для ЭУ ОграниченияПоПолу.
 ///
&НаСервере
Процедура СформироватьСписокОграниченияПоПолу()
	ПолНеопределен_ = Перечисления.ПолПациентов.Неопределен;
	СписокВыбора_ = ЭтотОбъект.Элементы.ОграниченияПоПолу.СписокВыбора;
	Если ЭтотОбъект.Объект.НеИспользоватьПолНеопр Тогда
		ЭлементСпискаВыбора = СписокВыбора_.НайтиПоЗначению(ПолНеопределен_);
		СписокВыбора_.Удалить(ЭлементСпискаВыбора);
		Если Объект.ОграниченияПоПолу = ПолНеопределен_ Тогда
			Объект.ОграниченияПоПолу = Неопределено;
		КонецЕсли;
	ИначеЕсли СписокВыбора_.НайтиПоЗначению(ПолНеопределен_) = Неопределено Тогда
		СписокВыбора_.Добавить(ПолНеопределен_);
	КонецЕсли;
КонецПроцедуры

////
 // Процедура: НеИспользоватьПолНеопрПриИзменении
 //   Стандартный обработчик события ПриИзменении ЭУ НеИспользоватьПол.
 //   В обработчике происходит переформирование списка выбора ЭУ ОграничениеПоПолу.
 //
 // Параметры:
 //   Элемент
 //     Ссылка на элемент формы 
 ///
&НаКлиенте
Процедура НеИспользоватьПолНеопрПриИзменении(Элемент)
	// Формируем новый список
	СформироватьСписокОграниченияПоПолу();
КонецПроцедуры

////
 // Процедура: ОграниченияПоПолуАвтоПодбор
 //   Стандартный обработчик события АвтоПодбор ЭУ ОграниченияПоПолу.
 //   В обработчике происходит отмена выполнения стандартных действий.
 //
 // Параметры:
 //   Элемент
 //     Ссылка на элемент формы
 //   Текст
 //     Текст, введенный в поле ввода 
 //   ДанныеВыбора
 //     Список значений, который будет использоваться при стандартной обработке
 //   Ожидание.
 //     Интервал времени после ввода текста, через который произошло событие
 //   СтандартнаяОбработка.
 //     Признак выполнения стандартной обработки
 ///
&НаКлиенте
Процедура ОграниченияПоПолуАвтоПодбор(Элемент, Текст, ДанныеВыбора, Ожидание, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
КонецПроцедуры

////
 // Процедура: УсловияОказанияПомощиПриИзменении
 //   Стандартный обработчик события ПриИзменении ЭУ УсловияОказанияПомощи.
 //   В обработчике происходит установка видимости элементов на форме в зависимости от выб значения.
 //
 // Параметры:
 //   Элемент
 //     Ссылка на элемент формы 
 ///
&НаКлиенте
Процедура УсловияОказанияПомощиПриИзменении(Элемент)
	// Для амбулаторно поликлинических условий доступно изменение типа участка
	// для остальных условий определяется отдельно.
	УправлениеДоступностьюПоУсловиюОказанияПомощи(
						Объект, Элементы, 
						УсловияМедПомощиАмбулаторноПоликлиническая,
						УсловияМедПомощиСтационарная);
КонецПроцедуры

////
 // Процедура: ОграниченияПоПолуОбработкаВыбора
 //   Стандартный обработчик события ОбработкаВыбора ЭУ ОграниченияПоПолу.
 //   В обработчике происходит очистка выбранного значения при выполнении условия.
 //
 // Параметры:
 //   Элемент
 //     Ссылка на элемент формы
 //   ВыбранноеЗначение
 //     Выбранное значение, которое будет установлено как значение поля ввода 
 //   СтандартнаяОбработка.
 //     Признак выполнения стандартной обработки
 ///
&НаКлиенте
Процедура ОграниченияПоПолуОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	Если ЭтотОбъект.Объект.НеИспользоватьПолНеопр И 
		ВыбранноеЗначение = ПредопределенноеЗначение("Перечисление.ПолПациентов.НеОпределен") 
	Тогда
		ВыбранноеЗначение = "";
	КонецЕсли;
	
КонецПроцедуры

////
 // Процедура: ПередЗаписью
 //   Стандартный обработчик события формы ПередЗаписью.
 //   Если установлен флажок и не заполнена таблица на вкладке "Ввод на основании", 
 //   то запись элемента отменяется.
 //
 // Параметры:
 //   Отказ
 //     Признак отказа от записи
 //   ПараметрыЗаписи
 //     Структура, содержащая параметры записи 
 ///
&НаКлиенте
Процедура ПередЗаписью(Отказ, ПараметрыЗаписи)
	Если ЭтотОбъект.Объект.ВводТолькоНаОсновании И 
		 ЭтотОбъект.Объект.ТипыКартОснования.Количество() = 0 
	Тогда
		СообщенияПользователю.Показать("МедицинскиеКарты_НеЗаданыТипыКартОснования");
		Отказ = Истина;
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ПриЗаписиНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	Если ТекущийОбъект.ДополнительныеСвойства.ЭтоНовый Тогда
		НаборЗаписей = РегистрыСведений.ШаблоныНомеров.СоздатьНаборЗаписей();
		НаборЗаписей.Отбор.КлючШаблонаНомера.Установить(ТекущийОбъект.Ссылка);
		НоваяЗапись = НаборЗаписей.Добавить();
		НоваяЗапись.Период = ПолучитьПериодШаблонаПоУмолчанию();
		НоваяЗапись.КлючШаблонаНомера = ТекущийОбъект.Ссылка;
		НоваяЗапись.ШаблонНомера
			= Справочники.ШаблоныНомеров.ШаблонПоУмолчанию()
		;
		НаборЗаписей.Записать();
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	Если НЕ ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ТекущийОбъект.ДополнительныеСвойства.Вставить("ЭтоНовый",Истина);
	Иначе
		ТекущийОбъект.ДополнительныеСвойства.Вставить("ЭтоНовый",Ложь);
	КонецЕсли;
КонецПроцедуры

Функция ПолучитьПериодШаблонаПоУмолчанию()
	Возврат НастройкиКлиентСервер.Получить("НачальныйПериодНумерацииМедицинскихКарт");
КонецФункции

&НаКлиентеНаСервереБезКонтекста
Процедура УправлениеДоступностьюПоУсловиюОказанияПомощи(
						Объект, Элементы, 
						УсловияМедПомощиАмбулаторноПоликлиническая,
						УсловияМедПомощиСтационарная)

	Если Объект.УсловияОказанияПомощи = УсловияМедПомощиАмбулаторноПоликлиническая Тогда
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы,"ОтказнойТип", "Видимость", Ложь);
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы,"ТипУчастка", "Видимость", Истина);
	ИначеЕсли Объект.УсловияОказанияПомощи = УсловияМедПомощиСтационарная Тогда
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы,"ОтказнойТип", "Видимость", Истина);
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы,"ТипУчастка", "Видимость", Ложь);
	Иначе
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы,"ОтказнойТип", "Видимость", Ложь);
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы,"ТипУчастка", "Видимость", Истина);
	КонецЕсли;
КонецПроцедуры


#КонецОбласти