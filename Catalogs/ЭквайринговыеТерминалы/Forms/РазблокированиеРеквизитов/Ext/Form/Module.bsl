﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;
	
	РазрешитьРедактированиеБанковскийСчет = Истина;
	РазрешитьРедактированиеЭквайер = Истина;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура РазрешитьРедактирование(Команда)
	
	Результат = Новый Массив;
	Если РазрешитьРедактированиеБанковскийСчет Тогда
		Результат.Добавить("БанковскийСчет");
	КонецЕсли;
	Если РазрешитьРедактированиеЭквайер Тогда
		Результат.Добавить("Эквайер");
	КонецЕсли;
	
	Закрыть(Результат);
	
КонецПроцедуры

#КонецОбласти
