﻿
&НаКлиенте
Перем КэшированныеЗначения;

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	УстановитьУсловноеОформление();
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	ОбновлениеИнформационнойБазы.ПроверитьОбъектОбработан(Объект, ЭтотОбъект);

	Если ЗначениеЗаполнено(Объект.Ссылка) Тогда
		
		ТаблицаКомандыФормы = Объект.Ссылка.КомандыФормы.Получить();
		Если ТаблицаКомандыФормы <> Неопределено Тогда
			КомандыФормы.Загрузить(ТаблицаКомандыФормы);
		КонецЕсли;
		
		КоличествоКомандФормы = КомандыФормы.Количество();
		
	КонецЕсли;
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтотОбъект, Элементы.ПодменюПечать);
	// Конец СтандартныеПодсистемы.Печать

КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ЗаполнитьПоУмолчаниюНаКлиенте();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	Для Каждого СтрокаТЧ Из КомандыФормы Цикл
		Если Не ЗначениеЗаполнено(СтрокаТЧ.Клавиша) Тогда
			СтрокаТЧ.Клавиша = Строка(Клавиша.Нет);
		КонецЕсли;
	КонецЦикла;
	
	ТекущийОбъект.КомандыФормы = Новый ХранилищеЗначения(КомандыФормы.Выгрузить());

КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия = "ЧтениеКомандФормы"
		И Параметр.Форма = УникальныйИдентификатор Тогда
		
		ЗаполнитьПоУмолчаниюНаСервере(Параметр.АдресВоВременномХранилище);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыКоманды

&НаКлиенте
Процедура КомандыФормыВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	Если Элементы.КомандыФормыСочетаниеКлавиш = Поле Тогда
		
		СтандартнаяОбработка = Ложь;
		
		ВозвращаемоеЗначение = Неопределено;

		
		ОткрытьФорму("Справочник.ГорячиеКлавиши.Форма.ФормаВыборСочетанияКлавиш", Новый Структура("Адрес", АдресВоВременномХранилище()),,,,, Новый ОписаниеОповещения("КомандыФормыВыборЗавершение", ЭтотОбъект, Новый Структура("ВыбраннаяСтрока", ВыбраннаяСтрока)), РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура КомандыФормыВыборЗавершение(Результат, ДополнительныеПараметры) Экспорт
    
    ВыбраннаяСтрока = ДополнительныеПараметры.ВыбраннаяСтрока;
    
    
    ВозвращаемоеЗначение = Результат;
    
    Если ВозвращаемоеЗначение <> Неопределено Тогда
        
        ОчиститьСочетаниеКлавиш(ВозвращаемоеЗначение);
        
        ТекущаяСтрока = КомандыФормы.НайтиПоИдентификатору(ВыбраннаяСтрока);
        ТекущаяСтрока.СочетаниеКлавиш = ПредставлениеСочетанияКлавиш(ВозвращаемоеЗначение);
        
        ЗаполнитьЗначенияСвойств(ТекущаяСтрока, ВозвращаемоеЗначение);
        Модифицированность = Истина;
        
    КонецЕсли;

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ЗаполнитьПоУмолчанию(Команда)
	
	ЗаполнитьПоУмолчаниюНаКлиенте();
	
КонецПроцедуры

&НаКлиенте
Процедура СкрытьВсеКоманды(Команда)
	
	Для Каждого СтрокаТЧ Из КомандыФормы Цикл
		СтрокаТЧ.Скрывать = Ложь;
	КонецЦикла;
	
КонецПроцедуры

&НаКлиенте
Процедура ПоказатьВсеКоманды(Команда)
	
	Для Каждого СтрокаТЧ Из КомандыФормы Цикл
		СтрокаТЧ.Скрывать = Истина;
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформление()

	УсловноеОформление.Элементы.Очистить();

КонецПроцедуры

#Область Прочее

&НаСервере
Функция НоваяКоманда(Таблица, ДанныеКоманды)
	
	НоваяСтрока = Таблица.Добавить();
	НоваяСтрока.Команда   = ДанныеКоманды.ИмяКоманды;
	НоваяСтрока.Заголовок = ДанныеКоманды.Заголовок;
	НоваяСтрока.Клавиша   = ДанныеКоманды.СочетаниеКлавиш.Клавиша;
	НоваяСтрока.Alt       = ДанныеКоманды.СочетаниеКлавиш.Alt;
	НоваяСтрока.Ctrl      = ДанныеКоманды.СочетаниеКлавиш.Ctrl;
	НоваяСтрока.Shift     = ДанныеКоманды.СочетаниеКлавиш.Shift;
	
	НоваяСтрока.СочетаниеКлавиш = ПредставлениеСочетанияКлавиш(ДанныеКоманды.СочетаниеКлавиш);
	
	Возврат НоваяСтрока;
	
КонецФункции

&НаКлиенте
Процедура ЗаполнитьПоУмолчаниюНаКлиенте()
	
	ФормаДокументаЧекККМ = ПолучитьФорму("Документ.ЧекККМ.Форма.ФормаДокументаРМК", Новый Структура("Основание, АвтоТест", Новый Структура("ЧтениеКомандФормы")));
	Оповестить("ЧтениеКомандФормы", Новый Структура("Форма, ФормаВладелец", ФормаДокументаЧекККМ.УникальныйИдентификатор, УникальныйИдентификатор));
	
КонецПроцедуры

&НаСервере
Функция ПредставлениеСочетанияКлавиш(Сочетание)
	
	Возврат ОбщиеМеханизмыКлиентСервер.ПредставлениеСочетанияКлавиш(Сочетание, Истина);
	
КонецФункции

&НаСервере
Процедура ЗаполнитьПоУмолчаниюНаСервере(АдресВоВременномХранилище)
	
	КомандыФормы.Очистить();
	
	ТаблицаКомандыФормы = ПолучитьИзВременногоХранилища(АдресВоВременномХранилище);
	Для Каждого КомандаФормы Из ТаблицаКомандыФормы Цикл
		НоваяСтрока = НоваяКоманда(КомандыФормы, КомандаФормы);
	КонецЦикла;
	
	КоличествоКомандФормы = КомандыФормы.Количество();
	
КонецПроцедуры

&НаСервере
Функция АдресВоВременномХранилище()
	
	Возврат ПоместитьВоВременноеХранилище(
		Новый Структура(
			"КомандыФормы", КомандыФормы.Выгрузить()
		),
		УникальныйИдентификатор);
	
КонецФункции

&НаКлиенте
Процедура ОчиститьСочетаниеКлавиш(СочетаниеКлавиш)
	
	ПредставлениеСочетанияКлавиш = ПредставлениеСочетанияКлавиш(СочетаниеКлавиш);
	
	Для Каждого СтрокаТЧ Из КомандыФормы Цикл
		Если СтрокаТЧ.СочетаниеКлавиш = ПредставлениеСочетанияКлавиш Тогда
			СтрокаТЧ.Клавиша = Строка(Клавиша.Нет);
			СтрокаТЧ.Ctrl = Ложь;
			СтрокаТЧ.Shift = Ложь;
			СтрокаТЧ.Alt = Ложь;
			СтрокаТЧ.СочетаниеКлавиш = "";
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтотОбъект, Объект);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

#КонецОбласти

#КонецОбласти
