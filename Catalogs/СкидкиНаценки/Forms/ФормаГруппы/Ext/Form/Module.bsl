﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	// Обработчик подсистемы запрета редактирования реквизитов объектов
	ЗапретРедактированияРеквизитовОбъектов.ЗаблокироватьРеквизиты(ЭтотОбъект);
	
	Элементы.Наименование.ОтметкаНезаполненного = Ложь;
	
	Если Объект.Ссылка = Справочники.СкидкиНаценки.КорневаяГруппа Тогда
		Элементы.Родитель.Видимость = Ложь;
	КонецЕсли;
	
	ПриСозданииЧтенииНаСервере();

КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	ПриСозданииЧтенииНаСервере();
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	// Обработчик подсистемы запрета редактирования реквизитов объектов
	ЗапретРедактированияРеквизитовОбъектов.ЗаблокироватьРеквизиты(ЭтотОбъект);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ВариантСовместногоПримененияСкидокНаценокПриИзменении(Элемент)
	
	ОбновитьЭлементыФормы(ЭтотОбъект, Описание);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Прочее

// Обработчик команды, создаваемой механизмом запрета редактирования ключевых реквизитов.
//
&НаКлиенте
Процедура Подключаемый_РазрешитьРедактированиеРеквизитовОбъекта(Команда) Экспорт
	
	ОбщиеМеханизмыКлиент.РазрешитьРедактированиеРеквизитовОбъекта(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ОбновитьЭлементыФормы(Форма, Описание)
	
	Если Форма.Объект.ВариантСовместногоПрименения = ПредопределенноеЗначение("Перечисление.ВариантыСовместногоПримененияСкидокНаценок.Максимум") Тогда
		Описание = НСтр("ru = 'Применяется одна наибольшая скидка внутри группы'");
		Форма.Элементы.ВариантРасчетаРезультатаСовместногоПрименения.Видимость = Истина;
	ИначеЕсли Форма.Объект.ВариантСовместногоПрименения = ПредопределенноеЗначение("Перечисление.ВариантыСовместногоПримененияСкидокНаценок.Минимум") Тогда
		Описание = НСтр("ru = 'Применяется одна наименьшая скидка внутри группы'");
		Форма.Элементы.ВариантРасчетаРезультатаСовместногоПрименения.Видимость = Истина;
	ИначеЕсли Форма.Объект.ВариантСовместногоПрименения = ПредопределенноеЗначение("Перечисление.ВариантыСовместногоПримененияСкидокНаценок.Вытеснение") Тогда
		Описание = НСтр("ru = 'При совместном действии скидок (наценок) в одной группе будет действовать только та скидка, которая имеет наивысший приоритет в группе'");
		Форма.Элементы.ВариантРасчетаРезультатаСовместногоПрименения.Видимость = Истина;
	ИначеЕсли Форма.Объект.ВариантСовместногоПрименения = ПредопределенноеЗначение("Перечисление.ВариантыСовместногоПримененияСкидокНаценок.Сложение") Тогда
		Описание = НСтр("ru = 'Применяется сумма всех скидок внутри группы'");
		Форма.Элементы.ВариантРасчетаРезультатаСовместногоПрименения.Видимость = Ложь;
	ИначеЕсли Форма.Объект.ВариантСовместногоПрименения = ПредопределенноеЗначение("Перечисление.ВариантыСовместногоПримененияСкидокНаценок.Умножение") Тогда
		Описание = НСтр("ru = 'Скидки применяются последовательно в порядке приоритета внутри группы'");
		Форма.Элементы.ВариантРасчетаРезультатаСовместногоПрименения.Видимость = Ложь;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПриСозданииЧтенииНаСервере()
	
	ОбновитьЭлементыФормы(ЭтотОбъект, Описание);
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти
