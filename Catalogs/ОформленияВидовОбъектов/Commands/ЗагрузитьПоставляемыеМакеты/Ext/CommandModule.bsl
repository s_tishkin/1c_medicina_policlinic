﻿
#Область ПрограммныйИнтерфейс

&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	ОповещениеОЗамещении = Новый ОписаниеОповещения("ОбработкаОтветаОЗамещении", ЭтотОбъект);
	Текст = СообщенияПользователю.Получить("ЗагрузкаВыгрузкаДанных_ПодтвердитьЗаменуДополнениеДанных");
	ПоказатьВопрос(ОповещениеОЗамещении, Текст, РежимДиалогаВопрос.ДаНет);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОтветаОЗамещении(Ответ, Параметры)
	
	Если Ответ = КодВозвратаДиалога.Да Тогда
		ТекстСостояния =  СообщенияПользователю.Получить("ЗагрузкаВыгрузкаДанных_НачалоЗагрузкиДанных",
															Новый Структура("ТипДанных","макета")
													);
		ПоказатьОповещениеПользователя(, , ТекстСостояния);
		
		ЗагрузитьПоставляемыеДанные();
		
		ОповеститьОбИзменении(Тип("СправочникСсылка.ОформленияВидовОбъектов"));
		Оповестить("ИзменениеОформленияВидовОбъектов");
		
		ТекстСостояния =  СообщенияПользователю.Получить("ЗагрузкаВыгрузкаДанных_ЗагрузкаДанныхЗавершена",
															Новый Структура("ТипДанных","макета")
													);
		ПоказатьОповещениеПользователя(, , ТекстСостояния);
	КонецЕсли;

КонецПроцедуры

&НаСервере
Процедура ЗагрузитьПоставляемыеДанные()
	
	РезультирующаяТаблица = ОформленияВидовОбъектовПовтИсп.ПолучитьТаблицуПоставляемыхЭлементов();
	
	Если РезультирующаяТаблица.Количество() > 0 Тогда
		// обновляем все
		Справочники.ОформленияВидовОбъектов.ЗаполнитьЗначенияСправочникаИзТаблицы(РезультирующаяТаблица, Истина);
	КонецЕсли;
	
КонецПроцедуры


#КонецОбласти