﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	// Подсистема Архивирование
	АрхивированиеСервер.ФормаСпискаПриСозданииНаСервере(ЭтотОбъект, "Список");
	// конец Подсистема Архивирование
КонецПроцедуры

#КонецОбласти

#Область Архивирование

// Подсистема Архивирование
&НаКлиенте
Процедура Подключаемый_ПросмотрАрхивныхЭлементов(Команда) Экспорт
	
	Архивирование.КомандаПросмотрАрхивныхЭлементов(ЭтотОбъект, "Список", Команда);
	
КонецПроцедуры
// конец Подсистема Архивирование

#КонецОбласти
