﻿
#Область ПрограммныйИнтерфейс

// Функция ПрочитатьДанныеКлассификатораИзXDTO получает данные из Объекта XDTO
// и формирует таблицу значений.
//
// Параметры:
//  ОбъектXDTO     - ОбъектXDTO.
//
// Возвращаемое значение:
//  ТаблицаЗначений.
//
Функция ПрочитатьДанныеКлассификатораИзXDTO(ОбъектXDTO) Экспорт
	
	Таблица_ = Новый ТаблицаЗначений;
	Таблица_.Колонки.Добавить("КодМинздрава", Новый ОписаниеТипов("Строка"));
	Таблица_.Колонки.Добавить("Наименование", Новый ОписаниеТипов("Строка"));
	Таблица_.Колонки.Добавить("Раздел", Новый ОписаниеТипов("СправочникСсылка.РазделыМедицинскихУслуг"));
	Таблица_.Колонки.Добавить("Подраздел", Новый ОписаниеТипов("СправочникСсылка.ПодразделыМедицинскихУслуг"));
	Таблица_.Колонки.Добавить("Класс", Новый ОписаниеТипов("ПеречислениеСсылка.КлассыМедицинскихУслуг"));

	Если Не ТипЗнч(ОбъектXDTO) = Тип("ОбъектXDTO") Тогда
		Возврат Таблица_;
	КонецЕсли; 
	
	СписокЗаписей_ = ОбъектXDTO.item;
	Если ТипЗнч(СписокЗаписей_) <> Тип("СписокXDTO") Тогда
		// Ошибка
		Возврат Таблица_;
	КонецЕсли; 
	
	Для каждого ЭлементВерхнегоУровня_ Из СписокЗаписей_ Цикл
		
		СписокПолей_ = ЭлементВерхнегоУровня_.children.item;
		
		НоваяСтрока_ = Таблица_.Добавить();
		
		Для Каждого Элемент_ Из СписокПолей_ Цикл
			Если ВРег(Элемент_.key) = "S_CODE" Тогда
				НоваяСтрока_.КодМинздрава = Элемент_.value;
			ИначеЕсли ВРег(Элемент_.key) = "NAME" Тогда
				НоваяСтрока_.Наименование = Элемент_.value;
			КонецЕсли;
		КонецЦикла;
		
		ДанныеКода_ = ПолучитьДанныеКода(НоваяСтрока_.КодМинздрава);
		Если ТипЗнч(ДанныеКода_) = Тип("Структура") Тогда
			ЗаполнитьЗначенияСвойств(НоваяСтрока_, ДанныеКода_, "Класс, Раздел, Подраздел");
		КонецЕсли; 
		
	КонецЦикла;
		
	Возврат Таблица_;
	
КонецФункции

// Функция позвращает параметры для запроса данных классификатора из веб-сервиса
//
// Возвращаемое значение:
//   Структура   - структура с параметрами.
//
Функция ПолучитьПараметрыЗапроса() Экспорт

	Параметры_ = Новый Структура;
	Параметры_.Вставить("OID", ПолучитьOIDКлассификатора());
	Параметры_.Вставить("ID", ПолучитьIDПользователя());
	
	Возврат Параметры_

КонецФункции

// Функция ПолучитьOIDКлассификатора возвращает OID классификатора
//
// Возвращаемое значение:
//   Строка
//
Функция ПолучитьOIDКлассификатора() Экспорт

	Возврат "1.2.643.5.1.13.13.11.1070";

КонецФункции

// Функция ПолучитьIDПользователя возвращает ID пользователя
//
// Возвращаемое значение:
//   Строка
//
Функция ПолучитьIDПользователя() Экспорт

	Возврат "93c515da98f1ecd4c75e7fd437fa2b2d";

КонецФункции

///
// Функция: ПолучитьДанныеКода
//    Возвращает данные кода номенклатуры медицинской услуги.
//
// Параметры:
//   Код
//
// Возврат: {Структура, Неопределено}   
///
Функция ПолучитьДанныеКода(Код)
	Части_ = СтроковыеФункцииКлиентСервер.РазложитьСтрокуВМассивПодстрок(Код, ".");
	Если Части_.Количество() < 2 Или Части_.Количество() > 5 Тогда
		Возврат Неопределено;	
	КонецЕсли;
	
	Класс_ = Лев(Части_[0], 1);
	Раздел_ = Сред(Части_[0], 2);
	Подраздел_ = Части_[1];
	
	Данные_ = Новый Структура;
	Данные_.Вставить("Класс", Перечисления.КлассыМедицинскихУслуг.НайтиПоНаименованию(Класс_));
	Данные_.Вставить("Раздел", Справочники.РазделыМедицинскихУслуг.НайтиПоКоду(Класс_ + Раздел_));
	Данные_.Вставить("Подраздел", Справочники.ПодразделыМедицинскихУслуг.НайтиПоКоду(Класс_ + Подраздел_));
	Возврат Данные_;
	
КонецФункции

#КонецОбласти