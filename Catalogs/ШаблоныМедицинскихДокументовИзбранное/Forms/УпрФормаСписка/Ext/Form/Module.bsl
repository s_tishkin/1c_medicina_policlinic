﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	ЭтотОбъект.СписокШМД.ТекстЗапроса = ДинамическоеПолеФормы.ПроверитьЗапрос(
		"ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ШаблоныМедицинскихДокументов.Ссылка КАК Ссылка
		|ИЗ
		|	РегистрСведений.ШаблоныМедицинскихДокументовДляМедицинскихУслуг КАК ШаблоныМедицинскихДокументовДляМедицинскихУслуг
		|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.МедицинскийДокумент.ВыполненныеУслуги КАК МедицинскийДокументВыполненныеУслуги
		|		ПО ШаблоныМедицинскихДокументовДляМедицинскихУслуг.Номенклатура = МедицинскийДокументВыполненныеУслуги.НоменклатураСоставаУслуги
		|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ШаблоныМедицинскихДокументов КАК ШаблоныМедицинскихДокументов
		|		ПО ШаблоныМедицинскихДокументовДляМедицинскихУслуг.ШаблонМедицинскогоДокумента = ШаблоныМедицинскихДокументов.Ссылка
		|			И ШаблоныМедицинскихДокументовДляМедицинскихУслуг.ШаблонМедицинскогоДокумента = ШаблоныМедицинскихДокументов.Ссылка
		|ГДЕ
		|	МедицинскийДокументВыполненныеУслуги.Ссылка = &СсылкаМедицинскогоДокумента
		|	И ШаблоныМедицинскихДокументов.Актуальность = ИСТИНА
		|	И ШаблоныМедицинскихДокументов.ПометкаУдаления = ЛОЖЬ
		|			
		|ОБЪЕДИНИТЬ
		|			
		|ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ШаблоныМедицинскихДокументов.Ссылка КАК Ссылка
		|ИЗ
		|	Справочник.ШаблоныМедицинскихДокументов КАК ШаблоныМедицинскихДокументов
		|ГДЕ
		|	ШаблоныМедицинскихДокументов.Ссылка В (&СписокШМД)
		|	И ШаблоныМедицинскихДокументов.Актуальность = ИСТИНА
		|	И ШаблоныМедицинскихДокументов.ПометкаУдаления = ЛОЖЬ"
	);
	ЭтотОбъект.СписокШМД.Параметры.УстановитьЗначениеПараметра("СсылкаМедицинскогоДокумента", ЭтотОбъект.Параметры.МедицинскийДокумент);
	ЭтотОбъект.СписокШМД.Параметры.УстановитьЗначениеПараметра("СписокШМД", ЭтотОбъект.Параметры.СписокШМД.ВыгрузитьЗначения());
	ЭтотОбъект.Пользователь = Пользователи.ТекущийПользователь();
	ЗаполнитьДеревоЭлементов();
КонецПроцедуры


#КонецОбласти

#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура СписокШМДВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	ДобавитьШМДвДерево(Элементы.СписокШМД.ТекущиеДанные.Ссылка, ЭтотОбъект.Пользователь);
КонецПроцедуры

&НаКлиенте
Процедура ДеревоТекущихЭлементовПередУдалением(Элемент, Отказ)
	ТекущиеДанные_ = Элементы.ДеревоТекущихЭлементов.ТекущиеДанные;
	Если ЗначениеЗаполнено(ТекущиеДанные_.Ссылка) Тогда
		СписокУдаляемых.Добавить(ТекущиеДанные_.Ссылка);
	Иначе
		Если ТекущиеДанные_.Наименование = "Избранные ШМД" Тогда
			Отказ = Истина;
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ДеревоТекущихЭлементовВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	ТекущиеДанные_ = Элементы.ДеревоТекущихЭлементов.ТекущиеДанные;
	Если ТекущиеДанные_ <> Неопределено Тогда
		Если ЗначениеЗаполнено(ТекущиеДанные_.Ссылка) Тогда
			ПоказатьЗначение(Новый ОписаниеОповещения(
						"ПослеЗакрытияФормыЭлемента", 
						ЭтотОбъект
					),
				ТекущиеДанные_.Ссылка
			);
		Иначе
			СообщенияПользователюКлиент.ВывестиПредупреждение("РаботаСИзбранным_ЭлементыНеЗаписаны");
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗакрытияФормыЭлемента(РезультатОткрытия = Неопределено, ДополнительныйПараметр = Неопределено) Экспорт
	ДеревоТекущихЭлементов.ПолучитьЭлементы().Очистить();
	ЗаполнитьДеревоЭлементов();
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомманд

&НаСервере
Процедура ЗаписатьНаСервере()
	ЭлементыДерева_ = ДеревоТекущихЭлементов.ПолучитьЭлементы();
	ЗаписатьНовыеЭлементыДерева(ЭлементыДерева_);
КонецПроцедуры

&НаКлиенте
Процедура Записать(Команда)
	ПометитьУдаляемыеЭлементы();
	ЗаписатьНаСервере();
	ДеревоТекущихЭлементов.ПолучитьЭлементы().Очистить();
	ЗаполнитьДеревоЭлементов();
КонецПроцедуры

#КонецОбласти

#Область ВспомогателиныеПроцедурыИФункции

////// Заполнение дерева элементов.

&НаСервере
Процедура ЗаполнитьДеревоЭлементов()
	Запрос_ = Новый Запрос(
		"ВЫБРАТЬ
		|	ШаблоныМедицинскихДокументовИзбранное.Ссылка,
		|	ШаблоныМедицинскихДокументовИзбранное.Владелец,
		|	ШаблоныМедицинскихДокументовИзбранное.Код,
		|	ШаблоныМедицинскихДокументовИзбранное.Наименование,
		|	ШаблоныМедицинскихДокументовИзбранное.ШаблонМедицинскогоДокумента,
		|	ШаблоныМедицинскихДокументовИзбранное.РеквизитДопУпорядочевания
		|ИЗ
		|	Справочник.ШаблоныМедицинскихДокументовИзбранное КАК ШаблоныМедицинскихДокументовИзбранное
		|ГДЕ
		|	ШаблоныМедицинскихДокументовИзбранное.Владелец = &Пользователь
		|	И ШаблоныМедицинскихДокументовИзбранное.ПометкаУдаления = ЛОЖЬ
		|АВТОУПОРЯДОЧИВАНИЕ
		|ИТОГИ ПО Ссылка ИЕРАРХИЯ"
	);
	Запрос_.УстановитьПараметр("Пользователь", ЭтотОбъект.Пользователь);
	Результат_ = Запрос_.Выполнить().Выбрать(ОбходРезультатаЗапроса.ПоГруппировкамСИерархией);
	Дерево_ = ДеревоТекущихЭлементов.ПолучитьЭлементы();
	ЗаполнитьВеткуДерева(Результат_, Дерево_);
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьВеткуДерева(ВыборкаРезультата, ВетвьДерева)
	Пока ВыборкаРезультата.Следующий() Цикл
		НоваяВетвь_ = ВетвьДерева.Добавить();
		ЗаполнитьЗначенияСвойств(НоваяВетвь_, ВыборкаРезультата);
	КонецЦикла;
КонецПроцедуры

///// При удалении элементов дерева проверим, если есть элемент справочника то пометим его на удаление,
///// если элемента справочника нет, то просто удалим элемент из дерева.

&НаСервере
Процедура ПометитьУдаляемыеЭлементы()
	Для Каждого Элемент_ Из СписокУдаляемых Цикл
		Объект_ = Элемент_.Значение.ПолучитьОбъект();
		Объект_.УстановитьПометкуУдаления(Истина, Истина);
		Объект_.Записать();
	КонецЦикла;
КонецПроцедуры

/// Запись новых элементов.
&НаСервере
Процедура ЗаписатьНовыеЭлементыДерева(ЭлементыДерева)
	Для Каждого Элемент_ Из ЭлементыДерева Цикл
		Если Не ЗначениеЗаполнено(Элемент_.Ссылка)
			И ЗначениеЗаполнено(Элемент_.Наименование)
			И ЗначениеЗаполнено(Элемент_.Код)
		Тогда
			НовыйЭлСправочника_ = Справочники.ШаблоныМедицинскихДокументовИзбранное.СоздатьЭлемент();
			НовыйЭлСправочника_.ШаблонМедицинскогоДокумента = Элемент_.ШаблонМедицинскогоДокумента;
			ЗаполнитьЗначенияСвойств(НовыйЭлСправочника_, Элемент_,,"Ссылка, ШаблонМедицинскогоДокумента, Родитель");
			НовыйЭлСправочника_.Записать();
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры


&НаСервере
Процедура ДобавитьШМДвДерево(СсылкаШМД, Владелец = Неопределено)
	Если ШМДПрисутствуетВДереве(СсылкаШМД) Тогда
		СообщенияПользователю.Показать("ШаблоныМедицинскихДокументов_ПовторноеДобавление", Новый Структура("Шаблон", СсылкаШМД));
	Иначе
		Если Владелец = Неопределено Тогда
			Владелец = ПользователиКлиентСервер.ТекущийПользователь();
		КонецЕсли;
		ЭлементыДерева_ = ДеревоТекущихЭлементов.ПолучитьЭлементы();
		НовыйЭлемент_ = ЭлементыДерева_.Добавить();
		ЗаполнитьЗначенияСвойств(НовыйЭлемент_,СсылкаШМД,,"Ссылка");
		НовыйЭлемент_.ШаблонМедицинскогоДокумента = СсылкаШМД;
		НовыйЭлемент_.Владелец = Владелец;
	КонецЕсли;
КонецПроцедуры

&НаСервере
Функция ШМДПрисутствуетВДереве(ШМДСсылка)
	Дерево_ = ДанныеФормыВЗначение(ЭтотОбъект.ДеревоТекущихЭлементов, Тип("ДеревоЗначений")).Строки;
	Результат_ = Дерево_.Найти(ШМДСсылка,,Истина);
	Возврат Результат_ <> Неопределено;
КонецФункции

#КонецОбласти