﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	// Обработчик подсистемы "Свойства"
	ДополнительныеПараметры = Новый Структура;
	ДополнительныеПараметры.Вставить("Объект", Объект);
	ДополнительныеПараметры.Вставить("ИмяЭлементаДляРазмещения", "ГруппаДополнительныеРеквизиты");
	УправлениеСвойствами.ПриСозданииНаСервере(ЭтотОбъект, ДополнительныеПараметры);
	
	// подсистема запрета редактирования ключевых реквизитов объектов
	ЗапретРедактированияРеквизитовОбъектов.ЗаблокироватьРеквизиты(ЭтотОбъект);
	
	ПодсистемаПодключаемоеОборудование.НастроитьПодключаемоеОборудование(ЭтотОбъект);
	
	Если НЕ ЗначениеЗаполнено(Объект.Ссылка) И ЗначениеЗаполнено(Объект.Владелец) Тогда
		
		Запрос = Новый Запрос(
		"ВЫБРАТЬ
		|	ВидыКартЛояльности.Статус,
		|	ВидыКартЛояльности.ДатаНачалаДействия,
		|	ВидыКартЛояльности.ДатаОкончанияДействия,
		|	ВидыКартЛояльности.ТипКарты
		|ИЗ
		|	Справочник.ВидыКартЛояльности КАК ВидыКартЛояльности
		|ГДЕ
		|	ВидыКартЛояльности.Ссылка = &Ссылка");
		
		Запрос.УстановитьПараметр("Ссылка", Объект.Владелец);
		
		Результат = Запрос.Выполнить();
		Выборка = Результат.Выбрать();
		Выборка.Следующий();
		
		Если ЗначениеЗаполнено(Выборка.ДатаОкончанияДействия) И НачалоДня(ТекущаяДата()) > Выборка.ДатаОкончанияДействия Тогда
			ВызватьИсключение СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru = 'Срок действия карт лояльности ""%1"" истек.'"), Объект.Владелец);
		КонецЕсли;
		Если Выборка.Статус = Перечисления.СтатусыВидовКартЛояльности.Закрыт Тогда
			ВызватьИсключение СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(НСтр("ru = 'Карты лояльности ""%1"" закрыты.'"), Объект.Владелец);
		КонецЕсли;
		
	КонецЕсли;
	
	ПриВыбореВидаКартыЛояльности();
	СформироватьАвтонаименование(ЭтотОбъект);
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтотОбъект, Элементы.ПодменюПечать);
	// Конец СтандартныеПодсистемы.Печать

КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	УправлениеСвойствами.ПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);

	ЗаполнитьИнформациюОПартнере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствамиКлиент.ПослеЗагрузкиДополнительныхРеквизитов(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.Свойства
	
	МенеджерОборудованияКлиентПоликлиника.НачатьПодключениеОборудованиеПриОткрытииФормы(ЭтотОбъект, "СканерШтрихкода,СчитывательМагнитныхКарт");
	
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	
	// СтандартныеПодсистемы.Свойства
	УправлениеСвойствами.ОбработкаПроверкиЗаполнения(ЭтотОбъект, Отказ, ПроверяемыеРеквизиты);
	// Конец СтандартныеПодсистемы.Свойства
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)
	
	Если ЗавершениеРаботы Тогда
		Возврат;
	КонецЕсли;
	
	// ПодключаемоеОборудование
	МенеджерОборудованияКлиентПоликлиника.НачатьОтключениеОборудованиеПриЗакрытииФормы(ЭтотОбъект);
	// Конец ПодключаемоеОборудование
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	// СтандартныеПодсистемы.Свойства
	Если УправлениеСвойствамиКлиент.ОбрабатыватьОповещения(ЭтотОбъект, ИмяСобытия, Параметр) Тогда
		ОбновитьЭлементыДополнительныхРеквизитов();
		УправлениеСвойствамиКлиент.ПослеЗагрузкиДополнительныхРеквизитов(ЭтотОбъект);
	КонецЕсли;
	// Конец СтандартныеПодсистемы.Свойства
	
	// ПодключаемоеОборудование
	Если Источник = "ПодключаемоеОборудование" И ВводДоступен() Тогда
		Если ИмяСобытия = "ScanData" И МенеджерОборудованияКлиентПоликлиника.ЕстьНеобработанноеСобытие() Тогда
			ОбработатьШтрихкоды(МенеджерОборудованияКлиентПоликлиника.ПреобразоватьДанныеСоСканераВМассив(Параметр));
		ИначеЕсли ИмяСобытия ="TracksData" Тогда
			ОбработатьДанныеСчитывателяМагнитныхКарт(Параметр);
		КонецЕсли;
	КонецЕсли;
	// Конец ПодключаемоеОборудование
	
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	
	// Свойства
	УправлениеСвойствами.ПередЗаписьюНаСервере(ЭтотОбъект, ТекущийОбъект);
	// Конец Свойства

КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	// подсистема запрета редактирования ключевых реквизитов объектов
	ЗапретРедактированияРеквизитовОбъектов.ЗаблокироватьРеквизиты(ЭтотОбъект);

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытий

&НаКлиенте
Процедура ПартнерПриИзменении(Элемент)
	
	ЗаполнитьИнформациюОПартнере();
	
КонецПроцедуры

&НаКлиенте
Процедура ВладелецПриИзменении(Элемент)
	
	ПриВыбореВидаКартыЛояльности();
	СформироватьАвтонаименование(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура СоглашениеНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ПродажиКлиент.НачалоВыбораСоглашенияСКлиентом(
		Элемент,
		СтандартнаяОбработка,
		Объект.Партнер,
		Объект.Соглашение,
		ТекущаяДата(),
		,
		,
		ПредопределенноеЗначение("Перечисление.ХозяйственныеОперации.РеализацияКлиенту"),
		Объект);
	
КонецПроцедуры

&НаСервере
Процедура ПриВыбореВидаКартыЛояльности()
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	ВидыКартЛояльности.Статус,
	|	ВидыКартЛояльности.ДатаНачалаДействия,
	|	ВидыКартЛояльности.ДатаОкончанияДействия,
	|	ВидыКартЛояльности.Персонализирована,
	|	ВидыКартЛояльности.ТипКарты
	|ИЗ
	|	Справочник.ВидыКартЛояльности КАК ВидыКартЛояльности
	|ГДЕ
	|	ВидыКартЛояльности.Ссылка = &Ссылка");
	
	Запрос.УстановитьПараметр("Ссылка", Объект.Владелец);
	
	Результат = Запрос.Выполнить();
	Выборка = Результат.Выбрать();
	Если Выборка.Следующий() Тогда
	
		ТипКарты = Выборка.ТипКарты;
		
		Элементы.Штрихкод.Видимость = (ТипКарты = Перечисления.ТипыКарт.Штриховая ИЛИ ТипКарты = Перечисления.ТипыКарт.Смешанная);
		Элементы.МагнитныйКод.Видимость = (ТипКарты = Перечисления.ТипыКарт.Магнитная ИЛИ ТипКарты = Перечисления.ТипыКарт.Смешанная);
		Элементы.ГруппаВладелец.Видимость = Выборка.Персонализирована;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ЗаписатьДанныеПартнераИЗакрытьФорму(Команда)
	
	ЗаписатьДанныеПартнераНаСервере();
	Закрыть();
	
КонецПроцедуры

// Обработчик команды, создаваемой механизмом запрета редактирования ключевых реквизитов.
//
&НаКлиенте
Процедура Подключаемый_РазрешитьРедактированиеРеквизитовОбъекта(Команда) Экспорт
	
	ОбщегоНазначенияУТКлиент.РазрешитьРедактированиеРеквизитовОбъекта(ЭтотОбъект);
	
КонецПроцедуры

// ИнтеграцияС1СДокументооборотом
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуИнтеграции(Команда)
	
КонецПроцедуры
//Конец ИнтеграцияС1СДокументооборотом

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// СтандартныеПодсистемы.Свойства

&НаКлиенте
Процедура Подключаемый_ПриИзмененииДополнительногоРеквизита(Элемент)
	УправлениеСвойствамиКлиент.ОбновитьЗависимостиДополнительныхРеквизитов(ЭтотОбъект);
КонецПроцедуры

// Конец СтандартныеПодсистемы.Свойства

#Область ШтрихкодыИТорговоеОборудование

// МеханизмВнешнегоОборудования
&НаКлиенте
Процедура ОбработатьШтрихкоды(ДанныеШтрихкода)
	
	Объект.Штрихкод = ДанныеШтрихкода[0].Штрихкод;
	
КонецПроцедуры
// Конец МеханизмВнешнегоОборудования

&НаКлиенте
Процедура ОбработатьДанныеСчитывателяМагнитныхКарт(Данные)
	
	СписокКодов = Новый СписокЗначений;
	
	РасшифрованныеДанные = Данные[1][3];
	Если РасшифрованныеДанные <> Неопределено Тогда
		Для Каждого Структура Из РасшифрованныеДанные Цикл
			
			ШаблонМагнитнойКарты = Структура.Шаблон;
			КодКарты             = Неопределено;
			Для Каждого ДанныеПоля Из Структура.ДанныеДорожек Цикл
				Если ДанныеПоля.Поле = ПредопределенноеЗначение("Перечисление.ПоляШаблоновМагнитныхКарт.Код") Тогда
					СписокКодов.Добавить(ДанныеПоля.ЗначениеПоля);
				КонецЕсли;
			КонецЦикла;
			
		КонецЦикла;
	КонецЕсли;
	
	Если СписокКодов.Количество() = 1 Тогда
		Объект.МагнитныйКод = СписокКодов.Получить(0).Значение;
	ИначеЕсли СписокКодов.Количество() = 0 Тогда
		ПоказатьПредупреждение(Неопределено, НСтр("ru = 'Код карты %1 не соответствует ни одному из шаблонов магнитных карт.'"));
	Иначе
		ВыбранноеЗначение = Неопределено;

		СписокКодов.ПоказатьВыборЭлемента(Новый ОписаниеОповещения("ОбработатьДанныеСчитывателяМагнитныхКартЗавершение", ЭтотОбъект), НСтр("ru = 'Выбор кода магнитной карты'"));
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработатьДанныеСчитывателяМагнитныхКартЗавершение(ВыбранныйЭлемент, ДополнительныеПараметры) Экспорт
    
    ВыбранноеЗначение = ВыбранныйЭлемент;
    Если ВыбранноеЗначение <> Неопределено Тогда
        Объект.МагнитныйКод = ВыбранноеЗначение.Значение;
    КонецЕсли;

КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаСервере
Процедура ОбновитьЭлементыДополнительныхРеквизитов()
	
	УправлениеСвойствами.ОбновитьЭлементыДополнительныхРеквизитов(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_РедактироватьСоставСвойств()
	УправлениеСвойствамиКлиент.РедактироватьСоставСвойств(ЭтотОбъект);
КонецПроцедуры

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтотОбъект, Объект);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

&НаКлиенте
Процедура ШтрихкодПриИзменении(Элемент)
	СформироватьАвтонаименование(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура МагнитныйКодПриИзменении(Элемент)
	СформироватьАвтонаименование(ЭтотОбъект);
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура СформироватьАвтонаименование(Форма)
	
	Наименование = Строка(Форма.Объект.Владелец)
	               + ?(ЗначениеЗаполнено(Форма.Объект.Штрихкод), " " + Строка(Форма.Объект.Штрихкод), "")
	               + ?(ЗначениеЗаполнено(Форма.Объект.МагнитныйКод), " " + Строка(Форма.Объект.МагнитныйКод), "");
	
	Форма.Элементы.Наименование.СписокВыбора.Очистить();
	Форма.Элементы.Наименование.СписокВыбора.Добавить(Наименование);
	
КонецПроцедуры

&НаСервере
Процедура ЗаписатьДанныеПартнераНаСервере()
	
	УстановитьПривилегированныйРежим(Истина);
	
	ПартнерОбъект = Объект.Партнер.ПолучитьОбъект();
	ПартнерОбъект.НаименованиеПолное = ФИОПартнера;
	ПартнерОбъект.Записать();
	
	УстановитьПривилегированныйРежим(Ложь);
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьИнформациюОПартнере()
	
	ДанныеКартыЛояльности = КартыЛояльностиВызовСервера.ПолучитьДанныеКартыЛояльности(Объект.Ссылка, Истина);
	
	Элементы.Партнер.Видимость    = ДанныеКартыЛояльности.ПартнерДоступен;
	Элементы.Контрагент.Видимость =  ДанныеКартыЛояльности.КонтрагентДоступен;
	Элементы.Соглашение.Видимость =  ДанныеКартыЛояльности.СоглашениеДоступно;
	
	
	ОтображатьДанныеПартнера =
		ТипЗнч(Объект.Партнер) = Тип("СправочникСсылка.Картотека");
		
	Элементы.ГруппаДанныеПартнераФизическогоЛица.Видимость = ОтображатьДанныеПартнера;
	Если ОтображатьДанныеПартнера Тогда
	
		ФИОПартнера = Строка(Объект.Партнер);
		ПолПациента_ = Регистратура.ПолучитьПолПациента(Объект.Партнер);
		ЭтотОбъект.ПолПартнера = ?(ПолПациента_ = Перечисления.ПолПациентов.Мужской, Перечисления.ПолФизическогоЛица.Мужской, ?(ПолПациента_ = Перечисления.ПолПациентов.Женский, Перечисления.ПолФизическогоЛица.Женский, Неопределено));
		ЭтотОбъект.ДатаРожденияПартнера = Регистратура.ПолучитьДатуРожденияПациента(Объект.Партнер);
		
		Элементы.ГруппаДанныеПартнераФизическогоЛица.Доступность = НЕ ДанныеКартыЛояльности.ПартнерДоступен;
		
		Если НЕ ДанныеКартыЛояльности.ПартнерДоступен Тогда
			
			Элементы.ЗаписатьДанныеПартнераИЗакрытьФорму.Видимость = Истина;
			Элементы.ЗаписатьДанныеПартнераИЗакрытьФорму.КнопкаПоУмолчанию = Истина;
			Элементы.ЗаписатьИЗакрыть.Видимость = Ложь;
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти
