﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	ПодсистемаПодключаемоеОборудование.НастроитьПодключаемоеОборудование(ЭтотОбъект);
	
	УстановитьВидКартыИПартнераИзОтбора();
	
	Список.Параметры.УстановитьЗначениеПараметра("ДатаАктуальности", НачалоДня(ТекущаяДата()));

	ОтборыСписковКлиентСервер.ЗаполнитьСписокВыбораОтбораПоАктуальности(Элементы.ОтборСрокДействия.СписокВыбора);
	ОтборыСписковКлиентСервер.ОтборПоАктуальностиПриСозданииНаСервере(Список, Актуальность, ДатаСобытия, СтруктураБыстрогоОтбора);

	СтруктураБыстрогоОтбора = Неопределено;
	Параметры.Свойство("СтруктураБыстрогоОтбора", СтруктураБыстрогоОтбора);
	
	Если ОтборыСписковКлиентСервер.НеобходимОтборПоСостояниюПриСозданииНаСервере(Состояние, СтруктураБыстрогоОтбора) Тогда
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Состояние", Состояние, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(Состояние));
	КонецЕсли;
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтотОбъект, Элементы.ПодменюПечать);
	// Конец СтандартныеПодсистемы.Печать

КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	МенеджерОборудованияКлиентПоликлиника.НачатьПодключениеОборудованиеПриОткрытииФормы(ЭтотОбъект, "СканерШтрихкода,СчитывательМагнитныхКарт");
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)
	
	Если ЗавершениеРаботы Тогда
		Возврат;
	КонецЕсли;
	
	// ПодключаемоеОборудование
	МенеджерОборудованияКлиентПоликлиника.НачатьОтключениеОборудованиеПриЗакрытииФормы(ЭтотОбъект);
	// Конец ПодключаемоеОборудование
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	// ПодключаемоеОборудование
	Если Источник = "ПодключаемоеОборудование" И ВводДоступен() Тогда
		Если ИмяСобытия = "ScanData" И МенеджерОборудованияКлиентПоликлиника.ЕстьНеобработанноеСобытие() Тогда
			ОбработатьШтрихкоды(МенеджерОборудованияКлиентПоликлиника.ПреобразоватьДанныеСоСканераВМассив(Параметр));
		ИначеЕсли ИмяСобытия ="TracksData" Тогда
			ОбработатьДанныеСчитывателяМагнитныхКарт(Параметр);
		КонецЕсли;
	КонецЕсли;
	// Конец ПодключаемоеОборудование
	
	Если ИмяСобытия = "СчитанаКартаЛояльности"
	 И Параметр.ФормаВладелец = УникальныйИдентификатор Тогда
		Элементы.Список.ТекущаяСтрока = Параметр.КартаЛояльности;
	КонецЕсли;
	
	Если ИмяСобытия = "Запись_КартаЛояльности" Тогда
		Элементы.Список.Обновить();
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПриЗагрузкеДанныхИзНастроекНаСервере(Настройки)
	
	Если УстановитьВидКартыИПартнераИзОтбора() Тогда
		
		ТаблицаСохраненныеНастройки = Новый ТаблицаЗначений;
		ТаблицаСохраненныеНастройки.Колонки.Добавить("Ключ");
		ТаблицаСохраненныеНастройки.Колонки.Добавить("Значение");
		
		Для Каждого КлючИЗначение Из Настройки Цикл
			НоваяСтрока = ТаблицаСохраненныеНастройки.Добавить();
			НоваяСтрока.Ключ = КлючИЗначение.Ключ;
			НоваяСтрока.Значение = КлючИЗначение.Значение;
		КонецЦикла;
		
		СохраненныеНастройки = ПоместитьВоВременноеХранилище(ТаблицаСохраненныеНастройки, УникальныйИдентификатор);
		
		Настройки.Очистить();
		
	Иначе
		
		Партнер = Настройки.Получить("Партнер");
		ВидКарты = Настройки.Получить("ВидКарты");
		
	КонецЕсли;
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Партнер", Партнер, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(Партнер));
	ВидКартыОтборПриИзмененииНаСервере();
	
	ОтборыСписковКлиентСервер.ОтборПоАктуальностиПриЗагрузкеИзНастроек(Список, Актуальность, ДатаСобытия, СтруктураБыстрогоОтбора, Настройки);

	Если ОтборыСписковКлиентСервер.НеобходимОтборПоСостояниюПриЗагрузкеИзНастроек(Список, Состояние, СтруктураБыстрогоОтбора, Настройки) Тогда
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Состояние", Состояние, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(Состояние));
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПриСохраненииДанныхВНастройкахНаСервере(Настройки)
	
	Если ЭтоАдресВременногоХранилища(СохраненныеНастройки) Тогда
		
		ТаблицаСохраненныеНастройки = ПолучитьИзВременногоХранилища(СохраненныеНастройки);
		Для Каждого СтрокаТЧ Из ТаблицаСохраненныеНастройки Цикл
			Настройки.Вставить(СтрокаТЧ.Ключ, СтрокаТЧ.Значение);
		КонецЦикла;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ВидКартыОтборПриИзменении(Элемент)
	
	ВидКартыОтборПриИзмененииНаСервере();
	
КонецПроцедуры

&НаКлиенте
Процедура ПартнерОтборПриИзменении(Элемент)
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Партнер", Партнер, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(Партнер));
	
КонецПроцедуры

&НаКлиенте
Процедура ОтборСостояниеПриИзменении(Элемент)
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Состояние", Состояние, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(Состояние));
	
КонецПроцедуры

&НаКлиенте
Процедура ОтборСрокДействияПриИзменении(Элемент)
	
	ОтборыСписковКлиентСервер.ПриИзмененииОтбораПоАктуальности(Список, Актуальность, ДатаСобытия);
	
КонецПроцедуры

&НаКлиенте
Процедура ОтборСрокДействияОчистка(Элемент, СтандартнаяОбработка)
	
	ОтборыСписковКлиентСервер.ПриОчисткеОтбораПоАктуальности(Список, Актуальность, ДатаСобытия, СтандартнаяОбработка);
	
КонецПроцедуры

&НаКлиенте
Процедура ОтборСрокДействияОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	
	РаботаСДиалогамиКлиент.ПриВыбореОтбораПоАктуальности(
		ВыбранноеЗначение, 
		СтандартнаяОбработка, 
		ЭтотОбъект,
		Список, 
		"Актуальность", 
		"ДатаСобытия");
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

// ИнтеграцияС1СДокументооборотом
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуИнтеграции(Команда)

КонецПроцедуры
//Конец ИнтеграцияС1СДокументооборотом

&НаКлиенте
Процедура Подключаемый_ВыполнитьПереопределяемуюКоманду(Команда)
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область ПриИзмененииРеквизитов

&НаСервере
Процедура ВидКартыОтборПриИзмененииНаСервере()
	
	Персонализирована = КартаПерсонализирована(ВидКарты);
	
	Если Не Персонализирована Тогда
		Партнер = Справочники.Партнеры.ПустаяСсылка();
		ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Партнер", Партнер, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(Партнер));
	КонецЕсли;
	
	Элементы.ПартнерОтбор.Доступность = Персонализирована;
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Владелец", ВидКарты, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(ВидКарты));
	
КонецПроцедуры

#КонецОбласти

#Область ШтрихкодыИТорговоеОборудование

// МеханизмВнешнегоОборудования
&НаКлиенте
Процедура ОбработатьШтрихкоды(ДанныеШтрихкодов)
	
	КартыЛояльностиКлиент.ОбработатьШтрихкоды(ЭтотОбъект, ДанныеШтрихкодов);
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработатьДанныеСчитывателяМагнитныхКарт(Данные)
	
	КартыЛояльностиКлиент.ОбработатьДанныеСчитывателяМагнитныхКарт(ЭтотОбъект, Данные);
	
КонецПроцедуры

#КонецОбласти

#Область Прочее

&НаСервере
Функция УстановитьВидКартыИПартнераИзОтбора()
	
	ЗначениеОтбораУстановлено = Ложь;
	
	Если Параметры.Свойство("Отбор")
	   И Параметры.Отбор.Свойство("Владелец")
	Тогда
		ВидКарты = Параметры.Отбор.Владелец;
		Элементы.ВидКартыОтбор.ТолькоПросмотр = Истина;
		ЗначениеОтбораУстановлено = Истина;
	Иначе
		ВидКарты = Неопределено;
	КонецЕсли;
	
	Если Параметры.Свойство("Отбор")
	   И Параметры.Отбор.Свойство("Партнер")
	Тогда
		Партнер = Параметры.Отбор.Партнер;
		Элементы.ПартнерОтбор.ТолькоПросмотр = Истина;
		ЗначениеОтбораУстановлено = Истина;
	Иначе
		Партнер = Неопределено;
	КонецЕсли;
	
	Возврат ЗначениеОтбораУстановлено;
	
КонецФункции

&НаСервереБезКонтекста
Функция КартаПерсонализирована(ВидКарты)
	
	Персонализирована = Ложь;
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	ВидыКартЛояльности.Персонализирована КАК Персонализирована
	|ИЗ
	|	Справочник.ВидыКартЛояльности КАК ВидыКартЛояльности
	|ГДЕ
	|	ВидыКартЛояльности.Ссылка = &Ссылка");
	
	Запрос.УстановитьПараметр("Ссылка", ВидКарты);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда
		Персонализирована = Выборка.Персонализирована;
	КонецЕсли;
	
	Возврат Персонализирована;
	
КонецФункции

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтотОбъект, Элементы.Список);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

#КонецОбласти

#КонецОбласти
