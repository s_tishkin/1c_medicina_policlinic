﻿
#Область ПрограммныйИнтерфейс

#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ

// Процедура - обработчик события "ПередУдалением"
//
Процедура ПередУдалением(Отказ)

	Если ЭтоГруппа Тогда
		Возврат;
	КонецЕсли;
	
	Если СпособФормирования <> 
			Перечисления.СпособыФормированияСегментов.ФормироватьДинамически Тогда
		СегментыСервер.Очистить(Ссылка);
	КонецЕсли;

КонецПроцедуры

// Процедура - обработчик события "ОбработкаЗаполнения"
//
Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)

	Если ЭтоГруппа Тогда
		Возврат;
	КонецЕсли;
	
	// установить ответственного по умолчанию
	Если НЕ ЗначениеЗаполнено(Ответственный) Тогда
		Ответственный = Пользователи.ТекущийПользователь();
	КонецЕсли;

	// установить дату создания
	Если НЕ ЗначениеЗаполнено(ДатаСоздания) Тогда
		ДатаСоздания = ТекущаяДатаСеанса();
	КонецЕсли;

КонецПроцедуры

// Процедура - обработчик события "ОбработкаЗаполнения"
//
Процедура ПриЗаписи(Отказ)

	Если ЭтоГруппа Тогда
		Возврат;
	КонецЕсли;
	
	Если СпособФормирования =
			Перечисления.СпособыФормированияСегментов.ФормироватьДинамически Тогда
		СегментыСервер.Очистить(Ссылка);
	КонецЕсли;

КонецПроцедуры

#КонецЕсли

#КонецОбласти