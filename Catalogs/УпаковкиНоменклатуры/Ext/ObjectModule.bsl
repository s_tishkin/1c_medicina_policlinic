﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	НепроверяемыеРеквизиты = Новый Массив;
	
	Если Не СостоитИзДругихУпаковок Тогда
		НепроверяемыеРеквизиты.Добавить("Родитель");
		НепроверяемыеРеквизиты.Добавить("КоличествоУпаковок");		
	КонецЕсли;	
	
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, НепроверяемыеРеквизиты);
		
КонецПроцедуры

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	ТипЗначения = ТипЗнч(ДанныеЗаполнения);
	Если ТипЗначения = Тип("Структура") Тогда
		Если ДанныеЗаполнения.Свойство("Родитель", Родитель) Тогда
			Если ЗначениеЗаполнено(Родитель) Тогда
				СостоитИзДругихУпаковок = Истина;
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;

КонецПроцедуры

Процедура ПередЗаписью(Отказ)

	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	Если СостоитИзДругихУпаковок Тогда
		Коэффициент = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Родитель, "Коэффициент") * КоличествоУпаковок;
	Иначе
		Родитель = Справочники.УпаковкиНоменклатуры.ПустаяСсылка();
		КоличествоУпаковок = 0;
	КонецЕсли;

	Если Не ЭтоНовый() Тогда
		КоэффициентСтарый = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Ссылка, "Коэффициент");
		Если Коэффициент <> КоэффициентСтарый Тогда
		 
		 	ДополнительныеСвойства.Вставить("ОбновитьЗависимыеУпаковки");
		 	ДополнительныеСвойства.Вставить("ИзменилсяКоэффициент", Коэффициент <> КоэффициентСтарый);
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ПриЗаписи(Отказ)

	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	Если ДополнительныеСвойства.Свойство("ОбновитьЗависимыеУпаковки") Тогда
		ОбновитьЗависимыеУпаковки(ДополнительныеСвойства.ИзменилсяКоэффициент);
	КонецЕсли;
	
КонецПроцедуры

Процедура ОбновитьЗависимыеУпаковки(ИзменилсяКоэффициент)
	
	Если Не ИзменилсяКоэффициент Тогда
		Возврат;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	УпаковкиНоменклатуры.Ссылка
	|ИЗ
	|	Справочник.УпаковкиНоменклатуры КАК УпаковкиНоменклатуры
	|ГДЕ
	|	УпаковкиНоменклатуры.Владелец = &Владелец
	|	И УпаковкиНоменклатуры.Родитель = &Родитель";
	Запрос.УстановитьПараметр("Владелец", Владелец);
	Запрос.УстановитьПараметр("Родитель", Ссылка);
	
	ШаблонСообщения = НСтр("ru='Была изменена упаковка ""%Упаковка%"". Проверьте корректность заполнения.'");
	
	РезультатЗапрос = Запрос.Выполнить();
	Если Не РезультатЗапрос.Пустой() Тогда
		
		Выборка = РезультатЗапрос.Выбрать();
		Пока Выборка.Следующий() Цикл
			
			ЗависимаяУпаковка = Выборка.Ссылка.ПолучитьОбъект();
			ЗависимаяУпаковка.Записать();
			ТекстСообщения = СтрЗаменить(ШаблонСообщения, "%Упаковка%", ЗависимаяУпаковка.Наименование);
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(ТекстСообщения, ЗависимаяУпаковка.Ссылка);
			
		КонецЦикла;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли