﻿
#Область ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

// Процедура - обработчик события формы "ПриСозданииНаСервере".
//
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Список.Параметры.УстановитьЗначениеПараметра("ТолькоТиповые",               Параметры.ТолькоТиповые);
	Список.Параметры.УстановитьЗначениеПараметра("ТолькоИндивидуальные",        Параметры.ТолькоИндивидуальные);
	Список.Параметры.УстановитьЗначениеПараметра("Партнер",                     Параметры.Партнер);
	Список.Параметры.УстановитьЗначениеПараметра("ДатаДокумента",               ?(ЗначениеЗаполнено(Параметры.ДатаДокумента), НачалоДня(Параметры.ДатаДокумента), НачалоДня(ТекущаяДатаСеанса())));
	Список.Параметры.УстановитьЗначениеПараметра("ТолькоКредитные",             Параметры.ТолькоКредитные);
	Список.Параметры.УстановитьЗначениеПараметра("Договор",                     Параметры.Договор);
	
	Если Не ЗначениеЗаполнено(Параметры.Договор) И Параметры.Отбор.Свойство("Договор") Тогда
		ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список, "Договор", Параметры.Отбор.Договор, Истина);
	КонецЕсли;

	Если Параметры.Отбор.Свойство("ИсточникФинансирования") И
		 ЗначениеЗаполнено(Параметры.Отбор.ИсточникФинансирования)
	Тогда
		ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список, "ИсточникФинансирования", Параметры.Отбор.ИсточникФинансирования, Истина);
	КонецЕсли;
	
	ДоступныеИсточникиФинансирования_ = РаботаССоглашениями.ПолучитьСписокРазрешенныхИсточниковФинансирования();
	ОбщегоназначенияКлиентСервер.УстановитьЭлементОтбора(Список.Отбор,
															"ИсточникФинансирования",
															ДоступныеИсточникиФинансирования_,
															ВидСравненияКомпоновкиДанных.ВСписке,
															"Доступные",
															Истина);
	
	Если Параметры.ТолькоНаКомиссию Тогда
		ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список, "ХозяйственнаяОперация", Перечисления.ХозяйственныеОперации.ПередачаНаКомиссию, Истина);
	КонецЕсли;
	
	Если Параметры.ТолькоТиповые Тогда
		Заголовок = НСтр("ru='Типовые соглашения с клиентами'");
	ИначеЕсли Параметры.ТолькоИндивидуальные Тогда
		Заголовок = НСтр("ru='Индивидуальные соглашения с клиентами'");
	КонецЕсли;
	
КонецПроцедуры // ПриСозданииНаСервере()

#КонецОбласти