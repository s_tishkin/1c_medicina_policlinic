﻿
#Область ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

// Процедура - обработчик события формы "ПриСозданииНаСервере".
//
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтотОбъект, Элементы.СписокКоманднаяПанель);
	// Конец СтандартныеПодсистемы.Печать
	
	// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
	
	Список.Параметры.УстановитьЗначениеПараметра("ДатаАктуальности", НачалоДня(ТекущаяДатаСеанса()));
	Список.Параметры.УстановитьЗначениеПараметра("Партнер",          Параметры.Партнер);
	
	Если ЗначениеЗаполнено(Параметры.Партнер) Тогда
		Заголовок = НСтр("ru='Соглашения с клиентом'");
	КонецЕсли;
	
	СтруктураБыстрогоОтбора = Неопределено;
	Параметры.Свойство("СтруктураБыстрогоОтбора", СтруктураБыстрогоОтбора);
	
	ОтборыСписковКлиентСервер.ОтборПоМенеджеруПриСозданииНаСервере(Список, Менеджер, СтруктураБыстрогоОтбора);
	ОтборыСписковКлиентСервер.ОтборПоАктуальностиПриСозданииНаСервере(Список, Актуальность, ДатаАктуальности, СтруктураБыстрогоОтбора);

	Если ОтборыСписковКлиентСервер.НеобходимОтборПоПриоритетуПриСозданииНаСервере(Список, Состояние, СтруктураБыстрогоОтбора) Тогда
		ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список, "Состояние", Состояние, ЗначениеЗаполнено(Состояние));
	КонецЕсли;
	
	Если СтруктураБыстрогоОтбора <> Неопределено Тогда
		СтруктураБыстрогоОтбора.Свойство("ТипСоглашения",    ТипСоглашения);
	КонецЕсли;
	
	Элементы.ДатаАктуальности.Доступность = ЗначениеЗаполнено(Актуальность);
	
КонецПроцедуры // ПриСозданииНаСервере()

// Процедура - обработчик события формы "ПриЗагрузкеДанныхИзНастроекНаСервере".
//
&НаСервере
Процедура ПриЗагрузкеДанныхИзНастроекНаСервере(Настройки)
	
	Если СтруктураБыстрогоОтбора = Неопределено Тогда
		
		ТипСоглашения    = Настройки.Получить("ТипСоглашения");
		
	Иначе
	
		Если Не СтруктураБыстрогоОтбора.Свойство("ТипСоглашения") Тогда
			Менеджер = Настройки.Получить("ТипСоглашения");
		КонецЕсли;
		
	КонецЕсли;
		
	Настройки.Удалить("ТипСоглашения");
	
	ОтборыСписковКлиентСервер.ОтборПоМенеджеруПриЗагрузкеИзНастроек(Список, Менеджер, СтруктураБыстрогоОтбора, Настройки);
	ОтборыСписковКлиентСервер.ОтборПоАктуальностиПриЗагрузкеИзНастроек(Список, Актуальность, ДатаАктуальности, СтруктураБыстрогоОтбора, Настройки);

	Если ОтборыСписковКлиентСервер.НеобходимОтборПоСостояниюПриЗагрузкеИзНастроек(Список, Состояние, СтруктураБыстрогоОтбора, Настройки) Тогда
		ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список, "Состояние", Состояние, ЗначениеЗаполнено(Состояние));
	КонецЕсли;
	
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список, "ТипСоглашения", ТипСоглашения, ЗначениеЗаполнено(ТипСоглашения));
	
	Элементы.ДатаАктуальности.Доступность = ЗначениеЗаполнено(Актуальность);

КонецПроцедуры // ПриЗагрузкеДанныхИзНастроекНаСервере()

//////////////////////////////////////////////////////////////////////////////////
//// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ УПРАВЛЕНИЯ ФОРМЫ

// Процедура - обработчик события "ПриИзменении" элемента формы "Статус".
//
&НаКлиенте
Процедура ОтборСостояниеПриИзменении(Элемент)
	
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список, "Состояние", Состояние, ЗначениеЗаполнено(Состояние));
	
КонецПроцедуры // ОтборСостояниеПриИзменении()

// Процедура - обработчик события "ПриИзменении" элемента формы "Менеджер".
//
&НаКлиенте
Процедура МенеджерПриИзменении(Элемент)
	
	ОтборыСписковКлиентСервер.УстановитьОтборВСпискеПоМенеджеру(Список, Менеджер);
	
КонецПроцедуры // МенеджерПриИзменении()

// Процедура - обработчик события "ПриИзменении" элемента формы "ТипСоглашения".
//
&НаКлиенте
Процедура ТипСоглашенияПриИзменении(Элемент)
	
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список, "ТипСоглашения", ТипСоглашения, ЗначениеЗаполнено(ТипСоглашения));
	
КонецПроцедуры // ВыводитьСоглашенияПриИзменении()

// Процедура - обработчик события "ПриИзменении" элемента формы "ТолькоПросроченные".
//
&НаКлиенте
Процедура АктуальностьПриИзменении(Элемент)
	
	ОтборыСписковКлиентСервер.ПриИзмененииОтбораПоАктуальности(Список, Актуальность, ДатаАктуальности, Элементы.ДатаАктуальности);
	
КонецПроцедуры // ТолькоПросроченныеПриИзменении()

// Процедура - обработчик события "ПриИзменении" элемента формы "ДатаАктуальности".
//
&НаКлиенте
Процедура ДатаАктуальностиПриИзменении(Элемент)
	
	ОтборыСписковКлиентСервер.УстановитьОтборВСпискеПоДатеАктуальности(Список, ДатаАктуальности);
	
КонецПроцедуры // ДатаАктуальностиПриИзменении()

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ КОМАНД

// Процедура - обработчик команды "УстановитьСтатусНеСогласовано" формы.
//
&НаКлиенте
Процедура УстановитьСтатусНеСогласовано(Команда)
	
	ПродажиКлиент.УстановитьСтатусСоглашенийСКлиентамиНеСогласовано(Элементы.Список);

КонецПроцедуры // УстановитьСтатусНеСогласовано()

// Процедура - обработчик команды "УстановитьСтатусДействует" формы.
//
&НаКлиенте
Процедура УстановитьСтатусДействует(Команда)
	
	ПродажиКлиент.УстановитьСтатусСоглашенийСКлиентамиДействует(Элементы.Список);
	
КонецПроцедуры // УстановитьСтатусДействует()

// Процедура - обработчик команды "УстановитьСтатусЗакрыто" формы.
//
&НаКлиенте
Процедура УстановитьСтатусЗакрыто(Команда)
	
	ПродажиКлиент.УстановитьСтатусСоглашенийСКлиентамиЗакрыто(Элементы.Список);
	
КонецПроцедуры // УстановитьСтатусЗакрыто()

#Область ОбработчикиСобытийКомандФормы

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтотОбъект, Элементы.Список);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

#КонецОбласти

#КонецОбласти