﻿
#Область ПрограммныйИнтерфейс

// Процедура - обработчик события "ОбработкаКоманды".
//
&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	ОткрытьФорму(
		"Справочник.СоглашенияСКлиентами.ФормаОбъекта",
		Новый Структура("Типовое",Истина),
		,
		,
	);

КонецПроцедуры // ОбработкаКоманды()


#КонецОбласти