﻿
#Область ПрограммныйИнтерфейс






////////////////////////////////////////////////////////////////////////////////
// процедуры контроля возможности взаимодействия обработки и шаблона типа операции

// функция проверяет ТипОперации на соответствие допустимым полям
//
// Параметры
//     СтруктураДопустимыхПолей - Структура полей, которые требуются обработке. 
//			Ключ - имя поля, значение - тип поля ("Строка", "Число", "Дата")
//     СсылкаТипОперации - Справочник УМО_ТипыОперацийОбменаСКонтрагентами
//     ДляТЧ - Булево. Истина - поля, применяемые в ТЧ, Ложь - в шапке
// Возвращает.
//     Булево. Истина, если все допустимые поля найдены в шаблоне и совпадают по типу
Функция СравнитьСтруктуруДопустимыхПолей(СтруктураДопустимыхПолей, СсылкаТипОперации, ДляТЧ = Истина) Экспорт
	СтруктураПолей = ПолучитьСтруктуруТиповыхПолей(СсылкаТипОперации, ДляТЧ);
	Ответ = Истина;
	Для каждого стр Из СтруктураДопустимыхПолей Цикл
		Врем = Неопределено;
		Если СтруктураПолей.Свойство(стр.Ключ,Врем) Тогда
			Если НЕ Врег(СокрЛП(Врем.ТипПоля)) = Врег(СокрЛП(стр.Значение)) Тогда
				Ответ = Ложь;
				Прервать;
			КонецЕсли;
		Иначе
			Ответ = Ложь;
			Прервать;
		КонецЕсли;			
	КонецЦикла;
	
	Возврат Ответ;
КонецФункции


// процедуры получения данных для предопределенных списков

// функция возвращает таблицу типовых полей загрузки
//
// Параметры
//     СсылкаТипОперации - Справочник УМО_ТипыОперацийОбменаСКонтрагентами
//     ДляТЧ - Булево. Истина - поля, применяемые в ТЧ, Ложь - в шапке
// Возвращает
//     ТаблицуЗначений.
//		ИмяПоля,ПредставлениеПоля,ТипПоля ("Строка", "Число", "Дата"),ПустоеЗначениеПоля - соответствующее пустое значение.
Функция ПолучитьТаблицуТиповыхПолей(СсылкаТипОперации = Неопределено, ДляТЧ = Истина) Экспорт
	Запрос = Новый Запрос;
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	УМО_ТипыОперацийОбменаСКонтрагентамиОписаниеПолей.ИмяПоля,
	|	УМО_ТипыОперацийОбменаСКонтрагентамиОписаниеПолей.ПредставлениеПоля,
	|	УМО_ТипыОперацийОбменаСКонтрагентамиОписаниеПолей.НомерСтроки,
	|	УМО_ТипыОперацийОбменаСКонтрагентамиОписаниеПолей.ТипПоля
	|ПОМЕСТИТЬ ВТТ_исходная
	|ИЗ
	|	Справочник.УМО_ТипыОперацийОбменаСКонтрагентами.ОписаниеПолей КАК УМО_ТипыОперацийОбменаСКонтрагентамиОписаниеПолей
	|ГДЕ
	|	УМО_ТипыОперацийОбменаСКонтрагентамиОписаниеПолей.Ссылка = &Ссылка
	|	И УМО_ТипыОперацийОбменаСКонтрагентамиОписаниеПолей.ИскатьВТЧ 
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ВТТ_исходная.ИмяПоля,
	|	ВТТ_исходная.ПредставлениеПоля,
	|	ВТТ_исходная.ТипПоля,
	|	ВЫБОР
	|		КОГДА ВТТ_исходная.ТипПоля = ""Число""
	|			ТОГДА 0
	|		КОГДА ВТТ_исходная.ТипПоля = ""Строка""
	|			ТОГДА ДАТАВРЕМЯ(1, 1, 1)
	|		ИНАЧЕ """"
	|	КОНЕЦ КАК ПустоеЗначениеПоля
	|ИЗ
	|	ВТТ_исходная КАК ВТТ_исходная
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ (ВЫБРАТЬ
	|			ВТТ_исходная.ИмяПоля КАК ИмяПоля,
	|			МИНИМУМ(ВТТ_исходная.НомерСтроки) КАК НомерСтроки
	|		ИЗ
	|			ВТТ_исходная КАК ВТТ_исходная
	|		
	|		СГРУППИРОВАТЬ ПО
	|			ВТТ_исходная.ИмяПоля) КАК ВложенныйЗапрос
	|		ПО ВТТ_исходная.НомерСтроки = ВложенныйЗапрос.НомерСтроки";
	
	Если НЕ ДляТЧ Тогда
		ТекстЗапроса = СтрЗаменить(ТекстЗапроса,"ИскатьВТЧ","ИскатьВШапке");
	КонецЕсли;
	Запрос.Текст = ТекстЗапроса;
	Запрос.УстановитьПараметр("Ссылка",СсылкаТипОперации);
	Ответ = Запрос.Выполнить().Выгрузить();
	
	МассивКУдалению = Новый Массив;
	Для каждого стр Из  Ответ Цикл
		Если НЕ ПроверитьКорректностьИмяПоля(стр.ИмяПоля) Тогда
			МассивКУдалению.Добавить(стр);
		Иначе
			Если ПустаяСтрока(стр.ПредставлениеПоля) Тогда
				стр.ПредставлениеПоля = стр.ИмяПоля;
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
	
	Для каждого стр Из  МассивКУдалению Цикл
		Ответ.Удалить(стр);
	КонецЦикла;
	
	Возврат Ответ;
КонецФункции

// функция возвращает структуру типовых полей загрузки для управляемых форм
//
// Параметры
//     СсылкаТипОперации - Справочник УМО_ТипыОперацийОбменаСКонтрагентами
//     ДляТЧ - Булево. Истина - поля, применяемые в ТЧ, Ложь - в шапке
// Возвращает.
//     Структура. Ключ - ИмяПоля, Значение - структура:
//		ПредставлениеПоля,ТипПоля ("Строка", "Число", "Дата"),ПустоеЗначениеПоля - соответствующее пустое значение.
Функция ПолучитьСтруктуруТиповыхПолей(СсылкаТипОперации = Неопределено, ДляТЧ = Истина) Экспорт
	ТаблицаПолей = ПолучитьТаблицуТиповыхПолей(СсылкаТипОперации, ДляТЧ);
	
	Ответ = Новый Структура;
	Для каждого стр Из  ТаблицаПолей Цикл
		СтруктураПоля = Новый Структура("ПредставлениеПоля,ТипПоля,ПустоеЗначениеПоля","","Строка","");
		ЗаполнитьЗначенияСвойств(СтруктураПоля,стр);
		Ответ.Вставить(стр.ИмяПоля,СтруктураПоля);
	КонецЦикла;
	
	Возврат Ответ;
КонецФункции

// функция проверяет ИмяПоля на допустимость
Функция ПроверитьКорректностьИмяПоля(ИмяПоля) Экспорт
	Ответ = Истина;
	ВремСтрук = Новый Структура ("НомерСтроки");
	Если Не ПустаяСтрока(ИмяПоля) Тогда
		Попытка
			Выполнить("ВремСтрук.Вставить(ИмяПоля,0);");
		Исключение
			Ответ = Ложь;
		КонецПопытки;
	Иначе
		Ответ = Ложь;
	КонецЕсли;
	
	Если Ответ Тогда
		Если ВремСтрук.Количество() = 1 Тогда
			Ответ = Ложь;
		КонецЕсли;
	КонецЕсли;
	
	Возврат Ответ;
КонецФункции



// функция возвращает список типовых расширений файлов
//
Функция ПолучитьСписокТиповыхРасширений(ФорматФайла) Экспорт
	Ответ = Новый Массив;
	ТекВидФайла = ФорматФайла.ВидФайлаОбмена;
	Если ТекВидФайла="DBF" Тогда
		Ответ.Добавить("dbf");
	ИначеЕсли ТекВидФайла="TXT" Тогда
		Ответ.Добавить("txt");
		Ответ.Добавить("csv");
		Ответ.Добавить("sst");
	ИначеЕсли ТекВидФайла="XLS" Тогда
		Ответ.Добавить("xls");
	ИначеЕсли ТекВидФайла="XML" Тогда
		Ответ.Добавить("xml");
	Иначе
	КонецЕсли;
	Возврат Ответ;
КонецФункции












// функция возвращает список типовых полей загрузки
//
Функция ПолучитьПустуюСтруктуруТиповыхПолейШапки() Экспорт
	
	Ответ = Новый Структура;
	
	Врем =  ПолучитьСписокТиповыхПолейШапкиСТипами();
	Для каждого стр Из Врем Цикл
		Если стр.Представление = "Число" Тогда
			ПустоеЗначение1 = 0;
		ИначеЕсли стр.Представление = "Дата" Тогда
			ПустоеЗначение1 = '00010101';
		Иначе
			ПустоеЗначение1 = "";
		КонецЕсли;
		
		Ответ.Вставить(стр.Значение,ПустоеЗначение1);
	КонецЦикла;	
	
	
	Возврат Ответ;
	
	Возврат Ответ;
КонецФункции



// функция возвращает список типовых полей загрузки
//
Функция ПолучитьСписокТиповыхПолей(СсылкаТипОперации = Неопределено) Экспорт
	Ответ = Новый СписокЗначений;
	Если ЗначениеЗаполнено(СсылкаТипОперации) Тогда
		Запрос = Новый Запрос;
		Запрос.Текст =
		"ВЫБРАТЬ
		|	УМО_ТипыОперацийОбменаСКонтрагентамиОписаниеПолей.ИмяПоля,
		|	УМО_ТипыОперацийОбменаСКонтрагентамиОписаниеПолей.ПредставлениеПоля,
		|	УМО_ТипыОперацийОбменаСКонтрагентамиОписаниеПолей.НомерСтроки
		|ПОМЕСТИТЬ ВТТ_исходная
		|ИЗ
		|	Справочник.УМО_ТипыОперацийОбменаСКонтрагентами.ОписаниеПолей КАК УМО_ТипыОперацийОбменаСКонтрагентамиОписаниеПолей
		|ГДЕ
		|	УМО_ТипыОперацийОбменаСКонтрагентамиОписаниеПолей.Ссылка = &Ссылка
		|	И УМО_ТипыОперацийОбменаСКонтрагентамиОписаниеПолей.ИскатьВТЧ
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВТТ_исходная.ИмяПоля,
		|	ВТТ_исходная.ПредставлениеПоля
		|ИЗ
		|	ВТТ_исходная КАК ВТТ_исходная
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ (ВЫБРАТЬ
		|			ВТТ_исходная.ИмяПоля КАК ИмяПоля,
		|			МИНИМУМ(ВТТ_исходная.НомерСтроки) КАК НомерСтроки
		|		ИЗ
		|			ВТТ_исходная КАК ВТТ_исходная
		|		
		|		СГРУППИРОВАТЬ ПО
		|			ВТТ_исходная.ИмяПоля) КАК ВложенныйЗапрос
		|		ПО ВТТ_исходная.НомерСтроки = ВложенныйЗапрос.НомерСтроки";
		
	    Запрос.УстановитьПараметр("Ссылка",СсылкаТипОперации);
		Выборка = Запрос.Выполнить().Выбрать();
		Пока Выборка.Следующий() Цикл
			Ответ.Добавить(Выборка.ИмяПоля, Выборка.ПредставлениеПоля);
		КонецЦикла;
	КонецЕсли;
	
	//
	//Ответ.Добавить("НомерКод", "Номер, код");
	//Ответ.Добавить("Артикул", "Артикул");
	//Ответ.Добавить("Наименование", "Наименование");
	//Ответ.Добавить("ЕдиницаИзмерения", "Ед. измерения");
	//Ответ.Добавить("Производитель", "Производитель");
	//Ответ.Добавить("ШтрихКод", "Штрих-код");
	//Ответ.Добавить("НомерРЛС", "Номер РЛС");
	//
	//Ответ.Добавить("Серия", "Серия");
	//Ответ.Добавить("НомерГТД", "Номер ГТД");
	//Ответ.Добавить("СрокГодности", "Срок годности");
	//
	//Ответ.Добавить("СтавкаНДС", "Ставка НДС");
	//Ответ.Добавить("ЦенаБезНДС", "Цена без НДС");
	//Ответ.Добавить("ЦенаСНДС", "Цена с НДС");
	//
	//Ответ.Добавить("СуммаБезНДС", "Сумма без НДС");
	//Ответ.Добавить("СуммаНДС", "НДС");
	//Ответ.Добавить("СуммаСНДС", "Сумма с НДС");
	//
	//Ответ.Добавить("Количество", "Количество");
	//
	//// в некоторых форматах в строку вносят информацию о документе
	//Ответ1 = ПолучитьСписокТиповыхПолейШапки();
	//Для каждого стр Из Ответ1 Цикл
	//	Ответ.Добавить(стр.Значение, стр.Представление);
	//КонецЦикла;
	
	
	Возврат Ответ;
КонецФункции

// функция возвращает список типовых полей загрузки
//
Функция ПолучитьПустуюСтруктуруТиповыхПолей() Экспорт
	Ответ = Новый Структура;
	
	Врем =  ПолучитьСписокТиповыхПолейСТипами();
	Для каждого стр Из Врем Цикл
		Если стр.Представление = "Число" Тогда
			ПустоеЗначение1 = 0;
		ИначеЕсли стр.Представление = "Дата" Тогда
			ПустоеЗначение1 = '00010101';
		Иначе
			ПустоеЗначение1 = "";
		КонецЕсли;
		
		Ответ.Вставить(стр.Значение,ПустоеЗначение1);
	КонецЦикла;	
	
	
	Возврат Ответ;
КонецФункции

// функция возвращает список типовых полей загрузки
//
Функция ПолучитьСписокТиповыхПолейШапки() Экспорт
	Ответ = Новый СписокЗначений;
	Ответ.Добавить("Номер", "Номер вх.документа");
	Ответ.Добавить("Дата", "Дата вх.документа");
	Ответ.Добавить("ВалютаДокумента", "Валюта документа");
	Ответ.Добавить("КурсДокумента", "Курс валюты");
	Ответ.Добавить("Комментарий", "Комментарий");
	
	Возврат Ответ;
КонецФункции

// функция возвращает список типовых полей загрузки
//
Функция ПолучитьСписокТиповыхПолейПоиска() Экспорт
	Ответ = Новый СписокЗначений;
	Ответ.Добавить("НомерКод", "Номер, код");
	Ответ.Добавить("Артикул", "Артикул");
	Ответ.Добавить("Наименование", "Наименование");
	Ответ.Добавить("ЕдиницаИзмерения", "Ед. измерения");
	Ответ.Добавить("Производитель", "Производитель");
	Ответ.Добавить("ШтрихКод", "Штрих-код");
	Ответ.Добавить("НомерРЛС", "Номер РЛС");
	
	Возврат Ответ;
КонецФункции

// функция возвращает список типовых полей загрузки
//
Функция ПолучитьСписокТиповыхПолейСТипами() Экспорт
	Ответ = Новый СписокЗначений;
	Ответ.Добавить("НомерКод", "Строка");
	Ответ.Добавить("Артикул", "Строка");
	Ответ.Добавить("Наименование", "Строка");
	Ответ.Добавить("ЕдиницаИзмерения", "Строка");
	Ответ.Добавить("Производитель", "Строка");
	Ответ.Добавить("ШтрихКод", "Строка");
	Ответ.Добавить("НомерРЛС", "Строка");
	
	Ответ.Добавить("Серия", "Строка");
	Ответ.Добавить("НомерГТД", "Строка");
	Ответ.Добавить("СрокГодности", "Дата");
	
	Ответ.Добавить("СтавкаНДС", "Число");
	Ответ.Добавить("ЦенаБезНДС", "Число");
	Ответ.Добавить("ЦенаСНДС", "Число");
	
	Ответ.Добавить("СуммаБезНДС", "Число");
	Ответ.Добавить("СуммаНДС", "Число");
	Ответ.Добавить("СуммаСНДС", "Число");
	
	Ответ.Добавить("Количество", "Число");
	
	// в некоторых форматах в строку вносят информацию о документе
	Ответ1 = ПолучитьСписокТиповыхПолейШапкиСТипами();
	Для каждого стр Из Ответ1 Цикл
		Ответ.Добавить(стр.Значение, стр.Представление);
	КонецЦикла;
	
	Возврат Ответ;
КонецФункции

// функция возвращает список типовых полей загрузки
//
Функция ПолучитьСписокТиповыхПолейШапкиСТипами() Экспорт
	Ответ = Новый СписокЗначений;
	Ответ.Добавить("Номер", "Строка");
	Ответ.Добавить("Дата", "Дата");
	Ответ.Добавить("ВалютаДокумента", "Строка");
	Ответ.Добавить("КурсДокумента", "Число");
	Ответ.Добавить("Комментарий", "Строка");
	
	Возврат Ответ;
КонецФункции



#КонецОбласти