﻿
#Область ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ТаблицаПолей.Очистить();
	
	Если Параметры.Свойство("ВходящийКод") И Параметры.Свойство("ВходящиеИмена") Тогда //строка
		ТекстКода.УстановитьТекст(Параметры.ВходящийКод);
		
		Для каждого СтрокаПоля Из Параметры.ВходящиеИмена Цикл
			Если Не ПустаяСтрока(СтрокаПоля.Значение) Тогда
				стрНовая = ТаблицаПолей.Добавить();
				стрНовая.ИмяПоля = СтрокаПоля.Значение;
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры //ПриСозданииНаСервере()

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ

// Процедура - обработчик команды "ПеренестиПеременную" формы.
//
&НаКлиенте
Процедура ПеренестиПеременную(Команда)
	ВставитьПеременнуюВКод();
КонецПроцедуры //ПеренестиПеременную()

// Процедура - обработчик команды "ПроверитьКод" формы.
//
&НаКлиенте
Процедура ПроверитьКод(Команда)
	
	// подготовка переменных
	СтруктураПолейКонтекста = Новый Структура;
	Для каждого СтрокаПоля Из ТаблицаПолей Цикл
		СтруктураПолейКонтекста.Вставить(СтрокаПоля.ИмяПоля, СтрокаПоля.ЗначениеИсходное);
	КонецЦикла;
	ТекстАлгоритма = ТекстКода.ПолучитьТекст();
	
	ОтказЛокальный = Ложь;
	Ответ = ПроверитьНаСервере(ТекстАлгоритма, СтруктураПолейКонтекста, ОтказЛокальный);
	
	// обработка результата
	Если ОтказЛокальный Тогда
		СообщенияПользователю.ПоказатьСВыводомВЖурналРегистрации(
			"Настройки_ВПроцедуреУстановленПризнакОтказИстина"
		    );
	КонецЕсли;	
	Если ТипЗнч(Ответ) = Тип("Строка") Тогда
		// ошибка
		Сообщение_ = Новый СообщениеПользователю;
		Сообщение_.Текст = ""+Ответ;
		Сообщение_.Сообщить();
	Иначе
		Для каждого СтрокаПоля Из ТаблицаПолей Цикл
			Врем = "";
			Если НЕ Ответ.Свойство(СтрокаПоля.ИмяПоля, Врем) Тогда
				// в ответе нет такого поля
				Врем = Неопределено;
			КонецЕсли;
			СтрокаПоля.ЗначениеРезультат = Врем;
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры //ПроверитьКод()

// Процедура - обработчик команды "СохранитьДанные" формы.
//
&НаКлиенте
Процедура СохранитьДанные(Команда)
	
	Модифицированность = Ложь;
	ОповеститьОВыборе(ТекстКода.ПолучитьТекст());
	
КонецПроцедуры //СохранитьДанные()

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ФОРМЫ

// Процедура - обработчик события "Выбор" элемента формы ТаблицаПолей.
//
&НаКлиенте
Процедура ТаблицаПолейВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	Если Поле.Имя = "ТаблицаПолейИмяПоля" Тогда
		СтандартнаяОбработка = Ложь;
		ВставитьПеременнуюВКод();
	КонецЕсли;
	
КонецПроцедуры //ТаблицаПолейВыбор()

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ

// Процедура переносит имя переменной из таблицы полей в код
//
&НаКлиенте
Процедура ВставитьПеременнуюВКод()
	
	ТекДанные = Элементы.ТаблицаПолей.ТекущиеДанные;
	Если НЕ ТекДанные = Неопределено Тогда
		Элементы.ТекстКода.ВыделенныйТекст = ТекДанные.ИмяПоля;
	КонецЕсли;
	
КонецПроцедуры //ВставитьПеременнуюВКод()

// Функция проверяет текст кода
// Параметры
// 	ТекстАлгоритма - строка, исполняемый код
// 	СтруктураДопПеременных - структура.
//		Ключ - имя переменной, доступной в контексе исполнения кода
//		Значение - начальное значение переменной, доступной в контексе исполнения кода
// 	Отказ - булево. Устанавливается перед выполнением аолгоритма в "ложь", возможно изменение в теле алгоритма.
//
// Возвращает
//	Строка - описание ошибки
//	или структура (если нет ошибки)
//		Ключ - имя переменной, доступной в контексе исполнения кода
//		Значение - конечное значение переменной, доступной в контексе исполнения кода
&НаСервереБезКонтекста
Функция ПроверитьНаСервере(ТекстАлгоритма, СтруктураДопПеременных, ОтказЛокальный)
	ОтказЛокальный = Ложь;
	РезультатВыполнения = МенеджерЗагрузкиВнешнихДанныхСервер.ВыполнитьАлгоритмПользователя(ТекстАлгоритма, СтруктураДопПеременных, ОтказЛокальный);
	Возврат РезультатВыполнения;
	
КонецФункции //ПроверитьНаСервере()


#КонецОбласти