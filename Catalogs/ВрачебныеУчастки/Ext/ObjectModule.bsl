﻿
#Область ПрограммныйИнтерфейс

Процедура ПередЗаписью(Отказ)
	Если ПроверкаДубликата(ЭтотОбъект.Ссылка) Тогда
		Отказ = Истина;
		СообщенияПользователю.Показать(
							"Участки_УчастокУжеИмеетсяВСправочнике",
							Новый Структура(
									"Наименование,ТипУчастка,Подразделение",
									ЭтотОбъект.Наименование, ЭтотОбъект.ТипУчастка, ЭтотОбъект.Владелец
								)
						);
	КонецЕсли;
КонецПроцедуры

/// Функция ПроверкаДубликата
//		проверяет существование участка с таким же наименованием и типом
///
Функция ПроверкаДубликата(Элемент)
	Запрос=Новый Запрос;
	Запрос.Текст="ВЫБРАТЬ
					|	1
					|ИЗ
					|	Справочник.ВрачебныеУчастки КАК Таблица
					|ГДЕ
					|	Таблица.Ссылка <> &Ссылка
					|	И Таблица.Наименование=&Наименование 
					|	И Таблица.Владелец=&Владелец
					|	И Таблица.ТипУчастка=&ТипУчастка ";
	Запрос.УстановитьПараметр("Ссылка",Элемент);
	Запрос.УстановитьПараметр("Наименование",Элемент.Наименование);
	Запрос.УстановитьПараметр("Владелец",Элемент.Владелец);
	Запрос.УстановитьПараметр("ТипУчастка",Элемент.ТипУчастка);
	Результат=Запрос.Выполнить().Выгрузить();
	
	Возврат Результат.Количество()>0;	
КонецФункции

#КонецОбласти