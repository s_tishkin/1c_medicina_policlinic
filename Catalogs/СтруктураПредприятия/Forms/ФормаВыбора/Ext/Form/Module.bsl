﻿
#Область ПрограммныйИнтерфейс

///////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

// Процедура - обработчик события "ПриСозданииНаСервере" формы.
//
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	СписокОрганизаций = ПолучитьСписокОрганизаций();
	// При установке отбора списков невозможно использовать другую серверную процедуру
	// в дин списк начинается беспорядочное обращение к серверу.
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список,"Владелец", СписокОрганизаций, Истина, ВидСравненияКомпоновкиДанных.ВСписке);
	
	Если Параметры.ТолькоДоступные Тогда
		СписокДоступныхПодразделений = Справочники.СтруктураПредприятия.ПолучитьДоступныеПользователюПодразделения();
		ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список,"Ссылка", СписокДоступныхПодразделений, Истина, ВидСравненияКомпоновкиДанных.ВСписке);
	КонецЕсли;
	Элементы.Список.Отображение = ОтображениеТаблицы.Дерево;
	// Подсистема Архивирование
	АрхивированиеСервер.ФормаСпискаПриСозданииНаСервере(ЭтотОбъект, "Список");
	// конец Подсистема Архивирование
	
	ЭтотОбъект.СписокВыбора.ЗагрузитьЗначения(Параметры.СписокВыбора.ВыгрузитьЗначения());

КонецПроцедуры // ПриСозданииНаСервере()

&НаСервере
////
// Функция ПолучитьСписокОрганизаций
//		Возвращает списокорганизаций предприятия
//	Возврат
//		СписокЗначений - организации предприятия
///
Функция ПолучитьСписокОрганизаций()
	СписокВозврата = Новый СписокЗначений();
	ЗапросОрганизации = Новый Запрос;
	ЗапросОрганизации.Текст = 
					"ВЫБРАТЬ
					|	Организации.Ссылка КАК Ссылка
					|ИЗ
					|	Справочник.Организации КАК Организации
					|ГДЕ
					|	Организации.ПометкаУдаления = ЛОЖЬ";
	Выборка = ЗапросОрганизации.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		СписокВозврата.Добавить(Выборка.Ссылка);
	КонецЦикла;
	Если СписокВозврата.Количество() = 0 Тогда
		СписокВозврата.Добавить(Справочники.Организации.ПустаяСсылка());
	КонецЕсли;
	Возврат СписокВозврата;
КонецФункции // ПолучитьСписокОрганизаций()

&НаКлиенте
Процедура СписокВыборЗначения(Элемент, Значение, СтандартнаяОбработка)

	Если ЭтотОбъект.СписокВыбора.Количество() > 0 
		И ЭтотОбъект.СписокВыбора.НайтиПоЗначению(Значение) = Неопределено
	Тогда
		СтандартнаяОбработка = Ложь;
		Значение = Неопределено;
	КонецЕсли;

КонецПроцедуры

// Подсистема Архивирование
&НаКлиенте
Процедура Подключаемый_ПросмотрАрхивныхЭлементов(Команда) Экспорт
	
	Архивирование.КомандаПросмотрАрхивныхЭлементов(ЭтотОбъект, "Список", Команда);
	
КонецПроцедуры
// конец Подсистема Архивирование



#КонецОбласти