﻿
#Область ПрограммныйИнтерфейс

#Если Не ТолстыйКлиентУправляемоеПриложение Или Сервер Тогда

////
 // Процедура: ПервоначальноеЗаполнение
 //   Заполняет Владельца если он не заполнен и используется одна организация.
 //
 // Параметры:
 //   ИзменятьСуществующие
 //     При установке в значение Истина реквизит будет заполнен у всех значений.
 //     При установке в значение Ложь - только у тех, у которых он еще не был заполнен.
 ///
Процедура ПервоначальноеЗаполнение(ИзменятьСуществующие = Ложь, ТребуетсяПерезапуск = Ложь) Экспорт
	Перем Ошибка;
	
	Попытка
	
		ЗапросОрганизации = Новый Запрос;
		ЗапросОрганизации.Текст =
			"ВЫБРАТЬ
			|	Организации.Ссылка КАК Ссылка
			|ИЗ
			|	Справочник.Организации КАК Организации
			|ГДЕ
			|	Организации.ПометкаУдаления = ЛОЖЬ";
		ЗапросПодразделения = Новый Запрос;
		ЗапросПодразделения.Текст =
			"ВЫБРАТЬ
			|	СтруктураПредприятия.Ссылка
			|ИЗ
			|	Справочник.СтруктураПредприятия КАК СтруктураПредприятия
			|ГДЕ
			|	СтруктураПредприятия.Владелец = ЗНАЧЕНИЕ(Справочник.Организации.ПустаяСсылка)";
		Выборка = ЗапросОрганизации.Выполнить().Выгрузить();
		Если Выборка.Количество() > 1 Тогда
			Возврат;
		ИначеЕсли Выборка.Количество() = 1 Тогда
			ВыборкаПодразделения = ЗапросПодразделения.Выполнить().Выбрать();
			Пока ВыборкаПодразделения.Следующий() Цикл
				ЗаполнитьВладельцаИерархииРодителей(ВыборкаПодразделения.Ссылка, Выборка[0].Ссылка);
			КонецЦикла;
		КонецЕсли;
	Исключение
		Ошибка = ПодробноеПредставлениеОшибки(ИнформацияОбОшибке());
		__ПРОВЕРКА__(Ложь, "ПервоначальноеЗаполнение: Непредвиденная ошибка: " + Ошибка);
		ВызватьИсключение;
	КонецПопытки;
КонецПроцедуры

// Заполняет список команд печати.
// 
// Параметры:
//   КомандыПечати - ТаблицаЗначений - состав полей см. в функции УправлениеПечатью.СоздатьКоллекциюКомандПечати.
//
Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт
	
КонецПроцедуры

///
 //  Процедура ЗаполнитьВладельцаИерархииРодителей
 //        рекурсивно заполняет владельцев у всех родителей
 //
 // Параметры:
 //   СсылкаРодитель
 //     Ссылка на элемент справочника Новый Структура предприятия 
 //   Организация.
 //     Организация, которая будет присваиваться подразделению
///
Процедура ЗаполнитьВладельцаИерархииРодителей(СсылкаРодитель, Организация) Экспорт
	Если ЗначениеЗаполнено(СсылкаРодитель.Родитель) Тогда
		ЗаполнитьВладельцаИерархииРодителей(СсылкаРодитель.Родитель, Организация);
	КонецЕсли;

	Если НЕ ЗначениеЗаполнено(СсылкаРодитель.Владелец) Тогда
		ПодразделениеОбъект = СсылкаРодитель.ПолучитьОбъект();
		ПодразделениеОбъект.Владелец = Организация;
		ПодразделениеОбъект.Записать();
	КонецЕсли;
КонецПроцедуры

////
// Функция ПолучитьДоступныеПользователюПодразделения
//		Получает все подразделения, доступные пользователю.
//
// Параметры
//  Пользователь  - СправочникСсылка.Пользователи
//		пользователь дял которого необходимо получить списко подразделений.
//
// Возвращаемое значение:
//   СписокЗначений   - Список подразделений.
///
Функция ПолучитьДоступныеПользователюПодразделения(Дата = Неопределено, Пользователь = Неопределено) Экспорт
	СписокПодразделений = НастройкиКлиентСервер.Получить("Подразделения", Дата, Пользователь);
	Возврат СписокПодразделений;
КонецФункции // ПолучитьДоступныеПользователюПодразделения()

////
// Функция ПолучитьПодразделениеПоОтделению
//       Получает подразделение по переданному элементу.
//
// Параметры
//  СтруктураПредприятия  - СправочникСсылка.СтруктураПредприятия
//      элемент структуры.
//
// Возвращаемое значение:
//   Справочник.СтруктураПредприятия   - ссылка на элемент справочника.
///
Функция ПолучитьПодразделениеПоОтделению(СтруктураПредприятия) Экспорт
	Если ЗначениеЗаполнено(СтруктураПредприятия.Родитель) Тогда
		Возврат ПолучитьПодразделениеПоОтделению(СтруктураПредприятия.Родитель);
	Иначе
		Возврат СтруктураПредприятия;
	КонецЕсли;
КонецФункции

// Подсистема: Архивирование

///
//  Процедура СписокЗависимыхОбъектов
//       заполняет список оъектов, от которых зависят элементы текущего объекта
//  Параметры:
//     ЗависимыеОбъекты {Массив}
//          массив зависимостей.
//  Пример вызова для справочника МедицинскиеКабинеты:
//      Архивирование.ДобавитьЗависимыйОбъект(ЗависимыеОбъекты, "Справочник.МедицинскиеРабочиеМеста", "МедицинскийКабинет");
///
Процедура СписокЗависимыхОбъектов(ЗависимыеОбъекты) Экспорт
	
	Архивирование.ДобавитьЗависимыйОбъект(ЗависимыеОбъекты, "Справочник.ВрачебныеУчастки", "Владелец");
	Архивирование.ДобавитьЗависимыйОбъект(ЗависимыеОбъекты, "Справочник.МедицинскиеКабинеты", "Подразделение");
	Архивирование.ДобавитьЗависимыйОбъект(ЗависимыеОбъекты, "Справочник.МедицинскиеРабочиеМеста", "Подразделение");
	Архивирование.ДобавитьЗависимыйОбъект(ЗависимыеОбъекты, "Справочник.Ресурсы", "Подразделение");
	Архивирование.ДобавитьЗависимыйОбъект(ЗависимыеОбъекты, "Справочник.Сотрудники", "Подразделение");
	
КонецПроцедуры

// Конец Подсистема: Архивирование

#КонецЕсли

#КонецОбласти