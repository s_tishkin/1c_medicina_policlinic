﻿#Область ОбработчикиСобытий

Процедура ОбработкаЗаполнения(Основание)

	Если ТипЗнч(Основание) <> Тип("СправочникСсылка.СпецификацииМедицинскихУслуг") Тогда
		Возврат;
	КонецЕсли;
	
	// Увеличим на единицу код версии.
	ОснованиеКодВерсии = ПолучитьПоследнююВерсию(Основание);
	ЧислоКодВерсии = "";
	ПрефиксКодаВерсии = "";
	Для Сч = 1 По СтрДлина(ОснованиеКодВерсии) Цикл
		СимволКода = Сред(ОснованиеКодВерсии, Сч, 1);
		Если Найти("0123456789", СимволКода) > 0 Тогда
			ЧислоКодВерсии = ЧислоКодВерсии + СимволКода;
		Иначе
			ПрефиксКодаВерсии = ПрефиксКодаВерсии + СимволКода;
		КонецЕсли;
	КонецЦикла;
	ДлинаЧЦ = СтрДлина(ЧислоКодВерсии);
	ЧислоКодВерсии = Формат(Число(ЧислоКодВерсии) + 1, "ЧЦ="+ДлинаЧЦ+";ЧВН=;ЧГ=");
	
	// Заполнить реквизиты шапки спецификации.
	Родитель = Основание.Родитель;
	Код = Основание.Код;
	КодВерсии = ПрефиксКодаВерсии + Строка(ЧислоКодВерсии);
	Наименование = Основание.Наименование;
	Номенклатура = Основание.Номенклатура;
	
	
	СоставУслуги.Загрузить(Основание.СоставУслуги.Выгрузить());
	СоставУслугиМатериалы.Загрузить(Основание.СоставУслугиМатериалы.Выгрузить());
	СоставУслугиПоНоменклатуре.Загрузить(Основание.СоставУслугиПоНоменклатуре.Выгрузить());
	ЭтапыВыполнения.Загрузить(Основание.ЭтапыВыполнения.Выгрузить());
	//НазначаемыйАссортимент.Загрузить(Основание.НазначаемыйАссортимент.Выгрузить());
	//НоменклатураВладельцев.Загрузить(Основание.НоменклатураВладельцев.Выгрузить());
	
КонецПроцедуры // ОбработкаЗаполнения()

Процедура ПередЗаписью(Отказ)
	
	Если ЭтотОбъект.ОбменДанными.Загрузка = Истина Тогда
		Возврат;	
	КонецЕсли;
	
	Если ЭтотОбъект.Состояние = Перечисления.СостоянияОбъектов.Утвержден Тогда
		Отказ = Не (Истина
			И Проверка_ИспользоватьУтверждениеСпецификаций()
			И Проверка_ПометкаУдаленияУтвержденного()
			И Проверка_ВремяУчастияИсполнителей()
			И Проверка_СоставУслуги()
			И Проверка_СоставУслугиПоНоменклатуреВладельцев()
			И Проверка_СоставУслугиПоНоменклатуреРаботИУслуг()
			И Проверка_ВариантыИспользуемыхРесурсов()
			И Проверка_МестаВыполненияЭтапов()
		);
	КонецЕсли;
	
	Если Отказ = Ложь Тогда
		Отказ = Не Проверка_ПометкаУдаленияОсновнойСпецификации();
	КонецЕсли;   
	
	Если Отказ = Ложь Тогда
		Для Каждого СтрокаСоставаУслуги_ из ЭтотОбъект.СоставУслугиПоНоменклатуре Цикл 
			Если Не ЗначениеЗаполнено(СтрокаСоставаУслуги_.КлючСтроки) Тогда
				СтрокаСоставаУслуги_.КлючСтроки = ОпределениеТекущегоКлючаСтроки(
					ЭтотОбъект.СоставУслугиПоНоменклатуре
				);
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	
	Если Отказ = Ложь Тогда
		ОбработатьАктуальностьКэшаСетки();
	КонецЕсли;
	
КонецПроцедуры

Процедура ПриЗаписи(Отказ)
	Если ЭтотОбъект.ОбменДанными.Загрузка = Истина Тогда
		Возврат;	
	КонецЕсли;
	
	МестаВыполненияЭтапов_Выгрузить();
	Если ЭтотОбъект.Номенклатура.ВидМедицинскойУслуги = Перечисления.ВидыМедицинскихУслуг.Многоэтапная Тогда
		РегистрыСведений.ЦеныНоменклатурыВСоставеМногоэтапной.ЗаполнитьДолиЦенЭтаповМногоэтапнойУслуги(
			Ложь, ЭтотОбъект.Ссылка
		);
	КонецЕсли;
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

////
 // Функция: ОпределениеТекущегоКлючаСтроки
 //   Возвращает максимальное значение ключа, увеличенное на единицу.
 //
 // Параметры:
 //   ТЧ 
 //     Табличная часть.
 //   ИмяПоля {Строка}
 //     Имя поля.
 ///
Функция ОпределениеТекущегоКлючаСтроки(ТЧ, ИмяПоля = "КлючСтроки") 
	
	МаксКлюч=1;
	
	Если ТЧ.Количество()=1 Тогда
		Возврат 1;
	КонецЕСли;
	
	Для каждого стрТЧ из ТЧ Цикл
		Если стрТЧ[ИмяПоля] > МаксКлюч Тогда
			МаксКлюч = стрТЧ[ИмяПоля];
		КонецЕсли;		
	КонецЦикла;	
	
	Возврат МаксКлюч+1;
	
КонецФункции

////
 // Функция: ПолучитьПоследнююВерсию
 //   Возвращает максимальный код версии спецификации.
 // 
 // Параметры:
 //   СпрСсылка {СправочникСсылка.СпецификацииМедицинскихУслуг}
 //     Ссылка на элемент.
 //
 // Возврат: {Строка}
 //   Последнюю версию спецификации.
 ///
Функция ПолучитьПоследнююВерсию(СпрСсылка)
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Код", СпрСсылка.Код);
	Запрос.Текст =
	"ВЫБРАТЬ
	|	МАКСИМУМ(СпецификацииМедицинскихУслуг.КодВерсии) КАК КодВерсии
	|ИЗ
	|	Справочник.СпецификацииМедицинскихУслуг КАК СпецификацииМедицинскихУслуг
	|ГДЕ
	|	СпецификацииМедицинскихУслуг.Код = &Код";
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() Тогда
		Возврат Выборка.КодВерсии;
	Иначе 
		Возврат "00000";
	КонецЕсли;
КонецФункции

////
 // Функция: Проверка_ИспользоватьУтверждениеСпецификаций
 //   Проверяет используется ли утверждение спецификаций.
 //
 // Возврат: {Булево}
 //   Признак использования утверждения спецификаций.
 ///
Функция Проверка_ИспользоватьУтверждениеСпецификаций()
	ИспользоватьУтверждениеСпецификаций=ПолучитьФункциональнуюОпцию("ИспользоватьУтверждениеСпецификаций");
	Если НЕ ЭтотОбъект.Ссылка.Состояние=Перечисления.СостоянияОбъектов.Утвержден Тогда
		Если НЕ РольДоступна("ПолныеПрава") И
			ИспользоватьУтверждениеСпецификаций И НЕ РольДоступна("УтверждениеСпецификацийМедицинскихУслуг") Тогда 
			СообщитьОшибкуЗаписиНоменклатуры();
			СообщенияПользователю.Показать("Спецификации_ДляИзмененияСостоянияНеобходимаРоль");
			Возврат Ложь;
		КонецЕсли;
	КонецЕсли;
	Возврат Истина;
КонецФункции

////
 // Функция: Проверка_ПометкаУдаленияУтвержденного
 //   Проверяет не была ли помечена на удаление утвержденная спецификация.
 //
 // Возврат: {Булево}
 //   Признак пометки на удаление утвержденной спецификации.
 ///
Функция Проверка_ПометкаУдаленияУтвержденного()
	Если
		ЭтотОбъект.Состояние = Перечисления.СостоянияОбъектов.Утвержден
		И ЭтотОбъект.ПометкаУдаления	
	Тогда
		СообщитьОшибкуЗаписиНоменклатуры();
		СообщенияПользователю.Показать("Спецификации_НевозможнаПометкаУдаленияУтвержденнойСпецификации");
		Возврат Ложь;
		
	КонецЕсли;
	
	Возврат Истина;
КонецФункции

// Проверка установки пометки удаления основной спецификации.
Функция Проверка_ПометкаУдаленияОсновнойСпецификации()
	Если ЭтотОбъект.ПометкаУдаления	= Ложь Тогда
		Возврат Истина;	
	КонецЕсли;
	
	ОК_ = Истина;
	Если ЭтоОсновнаяСпецификация() Тогда
		ОК_ = Ложь;
		СообщитьОшибкуЗаписиНоменклатуры();
		СообщенияПользователю.Показать("Спецификации_НельзяПометитьНаУдалениеОсновнуюСпецификацию");
	КонецЕсли;
	
	Возврат ОК_;
КонецФункции

////
 // Функция: Проверка_ВремяУчастияИсполнителей
 //   Для одноэтапных услуг проверяет два условия:
 //   1. суммарное время участия исполнителей должно быть >= времени выполнения этапа и
 //   2. время участия одного исполнителя не должно превышать время выполнения этапа.
 //
 // Возврат: {Булево}
 //   Если условия выполняются, то возвращает Истина.
 ///
Функция Проверка_ВремяУчастияИсполнителей()
	Перем ЭтоОдноэтапнаяУслуга, ВремяВыполнения, СуммарноеВремя, Дифференциал, СтрокаИсполнителя;
	ЭтоОдноэтапнаяУслуга = ЭтотОбъект.Номенклатура.ВидМедицинскойУслуги = Перечисления.ВидыМедицинскихУслуг.Одноэтапная;
	Если Не ЭтоОдноэтапнаяУслуга Тогда 
		Если ЭтотОбъект.Исполнители.Количество() = 0 Тогда
			Возврат Истина;
		КонецЕсли;
		СообщитьОшибкуЗаписиНоменклатуры();
		СообщенияПользователю.ПоказатьСПривязкой("Спецификации_НенулевоеЧислоИсполнителейМногоэтапнойУслуги",, "Исполнители", ЭтотОбъект);
		Возврат Ложь;
	КонецЕсли;
	Дифференциал = Дата(1, 1, 1, 0, 0, 0);
	ВремяВыполнения = ЭтотОбъект.ЭтапыВыполнения[0].ВремяВыполнения;
	СуммарноеВремя = Дата(1, 1, 1, 0, 0, 0);
	Для Каждого СтрокаИсполнителя Из ЭтотОбъект.Исполнители Цикл
		СуммарноеВремя = СуммарноеВремя + (СтрокаИсполнителя.ВремяУчастия - Дифференциал);
		Если СтрокаИсполнителя.ВремяУчастия <= ВремяВыполнения Тогда
			Продолжить;
		КонецЕсли;
		СообщитьОшибкуЗаписиНоменклатуры();
		СообщенияПользователю.ПоказатьСПривязкой("Спецификации_ВремяУчастияПревосходитВремяВыполнения",
												,
												"Исполнители[" + (СтрокаИсполнителя.НомерСтроки - 1) + "].ВремяУчастия",
												ЭтотОбъект
											);
		Возврат Ложь;
	КонецЦикла;
	//СуммарноеВремя = СуммарноеВремя + Дифференциал; 
	Если Дата(СуммарноеВремя) >= Дата(ВремяВыполнения) Тогда
		Возврат Истина;
	КонецЕсли;
	СообщитьОшибкуЗаписиНоменклатуры();
	СообщенияПользователю.ПоказатьСПривязкой("Спецификации_СуммарноеВремяУчастияМеньшеВремениВыполнения",
											,
											"Исполнители",
											ЭтотОбъект
										);
	Возврат Ложь;
КонецФункции

////
 // Функция: Проверка_СоставУслуги
 //   Для Многоэтапной услуги проверяет условие: в тч "Состав услуги" д/б хотя бы одна услуга с частотой 
 //   предоставления 1.
 //
 // Возврат: {Булево}
 //   Результат проверки условия: Истина - если условие выполняется.
 //
Функция Проверка_СоставУслуги()
	ЭтоМногоэтапнаяУслуга = (ЭтотОбъект.Номенклатура.ВидМедицинскойУслуги = Перечисления.ВидыМедицинскихУслуг.Многоэтапная);
	Если ЭтоМногоэтапнаяУслуга Тогда
		Кол = 0;
		Для Каждого СтрТЧ Из ЭтотОбъект.СоставУслуги Цикл
			Если СтрТЧ.ЧастотаПредоставления = 1 Тогда
				Кол = Кол + 1;
			КонецЕсли;
		КонецЦикла;
		Если Кол < 1 Тогда 
			СообщитьОшибкуЗаписиНоменклатуры();
			СообщенияПользователю.Показать("Спецификации_ДляМногоэтапнойУслугиДолжнаБытьХотяБыОднаУслугаСЧП1");
			Возврат Ложь;
		КонецЕсли;
	КонецЕсли;

	Возврат Истина;
КонецФункции

////
 // Функция: Проверка_СоставУслугиПоНоменклатуреРаботИУслуг
 //   Проверяет условие: тч "Состав услуги по номенклатуре работ и услуг" может быть заполнена
 //   только для одноэтапной услуги.
 //
 // Возврат: {Булево}
 //   Результат проверки условия: Истина - если условие выполняется.
 ///
Функция Проверка_СоставУслугиПоНоменклатуреРаботИУслуг()
	
	ЭтоОдноэтапнаяУслуга = ЭтотОбъект.Номенклатура.ВидМедицинскойУслуги = Перечисления.ВидыМедицинскихУслуг.Одноэтапная;
	Если Не ЭтоОдноэтапнаяУслуга Тогда
		Если ЭтотОбъект.СоставУслугиПоНоменклатуре.Количество() = 0 Тогда
			Возврат Истина;
		КонецЕсли;
		СообщитьОшибкуЗаписиНоменклатуры();
		СообщенияПользователю.ПоказатьСПривязкой("Спецификации_НепустойСоставУслугиПоНоменклатуреРаботИУслугДляМногоэтапнойУслуги",
												,
												"СоставУслугиПоНоменклатуре",
												ЭтотОбъект
											);
		
		Возврат Ложь;
		
	КонецЕсли;
	
	Возврат Истина;
	
КонецФункции

////
 // Функция: Проверка_СоставУслугиПоНоменклатуреВладельцев
 //   Проверяет правильность заполнения тч "Состав услуги по номенклатуре владельцев".
 //
 // Возврат: {Булево}
 //   Результат проверки условия.
 ///
Функция Проверка_СоставУслугиПоНоменклатуреВладельцев()
	Возврат Истина;
КонецФункции

////
 // Функция: Проверка_ВариантыИспользуемыхРесурсов
 //   Проверяет правильность заполнения ТЧ "Варианты используемого оборудования".
 //   В частности, в табличной части активной спецификации нельзя использовать ресурс, 
 //   помещенный в архив.
 //
 // Возврат: {Булево}
 //   Результат проверки условия.
 ///
Функция Проверка_ВариантыИспользуемыхРесурсов()
	БезОшибок = Истина;
	Если Не ЭтотОбъект.Активная Тогда
		Возврат БезОшибок;
	КонецЕсли;
	Запрос = Новый Запрос;
	Запрос.Текст =
		"ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	Ресурсы.Ссылка
		|ИЗ
		|	Справочник.Ресурсы КАК Ресурсы
		|ГДЕ
		|	Ресурсы.Ссылка В(&Ресурсы)
		|	И Ресурсы.Ссылка <> ЗНАЧЕНИЕ(Справочник.Ресурсы.ПустаяСсылка)
		|	И НЕ Ресурсы.Актуальность"
	;
	Запрос.УстановитьПараметр(
		"Ресурсы", ЭтотОбъект.ВариантыИспользуемыхРесурсов.ВыгрузитьКолонку("Ресурс")
	);
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		Если БезОшибок Тогда
			БезОшибок = Ложь;
		КонецЕсли;
		СообщитьОшибкуЗаписиНоменклатуры();
		СообщенияПользователю.ПоказатьСПривязкой(
			"Спецификации_ОборудованиеНеМожетБытьИспользовано",
			Новый Структура("Оборудование", Выборка.Ссылка),
			"ВариантыИспользуемыхРесурсов[0].Ресурс"
		);
	КонецЦикла;
	Возврат БезОшибок;
КонецФункции

Функция Проверка_МестаВыполненияЭтапов()
	ОК_ = Истина;
	ВрачСМП_ = Перечисления.ИсполнителиМедицинскихУслуг.ВрачСМП;
	Врачи_ = Перечисления.ВидыМедицинскихДолжностей.Врачи;
	СМП_ = Перечисления.ВидыМедицинскихДолжностей.СреднийМедицинскийПерсонал;
	Если ЭтотОбъект.Номенклатура.ИсполнительМедицинскойУслуги = ВрачСМП_ Тогда
		Запрос_ = Новый Запрос(
			"ВЫБРАТЬ
			|	СправочникМедицинскиеРабочиеМестаИсполнители.Ссылка КАК РабочееМесто,
			|	СправочникДолжностиОрганизаций.ВидМедицинскойДолжности
			|ИЗ
			|	Справочник.МедицинскиеРабочиеМеста.Исполнители КАК СправочникМедицинскиеРабочиеМестаИсполнители
			|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.Сотрудники КАК СправочникСотрудники
			|			ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ДолжностиОрганизаций КАК СправочникДолжностиОрганизаций
			|			ПО СправочникСотрудники.Должность = СправочникДолжностиОрганизаций.Ссылка
			|		ПО СправочникМедицинскиеРабочиеМестаИсполнители.Сотрудник = СправочникСотрудники.Ссылка
			|ГДЕ
			|	СправочникМедицинскиеРабочиеМестаИсполнители.Ссылка В(&РабочиеМеста)"
		);
		Запрос_.УстановитьПараметр(
			"РабочиеМеста", ЭтотОбъект.МестаВыполненияЭтапов.ВыгрузитьКолонку("МестоВыполнения")
		);
		Результат_ = Запрос_.Выполнить();
		Выгрузка_ = Результат_.Выгрузить();
		ОтборВрачей_ = Новый Структура("ВидМедицинскойДолжности, РабочееМесто", Врачи_);		
		ОтборСМП_ = Новый Структура("ВидМедицинскойДолжности, РабочееМесто", СМП_);
		Для Каждого СтрокаМестаВыполнения_ Из ЭтотОбъект.МестаВыполненияЭтапов Цикл
			ОтборВрачей_.РабочееМесто = СтрокаМестаВыполнения_.МестоВыполнения;	
			Если Выгрузка_.НайтиСтроки(ОтборВрачей_).Количество() = 0 Тогда
				ОК_ = Ложь;
				СообщитьОшибкуЗаписиНоменклатуры();
				ИмяСообщения_ = "Спецификации_НеУказанСотрудникСВидомДолжностиУРабочегоМеста";
				ПараметрыСообщения_ = Новый Структура("ВидДолжности, РабочееМесто");
				ПараметрыСообщения_.ВидДолжности = Врачи_;
				ПараметрыСообщения_.РабочееМесто = СтрокаМестаВыполнения_.МестоВыполнения;
				СообщенияПользователю.Показать(ИмяСообщения_, ПараметрыСообщения_);
			КонецЕсли;
			
			ОтборСМП_.РабочееМесто = СтрокаМестаВыполнения_.МестоВыполнения;	
			Если Выгрузка_.НайтиСтроки(ОтборСМП_).Количество() = 0 Тогда
				ОК_ = Ложь;	
				СообщитьОшибкуЗаписиНоменклатуры();
				ИмяСообщения_ = "Спецификации_НеУказанСотрудникСВидомДолжностиУРабочегоМеста";
				ПараметрыСообщения_ = Новый Структура("ВидДолжности, РабочееМесто");
				ПараметрыСообщения_.ВидДолжности = СМП_;
				ПараметрыСообщения_.РабочееМесто = СтрокаМестаВыполнения_.МестоВыполнения;
				СообщенияПользователю.Показать(ИмяСообщения_, ПараметрыСообщения_);
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	Возврат ОК_;
КонецФункции

////
 // Процедура: МестаВыполненияЭтапов_Выгрузить
 //   Выгружает табличную часть МестаВыполненияЭтапов
 //   в регистр сведений МестаВыполненияЭтаповУслуг.
 ///
Процедура МестаВыполненияЭтапов_Выгрузить()
	Перем НаборЗаписей,
				СтрокаЭтапа,
				ЭтоУтвержденный,
				Период,
				ТипНабораЗаписей;
	ЭтоУтвержденный = ЭтотОбъект.Состояние = Перечисления.СостоянияОбъектов.Утвержден;
	НаборЗаписей = РегистрыСведений.МестаВыполненияЭтаповУслуг.СоздатьНаборЗаписей();
	НаборЗаписей.Отбор.СпецификацияМедицинскойУслуги.Установить(ЭтотОбъект.Ссылка);
	Если ЭтотОбъект.Состояние = Перечисления.СостоянияОбъектов.Утвержден Тогда
		Для Каждого СтрокаМестаВыполнения Из ЭтотОбъект.МестаВыполненияЭтапов Цикл
			СтрокаНабора = НаборЗаписей.Добавить();
			СтрокаНабора.Вместимость = СтрокаМестаВыполнения.Вместимость;
			СтрокаНабора.КлючСтрокиЭтапа = СтрокаМестаВыполнения.КлючСтрокиЭтапа;
			СтрокаНабора.МестоВыполнения = СтрокаМестаВыполнения.МестоВыполнения;
			СтрокаНабора.МеткаПериода = СтрокаМестаВыполнения.МеткаПериода;
			СтрокаНабора.УсловиеФункционирования = СтрокаМестаВыполнения.УсловиеФункционирования;
			СтрокаЭтапа = КлючСтроки.НайтиСтроку(СтрокаНабора.КлючСтрокиЭтапа, ЭтотОбъект.ЭтапыВыполнения);
			СтрокаНабора.Номенклатура = ЭтотОбъект.Номенклатура;
			СтрокаНабора.СпецификацияМедицинскойУслуги = ЭтотОбъект.Ссылка;
			СтрокаНабора.ТипЭтапа = СтрокаЭтапа.ТипЭтапа;
		КонецЦикла;
	КонецЕсли;
	НаборЗаписей.Записать(Истина);
КонецПроцедуры

Процедура СообщитьОшибкуЗаписиНоменклатуры()
	Если ЭтотОбъект.ДополнительныеСвойства.Свойство("СообщитьОшибкуЗаписиНоменклатуры") = Ложь Тогда
		ИмяСообщения_ = "Спецификации_НеУдалосьЗаписатьСпецификациюДляНоменклатуры";
		ПараметрыСообщения_ = Новый Структура("Номенклатура", ЭтотОбъект.Номенклатура);
		СообщенияПользователю.Показать(ИмяСообщения_, ПараметрыСообщения_);
		ЭтотОбъект.ДополнительныеСвойства.Вставить("СообщитьОшибкуЗаписиНоменклатуры");
	КонецЕсли;
КонецПроцедуры

Функция ЭтоОсновнаяСпецификация()
	Если ЭтотОбъект.ДополнительныеСвойства.Свойство("ЭтоОсновнаяСпецификация") = Ложь Тогда
		ЭтоОсновнаяСпецификация_ = Ложь;
		Если ЗначениеЗаполнено(ЭтотОбъект.Ссылка) Тогда
			Запрос_ = Новый Запрос(
				"ВЫБРАТЬ ПЕРВЫЕ 1
				|	ОсновныеСпецификацииМедицинскихУслуг.Период
				|ИЗ
				|	РегистрСведений.ОсновныеСпецификацииМедицинскихУслуг КАК ОсновныеСпецификацииМедицинскихУслуг
				|ГДЕ
				|	ОсновныеСпецификацииМедицинскихУслуг.Номенклатура = &Номенклатура
				|	И ОсновныеСпецификацииМедицинскихУслуг.СпецификацияМедицинскойУслуги = &СпецификацияМедицинскойУслуги"
			);
			Запрос_.УстановитьПараметр("Номенклатура", ЭтотОбъект.Номенклатура);
			Запрос_.УстановитьПараметр("СпецификацияМедицинскойУслуги", ЭтотОбъект.Ссылка);
			Результат_ = Запрос_.Выполнить();
			ЭтоОсновнаяСпецификация_ = Не Результат_.Пустой();
		КонецЕсли;
		ЭтотОбъект.ДополнительныеСвойства.Вставить("ЭтоОсновнаяСпецификация", ЭтоОсновнаяСпецификация_);
	КонецЕсли;
	Возврат ЭтотОбъект.ДополнительныеСвойства["ЭтоОсновнаяСпецификация"];
КонецФункции

Функция ПолучитьМестаВыполнения(Объект)
	МестаВыполненияОбъекта_ = ОбщиеМеханизмы.СоздатьТаблицу("МестоВыполнения, ВремяВыполнения");
	Отбор_ = Новый Структура("КлючСтрокиЭтапа");
	Для Каждого СтрокаЭтапа_ из Объект.ЭтапыВыполнения Цикл 
		Отбор_.КлючСтрокиЭтапа = СтрокаЭтапа_.КлючСтроки;
		СтрокиМестВыполнения_ = Объект.МестаВыполненияЭтапов.НайтиСтроки(Отбор_);
		Для Каждого СтрокаМестаВыполнения_ Из СтрокиМестВыполнения_ Цикл
			МестоВыполненияОбъекта_ = МестаВыполненияОбъекта_.Добавить();
			МестоВыполненияОбъекта_.МестоВыполнения = СтрокаМестаВыполнения_.МестоВыполнения;
			МестоВыполненияОбъекта_.ВремяВыполнения = СтрокаЭтапа_.ВремяВыполнения;
		КонецЦикла;
	КонецЦикла;
	Возврат МестаВыполненияОбъекта_;
КонецФункции

Процедура ОбработатьАктуальностьКэшаСетки()
	Если ЭтоОсновнаяСпецификация() = Ложь Тогда
		Возврат;	
	КонецЕсли;
	
	МестаВыполнения_ = Новый Массив;
	ВсеМестаВыполненияОбъекта_ = ПолучитьМестаВыполнения(ЭтотОбъект);
	ВсеМестаВыполненияСсылки_ = ПолучитьМестаВыполнения(ЭтотОбъект.Ссылка);
	
	Отбор_ = Новый Структура("МестоВыполнения, ВремяВыполнения");
	Для Каждого СтрокаМестаВыполненияОбъекта_ Из ВсеМестаВыполненияОбъекта_ Цикл
		ЗаполнитьЗначенияСвойств(Отбор_, СтрокаМестаВыполненияОбъекта_);
		Если ВсеМестаВыполненияСсылки_.НайтиСтроки(Отбор_).Количество() = 0 Тогда
			АлгоритмыДляКоллекций.ДобавитьУникальное(
				МестаВыполнения_, СтрокаМестаВыполненияОбъекта_.МестоВыполнения
			);
		КонецЕсли;
	КонецЦикла;
	
	Для Каждого СтрокаМестаВыполненияСсылки_ Из ВсеМестаВыполненияСсылки_ Цикл
		ЗаполнитьЗначенияСвойств(Отбор_, СтрокаМестаВыполненияСсылки_);
		Если ВсеМестаВыполненияОбъекта_.НайтиСтроки(Отбор_).Количество() = 0 Тогда
			АлгоритмыДляКоллекций.ДобавитьУникальное(
				МестаВыполнения_, СтрокаМестаВыполненияСсылки_.МестоВыполнения
			);
		КонецЕсли;
	КонецЦикла;
	
	Для Каждого МестоВыполнения_ Из МестаВыполнения_ Цикл
		РегистрыСведений.КэшСетки.Удалить(МестоВыполнения_);
	КонецЦикла;
КонецПроцедуры

#КонецОбласти