﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// СтандартныеПодсистемы.ВерсионированиеОбъектов
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов
	
	ОформленияВидовОбъектов.НастроитьУсловноеОформление(
		ЭтотОбъект, "МедицинскиеУслуги.Исполнение", "МедицинскиеУслуги", "Подбор"
	);
	
	Элементы.ДинПолеПодборНоменклатуры.Видимость = Ложь;
	ДинамическоеПолеФормы.ПолучитьЭлемент(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), "НоменклатураСоглашения").Видимость = Ложь;
	ДинамическоеПолеФормы_Создать();
	ДинПолеПодборНоменклатурыПриСозданииНаСервере(Отказ, СтандартнаяОбработка);
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	Для Каждого Узел_ Из ЭтотОбъект.МедицинскиеУслуги.ПолучитьЭлементы() Цикл
		Элементы.МедицинскиеУслуги.Развернуть(Узел_.ПолучитьИдентификатор(), Истина);	
	КонецЦикла;
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	МедицинскиеУслугиПолучитьИзОбъекта(ТекущийОбъект);
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	МедицинскиеУслугиПеренестиВОбъект(ТекущийОбъект);
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура НаборыНоменклатурыВыборЗначения(Элемент, Значение, СтандартнаяОбработка)
	УзлыНоменклатур_ = МедицинскиеУслугиПолучитьУзлыНоменклатур();
	Для Каждого Набор_ Из Значение Цикл
		МедицинскиеУслугиДобавитьНаборНоменклатуры(Набор_, УзлыНоменклатур_);
	КонецЦикла;
КонецПроцедуры

&НаКлиенте
Процедура МедицинскиеУслугиПередУдалением(Элемент, Отказ)
	Узел_ = Элементы.МедицинскиеУслуги.ТекущиеДанные;
	Отказ = (Узел_.ПолучитьРодителя() <> Неопределено);
КонецПроцедуры

&НаКлиенте
Процедура МедицинскиеУслугиРеквизитПриИзменении(Элемент)
	Узел_ = Элементы.МедицинскиеУслуги.ТекущиеДанные;
	Если Элемент = Элементы.МедицинскиеУслугиИсполнение Тогда
		МедицинскиеУслугиУстановитьРеквизит(Узел_, "Исполнение", Узел_.Исполнение);		
	ИначеЕсли Элемент = Элементы.МедицинскиеУслугиКоличество Тогда
		МедицинскиеУслугиУстановитьРеквизит(Узел_, "Количество", Узел_.Количество);		
	ИначеЕсли Элемент = Элементы.МедицинскиеУслугиОчередностьВыполнения Тогда
		МедицинскиеУслугиУстановитьРеквизит(Узел_, "ОчередностьВыполнения", Узел_.ОчередностьВыполнения);		
	ИначеЕсли Элемент = Элементы.МедицинскиеУслугиЦена Тогда
		МедицинскиеУслугиУстановитьРеквизит(Узел_, "Цена", Узел_.Цена);		
	КонецЕсли;
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ПодборНоменклатуры(Команда)
	Элемент_ = Элементы.МедицинскиеУслугиПодборНоменклатуры;
	Элемент_.Пометка = Не Элемент_.Пометка;
	Если Элемент_.Пометка = Истина Тогда
		ДинамическоеПолеФормы_Отобразить();
	Иначе
		ДинамическоеПолеФормы_Скрыть();	
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ПодборНаборовНоменклатуры(Команда)
	Элемент_ = Элементы.МедицинскиеУслугиПодборНаборовНоменклатуры;
	Элемент_.Пометка = Не Элемент_.Пометка;
	Если Элемент_.Пометка = Истина Тогда
		ПодборНаборовНоменклатурыОтобразить()
	Иначе
		ПодборНаборовНоменклатурыСкрыть()
	КонецЕсли;
КонецПроцедуры

#КонецОбласти

#Область ВспомогательныеМетоды

&НаКлиенте
Процедура ДинамическоеПолеФормы_КлиентскийОбработчик(
					Элемент, 
					Арг1 = Неопределено,
					Арг2 = Неопределено,
					Арг3 = Неопределено,
					Арг4 = Неопределено)
					
	ДинамическоеПолеФормы.ОбработатьСобытие(
		ЭтотОбъект, Элемент, "ДинамическоеПолеФормы_КлиентскийОбработчик", Арг1, Арг2, Арг3, Арг4
	);
		
КонецПроцедуры

//&НаСервере
//Процедура ДинамическоеПолеФормы_СерверныйОбработчик(Элемент)
//      ДинамическоеПолеФормы.ОбработатьСобытие(ЭтотОбъект, Элемент);
//КонецПроцедуры

// Обработчик выбора услуги в динамическом поле подбора номенклатуры.
&НаКлиенте
Процедура ДинамическоеПолеФормы_ОбработкаВыбора(Результат, ДополнительныеПараметры) Экспорт
	ВыбранныеСтроки = Неопределено;
	ДополнительныеПараметры.Свойство("ВыбраннаяСтрока", ВыбранныеСтроки);
	
	УзлыНоменклатур_ = МедицинскиеУслугиПолучитьУзлыНоменклатур();
	Если ТипЗнч(ВыбранныеСтроки) = Тип("Массив") Тогда
		Для Каждого Номенклатура_ Из ВыбранныеСтроки Цикл
			УзелНоменклатуры_ = МедицинскиеУслугиДобавитьНоменклатуру(Номенклатура_);
			Если УзелНоменклатуры_ <> Неопределено Тогда
				МедицинскиеУслугиДополнитьУзлыНоменклатур(УзлыНоменклатур_, УзелНоменклатуры_);
				МедицинскиеУслугиЗаполнитьРеквизитыНоменклатуры(УзелНоменклатуры_, УзлыНоменклатур_);
			КонецЕсли;
		КонецЦикла;
	Иначе
		УзелНоменклатуры_ = МедицинскиеУслугиДобавитьНоменклатуру(ВыбранныеСтроки);
		Если УзелНоменклатуры_ <> Неопределено Тогда
			МедицинскиеУслугиДополнитьУзлыНоменклатур(УзлыНоменклатур_, УзелНоменклатуры_);
			МедицинскиеУслугиЗаполнитьРеквизитыНоменклатуры(УзелНоменклатуры_, УзлыНоменклатур_);
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

// Обработка ответа на вопрос после изменения типа мед. программы.
//
&НаКлиенте
Процедура ТипМедицинскойПрограммыПриИзмененииПослеВопроса(Результат, Параметры) Экспорт
	Если Результат = КодВозвратаДиалога.Да Тогда
		РазрешеноКИсполнению_ = ПредопределенноеЗначение(
			"Перечисление.ИсполнениеУслугиПоДоговору.РазрешенаКИсполнению"
		);
		Узел_ = Неопределено;
		Пока АлгоритмыДляКоллекций.ОбходДерева_Следующий(ЭтотОбъект.МедицинскиеУслуги, Узел_) Цикл
			Если ЗначениеЗаполнено(Узел_.Номенклатура) Тогда
				Узел_.Исполнение = РазрешеноКИсполнению_;
			КонецЕсли;
		КонецЦикла;
	Иначе
		Объект.ТипМедицинскойПрограммы = ЭтотОбъект.ПредыдущийТипМедПрограммы;
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура МедицинскиеУслугиПолучитьИзОбъекта(ТекущийОбъект)
	ЭтотОбъект.МедицинскиеУслуги.ПолучитьЭлементы().Очистить();
	Индекс_ = 0;
	Для Каждого Услуга_ Из ТекущийОбъект.МедицинскиеУслуги Цикл
		Если Услуга_.ТолькоВНабореНоменклатуры = Ложь Тогда
			УзелНоменклатуры_ = ЭтотОбъект.МедицинскиеУслуги.ПолучитьЭлементы().Вставить(Индекс_);
			ЗаполнитьЗначенияСвойств(УзелНоменклатуры_, Услуга_);
			Индекс_ = Индекс_ + 1;
		КонецЕсли;
		
		ОтборНабора_ = Новый Структура("Номенклатура", Услуга_.Номенклатура);
		СтрокиНаборов_ = ТекущийОбъект.НаборыНоменклатуры.НайтиСтроки(ОтборНабора_);
		Для Каждого СтрокаНабора_ Из СтрокиНаборов_ Цикл
			ОтборУзлаНабора_ = Новый Структура("НаборНоменклатуры", СтрокаНабора_.Набор);
			УзелНабора_ = АлгоритмыДляКоллекций.НайтиВДеревеПоРодителю(
				ЭтотОбъект.МедицинскиеУслуги, ОтборУзлаНабора_,, Истина
			);
			Если УзелНабора_ = Неопределено Тогда
				УзелНабора_ = ЭтотОбъект.МедицинскиеУслуги.ПолучитьЭлементы().Добавить();
				УзелНабора_.НаборНоменклатуры = СтрокаНабора_.Набор
			КонецЕсли;
			
			Если ЗначениеЗаполнено(СтрокаНабора_.Поднабор) Тогда
				ОтборУзлаПоднабора_ = Новый Структура("НаборНоменклатуры", СтрокаНабора_.Поднабор);
				УзелПоднабора_ = АлгоритмыДляКоллекций.НайтиВДеревеПоРодителю(
					ЭтотОбъект.МедицинскиеУслуги, ОтборУзлаПоднабора_, УзелНабора_, Истина
				);
				Если УзелПоднабора_ = Неопределено Тогда
					УзелПоднабора_ = УзелНабора_.ПолучитьЭлементы().Добавить();
					УзелПоднабора_.НаборНоменклатуры = СтрокаНабора_.Поднабор
				КонецЕсли;
				УзелНоменклатуры_ = УзелПоднабора_.ПолучитьЭлементы().Добавить();
				ЗаполнитьЗначенияСвойств(УзелНоменклатуры_, Услуга_);
			Иначе
				УзелНоменклатуры_ = УзелНабора_.ПолучитьЭлементы().Добавить();
				ЗаполнитьЗначенияСвойств(УзелНоменклатуры_, Услуга_);
			КонецЕсли;
		КонецЦикла;
	КонецЦикла;
	ТекущийОбъект.МедицинскиеУслуги.Очистить();	
	ТекущийОбъект.НаборыНоменклатуры.Очистить();
КонецПроцедуры

&НаСервере
Процедура МедицинскиеУслугиПеренестиВОбъект(ТекущийОбъект)
	ТекущийОбъект.МедицинскиеУслуги.Очистить();	
	ТекущийОбъект.НаборыНоменклатуры.Очистить();
	Узлы_ = ЭтотОбъект.МедицинскиеУслуги.ПолучитьЭлементы();
	Для Каждого Узел_ Из Узлы_ Цикл
		МедицинскиеУслугиПеренестиУзелВОбъект(ТекущийОбъект, Узел_);
	КонецЦикла;
КонецПроцедуры

&НаСервере
Процедура МедицинскиеУслугиПеренестиУзелВОбъект(ТекущийОбъект, Узел)
	Если ЗначениеЗаполнено(Узел.Номенклатура) Тогда
		Услуга_ = ТекущийОбъект.МедицинскиеУслуги.Найти(Узел.Номенклатура, "Номенклатура");
		Если Услуга_ = Неопределено Тогда
			Услуга_ = ТекущийОбъект.МедицинскиеУслуги.Добавить();
			ЗаполнитьЗначенияСвойств(Услуга_, Узел);
			Услуга_.ТолькоВНабореНоменклатуры = Истина;	
		КонецЕсли;
		УзелНабора_ = Узел.ПолучитьРодителя();
		Если УзелНабора_ = Неопределено Тогда
			Услуга_.ТолькоВНабореНоменклатуры = Ложь;
		Иначе
			СтрокаНабора_ = ТекущийОбъект.НаборыНоменклатуры.Добавить();
			СтрокаНабора_.Номенклатура = Узел.Номенклатура;
			Если УзелНабора_.ПолучитьРодителя() = Неопределено Тогда
				СтрокаНабора_.Набор = УзелНабора_.НаборНоменклатуры;
			Иначе	
				СтрокаНабора_.Набор = УзелНабора_.ПолучитьРодителя().НаборНоменклатуры;
				СтрокаНабора_.Поднабор = УзелНабора_.НаборНоменклатуры;
			КонецЕсли;
		КонецЕсли;
	ИначеЕсли ЗначениеЗаполнено(Узел.НаборНоменклатуры) Тогда
		УзлыНабора_ = Узел.ПолучитьЭлементы();
		Для Каждого УзелНабора_ Из УзлыНабора_ Цикл
			МедицинскиеУслугиПеренестиУзелВОбъект(ТекущийОбъект, УзелНабора_);
		КонецЦикла;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Функция МедицинскиеУслугиДобавитьНоменклатуру(Номенклатура)
	Если МедицинскиеУслугиНоменклатураДобавлена(Номенклатура) = Истина Тогда
		Возврат Неопределено;	
	КонецЕсли;
	
	ИндексПервогоНабораНоменклатуры_ = -1;
	Узлы_ = ЭтотОбъект.МедицинскиеУслуги.ПолучитьЭлементы();
	Для Индекс_ = 0 По Узлы_.Количество() - 1 Цикл
		Если ЗначениеЗаполнено(Узлы_[Индекс_].НаборНоменклатуры) Тогда
			ИндексПервогоНабораНоменклатуры_ = Индекс_;
			Прервать;
		КонецЕсли;
	КонецЦикла;
	
	УзелНоменклатуры_ = Неопределено;
	Если ИндексПервогоНабораНоменклатуры_ >= 0 Тогда
		УзелНоменклатуры_ = Узлы_.Вставить(ИндексПервогоНабораНоменклатуры_);	
	Иначе	
		УзелНоменклатуры_ = Узлы_.Добавить();
	КонецЕсли;
	УзелНоменклатуры_.Номенклатура = Номенклатура;
	УзелНоменклатуры_.Исполнение = ПредопределенноеЗначение(
		"Перечисление.ИсполнениеУслугиПоДоговору.РазрешенаКИсполнению"
	);
	Возврат УзелНоменклатуры_;
КонецФункции

&НаКлиенте
Функция МедицинскиеУслугиНоменклатураДобавлена(Номенклатура)
	Добавлена_ = Ложь;
	Узлы_ = ЭтотОбъект.МедицинскиеУслуги.ПолучитьЭлементы();
	Для Каждого Узел_ Из Узлы_ Цикл
		Если Узел_.Номенклатура = Номенклатура Тогда
			Добавлена_ = Истина;
			Прервать;
		КонецЕсли;
	КонецЦикла;
	Возврат Добавлена_;
КонецФункции

&НаКлиенте
Процедура МедицинскиеУслугиДобавитьНаборНоменклатуры(Набор, УзлыНоменклатур)
	Если МедицинскиеУслугиНаборНоменклатурыДобавлен(Набор) = Истина Тогда
		Возврат;	
	КонецЕсли;
	
	УзелНабора_ = ЭтотОбъект.МедицинскиеУслуги.ПолучитьЭлементы().Добавить();
	УзелНабора_.НаборНоменклатуры = Набор;
	СоставНабора_ = МедицинскиеУслугиПолучитьСоставНабораНоменклатуры(Набор);
	МедицинскиеУслугиДобавитьНоменклатуруНабора(УзелНабора_, СоставНабора_.Номенклатура, УзлыНоменклатур);
	Для Каждого Строка_ Из СоставНабора_.Поднаборы Цикл
		МедицинскиеУслугиДобавитьПоднаборНоменклатуры(
			УзелНабора_, Строка_.НаборНоменклатуры, Строка_.Номенклатура, УзлыНоменклатур
		);
	КонецЦикла;
	Элементы.МедицинскиеУслуги.Развернуть(УзелНабора_.ПолучитьИдентификатор(), Истина);
КонецПроцедуры

&НаКлиенте
Функция МедицинскиеУслугиНаборНоменклатурыДобавлен(Набор)
	Добавлен_ = Ложь;
	Узлы_ = ЭтотОбъект.МедицинскиеУслуги.ПолучитьЭлементы();
	Для Каждого Узел_ Из Узлы_ Цикл
		Если Узел_.НаборНоменклатуры = Набор Тогда
			Добавлен_ = Истина;
			Прервать;
		КонецЕсли;
	КонецЦикла;
	Возврат Добавлен_;
КонецФункции

&НаСервереБезКонтекста
Функция МедицинскиеУслугиПолучитьСоставНабораНоменклатуры(Набор)
	Возврат Справочники.НаборыНоменклатуры.ПолучитьСоставНабораНоменклатуры(Набор);
КонецФункции

&НаКлиенте
Процедура МедицинскиеУслугиДобавитьНоменклатуруНабора(УзелНабора, НоменклатураНабора, УзлыНоменклатур)
	Для Каждого Строка_ Из НоменклатураНабора Цикл
		УзелНоменклатуры_ = УзелНабора.ПолучитьЭлементы().Добавить();
		МедицинскиеУслугиДополнитьУзлыНоменклатур(УзлыНоменклатур, УзелНоменклатуры_);
		
		ЗаполнитьЗначенияСвойств(УзелНоменклатуры_, Строка_);
		МедицинскиеУслугиЗаполнитьРеквизитыНоменклатуры(УзелНоменклатуры_, УзлыНоменклатур);
	КонецЦикла;
КонецПроцедуры

&НаКлиенте
Процедура МедицинскиеУслугиДобавитьПоднаборНоменклатуры(
	УзелНабора, Поднабор, НоменклатураПоднабора, УзлыНоменклатур
)
	УзелПоднабора_ = УзелНабора.ПолучитьЭлементы().Добавить();
	УзелПоднабора_.НаборНоменклатуры = Поднабор;
	МедицинскиеУслугиДобавитьНоменклатуруНабора(УзелПоднабора_, НоменклатураПоднабора, УзлыНоменклатур);
КонецПроцедуры

&НаКлиенте
Процедура МедицинскиеУслугиЗаполнитьРеквизитыНоменклатуры(УзелНоменклатуры, УзлыНоменклатур)
	УзлыНоменклатуры_ = МедицинскиеУслугиПолучитьУзлыНоменклатуры(
		УзелНоменклатуры.Номенклатура, УзлыНоменклатур
	);
	Если УзлыНоменклатуры_.Количество() > 1 Тогда
		Для Индекс_ = 0 По УзлыНоменклатуры_.ВГраница() Цикл
			Если УзелНоменклатуры <> УзлыНоменклатуры_[Индекс_] Тогда
				ЗаполнитьЗначенияСвойств(
					УзелНоменклатуры, УзлыНоменклатуры_[Индекс_],, "Номенклатура, НаборНоменклатуры"
				);
				Прервать;
			КонецЕсли;
		КонецЦикла;
	Иначе
		Родитель_ = УзелНоменклатуры.ПолучитьРодителя();
		Узлы_ = ?(
			Родитель_ = Неопределено, 
			ЭтотОбъект.МедицинскиеУслуги.ПолучитьЭлементы(), 
			Родитель_.ПолучитьЭлементы()
		);
		Если Узлы_.Количество() > 0 И ЗначениеЗаполнено(Узлы_[0].Номенклатура) И 
			 Узлы_[0] <> УзелНоменклатуры 
		Тогда
			Если Не ЗначениеЗаполнено(УзелНоменклатуры.Исполнение) Тогда
				УзелНоменклатуры.Исполнение = Узлы_[0].Исполнение;
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура МедицинскиеУслугиУстановитьРеквизит(Узел, ИмяРеквизита, ЗначениеРеквизита)
	УзлыНоменклатур_ = МедицинскиеУслугиПолучитьУзлыНоменклатур();
	Если ЗначениеЗаполнено(Узел.НаборНоменклатуры) Тогда
		МедицинскиеУслугиУстановитьРеквизитНабораНоменклатуры(
			Узел, ИмяРеквизита, ЗначениеРеквизита, УзлыНоменклатур_
		);		
		Узел[ИмяРеквизита] = Неопределено;
	ИначеЕсли ЗначениеЗаполнено(Узел.Номенклатура) Тогда
		МедицинскиеУслугиУстановитьРеквизитНоменклатуры(
			Узел.Номенклатура, ИмяРеквизита, ЗначениеРеквизита, УзлыНоменклатур_
		);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура МедицинскиеУслугиУстановитьРеквизитНабораНоменклатуры(
	УзелНабора, ИмяРеквизита, ЗначениеРеквизита, УзлыНоменклатур
)
	Для Каждого Узел_ Из УзелНабора.ПолучитьЭлементы() Цикл
		Если ЗначениеЗаполнено(Узел_.Номенклатура) Тогда
			МедицинскиеУслугиУстановитьРеквизитНоменклатуры(
				Узел_.Номенклатура, ИмяРеквизита, ЗначениеРеквизита, УзлыНоменклатур
			);
		ИначеЕсли ЗначениеЗаполнено(Узел_.НаборНоменклатуры) Тогда
			МедицинскиеУслугиУстановитьРеквизитНабораНоменклатуры(
				Узел_, ИмяРеквизита, ЗначениеРеквизита, УзлыНоменклатур
			);
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

&НаКлиенте
Процедура МедицинскиеУслугиУстановитьРеквизитНоменклатуры(
	Номенклатура, ИмяРеквизита, ЗначениеРеквизита, УзлыНоменклатур
)
	УзлыНоменклатуры_ = МедицинскиеУслугиПолучитьУзлыНоменклатуры(Номенклатура, УзлыНоменклатур);
	Для Каждого УзелНоменклатуры_ Из УзлыНоменклатуры_ Цикл
		УзелНоменклатуры_[ИмяРеквизита] = ЗначениеРеквизита;	
	КонецЦикла;
КонецПроцедуры

&НаКлиенте
Функция МедицинскиеУслугиПолучитьУзлыНоменклатур()
	УзлыНоменклатур_ = Новый Соответствие;
	Узел_ = Неопределено;
	Пока АлгоритмыДляКоллекций.ОбходДерева_Следующий(ЭтотОбъект.МедицинскиеУслуги, Узел_) Цикл
		Если ЗначениеЗаполнено(Узел_.Номенклатура) Тогда
			МедицинскиеУслугиДополнитьУзлыНоменклатур(УзлыНоменклатур_, Узел_);
		КонецЕсли;
	КонецЦикла;
	Возврат УзлыНоменклатур_;
КонецФункции	

&НаКлиенте
Процедура МедицинскиеУслугиДополнитьУзлыНоменклатур(УзлыНоменклатур, УзелНоменклатуры)
	УзлыНоменклатуры_ = УзлыНоменклатур.Получить(УзелНоменклатуры.Номенклатура);
	Если УзлыНоменклатуры_ = Неопределено Тогда
		УзлыНоменклатуры_ = Новый Массив;
		УзлыНоменклатур.Вставить(УзелНоменклатуры.Номенклатура, УзлыНоменклатуры_);
	КонецЕсли;
	УзлыНоменклатуры_.Добавить(УзелНоменклатуры);
КонецПроцедуры	

&НаКлиенте
Функция МедицинскиеУслугиПолучитьУзлыНоменклатуры(Номенклатура, УзлыНоменклатур)
	УзлыНоменлатуры_ = УзлыНоменклатур.Получить(Номенклатура);
	Возврат ?(УзлыНоменлатуры_ = Неопределено, Новый Массив, УзлыНоменлатуры_);
КонецФункции

// Событие динамического поля для снятия выделения номенклатуры в подборе.
//
&НаКлиенте 
Процедура ОбработкаСнятияВыделенияПодбораНоменклатуры(ДинПоле) Экспорт
	ЭтотОбъект.ПодключитьОбработчикОжидания("СнятьВыделениеПодбораНоменклатуры", 0.1, Истина);	
КонецПроцедуры

&НаКлиенте
Процедура ПодборНаборовНоменклатурыОтобразить()
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
		Элементы, "ГруппаМедицинскиеУслугиПодборНаборовНоменклатуры", "Видимость", Истина
	);
	Элементы.ГруппаМедицинскиеУслугиПодбор.ТекущаяСтраница = 
		Элементы.ГруппаМедицинскиеУслугиПодборНаборовНоменклатуры;
КонецПроцедуры

&НаКлиенте
Процедура ПодборНаборовНоменклатурыСкрыть()
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
		Элементы, "ГруппаМедицинскиеУслугиПодборНаборовНоменклатуры", "Видимость", Ложь
	);
КонецПроцедуры

&НаСервере
Процедура ДинамическоеПолеФормы_Создать()
	Параметры_ = Новый Структура;
	Параметры_.Вставить("КлючНазначенияИспользования", "ПодборУслугВСоглашение");
	Параметры_.Вставить("Префикс", "ПодборНоменклатуры");
	Параметры_.Вставить("РежимВыбораСтрок", "Множественный");
	
	ДинПолеПодборНоменклатурыУстановитьПараметрыИнициализации(Параметры_);
КонецПроцедуры

&НаСервере
Процедура ДинамическоеПолеФормы_Отобразить()
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
		Элементы, "ДинПолеПодборНоменклатуры", "Видимость", Истина
	);
	Элементы.ГруппаМедицинскиеУслугиПодбор.ТекущаяСтраница = Элементы.ДинПолеПодборНоменклатуры;
КонецПроцедуры

&НаСервере
Процедура ДинамическоеПолеФормы_Скрыть()
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(
		Элементы, "ДинПолеПодборНоменклатуры", "Видимость", Ложь
	);
КонецПроцедуры


#КонецОбласти

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	// Вставить содержимое обработчика.
ДинПолеПодборНоменклатурыОбработкаОповещения(ИмяСобытия, Параметр, Источник);
КонецПроцедуры
#Область ДинПолеПодборНоменклатуры
&НаСервере
Процедура ДинПолеПодборНоменклатурыУстановитьПараметрыИнициализации(ПараметрыИн)
	ДинамическоеПолеФормы.УстановитьРеквизит(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), "ПараметрыИнициализации", ПараметрыИн);
КонецПроцедуры

&НаСервере
Процедура ДинПолеПодборНоменклатурыПриСозданииНаСервере(Отказ, СтандартнаяОбработка) Экспорт
	ДинПоле = ДинПолеПодборНоменклатурыПолучитьДинПоле();
	ПараметрыПоля = ДинамическоеПолеФормы.ПолучитьРеквизит(ЭтотОбъект, ДинПоле, "ПараметрыИнициализации");
	Если ПараметрыПоля = Неопределено Тогда
		ДинПолеПодборНоменклатурыУстановитьПараметрыИнициализации(Параметры);	
	КонецЕсли;
	ПараметрыПоля = ДинамическоеПолеФормы.ПолучитьРеквизит(ЭтотОбъект, ДинПоле, "ПараметрыИнициализации");
	ДинПолеПодборНоменклатуры.ПриСозданииНаСервере(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), ПараметрыПоля);
КонецПроцедуры

&НаКлиенте
Процедура ДинПолеПодборНоменклатурыОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	Если ИмяСобытия = "СнятьВыделениеПодбораНоменклатуры" Тогда
		ДинПолеПодборНоменклатуры.СнятьВыделениеПодбораНоменклатуры(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле());
	КонецЕсли;
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция ДинПолеПодборНоменклатурыПолучитьДинПоле()
	Возврат "ДинПолеПодборНоменклатуры";
КонецФункции

&НаКлиенте
Процедура ДинПолеПодборНоменклатурыОбработчикПриИзменении(Элемент)
	Если ДинПолеПодборНоменклатуры.ОбработчикПриИзменении(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), Элемент) <> Истина Тогда
		ДинПолеПодборНоменклатурыОбработчикПриИзмененииНаСервере(Элемент.Имя);
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ДинПолеПодборНоменклатурыОбработчикПриИзмененииНаСервере(ИмяЭлемента)
	ДинПолеПодборНоменклатуры.ОбработчикПриИзмененииНаСервере(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), ИмяЭлемента);
КонецПроцедуры

&НаКлиенте
Процедура ДинПолеПодборНоменклатурыОбработчикНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	ДинПолеПодборНоменклатуры.ОбработчикНачалоВыбора(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), Элемент, ДанныеВыбора, СтандартнаяОбработка);
КонецПроцедуры

&НаКлиенте
Процедура ДинПолеПодборНоменклатурыОбработчикОткрытие(Элемент, СтандартнаяОбработка)
	Если ДинПолеПодборНоменклатуры.ОбработчикОткрытие(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), Элемент, СтандартнаяОбработка) <> Истина Тогда
		ДинПолеПодборНоменклатурыОбработчикОткрытиеНаСервере(Элемент.Имя, СтандартнаяОбработка);
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ДинПолеПодборНоменклатурыОбработчикОткрытиеНаСервере(ИмяЭлемента, СтандартнаяОбработка)
	ДинПолеПодборНоменклатуры.ОбработчикОткрытиеНаСервере(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), ИмяЭлемента, СтандартнаяОбработка);
КонецПроцедуры

&НаКлиенте
Процедура ДинПолеПодборНоменклатурыОбработчикАвтоПодбор(Элемент, Текст, ДанныеВыбора, ПараметрыПолученияДанных, Ожидание, СтандартнаяОбработка)
	Если ДинПолеПодборНоменклатуры.ОбработчикАвтоПодбор(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), Элемент, Текст, ДанныеВыбора, ПараметрыПолученияДанных, Ожидание, СтандартнаяОбработка) <> Истина Тогда
		ДинПолеПодборНоменклатурыОбработчикАвтоПодборНаСервере(Элемент.Имя, Текст, ДанныеВыбора, Ожидание, СтандартнаяОбработка);
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ДинПолеПодборНоменклатурыОбработчикАвтоПодборНаСервере(ИмяЭлемента, Текст, ДанныеВыбора, Ожидание, СтандартнаяОбработка)
	ДинПолеПодборНоменклатуры.ОбработчикАвтоПодборНаСервере(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), ИмяЭлемента, Текст, ДанныеВыбора, Ожидание, СтандартнаяОбработка);
КонецПроцедуры

&НаКлиенте
Процедура ДинПолеПодборНоменклатурыОбработчикСписокВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	ДинПолеПодборНоменклатуры.ОбработчикСписокВыбор(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка);
КонецПроцедуры

&НаКлиенте
Процедура ДинПолеПодборНоменклатурыОбработчикСписокПриАктивизацииСтроки(Элемент)
	ДинПолеПодборНоменклатуры.ОбработчикСписокПриАктивизацииСтроки(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), Элемент);
КонецПроцедуры

&НаКлиенте
Процедура ДинПолеПодборНоменклатурыОбработчикКоманд(Команда)
	Если ДинПолеПодборНоменклатуры.ОбработчикКоманд(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), Команда) <> Истина Тогда
		ДинПолеПодборНоменклатурыОбработчикКомандНаСервере(Команда.Имя);
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ДинПолеПодборНоменклатурыОбработчикКомандНаСервере(ИмяКоманды)
	ДинПолеПодборНоменклатуры.ОбработчикКомандНаСервере(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), ИмяКоманды);
КонецПроцедуры

&НаСервере
Процедура ДинПолеПодборНоменклатурыОбработатьСобытиеНаСервере(ИмяСобытия, Параметр1 = Неопределено, Параметр2 = Неопределено, Параметр3 = Неопределено, Параметр4 = Неопределено, Параметр5 = Неопределено, Параметр6 = Неопределено, Параметр7 = Неопределено)
	ДинПолеПодборНоменклатуры.ОбработчикСобытияНаСервере(ЭтотОбъект, ДинПолеПодборНоменклатурыПолучитьДинПоле(), ИмяСобытия, Параметр1, Параметр2, Параметр3, Параметр4, Параметр5, Параметр6, Параметр7);
КонецПроцедуры

&НаКлиенте
Процедура ДинПолеПодборНоменклатурыОбработатьОповещение(Результат, ДополнительныеПараметры) Экспорт
	ДинПолеПодборНоменклатурыОбработатьСобытиеНаСервере(ДополнительныеПараметры.ИмяСобытия);
КонецПроцедуры



#КонецОбласти



