﻿&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	ЭтотОбъект.Медпрограммы.ЗагрузитьЗначения(Параметры.Медпрограммы.ВыгрузитьЗначения());
	ЗаполнитьСоглашенияМедпрограмм();
	ЭтотОбъект.Медпрограммы.ЗаполнитьПометки(Истина);
	ЗаполнитьСоглашения(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура МедпрограммыПриИзменении(Элемент)
	ЗаполнитьСоглашения(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура МедпрограммыПометкаПриИзменении(Элемент)
	ЗаполнитьСоглашения(ЭтотОбъект);
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьСоглашения(Команда)
	Состояние(СообщенияПользователю.Получить("Общие_ОбновлениеДанныхВыполняется"));
	ОК_ = ОбновитьСоглашенияНаСервере();
	Если ОК_ = Истина Тогда
		Состояние(СообщенияПользователю.Получить("Общие_ОбновлениеДанныхЗавершено"));
		ОбщиеМеханизмыКлиентСервер.Пауза(1);
	КонецЕсли;
КонецПроцедуры

&НаСервере
Функция ОбновитьСоглашенияНаСервере()
	ОК_ = Истина;
	Для Каждого ЭлементСоглашения_ Из ЭтотОбъект.Соглашения Цикл
		Если ЭлементСоглашения_.Пометка = Истина Тогда
			СоглашениеОбъект_ = ЭлементСоглашения_.Значение.ПолучитьОбъект();
			УстановкаКвотОбъект_ = СоглашениеОбъект_.ЗаполнитьПоМедпрограммам();
			
			Попытка
				СоглашениеОбъект_.Записать();
				Если УстановкаКвотОбъект_ <> Неопределено Тогда
					УстановкаКвотОбъект_.Записать(РежимЗаписиДокумента.Проведение);	
				КонецЕсли;
			Исключение
				ОК_ = Ложь;
				СообщениеОбОшибке_ = КраткоеПредставлениеОшибки(ИнформацияОбОшибке());
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
					"Ошибка при обновлении соглашения " + ЭлементСоглашения_.Значение + Символы.ПС +
					СообщениеОбОшибке_
				);
				Прервать;
			КонецПопытки;
		КонецЕсли;
	КонецЦикла;
	Возврат ОК_;
КонецФункции

&НаСервере
Процедура ЗаполнитьСоглашенияМедпрограмм()
	Запрос_ = Новый Запрос(
		"ВЫБРАТЬ
		|	СоглашенияСКлиентамиМедицинскиеПрограммы.Ссылка КАК Соглашение,
		|	СоглашенияСКлиентамиМедицинскиеПрограммы.МедицинскаяПрограмма КАК Медпрограмма
		|ИЗ
		|	Справочник.СоглашенияСКлиентами.МедицинскиеПрограммы КАК СоглашенияСКлиентамиМедицинскиеПрограммы
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.СоглашенияСКлиентами КАК СоглашенияСКлиентами
		|		ПО СоглашенияСКлиентамиМедицинскиеПрограммы.Ссылка = СоглашенияСКлиентами.Ссылка
		|ГДЕ
		|	СоглашенияСКлиентамиМедицинскиеПрограммы.МедицинскаяПрограмма В(&Медпрограммы)
		|	И СоглашенияСКлиентами.ХозяйственнаяОперация = ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.РеализацияПоМедПрограмме)"
	);
	Запрос_.УстановитьПараметр("Медпрограммы", ЭтотОбъект.Медпрограммы.ВыгрузитьЗначения());
	Результат_ = Запрос_.Выполнить();
	ЭтотОбъект.СоглашенияМедпрограмм.Загрузить(Результат_.Выгрузить());
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура ЗаполнитьСоглашения(ТекущаяФорма)
	ТекущаяФорма.Соглашения.Очистить();
	Для Каждого ЭлементМедпрограммы_ Из ТекущаяФорма.Медпрограммы Цикл
		Если ЭлементМедпрограммы_.Пометка = Истина Тогда
			Отбор_ = Новый Структура("Медпрограмма", ЭлементМедпрограммы_.Значение);
			СтрокиСоглашенийМедпрограммы_ = ТекущаяФорма.СоглашенияМедпрограмм.НайтиСтроки(Отбор_);
			Для Каждого СтрокаСоглашенияМедпрограммы_ Из СтрокиСоглашенийМедпрограммы_ Цикл
				Соглашение_ = СтрокаСоглашенияМедпрограммы_.Соглашение;
				Если ТекущаяФорма.Соглашения.НайтиПоЗначению(Соглашение_) = Неопределено Тогда
					ТекущаяФорма.Соглашения.Добавить(Соглашение_,, Истина);	
				КонецЕсли;
			КонецЦикла;
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры


