﻿#Область ПрограммныйИнтерфейс


// Получает тип визуализатора для формы, созданной в редакторе ШМД.
// Для формы с раскрывающимся полем (ВидЭлемента=ФорматированноеПоле) нужен xslt-визуализатор.
//
// Параметры:
//  ФормаСсылка	 - Справочники.ФормыРедактораШМД.Ссылка	 - Форма редатора ШМД
// 
// Возвращаемое значение:
//  Строка - "xslt"/"epf" и Неопределено в случае, когда тип визуализатора не важен.
//
Функция ПолучитьТипВизуализатора(ФормаСсылка) Экспорт
	
	Запрос_ = Новый Запрос(
		"ВЫБРАТЬ
		|	ФормыРедактораШМДЭлементыФормы.Ссылка
		|ИЗ
		|	Справочник.ФормыРедактораШМД.ЭлементыФормы КАК ФормыРедактораШМДЭлементыФормы
		|ГДЕ
		|	ФормыРедактораШМДЭлементыФормы.Ссылка = &ФормаСсылка
		|	И ФормыРедактораШМДЭлементыФормы.ВидЭлемента = ""ФорматированноеПоле"""
	);
	Запрос_.УстановитьПараметр("ФормаСсылка", ФормаСсылка);
	Выборка_ = Запрос_.Выполнить().Выбрать();
	Если Выборка_.Следующий() Тогда
		Возврат "xslt";
	КонецЕсли;
	
	Возврат Неопределено;
КонецФункции

#КонецОбласти