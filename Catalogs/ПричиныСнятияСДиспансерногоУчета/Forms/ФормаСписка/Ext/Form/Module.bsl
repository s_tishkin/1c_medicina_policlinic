﻿#Область ПрограммныйИнтерфейс

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// Подсистема Архивирование
	АрхивированиеСервер.ФормаСпискаПриСозданииНаСервере(ЭтотОбъект, "Список");
	// конец Подсистема Архивирование
	
КонецПроцедуры

&НаКлиенте
Процедура ДобавитьИзКлассификатора(Команда) 
	Параметры_ = Новый Структура;
	Параметры_.Вставить("ИмяОбъекта", "ПричиныСнятияСДиспансерногоУчета"); 
	Параметры_.Вставить("ЗаголовокФормы", "Добавление причин снятия с диспансерного учета"); 
	ОткрытьФорму("ОбщаяФорма.ЗагрузкаНСИИзКлассификатора", Параметры_, Элементы.Список);
КонецПроцедуры

&НаКлиенте
Процедура ЗагрузитьССайтаМинздрава(Команда)
	Параметры_ = Новый Структура;
	Параметры_.Вставить("ИмяОбъекта", "ПричиныСнятияСДиспансерногоУчета"); 
	Параметры_.Вставить("ЗаголовокФормы", "Добавление причин снятия с диспансерного учета"); 
	Параметры_.Вставить("ЗагружатьИзИнтернета", "Истина");
	ОткрытьФорму("ОбщаяФорма.ЗагрузкаНСИИзКлассификатора", Параметры_, Элементы.Список);
КонецПроцедуры

#КонецОбласти

#Область Архивирование

// Подсистема Архивирование
&НаКлиенте
Процедура Подключаемый_ПросмотрАрхивныхЭлементов(Команда) Экспорт
	
	Архивирование.КомандаПросмотрАрхивныхЭлементов(ЭтотОбъект, "Список", Команда);
	
КонецПроцедуры
// конец Подсистема Архивирование


#КонецОбласти