﻿
#Область ПрограммныйИнтерфейс

#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

// Заполнить первоначальные данные.
//
Процедура ЗаполнитьПервоначальныеДанные() Экспорт
	
	Объект_ = Товар.ПолучитьОбъект();
	Объект_.Тип = Перечисления.ТипыНоменклатуры.Товар;
	ОбновлениеИнформационнойБазы.ЗаписатьДанные(Объект_);
	
	Объект_ = Услуга.ПолучитьОбъект();
	Объект_.Тип = Перечисления.ТипыНоменклатуры.Услуга;
	ОбновлениеИнформационнойБазы.ЗаписатьДанные(Объект_);
	
	Объект_ = ГотоваяЛекарственнаяФорма.ПолучитьОбъект();
	Объект_.Тип = Перечисления.ТипыНоменклатуры.Товар;
	ОбновлениеИнформационнойБазы.ЗаписатьДанные(Объект_);
	
	Объект_ = ИзготавливаемаяЛекарственнаяФорма.ПолучитьОбъект();
	Объект_.Тип = Перечисления.ТипыНоменклатуры.Товар;
	ОбновлениеИнформационнойБазы.ЗаписатьДанные(Объект_);

	Объект_ = МедицинскаяУслуга.ПолучитьОбъект();
	Объект_.Тип = Перечисления.ТипыНоменклатуры.Услуга;
	ОбновлениеИнформационнойБазы.ЗаписатьДанные(Объект_);

	Объект_ = СтандартМедицинскойПомощи.ПолучитьОбъект();
	Объект_.Тип = Перечисления.ТипыНоменклатуры.Услуга;
	ОбновлениеИнформационнойБазы.ЗаписатьДанные(Объект_);

	Объект_ = МедицинскаяПрограмма.ПолучитьОбъект();
	Объект_.Тип = Перечисления.ТипыНоменклатуры.Услуга;
	ОбновлениеИнформационнойБазы.ЗаписатьДанные(Объект_);
	
	Объект_ = Прием.ПолучитьОбъект();
	Объект_.Тип = Перечисления.ТипыНоменклатуры.Услуга;
	ОбновлениеИнформационнойБазы.ЗаписатьДанные(Объект_);
	
КонецПроцедуры

#КонецЕсли

#КонецОбласти