﻿#Область ПрограммныйИнтерфейс

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	Если НЕ Параметры.Отбор.Свойство("Владелец") Тогда
		Параметры.Отбор.Вставить("Владелец");
		Параметры.Отбор.Владелец = Пользователи.ТекущийПользователь();
		Если РольДоступна("ПолныеПрава") Тогда
			ЭтотОбъект.Элементы.Пользователь.Доступность = Истина;
		КонецЕсли;
	КонецЕсли;
	ЭтотОбъект.Пользователь = Параметры.Отбор.Владелец;
	ВосстановитьСостояниеКнопокДинПоля();
	ЗаполнитьДеревоЭлементов();
КонецПроцедуры

&НаКлиенте
Процедура ПользовательПриИзменении(Элемент)
	Если ЗначениеЗаполнено(ЭтотОбъект.Пользователь) Тогда
		ПослеЗакрытияФормыЭлемента();
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	ПрименитьФильтр();
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьДеревоЭлементов()
	Запрос_ = Новый Запрос(
		"ВЫБРАТЬ
		|	СправочникМКБ10Избранное.Ссылка,
		|	СправочникМКБ10Избранное.ПометкаУдаления,
		|	СправочникМКБ10Избранное.Владелец,
		|	СправочникМКБ10Избранное.Родитель,
		|	СправочникМКБ10Избранное.ЭтоГруппа,
		|	СправочникМКБ10Избранное.Код,
		|	СправочникМКБ10Избранное.Наименование,
		|	СправочникМКБ10Избранное.ЭлементМКБ10
		|ИЗ
		|	Справочник.МКБ10Избранное КАК СправочникМКБ10Избранное
		|ГДЕ 
		|	СправочникМКБ10Избранное.Владелец = &Пользователь
		|	И СправочникМКБ10Избранное.ПометкаУдаления = ЛОЖЬ
		|АВТОУПОРЯДОЧИВАНИЕ
		|ИТОГИ ПО Ссылка ИЕРАРХИЯ"
	);
	Запрос_.УстановитьПараметр("Пользователь", ЭтотОбъект.Пользователь);
	Результат_ = Запрос_.Выполнить().Выбрать(ОбходРезультатаЗапроса.ПоГруппировкамСИерархией);
	Дерево_ = ДеревоТекущихЭлементов.ПолучитьЭлементы();
	Корень_ = Дерево_.Добавить();
	Корень_.Наименование = "МКБ10Избранное";
	ЗаполнитьВеткуДерева(Результат_, Корень_.ПолучитьЭлементы(), Справочники.МКБ10Избранное.ПустаяСсылка());
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьВеткуДерева(ВыборкаРезультата, ВетвьДерева, ЗаписьДубль)
	Пока ВыборкаРезультата.Следующий() Цикл
		Если ЗаписьДубль <> ВыборкаРезультата.Ссылка Тогда
			
			НоваяВетвь_ = ВетвьДерева.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяВетвь_, ВыборкаРезультата);
			Если ВыборкаРезультата.ЭтоГруппа Тогда
				Если ВыборкаРезультата.ТипЗаписи() = ТипЗаписиЗапроса.ИтогПоИерархии Тогда
					ДочерняяВыборка = ВыборкаРезультата.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкамСИерархией, ВыборкаРезультата.Группировка());
				Иначе 
					ДочерняяВыборка = ВыборкаРезультата.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкамСИерархией);
				КонецЕсли;
				ЗаполнитьВеткуДерева(ДочерняяВыборка, НоваяВетвь_.ПолучитьЭлементы(), ВыборкаРезультата.Ссылка);
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

////////////////////////////////////////////
// Команды фомы

&НаКлиенте
Процедура УстановитьИерархию(Команда)
	КнопкаИерахия = Элементы.УстановитьИерархию;
	КнопкаИерахия.Пометка = НЕ КнопкаИерахия.Пометка;
	ЗапомнитьСостояниеКнопокДинПоля();
	ПрименитьФильтр();
КонецПроцедуры

&НаКлиенте
Процедура Выбрать(Команда)
	ВыделенныеСтроки = Элементы.ДинамическоеПолеМКБ.ВыделенныеСтроки;
	ТекущаяСтрокаИзбранного = ЭтотОбъект.Элементы.ДеревоТекущихЭлементов.ТекущиеДанные.ПолучитьИдентификатор();
	Если ВыделенныеСтроки.Количество() = 0 Тогда
		Возврат;
	ИначеЕсли ВыделенныеСтроки.Количество()=1 Тогда
		Строка = ВыделенныеСтроки.Получить(0);
		ДобавитьМКБ10вДерево(Строка,ТекущаяСтрокаИзбранного,ЭтотОбъект.Пользователь);
	Иначе	
		Для каждого Строка из ВыделенныеСтроки Цикл
			ДобавитьМКБ10вДерево(Строка,ТекущаяСтрокаИзбранного,ЭтотОбъект.Пользователь);
		КонецЦикла;
	КонецЕсли;
	Элементы.Список.Обновить();
КонецПроцедуры

//////////////////////////////////////////
// Процедуры выбора значения

&НаКлиенте
Процедура ДинамическоеПолеМКБ10Выбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	Если НЕ Поле.Имя = "ДинамическоеПолеМКБЛупа" Тогда
		Если Не МКБ10ЭтоГруппа(ВыбраннаяСтрока) Тогда
			СтандартнаяОбработка = Ложь;
			ТекущаяСтрокаИзбранного = ЭтотОбъект.Элементы.ДеревоТекущихЭлементов.ТекущиеДанные.ПолучитьИдентификатор();
			ДобавитьМКБ10вДерево(ВыбраннаяСтрока,ТекущаяСтрокаИзбранного,ЭтотОбъект.Пользователь);
		Иначе СтандартнаяОбработка = Ложь;
			ТекущаяСтрокаИзбранного = ЭтотОбъект.Элементы.ДеревоТекущихЭлементов.ТекущиеДанные.ПолучитьИдентификатор();
			ДобавитьИзбранноеПоИерархии(ВыбраннаяСтрока, ТекущаяСтрокаИзбранного, ЭтотОбъект.Пользователь);
		КонецЕсли;
		//СообщенияПользователю.Показать("МКБ10_НевозможноВыбатьГруппу", Новый Структура("Группа",ВыбраннаяСтрока));
	КонецЕсли;
	//Элементы.Список.Обновить();
КонецПроцедуры

///////////////////////////////////////////
// Поцедуры работы с фильтрами

&НаКлиенте
Процедура ФильтрКодАвтоПодбор(Элемент, Текст, ДанныеВыбора, Ожидание, СтандартнаяОбработка)
	ЭтотОбъект.ФильтрКод = Текст;
	СтандартнаяОбработка = Ложь;
	ПрименитьФильтр();
КонецПроцедуры

&НаКлиенте
Процедура ФильтрНаименованиеАвтоПодбор(Элемент, Текст, ДанныеВыбора, Ожидание, СтандартнаяОбработка)
	ЭтотОбъект.ФильтрНаименование = Текст;
	СтандартнаяОбработка = Ложь;
	ПрименитьФильтр();
КонецПроцедуры

&НаКлиенте
Процедура ФильтрКодПриИзменении(Элемент)
	ПрименитьФильтр();
КонецПроцедуры

&НаКлиенте
Процедура ФильтрНаименованиеПриИзменении(Элемент)
	ПрименитьФильтр();
КонецПроцедуры

///////////////////////////////////////////////////////////
/// Функции работы с запросами

&НаКлиенте
Процедура ПрименитьФильтр()
	ТекстЗапроса = ПолучитьЗапрос();
	ТекстУсловия = ПолучитьУсловиеЗапроса();
	ПараметрыЗапросаСтруктуой = ПолучитьПараметрыЗапросаСтруктурой();
	НовыйТекстЗапроса = СтрЗаменить(ТекстЗапроса,"&Условие",ТекстУсловия);
	УстановитьПараметрыЗапроса(НовыйТекстЗапроса,ПараметрыЗапросаСтруктуой);
КонецПроцедуры

&НаКлиенте
Функция ПолучитьЗапрос()
	ТекстЗапроса =
		"ВЫБРАТЬ
		|	МКБ10.Ссылка КАК Ссылка,
		|	МКБ10.ЭтоГруппа КАК ЭтоГруппа,
		|	МКБ10.Код КАК Код,
		|	МКБ10.Наименование КАК Наименование,
		|	ВЫБОР
		|		КОГДА МКБ10.ЭтоГруппа = ИСТИНА
		|			ТОГДА ЛОЖЬ
		|		ИНАЧЕ ИСТИНА
		|	КОНЕЦ КАК Лупа
		|ИЗ
		|	Справочник.МКБ10 КАК МКБ10
		|ГДЕ
		|	МКБ10.ПометкаУдаления = ЛОЖЬ &Условие"; 
	Возврат  ТекстЗапроса;
КонецФункции

&НаКлиенте
Функция ПолучитьУсловиеЗапроса()
	Условие = "";
	Если ЗначениеЗаполнено(ЭтотОбъект.ФильтрКод) Тогда
		Условие = Условие + " И МКБ10.Код ПОДОБНО &Код";
	КонецЕсли;
	Если ЗначениеЗаполнено(ЭтотОбъект.ФильтрНаименование) Тогда
		Условие = Условие + " И МКБ10.Наименование ПОДОБНО &Наименование";	
	КонецЕсли;
	Если Элементы.УстановитьИерархию.Пометка = Ложь Тогда
		Условие = Условие + " И МКБ10.ЭтоГруппа = Ложь";	
	КонецЕсли;
	Возврат Условие;
КонецФункции

&НаКлиенте
Функция ПолучитьПараметрыЗапросаСтруктурой()
	ПараметыЗапроса = Новый Структура();
	Если ЗначениеЗаполнено(ЭтотОбъект.ФильтрКод) Тогда
		ПараметыЗапроса.Вставить("Код","%"+Строка(ЭтотОбъект.ФильтрКод)+"%");
	КонецЕсли;
	Если ЗначениеЗаполнено(ЭтотОбъект.ФильтрНаименование) Тогда
		ПараметыЗапроса.Вставить("Наименование","%"+Строка(ЭтотОбъект.ФильтрНаименование)+"%");
	КонецЕсли;
	Возврат ПараметыЗапроса;
КонецФункции

&НаСервере
// устанавливает для запроса переданые параметры
Процедура УстановитьПараметрыЗапроса(Запрос,ПараметрыЗапроса)
	КнопкаИерахия = Элементы.УстановитьИерархию;
	СписокМКБ10 = ЭтотОбъект.ДинамическоеПолеМКБ10;
	СписокМКБ10Элемент = ЭтотОбъект.Элементы.ДинамическоеПолеМКБ;
	СписокМКБ10.ТекстЗапроса = ДинамическоеПолеФормы.ПроверитьЗапрос(Запрос);
	// устанавливаем параметры запроса
	Для Каждого Парам из  ПараметрыЗапроса Цикл
		СписокМКБ10.Параметры.УстановитьЗначениеПараметра(Парам.Ключ,Парам.Значение);
	КонецЦикла;
	/// устанавливаем отобажение списка
	Если КнопкаИерахия.Пометка = Истина И НЕ СписокМКБ10Элемент.Отображение=ОтображениеТаблицы.Дерево Тогда 
		СписокМКБ10Элемент.Отображение=ОтображениеТаблицы.Дерево;
	ИначеЕсли КнопкаИерахия.Пометка = Ложь И НЕ СписокМКБ10Элемент.Отображение=ОтображениеТаблицы.Список Тогда 
		СписокМКБ10Элемент.Отображение=ОтображениеТаблицы.Список;
	КонецЕсли;	
КонецПроцедуры

////////////////////////////////////////////////////////
/// Вспомогательные функции

&НаСервере
Процедура ДобавитьМКБ10вДерево(СсылкаМКБ10,СсылкаМКБ10Избранное = Неопределено, Владелец = Неопределено)
	Если Владелец = Неопределено Тогда
		Владелец = ПользователиКлиентСервер.ТекущийПользователь();
	КонецЕсли;
	ЭлементИзбранного_ = ДеревоТекущихЭлементов.НайтиПоИдентификатору(СсылкаМКБ10Избранное);
	Если ЭлементИзбранного_.ЭтоГруппа Или Не ЗначениеЗаполнено(ЭлементИзбранного_.ЭлементМКБ10) Тогда
		РодительДерева_ = ЭлементИзбранного_;
	Иначе
		РодительДерева_ = ЭлементИзбранного_.ПолучитьРодителя();
	КонецЕсли;
	ЭлементыДерева_ = РодительДерева_.ПолучитьЭлементы();
	НовыйЭлемент_ = ЭлементыДерева_.Добавить();
	ЗаполнитьЗначенияСвойств(НовыйЭлемент_,СсылкаМКБ10,,"Ссылка");
	НовыйЭлемент_.ЭлементМКБ10 = СсылкаМКБ10;
	НовыйЭлемент_.Владелец = Владелец;
	НовыйЭлемент_.Родитель = РодительДерева_.Ссылка;
КонецПроцедуры

////
 // Процедура: ДобавитьМКБ10вИзбранное
 //   Добавляет МКБ10 в справочник МКБ10Избанное
 //
 // Параметры:
 //		СсылкаМКБ10
 //			ссылка на справочник МКБ10
 //		СсылкаМКБ10Избранное
 //			ссылка на справочник МКБ10. ТекущиеДанные списка
 //		Владелец
 //			ссылка на Пользователя, необходима для записи элемента справочника.
 ///
&НаСервереБезКонтекста
Процедура ДобавитьМКБ10вИзбранное(СсылкаМКБ10,СсылкаМКБ10Избранное = Неопределено,Владелец = Неопределено) Экспорт
	Если Владелец = Неопределено Тогда
		Владелец = Пользователи.ТекущийПользователь();
	КонецЕсли;
	Если СсылкаМКБ10.ЭтоГруппа = Истина Тогда
		СообщенияПользователю.Показать("МКБ10_НевозможноВыбатьГруппу", Новый Структура("Группа",СсылкаМКБ10));
	Иначе
		НайденныйЭлемент = Справочники.МКБ10Избранное.НайтиПоРеквизиту("ЭлементМКБ10",СсылкаМКБ10,,Владелец);
		Если ЗначениеЗаполнено(НайденныйЭлемент) Тогда
			СообщенияПользователю.Показать("МКБ10_ЭлементУжеИмеетсяВСписке", Новый Структура("Элемент",СсылкаМКБ10));
		Иначе
			Родитель = Неопределено;
			Если НЕ СсылкаМКБ10Избранное = Неопределено Тогда
				Если СсылкаМКБ10Избранное.ЭтоГруппа = Истина Тогда
					Родитель = СсылкаМКБ10Избранное;
				ИначеЕсли ЗначениеЗаполнено(СсылкаМКБ10Избранное.Родитель) Тогда
					Родитель = СсылкаМКБ10Избранное.Родитель;
				КонецЕсли;
			КонецЕсли;
			НовыйОбъект = Справочники.МКБ10Избранное.СоздатьЭлемент();
			НовыйОбъект.ЭлементМКБ10 = СсылкаМКБ10;
			НовыйОбъект.Владелец = Владелец;
			НовыйОбъект.Родитель = Родитель;			
			НовыйОбъект.Записать();
		КонецЕсли;
	КонецЕсли;	
КонецПроцедуры

// возвращает свойство ЭтоГруппа для элемента справочника МКБ10
&НаСервереБезКонтекста
Функция МКБ10ЭтоГруппа(СсылкаМКБ10)
	  Возврат СсылкаМКБ10.ЭтоГруппа;
КонецФункции

&НаСервере
Процедура ДобавитьИзбранноеПоИерархии(ВыбранноеЗначение, СтрокаИзбранного, Владелец = Неопределено)
	Если Владелец = Неопределено Тогда
		Владелец = ПользователиКлиентСервер.ТекущийПользователь();
	КонецЕсли;
	ЭлементИзбранного_ = ДеревоТекущихЭлементов.НайтиПоИдентификатору(СтрокаИзбранного);
	Если ЭлементИзбранного_.ЭтоГруппа Или Не ЗначениеЗаполнено(ЭлементИзбранного_.ЭлементМКБ10) Тогда
		РодительДерева_ = ЭлементИзбранного_;
	Иначе
		РодительДерева_ = ЭлементИзбранного_.ПолучитьРодителя();
	КонецЕсли;
	ЭлементыДерева_ = РодительДерева_.ПолучитьЭлементы();
	Запрос_ = Новый Запрос(
		"ВЫБРАТЬ
		|	СправочникМКБ10.Ссылка,
		|	СправочникМКБ10.ПометкаУдаления,
		|	СправочникМКБ10.Родитель,
		|	СправочникМКБ10.ЭтоГруппа,
		|	СправочникМКБ10.Код,
		|	СправочникМКБ10.Наименование
		|ИЗ
		|	Справочник.МКБ10 КАК СправочникМКБ10
		|ГДЕ 
		|	СправочникМКБ10.Ссылка В ИЕРАРХИИ (&СсылкаМКБ)
		|	И СправочникМКБ10.ПометкаУдаления = ЛОЖЬ
		|АВТОУПОРЯДОЧИВАНИЕ"
	);
	Запрос_.УстановитьПараметр("СсылкаМКБ", ВыбранноеЗначение);
	Результат_ = Запрос_.Выполнить().Выбрать(ОбходРезультатаЗапроса.ПоГруппировкамСИерархией);
	ДобавитьВеткуДерева(Результат_, ЭлементыДерева_, Справочники.МКБ10.ПустаяСсылка(), Владелец);
	
КонецПроцедуры

&НаСервере
Процедура ДобавитьВеткуДерева(ВыборкаРезультата, ВетвьДерева, ЗаписьДубль, Владелец)
	Пока ВыборкаРезультата.Следующий() Цикл
		Если ЗаписьДубль <> ВыборкаРезультата.Ссылка Тогда
			
			НоваяВетвь_ = ВетвьДерева.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяВетвь_, ВыборкаРезультата,,"Ссылка");
			НоваяВетвь_.Владелец = Владелец;
			Если Не ВыборкаРезультата.ЭтоГруппа Тогда
				НоваяВетвь_.ЭлементМКБ10 = ВыборкаРезультата.Ссылка;
			Иначе
				Если ВыборкаРезультата.ТипЗаписи() = ТипЗаписиЗапроса.ИтогПоИерархии Тогда
					ДочерняяВыборка = ВыборкаРезультата.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкамСИерархией, ВыборкаРезультата.Группировка());
				Иначе 
					ДочерняяВыборка = ВыборкаРезультата.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкамСИерархией);
				КонецЕсли;
				ДобавитьВеткуДерева(ДочерняяВыборка, НоваяВетвь_.ПолучитьЭлементы(), ВыборкаРезультата.Ссылка, Владелец);
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

////
 // Процедура: ЗапомнитьСостояниеКнопокДинПоля
 //   Интефейсная функция, предназначения для облегчения работы сервера.
 //   Заменили обну серверную процедуру на клиентскую и серверную без контекста.
 //   Необходимо из за задержек в пеедачи контекста между клиентом и сервером.
 ///
&НаКлиенте
Процедура ЗапомнитьСостояниеКнопокДинПоля() Экспорт
	Настройки = Новый Структура;	
	КнопкаУИ = Элементы.УстановитьИерархию;
	Настройки.Вставить("УстановитьИерархию", КнопкаУИ.Пометка);	
	ЗапомнитьСостояниеКнопокДинПоля_Сервер(Настройки);
КонецПроцедуры

////
 // Процедура: ЗапомнитьСостояниеКнопокДинПоля
 //   Предназначена для сохранения состояний кнопок в хранилище общих настроек
 //
 // Параметры:
 //   СтруктуаНастроек
 //     Структура с именем и значением настройки
 ///
&НаСервереБезКонтекста
Процедура ЗапомнитьСостояниеКнопокДинПоля_Сервер(СтуктураНастроек) Экспорт
	ХранилищеОбщихНастроек.Сохранить(
		"МКБ10Избранное",
		,
		СтуктураНастроек
	);
КонецПроцедуры

////
 // Процедуа: ВосстановитьСостояниеКнопокДинПоля
 //   Предназначена для восстановления состояний кнопок из хранилища общих настроек
 ///
&НаСервере
Процедура ВосстановитьСостояниеКнопокДинПоля() Экспорт
	Настройки = ХранилищеОбщихНастроек.Загрузить("МКБ10Избранное");
	// Если удалось прочитать настройки...
	Если НЕ Настройки = Неопределено Тогда
		Для Каждого Настройка Из Настройки Цикл
			Попытка
				КнопкаФормы = Элементы[Настройка.Ключ];
				КнопкаФормы.Пометка = Настройка.Значение;
				// В зависимости от кнопки и ее состояния нужно выполнить доп действия элемент без группы.
			Исключение
			КонецПопытки;
		КонецЦикла;
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ЗаписатьНаСервере()
	ЭлементыДерева_ = ДеревоТекущихЭлементов.ПолучитьЭлементы();
	ЗаписатьНовыеЭлементыДерева(ЭлементыДерева_, Справочники.МКБ10Избранное.ПустаяСсылка());
КонецПроцедуры

&НаКлиенте
Процедура Записать(Команда)
	ПометитьУдаляемыеЭлементы();
	ЗаписатьНаСервере();
	ДеревоТекущихЭлементов.ПолучитьЭлементы().Очистить();
	ЗаполнитьДеревоЭлементов();
КонецПроцедуры

&НаСервере
Процедура ПометитьУдаляемыеЭлементы()
	Для Каждого Элемент_ Из СписокУдаляемых Цикл
		Объект_ = Элемент_.Значение.ПолучитьОбъект();
		Объект_.УстановитьПометкуУдаления(Истина, Истина);
		Объект_.Записать();
	КонецЦикла;
КонецПроцедуры

&НаСервере
Процедура ЗаписатьНовыеЭлементыДерева(ЭлементыДерева, СсылкаРодитель)
	Для Каждого Элемент_ Из ЭлементыДерева Цикл
		Если Не ЗначениеЗаполнено(Элемент_.Ссылка)
			И ЗначениеЗаполнено(Элемент_.Наименование)
			И ЗначениеЗаполнено(Элемент_.Код)
		Тогда
			Если Элемент_.ЭтоГруппа Тогда
				НовыйЭлСправочника_ = Справочники.МКБ10Избранное.СоздатьГруппу();
				НовыйЭлСправочника_.ИспользоватьВКоманднойПанеле = Элемент_.ИспользоватьВКоманднойПанеле;
			Иначе
				НовыйЭлСправочника_ = Справочники.МКБ10Избранное.СоздатьЭлемент();
				НовыйЭлСправочника_.ЭлементМКБ10 = Элемент_.ЭлементМКБ10;
			КонецЕсли;
			ЗаполнитьЗначенияСвойств(НовыйЭлСправочника_, Элемент_,,"Ссылка, ЭлементМКБ10, ИспользоватьВКоманднойПанеле");
			Если Не ЗначениеЗаполнено(НовыйЭлСправочника_.Родитель) Тогда
				НовыйЭлСправочника_.Родитель = СсылкаРодитель;
			КонецЕсли;
			НовыйЭлСправочника_.УстановитьСсылкуНового(Справочники.МКБ10Избранное.ПолучитьСсылку(Новый УникальныйИдентификатор));
			НоваяССылка_ = НовыйЭлСправочника_.ПолучитьСсылкуНового();
			НовыйЭлСправочника_.Записать();
		КонецЕсли;
		ЗаписатьНовыеЭлементыДерева(Элемент_.ПолучитьЭлементы(), НоваяССылка_);
	КонецЦикла;
КонецПроцедуры

&НаКлиенте
Процедура ДеревоТекущихЭлементовПередУдалением(Элемент, Отказ)
	ТекущиеДанные_ = Элементы.ДеревоТекущихЭлементов.ТекущиеДанные;
	Если ЗначениеЗаполнено(ТекущиеДанные_.Ссылка) Тогда
		СписокУдаляемых.Добавить(ТекущиеДанные_.Ссылка);
	Иначе
		Если ТекущиеДанные_.Наименование = "МКБ10Избранное" Тогда
			Отказ = Истина;
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ДеревоТекущихЭлементовВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	ТекущиеДанные_ = Элементы.ДеревоТекущихЭлементов.ТекущиеДанные;
	Если ТекущиеДанные_ <> Неопределено Тогда
		Если ЗначениеЗаполнено(ТекущиеДанные_.Ссылка) Тогда
			ПоказатьЗначение(Новый ОписаниеОповещения(
						"ПослеЗакрытияФормыЭлемента", 
						ЭтотОбъект
					),
				ТекущиеДанные_.Ссылка
			);
		Иначе
			СообщенияПользователюКлиент.ВывестиПредупреждение("МКБ10_ЭлементыНеЗаписаны");
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗакрытияФормыЭлемента(РезультатОткрытия = Неопределено, ДополнительныйПараметр = Неопределено) Экспорт
	ДеревоТекущихЭлементов.ПолучитьЭлементы().Очистить();
	ЗаполнитьДеревоЭлементов();
КонецПроцедуры

&НаКлиенте
Процедура УстановитьПризнакПрофиля(Команда)
	ТекущиеДанные_ = Элементы.ДеревоТекущихЭлементов.ТекущиеДанные;
	Если ТекущиеДанные_ <> Неопределено Тогда
		Если ТекущиеДанные_.ЭтоГруппа Тогда
			Если ЗначениеЗаполнено(ТекущиеДанные_.Ссылка) Тогда
				УстановитьПризнакПрофиляСервер(ТекущиеДанные_.Ссылка);
			Иначе
				ТекущиеДанные_.ИспользоватьВКоманднойПанеле = Не ТекущиеДанные_.ИспользоватьВКоманднойПанеле;
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура УстановитьПризнакПрофиляСервер(Ссылка)
	Объект_ = Ссылка.ПолучитьОбъект();
	Объект_.ИспользоватьВКоманднойПанеле = Не Объект_.ИспользоватьВКоманднойПанеле;
	Объект_.Записать();
КонецПроцедуры

#КонецОбласти