﻿
#Область ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

// Процедура - обработчик события "ПриСозданииНаСервере" формы.
//
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Макет = Справочники.ПрочиеОбмены_УзлыОбмена.ПолучитьМакет("ОписаниеВыгрузкиСебестоимости");
	ОписаниеВыгрузки = Макет.ПолучитьТекст();
	
КонецПроцедуры // ПриСозданииНаСервере()


#КонецОбласти