﻿
#Область ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

// Процедура - обработчик события "ПриСозданииНаСервере" формы.
//
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	СоздатьКомандыЭлементыДополнительныхРегистров();
	
КонецПроцедуры // ПриСозданииНаСервере()

// Процедура - обработчик события "ПриОткрытии" формы.
//
&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	УстановитьВидимость();
	
КонецПроцедуры  // ПриОткрытии()

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ

// Процедура открывает форму дополнительного регистра
//
&НаКлиенте
Процедура Подключаемый_ОткрытьФормуДопРегистра(Команда)
	
	ТекЗначение = Объект.Ссылка;
	Если ЗначениеЗаполнено(ТекЗначение) Тогда
		
		ПараметрыФормыРегистра = Новый Структура;
		ПараметрыФормыРегистра.Вставить("Отбор",               Новый Структура("УзелОбмена", ТекЗначение));
		ПараметрыФормыРегистра.Вставить("ЗначенияЗаполнения",  Новый Структура("УзелОбмена", ТекЗначение));
		
		// открываем форму записи РС
		
		МассивСтрок = ТаблицаДополнительныхРегистров.НайтиСтроки(Новый Структура("ИмяКоманды", Команда.Имя));
		Если МассивСтрок.Количество() > 0 Тогда
			
			ИмяФормыРегистра = "РегистрСведений." + МассивСтрок[0].ИмяРегистра + ".ФормаСписка";
			
			ОткрытьФорму(ИмяФормыРегистра, ПараметрыФормыРегистра, ЭтотОбъект);
			
		КонецЕсли;	
	КонецЕсли;
	
КонецПроцедуры // Подключаемый_ОткрытьФормуДопРегистра()

// Процедура - обработчик команды "УдалитьНастройки".
//
&НаКлиенте
Процедура УдалитьНастройки(Команда)
	
	Если Не Объект.Ссылка.Пустая() Тогда
		СтрокаВопрос = "Удалить настройки? Этот процесс необратимый";
		ОповещенияОтвета = Новый ОписаниеОповещения("ПослеОтветаНаВопросУдаленияНастроек", ЭтотОбъект);
		ПоказатьВопрос(ОповещенияОтвета, СтрокаВопрос, РежимДиалогаВопрос.ДаНетОтмена, 60);
	КонецЕсли;
	
КонецПроцедуры // УдалитьНастройки()

&НаКлиенте
Процедура ПослеОтветаНаВопросУдаленияНастроек(Ответ, ДополнительныеПараметры) Экспорт
	
	Если Ответ = КодВозвратаДиалога.Да Тогда
		УдалитьНастройкиНаСервере(Объект.Ссылка);
	КонецЕсли;
	
КонецПроцедуры // ПослеОтветаНаВопросУдаленияНастроек()



// Процедура - обработчик команды "ОткрытьОписаниеВыгрузкиСебестоимости".
//
&НаКлиенте
Процедура ОткрытьОписаниеВыгрузкиСебестоимости(Команда)
	
	ОткрытьФорму("Справочник.ПрочиеОбмены_УзлыОбмена.Форма.ФормаОписанияВыгрузкиСебестоимости",, ЭтотОбъект);
	
КонецПроцедуры // ОткрытьОписаниеВыгрузкиСебестоимости()

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ПОЛЕЙ ФОРМЫ

// Процедура - обработчик события ПриИзменении элемента "СуммовойУчетДвижений".
//
&НаКлиенте
Процедура СуммовойУчетДвиженийТМЦПриИзменении(Элемент)
	
	УстановитьВидимость();
	
КонецПроцедуры // СуммовойУчетДвиженийТМЦПриИзменении() 

// Процедура - обработчик события "ПриИзменении" поля "ТипКонфигурацииПриемника".
//
&НаКлиенте
Процедура ТипКонфигурацииПриемникаПриИзменении(Элемент)
	
	УстановитьВидимость();

КонецПроцедуры // ТипКонфигурацииПриемникаПриИзменении()

////////////////////////////////////////////////////////////////////////////////
// ООБЩИЕ ПРОЦЕДУРЫ И ФУНКЦИИ (НА СЕРВЕРЕ)

// Процедура удаляет данные доп регистров
//
&НаСервереБезКонтекста
Процедура УдалитьНастройкиНаСервере(Ссылка)
	
	// справочники
	ТекстЗапроса = 
	"ВЫБРАТЬ
	|	Справ.Ссылка
	|ИЗ
	|	Справочник.ПрочиеОбмены_ОбъектыВнешнихПрограмм КАК Справ
	|ГДЕ
	|	Справ.Владелец = &Владелец";
	
	СтруктураОбхода = Новый Структура;
	СтруктураОбхода.Вставить("ПрочиеОбмены_ОбъектыВнешнихПрограмм");
	
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Владелец", Ссылка);
	
	Для Каждого СтрокаОбхода из СтруктураОбхода Цикл
		Запрос.Текст = СтрЗаменить(ТекстЗапроса, "ПрочиеОбмены_ОбъектыВнешнихПрограмм", СтрокаОбхода.Ключ);
		
		Выборка = Запрос.Выполнить().Выбрать();
		Пока Выборка.Следующий() Цикл
			ОбъектВыборки = Выборка.Ссылка.ПолучитьОбъект();
			Попытка
				ОбъектВыборки.УстановитьПометкуУдаления(Истина);
			Исключение
			КонецПопытки;
		КонецЦикла;
	КонецЦикла;
	
	// регистры
	СтруктураВсехИменРегистров = Справочники.ПрочиеОбмены_УзлыОбмена.ПолучитьИменаДополнительныхРегистров();
	СтруктураОбхода = Новый Структура;
	Для Каждого СтрокаОбхода из СтруктураВсехИменРегистров Цикл
	    СтруктураОбхода.Вставить(СтрокаОбхода.Ключ, "УзелОбмена");
	КонецЦикла;	
	СтруктураОбхода.Вставить("ПрочиеОбмены_НастройкиТранспортаОбмена", "Узел");
	СтруктураОбхода.Вставить("ПрочиеОбмены_ПравилаДляОбменаДанными", "УзелОбмена");
	
	Для каждого СтрокаОбхода Из СтруктураОбхода Цикл
		
		Набор = РегистрыСведений[СтрокаОбхода.Ключ].СоздатьНаборЗаписей();
		Набор.Отбор[СтрокаОбхода.Значение].Установить(Ссылка, Истина);
		Набор.Прочитать();
		Набор.Очистить();
		Набор.Записать();
		
	КонецЦикла;
		
КонецПроцедуры // УдалитьНастройкиНаСервере()

// функция проверяет доступность выгрузки себестоимости списания
// Возвращает: булево
&НаСервереБезКонтекста
Функция ПолучитьДоступностьДополнительныхРеквизитов(ТекТипПриемника) 
	
	Ответ = Справочники.ПрочиеОбмены_УзлыОбмена.ПолучитьДоступныеДополнительныеРеквизиты(ТекТипПриемника);
	
	Возврат Ответ;
	
КонецФункции // ПолучитьДоступностьДополнительныхРеквизитов()

// Устанавливает видимость и доступность.
//
&НаКлиенте
Процедура УстановитьВидимость()
	
	ИзменитьСписокДопРегистров(Объект.ТипКонфигурацииПриемника);
	
	ДоступностьРеквизитов = ПолучитьДоступностьДополнительныхРеквизитов(Объект.ТипКонфигурацииПриемника);
	
	// общее отключение
	Для каждого ЭлементСтруктуры Из ДоступностьРеквизитов Цикл
		ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, 
			ЭлементСтруктуры.Ключ, 
			"Видимость", 
			ЭлементСтруктуры.Значение);
	КонецЦикла;	
	
	// уточнение видимости
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, 
		"ГруппаСебестоимость", 
		"Видимость", 
		ДоступностьРеквизитов.ВыгружатьСебестоимостьСписания);
		
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, 
		"ГруппаПредупреждение", 
		"Видимость", 
		ДоступностьРеквизитов.ВыгружатьСебестоимостьСписания Или ДоступностьРеквизитов.СуммовойУчетДвиженийТМЦ);
		
	ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, 
		"ЕдиницаИзмеренияПриСуммовомУчете", 
		"Видимость", 
		(ДоступностьРеквизитов.СуммовойУчетДвиженийТМЦ И
		ДоступностьРеквизитов.ЕдиницаИзмеренияПриСуммовомУчете И
		Объект.СуммовойУчетДвиженийТМЦ));
		
КонецПроцедуры // УстановитьВидимость()

////////////////////////////////////////////////////////////////////////////////
// КОМАНДЫ УПРАВЛЕНИЯ ДОПОЛНИТЕЛЬНЫМИ РЕГИСТРАМИ
//

// Процедура заполняет соответствие имен элементов и команд дополнительных регистров.
//
&НаСервере
Процедура СоздатьКомандыЭлементыДополнительныхРегистров()
	
	ТаблицаДополнительныхРегистров.Очистить();
	
	// получим полный список дополнительных регистров 
	СтруктураВсехИменРегистров = Справочники.ПрочиеОбмены_УзлыОбмена.ПолучитьИменаДополнительныхРегистров();
	
	// создадим группу-подменю в командной панели формы
	НовыйЭлементГруппа = Элементы.Добавить("ГруппаКнопокПерейти_ДопРегистры",
			Тип("ГруппаФормы"),
			ЭтотОбъект.КоманднаяПанель);
	НовыйЭлементГруппа.Вид = ВидГруппыФормы.Подменю;
	НовыйЭлементГруппа.Заголовок = "Перейти";
	
	// создадим все команды на форме
	Для каждого ИмяРегистра Из СтруктураВсехИменРегистров Цикл
		стрНовая = ТаблицаДополнительныхРегистров.Добавить();
		стрНовая.ИмяКоманды = ИмяРегистра.Ключ + "_КомандаОткрыть";
		стрНовая.ИмяЭлемента = ИмяРегистра.Ключ + "_КнопкаКомандаОткрыть";
		стрНовая.ИмяРегистра = ИмяРегистра.Ключ;
		
		// создаем кнопку
		НоваяКоманда = Команды.Добавить(стрНовая.ИмяКоманды);
		НоваяКоманда.Действие = "Подключаемый_ОткрытьФормуДопРегистра";
		
		НоваяКоманда.Заголовок = ИмяРегистра.Значение;
		НоваяКоманда.Подсказка = ИмяРегистра.Значение;
		
		НовыйЭлемент = Элементы.Добавить(стрНовая.ИмяЭлемента,
			Тип("КнопкаФормы"),
			НовыйЭлементГруппа);
			
		НовыйЭлемент.ИмяКоманды = НоваяКоманда.Имя;
		
	КонецЦикла;
	
КонецПроцедуры // СоздатьКомандыЭлементыДополнительныхРегистров()

// Функция возвращает структуру имен доступных дополнительных регистров
// и имен типовых команд открытия в формах справочника узлов.
//
&НаСервереБезКонтекста
Функция ПолучитьДоступныеДополнительныеРегистры(ТекущийТипКонфигурации)
	
	// Получим доступный список дополнительных регистров и имен команд в кнопке Перейти
	// подмножество полного списка.
	Ответ = Справочники.ПрочиеОбмены_УзлыОбмена.ПолучитьИменаДоступныхДополнительныхРегистров(ТекущийТипКонфигурации);
	
	Возврат Ответ;
	
КонецФункции // ПолучитьДоступныеДополнительныеРегистры()

// Процедура изменяет видимость списка дополнительных регистров
//
&НаКлиенте
Процедура ИзменитьСписокДопРегистров(ТекущийТипКонфигурации)
	
	// Получим доступный список дополнительных регистров и имен команд в кнопке Перейти
	// подмножество полного списка.
	МассивИменРегистров = ПолучитьДоступныеДополнительныеРегистры(ТекущийТипКонфигурации);

	Для каждого ЭлементКоманды Из ТаблицаДополнительныхРегистров Цикл
		
		Видим = Не (МассивИменРегистров.Найти(ЭлементКоманды.ИмяРегистра) = Неопределено);
		
		ОбщегоНазначенияКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, ЭлементКоманды.ИмяЭлемента, "Видимость", Видим);
		
	КонецЦикла;
	
КонецПроцедуры // ИзменитьСписокДопРегистров()




 









#КонецОбласти