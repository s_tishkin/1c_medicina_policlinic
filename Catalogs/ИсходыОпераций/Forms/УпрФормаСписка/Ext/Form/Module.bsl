﻿
#Область ПрограммныйИнтерфейс

////
 // Процедура: ДобавитьИзКлассификатора_Выполнить
 //   Обработчик нажатия кнопки "Добавить из классификатора".
 //   Открывает форму для выбора значений из классификатора.
 //
 // Параметры:
 //   Команда {КомандаФормы}
 //     Команда формы, связанная с кнопкой.
 ///
&НаКлиенте
Процедура ДобавитьИзКлассификатора(Команда)
	Параметры_ = Новый Структура;
	Параметры_.Вставить("ИмяОбъекта", "ИсходыОпераций"); 
	Параметры_.Вставить("ИмяМакета", "КлассификаторИсходыОпераций"); 
	Параметры_.Вставить("ЗаголовокФормы", "Добавление исходов операций в справочник"); 
	ОткрытьФорму("ОбщаяФорма.ЗагрузкаИзКлассификатора", Параметры_, Элементы.Список);
КонецПроцедуры


#КонецОбласти