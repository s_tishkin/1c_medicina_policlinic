﻿
#Область ПрограммныйИнтерфейс

///////////////////////////////////////////////////////////////////////////////////////
// События формы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	// Подсистема Медицинская НСИ
	ИнформационныйБлок.ИнициализироватьФорму(ЭтотОбъект, "ГруппаИнформацияИТС");

	ОперацииИТСМедицина = Новый Структура(
		"ОткрытьМКБ10,
		|ПринятьДанныеМКБ10,
		|НастройкиПользователя");
	ИТСМедицинаСервер.ИнициализироватьФорму(ЭтотОбъект, ОперацииИТСМедицина, "ФормаГруппаИТСМедицина", Истина);
	// Конец Подсистема Медицинская НСИ
КонецПроцедуры

///////////////////////////////////////////////////////////////////////////////////////
// Команды формы

&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуФормы(Команда)
	Если Команда.Имя = "ИТСМедицинаПринятьДанныеМКБ10" Тогда 
		Элементы.ГруппаИнформацияИТС.Видимость = Истина;
	ИначеЕсли Команда.Имя = "ИТСМедицинаПрерватьПолучениеДанных" Тогда
		Элементы.ГруппаИнформацияИТС.Видимость = Ложь;
	КонецЕсли;
	
	ВыполнитьКомандуФормы(Команда.Имя);
	
КонецПроцедуры

&НаКлиенте
Процедура Подключаемый_ИТСМедицинаПриемДанных()
	// обработчик ожидания
	ИТСМедицинаКлиент.ПринятьДанные(ЭтотОбъект);

КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ВСПОМОГАТЕЛЬНЫЕ ПРОЦЕДУРЫ ОБРАБОТКИ КОМАНД И СОБЫТИЙ ФОРМЫ

&НаКлиенте
Процедура ВыполнитьКомандуФормы(ИмяКоманды)
	
	НуженВызовСервера = Ложь;
	РаботаСУправляемойФормойКлиент.ВыполнитьКомандуФормы(ЭтотОбъект, ИмяКоманды, НуженВызовСервера);
	Если НуженВызовСервера Тогда
		ВыполнитьКомандуФормыНаСервере(ИмяКоманды);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ВыполнитьКомандуФормыНаСервере(ИмяКоманды)
	РаботаСУправляемойФормой.ВыполнитьКомандуФормы(ЭтотОбъект, ИмяКоманды);
КонецПроцедуры



#КонецОбласти