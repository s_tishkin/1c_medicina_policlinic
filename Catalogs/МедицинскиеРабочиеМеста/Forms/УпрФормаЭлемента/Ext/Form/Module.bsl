﻿
#Область ПрограммныйИнтерфейс

//////////////////////////////////////////////////
//// События формы

////
 // Процедура: ПриСозданииНаСервере
 //   обработчик события формы ПриСозданииНаСервере.
 //
 // Параметры:
 //   Отказ
 //     устанавливается в значение Истина для отмены открытия формы
 //   СтандартнаяОбработка
 //     устанавливается в значение Ложь для отмены вызова стандартной обработки события.
 ///
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	ЭтотОбъект.СпособПланированияВЗаказе = Перечисления.СпособыПланированияРабочегоМеста.ВЗаказе;
	ЭтотОбъект.СпособПланированияВСетке = Перечисления.СпособыПланированияРабочегоМеста.ВСетке;
	
	ОМС = ПолучитьФункциональнуюОпцию("ИспользоватьОМС");
	ПЛТ = ПолучитьФункциональнуюОпцию("ИспользоватьПЛТ");
	РаботаПоУчасткам = ПолучитьФункциональнуюОпцию("РаботаПоУчасткам");
	
	ЭтотОбъект.Элементы.Участок.Видимость = РаботаПоУчасткам;
	ЭтотОбъект.ИспользуетсяФункцияВрачебнойДолжности = ОМС и ПЛТ;
	
	Если Объект.ФункцияВрачебнойДолжности <= 1 Тогда
		ФункцияВрачебнойДолжности=Объект.ФункцияВрачебнойДолжности*100;
	КонецЕсли;
	
	ЗаполнитьПоляВрачИСМППоТЧИсполнители();
	ЭтотОбъект.МедицинскийКабинет = МедицинскиеРабочиеМеста.ПолучитьКабинетПоУмолчанию(Объект.Ссылка);
	ЭтотОбъект.МедицинскийКабинетАктуален = ПолучитьАктуальностьКабинета(ЭтотОбъект.МедицинскийКабинет);
	Элементы.НомерКабинета.Видимость = 
		Не ПолучитьФункциональнуюОпцию("ИспользоватьРаспределениеВрачейПоКабинетам");
	
	ОформленияВидовОбъектов.НастроитьУсловноеОформление(ЭтотОбъект, "МедицинскийКабинетАктуален", "НомерКабинета", "Архивирование", "Архивирование_АрхивныйЭлементВПолеВвода");
	
	// Подсистема Архивирование
	АрхивированиеСервер.ФормаЭлементаПриСозданииНаСервере(ЭтотОбъект, Объект[Архивирование.ИмяРеквизита()], "Архивирование_Статус");
	// конец Подсистема Архивирование
		
	ЭтотОбъект.ШагВремениВыполнения = ПланированиеУслуг.РазмерТалонаВСекундах() / 60;
	ЭтотОбъект.ВремяВыполнения = (Объект.ВремяВыполнения - ОбщиеМеханизмыКлиентСервер.ПустаяДата()) / 60;
	
	Врач_ = ПредопределенноеЗначение("Перечисление.ВидыМедицинскихДолжностей.Врачи");
	ЭтотОбъект.ДолжностиВрачей.ЗагрузитьЗначения(ПолучитьДолжности(Врач_));
	СМП_ = ПредопределенноеЗначение("Перечисление.ВидыМедицинскихДолжностей.СреднийМедицинскийПерсонал");
	ЭтотОбъект.ДолжностиСМП.ЗагрузитьЗначения(ПолучитьДолжности(СМП_));
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	ОтобразитьРежимРедактированияСотрудников();
	УстановитьВидимостьПолейСпособПланирования();
	
	// Подсистема Архивирование
	Архивирование.УстановитьВидимостьЭлементовАрхивности(ЭтотОбъект, Объект[Архивирование.ИмяРеквизита()]);
	// конец Подсистема Архивирование
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗаписью(Отказ, ПараметрыЗаписи)
	Если ЗначениеЗаполнено(ФункцияВрачебнойДолжности) ИЛИ ФункцияВрачебнойДолжности = 0 Тогда
		Объект.ФункцияВрачебнойДолжности=ФункцияВрачебнойДолжности/100;
	КонецЕсли;
	Объект.ВремяВыполнения = ОбщиеМеханизмыКлиентСервер.ПустаяДата() + ЭтотОбъект.ВремяВыполнения * 60;
КонецПроцедуры

&НаСервере
Процедура ПриЗаписиНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	Если ЭтотОбъект.МедицинскийКабинетИзменился = Истина Тогда
		ОК_ = МедицинскиеРабочиеМеста.УстановитьКабинетПоУмолчанию(
			ТекущийОбъект.Ссылка, ЭтотОбъект.МедицинскийКабинет
		);
		Если ОК_ = Истина Тогда
			ЭтотОбъект.МедицинскийКабинетИзменился = Ложь;
		Иначе
			Отказ = Истина;
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	Если Объект.ФункцияВрачебнойДолжности<=1 Тогда
		ФункцияВрачебнойДолжности=Объект.ФункцияВрачебнойДолжности*100;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	// Подсистема Архивирование
	Если ИмяСобытия = Архивирование.ИмяОповещения() Тогда
		Если ТипЗнч(Параметр) = Тип("Массив") Тогда
			Поиск_ = Параметр.Найти(Объект.Ссылка);
			Если Поиск_ <> Неопределено Тогда
				Архивирование.УстановитьВидимостьЭлементовАрхивности(ЭтотОбъект, Объект[Архивирование.ИмяРеквизита()]);
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;
	// конец Подсистема Архивирование
	
КонецПроцедуры

////////////////////////////////////////////////////
/// События элементов формы

&НаКлиенте
Процедура ИспользоватьРасширенныйРежимРедактированияСотрудниковПриИзменении(Элемент)
	ОтобразитьРежимРедактированияСотрудников();
КонецПроцедуры

&НаКлиенте
Процедура СпособПланированияПриИзменении(Элемент)
	Объект.ПечататьНомерОчереди = Ложь;
	Объект.Вместимость = 1;
	ЭтотОбъект.ФункцияВрачебнойДолжности = 0;
	ЭтотОбъект.ВремяВыполнения = 0;
	УстановитьВидимостьПолейСпособПланирования();
КонецПроцедуры

&НаКлиенте
Процедура НомерКабинетаПриИзменении(Элемент)
	ЭтотОбъект.МедицинскийКабинетАктуален = ПолучитьАктуальностьКабинета(ЭтотОбъект.МедицинскийКабинет);
	ЭтотОбъект.МедицинскийКабинетИзменился = Истина;
КонецПроцедуры

&НаСервере
Функция ПолучитьДолжности(Вид)
	Запрос_ = Новый Запрос(
		"ВЫБРАТЬ
		|	ДолжностиОрганизаций.Ссылка
		|ИЗ
		|	Справочник.ДолжностиОрганизаций КАК ДолжностиОрганизаций
		|ГДЕ
		|	ДолжностиОрганизаций.ВидМедицинскойДолжности = &Вид"
	);
	Запрос_.УстановитьПараметр("Вид", Вид);
	Результат_ = Запрос_.Выполнить();
	Возврат ?(Результат_.Пустой(), Новый Массив, Результат_.Выгрузить().ВыгрузитьКолонку("Ссылка"));
КонецФункции

&НаКлиенте
Процедура ВрачОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	ОбработатьВыборПодразделения(ЭтотОбъект.Врач, ВыбранноеЗначение, Объект.Подразделение);
	Врачи_ = Объект.Исполнители.НайтиСтроки(Новый Структура("Сотрудник", ЭтотОбъект.Врач));
	Если Врачи_.Количество() = 0 Тогда
		Для каждого Исполнитель_ Из Объект.Исполнители Цикл
			Исполнитель_.ОтветственныйЗаНазначение = Ложь;
		КонецЦикла;
		Врач_ = Объект.Исполнители.Добавить();
		Врач_.Сотрудник = ВыбранноеЗначение;
		Врач_.ОтветственныйЗаНазначение = Истина;
	Иначе
		Врачи_[0].Сотрудник = ВыбранноеЗначение;
	КонецЕсли;
	ОбновитьНаименование(ЭтотОбъект.Врач, ВыбранноеЗначение);
	ЭтотОбъект.Врач = ВыбранноеЗначение;
КонецПроцедуры

 &НаКлиенте
Процедура ВрачОчистка(Элемент, СтандартнаяОбработка)
	ОбновитьНаименование(ЭтотОбъект.Врач, Неопределено);
	Для каждого СтрокаТЧ Из Объект.Исполнители Цикл
		Если СтрокаТЧ.Сотрудник = ЭтотОбъект.Врач Тогда
			Объект.Исполнители.Удалить(СтрокаТЧ);
			Прервать;
		КонецЕсли; 
	КонецЦикла;
	НазначитьОтветственногоЗаНазначенияПоТЧИсполнители();
КонецПроцедуры

&НаКлиенте
Процедура СМПОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	СМП_ = Объект.Исполнители.НайтиСтроки(Новый Структура("Сотрудник", ЭтотОбъект.СМП));
	Если СМП_.Количество() = 0 Тогда
		Объект.Исполнители.Добавить().Сотрудник = ВыбранноеЗначение;
	Иначе
		СМП_[0].Сотрудник = ВыбранноеЗначение;
	КонецЕсли;
	ЭтотОбъект.СМП = ВыбранноеЗначение;
КонецПроцедуры

&НаКлиенте
Процедура СМПОчистка(Элемент, СтандартнаяОбработка)
	Для каждого СтрокаТЧ Из Объект.Исполнители Цикл
		Если СтрокаТЧ.Сотрудник = ЭтотОбъект.СМП Тогда
			Объект.Исполнители.Удалить(СтрокаТЧ);
			Прервать;
		КонецЕсли; 
	КонецЦикла;
	НазначитьОтветственногоЗаНазначенияПоТЧИсполнители();
КонецПроцедуры

&НаКлиенте
Процедура ИсполнителиСотрудникОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	СтруктураПоиска = Новый Структура("Сотрудник",ВыбранноеЗначение);
	ПоискДубликатов = ЭтотОбъект.Объект.Исполнители.НайтиСтроки(СтруктураПоиска);
	Если ПоискДубликатов.Количество() > 0 Тогда
		СтандартнаяОбработка = Ложь;
		СообщенияПользователюКлиент.ВывестиПредупреждение("МедицинскиеРабочиеМеста_СотрудникУжеВключенВСоставРабочегоМеста");
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ИсполнителиСотрудникПриИзменении(Элемент)
	ПредыдущийВрач_ = ЭтотОбъект.Врач;
	ЗаполнитьПоляВрачИСМППоТЧИсполнители();
	НазначитьОтветственногоЗаНазначенияПоТЧИсполнители();
	ОбновитьНаименование(ПредыдущийВрач_, ЭтотОбъект.Врач);
КонецПроцедуры

&НаКлиенте
Процедура ИсполнителиОтветственныйЗаНазначениеПриИзменении(Элемент)
	ТекДанные = ЭтотОбъект.Элементы.Исполнители.ТекущиеДанные;
	Если НЕ ТекДанные = Неопределено Тогда
		Для каждого СтрокаТЧ Из Объект.Исполнители Цикл
			СтрокаТЧ.ОтветственныйЗаНазначение = Ложь;
		КонецЦикла;
		ТекДанные.ОтветственныйЗаНазначение = Истина;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ИсполнителиПослеУдаления(Элемент)
	ПредыдущийВрач_ = ЭтотОбъект.Врач;
	ЗаполнитьПоляВрачИСМППоТЧИсполнители();
	НазначитьОтветственногоЗаНазначенияПоТЧИсполнители();
	ОбновитьНаименование(ПредыдущийВрач_, ЭтотОбъект.Врач);
КонецПроцедуры

//////////////////////////////////////////////////////
/// Вспомогательные методы


///
// Функция: ПолучитьПодразделениеПоВрачу
//      возвращает подразделение по сотруднику.
///
&НаСервереБезКонтекста
Функция ПолучитьПодразделениеПоВрачу(Сотрудник)
	Если НЕ Сотрудник = Неопределено Тогда
		Возврат Сотрудник.Подразделение;
	Иначе
		Возврат Неопределено;
	КонецЕсли;
КонецФункции

///
// Функция: ПолучитьАктуальностьКабинета
//      возвращает актуальность кабинета.
///
&НаСервереБезКонтекста
Функция ПолучитьАктуальностьКабинета(МедКабинет)
	Если ЗначениеЗаполнено(МедКабинет) Тогда
		Возврат МедКабинет.Актуальность;
	Иначе
		Возврат Истина;
	КонецЕсли;
КонецФункции

&НаКлиенте
Процедура ОтобразитьРежимРедактированияСотрудников()
	Если ЭтотОбъект.ИспользоватьРасширенныйРежимРедактированияСотрудников = Истина Тогда
		Элементы.СтраницыИсполнители.ТекущаяСтраница = Элементы.СтраницаИсполнители;
		Элементы.НомерКабинета.Доступность = Ложь;	
		Элементы.Врач.Доступность = Ложь;	
	Иначе	
		Элементы.СтраницыИсполнители.ТекущаяСтраница = Элементы.СтраницаИсполнителиПустая;
		Элементы.НомерКабинета.Доступность = Истина;	
		Элементы.Врач.Доступность = Истина;	
	КонецЕсли;
КонецПроцедуры

////
 // Процедура: УстановитьВидимостьПоляУчасток
 //   устанавливает видимость поля участок в зависимости от настройки
 //   ПланыВидовХарактеристик.НастройкиПользователей.РаботаПоУчасткам.
 ///
&НаКлиенте
Процедура УстановитьВидимостьПолейСпособПланирования()
	БезДаты_ = ПредопределенноеЗначение("Перечисление.ВариантыВремениПланирования.БезДаты");
	ЭлементБезДаты_ = Элементы.ВариантВремениПланирования.СписокВыбора.НайтиПоЗначению(БезДаты_);
	Если Объект.СпособПланирования = ЭтотОбъект.СпособПланированияВСетке Тогда
		Элементы.ПечататьНомерОчереди.Видимость = Истина;
		Элементы.Вместимость.Видимость = Истина;
		Элементы.ФункцияВрачебнойДолжности.Видимость = ИспользуетсяФункцияВрачебнойДолжности;
		Элементы.ВремяВыполнения.Видимость = Истина;
		Элементы.ИспользоватьГрафикПодразделения.Видимость = Истина;
		Если ЭлементБезДаты_ <> Неопределено Тогда
			Элементы.ВариантВремениПланирования.СписокВыбора.Удалить(ЭлементБезДаты_);
			Если Объект.ВариантВремениПланирования = ЭлементБезДаты_.Значение Тогда
				Объект.ВариантВремениПланирования = Неопределено;
			КонецЕсли;
		КонецЕсли;
	Иначе
		Элементы.ПечататьНомерОчереди.Видимость = Ложь;
		Элементы.Вместимость.Видимость = Ложь;
		Элементы.ФункцияВрачебнойДолжности.Видимость = Ложь;
		Элементы.ВремяВыполнения.Видимость = Ложь;
		Элементы.ИспользоватьГрафикПодразделения.Видимость = Ложь;
		Если ЭлементБезДаты_ = Неопределено Тогда
			Элементы.ВариантВремениПланирования.СписокВыбора.Добавить(БезДаты_);
		КонецЕсли;
	КонецЕсли;
	
	Если Объект.СпособПланирования = ЭтотОбъект.СпособПланированияВСетке 
		Или Объект.СпособПланирования = ЭтотОбъект.СпособПланированияВЗаказе
	Тогда
		Элементы.ВариантВремениПланирования.Видимость = Истина;
	Иначе
		Элементы.ВариантВремениПланирования.Видимость = Ложь;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОбработатьВыборПодразделения(ВрачДо, ВрачПосле, ПодразделениеТекущее)
	ПодразедлениеПоВрачДо = ПолучитьПодразделениеПоВрачу(ВрачДо);
	
	Если ПодразедлениеПоВрачДо = ПодразделениеТекущее Тогда
		ПодразделениеТекущее = ПолучитьПодразделениеПоВрачу(ВрачПосле);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура НазначитьОтветственногоЗаНазначенияПоТЧИсполнители()
	Перем КолВрачей_, КолСМП_, ПоследнийВрач_, ПоследнийСМП_;
	// проверим если ли уже ответственный врач
	Для каждого СтрокаТЧ Из Объект.Исполнители Цикл
		Если СтрокаТЧ.ОтветственныйЗаНазначение = Истина Тогда
			Возврат;
		КонецЕсли;
	КонецЦикла;
	
	КолВрачей_ = 0;
	КолСМП_ = 0;
	Для каждого СтрокаТЧ Из Объект.Исполнители Цикл
		Если СтрокаТЧ.Сотрудник.ЭтоГруппа = Ложь Тогда
			Если ЭтоВрач(СтрокаТЧ.Сотрудник) = Истина Тогда
				КолВрачей_ = КолВрачей_+1;
				СтрокаВрач = СтрокаТЧ;
			ИначеЕсли ЭтоСМП(СтрокаТЧ.Сотрудник) = Истина Тогда
				КолСМП_ = КолСМП_+1;
				СтрокаСМП = СтрокаТЧ;
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
	// если врач один, то он и ответственный
	Если КолВрачей_ = 1 Тогда
		Для каждого СтрокаТЧ Из Объект.Исполнители Цикл
			СтрокаТЧ.ОтветственныйЗаНазначение = Ложь;
		КонецЦикла;
		СтрокаВрач.ОтветственныйЗаНазначение = Истина;
		Возврат;
	КонецЕсли;
	// если нет врачей и один смп, то остальные не могут быть ответственными
	Если КолВрачей_ = 0 И КолСМП_ = 1 Тогда
		Для каждого СтрокаТЧ Из Объект.Исполнители Цикл
			СтрокаТЧ.ОтветственныйЗаНазначение = Ложь;
		КонецЦикла;
		СтрокаСМП.ОтветственныйЗаНазначение = Истина;
		Возврат;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьНаименование(ПредыдущийВрач, ТекущийВрач)
	Если ЗначениеЗаполнено(Объект.Наименование) Тогда
		Если Объект.Наименование = Строка(ПредыдущийВрач) Тогда
			Объект.Наименование = Строка(ТекущийВрач);
		КонецЕсли;
	Иначе
		Объект.Наименование = Строка(ТекущийВрач);
	КонецЕсли;
КонецПроцедуры

///
// Процедура: ЗаполнитьПоляВрачИСМППоТЧИсполнители
//   При открытии формы заполняет значения 
//   
///
&НаСервере
Процедура ЗаполнитьПоляВрачИСМППоТЧИсполнители()
	Перем КолВрачей_, КолСМП_, ПоследнийВрач_, ПоследнийСМП_;
	КолВрачей_ = 0;
	КолСМП_ = 0;
	КолДругих_ = 0;
	Для каждого СтрокаТЧ Из Объект.Исполнители Цикл
		Если СтрокаТЧ.Сотрудник.ЭтоГруппа = Ложь Тогда
			Если ЭтоВрач(СтрокаТЧ.Сотрудник) = Истина Тогда
				КолВрачей_ = КолВрачей_+1;
				ПоследнийВрач_ = СтрокаТЧ.Сотрудник;
			ИначеЕсли ЭтоСМП(СтрокаТЧ.Сотрудник) = Истина Тогда
				КолСМП_ = КолСМП_+1;
				ПоследнийСМП_ = СтрокаТЧ.Сотрудник;
			Иначе
				КолДругих_ = КолДругих_ + 1;
			КонецЕсли; 
		КонецЕсли; 
	КонецЦикла;
	
	Если КолВрачей_ = 1 Тогда
		ЭтотОбъект.Врач = ПоследнийВрач_;
	Иначе
		ЭтотОбъект.Врач = Неопределено;
	КонецЕсли;
		
	Если КолСМП_ = 1 Тогда
		ЭтотОбъект.СМП = ПоследнийСМП_;
	Иначе
		ЭтотОбъект.СМП = Неопределено;
	КонецЕсли;
	
	Если КолВрачей_ > 1 Или КолСМП_ > 1 Или КолДругих_ > 0 Или Объект.Исполнители.Количество() > 2 Тогда
		ЭтотОбъект.ИспользоватьРасширенныйРежимРедактированияСотрудников = Истина;
	Иначе
		ЭтотОбъект.ИспользоватьРасширенныйРежимРедактированияСотрудников = Ложь;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ВремяВыполненияРегулирование(Элемент, Направление, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	Минимум_ = Элементы.ВремяВыполнения.МинимальноеЗначение;
	Максимум_ = Элементы.ВремяВыполнения.МаксимальноеЗначение;
	Время_ = ЭтотОбъект.ВремяВыполнения + ЭтотОбъект.ШагВремениВыполнения * Направление;
	Время_ = ?(Время_ < Минимум_, Минимум_, ?(Время_ > Максимум_, Максимум_, Время_));
	ЭтотОбъект.ВремяВыполнения = Время_;
КонецПроцедуры

&НаКлиенте
Процедура ВремяВыполненияПриИзменении(Элемент)
	ЭтотОбъект.ВремяВыполнения = Окр(ЭтотОбъект.ВремяВыполнения / ЭтотОбъект.ШагВремениВыполнения, 0) * 
		ЭтотОбъект.ШагВремениВыполнения;
КонецПроцедуры

&НаСервере
Функция ЭтоВрач(Сотрудник)
	Врач_ = ПредопределенноеЗначение("Перечисление.ВидыМедицинскихДолжностей.Врачи");
	Возврат	Сотрудник.Должность.ВидМедицинскойДолжности = Врач_;
КонецФункции

&НаСервере
Функция ЭтоСМП(Сотрудник)
	СМП_ = ПредопределенноеЗначение("Перечисление.ВидыМедицинскихДолжностей.СреднийМедицинскийПерсонал");
	Возврат	Сотрудник.Должность.ВидМедицинскойДолжности = СМП_;
КонецФункции



#КонецОбласти