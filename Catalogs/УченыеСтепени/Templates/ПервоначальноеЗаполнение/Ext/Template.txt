﻿<Items
	Description="ПервоначальноеЗаполнениеСправочника"
	Columns="Код,Наименование">
	
	<Item
		Код="1"
		Наименование="Кандидат медицинских наук"
	/>
	
	<Item
		Код="2"
		Наименование="Доктор медицинских наук"
	/>
	
	<Item
		Код="3"
		Наименование="Кандидат биологических наук"
	/>
	
	<Item
		Код="4"
		Наименование="Доктор биологических наук"
	/>
	
	<Item
		Код="5"
		Наименование="Кандидат фармацевтических наук"
	/>
	
	<Item
		Код="6"
		Наименование="Доктор фармацевтических наук"
	/>
	
</Items>