﻿
#Область ПрограммныйИнтерфейс

////
 // Процедура: ПервоначальноеЗаполнение
 //   Заполняет реквизит УровеньДоступа у предопределенных значений.
 //
 // Параметры:
 //   ИзменятьСуществующие
 //     При установке в значение Истина реквизит будет заполнен у всех значений.
 //     При установке в значение Ложь - только у тех, у которых он еще не был заполнен.
 ///
Процедура ПервоначальноеЗаполнение(ИзменятьСуществующие = Ложь, ТребуетсяПерезапуск = Ложь) Экспорт
	Перем МакетXML, МакетТаблица, СтрокаМакета, Ошибка;
	
	Попытка
		
		СпособыОплатыМедПомощи = Справочники.СпособыОплатыМедицинскойПомощи;
		
		МакетXML = СпособыОплатыМедПомощи.ПолучитьМакет("СпособыОплатыМедицинскойПомощи").ПолучитьТекст();
		МакетТаблица = ПрочитатьXMLВТаблицу(МакетXML);
		
		Для Каждого СтрокаМакета Из МакетТаблица Цикл
			ЕстьИзменения = Ложь;
			Нашли = СпособыОплатыМедПомощи.НайтиПоРеквизиту("КодПоОМС", СтрокаМакета.КодПоОМС);
			Если ЗначениеЗаполнено(Нашли) Тогда
				СправочникОбъект = Нашли.ПолучитьОбъект();
				Если СправочникОбъект.Наименование <> СтрокаМакета.Наименование Тогда
					СправочникОбъект.Наименование = СтрокаМакета.Наименование;
					ЕстьИзменения = Истина;
				КонецЕсли; 
			Иначе
				СправочникОбъект = СпособыОплатыМедПомощи.СоздатьЭлемент();
				ЗаполнитьЗначенияСвойств(СправочникОбъект, СтрокаМакета);
				ЕстьИзменения = Истина;
			КонецЕсли;
			Если ЕстьИзменения Тогда
				СправочникОбъект.Записать();
			КонецЕсли; 
		КонецЦикла;
	Исключение
		
		Ошибка = ПодробноеПредставлениеОшибки(ИнформацияОбОшибке());
		__ПРОВЕРКА__(Ложь, "ПервоначальноеЗаполнение: Непредвиденная ошибка: " + Ошибка);
		ВызватьИсключение;
		
	КонецПопытки;
	
КонецПроцедуры

// Функция ПрочитатьXMLВТаблицу преобразует текст формата XML в таблицу значений,
// при этом колонки таблицы формируются на основе описания в XML.
//
// Параметры:
//  ТекстXML     - текст формата XML.
//
// Возвращаемое значение:
//  ТаблицаЗначений.
//
Функция ПрочитатьXMLВТаблицу(ТекстXML) Экспорт
	
	Чтение = Новый ЧтениеXML;
	Чтение.УстановитьСтроку(ТекстXML,неопределено,неопределено);
	Чтение.Прочитать();
	Если Чтение.Имя <> "packet" Тогда
		ВызватьИсключение("Ошибка в структуре XML");
	КонецЕсли;
	
	Таблица_ = Новый ТаблицаЗначений;
	Таблица_.Колонки.Добавить("КодПоОМС",     Новый ОписаниеТипов("Число"));
	Таблица_.Колонки.Добавить("Наименование", Новый ОписаниеТипов("Строка"));
	Пока Чтение.Прочитать() Цикл
		Если Чтение.ТипУзла <> ТипУзлаXML.НачалоЭлемента Тогда
			Продолжить;
		КонецЕсли;
		Если Чтение.Имя <> "zap" Тогда
			Продолжить;
		КонецЕсли; 
		новСтр = Таблица_.Добавить();
		
		Пока Чтение.Прочитать() Цикл
			Если Чтение.Имя = "zap" И Чтение.ТипУзла = ТипУзлаXML.КонецЭлемента Тогда
				Прервать;
			КонецЕсли;
			Если Чтение.ТипУзла = ТипУзлаXML.НачалоЭлемента Тогда
				ИмяРодительскогоУзла = Чтение.Имя;
			КонецЕсли;
			// устанавливаем значение узла в таблицу значений
			Если Чтение.ТипУзла = ТипУзлаXML.Текст Тогда
				Индекс = ?(ИмяРодительскогоУзла = "IDSP", 0, ?(ИмяРодительскогоУзла = "SPNAME", 1, Неопределено));
				Если Индекс <> Неопределено Тогда
					новСтр[Индекс] = Чтение.Значение;
				КонецЕсли;
			КонецЕсли;
		КонецЦикла;
		
	КонецЦикла;
		
	Возврат Таблица_;
	
КонецФункции


#КонецОбласти