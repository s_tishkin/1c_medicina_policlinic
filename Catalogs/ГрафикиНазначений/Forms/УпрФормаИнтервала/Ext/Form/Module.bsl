﻿
#Область ПрограммныйИнтерфейс


&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	ЭтотОбъект.ИнтервалОперацияНадМножеством = Перечисления.ГрафикиНазначенийОперацииНадМножествами.Пересечение;
	ЭтотОбъект.ИнтервалОкончаниеВключено = Истина;
	ЭтотОбъект.ИнтервалНачалоВключено = Истина;
	ЭтотОбъект.ИнтервалНомерСтроки = Параметры.НомерСтроки;
	ЭтотОбъект.ИнтервалТипИнтервала = Перечисления.ГрафикиНазначенийТипыИнтервалов.Интервал;
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	Если ТипЗнч(ЭтотОбъект.ВладелецФормы) = Тип("УправляемаяФорма")
		И ЗначениеЗаполнено(ЭтотОбъект.ИнтервалНомерСтроки)
	Тогда
		ТекущиеДанные_ = ЭтотОбъект.ВладелецФормы.Объект.Интервалы[ЭтотОбъект.ИнтервалНомерСтроки - 1];
		ЭтотОбъект.ИнтервалНаименование = ТекущиеДанные_.Наименование;
		ЭтотОбъект.ИнтервалНачало = ЭтотОбъект.ВладелецФормы.ПолучитьЗначениеИнтервалаГрафикаПоКлючуСтроки(
															ТекущиеДанные_.Начало).Значение;
		ЭтотОбъект.ИнтервалОкончание = ЭтотОбъект.ВладелецФормы.ПолучитьЗначениеИнтервалаГрафикаПоКлючуСтроки(
															ТекущиеДанные_.Окончание).Значение;
		ЭтотОбъект.ИнтервалСередина = ЭтотОбъект.ВладелецФормы.ПолучитьЗначениеИнтервалаГрафикаПоКлючуСтроки(
															ТекущиеДанные_.Середина).Значение;
															
		Ширина_ = ЭтотОбъект.ВладелецФормы.ПолучитьЗначениеИнтервалаГрафикаПоКлючуСтроки(
															ТекущиеДанные_.Ширина);
		ЭтотОбъект.ИнтервалШирина = Ширина_.Значение;
		ЭтотОбъект.ИнтервалЕдиницаИзмеренияШирины = Ширина_.ЕдиницаИзмерения;
		ЭтотОбъект.ИнтервалОперацияНадМножеством = ТекущиеДанные_.ОперацияНадМножеством;
		ЭтотОбъект.ИнтервалНачалоВключено = ТекущиеДанные_.НачалоВключено;
		ЭтотОбъект.ИнтервалОкончаниеВключено = ТекущиеДанные_.ОкончаниеВключено;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура Записать(Команда)
	
	Если ТипЗнч(ЭтотОбъект.ВладелецФормы) = Тип("УправляемаяФорма") Тогда
		
		Если ЗначениеЗаполнено(ЭтотОбъект.ИнтервалНомерСтроки) Тогда
			ТекущиеДанные_ = ЭтотОбъект.ВладелецФормы.Объект.Интервалы[ЭтотОбъект.ИнтервалНомерСтроки - 1];
		Иначе
			строкаТЧ_ = ЭтотОбъект.ВладелецФормы.Объект.Интервалы.Добавить();
			строкаТЧ_.ТипИнтервала = ЭтотОбъект.ИнтервалТипИнтервала;
			ТекущиеДанные_ = строкаТЧ_;
		КонецЕсли;
		
		ТекущиеДанные_.Наименование = ЭтотОбъект.ИнтервалНаименование;
		ЭтотОбъект.ВладелецФормы.ЗаписатьЗначениеИнтервалаГрафикаПоКлючуСтроки(
							ЭтотОбъект.ИнтервалНачало, , ТекущиеДанные_.Начало);
		ЭтотОбъект.ВладелецФормы.ЗаписатьЗначениеИнтервалаГрафикаПоКлючуСтроки(
							ЭтотОбъект.ИнтервалОкончание, , ТекущиеДанные_.Окончание);
		ЭтотОбъект.ВладелецФормы.ЗаписатьЗначениеИнтервалаГрафикаПоКлючуСтроки(
							ЭтотОбъект.ИнтервалСередина, , ТекущиеДанные_.Середина);
		ЭтотОбъект.ВладелецФормы.ЗаписатьЗначениеИнтервалаГрафикаПоКлючуСтроки(
							ЭтотОбъект.ИнтервалШирина, ЭтотОбъект.ИнтервалЕдиницаИзмеренияШирины, ТекущиеДанные_.Ширина);
		ТекущиеДанные_.ОперацияНадМножеством = ЭтотОбъект.ИнтервалОперацияНадМножеством;
		
		Если ЗначениеЗаполнено(ЭтотОбъект.ИнтервалНачало) Тогда
			ТекущиеДанные_.НачалоВключено = ЭтотОбъект.ИнтервалНачалоВключено;
		Иначе
			ТекущиеДанные_.НачалоВключено = Ложь;
		КонецЕсли;

		Если ЗначениеЗаполнено(ЭтотОбъект.ИнтервалОкончание) Тогда
			ТекущиеДанные_.ОкончаниеВключено = ЭтотОбъект.ИнтервалОкончаниеВключено;
		Иначе
			ТекущиеДанные_.ОкончаниеВключено = Ложь;
		КонецЕсли;
		
		ЭтотОбъект.ВладелецФормы.ЗаписатьРаспределения(ТекущиеДанные_, ЭтотОбъект.ИнтервалРаспределения);
		
	КонецЕсли;
	

	ЭтотОбъект.Закрыть();
КонецПроцедуры

&НаКлиенте
Процедура РаспределениеДлительности(Команда)
	
	ПараметрыОткрытия_ = Новый Структура("ИмяРеквизитаЗначения,НомерСтроки", "Ширина", ЭтотОбъект.ИнтервалНомерСтроки);
	
	ОткрытьФорму("Справочник.ГрафикиНазначений.Форма.УпрФормаРаспределения",
				ПараметрыОткрытия_,
				ЭтотОбъект,,,,,
				РежимОткрытияОкнаФормы.БлокироватьОкноВладельца
			);
	
КонецПроцедуры

&НаКлиенте
Процедура РаспределениеСередины(Команда)
	
	ПараметрыОткрытия_ = Новый Структура("ИмяРеквизитаЗначения,НомерСтроки", "Середина", ЭтотОбъект.ИнтервалНомерСтроки);
	
	ОткрытьФорму("Справочник.ГрафикиНазначений.Форма.УпрФормаРаспределения",
				ПараметрыОткрытия_,
				ЭтотОбъект,,,,,
				РежимОткрытияОкнаФормы.БлокироватьОкноВладельца
			);
	
КонецПроцедуры





#КонецОбласти