﻿
#Область ПрограммныйИнтерфейс

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	ЭтотОбъект.АвтоНаименование = Истина;
КонецПроцедуры

&НаСервере
Процедура ОбработкаПроверкиЗаполненияНаСервере(Отказ, ПроверяемыеРеквизиты)
	ПроверяемыеРеквизиты.Очистить();
КонецПроцедуры

&НаСервере
Процедура ПередЗаписьюНаСервере(Отказ, ТекущийОбъект, ПараметрыЗаписи)
	ТекущийОбъект.Интервалы.Очистить();
	ТекущийОбъект.Значения.Очистить();
	ТекущийОбъект.Распределения.Очистить();
	СоздатьИнтервалыДневногоГрафика(ТекущийОбъект);
	СоздатьИнтервалыНедельногоГрафика(ТекущийОбъект);
	СоздатьИнтервалыМесячногоГрафика(ТекущийОбъект);
	СоздатьИнтервалыГодовогоГрафика(ТекущийОбъект);
КонецПроцедуры

&НаСервере
Процедура СоздатьИнтервалыДневногоГрафика(ТекущийОбъект)
	Если ЗначениеЗаполнено(ЭтотОбъект.ДневнойГрафикЧастота) Тогда
		ЗначениеПериода_ = ТекущийОбъект.Значения.Добавить();
		ЗначениеПериода_.КлючЗначения = ТекущийОбъект.Значения.Количество();
		ЗначениеПериода_.Значение = ОбщиеМеханизмыКлиентСервер.ОдниСутки() / ЭтотОбъект.ДневнойГрафикЧастота;
		ЗначениеПериода_.ЕдиницаИзмерения = Перечисления.ГрафикиНазначенийЕдиницыИзмерения.Секунды; 
		Интервал_ = ТекущийОбъект.Интервалы.Добавить();
		Интервал_.ТипИнтервала = Перечисления.ГрафикиНазначенийТипыИнтервалов.ПериодическийИнтервал;
		Интервал_.ОперацияНадМножеством = Перечисления.ГрафикиНазначенийОперацииНадМножествами.Пересечение;
		Интервал_.Период = ЗначениеПериода_.КлючЗначения;
		Интервал_.Наименование = СогласоватьЧислительное(ЭтотОбъект.ДневнойГрафикЧастота, "раз", 1) + " в день";
		Если ЗначениеЗаполнено(ЭтотОбъект.ДневнойГрафикДлительность) Тогда
			ЗначениеШирины_ = ТекущийОбъект.Значения.Добавить();
			ЗначениеШирины_.КлючЗначения = ТекущийОбъект.Значения.Количество();
			ЗначениеШирины_.Значение = ЭтотОбъект.ДневнойГрафикДлительность;
			ЗначениеШирины_.ЕдиницаИзмерения = ЭтотОбъект.ДневнойГрафикДлительностьЕдиницаИзмерения; 
			Интервал_.Ширина = ЗначениеШирины_.КлючЗначения;
			Длительность_ = СогласоватьЧислительное(
				ЭтотОбъект.ДневнойГрафикДлительность, ЭтотОбъект.ДневнойГрафикДлительностьЕдиницаИзмерения, 3
			);
			Интервал_.Наименование = Интервал_.Наименование + " по " + Длительность_;
		КонецЕсли;
	КонецЕсли;
	Если ЗначениеЗаполнено(ЭтотОбъект.ДневнойГрафикСобытие) Тогда
		Интервал_ = ТекущийОбъект.Интервалы.Добавить();
		Интервал_.ТипИнтервала = Перечисления.ГрафикиНазначенийТипыИнтервалов.СобытийныйИнтервал;
		Интервал_.ОперацияНадМножеством = Перечисления.ГрафикиНазначенийОперацииНадМножествами.Пересечение;
		Интервал_.Событие = ЭтотОбъект.ДневнойГрафикСобытие;
		Интервал_.Наименование = ЭтотОбъект.ДневнойГрафикСобытие;
	КонецЕсли;
	Если ЗначениеЗаполнено(ЭтотОбъект.ДневнойГрафикПериод) Тогда
		ЗначениеПериода_ = ТекущийОбъект.Значения.Добавить();
		ЗначениеПериода_.КлючЗначения = ТекущийОбъект.Значения.Количество();
		ЗначениеПериода_.Значение = ЭтотОбъект.ДневнойГрафикПериод;
		ЗначениеПериода_.ЕдиницаИзмерения = Перечисления.ГрафикиНазначенийЕдиницыИзмерения.Сутки; 
		Интервал_ = ТекущийОбъект.Интервалы.Добавить();
		Интервал_.ТипИнтервала = Перечисления.ГрафикиНазначенийТипыИнтервалов.ПериодическийИнтервал;
		Интервал_.ОперацияНадМножеством = Перечисления.ГрафикиНазначенийОперацииНадМножествами.Пересечение;
		Интервал_.Период = ЗначениеПериода_.КлючЗначения;
		Период_ = ?(ЭтотОбъект.ДневнойГрафикПериод = 1, "день", "" + ЭтотОбъект.ДневнойГрафикПериод + "-й день");
		Интервал_.Наименование = "каждый " + Период_;
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура СоздатьИнтервалыНедельногоГрафика(ТекущийОбъект)
	Если Не ЗначениеЗаполнено(ЭтотОбъект.НедельныйГрафикПериод) Тогда
		Возврат;	
	КонецЕсли;
	
	ЗначениеПериода_ = ТекущийОбъект.Значения.Добавить();
	ЗначениеПериода_.КлючЗначения = ТекущийОбъект.Значения.Количество();
	ЗначениеПериода_.Значение = ЭтотОбъект.НедельныйГрафикПериод;
	ЗначениеПериода_.ЕдиницаИзмерения = Перечисления.ГрафикиНазначенийЕдиницыИзмерения.Недели; 
	Если ЭтотОбъект.НедельныйГрафикПонедельник = Истина Тогда
		Наименование_ = ?(
			ЭтотОбъект.НедельныйГрафикПериод = 1, 
			"каждый понедельник",
			"каждый " + ЭтотОбъект.НедельныйГрафикПериод + "-й понедельник"
		);
		ДобавитьИнтервалНедельногоГрафика(ТекущийОбъект, ЗначениеПериода_.КлючЗначения, Наименование_, 1); 
	КонецЕсли;
	Если ЭтотОбъект.НедельныйГрафикВторник = Истина Тогда
		Наименование_ = ?(
			ЭтотОбъект.НедельныйГрафикПериод = 1, 
			"каждый вторник",
			"каждый " + ЭтотОбъект.НедельныйГрафикПериод + "-й вторник"
		);
		ДобавитьИнтервалНедельногоГрафика(ТекущийОбъект, ЗначениеПериода_.КлючЗначения, Наименование_, 2); 
	КонецЕсли;
	Если ЭтотОбъект.НедельныйГрафикСреда = Истина Тогда
		Наименование_ = ?(
			ЭтотОбъект.НедельныйГрафикПериод = 1, 
			"каждую среду",
			"каждую " + ЭтотОбъект.НедельныйГрафикПериод + "-ю среду"
		);
		ДобавитьИнтервалНедельногоГрафика(ТекущийОбъект, ЗначениеПериода_.КлючЗначения, Наименование_, 3); 
	КонецЕсли;
	Если ЭтотОбъект.НедельныйГрафикЧетверг = Истина Тогда
		Наименование_ = ?(
			ЭтотОбъект.НедельныйГрафикПериод = 1, 
			"каждый четверг",
			"каждый " + ЭтотОбъект.НедельныйГрафикПериод + "-й четверг"
		);
		ДобавитьИнтервалНедельногоГрафика(ТекущийОбъект, ЗначениеПериода_.КлючЗначения, Наименование_, 4); 
	КонецЕсли;
	Если ЭтотОбъект.НедельныйГрафикПятница = Истина Тогда
		Наименование_ = ?(
			ЭтотОбъект.НедельныйГрафикПериод = 1, 
			"каждую пятницу",
			"каждую " + ЭтотОбъект.НедельныйГрафикПериод + "-ю пятницу"
		);
		ДобавитьИнтервалНедельногоГрафика(ТекущийОбъект, ЗначениеПериода_.КлючЗначения, Наименование_, 5); 
	КонецЕсли;
	Если ЭтотОбъект.НедельныйГрафикСуббота = Истина Тогда
		Наименование_ = ?(
			ЭтотОбъект.НедельныйГрафикПериод = 1, 
			"каждую субботу",
			"каждую " + ЭтотОбъект.НедельныйГрафикПериод + "-ю субботу"
		);
		ДобавитьИнтервалНедельногоГрафика(ТекущийОбъект, ЗначениеПериода_.КлючЗначения, Наименование_, 6); 
	КонецЕсли;
	Если ЭтотОбъект.НедельныйГрафикВоскресенье = Истина Тогда
		Наименование_ = ?(
			ЭтотОбъект.НедельныйГрафикПериод = 1, 
			"каждое воскресенье",
			"каждое " + ЭтотОбъект.НедельныйГрафикПериод + "-е воскресенье"
		);
		ДобавитьИнтервалНедельногоГрафика(ТекущийОбъект, ЗначениеПериода_.КлючЗначения, Наименование_, 7); 
	КонецЕсли;
	
	Если ТекущийОбъект.Интервалы.Количество() = 0 Тогда
		Интервал_ = ТекущийОбъект.Интервалы.Добавить();
		Интервал_.ТипИнтервала = Перечисления.ГрафикиНазначенийТипыИнтервалов.ПериодическийИнтервал;
		Интервал_.ОперацияНадМножеством = Перечисления.ГрафикиНазначенийОперацииНадМножествами.Пересечение;
		Интервал_.Период = ЗначениеПериода_.КлючЗначения;
		Интервал_.Наименование = ?(
			ЭтотОбъект.НедельныйГрафикПериод = 1, 
			"каждую неделю", 
			"каждую " + ЭтотОбъект.НедельныйГрафикПериод + "-ю неделю"
		);
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура СоздатьИнтервалыМесячногоГрафика(ТекущийОбъект)
	Если ЗначениеЗаполнено(ЭтотОбъект.МесячныйГрафикПериод) Тогда
		ЗначениеПериода_ = ТекущийОбъект.Значения.Добавить();
		ЗначениеПериода_.КлючЗначения = ТекущийОбъект.Значения.Количество();
		ЗначениеПериода_.Значение = ЭтотОбъект.МесячныйГрафикПериод;
		ЗначениеПериода_.ЕдиницаИзмерения = Перечисления.ГрафикиНазначенийЕдиницыИзмерения.Месяцы; 
		Интервал_ = ТекущийОбъект.Интервалы.Добавить();
		Интервал_.ТипИнтервала = Перечисления.ГрафикиНазначенийТипыИнтервалов.ПериодическийИнтервал;
		Интервал_.ОперацияНадМножеством = Перечисления.ГрафикиНазначенийОперацииНадМножествами.Пересечение;
		Интервал_.Период = ЗначениеПериода_.КлючЗначения;
		Период_ = ?(ЭтотОбъект.МесячныйГрафикПериод = 1, "месяц", "" + ЭтотОбъект.МесячныйГрафикПериод + "-й месяц");
		Интервал_.Наименование = "каждый " + Период_;
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура СоздатьИнтервалыГодовогоГрафика(ТекущийОбъект)
	Если ЗначениеЗаполнено(ЭтотОбъект.ГодовойГрафикПериод) Тогда
		ЗначениеПериода_ = ТекущийОбъект.Значения.Добавить();
		ЗначениеПериода_.КлючЗначения = ТекущийОбъект.Значения.Количество();
		ЗначениеПериода_.Значение = ЭтотОбъект.ГодовойГрафикПериод;
		ЗначениеПериода_.ЕдиницаИзмерения = Перечисления.ГрафикиНазначенийЕдиницыИзмерения.Годы; 
		Интервал_ = ТекущийОбъект.Интервалы.Добавить();
		Интервал_.ТипИнтервала = Перечисления.ГрафикиНазначенийТипыИнтервалов.ПериодическийИнтервал;
		Интервал_.ОперацияНадМножеством = Перечисления.ГрафикиНазначенийОперацииНадМножествами.Пересечение;
		Интервал_.Период = ЗначениеПериода_.КлючЗначения;
		Период_ = ?(ЭтотОбъект.ГодовойГрафикПериод = 1, "год", "" + ЭтотОбъект.ГодовойГрафикПериод + "-й год");
		Интервал_.Наименование = "каждый " + Период_;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура НаименованиеПриИзменении(Элемент)
	ЭтотОбъект.АвтоНаименование = Ложь;
КонецПроцедуры

&НаКлиенте
Процедура ГрафикПриИзменении(Элемент)
	ОбновитьАвтоНаименование()
КонецПроцедуры

&НаКлиенте
Процедура ОбновитьАвтоНаименование()
	Если ЭтотОбъект.АвтоНаименование = Ложь Тогда
		Возврат;	
	КонецЕсли;
	
	Наименование_ = "";
	Если ЗначениеЗаполнено(ЭтотОбъект.ДневнойГрафикЧастота) Тогда
		Наименование_ = Наименование_ + СогласоватьЧислительное(ЭтотОбъект.ДневнойГрафикЧастота, "раз", 1) + " в день ";
		Если ЗначениеЗаполнено(ЭтотОбъект.ДневнойГрафикДлительность) Тогда
			Длительность_ = СогласоватьЧислительное(
				ЭтотОбъект.ДневнойГрафикДлительность, ЭтотОбъект.ДневнойГрафикДлительностьЕдиницаИзмерения, 3
			);
			Наименование_ = Наименование_ + "по " + Длительность_ + " ";
		КонецЕсли;
	КонецЕсли;
	Если ЗначениеЗаполнено(ЭтотОбъект.ДневнойГрафикСобытие) Тогда
		Наименование_ = Наименование_ + ЭтотОбъект.ДневнойГрафикСобытие + " ";
	КонецЕсли;
	Если ЗначениеЗаполнено(ЭтотОбъект.ДневнойГрафикПериод) Тогда
		Период_ = ?(ЭтотОбъект.ДневнойГрафикПериод = 1, "день", "" + ЭтотОбъект.ДневнойГрафикПериод + "-й день");
		Наименование_ = ?(Наименование_ = "", "", СокрП(Наименование_) + "; ") + "каждый " + Период_ + " ";
	КонецЕсли;
	Если ЗначениеЗаполнено(ЭтотОбъект.НедельныйГрафикПериод) Тогда
		Наименование_ = ?(Наименование_ = "", "", СокрП(Наименование_) + "; ") + 
			ПолучитьАвтоНаименованиеНедельногоГрафика() + " ";
	КонецЕсли;
	Если ЗначениеЗаполнено(ЭтотОбъект.МесячныйГрафикПериод) Тогда
		Период_ = ?(ЭтотОбъект.МесячныйГрафикПериод = 1, "месяц", "" + ЭтотОбъект.МесячныйГрафикПериод + "-й месяц");
		Наименование_ = ?(Наименование_ = "", "", СокрП(Наименование_) + "; ") + "каждый " + Период_ + " ";
	КонецЕсли;
	Если ЗначениеЗаполнено(ЭтотОбъект.ГодовойГрафикПериод) Тогда
		Период_ = ?(ЭтотОбъект.ГодовойГрафикПериод = 1, "год", "" + ЭтотОбъект.ГодовойГрафикПериод + "-й год");
		Наименование_ = ?(Наименование_ = "", "", СокрП(Наименование_) + "; ") + "каждый " + Период_ + " ";
	КонецЕсли;
	Объект.Наименование = НРег(СокрЛП(Наименование_));
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Функция СогласоватьЧислительное(Значение, ЕдиницаИзмерения, Падеж)
	Результат_ = "";
	ЕдиницаИзмерения_ = НРег(СокрЛП(ЕдиницаИзмерения));
	ПараметрыПредметаИсчисления_ = " , , ";
	Если ЕдиницаИзмерения_ = "раз" Тогда
		Если Падеж = 1 Тогда
			ПараметрыПредметаИсчисления_ = "раз, раза, раз";
		ИначеЕсли Падеж = 3 Тогда	
			ПараметрыПредметаИсчисления_ = "разу, раза, раз";
		КонецЕсли;
	ИначеЕсли ЕдиницаИзмерения_ = "секунд" Тогда	
		Если Падеж = 1 Тогда
			ПараметрыПредметаИсчисления_ = "секунда, секунды, секунд";
		ИначеЕсли Падеж = 3 Тогда	
			ПараметрыПредметаИсчисления_ = "секунде, секунды, секунд";
		КонецЕсли;
	ИначеЕсли ЕдиницаИзмерения_ = "минут" Тогда	
		Если Падеж = 1 Тогда
			ПараметрыПредметаИсчисления_ = "минута, минуты, минут";
		ИначеЕсли Падеж = 3 Тогда	
			ПараметрыПредметаИсчисления_ = "минуте, минуты, минут";
		КонецЕсли;
	ИначеЕсли ЕдиницаИзмерения_ = "часов" Тогда	
		Если Падеж = 1 Тогда
			ПараметрыПредметаИсчисления_ = "час, часа, часов";
		ИначеЕсли Падеж = 3 Тогда	
			ПараметрыПредметаИсчисления_ = "часу, часа, часов";
		КонецЕсли;
	ИначеЕсли ЕдиницаИзмерения_ = "суток" Тогда	
		Если Падеж = 1 Тогда
			ПараметрыПредметаИсчисления_ = "сутки, суток, суток";
		ИначеЕсли Падеж = 3 Тогда	
			ПараметрыПредметаИсчисления_ = "суткам, суток, суток";
		КонецЕсли;
	ИначеЕсли ЕдиницаИзмерения_ = "недель" Тогда	
		Если Падеж = 1 Тогда
			ПараметрыПредметаИсчисления_ = "неделю, недели, недель";
		ИначеЕсли Падеж = 3 Тогда	
			ПараметрыПредметаИсчисления_ = "неделе, недели, недель";
		КонецЕсли;
	ИначеЕсли ЕдиницаИзмерения_ = "месяцев" Тогда	
		Если Падеж = 1 Тогда
			ПараметрыПредметаИсчисления_ = "месяц, месяца, месяцев";
		ИначеЕсли Падеж = 3 Тогда	
			ПараметрыПредметаИсчисления_ = "месяцу, месяца, месяцев";
		КонецЕсли;
	ИначеЕсли ЕдиницаИзмерения_ = "лет" Тогда	
		Если Падеж = 1 Тогда
			ПараметрыПредметаИсчисления_ = "год, года, лет";
		ИначеЕсли Падеж = 3 Тогда	
			ПараметрыПредметаИсчисления_ = "году, года, лет";
		КонецЕсли;
	КонецЕсли;
	Результат_ = СтроковыеФункцииКлиентСервер.ЧислоЦифрамиПредметИсчисленияПрописью(
		Значение, ПараметрыПредметаИсчисления_
	);
	Возврат Результат_;
КонецФункции                  

&НаКлиенте
Процедура ВсеДниНедели(Команда)
	ЭтотОбъект.НедельныйГрафикПонедельник = Истина;	
	ЭтотОбъект.НедельныйГрафикВторник = Истина;	
	ЭтотОбъект.НедельныйГрафикСреда = Истина;	
	ЭтотОбъект.НедельныйГрафикЧетверг = Истина;	
	ЭтотОбъект.НедельныйГрафикПятница = Истина;	
	ЭтотОбъект.НедельныйГрафикСуббота = Истина;	
	ЭтотОбъект.НедельныйГрафикВоскресенье = Истина;	
	ОбновитьАвтоНаименование()
КонецПроцедуры

&НаКлиенте
Процедура ВсеМесяцы(Команда)
	ЭтотОбъект.МесячныйГрафикЯнварь = Истина;	
	ЭтотОбъект.МесячныйГрафикФевраль = Истина;	
	ЭтотОбъект.МесячныйГрафикМарт = Истина;	
	ЭтотОбъект.МесячныйГрафикАпрель = Истина;	
	ЭтотОбъект.МесячныйГрафикМай = Истина;	
	ЭтотОбъект.МесячныйГрафикИюнь = Истина;	
	ЭтотОбъект.МесячныйГрафикИюль = Истина;	
	ЭтотОбъект.МесячныйГрафикАвгуст = Истина;	
	ЭтотОбъект.МесячныйГрафикСентябрь = Истина;	
	ЭтотОбъект.МесячныйГрафикОктябрь = Истина;	
	ЭтотОбъект.МесячныйГрафикНоябрь = Истина;	
	ЭтотОбъект.МесячныйГрафикДекабрь = Истина;	
КонецПроцедуры

&НаКлиенте
Функция ПолучитьАвтоНаименованиеНедельногоГрафика()
	Если Не ЗначениеЗаполнено(ЭтотОбъект.НедельныйГрафикПериод) Тогда
		Возврат "";
	КонецЕсли;
	
	Период_ = "";
	Если ЭтотОбъект.НедельныйГрафикПонедельник = Истина Тогда
		Период_ = ?(
			ЭтотОбъект.НедельныйГрафикПериод = 1, 
			"каждый понедельник, ", 
			"каждый " + ЭтотОбъект.НедельныйГрафикПериод + "-й понедельник, "
		);
	КонецЕсли;
	Если ЭтотОбъект.НедельныйГрафикВторник = Истина Тогда
		Если ПустаяСтрока(Период_) Тогда
			Период_ = ?(
				ЭтотОбъект.НедельныйГрафикПериод = 1, 
				"каждый вторник, ", 
				"каждый " + ЭтотОбъект.НедельныйГрафикПериод + "-й вторник, "
			);
		Иначе
			Период_ = Период_ + "вторник, ";	
		КонецЕсли;
	КонецЕсли;
	Если ЭтотОбъект.НедельныйГрафикСреда = Истина Тогда
		Если ПустаяСтрока(Период_) Тогда
			Период_ = ?(
				ЭтотОбъект.НедельныйГрафикПериод = 1, 
				"каждую среду, ", 
				"каждую " + ЭтотОбъект.НедельныйГрафикПериод + "-ю среду, "
			);
		Иначе
			Период_ = Период_ + "среду, ";	
		КонецЕсли;
	КонецЕсли;
	Если ЭтотОбъект.НедельныйГрафикЧетверг = Истина Тогда
		Если ПустаяСтрока(Период_) Тогда
			Период_ = ?(
				ЭтотОбъект.НедельныйГрафикПериод = 1, 
				"каждый четверг, ", 
				"каждый " + ЭтотОбъект.НедельныйГрафикПериод + "-й четверг, "
			);
		Иначе
			Период_ = Период_ + "четверг, ";	
		КонецЕсли;
	КонецЕсли;
	Если ЭтотОбъект.НедельныйГрафикПятница = Истина Тогда
		Если ПустаяСтрока(Период_) Тогда
			Период_ = ?(
				ЭтотОбъект.НедельныйГрафикПериод = 1, 
				"каждую пятницу, ", 
				"каждую " + ЭтотОбъект.НедельныйГрафикПериод + "-ю пятницу, "
			);
		Иначе
			Период_ = Период_ + "пятницу, ";	
		КонецЕсли;
	КонецЕсли;
	Если ЭтотОбъект.НедельныйГрафикСуббота = Истина Тогда
		Если ПустаяСтрока(Период_) Тогда
			Период_ = ?(
				ЭтотОбъект.НедельныйГрафикПериод = 1, 
				"каждую субботу, ", 
				"каждую " + ЭтотОбъект.НедельныйГрафикПериод + "-ю субботу, "
			);
		Иначе
			Период_ = Период_ + "субботу, ";	
		КонецЕсли;
	КонецЕсли;
	Если ЭтотОбъект.НедельныйГрафикВоскресенье = Истина Тогда
		Если ПустаяСтрока(Период_) Тогда
			Период_ = ?(
				ЭтотОбъект.НедельныйГрафикПериод = 1, 
				"каждое воскресенье, ", 
				"каждое " + ЭтотОбъект.НедельныйГрафикПериод + "-е воскресенье, "
			);
		Иначе
			Период_ = Период_ + "воскресенье, ";	
		КонецЕсли;
	КонецЕсли;
		
	Если ПустаяСтрока(Период_) Тогда
		Период_ = ?(
			ЭтотОбъект.НедельныйГрафикПериод = 1, 
			"каждую неделю", 
			"каждую " + ЭтотОбъект.НедельныйГрафикПериод + "-ю неделю"
		);
	Иначе
		Период_ = СокрП(Период_);
		СтроковыеФункцииКлиентСервер.УдалитьПоследнийСимволВСтроке(Период_);
	КонецЕсли;
	Возврат Период_;
КонецФункции

&НаСервере
Функция ДобавитьИнтервалНедельногоГрафика(ТекущийОбъект, Период, Наименование, ДеньНедели)
	Интервал_ = ТекущийОбъект.Интервалы.Добавить();
	Интервал_.ТипИнтервала = Перечисления.ГрафикиНазначенийТипыИнтервалов.ПериодическийИнтервал;
	Интервал_.ОперацияНадМножеством = Перечисления.ГрафикиНазначенийОперацииНадМножествами.Объединение;
	Интервал_.Период = Период;
	Интервал_.Начало = ПолучитьКлючЗначенияДняНедели(ТекущийОбъект, ДеньНедели);
	Интервал_.НачалоВключено = Истина;
	Интервал_.Окончание = ПолучитьКлючЗначенияДняНедели(ТекущийОбъект, ДеньНедели + 1);
	Интервал_.ОкончаниеВключено = Ложь;
	Интервал_.ВыравниваниеПоКалендарю = Перечисления.ГрафикиНазначенийКалендарныеЦиклы.ДеньНедели;
	Интервал_.Наименование = Наименование;
КонецФункции

&НаСервере
Функция ПолучитьКлючЗначенияДняНедели(ТекущийОбъект, ДеньНедели)
	ДатаДняНедели_ = ПолучитьДатуДняНедели(ДеньНедели);	
	СтрокаЗначения_ = ТекущийОбъект.Значения.Найти(ДатаДняНедели_, "Значение");
	Если СтрокаЗначения_ = Неопределено Тогда
		СтрокаЗначения_ = ТекущийОбъект.Значения.Добавить();
		СтрокаЗначения_.КлючЗначения = ТекущийОбъект.Значения.Количество();
		СтрокаЗначения_.Значение = ДатаДняНедели_;
	КонецЕсли;
	Возврат СтрокаЗначения_.КлючЗначения;
КонецФункции

&НаСервере
Функция ПолучитьДатуДняНедели(ДеньНедели)
	Возврат ОбщиеМеханизмыКлиентСервер.ПустаяДата() + 
		(ДеньНедели - 1) * ОбщиеМеханизмыКлиентСервер.ОдниСутки();	
КонецФункции



#КонецОбласти