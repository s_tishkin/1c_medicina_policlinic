﻿
#Область ПрограммныйИнтерфейс

// Получить графики назначений, которые являются сменами работы. 
//
// Возвращаемое значение:
//   Массив
// 
Функция ПолучитьГрафикиСмен() Экспорт
	Запрос_ = Новый Запрос(
		"ВЫБРАТЬ
		|	ГрафикиНазначений.Ссылка
		|ИЗ
		|	Справочник.ГрафикиНазначений КАК ГрафикиНазначений
		|ГДЕ
		|	ГрафикиНазначений.ПометкаУдаления = ЛОЖЬ
		|	И ГрафикиНазначений.Смена = ИСТИНА"
	);
	Результат_ = Запрос_.Выполнить();
	Возврат ?(Результат_.Пустой(), Новый Массив, Результат_.Выгрузить().ВыгрузитьКолонку("Ссылка"));
КонецФункции

// Получить период графика назначений, являющегося сменой работы. 
//
// Возвращаемое значение:
//   Структура
// 
Функция ПолучитьПериодГрафикаСмены(Смена) Экспорт
	ПустаяДата_ = ОбщиеМеханизмыКлиентСервер.ПустаяДата();
	Период_ = Новый Структура(
		"Начало, Окончание, НачалоВключено, ОкончаниеВключено", ПустаяДата_, ПустаяДата_, Ложь, Ложь
	);
	Запрос_ = Новый Запрос(
		"ВЫБРАТЬ ПЕРВЫЕ 1
		|	ЕСТЬNULL(ЗначениеНачала.Значение, ДАТАВРЕМЯ(1, 1, 1)) КАК Начало,
		|	ЕСТЬNULL(ЗначениеОкончания.Значение, КОНЕЦПЕРИОДА(ДАТАВРЕМЯ(1, 1, 1), ДЕНЬ)) КАК Окончание,
		|	Интервалы.НачалоВключено,
		|	Интервалы.ОкончаниеВключено
		|ИЗ
		|	Справочник.ГрафикиНазначений.Интервалы КАК Интервалы
		|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ГрафикиНазначений.Значения КАК ЗначениеНачала
		|		ПО Интервалы.Ссылка = ЗначениеНачала.Ссылка
		|			И Интервалы.Начало = ЗначениеНачала.КлючЗначения
		|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ГрафикиНазначений.Значения КАК ЗначениеОкончания
		|		ПО Интервалы.Ссылка = ЗначениеОкончания.Ссылка
		|			И Интервалы.Окончание = ЗначениеОкончания.КлючЗначения
		|ГДЕ
		|	Интервалы.Ссылка = &Смена
		|	И Интервалы.НомерСтроки = 1"
	);
	Запрос_.УстановитьПараметр("Смена", Смена);
	Результат_ = Запрос_.Выполнить();
	Выборка_ = Результат_.Выбрать();
	Если Выборка_.Следующий() Тогда
		Период_.Начало = ПустаяДата_ + (Выборка_.Начало - НачалоДня(Выборка_.Начало));
		Период_.Окончание = ПустаяДата_ + (Выборка_.Окончание - НачалоДня(Выборка_.Окончание));
		Период_.НачалоВключено = Выборка_.НачалоВключено;
		Период_.ОкончаниеВключено = Выборка_.ОкончаниеВключено;
	КонецЕсли;
	Возврат Период_;
КонецФункции

// Отобрать из переданных дат, те которые соответствуют графику назначения. 
//
// Возвращаемое значение:
//   Массив
// 
Функция ОтобратьДатыПоГрафику(ГрафикНазначений, Даты) Экспорт
	__ПРОВЕРКА__(ЗначениеЗаполнено(ГрафикНазначений), "Не указан график назначений");
	ГрафикНазначений_ = ГрафикНазначений.ПолучитьОбъект();
	Возврат ГрафикНазначений_.ОтобратьДаты(Даты);
КонецФункции

// Получить суточные и недельные периодические графики. 
//
// Возвращаемое значение:
//   Массив
// 
Функция ПолучитьСуточныеИНедельныеПериодическиеГрафики() Экспорт
	Запрос_ = Новый Запрос(
		"ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ГрафикиНазначений.Ссылка
		|ИЗ
		|	Справочник.ГрафикиНазначений КАК ГрафикиНазначений
		|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ГрафикиНазначений.Интервалы КАК ПрочиеИнтервалы
		|		ПО ГрафикиНазначений.Ссылка = ПрочиеИнтервалы.Ссылка
		|			И (ПрочиеИнтервалы.ТипИнтервала <> ЗНАЧЕНИЕ(Перечисление.ГрафикиНазначенийТипыИнтервалов.ПериодическийИнтервал))
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.ГрафикиНазначений.Интервалы КАК ПериодическиеИнтервалы
		|			ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.ГрафикиНазначений.Значения КАК Периоды
		|			ПО ПериодическиеИнтервалы.Ссылка = Периоды.Ссылка
		|				И ПериодическиеИнтервалы.Период = Периоды.КлючЗначения
		|		ПО ГрафикиНазначений.Ссылка = ПериодическиеИнтервалы.Ссылка
		|			И (ПериодическиеИнтервалы.ТипИнтервала = ЗНАЧЕНИЕ(Перечисление.ГрафикиНазначенийТипыИнтервалов.ПериодическийИнтервал))
		|ГДЕ
		|	ГрафикиНазначений.ПометкаУдаления = ЛОЖЬ
		|	И ПрочиеИнтервалы.Ссылка ЕСТЬ NULL 
		|	И Периоды.ЕдиницаИзмерения В (ЗНАЧЕНИЕ(Перечисление.ГрафикиНазначенийЕдиницыИзмерения.Сутки), ЗНАЧЕНИЕ(Перечисление.ГрафикиНазначенийЕдиницыИзмерения.Недели))"
	);
	Результат_ = Запрос_.Выполнить();
	Возврат ?(Результат_.Пустой(), Новый Массив, Результат_.Выгрузить().ВыгрузитьКолонку("Ссылка"));
КонецФункции

#КонецОбласти