﻿&НаКлиенте
Перем ВыполняетсяЗакрытие;

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// Пропускаем инициализацию, чтобы гарантировать получение формы при передаче параметра "АвтоТест".
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	
	Если Объект.ИспользоватьОтборПоОрганизациям Тогда
		
		ПравилоОтбораСправочников = "Отбор";
		
	Иначе
		
		Если Объект.ВыгружатьУправленческуюОрганизацию Тогда
			ПравилоОтбораСправочников = "УпрОрганизация";
		Иначе
			ПравилоОтбораСправочников = "БезОтбора";
		КонецЕсли;
		
	КонецЕсли;
	
	//Инициализируем доступность ссылок установки дата запрета редактирования и даты запрета получения
	МассивЭлементов = Новый Массив();
	МассивЭлементов.Добавить("ГруппаДатаЗапретаРедактированияДанных");
	МассивЭлементов.Добавить("ГруппаУстановитьДатуЗапретаПолучения");

	ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементовФормы(
		Элементы,
		МассивЭлементов,
		"Видимость",
		Истина);
		
	МассивЭлементов = Новый Массив();
	МассивЭлементов.Добавить("УстановитьДатуЗапретаПолученияДанных");
	МассивЭлементов.Добавить("УстановитьДатуЗапретаИзменений");
	
	ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(
		Элементы,
		МассивЭлементов,
		"Доступность",
		ПравоДоступа("Изменение", Метаданные.РегистрыСведений.ДатыЗапретаИзменения));
	
	УстановитьВидимостьНаСервере();
	ОбновитьНаименованиеКомандФормы();
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии()
	
	Оповестить("Запись_УзелПланаОбмена");
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	
	ОбновитьИнтерфейс();
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВыбора(ВыбранноеЗначение, ИсточникВыбора)
	
	ОбновитьДанныеОбъекта(ВыбранноеЗначение);
	Модифицированность = Истина;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	Если Не ВыполняетсяЗакрытие Тогда
		
		Если Модифицированность Тогда
			
			Отказ = Истина;
			
			ОписаниеОповещения = Новый ОписаниеОповещения("ПередЗакрытиемЗавершение", ЭтотОбъект);
			
			ТекстВопроса = НСтр("ru = 'Изменены настройки узла ""%УзелИнформационнойБазы%""'");
			ТекстВопроса = СтрЗаменить(ТекстВопроса, "%УзелИнформационнойБазы%", Строка(Объект.Наименование));
			ТекстВопроса = ТекстВопроса + Символы.ПС + НСтр("ru = 'Закрыть без сохранения изменений?'");
			
			ПоказатьВопрос(ОписаниеОповещения, ТекстВопроса, РежимДиалогаВопрос.ДаНет,, КодВозвратаДиалога.Да);
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытиемЗавершение(Ответ, ДополнительныеПараметры) Экспорт
	
	Если Ответ = КодВозвратаДиалога.Да Тогда
		ВыполняетсяЗакрытие = Истина;
		ЭтотОбъект.Закрыть();
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ФлагИспользоватьОтборПоОрганизациямПриИзменении(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ПереключательДокументыОтправлятьВручнуюПриИзменении(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ПереключательДокументыОтправлятьАвтоматическиПриИзменении(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ПереключательОтбораСправочниковСОтборомПриИзменении(Элемент)
	УстрановитьУсловияОрганиченияСинхронизации();
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ПереключательОтбораСправочниковБезОтбораСУпрПриИзменении(Элемент)
	УстрановитьУсловияОрганиченияСинхронизации();
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ПереключательОтбораСправочниковБезОтбораБезУпрПриИзменении(Элемент)
	УстрановитьУсловияОрганиченияСинхронизации();
	УстановитьВидимостьНаСервере();
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ОткрытьСписокВыбранныхОрганизаций(Команда)
	
	Если Не Объект.ВыгружатьУправленческуюОрганизацию
		И Ложь Тогда
		
		КоллекцияФильтров = Новый Массив;
		
		Накладываемыефильтры = Новый Структура();
		Накладываемыефильтры.Вставить("РеквизитОтбора",    "Ссылка");
		Накладываемыефильтры.Вставить("Условие",           "<>");
		Накладываемыефильтры.Вставить("ИмяПараметра",      "ИсключаемаяСсылка");
		Накладываемыефильтры.Вставить("ЗначениеПараметра", 
			ПредопределенноеЗначение("Справочник.Организации.ОсновнаяОрганизация"));
		
		КоллекцияФильтров.Добавить(Накладываемыефильтры);
		
	Иначе
		
		КоллекцияФильтров = Неопределено;
		
	КонецЕсли;
	
	ПараметрыФормы = Новый Структура();
	ПараметрыФормы.Вставить("ИмяЭлементаФормыДляЗаполнения",          "Организации");
	ПараметрыФормы.Вставить("ИмяРеквизитаЭлементаФормыДляЗаполнения", "Организация");
	ПараметрыФормы.Вставить("ИмяТаблицыВыбора",                       "Справочник.Организации");
	ПараметрыФормы.Вставить("ЗаголовокФормыВыбора",                   НСтр("ru = 'Выберите организации для отбора:'"));
	ПараметрыФормы.Вставить("МассивВыбранныхЗначений",                СформироватьМассивВыбранныхЗначений(ПараметрыФормы));
	ПараметрыФормы.Вставить("ПараметрыВнешнегоСоединения",            Неопределено);
	ПараметрыФормы.Вставить("КоллекцияФильтров",                      КоллекцияФильтров);
	
	ОткрытьФорму("ОбщаяФорма.ФормаВыбораДополнительныхУсловий",
		ПараметрыФормы,
		ЭтотОбъект);
	
КонецПроцедуры


#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#Область Прочее

&НаСервере
Процедура УстановитьВидимостьНаСервере()
	
	//Страница правила получения данных
	Элементы.ГруппаСтраницыСоздаватьПартнеровДляНовыхКонтрагентов.ТекущаяСтраница = 
			Элементы.ГруппаСтраницаСоздаватьПартнеровДляНовыхКонтрагентов;
		
	
	//Страница правила отправки данных
	ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(
		Элементы,
		"ДатаНачалаВыгрузкиДокументов",
		"Доступность",
		Объект.ПравилаОтправкиДокументов = "АвтоматическаяСинхронизация");


	//Видимость отбора по организациям
	ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(
		Элементы,
		"ГруппаСтраницыОтборПоОрганизациям",
		"Видимость",
		ПолучитьФункциональнуюОпцию("ИспользоватьНесколькоОрганизаций"));
		
	Если Элементы.ГруппаСтраницыОтборПоОрганизациям.Видимость Тогда
		
		
		Элементы.ГруппаСтраницыОтборПоОрганизациям.ТекущаяСтраница = 
			Элементы.ГруппаСтраницаОтборПоОрганизациям;
		
		Если Объект.ИспользоватьОтборПоОрганизациям Тогда
			
			Элементы.ГруппаСтраницыКомандаВыбораОрганизаций.ТекущаяСтраница = 
				Элементы.ГруппаСтраницаКомандаВыбратьОрганизации;
			
		Иначе
			
			Элементы.ГруппаСтраницыКомандаВыбораОрганизаций.ТекущаяСтраница = 
				Элементы.ГруппаСтраницаКомандаВыбратьОрганизацииПустая;
			
		КонецЕсли;
		
		//Видимость управленческой организации и вариантаотбора
		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(
			Элементы,
			"ГруппаВыборУправленческойОрганизации",
			"Видимость",
			Истина);
		
		Если Элементы.ГруппаВыборУправленческойОрганизации.Видимость Тогда
			
			Элементы.ГруппаСтраницыВариантВыбораОтбора.ТекущаяСтраница = 
				Элементы.ГруппаСтраницаПереключательОтбора;
			
		Иначе
			
			Элементы.ГруппаСтраницыВариантВыбораОтбора.ТекущаяСтраница = 
				Элементы.ГруппаСтраницаФлагОтбора;
			
		КонецЕсли;
		

		
	КонецЕсли;


КонецПроцедуры

&НаСервере
Процедура ОбновитьДанныеОбъекта(СтруктураПараметров)
	
	Объект[СтруктураПараметров.ИмяТаблицыДляЗаполнения].Очистить();
	
	СписокВыбранныхЗначений = ПолучитьИзВременногоХранилища(СтруктураПараметров.АдресТаблицыВоВременномХранилище);
	
	Если СписокВыбранныхЗначений.Количество() > 0 Тогда
		СписокВыбранныхЗначений.Колонки.Представление.Имя = СтруктураПараметров.ИмяКолонкиДляЗаполнения;
		Объект[СтруктураПараметров.ИмяТаблицыДляЗаполнения].Загрузить(СписокВыбранныхЗначений);
	КонецЕсли;
	
	ОбновитьНаименованиеКомандФормы();
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьНаименованиеКомандФормы()
	
	//Обновим заголовок выбранных организаций
	Если Объект.Организации.Количество() > 0 Тогда
		
		ВыбранныеОрганизации = Объект.Организации.Выгрузить().ВыгрузитьКолонку("Организация");
		
		НовыйЗаголовокОрганизаций = СтрСоединить(ВыбранныеОрганизации, ",");
		
	Иначе
		
		НовыйЗаголовокОрганизаций = НСтр("ru = 'Выбрать организации'");
		
	КонецЕсли;
	
	Элементы.ОткрытьСписокВыбранныхОрганизаций.Заголовок = НовыйЗаголовокОрганизаций;
	
	
КонецПроцедуры

&НаСервере
Функция СформироватьМассивВыбранныхЗначений(ПараметрыФормы)
	
	ТабличнаяЧасть           = Объект[ПараметрыФормы.ИмяЭлементаФормыДляЗаполнения];
	ТаблицаВыбранныхЗначений = ТабличнаяЧасть.Выгрузить(,ПараметрыФормы.ИмяРеквизитаЭлементаФормыДляЗаполнения);
	МассивВыбранныхЗначений  = ТаблицаВыбранныхЗначений.ВыгрузитьКолонку(ПараметрыФормы.ИмяРеквизитаЭлементаФормыДляЗаполнения);
	
	Возврат МассивВыбранныхЗначений;
	
КонецФункции

&НаКлиенте
Процедура УстрановитьУсловияОрганиченияСинхронизации()
	
	Если ПравилоОтбораСправочников = "Отбор" Тогда
		
		Объект.ИспользоватьОтборПоОрганизациям = Истина;
		Объект.ВыгружатьУправленческуюОрганизацию = Ложь;
		
	ИначеЕсли ПравилоОтбораСправочников = "УпрОрганизация" Тогда
		
		Объект.ИспользоватьОтборПоОрганизациям = Ложь;
		Объект.ВыгружатьУправленческуюОрганизацию = Истина;
		
	ИначеЕсли ПравилоОтбораСправочников = "БезОтбора" Тогда
		
		Объект.ИспользоватьОтборПоОрганизациям = Ложь;
		Объект.ВыгружатьУправленческуюОрганизацию = Ложь;
		
	КонецЕсли;
	
КонецПроцедуры


#КонецОбласти

#КонецОбласти

ВыполняетсяЗакрытие = Ложь;