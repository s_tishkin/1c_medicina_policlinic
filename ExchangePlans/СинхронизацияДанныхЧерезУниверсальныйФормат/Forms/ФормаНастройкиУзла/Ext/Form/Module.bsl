﻿
//////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("АвтоТест") Тогда
		Возврат;
	КонецЕсли;
	
	МетаданныеОбъекта = Метаданные.ПланыОбмена.СинхронизацияДанныхЧерезУниверсальныйФормат;
	ИмяПланаОбмена = МетаданныеОбъекта.Имя;
	ОбменДаннымиСервер.ФормаНастройкиУзлаПриСозданииНаСервере(ЭтотОбъект, ИмяПланаОбмена);
	
	ДокументУстановкиЦен = Метаданные.Документы.УстановкаЦенНоменклатуры;
	Для Каждого ЭлементСостава Из МетаданныеОбъекта.Состав Цикл
		Если ОбщегоНазначения.ЭтоДокумент(ЭлементСостава.Метаданные) И ЭлементСостава.Метаданные <> ДокументУстановкиЦен Тогда
			ДоступныеДокументы.Добавить(ОбщегоНазначения.ИдентификаторОбъектаМетаданных(ЭлементСостава.Метаданные));
		КонецЕсли;
	КонецЦикла;
	
	УстановитьВидимостьНаСервере();
	ОбновитьНаименованиеКомандФормы();
	
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаВыбора(ВыбранноеЗначение, ИсточникВыбора)
	
	Если ИсточникВыбора.ИмяФормы = "ОбщаяФорма.ФормаВыбораДополнительныхУсловий" Тогда
		ОбновитьДанныеОбъекта(ВыбранноеЗначение);
		Модифицированность = Истина;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗакрытием(Отказ, ЗавершениеРаботы, ТекстПредупреждения, СтандартнаяОбработка)
	
	ОбменДаннымиКлиент.ФормаНастройкиПередЗакрытием(Отказ, ЭтотОбъект, ЗавершениеРаботы);
	
КонецПроцедуры 

#КонецОбласти // ОбработчикиСобытийФормы

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ
#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ЗаписатьИЗакрыть(Команда)
	
	Если Не ЗаписатьИЗакрытьНаСервере() Тогда
		Возврат;
	КонецЕсли;
	
	ОбменДаннымиКлиент.ФормаНастройкиУзлаКомандаЗакрытьФорму(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьСписокВыбранныхОрганизаций(Команда)
	
	ПараметрыФормы = ПараметрыФормыВыбораЭлементовОтбораПоУмолчанию();
	ПараметрыФормы.ИмяЭлементаФормыДляЗаполнения          = "Организации";
	ПараметрыФормы.ИмяРеквизитаЭлементаФормыДляЗаполнения = "Организация";
	ПараметрыФормы.ИмяТаблицыВыбора                       = "Справочник.Организации";
	ПараметрыФормы.ЗаголовокФормыВыбора                   = НСтр("ru = 'Выберите организации для отбора:'");
	
	ОткрытьСписокВыбранныхЭлементов(ПараметрыФормы);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьСписокОтправляемыхВидовЦен(Команда)
	
	ПараметрыФормы = ПараметрыФормыВыбораЭлементовОтбораПоУмолчанию();
	ПараметрыФормы.ИмяЭлементаФормыДляЗаполнения          = "ВидыЦенНоменклатуры";
	ПараметрыФормы.ИмяРеквизитаЭлементаФормыДляЗаполнения = "ВидЦенНоменклатуры";
	ПараметрыФормы.ИмяТаблицыВыбора                       = "Справочник.ВидыЦен";
	ПараметрыФормы.ЗаголовокФормыВыбора                   = НСтр("ru = 'Выберите виды цен для отправки:'");
	
	ОткрытьСписокВыбранныхЭлементов(ПараметрыФормы);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьСписокВыбранныхДокументов(Команда)
	
	ПараметрыФормы = ПараметрыФормыВыбораЭлементовОтбораПоУмолчанию();
	ПараметрыФормы.ИмяЭлементаФормыДляЗаполнения          = "ВыгружаемыеДокументы";
	ПараметрыФормы.ИмяРеквизитаЭлементаФормыДляЗаполнения = "ИдентификаторДокумента";
	ПараметрыФормы.ИмяТаблицыВыбора                       = "Справочник.ИдентификаторыОбъектовМетаданных";
	ПараметрыФормы.ЗаголовокФормыВыбора                   = НСтр("ru = 'Выберете типы документов для отбора:'");
	
	ОтборСправочника = Новый Структура;
	ОтборСправочника.Вставить("РеквизитОтбора"   , "Ссылка");
	ОтборСправочника.Вставить("Условие"          , "В");
	ОтборСправочника.Вставить("ИмяПараметра"     , "ДоступныеДокументы");
	ОтборСправочника.Вставить("ЗначениеПараметра", ДоступныеДокументы);
	
	КоллекцияФильтров = Новый Массив;
	КоллекцияФильтров.Добавить(ОтборСправочника);
	
	ПараметрыФормы.КоллекцияФильтров = КоллекцияФильтров;
	
	ОткрытьСписокВыбранныхЭлементов(ПараметрыФормы);
	
КонецПроцедуры

#КонецОбласти // ОбработчикиКомандФормы

//////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ФОРМЫ
#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура ПереключательОтправлятьНСИАвтоматическиПриИзменении(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ПереключательОтправлятьНСИПоНеобходимостиПриИзменении(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ПереключательОтправлятьНСИНикогдаПриИзменении(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ПереключательДокументыОтправлятьАвтоматическиПриИзменении(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ПереключательДокументыОтправлятьВручнуюПриИзменении(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ПереключательДокументыНеОтправлятьПриИзменении(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ФлагИспользоватьОтборПоОрганизациямПриИзменении(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ФлагОтправлятьВидыЦенНоменклатурыПриИзменении(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

&НаКлиенте
Процедура ИспользоватьОтборПоТипамДокументовПриИзменении(Элемент)
	УстановитьВидимостьНаСервере();
КонецПроцедуры

#КонецОбласти // ОбработчикиСобытийЭлементовФормы

//////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ
#Область СлужебныеПроцедурыИФункции

#Область ФормированиеСпискаЭлементовОтбора

&НаКлиенте
Процедура ОткрытьСписокВыбранныхЭлементов(ПараметрыФормы)
	
	ПараметрыФормы.Вставить("МассивВыбранныхЗначений", СформироватьМассивВыбранныхЗначений(ПараметрыФормы));
	
	ОткрытьФорму("ОбщаяФорма.ФормаВыбораДополнительныхУсловий", ПараметрыФормы, ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Функция ПараметрыФормыВыбораЭлементовОтбораПоУмолчанию()
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("ИмяЭлементаФормыДляЗаполнения"         , "");
	ПараметрыФормы.Вставить("ИмяРеквизитаЭлементаФормыДляЗаполнения", "");
	ПараметрыФормы.Вставить("ИмяТаблицыВыбора"                      , "");
	ПараметрыФормы.Вставить("ЗаголовокФормыВыбора"                  , "");
	ПараметрыФормы.Вставить("ПараметрыВнешнегоСоединения"           , Неопределено);
	ПараметрыФормы.Вставить("КоллекцияФильтров"                     , Неопределено);
	ПараметрыФормы.Вставить("ТолькоПросмотр"                        , ТолькоПросмотр);
	
	Возврат ПараметрыФормы;
	
КонецФункции

&НаСервере
Функция СформироватьМассивВыбранныхЗначений(ПараметрыФормы)
	
	Возврат ЭтотОбъект[ПараметрыФормы.ИмяЭлементаФормыДляЗаполнения].Выгрузить(
				,
				ПараметрыФормы.ИмяРеквизитаЭлементаФормыДляЗаполнения)
					.ВыгрузитьКолонку(ПараметрыФормы.ИмяРеквизитаЭлементаФормыДляЗаполнения);
	
КонецФункции

#КонецОбласти // ФормированиеСпискаЭлементовОтбора

&НаСервере
Процедура УстановитьВидимостьНаСервере()
	
	// Отбор по организациям
	Если ПравилаОтправкиСправочников = "НеСинхронизировать" Тогда
		Элементы.ГруппаСтраницыОтборПоОрганизациям.ТекущаяСтраница = Элементы.ГруппаСтраницаОтборПоОрганизациямПустая;
		Элементы.ОткрытьСписокВыбранныхОрганизаций.Видимость = Ложь;
	Иначе
		Элементы.ГруппаСтраницыОтборПоОрганизациям.ТекущаяСтраница = Элементы.ГруппаСтраницаОтборПоОрганизациям;
		Элементы.ОткрытьСписокВыбранныхОрганизаций.Видимость = ИспользоватьОтборПоОрганизациям;
	КонецЕсли;
	
	// Выгрузка цен номенклатуры
	Если ПравилаОтправкиСправочников = "НеСинхронизировать"
	 Или ПравилаОтправкиСправочников = "СинхронизироватьПоНеобходимости" Тогда
		Элементы.ГруппаСтраницыОтправляемыеВидыЦен.ТекущаяСтраница = Элементы.ГруппаСтраницаОтправляемыеВидыЦенПустая;
		Элементы.ОткрытьСписокОтправляемыхВидовЦен.Видимость = Ложь;
	Иначе
		Элементы.ГруппаСтраницыОтправляемыеВидыЦен.ТекущаяСтраница = Элементы.ГруппаСтраницаОтправляемыеВидыЦен;
		Элементы.ОткрытьСписокОтправляемыхВидовЦен.Видимость = ВыгружатьЦеныНоменклатуры;
	КонецЕсли;
	
	// Отбор по типам документов
	Если ПравилаОтправкиДокументов = "НеСинхронизировать" Тогда
		Элементы.ГруппаСтраницыОтборПоТипамДокументов.ТекущаяСтраница = Элементы.ГруппаСтраницаОтборПоТипамДокументовПустая;
		Элементы.ОткрытьСписокВыбранныхДокументов.Видимость = Ложь;
	Иначе
		Элементы.ГруппаСтраницыОтборПоТипамДокументов.ТекущаяСтраница = Элементы.ГруппаСтраницаОтборПоТипамДокументов;
		Элементы.ОткрытьСписокВыбранныхДокументов.Видимость = ИспользоватьОтборПоТипамДокументов;
	КонецЕсли;
	
	Элементы.ДатаНачалаВыгрузкиДокументов.Доступность = (ПравилаОтправкиДокументов = "АвтоматическаяСинхронизация");
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьНаименованиеКомандФормы()
	
	// Обновление заголовка команды выбора организаций для ограничения миграции.
	Если Организации.Количество() > 0 Тогда
		НовыйЗаголовокОрганизаций = СтрСоединить(Организации.Выгрузить().ВыгрузитьКолонку("Организация"), ", ");
	Иначе
		НовыйЗаголовокОрганизаций = НСтр("ru = 'Выбрать организации'");
	КонецЕсли;
	
	Элементы.ОткрытьСписокВыбранныхОрганизаций.Заголовок = НовыйЗаголовокОрганизаций;
	
	// Обновление заголовка команды выбора видов цен для выгрузки цен номенклатуры.
	Если ВидыЦенНоменклатуры.Количество() > 0 Тогда
		НовыйЗаголовокВидовЦен = СтрСоединить(ВидыЦенНоменклатуры.Выгрузить().ВыгрузитьКолонку("ВидЦенНоменклатуры"), ", ");
	Иначе
		НовыйЗаголовокВидовЦен = НСтр("ru = 'Выбрать виды цен'");
	КонецЕсли;
	
	Элементы.ОткрытьСписокОтправляемыхВидовЦен.Заголовок = НовыйЗаголовокВидовЦен;
	
	// Обновление заголовка команды выбора типов документов для ограничения миграции.
	Если ВыгружаемыеДокументы.Количество() > 0 Тогда
		НовыйЗаголовокДокументы = СтрСоединить(ВыгружаемыеДокументы.Выгрузить().ВыгрузитьКолонку("ИдентификаторДокумента"), ", ");
	Иначе
		НовыйЗаголовокДокументы = НСтр("ru = 'Выбрать типы документов'");
	КонецЕсли;
	
	Элементы.ОткрытьСписокВыбранныхДокументов.Заголовок = НовыйЗаголовокДокументы;
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьДанныеОбъекта(СтруктураПараметров)
	
	ЭтотОбъект[СтруктураПараметров.ИмяТаблицыДляЗаполнения].Очистить();
	
	СписокВыбранныхЗначений = ПолучитьИзВременногоХранилища(СтруктураПараметров.АдресТаблицыВоВременномХранилище);
	
	Если СписокВыбранныхЗначений.Количество() > 0 Тогда
		СписокВыбранныхЗначений.Колонки.Представление.Имя = СтруктураПараметров.ИмяКолонкиДляЗаполнения;
		ЭтотОбъект[СтруктураПараметров.ИмяТаблицыДляЗаполнения].Загрузить(СписокВыбранныхЗначений);
	КонецЕсли;
	
	ОбновитьНаименованиеКомандФормы();
	
КонецПроцедуры

&НаСервере
Функция ЗаписатьИЗакрытьНаСервере()
	
	Если Не ПроверитьЗаполнение() Тогда
		Возврат Ложь;
	КонецЕсли;
	
	Если ПравилаОтправкиСправочников = "НеСинхронизировать" Тогда
		ИспользоватьОтборПоОрганизациям = Ложь;
		ВыгружатьЦеныНоменклатуры       = Ложь;
	ИначеЕсли ПравилаОтправкиСправочников = "СинхронизироватьПоНеобходимости" Тогда
		ВыгружатьЦеныНоменклатуры = Ложь;
	КонецЕсли;
	
	Если ПравилаОтправкиДокументов = "НеСинхронизировать" Тогда
		ИспользоватьОтборПоТипамДокументов = Ложь;
	КонецЕсли;
	
	Если ПравилаОтправкиДокументов <> "АвтоматическаяСинхронизация" Тогда
		ДатаНачалаВыгрузкиДокументов = Дата(1,1,1,0,0,0);
	КонецЕсли;
	
	Если Не ИспользоватьОтборПоОрганизациям И Организации.Количество() <> 0 Тогда
		Организации.Очистить();
	ИначеЕсли Организации.Количество() = 0 И ИспользоватьОтборПоОрганизациям Тогда
		ИспользоватьОтборПоОрганизациям = Ложь;
	КонецЕсли;
	
	Если Не ВыгружатьЦеныНоменклатуры И ВидыЦенНоменклатуры.Количество() <> 0 Тогда
		ВидыЦенНоменклатуры.Очистить();
	ИначеЕсли ВидыЦенНоменклатуры.Количество() = 0 И ВыгружатьЦеныНоменклатуры Тогда
		ВыгружатьЦеныНоменклатуры = Ложь;
	КонецЕсли;
	
	Если Не ИспользоватьОтборПоТипамДокументов И ВыгружаемыеДокументы.Количество() <> 0 Тогда
		ВыгружаемыеДокументы.Очистить();
	ИначеЕсли ВыгружаемыеДокументы.Количество() = 0 И ИспользоватьОтборПоТипамДокументов Тогда
		ИспользоватьОтборПоТипамДокументов = Ложь;
	КонецЕсли;
	
	Возврат Истина;
	
КонецФункции

#КонецОбласти // СлужебныеПроцедурыИФункции
