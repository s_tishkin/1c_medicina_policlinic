﻿
#Область ПрограммныйИнтерфейс

// Функция возвращает код половой принадлежности в соответствии с кодировкой ФФОМС.
//
// Возвращает строку
//
// Параметры:
//  УсловиеОказанияМедПомощи  - ПеречислениеСсылка.ПолПациентов - условия оказания медицинской помощи.
//
// Возвращаемое значение:
//	Строка - Код в соответствии с кодировкой ФФОС при выбранном ПолПациентов.
//
Функция КодПоОМС(ПолПациента) Экспорт
	Если 		ПолПациента = Перечисления.ПолПациентов.Женский Тогда
		возврат "2";
	ИначеЕсли 	ПолПациента = Перечисления.ПолПациентов.Мужской Тогда
		возврат "1";
	КонецЕсли;
	возврат "";
КонецФункции 

#КонецОбласти