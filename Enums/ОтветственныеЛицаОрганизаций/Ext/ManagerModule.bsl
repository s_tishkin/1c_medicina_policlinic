﻿
#Область ПрограммныйИнтерфейс


// Обработчик события "ОбработкаПолученияДанныхВыбора".
//
Процедура ОбработкаПолученияДанныхВыбора(ДанныеВыбора, Параметры, СтандартнаяОбработка)
	
	СтруктурнаяЕдиница = Неопределено;
	
	Если Параметры.Свойство("СтруктурнаяЕдиница",СтруктурнаяЕдиница) Тогда
		
		СтандартнаяОбработка = Ложь;
		ДанныеВыбора = Новый СписокЗначений();
		ТипСтруктурнойЕдиницы = ТипЗнч(СтруктурнаяЕдиница);

		Если ТипСтруктурнойЕдиницы = Тип("СправочникСсылка.Кассы") Тогда
			ДанныеВыбора.Добавить(Перечисления.ОтветственныеЛицаОрганизаций.Кассир);
		ИначеЕсли ТипСтруктурнойЕдиницы = Тип("СправочникСсылка.Организации") Тогда
			
			ДанныеВыбора.Добавить(Перечисления.ОтветственныеЛицаОрганизаций.Руководитель);
			ДанныеВыбора.Добавить(Перечисления.ОтветственныеЛицаОрганизаций.ГлавныйБухгалтер);
			
		КонецЕсли;
		
	КонецЕсли;

КонецПроцедуры // ОбработкаПолученияДанныхВыбора()


#КонецОбласти