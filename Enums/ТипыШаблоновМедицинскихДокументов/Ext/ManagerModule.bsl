﻿
#Область ПрограммныйИнтерфейс


Процедура ОбработкаПолученияДанныхВыбора(ДанныеВыбора, Параметры, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	ДанныеВыбора = Новый СписокЗначений;
	ДанныеВыбора.Добавить(Перечисления.ТипыШаблоновМедицинскихДокументов.EPFШаблон);
	ДанныеВыбора.Добавить(Перечисления.ТипыШаблоновМедицинскихДокументов.EPFШаблонСМакетом);
	ДанныеВыбора.Добавить(Перечисления.ТипыШаблоновМедицинскихДокументов.HTMLШаблон);
	ДанныеВыбора.Добавить(Перечисления.ТипыШаблоновМедицинскихДокументов.XHTMLШаблон);
	Если Константы.ИспользоватьОбработкиШМДКонфигурации.Получить() Тогда
		ДанныеВыбора.Добавить(Перечисления.ТипыШаблоновМедицинскихДокументов.ОбработкаШаблон);
		ДанныеВыбора.Добавить(Перечисления.ТипыШаблоновМедицинскихДокументов.ОбработкаШаблонСМакетом);
	КонецЕсли;	
КонецПроцедуры


#КонецОбласти