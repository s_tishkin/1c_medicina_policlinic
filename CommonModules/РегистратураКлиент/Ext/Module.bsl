﻿#Область ПрограммныйИнтерфейс

#Область ФормыМедицинскихКартИПациентов

////
 // Процедура: ОткрытьПациента
 //   Открытие карты пациента.
 // Параметры:
 //   Пациент - СправочникСсылка.Картотека -
 //     Ссылка на пациента, которого следует открыть.
 //   РасширенныйФункционал - Булево -
 //     Определяем форму открытия карты:
 //     Истина - стандартная форма с расширенными возможностями,
 //     Ложь - форма без расширенных возможностей.
 //   Владелец - УправляемаяФорма - Владелец формы - форма или элемент управления другой формы.
 //   Окно - ОкноКлиентскогоПриложения; ВариантОткрытияОкна - Окно приложения, в котором будет открыта форма.
///
Процедура ОткрытьКартуИлиПациента(Знач МедицинскаяКартаИлиПациент, Знач ОсновнаяФорма = Истина, Знач Владелец = Неопределено, Знач Окно = Неопределено) Экспорт
	
	Если ТипЗнч(МедицинскаяКартаИлиПациент) = Тип("СправочникСсылка.МедицинскиеКарты") Тогда
		ОткрытьМедицинскуюКарту(МедицинскаяКартаИлиПациент, ОсновнаяФорма, Владелец, Окно);
	ИначеЕсли ТипЗнч(МедицинскаяКартаИлиПациент) = Тип("СправочникСсылка.Картотека") Тогда
		ОткрытьПациента(МедицинскаяКартаИлиПациент, Владелец, Окно);
	КонецЕсли;
	
КонецПроцедуры

////
 // Процедура: ОткрытьМедицинскуюКарту
 //   Открытие карты пациента.
 // Параметры:
 //   МедицинскаяКарта - СправочникСсылка.МедицинскиеКарты -
 //     Ссылка на карту, которую следует открыть.
 //   РасширенныйФункционал - Булево -
 //     Определяем форму открытия карты:
 //     Истина - стандартная форма с расширенными возможностями,
 //     Ложь - форма без расширенных возможностей.
 //   Владелец - УправляемаяФорма - Владелец формы - форма или элемент управления другой формы.
 //   Окно - ОкноКлиентскогоПриложения; ВариантОткрытияОкна - Окно приложения, в котором будет открыта форма.
///
Процедура ОткрытьМедицинскуюКарту(Знач МедицинскаяКарта, Знач ОсновнаяФорма = Истина, Знач Владелец = Неопределено, Знач Окно = Неопределено) Экспорт
	
	УидЗамераПроизводительности_ = ОценкаПроизводительностиКлиент.НачатьЗамерВремени(Истина, "ОткрытиеМедицинскойКартыПациента");
	ПараметрыФормы = Новый Структура("МедицинскаяКарта", МедицинскаяКарта);
	
	Если ОсновнаяФорма = Истина Тогда
		ИмяФормы = "Обработка.МедицинскаяКартаПациента.Форма";
	Иначе
		ПараметрыФормы.Вставить("РежимПросмотра", Истина);
		ИмяФормы = "Обработка.МедицинскаяКартаПациента.Форма.ПросмотрМедицинскойКарты";
	КонецЕсли;
	
	ФормаМедицинскойКарты_ = ПолучитьФорму(ИмяФормы, ПараметрыФормы, Владелец,, Окно);
	
	Если ФормаМедицинскойКарты_ = Неопределено ИЛИ ФормаМедицинскойКарты_.Открыта() Тогда
		Возврат;
	КонецЕсли;
	
	ФормаМедицинскойКарты_.Открыть();
	
	Пациент_ = Регистратура.ПациентКарты(МедицинскаяКарта);
	
	// ПослеОткрытия. Выводятся предупреждения, информирующие сообщения, проходит контроль данных.
	ПослеОткрытияМедицинскойКарты(Пациент_, МедицинскаяКарта);
	
КонецПроцедуры

////
 // Процедура: ОткрытьПациента
 //   Открытие карты пациента.
 // Параметры:
 //   Пациент - СправочникСсылка.Картотека -
 //     Ссылка на пациента, которого следует открыть.
///
Процедура ОткрытьПациента(Знач Пациент,  Знач Владелец = Неопределено, Знач Окно = Неопределено) Экспорт
	
	ОценкаПроизводительностиКлиент.НачатьЗамерВремени(Истина, "ОткрытиеПациента");
	ПараметрыФормы = Новый Структура("Ключ", Пациент);
	ОткрытьФорму(
		"Справочник.Картотека.ФормаОбъекта",
		ПараметрыФормы,
		Владелец,,
		Окно
	);
	
	// ПослеОткрытия. Выводятся предупреждения, информирующие сообщения, проходит контроль данных.
	ПослеОткрытияМедицинскойКарты(Пациент, Неопределено);
	
КонецПроцедуры

// Вызывается после открытия медицинской карты, чтобы выполнить проверки,
// отобразить предупреждения и задать вопросы.
//
// Параметры:
//  МедицинскаяКарта  - СправочникСсылка.МедицинскиеКарты - карта,
//                 которую открыли.
//
Процедура ПослеОткрытияМедицинскойКарты(Знач Пациент, Знач МедицинскаяКарта) Экспорт
	
	ПараметрКоманды = Новый Массив();
	ПараметрКоманды.Добавить(Пациент);
	
	НастройкаУведомления = НастройкиКлиентСервер.Получить("УведомлениеОбОтсутствииСогласияНаОбработкуПД");
	Если НастройкаУведомления = Истина Тогда
		Согласие_ = ЗащитаПерсональныхДанныхПоликлиникаСервер.ДействующееСогласиеНаОбработкуПерсональныхДанных(
					Пациент,
					,
					ОбщегоНазначенияКлиент.ДатаСеанса()
		);
		Если Согласие_ = Неопределено Тогда
			ОповещениеОтвет = Новый ОписаниеОповещения("ПослеОтветаНаВопросОПечатиСогласия", ЭтотОбъект, ПараметрКоманды);
			ПоказатьВопрос(ОповещениеОтвет, СообщенияПользователю.Получить("ФПДП_НетДействующегоСогласияНаОбработкуПерсональныхДанных"),
							РежимДиалогаВопрос.ДаНет
							);
		КонецЕсли;
	КонецЕсли;
	
	ОбработчикиСобытия = ОбщегоНазначенияКлиент.ОбработчикиСлужебногоСобытия("Регистратура.МедицинскиеКарты\ПослеОткрытияМедицинскойКарты");
	Если ОбработчикиСобытия.Количество() > 0 Тогда
		Для каждого Обработчик Из ОбработчикиСобытия Цикл
			Обработчик.Модуль.ПослеОткрытияМедицинскойКарты(Пациент, МедицинскаяКарта);
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры

// определяет необходимость отображения вопроса о печати согласия на обработку ПД
//
// Параметры:
//  МедицинскаяКарта  - СправочникСсылка.МедицинскиеКарты - карта,
//                 которую открыли.
//
Процедура ПослеОтветаНаВопросОПечатиСогласия(Ответ, ДополнительныеПараметры) Экспорт
	
	Если Ответ = КодВозвратадиалога.Да Тогда
		ПараметрыОткрытия = Новый Структура("ОбъектыПечати,Форма", ДополнительныеПараметры, Неопределено);
		ЗащитаПерсональныхДанныхКлиент.ОткрытьФормуСогласиеНаОбработкуПерсональныхДанных(ПараметрыОткрытия);
	КонецЕсли;
	
КонецПроцедуры

// Выбирает пациента и/или карту
//
// Параметры:
//  Оповещение  - ОписаниеОповещения - <описание параметра>
//                 <продолжение описания параметра>
//  Владелец  - УправляемаяФорма,ЭлементФормы - <описание параметра>
//                 <продолжение описания параметра>
//  ВыборПациента  - Булево - признак разрешения
//                 выбора пациента
//  ВыборКарты  - Булево - признак разрешения
//                 выбора карты
//  ПараметрыФормы  - Структура - дополнительные параметры
//                 открытия формы поиска
//
Процедура ВыбратьПациентаКарту(Знач Оповещение,
								Знач Владелец,
								Знач ВыборПациента = Истина,
								Знач ВыборКарты = Истина,
								Знач ПараметрыФормы = Неопределено
								) Экспорт
	
	Если ПараметрыФормы = Неопределено Тогда
		ПараметрыФормы = Новый Структура;
	КонецЕсли;
	
	ПараметрыФормы.Вставить("ВыборПациента", ВыборПациента);
	ПараметрыФормы.Вставить("ВыборКарты", ВыборКарты);
	
	ОткрытьФорму("Обработка.ИдентификацияПациента.Форма",
				ПараметрыФормы,
				Владелец,
				Истина,,,
				Оповещение,
				РежимОткрытияОкнаФормы.БлокироватьОкноВладельца
			);
	
КонецПроцедуры

#КонецОбласти

#Область РаботаСПолисом

////
 // Функция: СформироватьПредставлениеПолиса
 //   По структуре полиса формирует его строковое представление.
 //   Добавляет слова Серия, Номер и т.п.
 //
 // Параметры:
 //   стр
 //     Структура, поля которой соответствуют реквизитам регистра 
 //     Полисы страхования пациентов
 //
 // Возврат:
 //   Строка, представление полиса.
 ///
Функция СформироватьПредставлениеПолиса(стр) Экспорт
	Перем Рез, Шапка, Темп;
	Если стр = Неопределено Тогда
		Возврат "";
	КонецЕсли;
	Шапка = "Полис "+стр.ВидПолиса+". "+Символы.ПС;
	Рез = "";
	Если ЗначениеЗаполнено(стр.СерияПолиса) Тогда
		Рез = "Серия: "+стр.СерияПолиса + ", "; 	
	КонецЕсли;
	Если ЗначениеЗаполнено(стр.НомерПолиса) Тогда
		Рез = Рез+"№: "+стр.НомерПолиса+", "; 	
	КонецЕсли;
	Если ЗначениеЗаполнено(стр.Страховщик) Тогда
		Рез = Рез+стр.Страховщик+" "; 	
	КонецЕсли;
	Если ЗначениеЗаполнено(стр.ДоговорПолиса) Тогда
		Если стр.Свойство("ДоговорПолисаИмя",Темп) Тогда
			Рез = Рез+" ("+стр.ДоговорПолисаИмя+") "; 	
		Иначе
			Рез = Рез+" ("+Регистратура.ПолучитьНаименованиеДоговораПолиса(стр.ДоговорПолиса)+") ";
		КонецЕсли;		
	КонецЕсли;
	Если ЗначениеЗаполнено(стр.СрокДействияПолиса) Тогда
		Рез = Рез + "до: " +Формат(стр.СрокДействияПолиса,"ДЛФ=D");
	КонецЕсли;
	
	Возврат (Шапка + Рез);
КонецФункции // СформироватьПредставлениеПолиса()

#КонецОбласти

#Область ПечатьМедицинскойКарты

////
 // Процедура: ПечатьЛицевойСтраницыМедицинскойКарты
 //   Определяет каким образом нужно печатать лицевую сторону мед. карты.
 //   Для этого выполняется запрос к серверу.
 //
 // Параметры:
 //   МедКарта
 //     Ссылка на мед. карту.
 //   Источник
 //     Форма с которой была вызвана печать карты.
 ///
Процедура ПечатьЛицевойСтраницыМедицинскойКарты(МедКарта, Источник) Экспорт
	Перем ТабличныйДок,Обработка,ПараметрыОбработки;
	
	ДанныеОбработки = Регистратура.ПолучитьДанныеПечатнойФормыЛицевойСтороны(МедКарта);
	Обработка = ДанныеОбработки.Обработка;
	
	МассивОбъектов = Новый Массив;
	МассивОбъектов.Добавить(МедКарта);
	
	Если ЗначениеЗаполнено(Обработка) Тогда
		Если ТипЗнч(Обработка)=Тип("Строка") Тогда
			
			// Задано имя внутренней обработки или отчета
				
			ВыполнитьПечатьЛицевойСтраницыМедицинскойКарты(МассивОбъектов, Обработка, Источник);
			
		Иначе
			
			ПараметрыОткрытия = Новый Структура("ИсточникДанных, ПараметрыИсточника");
			ПараметрыОткрытия.ИсточникДанных     = Обработка;
			ПараметрыОткрытия.ПараметрыИсточника = Новый Структура("ИдентификаторКоманды, ОбъектыНазначения");
			ПараметрыОткрытия.ПараметрыИсточника.ИдентификаторКоманды = ДанныеОбработки.Идентификатор;
			ПараметрыОткрытия.ПараметрыИсточника.ОбъектыНазначения    = МассивОбъектов;
			
			ОткрытьФорму("ОбщаяФорма.ПечатьДокументов", ПараметрыОткрытия);
			
		КонецЕсли;
	Иначе
		Если ТипЗнч(ДанныеОбработки.ДокументКарты) = Тип("ДокументСсылка.РегистрацияПациента") Тогда
			
			Если ДанныеОбработки.ЭтоСтационарнаяКарта = Истина И 
				ОбщиеМеханизмыПовтИсп.ИспользоватьСтационарныеУсловия() Тогда
				Обработка = "Отчет.СтатистическаяКартаСтационарногоБольногоФорма003у";
			Иначе				
				Обработка = "Отчет.СтатистическаяКартаАмбулаторногоБольного";
			КонецЕсли;
			
			ВыполнитьПечатьЛицевойСтраницыМедицинскойКарты(МассивОбъектов, Обработка, Источник);
			
		Иначе
		    //Пока ничего не делаем
			
		
		КонецЕсли;
		
	КонецЕсли;
КонецПроцедуры

////
 //	Процедура: ВыполнитьПечатьЛицевойСтороныМедицинскойКарты
 //   Выполнение команды печати 
 // Параметры:
 //   МассивОбъектов
 //     Массив с ссылкой на медкарту
 //   Обработка
 //     Имя обработки или отчета, формирующих печатную форму.
 //   Источник.
 //     Форма с которой была вызвана печать карты.
 ///
Процедура ВыполнитьПечатьЛицевойСтраницыМедицинскойКарты(МассивОбъектов, Обработка, Источник)
	УправлениеПечатьюКлиент.ВыполнитьКомандуПечати(Обработка, "ЛицеваяСтраницаМедицинскойКарты", 
			МассивОбъектов, Источник, Неопределено);
КонецПроцедуры // ВыполнитьПечатьЛицевойСтороныМедицинскойКарты()

// Осуществляет печать основной печатной формы медицинской карты, 
// используется как обработчик печати лицеовой страницы медицинской карты.
//
// Параметры:
//	ПараметрыПечати - структура параметров обработки команды печати,
//		должна содержать поля ОбъектыПечати и Форма.
//
Функция ЛицеваяСтраницыМедицинскойКарты(ПараметрыПечати) Экспорт
	
	ОбъектыПечати = ПараметрыПечати.ОбъектыПечати;
	ФормаИсточник = ПараметрыПечати.Форма;
	
	Для каждого ПечатаемыйОбъект Из ОбъектыПечати Цикл
		
		ПечатьЛицевойСтраницыМедицинскойКарты(ПечатаемыйОбъект, ФормаИсточник);
		
	КонецЦикла;
	
КонецФункции

#КонецОбласти

#Область ДубликатыУслуг

// Проверка заказа номенклатуры, которая могла быть уже заказаны ранее.
// 
Функция ПроверитьЗаказДубликатовУслуги(МедицинскаяКарта, Пациент, Номенклатура, ОповещениеПослеПроверки) Экспорт
	ОК_ = Истина;
	Если ЗначениеЗаполнено(МедицинскаяКарта) Или ЗначениеЗаполнено(Пациент) Тогда
		Дубликаты_ = УправлениеЗаказамиУслугами.ПолучитьДубликатыЗаказовНоменклатуры(
			МедицинскаяКарта, Пациент, ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(Номенклатура)
		);
		Если Дубликаты_.Количество() > 0 Тогда
			ОК_ = Ложь;
			Параметры_ = Новый Структура(
				"Номенклатура, Дата", 
				Дубликаты_[0].Номенклатура, Формат(Дубликаты_[0].Дата, "ДФ=dd.MM.yyyy ЧЧ:мм")
			);
			Сообщение_ = СообщенияПользователю.Получить("ОформлениеЗаказов_УслугаУжеЗаказанаДата", Параметры_);
			ПоказатьВопрос(ОповещениеПослеПроверки, Сообщение_, РежимДиалогаВопрос.ДаНет, , КодВозвратаДиалога.Нет);
		КонецЕсли;
	КонецЕсли;
	Возврат ОК_;
КонецФункции

// Проверка заказа номенклатуры услуг, которые могла быть уже заказаны ранее.
// 
Функция ПроверитьЗаказДубликатовУслуг(
	МедицинскаяКарта, Пациент, НоменклатураУслуг, НоменклатураДубликатов, ОповещениеПослеПроверки
) Экспорт
	ОК_ = Истина;
	Если ЗначениеЗаполнено(МедицинскаяКарта) Или ЗначениеЗаполнено(Пациент) Тогда
		Дубликаты_ = УправлениеЗаказамиУслугами.ПолучитьДубликатыЗаказовНоменклатуры(
			МедицинскаяКарта, Пациент, НоменклатураУслуг
		);
		Если Дубликаты_.Количество() > 1 Тогда
			ОК_ = Ложь;
			Услуги_ = "";
			Для Каждого Дубликат_ Из Дубликаты_ Цикл
				НоменклатураДубликатов.Добавить(Дубликат_.Номенклатура);
				Услуги_ = Услуги_ + Дубликат_.Номенклатура + " - " + 
					Формат(Дубликат_.Дата, "ДФ=dd.MM.yyyy ЧЧ:мм") + Символы.ПС;	
			КонецЦикла;
			Услуги_ = СокрЛП(Услуги_);
			Параметры_ = Новый Структура("Услуги", Услуги_);
			Сообщение_ = СообщенияПользователю.Получить("ОформлениеЗаказов_УслугиУжеЗаказаныРанее", Параметры_);
			ПоказатьВопрос(ОповещениеПослеПроверки, Сообщение_, РежимДиалогаВопрос.ДаНет, , КодВозвратаДиалога.Нет);
		ИначеЕсли Дубликаты_.Количество() = 1 Тогда
			ОК_ = Ложь;
			НоменклатураДубликатов.Добавить(Дубликаты_[0].Номенклатура);
			Параметры_ = Новый Структура(
				"Номенклатура, Дата", 
				Дубликаты_[0].Номенклатура, Формат(Дубликаты_[0].Дата, "ДФ=dd.MM.yyyy ЧЧ:мм")
			);
			Сообщение_ = СообщенияПользователю.Получить("ОформлениеЗаказов_УслугаУжеЗаказанаДата", Параметры_);
			ПоказатьВопрос(ОповещениеПослеПроверки, Сообщение_, РежимДиалогаВопрос.ДаНет, , КодВозвратаДиалога.Нет);
		КонецЕсли;
	КонецЕсли;
	Возврат ОК_;
КонецФункции

#КонецОбласти

#КонецОбласти