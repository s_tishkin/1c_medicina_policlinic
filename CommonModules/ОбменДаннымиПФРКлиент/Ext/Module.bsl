﻿#Область ПрограммныйИнтерфейс

Процедура ЗапростьСНИЛС // Экспорт, 7 параметров
(
	Фамилия,
	Имя,
	Отчество,
	ДатаРождения,
	Пол,
	СтандартнаяОбработка = Истина,
	ОписаниеОповещения = Неопределено
)
Экспорт
	
	Если Не ОбщегоНазначенияКлиент.ПодсистемаСуществует("СтандартныеПодсистемы.ЭлектроннаяПодпись") Тогда
		Возврат;
	КонецЕсли;
	
	ВходящиеПараметры = Новый Структура(
		"СтандартнаяОбработка, ОписаниеОповещения",
		СтандартнаяОбработка, ОписаниеОповещения
	);
	
	Если Пол = ПредопределенноеЗначение("Перечисление.ПолПациентов.Женский") Тогда
		ПолСтрокой_ = "F";
	ИначеЕсли Пол = ПредопределенноеЗначение("Перечисление.ПолПациентов.Мужской") Тогда
		ПолСтрокой_ = "M";
	Иначе
		__ПРОВЕРКА__(Ложь, СтрШаблон("ОбменДаннымиПФР: Не задан пол: %1.", Пол));
	КонецЕсли;
	
	ИдПодписи_ = "id-" + Строка(Новый УникальныйИдентификатор);
	wsu_Id_ = "id-" + Строка(Новый УникальныйИдентификатор);
	
	ПараметрыССервера_ = ОбменДаннымиПФРСервер.ПолучитьПараметрыСообщения();
	
	ТегMessage_ = СформироватьMessage( , , ПараметрыССервера_.ТекущаяУниверсальнаяДата);
	ТегMessageData_ = СформироватьMessageData(Фамилия, Имя, Отчество, ДатаРождения, ПолСтрокой_);
	ТегEnvelope_ = СформироватьEnvelope(ИдПодписи_, wsu_Id_, ТегMessage_, ТегMessageData_);
	
	ПараметрыXMLDSig_ = ПолучитьПараметрыXMLDSig(ИдПодписи_, wsu_Id_);
	
	Данные_ = Новый Структура(
		"ПараметрыXMLDSig, КонвертSOAP",
		ПараметрыXMLDSig_, ТегEnvelope_
	);
	Операция_ = НСтр("ru = 'Подписание сообщения в ПФР'");
	ЗаголовокДанных_ = НСтр("ru = 'Получение страхового номера ПФР (СНИЛС)'");
	
	ОписаниеДанных_ = Новый Структура(
		"Данные, Операция,  Представление,  БезПодтверждения, ПоказатьКомментарий",
		Данные_, Операция_, ЗаголовокДанных_, Истина,           Ложь
	);
	
	Если ЗначениеЗаполнено(ПараметрыССервера_.Сертификат) Тогда
		ОписаниеДанных_.Вставить(
			"ОтборСертификатов",
			АлгоритмыДляКоллекций.СоздатьМассив(ПараметрыССервера_.Сертификат)
		);
	КонецЕсли;
	
	ОбработкаРезультата_ = Новый ОписаниеОповещения(
		"ПослеУстановкиПодписи",
		ЭтотОбъект,
		ВходящиеПараметры
	);
	
	ЭлектроннаяПодписьКлиент.Подписать(ОписаниеДанных_, , ОбработкаРезультата_);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеМетоды

Процедура ПослеУстановкиПодписи(Результат, ВходящиеПараметры) Экспорт
	
	__ПОЛЯ__(ВходящиеПараметры, "СтандартнаяОбработка, ОписаниеОповещения", "ОбменДаннымиПФР: Нет нужных полей.");
	
	Успех_ = Истина
		И ТипЗнч(Результат) = Тип("Структура")
		И Результат.Свойство("Успех")
		И Результат.Успех = Истина;
	;
	
	Если Не Истина = Успех_ Тогда
		
		Если Истина = ВходящиеПараметры.СтандартнаяОбработка Тогда
			Сообщить(НСтр("ru = 'Не удалось подписать сообщение в ПФР.'"));
		КонецЕсли;
		
		Если ТипЗнч(ВходящиеПараметры.ОписаниеОповещения) = Тип("ОписаниеОповещения") Тогда
			ВыполнитьОбработкуОповещения(ВходящиеПараметры.ОписаниеОповещения, Неопределено);
		КонецЕсли;
		
		Возврат;
		
	КонецЕсли;
	
	__ПОЛЯ__(Результат, "СвойстваПодписи", "ОбменДаннымиПФР: Нет свойств подписи.");
	__ПОЛЯ__(Результат.СвойстваПодписи, "Подпись", "ОбменДаннымиПФР: Нет подписи.");
	
	ЗапросSOAP_ = Результат.СвойстваПодписи.Подпись;
	
	РезультатОбмена_ = ОбменДаннымиПФРСервер.ВыполнитьОбменДанными(ЗапросSOAP_);
	
	ОбработатьРезультатОбмена(РезультатОбмена_, ВходящиеПараметры);
	
КонецПроцедуры

Функция ПолучитьПараметрыXMLDSig(ИдПодписи, wsu_Id)
	
	ПараметрыXMLDSig_ = ЭлектроннаяПодписьКлиентСервер.ПараметрыXMLDSig();
	
	ПараметрыXMLDSig_.XPathSignedInfo = СтрШаблон(
		"(//. | //@* | //namespace::*)[
		|	ancestor-or-self::*[
		|		local-name()='SignedInfo' and namespace-uri()='%2'
		|		and ancestor::*[
		|			local-name()='Signature' and namespace-uri()='%2'
		|			and @Id='%1'
		|		]
		|	]
		|]",
		ИдПодписи,
		xmlns.ds()
	);
	
	ПараметрыXMLDSig_.XPathПодписываемыйТег = СтрШаблон(
		"(//. | //@* | //namespace::*)[
		|	ancestor-or-self::*[
		|		attribute::*[
		|			local-name()='Id'
		|			and string()='%1'
		|			and namespace-uri()='%2'
		|		]
		|	]
		|]",
		wsu_Id,
		xmlns.wsu()
	);
	
	ПараметрыXMLDSig_.ИмяАлгоритмаПодписи = "GOST R 34.10-2001";   //"sha1RSA";
	ПараметрыXMLDSig_.OIDАлгоритмаПодписи = "1.2.643.2.2.3";       //"1.2.840.113549.1.1.5";
	ПараметрыXMLDSig_.ИмяАлгоритмаХеширования = "GOST R 34.11-94"; // "MD5";
	ПараметрыXMLDSig_.OIDАлгоритмаХеширования = "1.2.643.2.2.9";   // "1.2.840.113549.2.5";
	
	Возврат ПараметрыXMLDSig_;
	
КонецФункции

Функция СформироватьMessage(КодОтправителя = "PFRF01001", ИмяОтправителя = "Пенсионный фонд РФ", Дата)
	
	Сообщение_ = СтрШаблон(
		"<Message xmlns=""%1"">
		|	<Sender>
		|		<Code>%2</Code>
		|		<Name>%3</Name>
		|	</Sender>
		|	<Recipient>
		|		<Code>PFRF01001</Code>
		|		<Name>Пенсионный фонд РФ</Name>
		|	</Recipient>
		|	<Service>
		|		<Mnemonic>SNILS_BY_DATA</Mnemonic>
		|		<Version>1.00</Version>
		|	</Service>
		|	<TypeCode>GSRV</TypeCode>
		|	<Status>REQUEST</Status>
		|	<Date>%4</Date>
		|	<ExchangeType>2</ExchangeType>
		|</Message>",
		xmlns.gu(),
		КодОтправителя,
		ИмяОтправителя,
		Формат(Дата, "ДФ=yyyy-MM-ddTHH:mm:ssZ")
	);
	
	Возврат Сообщение_;
	
КонецФункции

Функция СформироватьMessageData(Фамилия, Имя, Отчество, ДатаРождения, ПолСтрокой)
	
	__ПРОВЕРКА__(ПолСтрокой = "M" Или ПолСтрокой = "F", СтрШаблон("ОбменДаннымиПФР: Неправильные пол в запросе: %1.", ПолСтрокой));
	
	ТегPatronymic_ = "";
	Если ЗначениеЗаполнено(Отчество) Тогда
		ТегPatronymic_ = СтрШаблон(
			"<pfr:Patronymic>%1</pfr:Patronymic>",
			Отчество
		);
	КонецЕсли;
	
	Запрос_ = СтрШаблон(
		"<MessageData xmlns=""%1"">
		|	<AppData>
		|		<request
		|			 xmlns:snils=""http://snils-by-data.skmv.rstyle.com""
		|			 xmlns:pfr=""http://pfr.skmv.rstyle.com"">
		|			<snils:fio>
		|				<pfr:LastName>%2</pfr:LastName>
		|				<pfr:FirstName>%3</pfr:FirstName>
		|				%4
		|			</snils:fio>
		|			<snils:gender>%5</snils:gender>
		|			<snils:birthDate>%6</snils:birthDate>
		|		</request>
		|	</AppData>
		|</MessageData>",
		xmlns.gu(),
		Фамилия,
		Имя,
		ТегPatronymic_,
		ПолСтрокой,
		Формат(ДатаРождения, "ДФ=dd-MM-yyyy")
	);
	
	Возврат Запрос_;
	
КонецФункции

Функция СформироватьEnvelope(ИдПодписи, wsu_Id, ТегMessage, ТегMessageData)
	
	Конверт_ = СтрШаблон(
		"<soap:Envelope
		|	 xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
		|	<soap:Header>
		|		<wsse:Security
		|			 soap:actor=""http://smev.gosuslugi.ru/actors/smev""
		|			 soap:mustUnderstand=""0""
		|			 xmlns:wsse=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"">
		|			<wsse:BinarySecurityToken
		|				 EncodingType=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary""
		|				 ValueType=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3""
		|				 xmlns:wsu=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd""
		|				 wsu:Id=""id-BinarySecurityToken"">%1</wsse:BinarySecurityToken>
		|			<Signature
		|				 Id=""%4""
		|				 xmlns=""http://www.w3.org/2000/09/xmldsig#"">
		|				<SignedInfo>
		|					<CanonicalizationMethod
		|						 Algorithm=""http://www.w3.org/2001/10/xml-exc-c14n#""
		|					/>
		|					<SignatureMethod
		|						 Algorithm=""http://www.w3.org/2001/04/xmldsig-more#gostr34102001-gostr3411""
		|					/>
		|					<Reference
		|						 URI=""#%5"">
		|						<Transforms>
		|							<Transform
		|								 Algorithm=""http://www.w3.org/2001/10/xml-exc-c14n#""
		|							/>
		|						</Transforms>
		|						<DigestMethod
		|							 Algorithm=""http://www.w3.org/2001/04/xmldsig-more#gostr3411""
		|						/>
		|						<DigestValue>%2</DigestValue>
		|					</Reference>
		|				</SignedInfo>
		|				<SignatureValue>%3</SignatureValue>
		|				<KeyInfo>
		|					<wsse:SecurityTokenReference>
		|						<wsse:Reference
		|							 URI=""#id-BinarySecurityToken""
		|							 ValueType=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3""
		|						/>
		|					</wsse:SecurityTokenReference>
		|				</KeyInfo>
		|			</Signature>
		|		</wsse:Security>
		|	</soap:Header>
		|	<soap:Body
		|		 xmlns:wsu=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd""
		|		 wsu:Id=""%5"">
		|		<SnilsByDataRequest
		|			 xmlns=""http://snils-by-data.skmv.rstyle.com"">
		|			%6
		|			%7
		|		</SnilsByDataRequest>
		|	</soap:Body>
		|</soap:Envelope>",
		"%BinarySecurityToken%",
		"%DigestValue%",
		"%SignatureValue%",
		ИдПодписи,
		wsu_Id,
		ТегMessage,
		ТегMessageData
	);
	
	// Конверт_ = СтрЗаменить(Конверт_, Символы.ПС, "");
	// Конверт_ = СтрЗаменить(Конверт_, Символы.ВК, "");
	// Конверт_ = СтрЗаменить(Конверт_, Символы.Таб, "");
	
	Возврат Конверт_;
	
КонецФункции

Процедура ОбработатьРезультатОбмена(Результат, ВходящиеПараметры)
	
	__ПОЛЯ__(ВходящиеПараметры, "СтандартнаяОбработка, ОписаниеОповещения", "ОбменДаннымиПФР: Неправильные входящие параметры.");
	
	Успех_ = Истина
		И ТипЗнч(Результат) = Тип("Структура")
		И Результат.Свойство("Успех")
		И Истина = Результат.Успех
		И Результат.Свойство("СтраховойНомерПФР")
		И Тип("Строка") = ТипЗнч(Результат.СтраховойНомерПФР)
		И 14 = СтрДлина(Результат.СтраховойНомерПФР)
	;
	
	Если Истина = Успех_ Тогда
		
		СтраховойНомерПФР_ = Результат.СтраховойНомерПФР;
		
		Если Истина = ВходящиеПараметры.СтандартнаяОбработка Тогда
			ПоказатьОповещениеПользователя(
				СтрШаблон(НСтр("ru = 'Получен страховой номер ПФР: %1.'"), СтраховойНомерПФР_),
			);
		КонецЕсли;
		
		Если Тип("ОписаниеОповещения") = ТипЗнч(ВходящиеПараметры.ОписаниеОповещения) Тогда
			ВыполнитьОбработкуОповещения(ВходящиеПараметры.ОписаниеОповещения, СтраховойНомерПФР_);
		КонецЕсли;
		
		Возврат;
		
	КонецЕсли;
	
	ТекстОшибки_ = НСтр("ru = 'Неизвестная ошибка при обращении к серверу Пенсионного фонда.'");
	Если Тип("Структура") = ТипЗнч(Результат) Тогда
		
		Если Результат.Свойство("ТекстОшибки") Тогда
			__ПРОВЕРКА__(ЗначениеЗаполнено(Результат.ТекстОшибки), "ОбменДаннымиПФР: Не заполнен текст ошибки");
			ТекстОшибки_ = Результат.ТекстОшибки;
		КонецЕсли;
		
		Если Результат.Свойство("Дубликаты") Тогда
			
			__ТИП__(Результат.Дубликаты, "Массив", "ОбменДаннымиПФР: Неожиданный тип дубликатов.");
			__ПРОВЕРКА__(Не 0 = Результат.Дубликаты.Количество(), "ОбменДаннымиПФР: Неожиданное количество дубликатов.");
			
			ТекстОшибки_ = "";
			
			Для Каждого Дубликат_ Из Результат.Дубликаты Цикл
				
				__ПОЛЯ__(Дубликат_, "СтраховойНомерПФР, Документ, МестоРождения", "ОбменДаннымиПФР: Не все поля дубликата.");
				
				ТекстОшибки_ = ТекстОшибки_ + СтрШаблон(
					"%1
					|Документ: %2
					|Место рождения: %3
					|
					|",
					Дубликат_.СтраховойНомерПФР,
					Дубликат_.Документ,
					Дубликат_.МестоРождения
				);
				
			КонецЦикла;
			
		КонецЕсли;
		
	КонецЕсли;
	
	Если Истина = ВходящиеПараметры.СтандартнаяОбработка Тогда
		ПоказатьОповещениеПользователя(ТекстОшибки_);
	КонецЕсли;
	
	Если Тип("ОписаниеОповещения") = ТипЗнч(ВходящиеПараметры.ОписаниеОповещения) Тогда
		ВыполнитьОбработкуОповещения(ВходящиеПараметры.ОписаниеОповещения, Неопределено);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

