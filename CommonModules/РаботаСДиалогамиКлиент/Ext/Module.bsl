﻿
#Область ПрограммныйИнтерфейс

// Вызывается при выборе дате актуальности или даты события
//  Изменяет значение даты актуальности или даты события в зависимости от выбранного значения
//
// Параметры:
//  ВыбранноеЗначение		 - Строка	 - выбранное значение отбора
//  СтандартнаяОбработка	 - Булево			 - Флаг стандартной обработки события
//  Форма					 - УправляемаяФорма	 - форма списка документов
//  Список					 - ДинамическийСписок - Список документов
//  ИмяРеквизитаАктуальность - Строка			 - имя реквизита строки отбора по актуальности
//  ИмяРеквизитаДатаСобытия	 - Строка			 - имя реквизита даты, на которую документ будет просрочен
//
Процедура ПриВыбореОтбораПоАктуальности(ВыбранноеЗначение, 
	СтандартнаяОбработка, Форма, Список, ИмяРеквизитаАктуальность, ИмяРеквизитаДатаСобытия) Экспорт
	
	Если ВыбранноеЗначение = "Истекает на дату" Тогда
		
		СтандартнаяОбработка = Ложь;
		ДополнительныеПараметры = Новый Структура;
		ДополнительныеПараметры.Вставить("Форма", Форма);
		ДополнительныеПараметры.Вставить("Список",            Список);
		ДополнительныеПараметры.Вставить("ИмяРеквизитаАктуальность",      ИмяРеквизитаАктуальность);
		ДополнительныеПараметры.Вставить("ИмяРеквизитаДатаСобытия",       ИмяРеквизитаДатаСобытия);
		
		Оповещение = Новый ОписаниеОповещения("ПриВыбореОтбораПоАктуальностиЗавершение", ЭтотОбъект, ДополнительныеПараметры);
		
		ПоясняющийТекст = НСтр("ru='Выберите дату для вывода просроченных документов:'") + " ";
		ОткрытьФорму(
			"ОбщаяФорма.ВыборДаты",
			Новый Структура("ПоясняющийТекст, НачальноеЗначение", ПоясняющийТекст),
			,
			,
			,
			,
			Оповещение,
			РежимОткрытияОкнаФормы.БлокироватьВесьИнтерфейс);
		
		Возврат;
	Иначе
		ДатаСобытия = Дата(1,1,1);
	КонецЕсли;
	
КонецПроцедуры

// Проверяет наличие выделенных в списке строк
//
// Параметры:
// Список - ДинамическийСписок
//
// Возвращаемое значение:
// Булево - Ложь - если в списке нет выделенных строк, иначе Истина.
//
Функция ПроверитьНаличиеВыделенныхВСпискеСтрок(Список) Экспорт
	
	МассивСсылок = Новый Массив;
	
	Для н_ = 0 По Список.ВыделенныеСтроки.Количество()-1 Цикл
		Если ТипЗнч(Список.ВыделенныеСтроки[н_]) <> Тип("СтрокаГруппировкиДинамическогоСписка") Тогда
			МассивСсылок.Добавить(Список.ВыделенныеСтроки[н_]);
		КонецЕсли;
	КонецЦикла;
	
	Если МассивСсылок.Количество() = 0 Тогда
		ПоказатьПредупреждение(, СообщенияПользователю.Получить("Общие_КомандаНеМожетБытьВыполненаДляУказанногоОбъекта"));
		Возврат Ложь;
	КонецЕсли;

	Возврат Истина;
	
КонецФункции // ПроверитьНаличиеВыделенныхВСпискеСтрок()

// Проверяет наличие выделенных в списке ссылок и возвращает массив
//
// Параметры:
// 		Список - ДинамическийСписок
//
// Возвращаемое значение:
// 		Массив - Массив выделенных с списке ссылок.
//
Функция ПроверитьПолучитьВыделенныеВСпискеСсылки(Список) Экспорт
	
	МассивСсылок = Новый Массив;
	
	Для н_ = 0 По Список.ВыделенныеСтроки.Количество()-1 Цикл
		Если ТипЗнч(Список.ВыделенныеСтроки[н_]) <> Тип("СтрокаГруппировкиДинамическогоСписка") Тогда
			МассивСсылок.Добавить(Список.ВыделенныеСтроки[н_]);
		КонецЕсли;
	КонецЦикла;
	
	Если МассивСсылок.Количество() = 0 Тогда
		ПоказатьПредупреждение(, СообщенияПользователю.Получить("Общие_КомандаНеМожетБытьВыполненаДляУказанногоОбъекта"));
	КонецЕсли;
	
	Возврат МассивСсылок;
	
КонецФункции // ПроверитьПолучитьВыделенныеВСпискеСсылки()

// Проверяет наличие в списке строк
//
// Параметры:
// 		Список - ДинамическийСписок
// 		ПредставлениеСписка - 
// Возвращаемое значение:
// 		Булево - Ложи когда количество строк = 0, иначе истина.
//
Функция ПроверитьНаличиеСтрокВСписке(Список, ПредставлениеСписка) Экспорт
	
	Если Список.Количество() = 0 Тогда
		ПоказатьПредупреждение(, СообщенияПользователю.Получить("Общие_НеВведеноНиОднойСтрокиВСписок",Новый Структура("ПредставлениеСписка",ПредставлениеСписка)));
		Возврат Ложь;
	КонецЕсли;
	
	Возврат Истина;
	
КонецФункции

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти
