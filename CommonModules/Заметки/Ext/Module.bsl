﻿
#Область ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// Подсистема "Заметки"
// 
////////////////////////////////////////////////////////////////////////////////

// Производит запись в служебных регистр информации о наличии заметки по предмету.
//
// Параметры совпадают с параметрами обработчика при записи у элемента справочника.
Процедура ПроверитьНаличиеЗаметокПоПредмету(Источник, Отказ) Экспорт
	
	Если Источник.ОбменДанными.Загрузка Тогда 
		Возврат; 
	КонецЕсли;
	
	УстановитьПривилегированныйРежим(Истина);
	
	Если Не ЗначениеЗаполнено(Источник.Предмет) Тогда
		Возврат;
	КонецЕсли;
	
	Запрос = Новый Запрос;
	
	ТекстЗапроса = 
	"ВЫБРАТЬ РАЗРЕШЕННЫЕ ПЕРВЫЕ 1
	|	Заметки.Ссылка
	|ИЗ
	|	Справочник.Заметки КАК Заметки
	|ГДЕ
	|	Заметки.Предмет = &Предмет
	|	И Заметки.Автор = &Пользователь
	|	И Заметки.ПометкаУдаления = ЛОЖЬ";
	
	Запрос.УстановитьПараметр("Предмет", Источник.Предмет);
	Запрос.УстановитьПараметр("Пользователь", Источник.Автор);
	
	Запрос.Текст = ТекстЗапроса;
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	ЕстьЗаметки = Выборка.Количество() > 0;

	НаборЗаписей = РегистрыСведений.НаличиеЗаметокПоПредмету.СоздатьНаборЗаписей();

	НаборЗаписей.Отбор.Автор.Установить(Источник.Автор);
	НаборЗаписей.Отбор.Предмет.Установить(Источник.Предмет);
	
	Если ЕстьЗаметки Тогда 
		Если НаборЗаписей.Количество() = 0 Тогда
			НоваяЗапись = НаборЗаписей.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяЗапись, Источник);
			НоваяЗапись.ЕстьЗаметки = Истина;				
		Иначе
			Для Каждого Запись Из НаборЗаписей Цикл
				Запись.ЕстьЗаметки = Истина;
			КонецЦикла;
		КонецЕсли;
	Иначе
		НаборЗаписей.Очистить();
	КонецЕсли;

	НаборЗаписей.Записать();
	
КонецПроцедуры

////
 // Функция: ТекстПоУмолчанию
 //   Служит для получения текста по умолчанию.
 //
 //	Параметры:
 //
 // Возврат:
 //   Строка
 ///
Функция ТекстПоУмолчанию() Экспорт
	Возврат СообщенияПользователю.Получить("Заметки_ТекстОтсутствияЗаметкиПоУмолчанию");
КонецФункции

////
 // Процедура: ВосстановитьКомментарийКЗаказу
 //   Восстановить комментарий к заказу.
 ///
Функция ПолучитьПоследнююЗаметкуПредмета(Предмет) Экспорт
	ЗапросДата_ = Новый Запрос(
		"ВЫБРАТЬ
		|	МАКСИМУМ(Заметки.ДатаИзменения) КАК Дата
		|ИЗ
		|	Справочник.Заметки КАК Заметки
		|ГДЕ
		|	Заметки.Предмет = &Предмет
		|	И Заметки.ПометкаУдаления = ЛОЖЬ
		|СГРУППИРОВАТЬ ПО
		|	Заметки.Предмет"
	);
	ЗапросЗаметка_ = Новый Запрос(
		"ВЫБРАТЬ
		|	Заметки.Ссылка КАК Ссылка
		|ИЗ
		|	Справочник.Заметки КАК Заметки
		|ГДЕ
		|	Заметки.Предмет = &Предмет
		|	И Заметки.ДатаИзменения = &ДатаИзменения"
	);
	ЗапросДата_.УстановитьПараметр("Предмет", Предмет);
	ВыборкаДата_ = ЗапросДата_.Выполнить().Выбрать();
	Если ВыборкаДата_.Следующий() Тогда
		ЗапросЗаметка_.УстановитьПараметр("Предмет", Предмет);
		ЗапросЗаметка_.УстановитьПараметр("ДатаИзменения", ВыборкаДата_.Дата);
		ВыборкаЗаметки_ = ЗапросЗаметка_.Выполнить().Выбрать();
		Если ВыборкаЗаметки_.Следующий() Тогда
			Возврат ВыборкаЗаметки_.Ссылка;
		КонецЕсли;
	КонецЕсли;
	Возврат Справочники.Заметки.ПустаяСсылка();
КонецФункции

#КонецОбласти