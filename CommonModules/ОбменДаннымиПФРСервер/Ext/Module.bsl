﻿#Область ПрограммныйИнтерфейс

/// Возвращает текущую универсальную дату и ссылку на сертификат организации
Функция ПолучитьПараметрыСообщения() Экспорт
	
	Параметры_ = Новый Структура(
		"ТекущаяУниверсальнаяДата, Сертификат",
		ТекущаяУниверсальнаяДата(), ПолучитьСертификатОрганизации()
	);
	
	Возврат Параметры_;
	
КонецФункции

/// Отправляет на сервер ПФР запрос и возвращает ответ в виде структуры
Функция ВыполнитьОбменДанными(Запрос) Экспорт
	
	АдресСервера_ = МестоположениеWSDL();
	РесурсНаСервере_ = "";
	ПозицияЗавершенияСхемы_ = СтрНайти(АдресСервера_, "://");
	Если ПозицияЗавершенияСхемы_ > 0 Тогда
		СтрокаURI_ = Сред(АдресСервера_, ПозицияЗавершенияСхемы_ + 3);
		ПозицияЗавершенияСоединения = СтрНайти(СтрокаURI_, "/");
		ДлинаСоединения_ = ?(ПозицияЗавершенияСоединения > 0, ПозицияЗавершенияСоединения - 1, СтрДлина(СтрокаURI_));
		РесурсНаСервере_ = Сред(АдресСервера_, ПозицияЗавершенияСхемы_ + 3 + ДлинаСоединения_);
		АдресСервера_ = Лев(АдресСервера_, ПозицияЗавершенияСхемы_ + 2 + ДлинаСоединения_);
		
		ПозицияПараметров_ = СтрНайти(РесурсНаСервере_, "?");
		ДлинаРесурсаНаСервере_ = ?(ПозицияПараметров_ > 0, ПозицияПараметров_ - 1, СтрДлина(РесурсНаСервере_));
		РесурсНаСервере_ = Лев(РесурсНаСервере_, ДлинаРесурсаНаСервере_);
	КонецЕсли;
	
	ОписаниеОшибкиУстановкиСоединения_ = "";
	СоединениеHTTP_ = ДокументооборотСКО.УстановитьСоединениеССерверомИнтернета(
		АдресСервера_, ОписаниеОшибкиУстановкиСоединения_
	);
	Если СоединениеHTTP_ = Неопределено Тогда
		ТекстОшибки_ = НСтр("ru = 'Не удалось установить соединение с сервером:
						   |%1'");
		ТекстОшибки_ = СтрШаблон(ТекстОшибки_, ОписаниеОшибкиУстановкиСоединения_);
		Сообщить(ТекстОшибки_);
		Возврат Неопределено;
	КонецЕсли;
	
	ТелоЗапросаSOAP_ = Запрос;
	
	Попытка
		
		HTTPЗапрос_ = Новый HTTPЗапрос(РесурсНаСервере_);
		HTTPЗапрос_.УстановитьТелоИзСтроки(
			ТелоЗапросаSOAP_,
			,
			ИспользованиеByteOrderMark.НеИспользовать
		);
		
		ЗаголовкиHTTP_ = HTTPЗапрос_.Заголовки;
		ЗаголовкиHTTP_.Вставить("Content-Type", "text/xml; charset=utf-8");
		ЗаголовкиHTTP_.Вставить("Proxy-Connection", "Keep-Alive");
		
		HTTPОтвет_ = СоединениеHTTP_.ОтправитьДляОбработки(HTTPЗапрос_);
		
	Исключение
		
		Возврат Неопределено;
		
	КонецПопытки;
	
	СтрокаОтвета_ = HTTPОтвет_.ПолучитьТелоКакСтроку();
	
	Если Не ЗначениеЗаполнено(СтрокаОтвета_) Тогда
		Возврат Неопределено;
	КонецЕсли;
	
	Результат_ = РазобратьОтветСервера(СтрокаОтвета_);
	
	Возврат Результат_;
	
КонецФункции

#КонецОбласти

#Область СлужебныеМетоды

/// Получение сертификата организации для обмена с ПФР
Функция ПолучитьСертификатОрганизации()
	
	Отпечаток_ = Константы.ОтпечатокСертификатаМедОрганизации.Получить();
	
	Если Не ЗначениеЗаполнено(Отпечаток_) Тогда
		Возврат Неопределено;
	КонецЕсли;
	
	Запрос_ = Новый Запрос(
		"ВЫБРАТЬ ПЕРВЫЕ 1
		|	СертификатыКлючейЭлектроннойПодписиИШифрования.Ссылка
		|ИЗ
		|	Справочник.СертификатыКлючейЭлектроннойПодписиИШифрования КАК СертификатыКлючейЭлектроннойПодписиИШифрования
		|ГДЕ
		|	СертификатыКлючейЭлектроннойПодписиИШифрования.Отпечаток = &Отпечаток"
	);
	
	Запрос_.УстановитьПараметр("Отпечаток", Отпечаток_);
	
	РезультатЗапроса_ = Запрос_.Выполнить();
	
	Если Не РезультатЗапроса_.Пустой() Тогда
		
		Выгрузка_ = РезультатЗапроса_.Выгрузить();
		
		Если Не 0 = Выгрузка_.Количество() Тогда
			Возврат Выгрузка_[0].Ссылка;
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат Неопределено;
	
КонецФункции

/// Адрес веб-сервиса ПФР
Функция МестоположениеWSDL()
	
	Если Не ИспользуетсяРежимТестирования() Тогда
		Возврат "http://172.16.90.14:7777/gateway/services/SID0003814?wsdl";
	Иначе
		// Тестовый сервер
		Возврат "http://smev-mvf.test.gosuslugi.ru:7777/gateway/services/SID0003577/1.00?wsdl";
	КонецЕсли; 
	
КонецФункции

/// Формирование структуры с результатом ответа сервера ПФР
Функция РазобратьОтветСервера(СтрокаXML)
	
	Результат_ = Новый Структура(
		"ОтветСервера, Успех",
		СтрокаXML
	);
	
	ЧтениеXML_ = Новый ЧтениеXML;
	ЧтениеXML_.УстановитьСтроку(СтрокаXML);
	ПостроительDOM_ = Новый ПостроительDOM;
	ДокDOM_ = ПостроительDOM_.Прочитать(ЧтениеXML_);
	
	СоответствиеПИ_ = Новый Соответствие;
	СоответствиеПИ_.Вставить("soap", xmlns.soap());
	СоответствиеПИ_.Вставить("gu", xmlns.gu());
	СоответствиеПИ_.Вставить("snils", xmlns.snils());
	СоответствиеПИ_.Вставить("pfr", xmlns.pfr());
	
	Разым_ = Новый РазыменовательПространствИменDOM(СоответствиеПИ_);
	
	ТипРезСтрока_ = ТипРезультатаDOMXPath.Строка;
	
	XPath_ =
		"//soap:Body/snils:SnilsByDataResponse/gu:MessageData/gu:AppData
		|/snils:result/snils:snils/text()"
	;
	Рез_ = ДокDOM_.ВычислитьВыражениеXPath(XPath_, ДокDOM_, Разым_, ТипРезСтрока_);
	
	Если ЗначениеЗаполнено(Рез_.СтроковоеЗначение) Тогда
		Результат_.Вставить("СтраховойНомерПФР", Рез_.СтроковоеЗначение);
		Результат_.Успех = Истина;
		Возврат Результат_;
	КонецЕсли;
	
	XPath_ =
		"//soap:Body/snils:SnilsByDataResponse/gu:MessageData/gu:AppData
		|/pfr:fault/pfr:message/text()"
	;
	Рез_ = ДокDOM_.ВычислитьВыражениеXPath(XPath_, ДокDOM_, Разым_, ТипРезСтрока_);
	
	Если ЗначениеЗаполнено(Рез_.СтроковоеЗначение) Тогда
		Результат_.Вставить("ТекстОшибки", Рез_.СтроковоеЗначение);
		Результат_.Успех = Ложь;
		Возврат Результат_;
	КонецЕсли;
	
	ТипРезУзлы_ = ТипРезультатаDOMXPath.УпорядоченныйИтераторУзлов;
	
	XPath_ =
		"//soap:Body/snils:SnilsByDataResponse/gu:MessageData/gu:AppData
		|/snils:result/snils:twinData"
	;
	Рез_ = ДокDOM_.ВычислитьВыражениеXPath(XPath_, ДокDOM_, Разым_, ТипРезУзлы_);
	
	XPath_twinSnils = "./snils:twinSnils/text()";
	XPath_Документ =
		"normalize_space(concat(
		| ./snils:twinDocument/pfr:document/pfr:name/text(),
		| ' ',
		| ./snils:twinDocument/pfr:document/pfr:seriesRomanNumerals/text(),
		| ' ',
		| ./snils:twinDocument/pfr:document/pfr:seriesRussianSymbols/text(),
		| ' ',
		| ./snils:twinDocument/pfr:document/pfr:number/text(),
		|))"
	;
	XPath_МестоРождения =
		"normalize-space(concat(
		| ./snils:twinBirthPlace/pfr:country/text()
		| ' '
		| ./snils:twinBirthPlace/pfr:region/text()
		| ' '
		| ./snils:twinBirthPlace/pfr:district/text()
		| ' '
		| ./snils:twinBirthPlace/pfr:settlement/text()
		|))"
	;
	
	Дубликаты_ = Новый Массив;
	
	Пока Истина Цикл
		
		ЭлDOM_ = Рез_.ПолучитьСледующий();
		Если Не ТипЗнч(ЭлDOM_) = Тип("ЭлементDOM") Тогда
			Прервать;
		КонецЕсли;
		
		Рез1_ = ДокDOM_.ВычислитьВыражениеXPath(XPath_twinSnils, ЭлDOM_, Разым_, ТипРезСтрока_);
		Рез2_ = ДокDOM_.ВычислитьВыражениеXPath(XPath_Документ, ЭлDOM_, Разым_, ТипРезСтрока_);
		Рез3_ = ДокDOM_.ВычислитьВыражениеXPath(XPath_МестоРождения, ЭлDOM_, Разым_, ТипРезСтрока_);
		
		Дубликаты_.Добавить(
			Новый Структура(
				"СтраховойНомерПФР, Документ, МестоРождения",
				Рез1_.СтроковоеЗначение,
				Рез2_.СтроковоеЗначение,
				Рез3_.СтроковоеЗначение
			)
		);
		
	КонецЦикла;
	
	Если Не 0 = Дубликаты_.Количество() Тогда
		Результат_.Вставить("Дубликаты", Дубликаты_);
		Результат_.Успех = Истина;
		Возврат Результат_;
	КонецЕсли;
	
	ТекстОшибки_ = НСтр("ru = 'Неизвестная ошибка обработки результата запроса к ПФР.'");
	Результат_.Вставить("ТекстОшибки", ТекстОшибки_);
	Результат_.Успех = Ложь;
	
	Возврат Результат_;
	
КонецФункции

// Получает значение константы ИспользоватьОбменСПФР_РежимТестирования
Функция ИспользуетсяРежимТестирования()
	
	УстановитьПривилегированныйРежим(Истина);
	ПФРРежимТестирования_ = Константы.ИспользоватьОбменСПФР_РежимТестирования.Получить();
	УстановитьПривилегированныйРежим(Ложь);
	
	Возврат ПФРРежимТестирования_;
	
КонецФункции
#КонецОбласти
