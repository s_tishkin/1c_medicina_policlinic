﻿
#Область ПрограммныйИнтерфейс


////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

// Сохраняет настройки фильтров
//
// Параметры
//	Форма - управляемая форма, для которой применяются фильтры
//
Процедура СохранитьЗначенияНастроек(Форма) Экспорт
	
	ПрефиксЭлементов = ОтборПоФильтрамКлиентСервер.ПрефиксЭлементов();
	
	Настройки = Новый Структура;
	Настройки.Вставить("ИспользоватьФильтры", Форма[ПрефиксЭлементов + "ИспользоватьФильтры"]);
	
	ВариантФильтра = Форма[ПрефиксЭлементов + "ВариантФильтра"];
	ВариантыФильтров = Форма.Элементы[ПрефиксЭлементов + "ВариантФильтра"].СписокВыбора.ВыгрузитьЗначения();
	Настройки.Вставить("ВариантОтбора", ВариантыФильтров.Найти(ВариантФильтра) + 1);
	
	Для каждого ВариантФильтра Из ВариантыФильтров Цикл
		Выполнить(ВариантФильтра + "Клиент.СохранитьЗначенияНастроек(Форма, Настройки)");
	КонецЦикла;
	
	ОбщегоНазначенияВызовСервера.ХранилищеОбщихНастроекСохранить(ОтборПоФильтрамКлиентСервер.КлючНастроекФормы(Форма), "", Настройки);
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ

// Устанавливает элемент фильтра текущим активным элементом формы
//
// Параметры
//	Форма - управляемая форма, для которой применяются фильтры
//
Процедура УстановитьТекущийЭлемент(Форма, Команда) Экспорт
	
	ВариантФильтра = ОтборПоФильтрамКлиентСервер.ТекущийВариантФильтра(Форма);
	Выполнить(ВариантФильтра + "Клиент.УстановитьТекущийЭлемент(Форма);");
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ФОРМЫ

// Обрабатыывает изменение признака использования фильтров
//
// Параметры
//	Форма - управляемая форма, для которой применяются фильтры
//
Процедура ПриИзмененииИспользоватьФильтры(Форма, Элемент) Экспорт
	
	ИспользоватьФильтры = ОтборПоФильтрамКлиентСервер.ИспользоватьФильтры(Форма);
	ВариантФильтра = ОтборПоФильтрамКлиентСервер.ТекущийВариантФильтра(Форма);
	
	Если ИспользоватьФильтры Тогда
		ОтборПоФильтрамКлиентСервер.УстановитьОтбор(Форма, ВариантФильтра);
	Иначе
		ОтборПоФильтрамКлиентСервер.СброситьОтбор(Форма, ВариантФильтра);
	КонецЕсли;
	
	ОтборПоФильтрамКлиентСервер.УстановитьДоступностьФильтров(Форма);
	
КонецПроцедуры


#КонецОбласти