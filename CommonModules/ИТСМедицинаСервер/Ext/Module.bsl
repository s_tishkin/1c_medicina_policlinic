﻿
#Область ПрограммныйИнтерфейс

// ИнициализироватьФорму
Процедура ИнициализироватьФорму(Форма,
								Операции,
								ГруппаКоманд,
								ВыводитьИнформациюВИнформационныйБлок,
								ЭтоФормаЭлемента = Ложь) Экспорт
	
	СоздатьРеквизиты(Форма, Операции);
	
	Префикс = ИТСМедицинаКлиентСервер.ПрефиксЭлементов();
	
	Форма[Префикс + "ВыводитьИнформациюВИнформационныйБлок"] = ВыводитьИнформациюВИнформационныйБлок;
	Форма[Префикс + "ЭтоФормаЭлемента"] = ЭтоФормаЭлемента;
	Если ИТСМедицинаКлиентСервер.ЕстьОперацииПолученияДанных(Операции) Тогда
		Форма[Префикс + "ОбработчикОжиданияПриемаДанных"] = "Подключаемый_ИТСМедицинаПриемДанных";
	КонецЕсли;
	
	СоздатьКомандыФормы(Форма, Операции);
	СоздатьЭлементыФормы(Форма, Операции, ГруппаКоманд);
	
КонецПроцедуры

Процедура СоздатьРеквизиты(Форма, Операции)
	
	Префикс = ИТСМедицинаКлиентСервер.ПрефиксЭлементов();
	
	НовыеРеквизиты = Новый Массив;
	
	НовыеРеквизиты.Добавить(Новый РеквизитФормы(Префикс + "ЭтоФормаЭлемента", Новый ОписаниеТипов("Булево")));
	НовыеРеквизиты.Добавить(Новый РеквизитФормы(Префикс + "ВыводитьИнформациюВИнформационныйБлок", Новый ОписаниеТипов("Булево")));

	Если ИТСМедицинаКлиентСервер.ЕстьОперацииПолученияДанных(Операции) Тогда
		НовыеРеквизиты.Добавить(Новый РеквизитФормы(Префикс + "ЗагружаемыеФайлы", Новый ОписаниеТипов("СписокЗначений")));
		НовыеРеквизиты.Добавить(Новый РеквизитФормы(Префикс + "КоличествоОжидаемыхФайлов", Новый ОписаниеТипов("Число")));
		НовыеРеквизиты.Добавить(Новый РеквизитФормы(Префикс + "КоличествоПринятыхФайлов", Новый ОписаниеТипов("Число")));
		НовыеРеквизиты.Добавить(Новый РеквизитФормы(Префикс + "КоличествоЗагруженныхФайлов", Новый ОписаниеТипов("Число")));
		НовыеРеквизиты.Добавить(Новый РеквизитФормы(Префикс + "ВыполняетсяПолучениеДанных", Новый ОписаниеТипов("Булево")));
		НовыеРеквизиты.Добавить(Новый РеквизитФормы(Префикс + "КаталогЗагрузки", Новый ОписаниеТипов("Строка")));
		НовыеРеквизиты.Добавить(Новый РеквизитФормы(Префикс + "ОбработчикОжиданияПриемаДанных", Новый ОписаниеТипов("Строка")));
		НовыеРеквизиты.Добавить(Новый РеквизитФормы(Префикс + "ДатаНачалаПриемаДанных", Новый ОписаниеТипов("Дата")));
		НовыеРеквизиты.Добавить(Новый РеквизитФормы(Префикс + "СписокРезультатов", Новый ОписаниеТипов("СписокЗначений")));
		НовыеРеквизиты.Добавить(Новый РеквизитФормы(Префикс + "ОбработчикПринятыхФайлов", Новый ОписаниеТипов("Строка")));
	КонецЕсли;
	
	Если Не РаботаСУправляемойФормой.ОбработчикиКомандИнициализированы(Форма) Тогда
		РаботаСУправляемойФормой.ОбработчикиКомандСоздатьРеквизитыФормы(НовыеРеквизиты);
	КонецЕсли;
		
	Если Не РаботаСУправляемойФормой.ОбработчикиСобытийИнициализированы(Форма) Тогда
		РаботаСУправляемойФормой.ОбработчикиСобытийСоздатьРеквизитыФормы(НовыеРеквизиты);
	КонецЕсли;
	
	Форма.ИзменитьРеквизиты(НовыеРеквизиты);
	
КонецПроцедуры

Процедура СоздатьКомандыФормы(Форма, Операции)
	
	ОписаниеКомандФормы = РаботаСУправляемойФормой.ПрочитатьОписаниеЭлементовФормыИзСтроки(
		ТекстовоеОписаниеКомандФормы(Операции));
	РаботаСУправляемойФормой.СоздатьКомандыФормы(
		Форма,
		ОписаниеКомандФормы,
		ИТСМедицинаКлиентСервер.ПрефиксЭлементов());
	
КонецПроцедуры

Функция ТекстовоеОписаниеКомандФормы(Операции)
	
	ДоступныеОперации = ПолучитьДоступныеОперации();
	
	ШаблонКоманды = "
	|	<Команда
	|			Имя='%1'
	|			Заголовок='%2'
	|			Подсказка='%3'
	|			Действие='%4'
	|			ОбработчикиКоманды='%5'
	|			Картинка='%6'
	|			Отображение='%7'/>
	|";
	
	ОписаниеКоманд =
	"
	|<Команды>";
	
	Для каждого Операция Из ДоступныеОперации Цикл
		Если Операции.Свойство(Операция.Имя) Тогда
			ОписаниеКоманд  = ОписаниеКоманд + СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
				ШаблонКоманды,
				Операция.Имя,
				Операция.Заголовок,
				Операция.Подсказка,
				Операция.Действие,
				Операция.ОбработчикиКоманды,
				Операция.Картинка,
				Операция.Отображение);
		КонецЕсли;
	КонецЦикла;
	
	Если ИТСМедицинаКлиентСервер.ЕстьОперацииПолученияДанных(Операции) Тогда
		ОписаниеКоманд  = ОписаниеКоманд + СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			ШаблонКоманды,
			"ПрерватьПолучениеДанных",
			НСтр("ru='Прервать прием данных'"),
			НСтр("ru=''"),
			"Подключаемый_ВыполнитьКомандуФормы",
			"Клиент:ИТСМедицинаКлиент.ПрерватьПриемДанных",
			"Остановить",
			"КартинкаИТекст");
	КонецЕсли;
	
	///////////////////////////////////////////////////////////////////////////////
	// КОМАНДЫ НАСТРОЙКИ ВЗАИМОДЕЙСТВИЯ С ИТС:МЕДИЦИНА

	Если Операции.Свойство("НастройкиПользователя") Тогда
		ОписаниеКоманд  = ОписаниеКоманд + СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			ШаблонКоманды,
			"НастройкиПользователя",
			НСтр("ru='Настроить...'"),
			НСтр("ru=''"),
			"Подключаемый_ВыполнитьКомандуФормы",
			"Клиент:ИТСМедицинаКлиент.НастройкиПользователя",
			"НастройкаСписка",
			"КартинкаИТекст");
	КонецЕсли;

	ОписаниеКоманд  = ОписаниеКоманд + "
	|</Команды>
	|";;
	
	Возврат ОписаниеКоманд;
	
КонецФункции

Процедура СоздатьЭлементыФормы(Форма, Операции, ГруппаКоманд)
	
	Префикс = ИТСМедицинаКлиентСервер.ПрефиксЭлементов();
	Элементы = Форма.Элементы;
	
	ПодменюИТСМедицина = Элементы.Найти(ГруппаКоманд);
	
	ОписаниеЭлементовФормы = РаботаСУправляемойФормой.ПрочитатьОписаниеЭлементовФормыИзСтроки(
		ТекстовоеОписаниеЭлементовФормы(Операции));
	РаботаСУправляемойФормой.СоздатьЭлементыФормы(Форма, ОписаниеЭлементовФормы, Префикс, ПодменюИТСМедицина);
	
	Если Форма[Префикс + "ВыводитьИнформациюВИнформационныйБлок"]
	   И ИТСМедицинаКлиентСервер.ЕстьОперацииПолученияДанных(Операции) Тогда
		ИнформационныйБлок.ДобавитьРаздел(Форма, "ИТСМедицина", ИТСМедицинаСервер);
	КонецЕсли;
	
	ОбщегоНазначенияНСИКлиентСервер.УстановитьСвойствоЭлементаФормы(
		Элементы,
		Префикс + "ПрерватьПолучениеДанных",
		"Доступность",
		Ложь);
	
КонецПроцедуры

Функция ТекстовоеОписаниеЭлементовФормы(Операции)
	
	ШаблонПунктаМеню = "
	|	<КнопкаФормы
	|			Имя='%1'
	|			Вид='КнопкаКоманднойПанели'
	|			ИмяКоманды='$Префикс$%1'/>
	|";
	
	ДоступныеОперации = ПолучитьДоступныеОперации();
		
	ОписаниеЭлементов = 
	"<Элементы>
	|	<ГруппаФормы
	|		Имя='ПодменюИТСМедицина'
	|		Вид='Подменю'
	|		Заголовок='ИТС:Медицина'
	|		Картинка='ИТСМедицина'
	|		Отображение='КартинкаИТекст'>
	|";
	
	Для каждого Операция Из ДоступныеОперации Цикл
		Если Операции.Свойство(Операция.Имя) Тогда
			ОписаниеЭлементов = ОписаниеЭлементов + СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
				ШаблонПунктаМеню,
				Операция.Имя);
		КонецЕсли;
	КонецЦикла;
	
	Если ИТСМедицинаКлиентСервер.ЕстьОперацииПолученияДанных(Операции) Тогда
		ОписаниеЭлементов = ОписаниеЭлементов + СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			ШаблонПунктаМеню,
			"ПрерватьПолучениеДанных");
	КонецЕсли;
	
	Если Операции.Свойство("НастройкиПользователя") Тогда
		ОписаниеЭлементов = ОписаниеЭлементов + СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
			ШаблонПунктаМеню,
			"НастройкиПользователя");
	КонецЕсли;
	
	ОписаниеЭлементов = ОписаниеЭлементов + "
	|	
	|	</ГруппаФормы>
	|</Элементы>
	|";
	
	Возврат СтрЗаменить(ОписаниеЭлементов, "$Префикс$", ИТСМедицинаКлиентСервер.ПрефиксЭлементов());
	
КонецФункции


Процедура ИнформационныйБлокСоздатьРеквизиты(Форма) Экспорт
	
	Префикс = ИнформационныйБлокКлиентСервер.ПрефиксЭлементов() + ИТСМедицинаКлиентСервер.ПрефиксЭлементов();
	
	НовыеРеквизиты = Новый Массив;
	НовыеРеквизиты.Добавить(Новый РеквизитФормы(Префикс + "Состояние", Новый ОписаниеТипов("Строка")));
	НовыеРеквизиты.Добавить(Новый РеквизитФормы(Префикс + "ВремяРаботы", Новый ОписаниеТипов("Строка")));
	Форма.ИзменитьРеквизиты(НовыеРеквизиты);
	
КонецПроцедуры

Процедура ИнформационныйБлокСоздатьЭлементыФормы(Форма, Группа) Экспорт
	
	Префикс = ИнформационныйБлокКлиентСервер.ПрефиксЭлементов() + ИТСМедицинаКлиентСервер.ПрефиксЭлементов();
	Элементы = Форма.Элементы;
	
	ОписаниеЭлементовФормы = РаботаСУправляемойФормой.ПрочитатьОписаниеЭлементовФормыИзСтроки(
		ИнформационныйБлокТекстовоеОписаниеЭлементовФормы());
	РаботаСУправляемойФормой.СоздатьЭлементыФормы(Форма, ОписаниеЭлементовФормы, Префикс, Группа);
	
КонецПроцедуры

Функция ИнформационныйБлокТекстовоеОписаниеЭлементовФормы()
	
	ОписаниеЭлементов = 
	"<Элементы>
	|	<ГруппаФормы
	|			Имя='Шапка'
	|			Вид='ОбычнаяГруппа'
	|			ОтображатьЗаголовок='Ложь'
	|			Отображение='Ложь'
	|			Группировка='Горизонтальная'>
	|		<ГруппаФормы
	|				Имя='ШапкаЛево'
	|				Вид='ОбычнаяГруппа'
	|				ОтображатьЗаголовок='Ложь'
	|				Отображение='Ложь'>
	|			<ДекорацияФормы
	|					Имя='ДлительнаяОперация'
	|					Вид='Картинка'
	|					Картинка='ДлительнаяОперация48'
	|					Ширина='6'
	|					Высота='3'/>
	|
	|		</ГруппаФормы>
	|		<ГруппаФормы
	|				Имя='ШапкаПраво'
	|				Вид='ОбычнаяГруппа'
	|				ОтображатьЗаголовок='Ложь'
	|				Отображение='Ложь'>
	|			<ДекорацияФормы
	|					Имя='Заголовок'
	|					Вид='Надпись'
	|					Заголовок='" + НСтр("ru='Получение данных с ИТС:Медицина'") + "'
	|					Ширина='0'/>
	|			<ПолеФормы
	|					Имя='Состояние'
	|					Вид='ПолеНадписи'
	|					ПутьКДанным='%ПрефиксИнформационныйБлок%Состояние'
	|					РастягиватьПоВертикали='Истина'
	|					Заголовок='" + НСтр("ru='Состояние'") + "'
	|					ПоложениеЗаголовка='Ложь'/>
	|			<ПолеФормы
	|					Имя='ВремяРаботы'
	|					Вид='ПолеНадписи'
	|					Заголовок='" + НСтр("ru='Время работы'") + "'
	|					ПутьКДанным='%ПрефиксИнформационныйБлок%ВремяРаботы'/>
	|			<КнопкаФормы
	|					Имя='Прервать'
	|					Вид='ОбычнаяКнопка'
	|					ИмяКоманды='%ПрефиксИТС%ПрерватьПолучениеДанных'
	|					Отображение='Текст'/>
	|
	|		</ГруппаФормы>
	|
	|	</ГруппаФормы>
	|</Элементы>
	|";
	
	ПрефиксИТС = ИТСМедицинаКлиентСервер.ПрефиксЭлементов();
	ОписаниеЭлементов = СтрЗаменить(ОписаниеЭлементов, "%ПрефиксИТС%", ПрефиксИТС);
	
	ПрефиксИнформационныйБлок = ИнформационныйБлокКлиентСервер.ПрефиксЭлементов() +  ПрефиксИТС;
	ОписаниеЭлементов = СтрЗаменить(ОписаниеЭлементов, "%ПрефиксИнформационныйБлок%", ПрефиксИнформационныйБлок);
	
	Возврат ОписаниеЭлементов;
	
КонецФункции

Функция ПолучитьДоступныеОперации()
	
	ДоступныеОперации = Новый Массив;
	
	///////////////////////////////////////////////////////////////////////////////
	// КОМАНДЫ ОТКРЫТИЯ ИТС:МЕДИЦИНА
	
	Операция = СоздатьОписаниеОперации("ОткрытьПоискТоваровВБазеРЛС");
	Операция.Заголовок = НСтр("ru='Открыть поиск товаров в базе РЛС'");
	Операция.Подсказка = НСтр("ru='Открыть страницу поиска товаров в базе РЛС на диске ИТС:Медицина.'");
	Операция.ОбработчикиКоманды = "Клиент:ИТСМедицинаКлиент.ОткрытьПоискТоваровВБазеРЛС";
	Операция.Картинка = "Найти";
	Операция.Отображение = "КартинкаИТекст";
    ДоступныеОперации.Добавить(Операция);
	
	Операция = СоздатьОписаниеОперации("ОткрытьТоварВБазеРЛС");
	Операция.Заголовок = НСтр("ru='Открыть описание товара в базе РЛС'");
	Операция.Подсказка = НСтр("ru=''");
	Операция.ОбработчикиКоманды = "Клиент:ИТСМедицинаКлиент.ОткрытьТоварВБазеРЛС";
	Операция.Картинка = "ПоказатьДанные";
	Операция.Отображение = "КартинкаИТекст";
    ДоступныеОперации.Добавить(Операция);

	Операция = СоздатьОписаниеОперации("ОткрытьПоискСинонимовТовараВБазеРЛС");
	Операция.Заголовок = НСтр("ru='Поиск синонимов в базе РЛС'");
	Операция.Подсказка = НСтр("ru=''");
	Операция.ОбработчикиКоманды = "Клиент:ИТСМедицинаКлиент.ОткрытьПоискСинонимовТовараВБазеРЛС";
	Операция.Картинка = "НайтиПоНомеру";
	Операция.Отображение = "КартинкаИТекст";
    ДоступныеОперации.Добавить(Операция);
	
	///////////////////////////////////////////////////////////////////////////////
	// КОМАНДЫ ПРИЕМА ДАННЫХ ТОВАРОВ
	
	Операция = СоздатьОписаниеОперации("ПринятьДанныеТоваровИзБазыРЛС");
	Операция.Заголовок = НСтр("ru='Принять данные товаров из базы РЛС'");
	Операция.Подсказка = НСтр("ru=''");
	Операция.ОбработчикиКоманды = "Клиент:ИТСМедицинаКлиент.ПринятьДанныеТоваровИзБазыРЛС";
    ДоступныеОперации.Добавить(Операция);

	Операция = СоздатьОписаниеОперации("ОбновитьВыделенныеТоварыИзБазыРЛС");
	Операция.Заголовок = НСтр("ru='Обновить выделенные товары из базе РЛС'");
	Операция.Подсказка = НСтр("ru=''");
	Операция.ОбработчикиКоманды = "Клиент:ИТСМедицинаКлиент.ОбновитьВыделенныеТоварыИзБазыРЛС";
    ДоступныеОперации.Добавить(Операция);
	
	Операция = СоздатьОписаниеОперации("СвязатьСТоваромИзБазыРЛС");
	Операция.Заголовок = НСтр("ru='Связать с товаром из базы РЛС'");
	Операция.Подсказка = НСтр("ru=''");
	Операция.ОбработчикиКоманды = "Клиент:ИТСМедицинаКлиент.СвязатьСТоваромИзБазыРЛС";
    ДоступныеОперации.Добавить(Операция);
	
	ОбщегоНазначенияНСИКлиентСервер.ДополнитьМассив(
		ДоступныеОперации,
		ИТСМедицинаСерверПереопределяемый.ПолучитьДоступныеОперации());
	
	Возврат ДоступныеОперации;
	
КонецФункции

// СоздатьОписаниеОперации
Функция СоздатьОписаниеОперации(ИмяОперации) Экспорт
	
	Операция = Новый Структура;
	Операция.Вставить("Имя", ИмяОперации);
	Операция.Вставить("Заголовок", "");
	Операция.Вставить("Подсказка", "");
	Операция.Вставить("Действие", "Подключаемый_ВыполнитьКомандуФормы");
	Операция.Вставить("ОбработчикиКоманды", "");
	Операция.Вставить("Картинка");
	Операция.Вставить("Отображение");
	
	Возврат Операция;
	
КонецФункции


#КонецОбласти