﻿#Область РеализацияИнтерфейса

Функция ПолучитьИмяМодуля() Экспорт
	Возврат "ПоведениеГруппыМестоХраненияКарты";
КонецФункции

Процедура Инициализировать(Форма, Отказ) Экспорт
	// 1. Регистрируем обработчики событий
	СоздатьОбработчикиСобытий(Форма);
	// 2. Регистрируем обработчики оповещений
	СоздатьОбработчикиОповещений(Форма);
	// 2. Устанавливаем раздел справки
	ПоведениеГруппОбщее.УстановитьРазделСправки(Форма, "Документ.СменаМестаХраненияКарты");
КонецПроцедуры

Функция ПолучитьИменаРеквизитовГруппы(ТолькоОбязательныеДляОтображения = Ложь) Экспорт
	МассивИменРеквизитов_ = ПоведениеГрупп.СоздатьМассивЭлементов(
		"МестоХранения",
		"ПримечаниеМестаХранения",
	);
	Возврат МассивИменРеквизитов_;
КонецФункции

Функция ПолучитьИменаКомандГруппы(ТолькоОбязательныеДляОтображения = Ложь) Экспорт
	МассивКоманд_ = ПоведениеГрупп.СоздатьМассивЭлементов();
	Если Не ТолькоОбязательныеДляОтображения = Истина Тогда
		ПоведениеГрупп.ДополнитьМассив(
			МассивКоманд_,
			ПоведениеГрупп.СоздатьМассивЭлементов(
			)
		);
	КонецЕсли;
	Возврат МассивКоманд_;
КонецФункции

#КонецОбласти

#Область Инициализация

Процедура СоздатьОбработчикиСобытий(Форма)
	ИмяМодуля_ = ПолучитьИмяМодуля();
	
	ПоведениеГрупп.ЗарегистрироватьОбработчикСобытияФормы(
		Форма, ИмяМодуля_, "ПередЗагрузкойДанныхИзНастроекНаСервере"
	);
	
КонецПроцедуры

Процедура СоздатьОбработчикиОповещений(Форма)
	ИмяМодуля_ = ПолучитьИмяМодуля();
	Оповещения_ = ПолучитьОбрабатываемыеОповещения();
	Для Каждого Оповещение_ Из Оповещения_ Цикл
		ПоведениеГрупп.ЗарегистрироватьОбработчикОповещенияГрупп(Форма, ИмяМодуля_, Оповещение_);
	КонецЦикла;
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытий

Процедура Форма_ПередЗагрузкойДанныхИзНастроекНаСервере(Форма, Настройки) Экспорт
	Если ЗначениеЗаполнено(Форма.Объект.МедицинскаяКарта) Тогда
		Настройки.Удалить("Объект.МестоХранения");
	КонецЕсли;
КонецПроцедуры

#Область Имплементация

#КонецОбласти

#КонецОбласти

#Область ОбработкаОповещенийПоведенияГрупп

Процедура ОбработкаОповещенияПоведенияГруппНаКлиенте(Форма, ИмяСобытия, Параметры) Экспорт
	Если ИмяСобытия = "УстановитьДоступность" Тогда
		ОбработкаОповещения_УстановитьДоступность(Форма, Параметры);
	Иначе
		__ПРОВЕРКА__(Ложь, "Необработанное событие: " + ИмяСобытия + ". Обратитесь к разработчику.");
	КонецЕсли;
КонецПроцедуры

#Область ОбработчикиОповещений

Процедура ОбработкаОповещения_УстановитьДоступность(Форма, Параметры)
	__ТИП__(Параметры, "Структура");
	__ПРОВЕРКА__(Параметры.Свойство("Доступность"), "Не установлен параметр Доступность. Обратитесь к разработчику");
	ИмяМодуля_ = ПолучитьИмяМодуля();
	Доступность_ = Параметры.Доступность;
	ИменаЭлементов_ = ПоведениеГрупп.ПолучитьИменаЭлементовГруппы(Форма, ИмяМодуля_);
	Для Каждого ИмяЭлемента_ Из ИменаЭлементов_ Цикл
		Элемент_ = Форма.Элементы.Найти(ИмяЭлемента_);
		__ПРОВЕРКА__(Не Элемент_ = Неопределено, "Элемент " + ИмяЭлемента_ + " не найден на форме. Обратитесь к разработчику.");
		Если ТипЗнч(Элемент_) = Тип("КнопкаФормы") Тогда
			Элемент_.Доступность = Доступность_;
		Иначе
			Элемент_.ТолькоПросмотр = Не Доступность_;
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

#КонецОбласти

#Область ВспомогательныеМетоды

Функция ПолучитьОбрабатываемыеОповещения()
	Возврат ПоведениеГрупп.СоздатьМассивЭлементов(
		"УстановитьДоступность"
	);
КонецФункции

#КонецОбласти

#КонецОбласти

#Область ВспомогательныеМетоды

#КонецОбласти
