﻿#Область ПрограммныйИнтерфейс

// Получить оборудование подключенное к терминалу.
//
// Параметры:
//  ЭквайринговыйТерминал - СправочникСсылка.ЭквайринговыеТерминалы - Эквайринговый терминал.
// 
// Возвращаемое значение:
//  Структура - Структура по свойствами:
//   * Терминал - СправочникСсылка.ПодключаемоеОборудование - Терминал.
//   * ККТ - СправочникСсылка.ПодключаемоеОборудование - ККТ.
//
Функция ОборудованиеПодключенноеКТерминалу(ЭквайринговыйТерминал) Экспорт
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	Т.Ссылка КАК Ссылка
	|ПОМЕСТИТЬ НастройкаРМК
	|ИЗ
	|	Справочник.НастройкиРМК КАК Т
	|ГДЕ
	|	Т.РабочееМесто = &РабочееМесто
	|
	|;
	|ВЫБРАТЬ ПЕРВЫЕ 1
	|	ЭквайринговыеТерминалы.ПодключаемоеОборудование               КАК Терминал,
	|	ЭквайринговыеТерминалы.ИспользоватьБезПодключенияОборудования КАК ИспользоватьБезПодключенияОборудования
	|ИЗ
	|	Справочник.НастройкиРМК.ЭквайринговыеТерминалы КАК ЭквайринговыеТерминалы
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ НастройкаРМК КАК НастройкаРМК
	|		ПО НастройкаРМК.Ссылка = ЭквайринговыеТерминалы.Ссылка
	|ГДЕ
	|	ЭквайринговыеТерминалы.ЭквайринговыйТерминал = &ЭквайринговыйТерминал");
	
	Запрос.УстановитьПараметр("РабочееМесто",          МенеджерОборудованияВызовСервера.ПолучитьРабочееМестоКлиента());
	Запрос.УстановитьПараметр("ЭквайринговыйТерминал", ЭквайринговыйТерминал);
	
	ВозвращаемоеЗначение = Новый Структура;
	ВозвращаемоеЗначение.Вставить("Терминал");
	
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() Тогда
		
		ЗаполнитьЗначенияСвойств(ВозвращаемоеЗначение, Выборка);
		Если Выборка.ИспользоватьБезПодключенияОборудования Тогда
			ВозвращаемоеЗначение.Терминал = Неопределено;
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат ВозвращаемоеЗначение;
	
КонецФункции

// Получить оборудование подключенное к кассе.
//
// Параметры:
//  Касса - СправочникСсылка.Кассы - Касса.
// 
// Возвращаемое значение:
//  Структура - Структура по свойствами:
//   * Терминал - СправочникСсылка.ПодключаемоеОборудование - Терминал.
//   * ККТ - СправочникСсылка.ПодключаемоеОборудование - ККТ.
//
Функция ОборудованиеПодключенноеККассе(Касса) Экспорт
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	Т.Ссылка КАК Ссылка
	|ПОМЕСТИТЬ НастройкаРМК
	|ИЗ
	|	Справочник.НастройкиРМК КАК Т
	|ГДЕ
	|	Т.РабочееМесто = &РабочееМесто
	|
	|;
	|ВЫБРАТЬ ПЕРВЫЕ 1
	|	Кассы.ПодключаемоеОборудование КАК ККТ
	|ИЗ
	|	Справочник.НастройкиРМК.Кассы КАК Кассы
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ НастройкаРМК КАК НастройкаРМК
	|		ПО НастройкаРМК.Ссылка = Кассы.Ссылка
	|ГДЕ
	|	Кассы.Касса = &Касса");
	
	Запрос.УстановитьПараметр("РабочееМесто", МенеджерОборудованияВызовСервера.ПолучитьРабочееМестоКлиента());
	Запрос.УстановитьПараметр("Касса",        Касса);
	
	ВозвращаемоеЗначение = Новый Структура;
	ВозвращаемоеЗначение.Вставить("Терминал");
	ВозвращаемоеЗначение.Вставить("ККТ");
	
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() Тогда
		ЗаполнитьЗначенияСвойств(ВозвращаемоеЗначение, Выборка);
	КонецЕсли;
	
	Возврат ВозвращаемоеЗначение;
	
КонецФункции

// Получить оборудование подключенное к кассе ККМ.
//
// Параметры:
//  КассаККМ - СправочникСсылка.КассыККМ - Касса ККМ.
// 
// Возвращаемое значение:
//  Структура - Структура по свойствами:
//   * Терминал - СправочникСсылка.ПодключаемоеОборудование - Терминал.
//   * ККТ - СправочникСсылка.ПодключаемоеОборудование - ККТ.
//
Функция ОборудованиеПодключенноеККассеККМ(КассаККМ) Экспорт
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	Т.Ссылка КАК Ссылка
	|ПОМЕСТИТЬ НастройкаРМК
	|ИЗ
	|	Справочник.НастройкиРМК КАК Т
	|ГДЕ
	|	Т.РабочееМесто = &РабочееМесто
	|
	|;
	|ВЫБРАТЬ ПЕРВЫЕ 1
	|	КассыККМ.ПодключаемоеОборудование КАК ККТ
	|ИЗ
	|	Справочник.НастройкиРМК.КассыККМ КАК КассыККМ
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ НастройкаРМК КАК НастройкаРМК
	|		ПО НастройкаРМК.Ссылка = КассыККМ.Ссылка
	|ГДЕ
	|	Не КассыККМ.ИспользоватьБезПодключенияОборудования
	|	И КассыККМ.КассаККМ = &КассаККМ");
	
	Запрос.УстановитьПараметр("РабочееМесто", МенеджерОборудованияВызовСервера.ПолучитьРабочееМестоКлиента());
	Запрос.УстановитьПараметр("КассаККМ",     КассаККМ);
	
	ВозвращаемоеЗначение = Новый Структура;
	ВозвращаемоеЗначение.Вставить("Терминал");
	ВозвращаемоеЗначение.Вставить("ККТ");
	
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() Тогда
		ЗаполнитьЗначенияСвойств(ВозвращаемоеЗначение, Выборка);
	КонецЕсли;
	
	Возврат ВозвращаемоеЗначение;
	
КонецФункции

// Получить оборудование подключенное по организации.
//
// Параметры:
//  Организация - ОпределяемыйТип.ОрганизацияБПО - Организация.
// 
// Возвращаемое значение:
//  Структура - Структура по свойствами:
//   * Терминал - СправочникСсылка.ПодключаемоеОборудование - Терминал.
//   * ККТ - СправочникСсылка.ПодключаемоеОборудование - ККТ.
//
Функция ОборудованиеПодключенноеПоОрганизации(Организация) Экспорт
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ
	|	ПодключаемоеОборудование.Ссылка КАК ККТ
	|ИЗ
	|	Справочник.ПодключаемоеОборудование КАК ПодключаемоеОборудование
	|ГДЕ
	|	ПодключаемоеОборудование.Организация = &Организация
	|	И ПодключаемоеОборудование.УстройствоИспользуется
	|	И ПодключаемоеОборудование.РабочееМесто = &РабочееМесто");
	
	Запрос.УстановитьПараметр("РабочееМесто", МенеджерОборудованияВызовСервера.ПолучитьРабочееМестоКлиента());
	Запрос.УстановитьПараметр("Организация",  Организация);
	
	ВозвращаемоеЗначение = Новый Структура;
	ВозвращаемоеЗначение.Вставить("Терминал");
	ВозвращаемоеЗначение.Вставить("ККТ", Новый Массив);
	
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		ВозвращаемоеЗначение.ККТ.Добавить(Выборка.ККТ);
	КонецЦикла;
	
	Возврат ВозвращаемоеЗначение;
	
КонецФункции

// Получить данные журнала фискальных операций.
//
// Параметры:
//  ДокументСсылка - ДокументСсылка - Документ-основание.
// 
// Возвращаемое значение:
//  Структура - Структура по свойствами:
//   * ИдентификаторЗаписи - Строка - Идентификатор записи.
//   * НомерЧекаККМ - Число - Номер чека ККМ.
//   * Сумма - Число - Сумма.
//   * ДокументОснование - ДокументСсылка - Документ-основание.
//   * Данные - ХранилищеЗначений - Данные чека, переданные в ККТ (XML).
//
Функция ДанныеЖурналаФискальныхОпераций(ДокументСсылка) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	Запрос = Новый Запрос(
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	ЖурналФискальныхОпераций.ФискальнаяОперацияНомерЧекаККМ КАК НомерЧекаККМ,
	|	ЖурналФискальныхОпераций.ФискальнаяОперацияСумма        КАК Сумма,
	|	ЖурналФискальныхОпераций.ДокументОснование              КАК ДокументОснование,
	|	ЖурналФискальныхОпераций.Данные                         КАК Данные,
	|	ЖурналФискальныхОпераций.ИдентификаторЗаписи            КАК ИдентификаторЗаписи
	|ИЗ
	|	РегистрСведений.ЖурналФискальныхОпераций КАК ЖурналФискальныхОпераций
	|ГДЕ
	|	ЖурналФискальныхОпераций.ДокументОснование = &ДокументСсылка
	|");
	Запрос.УстановитьПараметр("ДокументСсылка", ДокументСсылка);
	
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() Тогда
		
		ФискальнаяОперация = Новый Структура;
		ФискальнаяОперация.Вставить("ИдентификаторЗаписи");
		ФискальнаяОперация.Вставить("НомерЧекаККМ");
		ФискальнаяОперация.Вставить("Сумма");
		ФискальнаяОперация.Вставить("ДокументОснование");
		ФискальнаяОперация.Вставить("Данные");
		
		ЗаполнитьЗначенияСвойств(ФискальнаяОперация, Выборка);
		
		Возврат ФискальнаяОперация;
		
	КонецЕсли;
	
	Возврат Неопределено;
	
КонецФункции

// Записать операцию в журнал фискальных операций.
//
// Параметры:
//  ТребуетсяПовторнаяПопыткаЗаписи - Булево - Требуется повторная попытка записи.
//  РеквизитыОперацииКассовогоУзла - Структура - см. функцию ПодключаемоеОборудованиеУТКлиент.СтруктураРеквизитыФискальнойОперацииКассовогоУзла().
//
Функция ЗаписатьВЖурналФискальныхОпераций(ТребуетсяПовторнаяПопыткаЗаписи, Знач РеквизитыФискальнойОперацииКассовогоУзла) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	НачатьТранзакцию();
	Попытка
		
		Блокировка = Новый БлокировкаДанных;
		ЭлементБлокировки = Блокировка.Добавить();
		ЭлементБлокировки.Область = "РегистрСведений.ЖурналФискальныхОпераций";
		ЭлементБлокировки.Режим = РежимБлокировкиДанных.Разделяемый;
		ЭлементБлокировки.УстановитьЗначение("ДокументОснование", РеквизитыФискальнойОперацииКассовогоУзла.ДокументОснование);
		Блокировка.Заблокировать();
		
		ВнесениеДенежныхСредствВКассуККМ = Неопределено;
		Если РеквизитыФискальнойОперацииКассовогоУзла.ДополнительныеПараметры.Свойство(
				"ВнесениеДенежныхСредствВКассуККМ",
				ВнесениеДенежныхСредствВКассуККМ)
				И ВнесениеДенежныхСредствВКассуККМ <> Неопределено Тогда
			РеквизитыФискальнойОперацииКассовогоУзла.ДокументОснование = РозничныеПродажи.СоздатьДокументВнесениеДенежныхСредствВКассуККМ(ВнесениеДенежныхСредствВКассуККМ);
		КонецЕсли;
		
		ВыемкаДенежныхСредствИзКассыККМ = Неопределено;
		Если РеквизитыФискальнойОперацииКассовогоУзла.ДополнительныеПараметры.Свойство(
				"ВыемкаДенежныхСредствИзКассыККМ",
				ВыемкаДенежныхСредствИзКассыККМ)
				И ВыемкаДенежныхСредствИзКассыККМ <> Неопределено Тогда
			РеквизитыФискальнойОперацииКассовогоУзла.ДокументОснование = РозничныеПродажи.СоздатьДокументВыемкаДенежныхСредствИзКассыККМ(ВыемкаДенежныхСредствИзКассыККМ);
		КонецЕсли;
		
		ПодсистемаПодключаемоеОборудованиеСервер.ЗаписатьВЖурналФискальныхОперацийУпрощенно(РеквизитыФискальнойОперацииКассовогоУзла);
		
		Результат = Истина;
		
		ЗафиксироватьТранзакцию();
		
	Исключение
		
		ОтменитьТранзакцию();
		ТребуетсяПовторнаяПопыткаЗаписи = Истина;
		
		Результат = Ложь;
		
	КонецПопытки;
	
	Возврат Результат;
	
КонецФункции

#КонецОбласти
