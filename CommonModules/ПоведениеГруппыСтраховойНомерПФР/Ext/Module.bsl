﻿// TODO: Избавиться от имен элементов, заменив их соответствием ИмяЭлемента->ИмяРеквизита.

#Область РеализацияИнтерфейса

Функция ПолучитьИмяМодуля() Экспорт
	Возврат "ПоведениеГруппыСтраховойНомерПФР";
КонецФункции

Процедура Инициализировать(Форма, Отказ) Экспорт
	// 1. Создаем обработчики событий
	СоздатьОбработчикиСобытий(Форма);
	// 2. Создаем обработчики оповещений
	СоздатьОбработчикиОповещений(Форма);
КонецПроцедуры

Функция ПолучитьИменаРеквизитовГруппы(ТолькоОбязательныеДляОтображения = Ложь) Экспорт
	МассивИменРеквизитов_ = ПоведениеГрупп.СоздатьМассивЭлементов(
		"СтраховойНомерПФР"
	);
	Возврат МассивИменРеквизитов_;
КонецФункции

Функция ПолучитьИменаКомандГруппы(ТолькоОбязательныеДляОтображения = Ложь) Экспорт
	МассивКоманд_ = ПоведениеГрупп.СоздатьМассивЭлементов(
		"ПолучитьСтраховойНомерПФР"
	);
	Возврат МассивКоманд_;
КонецФункции

#КонецОбласти

#Область Инициализация

Процедура СоздатьОбработчикиСобытий(Форма)
	// 1. Сначала регистрируем события формы
	ИмяМодуля_ = ПолучитьИмяМодуля();
	// 2. Регистрируем события элементов формы
	// 2.1. Запоминаем вспомогательные переменные.
	СтруктураСобытий_ = Новый Структура;
	СтруктураСобытий_.Вставить("ПриИзменении", "СтраховойНомерПФР");
	// 2.2. Регистрируем обработчики события ПриИзменении элементов формы
	ПоведениеГрупп.ЗарегистрироватьОбработчикиСобытийЭлементовФормы(Форма, ИмяМодуля_, СтруктураСобытий_);
	
	// 3. Регистрируем обработчики команд формы
	ИменаКоманд_ = ПолучитьИменаКомандГруппы();
	Для Каждого ИмяКоманды_ Из ИменаКоманд_ Цикл
		Команда_ = Форма.Команды[ИмяКоманды_];
		ПоведениеГрупп.ЗарегистрироватьОбработчикСобытияКомандыФормы(Форма, Команда_, ИмяМодуля_);
	КонецЦикла;
КонецПроцедуры

Процедура СоздатьОбработчикиОповещений(Форма)
	ИмяМодуля_ = ПолучитьИмяМодуля();
	Оповещения_ = ПолучитьОбрабатываемыеОповещения();
	Для Каждого Оповещение_ Из Оповещения_ Цикл
		ПоведениеГрупп.ЗарегистрироватьОбработчикОповещенияГрупп(Форма, ИмяМодуля_, Оповещение_);
	КонецЦикла;
КонецПроцедуры

#КонецОбласти

#Область ОбработкаКомандФормы

&НаКлиенте
Процедура Команда_Выполнить(Форма, Команда) Экспорт
	ИмяКоманды_ = Команда.Имя;
	Если      ИмяКоманды_ = "ПолучитьСтраховойНомерПФР" Тогда
		ПолучитьСтраховойНомерПФР_Выполнить(Форма, Команда);
	Иначе
		__ПРОВЕРКА__(Ложь, "Событие команды " + ИмяКоманды_ + " не обрабатывается. Обратитесь к разработчику.");
	КонецЕсли;
КонецПроцедуры

#Область ОбработчикиКоманд

&НаКлиенте
Процедура ПолучитьСтраховойНомерПФР_Выполнить(Форма, Команда)
	
	ОписаниеОповещения_ = Новый ОписаниеОповещения(
		"ПослеПолученияСтраховогоНомераПФР",
		ЭтотОбъект,
		Форма
	);
	
	// Синтаксический контроль
	Если Ложь Тогда
		ПослеПолученияСтраховогоНомераПФР(0, 0);
	КонецЕсли;
	
	ОбменДаннымиПФР.ЗапростьСНИЛС(
		Форма.Объект.Фамилия,
		Форма.Объект.Имя,
		Форма.Объект.Отчество,
		Форма.Объект.ДатаРождения,
		Форма.Объект.Пол,
		Истина,
		ОписаниеОповещения_
	);
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеПолученияСтраховогоНомераПФР(Результат, Форма) Экспорт
	
	Если ЗначениеЗаполнено(Результат) Тогда
		Форма.Объект.СтраховойНомерПФР = Результат;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область ОбработкаСобытийЭлементовФормы

Процедура Элемент_ПриИзменении(Форма, Элемент) Экспорт
	ИмяРеквизита_ = ПоведениеГрупп.ПолучитьИмяРеквизитаПоИмениЭлемента(Форма, Элемент.Имя);
	Если      ИмяРеквизита_ = "СтраховойНомерПФР" Тогда
		СтраховойНомерПФР_ПриИзменении(Форма, Элемент);
	Иначе
		__ПРОВЕРКА__(Ложь, "Событие ПриИзменении элемента " + Элемент.Имя + " не обрабатывается. Обратитесь к разработчику.");
	КонецЕсли;
КонецПроцедуры

#Область ОбработчикиСобытийЭлементов

Процедура СтраховойНомерПФР_ПриИзменении(Форма, Элемент)
	СтраховойНомерПФР_ = Форма.Объект.СтраховойНомерПФР;
	
	СтрокаСтраховойНомерПФР_ = ""
		+ СтрЗаменить( Лев(СтраховойНомерПФР_, 3   ), " ", "")
		+ СтрЗаменить(Сред(СтраховойНомерПФР_, 5, 3), " ", "")
		+ СтрЗаменить(Сред(СтраховойНомерПФР_, 9, 3), " ", "")
		+ СтрЗаменить(Прав(СтраховойНомерПФР_, 2   ), " ", "")
	;
	ЧислоДополнительныхПробелов_ = 11 - СтрДлина(СтраховойНомерПФР_);
	СтрокаСтраховойНомерПФР_ = СтрокаСтраховойНомерПФР_ + Лев("           ", ЧислоДополнительныхПробелов_);
	
	Форма.Объект.СтраховойНомерПФР = ""
		+        Лев(СтрокаСтраховойНомерПФР_, 3)
		+ "-" + Сред(СтрокаСтраховойНомерПФР_, 4, 3)
		+ "-" + Сред(СтрокаСтраховойНомерПФР_, 7, 3)
		+ " " + Прав(СтрокаСтраховойНомерПФР_, 2)
	;
	
	ИмяМодуля_ = ПолучитьИмяМодуля();
	ЭлементСтраховойНомерПФР_ = ПоведениеГрупп.ПолучитьЭлементПоИмениРеквизита(Форма, "СтраховойНомерПФР");
	РегистратураКлиентСервер.КонтрольОсновныхДанных_СтраховойНомерПФР(ЭлементСтраховойНомерПФР_, Форма.Объект.СтраховойНомерПФР);
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область ОбработкаОповещенийПоведенияГрупп

&НаКлиенте
Процедура ОбработкаОповещенияПоведенияГруппНаКлиенте(Форма, ИмяСобытия, Параметры) Экспорт
	Если ИмяСобытия = "УстановитьДоступность" Тогда
		ОбработкаОповещения_УстановитьДоступность(Форма, Параметры);
	Иначе
		__ПРОВЕРКА__(Ложь, "Необработанное событие: " + ИмяСобытия + ". Обратитесь к разработчику.");
	КонецЕсли;
КонецПроцедуры

Процедура ОбработкаОповещенияПоведенияГруппНаСервере(Объект, ИмяСобытия, Параметры) Экспорт
КонецПроцедуры

#Область ОбработчикиОповещений

&НаКлиенте
Процедура ОбработкаОповещения_УстановитьДоступность(Форма, Параметры)
	__ТИП__(Параметры, "Структура");
	__ПРОВЕРКА__(Параметры.Свойство("Доступность"), "Не установлен параметр Доступность. Обратитесь к разработчику");
	ИмяМодуля_ = ПолучитьИмяМодуля();
	Доступность_ = Параметры.Доступность;
	ИменаЭлементов_ = ПоведениеГрупп.ПолучитьИменаЭлементовГруппы(Форма, ИмяМодуля_);
	Для Каждого ИмяЭлемента_ Из ИменаЭлементов_ Цикл
		Элемент_ = Форма.Элементы.Найти(ИмяЭлемента_);
		__ПРОВЕРКА__(Не Элемент_ = Неопределено, "Элемент " + ИмяЭлемента_ + " не найден на форме. Обратитесь к разработчику.");
		Если ТипЗнч(Элемент_) = Тип("КнопкаФормы") Тогда
			Элемент_.Доступность = Доступность_;
		Иначе
			Элемент_.ТолькоПросмотр = Не Доступность_;
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

#КонецОбласти

#Область ВспомогательныеМетоды

Функция ПолучитьОбрабатываемыеОповещения()
	Возврат ПоведениеГрупп.СоздатьМассивЭлементов(
		"УстановитьДоступность"
	);
КонецФункции

#КонецОбласти

#КонецОбласти
