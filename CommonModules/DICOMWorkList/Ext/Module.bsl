﻿#Область ПрограммныйИнтерфейсWorkList

// Получает данные запроса WorkList(текст и параметры)
//
//
// Возвращаемое значение:
//   Структура(ТекстЗапроса, ПараметрыЗапроса)   - данные запроса.
//
Функция ДанныеЗапросаWorkList() Экспорт
	
	Результат = Новый Структура("ТекстЗапроса, ПараметрыЗапроса", "", Новый Структура);
	
	ОбработчикиСобытия = ОбщегоНазначения.ОбработчикиСлужебногоСобытия(
		"ПодсистемаDICOM\ПриОпределенииДанныхЗапросаWorkList");
		
	Для каждого Обработчик Из ОбработчикиСобытия Цикл
		Обработчик.Модуль.ПриОпределенииДанныхЗапросаWorkList(Результат);
	КонецЦикла;
	
	Возврат Результат;
	
КонецФункции // ДанныеЗапросаWorkList()

////
 // Процедура: ОчиститьWorkList
 //   Очистка DICOMWorkList для услуги.
///
Функция ОчиститьWorkList(Знач УникальныйИдентификаторИсследования, Знач КлючСтрокиСоставаУслуги) Экспорт

	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	DicomWorkList.УникальныйИдентификаторИсследования
	|ИЗ
	|	РегистрСведений.DicomWorkList КАК DicomWorkList
	|ГДЕ
	|";

	Если УникальныйИдентификаторИсследования <> Неопределено Тогда
		Запрос.Текст = Запрос.Текст
		+ " DicomWorkList.УникальныйИдентификаторУслуги = &УникальныйИдентификаторИсследования";
		
		Запрос.УстановитьПараметр("УникальныйИдентификаторИсследования", УникальныйИдентификаторИсследования);
	КонецЕсли;	
	
	Если КлючСтрокиСоставаУслуги <> Неопределено Тогда
		Запрос.Текст = Запрос.Текст
		+ " И DicomWorkList.КлючСтрокиСоставаУслуги = &КлючСтрокиСоставаУслуги";
		
		Запрос.УстановитьПараметр("КлючСтрокиСоставаУслуги", КлючСтрокиСоставаУслуги);
	КонецЕсли;
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	НаборЗаписей_ = РегистрыСведений.DicomWorkList.СоздатьНаборЗаписей();
	
	Результат_ = Неопределено;
	Пока Выборка.Следующий() Цикл
		НаборЗаписей_.Отбор.УникальныйИдентификаторИсследования.Установить(
							Выборка.УникальныйИдентификаторИсследования);
		Результат_ = Выборка.УникальныйИдентификаторИсследования;
		НаборЗаписей_.Записать();
	КонецЦикла;
	
	Возврат Результат_;

КонецФункции

///
// Функция: ОпределитьModalityИсследований
//    Возвращает массив Modality выполненных услуг медицинского документа.
//
// Параметры: 
//   Параметр - произвольный тип
//
// Возврат: {Массив}
///
Функция ОпределитьModalityИсследований(Знач Параметр) Экспорт
	
	Результат = Новый Массив;
	
	ОбработчикиСобытия = ОбщегоНазначения.ОбработчикиСлужебногоСобытия(
		"ПодсистемаDICOM\ПриОпределенииModalityИсследований");
		
	Для каждого Обработчик Из ОбработчикиСобытия Цикл
		Обработчик.Модуль.ПриОпределенииModalityИсследований(Параметр, Результат);
	КонецЦикла;
	
	Возврат Результат;
	
КонецФункции

#КонецОбласти
