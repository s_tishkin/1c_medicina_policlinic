﻿
#Область ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// Модуль "ПродажиКлиент", содержит процедуры и функции для 
// обработки действий пользователя в процессе работы с документами продажи.
//

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ И ФУНКЦИИ РАБОТЫ С ВЫБОРОМ ЗНАЧЕНИЙ

// Процедура - обработчик события "НачалоВыбора" элемента формы "Соглашение"
//
// Параметры:
// Партнер  - СправочникСсылка.Партнеры - ссылка на партнера, для которого необходимо выбрать соглашение.
// Документ - СправочникСсылка.СоглашенияСКлиентами - ссылка на ранее выбранное соглашение
//                                                    для начального позиционирования в списке.
// ТолькоТиповые                - Булево - флаг, позволяющий включать в список выбора только типовые соглашения.
// ТолькоИспользуемыеВРаботеТП  - Булево - флаг, позволяющий влкючаеть в список выбора только соглашения,
// используемые в работе торговых представителей.
// ТолькоНаКомиссию             - Булево - флаг, позволяющий влкючаеть в список выбора только комиссионные соглашения.
// ТолькоКредитные - Булево - флаг, определяющие необходимость отбора только соглашений с кредитными графиками оплаты.
//
Процедура НачалоВыбораСоглашенияСКлиентом(
	Элемент, 
	СтандартнаяОбработка, 
	Партнер, 
	ЭлементПартнер, 
	Документ,
	ДатаДокумента='00010101',
	ТолькоТиповые=Ложь,
	ТолькоИндивидуальные=Ложь,
	ТолькоНаКомиссию=Ложь,
	ТолькоКредитные=Ложь,
	Договор = Неопределено
) Экспорт
	
	СтандартнаяОбработка = Ложь;
	
	Параметры_ = Новый Структура;
	Параметры_.Вставить("Партнер", Партнер);
	Параметры_.Вставить("Элемент", Элемент);
	Параметры_.Вставить("Документ", Документ);
	Параметры_.Вставить("ДатаДокумента", ДатаДокумента);
	Параметры_.Вставить("ТолькоТиповые", ТолькоТиповые);
	Параметры_.Вставить("ТолькоИндивидуальные", ТолькоИндивидуальные);
	Параметры_.Вставить("ТолькоНаКомиссию", ТолькоНаКомиссию);
	Параметры_.Вставить("ТолькоКредитные", ТолькоКредитные);
	Параметры_.Вставить("Договор", Договор);
	
	Если Не ЗначениеЗаполнено(Партнер) Тогда
		ПараметрыФормы_ = Новый Структура("Отбор", Новый Структура("Клиент", Истина));
		Оповещение_ = Новый ОписаниеОповещения(
			"НачалоВыбораСоглашенияСКлиентомПослеВыбораПартнера", ЭтотОбъект, Параметры_
		);
		ОткрытьФорму(
			"Справочник.Партнеры.ФормаВыбора", ПараметрыФормы_, ЭлементПартнер, ,,,Оповещение_,
			РежимОткрытияОкнаФормы.БлокироватьОкноВладельца
		);
	Иначе
		НачалоВыбораСоглашенияСКлиентомПослеВыбораПартнера(Партнер, Параметры_);
	КонецЕсли;
		
КонецПроцедуры // НачалоВыбораСоглашенияСКлиентом()

////
 // Процедура: НачалоВыбораСоглашенияСКлиентомПослеВыбораПартнера
 //   Вызывается после закрытия формы выбора партнера, либо напрямую
 //
 // Параметры:
 //   П1 (Тип)
 //     Описание
 //   П2 (Тип)
 //     Описание
///
Процедура НачалоВыбораСоглашенияСКлиентомПослеВыбораПартнера(Партнер, ДополнительныеПараметры) Экспорт

	Если Не ЗначениеЗаполнено(Партнер) Тогда
		Возврат;
	КонецЕсли;
	
	Параметры_ = Новый Структура;
	Параметры_.Вставить("Партнер", Партнер);
	Параметры_.Вставить("Договор", ДополнительныеПараметры.Договор);
	Параметры_.Вставить("ДатаДокумента", ДополнительныеПараметры.ДатаДокумента);
	Параметры_.Вставить("ТолькоТиповые", ДополнительныеПараметры.ТолькоТиповые);
	Параметры_.Вставить("ТолькоИндивидуальные", ДополнительныеПараметры.ТолькоИндивидуальные);
	Параметры_.Вставить("ТолькоНаКомиссию", ДополнительныеПараметры.ТолькоНаКомиссию);
	Параметры_.Вставить("ТолькоКредитные", ДополнительныеПараметры.ТолькоКредитные);
	Параметры_.Вставить("ТекущаяСтрока", ДополнительныеПараметры.Документ);
	ОткрытьФорму("Справочник.СоглашенияСКлиентами.ФормаВыбора", Параметры_, ДополнительныеПараметры.Элемент);
КонецПроцедуры


////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ ОПОВЕЩЕНИЯ ПОЛЬЗОВАТЕЛЯ О ВЫПОЛНЕННЫХ ДЕЙСТВИЯХ

// Показывает оповещение пользователя об окончании заполнения условий продаж по умолчанию.
//
Процедура ОповеститьОбОкончанииЗаполненияУсловийПродажПоУмолчанию() Экспорт

	ПоказатьОповещениеПользователя(
		СообщенияПользователю.Получить("Общие_УсловияПродажЗаполнены"),
		,
		СообщенияПользователю.Получить("Общие_УсловияПродажПоУмолчаниюЗаполнены"),
		БиблиотекаКартинок.Информация32
	);

КонецПроцедуры // ОповеститьОбОкончанииЗаполненияУсловийПродажПоУмолчанию()

// Показывает оповещение пользователя об окончании заполнения условий продаж
//
Процедура ОповеститьОбОкончанииЗаполненияУсловийПродаж() Экспорт

	ПоказатьОповещениеПользователя(
		СообщенияПользователю.Получить("Общие_УсловияПродажЗаполнены"),
		,
		СообщенияПользователю.Получить("Общие_УсловияПродажПоУмолчаниюЗаполнены"),
		БиблиотекаКартинок.Информация32
	);

КонецПроцедуры // ОповеститьОбОкончанииЗаполненияУсловийПродаж()

// Показывает оповещение пользователя об окончании заполнения цен по соглашению с клиентом.
//
// Параметры:
// ЦеныРассчитаны - Булево - Признак успешного расчета цен хотя бы в одной строке.
//
Процедура ОповеститьОбОкончанииЗаполненияЦенПоСоглашению(ЦеныРассчитаны = Истина) Экспорт

	Если ЦеныРассчитаны Тогда
		ПоказатьОповещениеПользователя(
			СообщенияПользователю.Получить("Общие_ЦеныЗаполнены"),
			,
			СообщенияПользователю.Получить("Общие_ЦеныПоСоглашениюСКлиентомЗаполнены"),
			БиблиотекаКартинок.Информация32
		);
	Иначе
		ПоказатьОповещениеПользователя(
			СообщенияПользователю.Получить("Общие_ЦеныНеЗаполнены"),
			,
			СообщенияПользователю.Получить("Общие_НиВОднойСтрокеЦеныПоСоглашениюСКлиентомНеЗаполнены"),
			БиблиотекаКартинок.Информация32
		);
	КонецЕсли;
	
КонецПроцедуры // ОповеститьОбОкончанииЗаполненияЦенПоСоглашению()

// Показывает оповещение пользователя об окончании заполнения цен по виду цен
//
// Параметры:
// ЦеныРассчитаны - Булево - Признак успешного расчета цен хотя бы в одной строке.
// ВидЦен - СправочникСсылка.ВидыЦен - Вид цен, по которому осуществлялось заполнение цен.
//
Процедура ОповеститьОбОкончанииЗаполненияЦенПоВидуЦен(ЦеныРассчитаны = Истина, ВидЦен) Экспорт

	Если ЦеныРассчитаны Тогда
		ПоказатьОповещениеПользователя(
			СообщенияПользователю.Получить("Общие_ЦеныЗаполнены"),
			,
			СообщенияПользователю.Получить("Общие_ЦеныПоВидуЦенЗаполнены",Новый Структура("ВидЦен",ВидЦен)),
			БиблиотекаКартинок.Информация32
		);
	Иначе
		ПоказатьОповещениеПользователя(
			СообщенияПользователю.Получить("Общие_ЦеныЗаполнены"),
			,
			СообщенияПользователю.Получить("Общие_НиВОднойСтрокеЦеныПоВидуЦенНеЗаполнены",Новый Структура("ВидЦен",ВидЦен)),
			БиблиотекаКартинок.Информация32
		);
	КонецЕсли;
	
КонецПроцедуры // ОповеститьОбОкончанииЗаполненияЦенПоВидуЦен()

// Процедура показывает оповещение после обработки выделенных в списке соглашений с клиентами.
//
// Параметры
// КоличествоОбработанных - Число - количество успешно обработанных соглашений с клиентами.
// СписокДокументов       - ДинамическийСписок - Элемент формы
// Статус                 - Установленный статус
//
Процедура ОповеститьПользователяОбУстановкеСтатуса(КоличествоОбработанных, СписокДокументов, Статус) Экспорт
	
	Если КоличествоОбработанных > 0 Тогда
		
		Если ТипЗнч(СписокДокументов) <> Тип("Структура") Тогда
			СписокДокументов.Обновить();
		КонецЕсли;
		Параметры_ = Новый Структура();
		Параметры_.Вставить("КоличествоОбработанных",КоличествоОбработанных);
		Параметры_.Вставить("КоличествоВсего",СписокДокументов.ВыделенныеСтроки.Количество());
		Параметры_.Вставить("Статус",Статус);
		ТекстСообщения = СообщенияПользователю.Получить("Общие_ДляВыделенныхСоглашенийУстановленСтатус",Параметры_);
		ТекстЗаголовка = СообщенияПользователю.Получить("Общие_ЗаголовокСтатусУстановлен",Новый Структура("Статус",Статус));
		ПоказатьОповещениеПользователя(ТекстЗаголовка,, ТекстСообщения, БиблиотекаКартинок.Информация32);
		
	Иначе
		
		ТекстСообщения = СообщенияПользователю.Получить("Общие_СтатусНеУстановлен",Новый Структура("Статус",Статус));
		ТекстЗаголовка = СообщенияПользователю.Получить("Общие_ЗаголовокСтатусНеУстановлен",Новый Структура("Статус",Статус));
		ПоказатьОповещениеПользователя(ТекстЗаголовка,, ТекстСообщения, БиблиотекаКартинок.Информация32);
		
	КонецЕсли;
	
КонецПроцедуры // ОповеститьПользователя()

// Показывает оповещение пользователя об отмене выделенных строк
//
// Параметры:
// ПричинаОтмены - СправочникСсылка.ПричиныОтмены - Причина отмены выделенных строк.
//
Процедура ОповеститьОбОтменеВыделенныхСтрок(ПричинаОтмены) Экспорт

		ПоказатьОповещениеПользователя(
			СообщенияПользователю.Получить("Общие_ВыделенныеСтрокиОтменены"),
			,
			СообщенияПользователю.Получить("Общие_ВыделенныеСтрокиОтмененыПоПричине",Новый Структура("ПричинаОтмены",ПричинаОтмены)),
			БиблиотекаКартинок.Информация32
		);
	
КонецПроцедуры // ОповеститьОбОтменеВыделенныхСтрок()

// Показывает оповещение пользователя об отмене выделенных строк
//
// Параметры:
// ПричинаОтмены - СправочникСсылка.ПричиныОтмены - Причина отмены выделенных строк.
//
Процедура ОповеститьОбОтменеНепоставленныхСтрок(ПричинаОтмены, КоличествоОтмененныхСтрок, ПроверятьОстатки) Экспорт

	Если КоличествоОтмененныхСтрок = 0 Тогда
			
		ПоказатьОповещениеПользователя(
			СообщенияПользователю.Получить("Общие_СтрокиНеОтменены"),
			,
			СообщенияПользователю.Получить("Общие_СтрокиНеМогутБытьОтменены"),
			БиблиотекаКартинок.Информация32
		);
			
		Иначе
			
			Если ПроверятьОстатки Тогда

				ПоказатьОповещениеПользователя(
					СообщенияПользователю.Получить("Общие_СтрокиОтменены"),
					,
					СообщенияПользователю.Получить("Общие_НепоставленныеСтрокиОтмененыПоПричине",Новый Структура("ПричинаОтмены",ПричинаОтмены)),
					БиблиотекаКартинок.Информация32
				);
			
			Иначе
				
				ПоказатьОповещениеПользователя(
					СообщенияПользователю.Получить("Общие_СтрокиОтменены"),
					,
					СообщенияПользователю.Получить("Общие_СтрокиОтмененыПоПричине",Новый Структура("ПричинаОтмены",ПричинаОтмены)),
					БиблиотекаКартинок.Информация32
				);
				
			КонецЕсли;
			
		КонецЕсли;
	
КонецПроцедуры // ОповеститьОбОтменеНепоставленныхСтрок()

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ ОБРАБОТКИ КОМАНД ПОЛЬЗОВАТЕЛЯ

// Устанавливает у выделенных в списке соглашений статус "НеСогласовано".
//
// Параметры:
// Список - ДинамическийСписок - Список соглашений.
//
Процедура УстановитьСтатусСоглашенийСКлиентамиНеСогласовано(Список) Экспорт
	
	ТекстВопроса = СообщенияПользователю.Получить("Соглашения_ВопросВСпискеСоглашенийБудетУстановленСтатусНеСогласовано");
	
	Параметры_ = Новый Структура("Список, ВыделенныеСтроки", Список, Список.ВыделенныеСтроки);
	ПоказатьВопрос(
		Новый ОписаниеОповещения("УстановитьСтатусСоглашенийСКлиентамиНеСогласованоПослеВопроса", ЭтотОбъект, Параметры_),
		ТекстВопроса,РежимДиалогаВопрос.ДаНет
	);
	
КонецПроцедуры // УстановитьСтатусСоглашенийСКлиентамиНеСогласовано()

////
 // Процедура: УстановитьСтатусСоглашенийСКлиентамиНеСогласованоПослеВопроса
 //   Вызывается после вопроса
 //
 // Параметры:
 //   П1 (Тип)
 //     Описание
 //   П2 (Тип)
 //     Описание
///
Процедура УстановитьСтатусСоглашенийСКлиентамиНеСогласованоПослеВопроса(Результат, Параметры) Экспорт
	Если Результат = КодВозвратаДиалога.Нет Тогда
		Возврат;
	КонецЕсли;
	
	КоличествоОбработанных = ПродажиСервер.УстановитьСтатусСоглашенийСКлиентамиНеСогласовано(Параметры.ВыделенныеСтроки);
	ОповеститьПользователяОбУстановкеСтатуса(КоличествоОбработанных, Параметры.Список, "Не согласовано");
КонецПроцедуры

// Устанавливает у выделенных в списке соглашений статус "Действует".
//
// Параметры:
// Список - ДинамическийСписок - Список соглашений.
//
Процедура УстановитьСтатусСоглашенийСКлиентамиДействует(Список) Экспорт
	
	ТекстВопроса = СообщенияПользователю.Получить("Соглашения_ВопросВСпискеСоглашенийБудетУстановленСтатусДействует");
	
	Параметры_ = Новый Структура("Список, ВыделенныеСтроки", Список, Список.ВыделенныеСтроки);
	ПоказатьВопрос(
		Новый ОписаниеОповещения("УстановитьСтатусСоглашенийСКлиентамиДействуетПослеВопроса", ЭтотОбъект, Параметры_),
		ТекстВопроса,РежимДиалогаВопрос.ДаНет
	);

	
КонецПроцедуры // УстановитьСтатусДействует()

////
 // Процедура: УстановитьСтатусСоглашенийСКлиентамиДействуетПослеВопроса
 //   Вызывается после вопроса
 //
 // Параметры:
 //   П1 (Тип)
 //     Описание
 //   П2 (Тип)
 //     Описание
///
Процедура УстановитьСтатусСоглашенийСКлиентамиДействуетПослеВопроса(Результат, Параметры) Экспорт
	Если Результат = КодВозвратаДиалога.Нет Тогда
		Возврат;
	КонецЕсли;
	
	КоличествоОбработанных = ПродажиСервер.УстановитьСтатусСоглашенийСКлиентамиДействует(Параметры.ВыделенныеСтроки);
	ОповеститьПользователяОбУстановкеСтатуса(КоличествоОбработанных, Параметры.Список, "Действует");
КонецПроцедуры

// Устанавливает у выделенных в списке соглашений статус "Закрыто".
//
// Параметры:
// Список - ДинамическийСписок - Список соглашений.
//
Процедура УстановитьСтатусСоглашенийСКлиентамиЗакрыто(Список) Экспорт
	
	ТекстВопроса = СообщенияПользователю.Получить("Соглашения_ВопросВСпискеСоглашенийБудетУстановленСтатусЗакрыто");
	
	Параметры_ = Новый Структура("Список, ВыделенныеСтроки", Список, Список.ВыделенныеСтроки);
	ПоказатьВопрос(
		Новый ОписаниеОповещения("УстановитьСтатусСоглашенийСКлиентамиЗакрытоПослеВопроса", ЭтотОбъект, Параметры_),
		ТекстВопроса,РежимДиалогаВопрос.ДаНет
	);

	
КонецПроцедуры // УстановитьСтатусЗакрыто()

////
 // Процедура: УстановитьСтатусСоглашенийСКлиентамиЗакрытоПослеВопроса
 //   Вызывается после вопроса
 //
 // Параметры:
 //   П1 (Тип)
 //     Описание
 //   П2 (Тип)
 //     Описание
///
Процедура УстановитьСтатусСоглашенийСКлиентамиЗакрытоПослеВопроса(Результат, Параметры) Экспорт
	Если Результат = КодВозвратаДиалога.Нет Тогда
		Возврат;
	КонецЕсли;
	
	КоличествоОбработанных = ПродажиСервер.УстановитьСтатусСоглашенийСКлиентамиЗакрыто(Параметры.ВыделенныеСтроки);
	ОповеститьПользователяОбУстановкеСтатуса(КоличествоОбработанных, Параметры.Список, "Закрыто");
КонецПроцедуры

// Устанавливает у выделенных в таблице строк признак Отменено и заполняет причину отмены.
//
// Параметры:
// ТабличнаяЧасть   - ДанныеФормыКоллекция - табличная часть, в которой необходимо осуществить проверку.
// ВыделенныеСтроки - Массив - массив выделенных пользователем строк табличной части.
// ПричинаОтмены    - СправочникСсылка.ПричиныОтменыЗаказов - причина, по которой отменяются строки.
//
Процедура ОтменитьВыделенныеСтроки(ТабличнаяЧасть, ВыделенныеСтроки, ПричинаОтмены) Экспорт
	
	Для Каждого ТекСтрока Из ВыделенныеСтроки Цикл
			
		СтрокаТаблицы = ТабличнаяЧасть.НайтиПоИдентификатору(ТекСтрока);
			
		Если СтрокаТаблицы <> Неопределено Тогда
				
			Если Не СтрокаТаблицы.Отменено Тогда
				СтрокаТаблицы.Отменено = Истина;
			КонецЕсли;
				
			Если СтрокаТаблицы.ПричинаОтмены <> ПричинаОтмены Тогда
				СтрокаТаблицы.ПричинаОтмены = ПричинаОтмены;
			КонецЕсли;
				
		КонецЕсли;
			
	КонецЦикла;

КонецПроцедуры // ОтменитьВыделенныеСтроки()



#КонецОбласти