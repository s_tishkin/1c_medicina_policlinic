﻿
#Область ПрограммныйИнтерфейс

// Получить стандарты медицинской помощи по диагнозу.
//
Функция ПолучитьПоДиагнозу(Диагноз) Экспорт
	Возврат ПолучитьПоДиагнозам(ОбщегоНазначенияКлиентСервер.ЗначениеВМассиве(Диагноз));
КонецФункции

// Получить стандарты медицинской помощи по диагнозам.
//
Функция ПолучитьПоДиагнозам(Диагнозы) Экспорт
	Запрос_ = Новый Запрос(
		"ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	СтандартыМедицинскойПомощи.Ссылка
		|ИЗ
		|	Справочник.СтандартыМедицинскойПомощи КАК СтандартыМедицинскойПомощи
		|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.СтандартыМедицинскойПомощи.Заболевания КАК СтандартыМедицинскойПомощиЗаболевания
		|		ПО СтандартыМедицинскойПомощи.Ссылка = СтандартыМедицинскойПомощиЗаболевания.Ссылка
		|ГДЕ
		|	СтандартыМедицинскойПомощи.ПометкаУдаления = ЛОЖЬ
		|	И СтандартыМедицинскойПомощиЗаболевания.МКБ10 В(&Диагнозы)"
	);
	Запрос_.УстановитьПараметр("Диагнозы", Диагнозы);
	Результат_ = Запрос_.Выполнить();
	Возврат Результат_.Выгрузить().ВыгрузитьКолонку("Ссылка");
КонецФункции

// Проверить, что стандарт медицинской помощи требует согласования по соглашению.
//
Функция ТребуетСогласования(Знач Стандарт, Знач Соглашение) Экспорт
	Номенклатура_ = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Стандарт, "Номенклатура");
	Возврат РаботаССоглашениями.УслугаТребуетСогласования(Номенклатура_, Соглашение);
КонецФункции

// Проверить, что стандарт медицинской помощи исполняется по необходимости по соглашению.
//
Функция ПоНеобходимости(Знач Стандарт, Знач Соглашение) Экспорт
	Номенклатура_ = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Стандарт, "Номенклатура");
	Возврат РаботаССоглашениями.УслугаПоНеобходимости(Номенклатура_, Соглашение);
КонецФункции

// Получить услуги стандарта мед. помоши.
Функция ПолучитьУслуги(НоменклатураСтандарта) Экспорт
	Возврат Справочники.СтандартыМедицинскойПомощи.ПолучитьСоставСтандартаМедицинскойПомощи(
		НоменклатураСтандарта
	);	
КонецФункции

// Получить номенклатуру услуг стандарта мед. помоши.
Функция ПолучитьНоменклатуруУслуг(НоменклатураСтандарта) Экспорт
	Услуги_ = ПолучитьУслуги(НоменклатураСтандарта);
	Возврат ОбщегоНазначения.ВыгрузитьКолонку(Услуги_, "Номенклатура");
КонецФункции

#КонецОбласти