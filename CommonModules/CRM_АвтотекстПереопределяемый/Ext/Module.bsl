﻿
#Область ПрограммныйИнтерфейс


// Процедура формирует список автотекста
//
// Параметры:
//	ТаблицаАвтотекста	- ТаблицаЗначений	- Таблица автотекста.
//
Процедура СформироватьСписокАвтотекста(ТаблицаАвтотекста) Экспорт
	НовыйТэг = ТаблицаАвтотекста.Добавить();
	НовыйТэг.Тэг					= "%ВРЕМЯ%";
	НовыйТэг.ИмяКоманды				= "ВставитьВремя";
	НовыйТэг.ЗаголовокКоманды		= "%ВРЕМЯ%";
	НовыйТэг = ТаблицаАвтотекста.Добавить();
	НовыйТэг.Тэг					= "%ДАТА%";
	НовыйТэг.ИмяКоманды				= "ВставитьДату";
	НовыйТэг.ЗаголовокКоманды		= "%ДАТА%";
	НовыйТэг = ТаблицаАвтотекста.Добавить();
	НовыйТэг.Тэг					= "%ДАТА_ВРЕМЯ%";
	НовыйТэг.ИмяКоманды				= "ВставитьДату_Время";
	НовыйТэг.ЗаголовокКоманды		= "%ДАТА_ВРЕМЯ%";
	НовыйТэг = ТаблицаАвтотекста.Добавить();
	НовыйТэг.Тэг					= "%УВАЖАЕМЫЙ/УВАЖАЕМАЯ%";
	НовыйТэг.ИмяКоманды				= "УважаемыйУважаемая";
	НовыйТэг.ЗаголовокКоманды		= "%УВАЖАЕМЫЙ/УВАЖАЕМАЯ%";
	НовыйТэг = ТаблицаАвтотекста.Добавить();
	НовыйТэг.Тэг					= "%ПОЛУЧАТЕЛЬ%";
	НовыйТэг.ИмяКоманды				= "ВставитьПолучатель";
	НовыйТэг.ЗаголовокКоманды		= "%ПОЛУЧАТЕЛЬ%";
	НовыйТэг = ТаблицаАвтотекста.Добавить();
	НовыйТэг.Тэг					= "%ПОЛУЧАТЕЛЬ_ИМЯ%";
	НовыйТэг.ИмяКоманды				= "ВставитьПолучатель_Имя";
	НовыйТэг.ЗаголовокКоманды		= "%ПОЛУЧАТЕЛЬ_ИМЯ%";
	НовыйТэг = ТаблицаАвтотекста.Добавить();
	НовыйТэг.Тэг					= "%ПОЛУЧАТЕЛЬ_ИМЯ_ОТЧЕСТВО%";
	НовыйТэг.ИмяКоманды				= "ВставитьПолучатель_Имя_Отчество";
	НовыйТэг.ЗаголовокКоманды		= "%ПОЛУЧАТЕЛЬ_ИМЯ_ОТЧЕСТВО%";
	НовыйТэг = ТаблицаАвтотекста.Добавить();
	НовыйТэг.Тэг					= "%ПОЛУЧАТЕЛЬ_ФАМИЛИЯ_И_О%";
	НовыйТэг.ИмяКоманды				= "ВставитьПолучатель_Фамилия_И_О";
	НовыйТэг.ЗаголовокКоманды		= "%ПОЛУЧАТЕЛЬ_ФАМИЛИЯ_И_О%";
	НовыйТэг = ТаблицаАвтотекста.Добавить();
	НовыйТэг.Тэг					= "%УВАЖАЕМОМУ_ПОЛУЧАТЕЛЮ_ФАМИЛИЯ_ИМЯ_ОТЧЕСТВО%";
	НовыйТэг.ИмяКоманды				= "ВставитьПолучатель_Фамилия_И_О_1";
	НовыйТэг.ЗаголовокКоманды		= "%УВАЖАЕМОМУ_ПОЛУЧАТЕЛЮ_ФАМИЛИЯ_ИМЯ_ОТЧЕСТВО%";
КонецПроцедуры // СформироватьСписокАвтотекста()

// Процедура заполняет командную панель форматированного документа
//
// Параметры:
//	Форма	- Управляемая форма	- Форма, на которой заполняется командная панель.
//
Процедура ЗаполнитьКоманднуюПанельФорматированногоДокумента(Форма) Экспорт
	Для Каждого СтрокаАвтотекста ИЗ Форма.ТаблицаАвтотекста Цикл
		НоваяКомандаФормы = Форма.Команды.Добавить(СтрокаАвтотекста.ИмяКоманды);
		НоваяКомандаФормы.Заголовок					= СтрокаАвтотекста.ЗаголовокКоманды;
		НоваяКомандаФормы.Действие					= "Подключаемый_ВставитьТэг";
		НоваяКомандаФормы.ИзменяетСохраняемыеДанные	= Истина;
		НовыйПунктМеню = Форма.Элементы.Вставить(СтрокаАвтотекста.ИмяКоманды, Тип("КнопкаФормы"), Форма.Элементы.АвтоТекст);
		НовыйПунктМеню.ИмяКоманды			= СтрокаАвтотекста.ИмяКоманды;
		НовыйПунктМеню.Отображение			= ОтображениеКнопки.Текст;
	КонецЦикла;
КонецПроцедуры // ЗаполнитьКоманднуюПанельФорматированногоДокумента()

// Процедура заполняет командную панель сообщенями СМС
//
// Параметры:
//	Форма	- Управляемая форма	- Форма, на которой заполняется командная панель.
//
Процедура ЗаполнитьКоманднуюПанельСообщенияСМС(Форма) Экспорт
	Для Каждого СтрокаАвтотекста ИЗ Форма.ТаблицаАвтотекста Цикл
		ИмяКоманды = СтрокаАвтотекста.ИмяКоманды+"_СМС";
		НоваяКомандаФормы = Форма.Команды.Добавить(ИмяКоманды);
		НоваяКомандаФормы.Заголовок					= СтрокаАвтотекста.ЗаголовокКоманды;
		НоваяКомандаФормы.Действие					= "Подключаемый_ВставитьТэгСМС";
		НоваяКомандаФормы.ИзменяетСохраняемыеДанные	= Истина;
		НовыйПунктМеню = Форма.Элементы.Вставить(ИмяКоманды, Тип("КнопкаФормы"), Форма.Элементы.АвтоТекстСМС);
		НовыйПунктМеню.ИмяКоманды			= ИмяКоманды;
		НовыйПунктМеню.Отображение			= ОтображениеКнопки.Текст;
	КонецЦикла;
КонецПроцедуры // ЗаполнитьКоманднуюПанельСообщенияСМС()

// Функция получает значение тэга
//
// Параметры:
//	Тэг				- Строка				- Тэг, значение которого необходимо получить.
//	СтрокаТаблицы	- СтрокаТаблицыЗначений	- Строка таблицы значений
//
// Возвращаемое значение:
//	Строка	- Значение тэга
//
Функция ПолучитьЗначениеТэга(Тэг, СтрокаТаблицы) Экспорт
	Если Тэг = "%ВРЕМЯ%" Тогда
		Возврат Формат(ТекущаяДатаСеанса(),"ДЛФ=T");
	ИначеЕсли Тэг = "%ДАТА%" Тогда
		Возврат Формат(ТекущаяДатаСеанса(),"ДЛФ=D");
	ИначеЕсли Тэг = "%ДАТА_ВРЕМЯ%" Тогда
		Возврат Формат(ТекущаяДатаСеанса(),"ДЛФ=DT");
	ИначеЕсли Тэг = "%УВАЖАЕМЫЙ/УВАЖАЕМАЯ%" Тогда
		Если ЗначениеЗаполнено(СтрокаТаблицы.КонтактноеЛицо) Тогда
			Результат = "Уважаемый";
			Возврат Результат;
		ИначеЕсли ТипЗнч(СтрокаТаблицы.Партнер) = Тип("СправочникСсылка.Партнеры") Тогда
			Результат = "Уважаемый";
			Возврат Результат;
		ИначеЕсли ТипЗнч(СтрокаТаблицы.Партнер) = Тип("СправочникСсылка.ФизическиеЛица") Тогда
			Результат = "Уважаемый";
			Если СтрокаТаблицы.Партнер.Пол = Перечисления.ПолФизическогоЛица.Женский Тогда
				Результат = "Уважаемая";
			ИначеЕсли СтрокаТаблицы.Партнер.Пол = Перечисления.ПолФизическогоЛица.Мужской Тогда
				Результат = "Уважаемый";
			КонецЕсли;
			Возврат Результат;
		Иначе
			Возврат "Уважаемый";
		КонецЕсли;		
	ИначеЕсли Тэг = "%ПОЛУЧАТЕЛЬ%" Тогда
		Если ЗначениеЗаполнено(СтрокаТаблицы.КонтактноеЛицо) Тогда
			Возврат СокрЛП(СтрокаТаблицы.КонтактноеЛицо.Наименование);
		Иначе
			Возврат СокрЛП(СтрокаТаблицы.Партнер.Наименование);
		КонецЕсли;
	ИначеЕсли Тэг = "%ПОЛУЧАТЕЛЬ_ИМЯ%" Тогда
		Возврат СокрЛП(СтрокаТаблицы.Партнер.Наименование);
	ИначеЕсли Тэг = "%ПОЛУЧАТЕЛЬ_ИМЯ_ОТЧЕСТВО%" Тогда
		Возврат СокрЛП(СтрокаТаблицы.Партнер.Наименование);
	ИначеЕсли Тэг = "%ПОЛУЧАТЕЛЬ_ФАМИЛИЯ_И_О%" Тогда
		Возврат СокрЛП(СтрокаТаблицы.Партнер.Наименование);
	ИначеЕсли Тэг = "%УВАЖАЕМОМУ_ПОЛУЧАТЕЛЮ_ФАМИЛИЯ_ИМЯ_ОТЧЕСТВО%" Тогда
		Если ЗначениеЗаполнено(СтрокаТаблицы.КонтактноеЛицо) Тогда
			Обращение = "Уважаемому ";
			Результат = Обращение;
			Возврат Результат;
		ИначеЕсли ТипЗнч(СтрокаТаблицы.Партнер) = Тип("СправочникСсылка.Партнеры") Тогда
			Обращение = "Уважаемому ";
			Результат = Обращение;
			Возврат Результат;
		ИначеЕсли ТипЗнч(СтрокаТаблицы.Партнер) = Тип("СправочникСсылка.ФизическиеЛица") Тогда
			Если СтрокаТаблицы.Партнер.Пол = Перечисления.ПолФизическогоЛица.Женский Тогда
				Обращение = "Уважаемой ";
			ИначеЕсли СтрокаТаблицы.Партнер.Пол = Перечисления.ПолФизическогоЛица.Мужской Тогда
				Обращение = "Уважаемому ";
			КонецЕсли;
			Стр = СокрЛП(СтрокаТаблицы.Партнер.Наименование);
			Результат = Обращение + Стр;
			Возврат Результат;
		Иначе
			Возврат СокрЛП(СтрокаТаблицы.Партнер.Наименование);
		КонецЕсли;
	КонецЕсли;
КонецФункции // ПолучитьЗначениеТэга()

// Функция получает значение тэга СМС
//
// Параметры:
//	Тэг			- Строка			- Тэг, значение которого необходимо получить.
//	Получатель	- СправочникСсылка	- Получатель СМС
//
// Возвращаемое значение:
//	Строка	- Значение тэга
//
Функция ПолучитьЗначениеТэгаСМС(Тэг, Получатель) Экспорт
	// +Медицина для контактного лица реквизиты Пол, Имя, Фамилия и Отчество
	// определяются в физическом лице
	// -Медицина.
	Если НЕ ЗначениеЗаполнено(Получатель) Тогда
		Возврат "";	
	ИначеЕсли Тэг = "%ВРЕМЯ%" Тогда
		Возврат Формат(ТекущаяДатаСеанса(),"ДЛФ=T");
	ИначеЕсли Тэг = "%ДАТА%" Тогда
		Возврат Формат(ТекущаяДатаСеанса(),"ДЛФ=D");
	ИначеЕсли Тэг = "%ДАТА_ВРЕМЯ%" Тогда
		Возврат Формат(ТекущаяДатаСеанса(),"ДЛФ=DT");
	ИначеЕсли Тэг = "%УВАЖАЕМЫЙ/УВАЖАЕМАЯ%" Тогда           
		Если ТипЗнч(Получатель) = Тип("СправочникСсылка.КонтактныеЛицаПартнеров") Тогда
			Если ЗначениеЗаполнено(Получатель.ФизЛицо) И ЗначениеЗаполнено(Получатель.ФизЛицо.Пол)
				И (Получатель.ФизЛицо.Пол = Перечисления.ПолФизическогоЛица.Женский) Тогда
				Возврат "Уважаемая";
			Иначе
				Возврат "Уважаемый";
			КонецЕсли;
		ИначеЕсли ТипЗнч(Получатель) = Тип("СправочникСсылка.ФизическиеЛица") Тогда
			Если ЗначениеЗаполнено(Получатель.Пол)
				И (Получатель.Пол = Перечисления.ПолФизическогоЛица.Женский) Тогда
				Возврат "Уважаемая";
			Иначе
				Возврат "Уважаемый";
			КонецЕсли;
		Иначе
			Возврат "Уважаемый";
		КонецЕсли;			
	ИначеЕсли Тэг = "%ПОЛУЧАТЕЛЬ%" Тогда
		Возврат СокрЛП(Получатель.Наименование);
	ИначеЕсли Тэг = "%ПОЛУЧАТЕЛЬ_ИМЯ%" Тогда
		Если ТипЗнч(Получатель) = Тип("СправочникСсылка.КонтактныеЛицаПартнеров") Тогда
			Если ЗначениеЗаполнено(Получатель.ФизЛицо) Тогда
				Возврат СокрЛП(Получатель.ФизЛицо.Имя);
			Иначе
				Возврат СокрЛП(Получатель.Наименование);
			КонецЕсли;	
		ИначеЕсли ТипЗнч(Получатель) = Тип("СправочникСсылка.ФизическиеЛица") Тогда
			Возврат СокрЛП(Получатель.Имя);
		Иначе
			Возврат СокрЛП(Получатель.Наименование);
		КонецЕсли;	
	ИначеЕсли Тэг = "%ПОЛУЧАТЕЛЬ_ИМЯ_ОТЧЕСТВО%" Тогда
		Если ТипЗнч(Получатель) = Тип("СправочникСсылка.КонтактныеЛицаПартнеров") Тогда
			Если ЗначениеЗаполнено(Получатель.ФизЛицо) Тогда
				Возврат СокрЛП(Получатель.ФизЛицо.Имя) + " " + СокрЛП(Получатель.ФизЛицо.Отчество);
			Иначе
				Возврат СокрЛП(Получатель.Наименование);
			КонецЕсли;	
		ИначеЕсли ТипЗнч(Получатель) = Тип("СправочникСсылка.ФизическиеЛица") Тогда
			Возврат СокрЛП(Получатель.Имя) + " " + СокрЛП(Получатель.Отчество);
		Иначе
			Возврат СокрЛП(Получатель.Наименование);
		КонецЕсли;	
	ИначеЕсли Тэг = "%ПОЛУЧАТЕЛЬ_ФАМИЛИЯ_И_О%" Тогда
		Если ТипЗнч(Получатель) = Тип("СправочникСсылка.КонтактныеЛицаПартнеров") Тогда
			Если ЗначениеЗаполнено(Получатель.ФизЛицо) Тогда
				Возврат  СокрЛП(Получатель.ФизЛицо.Фамилия) + " " + СокрЛП(Получатель.ФизЛицо.Имя) + " " + СокрЛП(Получатель.ФизЛицо.Отчество);
			Иначе
				Возврат СокрЛП(Получатель.Наименование);
			КонецЕсли;	
		ИначеЕсли ТипЗнч(Получатель) = Тип("СправочникСсылка.ФизическиеЛица") Тогда
			Возврат  СокрЛП(Получатель.Фамилия) + " " + СокрЛП(Получатель.Имя) + " " + СокрЛП(Получатель.Отчество);
		Иначе
			Возврат СокрЛП(Получатель.Наименование);
		КонецЕсли;	
	ИначеЕсли Тэг = "%УВАЖАЕМОМУ_ПОЛУЧАТЕЛЮ_ФАМИЛИЯ_ИМЯ_ОТЧЕСТВО%" Тогда
		Если ТипЗнч(Получатель) = Тип("СправочникСсылка.КонтактныеЛицаПартнеров") Тогда
			Если ЗначениеЗаполнено(Получатель.ФизЛицо) Тогда
				Если ЗначениеЗаполнено(Получатель.ФизЛицо.Пол) тогда
					Если Получатель.ФизЛицо.Пол = Перечисления.ПолФизическогоЛица.Женский Тогда
						Обращение = "Уважаемой ";
					Иначе
						Обращение = "Уважаемому ";
					КонецЕсли;
					Стр = СокрЛП(Получатель.ФизЛицо.Фамилия) + " " + СокрЛП(Получатель.ФизЛицо.Имя) + " " + СокрЛП(Получатель.ФизЛицо.Отчество);
					Результат = Обращение + CRM_ОбщегоНазначенияСервер.Склонение(Стр, "Д", Получатель.ФизЛицо.Пол);
					Возврат Результат;
				Иначе
					Результат = СокрЛП(Получатель.ФизЛицо.Фамилия) + " " + СокрЛП(Получатель.ФизЛицо.Имя) + " " + СокрЛП(Получатель.ФизЛицо.Отчество);
					Возврат Результат;
				КонецЕсли;	
			Иначе
				Результат = СокрЛП(Получатель.Наименование);
				Возврат Результат;
			КонецЕсли;
		ИначеЕсли ТипЗнч(Получатель) = Тип("СправочникСсылка.ФизическиеЛица") Тогда
			Если ЗначениеЗаполнено(Получатель.Пол) Тогда
				Если Получатель.Пол = Перечисления.ПолФизическогоЛица.Женский Тогда
					Обращение = "Уважаемой ";
				Иначе
					Обращение = "Уважаемому ";
				КонецЕсли;
				Стр = СокрЛП(Получатель.Фамилия) + " " + СокрЛП(Получатель.Имя) + " " + СокрЛП(Получатель.Отчество);
				Результат = Обращение + CRM_ОбщегоНазначенияСервер.Склонение(Стр, "Д", Получатель.Пол);
				Возврат Результат;
			Иначе
				Результат = СокрЛП(Получатель.Фамилия) + " " + СокрЛП(Получатель.Имя) + " " + СокрЛП(Получатель.Отчество);
				Возврат Результат;
			КонецЕсли;	
		Иначе
			Результат = СокрЛП(Получатель.Наименование);
			Возврат Результат;
		КонецЕсли;	
	КонецЕсли;
КонецФункции // ПолучитьЗначениеТэгаСМС()


#КонецОбласти