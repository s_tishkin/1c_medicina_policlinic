﻿////////////////////////////////////////////////////////////////////////////////
// Ответственные лица: процедуры и функции для работы с ответственным лицами.
//  
////////////////////////////////////////////////////////////////////////////////

#Область ПрограммныйИнтерфейс

// Функция возвращает структуру с реквизитами ответственных лиц.
//
// Параметры:
//  Организация - организация, для которой нужно определить руководящих лиц.
//	ДатаСреза - дата со временем, на которые необходимо определить сведения.
//	Подразделение - подразделение, для которого необходимо определить ответственных лиц.
//
Функция ОтветственныеЛица(Организация, ДатаСреза, Подразделение = Неопределено) Экспорт

	ДанныеОтветственныхЛиц = ОтветственныеЛицаБП.ПустаяСтруктураОтветственныхЛиц();

	Отбор = Новый Структура();
	Отбор.Вставить("Организация", 	Организация);
	Отбор.Вставить("Дата",			ДатаСреза);

	ТаблицаОтветственныхЛиц = ОтветственныеЛицаСервер.ПолучитьТаблицуОтветственныхЛицПоОтбору(Отбор);
	ТаблицаОтветственныхЛиц.Индексы.Добавить("ОтветственноеЛицо");
	
	МетаЗначенияПеречисления = Метаданные.Перечисления.ОтветственныеЛицаОрганизаций.ЗначенияПеречисления;
	
	Для Каждого ЗначениеПеречисления Из МетаЗначенияПеречисления Цикл
		
		ИмяЗначенияПеречисления = ЗначениеПеречисления.Имя;
		Если НЕ ДанныеОтветственныхЛиц.Свойство(ИмяЗначенияПеречисления) Тогда
			Продолжить;
		КонецЕсли;
		
		РольОтветственногоЛица 	= Перечисления.ОтветственныеЛицаОрганизаций[ИмяЗначенияПеречисления];
		
		НайденнаяСтрока = ТаблицаОтветственныхЛиц.Найти(РольОтветственногоЛица, "ОтветственноеЛицо");
		Если НайденнаяСтрока <> Неопределено Тогда
		
			ДанныеОтветственныхЛиц[ИмяЗначенияПеречисления] = НайденнаяСтрока.Сотрудник;
			ДанныеОтветственныхЛиц[ИмяЗначенияПеречисления + "ДолжностьПредставление"] = НайденнаяСтрока.Должность;
		
			СтруктураФИО = ДанныеОтветственныхЛиц[ИмяЗначенияПеречисления + "ФИО"];
			СтруктураФИО.Представление = ФизическиеЛицаКлиентСервер.ФамилияИнициалы(
				СтруктураФИО.Фамилия
				+ СтруктураФИО.Имя
				+ СтруктураФИО.Отчество);
			
			ДанныеОтветственныхЛиц[ИмяЗначенияПеречисления + "Представление"] = СтруктураФИО.Представление;
			
		КонецЕсли;
	
	КонецЦикла;

	Возврат ДанныеОтветственныхЛиц;

КонецФункции

// Функция возвращает массив с датами изменения в ответственных лицах.
//
Функция ДатыИзмененияОтветственныхЛицОрганизаций(Организация) Экспорт

	УстановитьПривилегированныйРежим(Истина);

	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Организация", Организация);
	
	Запрос.Текст = 
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	ОтветственныеЛицаОрганизаций.ДатаНачала КАК Период
	|ИЗ
	|	Справочник.ОтветственныеЛицаОрганизаций КАК ОтветственныеЛицаОрганизаций
	|ГДЕ
	|	ОтветственныеЛицаОрганизаций.Владелец = &Организация
	|
	|УПОРЯДОЧИТЬ ПО
	|	Период";
	
	Возврат Запрос.Выполнить().Выгрузить().ВыгрузитьКолонку("Период");
	
КонецФункции

// Функция возвращает структуру ДанныеУполномоченногоЛица сведения о полномочиях пользователя.
//
// Параметры:
//	Организация 	- СправочникСсылка.Организация
//	Пользователь 	- СправочникСсылка.Пользователи, если не заполнен, то для всех пользователей.
//
Функция ДанныеУполномоченногоЛица(Организация, Пользователь = Неопределено) Экспорт 
	
	Результат = ОтветственныеЛицаБП.ПустаяСтруктураУполномоченногоЛица();

	Отбор = Новый Структура();
	Отбор.Вставить("Организация", 	Организация);
	Отбор.Вставить("Дата", 			ТекущаяДатаСеанса());
		
	МассивРолейОтветственныхЛиц = Новый Массив;
	МассивРолейОтветственныхЛиц.Добавить(Перечисления.ОтветственныеЛицаОрганизаций.Руководитель);
	МассивРолейОтветственныхЛиц.Добавить(Перечисления.ОтветственныеЛицаОрганизаций.ГлавныйБухгалтер);
	Отбор.Вставить("ОтветственноеЛицо", МассивРолейОтветственныхЛиц);

	Если ЗначениеЗаполнено(Пользователь) Тогда
		СотрудникПоУмолчанию_ = РегистрыСведений.СотрудникиПользователя.ПолучитьСотрудникаПользователяПоУмолчанию(
																					Пользователь
																				);
		Отбор.Вставить("Сотрудник", СотрудникПоУмолчанию_);
	КонецЕсли;
		
	ТаблицаОтветственныхЛиц = ОтветственныеЛицаСервер.ПолучитьТаблицуОтветственныхЛицПоОтбору(Отбор);
	
	// Руководитель
	НайденнаяСтрока = ТаблицаОтветственныхЛиц.Найти(
		Перечисления.ОтветственныеЛицаОрганизаций.Руководитель, "ОтветственноеЛицо");
	
	Если НайденнаяСтрока <> Неопределено Тогда
		Результат.Руководитель = НайденнаяСтрока.Сотрудник;
		Если НайденнаяСтрока.ПравоПодписиПоДоверенности Тогда
			Результат.ПриказРуководитель = НайденнаяСтрока.ОснованиеПраваПодписи;
		КонецЕсли;
	КонецЕсли;

	// Главный бухгалтер
	НайденнаяСтрока = ТаблицаОтветственныхЛиц.Найти(
		Перечисления.ОтветственныеЛицаОрганизаций.ГлавныйБухгалтер, "ОтветственноеЛицо");
	
	Если НайденнаяСтрока <> Неопределено Тогда
		Результат.ГлавныйБухгалтер = НайденнаяСтрока.Сотрудник;
		Если НайденнаяСтрока.ПравоПодписиПоДоверенности Тогда
			Результат.ПриказГлавныйБухгалтер = НайденнаяСтрока.ОснованиеПраваПодписи;
		КонецЕсли;
	КонецЕсли;

	Возврат Результат;

КонецФункции

// Возвращает ответственное лицо на складе на указанную дату.
//
Функция ОтветственноеЛицоНаСкладе(Склад, Дата) Экспорт
	
	Если ЗначениеЗаполнено(Склад) Тогда
		Возврат ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Склад, "ТекущийОтветственный");
	Иначе
		Возврат Справочники.ФизическиеЛица.ПустаяСсылка();
	КонецЕсли;
	
КонецФункции

#КонецОбласти
