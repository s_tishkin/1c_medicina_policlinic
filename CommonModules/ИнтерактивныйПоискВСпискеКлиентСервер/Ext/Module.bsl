﻿
#Область ПрограммныйИнтерфейс


// Возвращает префикс элементов подсистемы ИнтерактивныйПоискВСписке
//
// Возвращаемое значение
//	Строка
//
Функция ПрефиксЭлементов() Экспорт
	Возврат "ПоискВСписке";
КонецФункции

// Возвращает ключ настроек формы для сохранения настроек
//
// Параметры
//	Форма - управляемая форма, для которой нужно получить ключ настроек
//
// Возвращаемое значение
//	Строка
//
Функция КлючНастроекФормы(Форма) Экспорт
	Возврат ПрефиксЭлементов() + Форма.ИмяФормы;
КонецФункции


#КонецОбласти