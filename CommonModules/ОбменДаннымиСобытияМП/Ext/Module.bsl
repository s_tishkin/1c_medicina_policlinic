﻿
#Область ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// Обмен Медицина Поликлиника 1.1 и Бухгалтерия предприятия 2.0 

// Процедура-обработчик события "ПередЗаписью" ссылочных типов данных (кроме документов) для механизма 
//   регистрации объектов на узлах.
//
// Параметры:
//  ИмяПланаОбмена – Строка – имя плана обмена, для которого выполняется механизм регистрации.
//  Источник       - источник события, кроме типа ДокументОбъект
//  Отказ          - Булево - флаг отказа от выполнения обработчика
// 
Процедура ОбменДаннымиОбменМедицинаПоликлиникаБухгалтерияПредприятияПередЗаписью(Источник, Отказ) Экспорт
	
	ОбменДаннымиСобытия.МеханизмРегистрацииОбъектовПередЗаписью("ОбменМедицинаПоликлиникаБухгалтерияПредприятия", Источник, Отказ);
	
КонецПроцедуры // ОбменДаннымиОбменМедицинаПоликлиникаБухгалтерияПредприятияПередЗаписью()

// Процедура-обработчик события "ПередЗаписью" документов для механизма регистрации объектов на узлах.
//
// Параметры:
//  ИмяПланаОбмена – Строка – имя плана обмена, для которого выполняется механизм регистрации.
//  Источник       - ДокументОбъект - источник события
//  Отказ          - Булево - флаг отказа от выполнения обработчика
// 
Процедура ОбменДаннымиОбменМедицинаПоликлиникаБухгалтерияПредприятияПередЗаписьюДокумента(Источник, Отказ, РежимЗаписи, РежимПроведения) Экспорт
	
	ОбменДаннымиСобытия.МеханизмРегистрацииОбъектовПередЗаписьюДокумента("ОбменМедицинаПоликлиникаБухгалтерияПредприятия", Источник, Отказ, РежимЗаписи, РежимПроведения);
	
КонецПроцедуры // ОбменДаннымиОбменМедицинаПоликлиникаБухгалтерияПредприятияПередЗаписьюДокумента()

// Процедура-обработчик события "ПередУдалением" ссылочных типов данных для механизма регистрации объектов на узлах.
//
// Параметры:
//  ИмяПланаОбмена – Строка – имя плана обмена, для которого выполняется механизм регистрации.
//  Источник       - источник события
//  Отказ          - Булево - флаг отказа от выполнения обработчика
// 
Процедура ОбменДаннымиОбменОбменМедицинаПоликлиникаБухгалтерияПредприятияПередУдалением(Источник, Отказ) Экспорт
	
	ОбменДаннымиСобытия.МеханизмРегистрацииОбъектовПередУдалением("ОбменМедицинаПоликлиникаБухгалтерияПредприятия", Источник, Отказ);
	
КонецПроцедуры // ОбменДаннымиОбменОбменМедицинаПоликлиникаБухгалтерияПредприятияПередУдалением()

// Процедура-обработчик события "ПередЗаписью" ссылочных типов данных (кроме документов) для механизма регистрации объектов на узлах
//
// Параметры:
//  ИмяПланаОбмена – Строка – имя плана обмена, для которого выполняется механизм регистрации
//  Источник       - источник события, кроме типа ДокументОбъект
//  Отказ          - Булево - флаг отказа от выполнения обработчика
// 
Процедура ОбменДаннымиМПиБухгалтерияПредприятия30ЗарегистрироватьПередЗаписью(Источник, Отказ) Экспорт
	
	ОбменДаннымиСобытия.МеханизмРегистрацииОбъектовПередЗаписью("ОбменМедицинаПоликлиникаБухгалтерияПредприятия30", Источник, Отказ);
	
КонецПроцедуры

// Процедура-обработчик события "ПередЗаписью" документов для механизма регистрации объектов на узлах
//
// Параметры:
//  ИмяПланаОбмена – Строка – имя плана обмена, для которого выполняется механизм регистрации
//  Источник       - ДокументОбъект - источник события
//  Отказ          - Булево - флаг отказа от выполнения обработчика
// 
Процедура ОбменДаннымиМПиБухгалтерияПредприятия30ЗарегистрироватьПередЗаписьюДокумента(Источник, Отказ, РежимЗаписи, РежимПроведения) Экспорт
	
	ОбменДаннымиСобытия.МеханизмРегистрацииОбъектовПередЗаписьюДокумента("ОбменМедицинаПоликлиникаБухгалтерияПредприятия30", Источник, Отказ, РежимЗаписи, РежимПроведения);
	
КонецПроцедуры

// Процедура-обработчик события "ПередУдалением" ссылочных типов данных для механизма регистрации объектов на узлах
//
// Параметры:
//  ИмяПланаОбмена – Строка – имя плана обмена, для которого выполняется механизм регистрации
//  Источник       - источник события
//  Отказ          - Булево - флаг отказа от выполнения обработчика
// 
Процедура ОбменДаннымиМПиБухгалтерияПредприятия30ЗарегистрироватьУдалениеПередУдалением(Источник, Отказ) Экспорт
	
	ОбменДаннымиСобытия.МеханизмРегистрацииОбъектовПередУдалением("ОбменМедицинаПоликлиникаБухгалтерияПредприятия30", Источник, Отказ);
	
КонецПроцедуры

// Процедура-обработчик события "ПередЗаписью" ссылочных типов данных (кроме документов) для механизма регистрации объектов на узлах
//
// Параметры:
//  ИмяПланаОбмена – Строка – имя плана обмена, для которого выполняется механизм регистрации
//  Источник       - источник события, кроме типа ДокументОбъект
//  Отказ          - Булево - флаг отказа от выполнения обработчика
// 
Процедура ОбменДаннымиМПиБухгалтерияПредприятия20ЗарегистрироватьПередЗаписью(Источник, Отказ) Экспорт
	
	Если Не ОбщегоНазначенияПовтИсп.РазделениеВключено() Тогда
		ОбменДаннымиСобытия.МеханизмРегистрацииОбъектовПередЗаписью("ОбменМедицинаПоликлиникаБухгалтерияПредприятия30", Источник, Отказ);
	КонецЕсли;
	
КонецПроцедуры

// Процедура-обработчик события "ПередЗаписью" документов для механизма регистрации объектов на узлах
//
// Параметры:
//  ИмяПланаОбмена – Строка – имя плана обмена, для которого выполняется механизм регистрации
//  Источник       - ДокументОбъект - источник события
//  Отказ          - Булево - флаг отказа от выполнения обработчика
// 
Процедура ОбменДаннымиМПиБухгалтерияПредприятия20ЗарегистрироватьПередЗаписьюДокумента(Источник, Отказ, РежимЗаписи, РежимПроведения) Экспорт
	
	Если Не ОбщегоНазначенияПовтИсп.РазделениеВключено() Тогда
		ОбменДаннымиСобытия.МеханизмРегистрацииОбъектовПередЗаписьюДокумента("ОбменМедицинаПоликлиникаБухгалтерияПредприятия30", Источник, Отказ, РежимЗаписи, РежимПроведения);
	КонецЕсли;
	
КонецПроцедуры

// Процедура-обработчик события "ПередУдалением" ссылочных типов данных для механизма регистрации объектов на узлах
//
// Параметры:
//  ИмяПланаОбмена – Строка – имя плана обмена, для которого выполняется механизм регистрации
//  Источник       - источник события
//  Отказ          - Булево - флаг отказа от выполнения обработчика
// 
Процедура ОбменДаннымиМПиБухгалтерияПредприятия20ЗарегистрироватьУдалениеПередУдалением(Источник, Отказ) Экспорт
	
	Если Не ОбщегоНазначенияПовтИсп.РазделениеВключено() Тогда
		ОбменДаннымиСобытия.МеханизмРегистрацииОбъектовПередУдалением("ОбменМедицинаПоликлиникаБухгалтерияПредприятия30", Источник, Отказ);
	КонецЕсли;

КонецПроцедуры

#КонецОбласти