﻿
#Область ПрограммныйИнтерфейс

////
// Процедура ОбработатьСобытиеЭлементаФормы
// Параметры:
///
Процедура ОбработатьСобытиеЭлементаФормы(Форма, Элемент, Событие, ПараметрыСобытия, НуженВызовСервера) Экспорт
	
	ОбработчикиСобытий = Форма.ОбработчикиСобытийЭлементовФормыКлиент;
	
	СтрокаПараметров = "";
	Для Индекс = 1 По ПараметрыСобытия.Количество() Цикл
		СтрокаПараметров = СтрокаПараметров + "ПараметрыСобытия[" + (Индекс - 1) + "],";
	КонецЦикла;
	СтрокаПараметров = Лев(СтрокаПараметров, СтрДлина(СтрокаПараметров) - 1);
	
	ОтборОбработчиков = Новый Структура("Элемент, Событие", Элемент.Имя, Событие);
	Обработчики = ОбработчикиСобытий.НайтиСтроки(ОтборОбработчиков);
	Для каждого Обработчик Из Обработчики Цикл
		
		Если ПустаяСтрока(Обработчик.Обработчик) Тогда
			НуженВызовСервера = Истина;
			Продолжить;
		КонецЕсли;
		
		КомандаВыполнения =
			Обработчик.Обработчик + "(Форма, Элемент" + ?(ПустаяСтрока(СтрокаПараметров), "", ", " + СтрокаПараметров) + ")";
		Выполнить(КомандаВыполнения);
		
	КонецЦикла;
	
КонецПроцедуры

////
// Процедура ВыполнитьКомандуФормы
// Параметры:
///
Процедура ВыполнитьКомандуФормы(Форма, ИмяКоманды, НуженВызовСервера) Экспорт
	
	ОбработчикиСобытий = Форма.ОбработчикиКомандФормыКлиент;
	
	ОтборОбработчиков = Новый Структура("Команда", ИмяКоманды);
	Обработчики = ОбработчикиСобытий.НайтиСтроки(ОтборОбработчиков);
	Для каждого Обработчик Из Обработчики Цикл
		Если ПустаяСтрока(Обработчик.Обработчик) Тогда
			НуженВызовСервера = Истина;
			Продолжить;
		КонецЕсли;
		КомандаВыполнения = Обработчик.Обработчик + "(Форма, ИмяКоманды)";
		Выполнить(КомандаВыполнения);
	КонецЦикла;
	
КонецПроцедуры


#КонецОбласти