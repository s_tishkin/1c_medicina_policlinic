﻿
#Область ПрограммныйИнтерфейс


// Получает текст операнда для вставки в формулу
//
// Параметры:
// 	Операнд - Строка - имя операнда.
//
// Возвращаемое значение:
// 	Строка
//
Функция ПолучитьТекстОперандаДляВставки(Операнд) Экспорт
	
	Возврат "[" + Операнд + "]";
	
КонецФункции // ПолучитьТекстОперандаДляВставки()

// Осуществляет проверку корректности формулы
//
// Параметры:
// 	Формула - Строка - текст формулы.
//	ДоступныеОперанды - Соответствие - Ключ - операнд, Значение - ОписаниеТипов
// 	ТекстОшибки- Строка - текст сообщения об ошибке
//
// Возвращаемое значение:
// 	Булево - Ложь, если есть ошибки, иначе Истина.
//
Функция ПроверитьФормулу(Формула, Знач Операнды = Неопределено, ТекстОшибки = "") Экспорт
	
	Результат = Истина;
	Если Операнды = Неопределено Тогда
		Операнды = ПолучитьОперандыФормулы(Формула);
	КонецЕсли;
	
	Если ТипЗнч(Операнды) = Тип("Соответствие") Тогда
		ДоступныеОперанды = Операнды;
	ИначеЕсли ТипЗнч(Операнды) = Тип("Массив") Тогда
		
		ДоступныеОперанды = Новый Соответствие;
		Для каждого Операнд Из Операнды Цикл
			ДоступныеОперанды.Вставить(Операнд, Новый ОписаниеТипов("Число"));
		КонецЦикла;
	Иначе
		ВызватьИсключение СообщенияПользователю.Получить("Общие_НекорректныйПараметрОперанды");
	КонецЕсли;
	
	Если ЗначениеЗаполнено(Формула) Тогда
		
		ТекстРасчета = Формула;
		
		Для каждого ОписаниеОперанда Из ДоступныеОперанды Цикл
			Операнд = ОписаниеОперанда.Ключ;
			ТипОперанда = ОписаниеОперанда.Значение;
			Если ТипОперанда = Неопределено Тогда
				ЗначениеЗамены = """Строка1""";
			Иначе
				Если ТипОперанда.СодержитТип(Тип("Число")) Тогда
					ЗначениеЗамены = 1;
				ИначеЕсли ТипОперанда.СодержитТип(Тип("Дата")) Тогда
					ЗначениеЗамены = '0001.01.01';
				Иначе
					ЗначениеЗамены = """Строка1""";
				КонецЕсли;
			КонецЕсли;
			ТекстРасчета = СтрЗаменить(ТекстРасчета, ПолучитьТекстОперандаДляВставки(Операнд), ЗначениеЗамены);
		КонецЦикла;
		
		Попытка
			
			#Если ВебКлиент Тогда
				РезультатРасчета = РаботаСФормулами.ВычислитьФормулу(ТекстРасчета);
			#Иначе
				РезультатРасчета = Вычислить(ТекстРасчета);
			#КонецЕсли
			
			ОтсутствиеРазделителей = Найти(СтрЗаменить(Формула, " ", ""), "][");
			Если ОтсутствиеРазделителей > 0 Тогда
				ТекстОшибки = СообщенияПользователю.Получить("Общие_МеждуОперандамиДолженПрисутствоватьОператорИлиРазделитель");
				Результат = Ложь;
			КонецЕсли;
		
		Исключение
			ТекстОшибки =
				СообщенияПользователю.Получить("Общие_ФормулыДолжныСоставлятьсяПоПравиламВстроенногоЯзыка")
				+ КраткоеПредставлениеОшибки(ИнформацияОбОшибке());
			Результат = Ложь;
		КонецПопытки;
		
	КонецЕсли;
	
	Возврат Результат;
	
КонецФункции // ПроверитьФормулу()

// Извлекает операнды из формулы
//
// Параметры:
// 	Формула - Строка - текст формулы.
//
// Возвращаемое значение:
// 	Массив - Операнды из текстовой формулы.
//
Функция ПолучитьОперандыФормулы(Формула) Экспорт
	
	Операнды = Новый Массив();
	
	ТекстФормулы = СокрЛП(Формула);
	
	ЕстьОперанды = (СтрЧислоВхождений(ТекстФормулы, "[") = СтрЧислоВхождений(ТекстФормулы, "]"));
	Пока ЕстьОперанды Цикл
		НачалоОперанда = Найти(ТекстФормулы, "[");
		КонецОперанда = Найти(ТекстФормулы, "]");
		
		Если НачалоОперанда = 0
		 Или КонецОперанда = 0
		 Или НачалоОперанда > КонецОперанда Тогда
		 
		 	ЕстьОперанды = Ложь;
			Прервать;
			
		КонецЕсли;
		
		ИмяОперанда = Сред(ТекстФормулы, НачалоОперанда + 1, КонецОперанда - НачалоОперанда - 1);
		Операнды.Добавить(ИмяОперанда);
		ТекстФормулы = СтрЗаменить(ТекстФормулы, "[" + ИмяОперанда + "]", "");
		КонецПрошлогоОперанда = КонецОперанда;
		
	КонецЦикла;
	
	Возврат Операнды
	
КонецФункции // ПолучитьОперандыФормулы()

// Подставляет значения операндов в формулу и вычисляет ее
//
// Параметры
//	Формула - Строка
//	ЗначенияОперандов - Соответствие
//
// Возвращаемое значение
//	Произвольный - результат вычисления формулы
//
Функция ВычислитьФормулу(Формула, ЗначенияОперандов) Экспорт
	
	ТекстРасчета = Формула;
	
	Индекс = 0;
	МассивОперандов = Новый Массив;
	
	Операнды = ПолучитьОперандыФормулы(Формула);
	Для каждого Операнд Из Операнды Цикл
		ТекстРасчета = СтрЗаменить(ТекстРасчета, ПолучитьТекстОперандаДляВставки(Операнд), "МассивОперандов[" + Индекс + "]");
		МассивОперандов.Добавить(ЗначенияОперандов[Операнд]);
		Индекс = Индекс + 1;
	КонецЦикла;
	
	#Если ВебКлиент Тогда
		РезультатРасчета = РаботаСФормулами.ВычислитьФормулу(ТекстРасчета);
	#ИначеЕсли ТонкийКлиент Тогда
		РезультатРасчета = Вычислить(ТекстРасчета);
	#Иначе
		УстановитьБезопасныйРежим(Истина);
		РезультатРасчета = Вычислить(ТекстРасчета);
	#КонецЕсли
	
	Возврат РезультатРасчета;
	
КонецФункции


#КонецОбласти