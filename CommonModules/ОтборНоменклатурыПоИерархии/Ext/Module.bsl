﻿
#Область ПрограммныйИнтерфейс


// Возвращает пользовательское представление варианта фильтра в списке выбора
//
// Возвращаемое значение
//	Строка
//
Функция ПредставлениеФильтра() Экспорт
	Возврат "иерархии";
КонецФункции

////////////////////////////////////////////////////////////////////////////////
// ИНИЦИАЛИЗАЦИЯ ФОРМЫ

// Создает нуобходимые реквизиты формы для функционирования фильтра
//
// Параметры
//	НовыеРеквизиты - Массив - массив, куда добавляются созданные реквизиты
//
Процедура ДобавитьРеквизитыФормы(НовыеРеквизиты) Экспорт
	
	ПрефиксЭлементов = ОтборНоменклатурыПоИерархииКлиентСервер.ПрефиксЭлементов();
	
	НовыеРеквизиты.Добавить(Новый РеквизитФормы(
		ПрефиксЭлементов + "ИерархияНоменклатуры",
		Новый ОписаниеТипов("ДинамическийСписок")));
	
КонецПроцедуры

// Инициализирует реквизиты формы значениями по умолчанию
//
// Параметры
//	Форма
//
Процедура ИнициализироватьРеквизитыФормы(Форма) Экспорт
	
	ПрефиксЭлементов = ОтборНоменклатурыПоИерархииКлиентСервер.ПрефиксЭлементов();
	
	ИерархияНоменклатуры = Форма[ПрефиксЭлементов + "ИерархияНоменклатуры"];
	ИерархияНоменклатуры.ОсновнаяТаблица = "Справочник.Номенклатура";
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбора(
		ИерархияНоменклатуры.Отбор,
		"Ссылка.ЭтоГруппа",
		Истина);
	
КонецПроцедуры

// Создает команды формы, необходимые для работы фильтра
//
// Параметры
//	Форма
//
Процедура СоздатьКомандыФормы(Форма) Экспорт
	
	ОписаниеКомандФормы = РаботаСУправляемойФормой.ПрочитатьОписаниеЭлементовФормыИзСтроки(
		ТекстовоеОписаниеКомандФормы());
	РаботаСУправляемойФормой.СоздатьКомандыФормы(
		Форма,
		ОписаниеКомандФормы,
		ОтборНоменклатурыПоИерархииКлиентСервер.ПрефиксЭлементов());
	
КонецПроцедуры

Функция ТекстовоеОписаниеКомандФормы()
	
	ОписаниеКоманд =
	"
	|<Команды>
	|	<Команда
	|		Имя='СоздатьГруппу'
	|		Заголовок='Создать'
	|		Действие='Подключаемый_ВыполнитьКомандуФормы'
	|		ОбработчикиКоманды='Клиент:ОтборНоменклатурыПоИерархииКлиент.СоздатьГруппуНоменклатуры'
	|		Картинка='СоздатьГруппу'/>
	|	<Команда
	|		Имя='Изменить'
	|		Заголовок='Изменить'
	|		Действие='Подключаемый_ВыполнитьКомандуФормы'
	|		ОбработчикиКоманды='Клиент:ОтборНоменклатурыПоИерархииКлиент.ИзменитьГруппуНоменклатуры'
	|		Картинка='Изменить'/>
	|	<Команда
	|		Имя='Скопировать'
	|		Заголовок='Скопировать'
	|		Действие='Подключаемый_ВыполнитьКомандуФормы'
	|		ОбработчикиКоманды='Клиент:ОтборНоменклатурыПоИерархииКлиент.СкопироватьГруппуНоменклатуры'
	|		Картинка='СкопироватьЭлементСписка'/>
	|	<Команда
	|		Имя='УстановитьПометкуУдаления'
	|		Заголовок='Установить пометку удаления'
	|		Действие='Подключаемый_ВыполнитьКомандуФормы'
	|		ОбработчикиКоманды='Клиент:ОтборНоменклатурыПоИерархииКлиент.УстановитьПометкуУдаленияГруппыНоменклатуры'
	|		Картинка='ПометитьНаУдаление'/>
	|	<Команда
	|		Имя='ИзменитьВыделенныеГруппы'
	|		Заголовок='Изменить выделенные'
	|		Действие='Подключаемый_ВыполнитьКомандуФормы'
	|		ОбработчикиКоманды='Клиент:ОтборНоменклатурыПоИерархииКлиент.ИзменитьВыделенныеГруппы'/>
	|
	|</Команды>
	|";
	
	Возврат ОписаниеКоманд;
	
КонецФункции

// Создает элементы формы, необходимые для работы фильтра
//
// Параметры
//	Форма
//	ГруппаДляРазмещения - элемент формы, где нужно разместить элементы
//
Процедура СоздатьЭлементыФормы(Форма, ГруппаДляРазмещения) Экспорт
	
	Элементы = Форма.Элементы;
	ПрефиксЭлементов = ОтборНоменклатурыПоИерархииКлиентСервер.ПрефиксЭлементов();
	
	ОписаниеЭлементовФормы = РаботаСУправляемойФормой.ПрочитатьОписаниеЭлементовФормыИзСтроки(
		ТекстовоеОписаниеЭлементовФормы());
	РаботаСУправляемойФормой.СоздатьЭлементыФормы(
		Форма,
		ОписаниеЭлементовФормы,
		ПрефиксЭлементов,
		ГруппаДляРазмещения);
		
	ДоступноРедактированиеНоменклатуры = Форма.Параметры.Свойство("ДоступноРедактированиеНоменклатуры");
	ЕстьПравоРедактирования = ПравоДоступа("Редактирование", Метаданные.Справочники.Номенклатура);
	ЕстьПравоСоздания = ПравоДоступа("Добавление", Метаданные.Справочники.Номенклатура);
	
	МассивЭлементов = Новый Массив;
	МассивЭлементов.Добавить(ПрефиксЭлементов + "Изменить");
	МассивЭлементов.Добавить(ПрефиксЭлементов + "УстановитьПометкуУдаления");
	МассивЭлементов.Добавить(ПрефиксЭлементов + "ИзменитьВыделенныеГруппы");
	ОбщегоНазначенияНСИКлиентСервер.УстановитьСвойствоЭлементовФормы(
		Элементы,
		МассивЭлементов,
		"Видимость",
		ДоступноРедактированиеНоменклатуры И ЕстьПравоРедактирования);
	
	МассивЭлементов = Новый Массив;
	МассивЭлементов.Добавить(ПрефиксЭлементов + "СоздатьГруппу");
	МассивЭлементов.Добавить(ПрефиксЭлементов + "Скопировать");
	ОбщегоНазначенияНСИКлиентСервер.УстановитьСвойствоЭлементовФормы(
		Элементы,
		МассивЭлементов,
		"Видимость",
		ДоступноРедактированиеНоменклатуры И ЕстьПравоСоздания);
		
	ОбщегоНазначенияНСИКлиентСервер.УстановитьСвойствоЭлементаФормы(
		Элементы,
		ПрефиксЭлементов + "КоманднаяПанельИерархияНоменклатуры",
		"Видимость",
		ДоступноРедактированиеНоменклатуры);
		
	ОбщегоНазначенияНСИКлиентСервер.УстановитьСвойствоЭлементаФормы(
		Элементы,
		ПрефиксЭлементов + "ИерархияНоменклатуры",
		"ИзменятьСоставСтрок",
		ДоступноРедактированиеНоменклатуры);
		
	ОбщегоНазначенияНСИКлиентСервер.УстановитьСвойствоЭлементаФормы(
		Элементы,
		ПрефиксЭлементов + "ИерархияНоменклатуры",
		"ИзменятьПорядокСтрок",
		ДоступноРедактированиеНоменклатуры);
		
КонецПроцедуры
	
Функция ТекстовоеОписаниеЭлементовФормы()
	
	ОписаниеЭлементов =
	"<Элементы>
	|	<ГруппаФормы
	|			Имя='ФильтрИерархияНоменклатуры'
	|			Вид='ОбычнаяГруппа'
	|			ОтображатьЗаголовок='Ложь'
	|			Отображение='Нет'>
	|		<ГруппаФормы
	|				Имя='КоманднаяПанельИерархияНоменклатуры'
	|				Вид='КоманднаяПанель'>
	|			<КнопкаФормы
	|					Имя='СоздатьГруппу'
	|					Вид='КнопкаКоманднойПанели'
	|					ИмяКоманды='%Префикс%СоздатьГруппу'
	|					Отображение='КартинкаИТекст'/>
	|			<КнопкаФормы
	|					Имя='Изменить'
	|					Вид='КнопкаКоманднойПанели'
	|					ИмяКоманды='%Префикс%Изменить'/>
	|			<КнопкаФормы
	|					Имя='Скопировать'
	|					Вид='КнопкаКоманднойПанели'
	|					ИмяКоманды='%Префикс%Скопировать'/>
	|			<КнопкаФормы
	|					Имя='УстановитьПометкуУдаления'
	|					Вид='КнопкаКоманднойПанели'
	|					ИмяКоманды='%Префикс%УстановитьПометкуУдаления'/>
	|
	|		</ГруппаФормы>
	|		<ТаблицаФормы
	|				Имя='ИерархияНоменклатуры'
	|				ПутьКДанным='%Префикс%ИерархияНоменклатуры'
	|				Обработчики='ПриАктивизацииСтроки:Подключаемый_ТаблицаФормыПриАктивизацииСтроки,
	|								Клиент:ОтборНоменклатурыПоИерархииКлиент.ПриАктивизацииСтрокиИерархияНоменклатуры'
	|				ПоложениеКоманднойПанели='Нет'
	|				Отображение='Дерево'
	|				НачальноеОтображениеДерева='РаскрыватьВерхнийУровень'
	|				Шапка='Ложь'
	|				ЧередованиеЦветовСтрок='Ложь'
	|				ВосстанавливатьТекущуюСтроку='Истина'>
	|			<КонтекстноеМеню>
	|				<КнопкаФормы
	|					Имя='ИзменитьВыделенныеГруппы'
	|					Вид='КнопкаКоманднойПанели'
	|					ИмяКоманды='%Префикс%ИзменитьВыделенныеГруппы'/>
	|			</КонтекстноеМеню>
	|			<ПолеФормы
	|					Имя='ИерархияНоменклатурыСсылка'
	|					Вид='ПолеНадписи'
	|					ПутьКДанным='%Префикс%ИерархияНоменклатуры.Ссылка'/>
	|		
	|		</ТаблицаФормы>
	|
	|	</ГруппаФормы>
	|
	|</Элементы>
	|";
	
	ПрефиксЭлементов = ОтборНоменклатурыПоИерархииКлиентСервер.ПрефиксЭлементов();
	Возврат СтрЗаменить(ОписаниеЭлементов, "%Префикс%", ПрефиксЭлементов);
	
КонецФункции

// Задает условное оформление формы, необходимые для работы фильтра
//
// Параметры
//	Форма
//
Процедура ЗадатьУсловноеОформлениеФормы(Форма) Экспорт
	
	ПрефиксЭлементов = ОтборНоменклатурыПоИерархииКлиентСервер.ПрефиксЭлементов();
	
	УсловноеОформление = Форма.УсловноеОформление.Элементы;
	Элемент = УсловноеОформление.Добавить();
	Отбор = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	Отбор.ЛевоеЗначение = Новый ПолеКомпоновкиДанных(ПрефиксЭлементов + "ИспользоватьФильтры");
	Отбор.ПравоеЗначение = Ложь;
	Отбор.Использование = Истина;
	Оформление = Элемент.Оформление;
	
	ДанныеОформления_ = ОформленияВидовОбъектов.Получить("МедицинскаяНСИ_НеиспользуемыеФильтры");
	ОформленияВидовОбъектов.СкопироватьОформление(Оформление, ДанныеОформления_.Оформление);
	
	Поля = Элемент.Поля.Элементы;
	Поле = Поля.Добавить();
	Поле.Поле = Новый ПолеКомпоновкиДанных(ПрефиксЭлементов + "ИерархияНоменклатуры");
	Поле.Использование = Истина;
	
КонецПроцедуры

// Устанавливает значения настроек фильтра из сохраненных данных, если нет сохраненных
// данных, то устанавливает значения по умолчанию.
//
// Параметры
//	Форма
//	Настройки - структура настроек фильтров
//
Процедура УстановитьЗначенияНастроек(Форма, Настройки) Экспорт
	Возврат;
КонецПроцедуры


#КонецОбласти