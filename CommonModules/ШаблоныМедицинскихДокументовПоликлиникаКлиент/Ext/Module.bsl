﻿
#Область ПрограммныйИнтерфейс

////
 // Функция: ПолучитьТелоМД
 //   Возвращает тело медицинского документа с сгенерированной для него шапкой.
 //
 // Параметры:
 //   МД (ДокументСсылка.МедицинскийДокумент)
 //     Ссылка на медицинский документ.
 //   ШМД (СправочникСсылка.ШаблоныМедицинскихДокументов)
 //     Ссылка на ШМД, используемый для создания МД
 //   СоздаватьШапку (Булево)
 //     Истина -- к телу документа добавляется шапка с данными из ИБ

 //
 // Возврат:
 //   Тело медицинского документа с шапкой или без нее.
 ///
Функция ПолучитьТелоМД(МД, ШМД = Неопределено, СоздаватьШапку = Истина) Экспорт
	ТелоМД = ШаблоныМедицинскихДокументовПоликлиника.ПолучитьТелоМедицинскогоДокумента(МД);
	// Если тело медицинского документа не заполнено, то возвращаем неопределено.
	Если ТелоМД = Неопределено Тогда
		Возврат Неопределено;
	КонецЕсли;
	Если Не СоздаватьШапку Тогда 
		Возврат ТелоМД;
	КонецЕсли;
	
	Если ШМД = Неопределено Тогда
		Возврат ШаблоныМедицинскихДокументовПоликлиника.СоздатьШапкуДокументу(ТелоМД, МД);
	Иначе
		ШМД2 = ШаблоныМедицинскихДокументовПоликлиника.ПолучитьШМДуМД(МД);
		Если Не ШМД2.Пустая() И ШМД2 = ШМД Тогда
			Возврат ШаблоныМедицинскихДокументовПоликлиника.СоздатьШапкуДокументу(ТелоМД, МД);
		Иначе
			Возврат Неопределено;
		КонецЕсли;
	КонецЕсли;
	
КонецФункции

//// Отображает список выбора ШМД
///
/// Параметры:
///  МедицинскийДокумент									 - Документы.МедицинскийДокумент.Ссылка	 - Медицинский документ, для которого требуется отобразить выбор ШМД
///  ОписаниеОповещенияОбработкиВыбораШМД - ОписаниеОповещения	 - Описание оповещение, которое слдеует запустить после выбора
 ///
Процедура ОтобразитьСписокВыбораШМД( // Экспорт
		МедицинскийДокумент, 
		ОписаниеОповещенияОбработкиВыбораШМД, 
		ШаблоныМедицинскихДокументов = Неопределено
	) Экспорт

	ПараметрыФормыВыбора_ = Новый Структура();
	МассивШМД_ = Новый Массив();
	
	Если ШаблоныМедицинскихДокументов <> Неопределено Тогда
		МассивШМД_ = ШаблоныМедицинскихДокументов;
	Иначе
		Если ЗначениеЗаполнено(МедицинскийДокумент) Тогда
			МассивШМД_ = ШаблоныМедицинскихДокументовПоликлиника.МассивВозможныхСсылокШМДДляМД(МедицинскийДокумент);
		Иначе
			МассивШМД_ = ШаблоныМедицинскихДокументовПоликлиника.МассивВозможныхСсылокШМД();
		КонецЕсли;
	КонецЕсли;

	СписокШМД_ = Новый СписокЗначений();
	СписокШМД_.ЗагрузитьЗначения(МассивШМД_);
	ПараметрыФормыВыбора_.Вставить("СписокШМД", СписокШМД_);

	ОткрытьФорму(
		"Обработка.ПоликлиникаШМД.Форма.ФормаВыбораШМД",
		ПараметрыФормыВыбора_,
		,,,,
		ОписаниеОповещенияОбработкиВыбораШМД
	);
КонецПроцедуры

// Функция - ПолучитьДанныеОСостоянииЗдоровьяПациента
//   Функция предназначена для получения содержимого медицинских документов, 
//    хранящихся в информационной базе в виде XML. 
//
// Параметры:
//    Форма	(УправляемаяФорма) 		
 //   Пути (Массив)
 //     Массив строк, в котором указаны XPath или XDTO адреса необходимых данных 
 //     в медицинских документах. Если параметр не указан, то массив строк будет пустым 
 //     и будет возвращена только заголовочная информация документа. 
 //   Первые (Число)
 //     Указывает количество выбираемых документов (первые N). Если параметр 
 //     не указан, то возвращаются все документы (с ограничением максимального числа; 
 //     см. возвращаемое значение). 
 //   Где (Строка)
 //     Часть запроса после ключевого слова ГДЕ, синтаксиса языка запросов 1С. 
 //   ДатаМин (Дата)
 //     Минимальная дата создания запрашиваемых документов. Если параметр не указан, 
 //     то нижней границы в дате создания документов не устанавливается. 
 //   ДатаМакс (Дата)
 //     Максимальная дата создания запрашиваемых документов. Если параметр не указан, 
 //     то верхней границы в дате создания документов не устанавливается. 
 //   ПоВозр (Булево)
 //     Порядок вывода по дате создания, в котором будут возвращены документы. 
 //     Если значение - Истина (true), то вначале будут идти старые документы, 
 //     иначе - новые. Если параметр не указан, то вначале идут старые документы. 
 //
 // Возвращаемое значение:
 //    Массив объектов следующей структуры:
 //      id - Уникальный идентификатор документа «Медицинский документ»
 //      title - Заголовок документа
 //      effectiveTime - Время создания документа. Точность значения времени должна быть минимум до дня
 //      assignedAuthor - Врач, оформивший медицинский документ
 //      assignedAuthorPrefix - Дополнительная информация о враче, оформившего медицинский документ 
 //        (специальность, должность, ученая степень, звание)
 //      representedOrganization - Медицинская организация, в которой работает автор документа. 
 //        Атрибут не передается для документов нашей организации.
 //      ComponentOf - Номер медицинской карты, в рамках которой сформирован медицинский документ,
 //      body - Массив пар строка-массив строк где ключ - идентификатор запрошенных данных 
 //        (например, XPath-выражение), в соответствие которому стоит массив строк, которые удовлетворяют 
 //        этому выражению. 
Функция ПолучитьДанныеОСостоянииЗдоровьяПациента(
	Форма,
	Пути = Неопределено, Первые = Неопределено, Где = "", 
	ДатаМин = Неопределено, ДатаМакс = Неопределено, ПоВозр = Истина) Экспорт
	Если ТипЗнч(Форма) = Тип("УправляемаяФорма") Тогда
		Возврат ШаблоныМедицинскихДокументовПоликлиника.ПолучитьДанныеОСостоянииЗдоровьяПациента(
			Форма.ШМД.МедицинскаяКартаИлиПациент,
			Пути, Первые, Где, ДатаМин, ДатаМакс, ПоВозр
		);
	Иначе
		ВызватьИсключение "Неправильный тип в ПолучитьДанныеОСостоянииЗдоровьяПациента";
	КонецЕсли;	
КонецФункции

// Функция - ПолучитьДанныеОСостоянииЗдоровьяПациентаJSON
//   Функция предназначена для получения содержимого медицинских документов, 
//    хранящихся в информационной базе в виде JSON. 
//
// Параметры:
//    Форма	(УправляемаяФорма) 		
 //   Пути (Массив)
 //     Массив строк, в котором указаны XPath или XDTO адреса необходимых данных 
 //     в медицинских документах. Если параметр не указан, то массив строк будет пустым 
 //     и будет возвращена только заголовочная информация документа. 
 //   Первые (Число)
 //     Указывает количество выбираемых документов (первые N). Если параметр 
 //     не указан, то возвращаются все документы (с ограничением максимального числа; 
 //     см. возвращаемое значение). 
 //   Где (Строка)
 //     Часть запроса после ключевого слова ГДЕ, синтаксиса языка запросов 1С. 
 //   ДатаМин (Дата)
 //     Минимальная дата создания запрашиваемых документов. Если параметр не указан, 
 //     то нижней границы в дате создания документов не устанавливается. 
 //   ДатаМакс (Дата)
 //     Максимальная дата создания запрашиваемых документов. Если параметр не указан, 
 //     то верхней границы в дате создания документов не устанавливается. 
 //   ПоВозр (Булево)
 //     Порядок вывода по дате создания, в котором будут возвращены документы. 
 //     Если значение - Истина (true), то вначале будут идти старые документы, 
 //     иначе - новые. Если параметр не указан, то вначале идут старые документы. 
 //
 // Возвращаемое значение:
 //    Строка c JSON-представлением данных о состоянии здоровья пациента. 
Функция ПолучитьДанныеОСостоянииЗдоровьяПациентаJSON(
	Форма,
	Пути = Неопределено, Первые = Неопределено, Где = "", 
	ДатаМин = Неопределено, ДатаМакс = Неопределено, ПоВозр = Истина) Экспорт
	Если ТипЗнч(Форма) = Тип("УправляемаяФорма") Тогда
		Возврат ШаблоныМедицинскихДокументовПоликлиника.ПолучитьДанныеОСостоянииЗдоровьяПациентаJSON(
			Форма.ШМД.МедицинскаяКартаИлиПациент,
			Пути, Первые, Где, ДатаМин, ДатаМакс, ПоВозр
		);
	Иначе
		ВызватьИсключение "Неправильный тип в ПолучитьДанныеОСостоянииЗдоровьяПациента";
	КонецЕсли;
КонецФункции

////
 // Процедура: ПослеПечатиМедицинскогоДокумента
 //    Предназначина для возможности переопределения
 //   дополнительных действий после печати.
 //
 // Параметры:
 //   СписокДокументов Тип СписокЗначений или ДокументСсылка.МедицинскийДокумент.
 //     Список документов для которых необходимо выполнить печать.
 //   Данные Тип структура
 //    Пользовательские данные
 ///
Процедура ПослеПечатиМедицинскогоДокумента(СписокДокументов, Данные) Экспорт
	
	ШаблоныМедицинскихДокументовПоликлиника.ПослеПечатиМедицинскогоДокумента(СписокДокументов, Данные);
	
КонецПроцедуры

// Открыть просмотр медицинского документа.
//
// Параметры:
//  МедицинскийДокумент - ДокументСсылка.МедицинскийДокумент - медицинский документ.
//  Форма - УправляемаяФорма - форма из которой открывается просмотр.
//
Процедура ОткрытьПросмотрМедицинскогоДокумента(МедицинскийДокумент, Форма) Экспорт
	Значения_ = 
		ШаблоныМедицинскихДокументовПоликлиника.ПолучитьЗначенияРеквизитовМедицинскогоДокумента(
			МедицинскийДокумент, 
			"ФайлМД, ТелоМедицинскогоДокумента, ШаблонМедицинскогоДокумента, Визуализатор"
		);	
	Если ЗначениеЗаполнено(Значения_.ФайлМД) Тогда
		ДанныеФайла_ = РаботаСФайламиСлужебныйВызовСервера.ДанныеФайлаДляОткрытия(
			Значения_.ФайлМД, Форма.УникальныйИдентификатор
		);
		РаботаСФайламиКлиент.Открыть(ДанныеФайла_);
	Иначе
		Если Значения_.ТелоМедицинскогоДокумента <> Неопределено Тогда
			ШаблоныМедицинскихДокументовКлиент.ПоказатьМД(
				Значения_.Визуализатор, Значения_.ТелоМедицинскогоДокумента, МедицинскийДокумент,
				Форма,,, Значения_.ШаблонМедицинскогоДокумента
			);
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

#КонецОбласти