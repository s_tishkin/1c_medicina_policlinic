﻿
#Область ПрограммныйИнтерфейс


// Функция получает файлы из указанного каталога и помещает во временное хранилище
//
// Параметры
//	КаталогЗагрузки - путь к каталогу загрузки
//	ИменаФайлов - массив, содержащий имена файлов для загрузки
//	УдалятьФайлыПослеПолучения - удалять файлы после помещения во временное хранилище.
//	УникальныйИдентификатор - уникальный идентификатор формы
//	МаксимальноеКоличествоФайлов.
//
// Возвращаемое значение
//	Массив - массив, содержащий описание файлов, помещенных во временное хранилище
//
Функция ПолучитьФайлыИзКаталога(КаталогЗагрузки,
								МаскаИмениФайла = "*",
								УдалятьФайлыПослеПолучения = Ложь,
								УникальныйИдентификатор = Неопределено,
								МаксимальноеКоличествоФайлов = 0) Экспорт
	
	
	Если ТипЗнч(МаскаИмениФайла) = Тип("Массив") Тогда
		ИменаФайлов = МаскаИмениФайла;
	Иначе
		ИменаФайлов = Новый Массив;
		ИменаФайлов.Добавить(МаскаИмениФайла);
	КонецЕсли;
	
	ПомещаемыеФайлы = Новый Массив;
	
	Для каждого ИмяФайла Из ИменаФайлов Цикл
		
		НайденныеФайлы = НайтиФайлы(КаталогЗагрузки, ИмяФайла);
		Для каждого Файл Из НайденныеФайлы Цикл
			
			Если Файл.ЭтоФайл() Тогда
				ПомещаемыеФайлы.Добавить(Новый ОписаниеПередаваемогоФайла(Файл.ПолноеИмя));
			КонецЕсли;
			
			Если МаксимальноеКоличествоФайлов И ПомещаемыеФайлы.Количество() = МаксимальноеКоличествоФайлов Тогда
				Прервать;
			КонецЕсли;
			
		КонецЦикла;
			
		Если МаксимальноеКоличествоФайлов И ПомещаемыеФайлы.Количество() = МаксимальноеКоличествоФайлов Тогда
			Прервать;
		КонецЕсли;
		
	КонецЦикла;
	
	ПомещенныеФайлы = Новый Массив;
	Если ПомещаемыеФайлы.Количество() Тогда
		
		Если УникальныйИдентификатор = Неопределено Тогда
			ПоместитьФайлы(ПомещаемыеФайлы, ПомещенныеФайлы,, Ложь);
		Иначе
			ПоместитьФайлы(ПомещаемыеФайлы, ПомещенныеФайлы,, Ложь, УникальныйИдентификатор);
		КонецЕсли;
	
		Если УдалятьФайлыПослеПолучения Тогда
			Для каждого Файл Из ПомещенныеФайлы Цикл
				УдалитьФайлы(Файл.Имя);
			КонецЦикла;
		КонецЕсли;
		
	КонецЕсли;
	
	Возврат ПомещенныеФайлы;
	
КонецФункции


#КонецОбласти