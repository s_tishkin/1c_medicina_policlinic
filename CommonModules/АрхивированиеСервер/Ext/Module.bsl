﻿
#Область ПрограммныйИнтерфейс

////
 // Процедура: Установить
 //   запускается при создании формы списка на сервере.
 //
 // Параметры:
 //   МассивОбъектов - Массив
 //     массив объектов, у которых надо поменять реквизит.
 //   ЗначениеРеквизита - Булево
 //     устанавливаемое значение реквизита.
///
Функция Установить(МассивОбъектов, Знач ЗначениеРеквизита) Экспорт
	
	МассивИзмененных = Новый Массив;
	ИмяРеквизита = Архивирование.ИмяРеквизита();
	Для каждого ОбъектУстановки Из МассивОбъектов Цикл
		// работает только для элементов
		Если ОбъектУстановки.ЭтоГруппа = Ложь Тогда
			Попытка
				// для формы списка ставим архивность несмотря на ошибки
				Если ОбъектУстановки[ИмяРеквизита] <> ЗначениеРеквизита Тогда
					ОбъектЗаписи = ОбъектУстановки.ПолучитьОбъект();
					ОбъектЗаписи[ИмяРеквизита] = ЗначениеРеквизита;
					ОбъектЗаписи.Записать();
					МассивИзмененных.Добавить(ОбъектУстановки);
				КонецЕсли;
			Исключение
				// в модуле объекта при отказе выдастся предупреждение
			КонецПопытки;
		КонецЕсли;
	КонецЦикла;
	
	Возврат МассивИзмененных;
	
КонецФункции

////
 // Процедура: ФормаСпискаПриСозданииНаСервер
 //   запускается при создании формы списка на сервере.
 //
 // Параметры:
 //   Форма - {Управляемая форма}
 //     форма, для которой сработало событие ПриСозданииНаСервере.
 //   ИмяСписка - {Строка}
 //     имя списка, для которого будет настраиваться отбор.
 //   ИмяКоманднойПанели - {Строка}
 //     имя командной панели, в которую будут помещены команды.
  ///
Процедура ФормаСпискаПриСозданииНаСервере(Форма, Знач ИмяСписка, Знач ИмяКоманднойПанели = "") Экспорт
	
	ИмяРеквизита_ = Архивирование.ИмяРеквизита();
	
	Если ОбщегоНазначения.ПодсистемаСуществует("ФункциональныеПодсистемы.ОформленияВидовОбъектов") Тогда
		МодульОформлений = ОбщегоНазначения.ОбщийМодуль("ОформленияВидовОбъектов");
		// создаем оформление списка формы
		МодульОформлений.НастроитьУсловноеОформлениеСписка(
												Форма,
												ИмяСписка,
												ИмяРеквизита_,
												"", // все поля
												"АрхивированиеСписок"
									);
	Иначе
		ЭлементУсловногоОформления = Форма[ИмяСписка].УсловноеОформление.Элементы.Добавить();
		ЭлементУсловногоОформления.РежимОтображения = РежимОтображенияЭлементаНастройкиКомпоновкиДанных.Недоступный;
		// отбор
		ЭлементОтбора = ЭлементУсловногоОформления.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
		ЭлементОтбора.ЛевоеЗначение =  Новый ПолеКомпоновкиДанных(ИмяРеквизита_);
		ЭлементОтбора.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
		ЭлементОтбора.ПравоеЗначение = Архивирование.ЗначениеНедоступности();
		ЭлементОтбора.Использование = Истина;
		// оформление
		ЭлементЦветаОформления = ЭлементУсловногоОформления.Оформление.Элементы.Найти("ЦветТекста");
		ЭлементЦветаОформления.Значение = Новый Цвет(188, 143, 143); 
		ЭлементЦветаОформления.Использование = Истина;
	КонецЕсли;
	
	// создаем нужные команды
	Если ОбщегоНазначения.ПодсистемаСуществует("ФункциональныеПодсистемы.СообщенияПользователю") Тогда
		МодульСообщенияПользователю = ОбщегоНазначения.ОбщийМодуль("СообщенияПользователю");
		СообщениеЗаголовокКоманды_ = МодульСообщенияПользователю.Получить("Архивирование_ЗаголовокКомандыОтбораВСписке",, Ложь);
	Иначе
		СообщениеЗаголовокКоманды_ = Новый Структура("Заголовок, Текст",
											НСтр("ru='Показывать архив'"),
											НСтр("ru='Показывать элементы, находящиеся в архиве'")
										);
	КонецЕсли;
	
	КомандаПросмотраАрхивности = Форма.Команды.Добавить("ПросмотрАрхивныхЭлементов_" + ИмяСписка);
	КомандаПросмотраАрхивности.Действие = "Подключаемый_ПросмотрАрхивныхЭлементов";
	КомандаПросмотраАрхивности.Заголовок = СообщениеЗаголовокКоманды_.Заголовок;
	КомандаПросмотраАрхивности.Картинка = БиблиотекаКартинок.НайтиВСодержании;
	КомандаПросмотраАрхивности.Отображение = ОтображениеКнопки.Картинка;
	КомандаПросмотраАрхивности.Подсказка = СообщениеЗаголовокКоманды_.Текст;
	
	// создаем кнопки
	ИмяКомПанели = ?(ПустаяСтрока(ИмяКоманднойПанели), "ФормаКоманднаяПанель", ИмяКоманднойПанели);
	КомПанельСписка = Форма.Элементы.Найти(ИмяКомПанели);
	КнопкаПросмотрАрхивности = Форма.Элементы.Добавить(КомандаПросмотраАрхивности.Имя, Тип("КнопкаФормы"), КомПанельСписка);
	КнопкаПросмотрАрхивности.ИмяКоманды = КомандаПросмотраАрхивности.Имя;
	// восстанавливаем пометку
	ХранилищеНастроекПометка_ = ОбщегоНазначения.ХранилищеОбщихНастроекЗагрузить(
															Форма.ИмяФормы + "/ПросмотрАрхивныхЭлементов",
															ИмяСписка,
															Ложь
														);
	КнопкаПросмотрАрхивности.Пометка = ХранилищеНастроекПометка_;
	// делаем первоначальный отбор
	Архивирование.УстановитьОтборСпискаПоКоманде(Форма[ИмяСписка], КнопкаПросмотрАрхивности.Пометка);
	
КонецПроцедуры

////
 // Процедура: ФормаЭлементаПриСозданииНаСервере
 //   запускается при создании формы элемента на сервере.
 //
 // Параметры:
 //   Форма - Управляемая форма
 //     форма, для которой сработало событие ПриСозданииНаСервере.
 //   АрхивностьОбъекта - Булево
 //     значение реквизита актуальности.
 //   ИмяГруппы - Строка
 //     имя группы, куда будут создаваться необходимые элементы.
  ///
Процедура ФормаЭлементаПриСозданииНаСервере(Форма, Знач ЗначениеРеквизита, Знач ИмяГруппы = "") Экспорт
	
	ЭлементыФормы = Форма.Элементы;
	// если группа не задана, то не создаем декорации
	Если ЗначениеЗаполнено(ИмяГруппы) Тогда
		// создаем декорации для обозначения архивности
		Родитель = ЭлементыФормы.Найти(ИмяГруппы);
		ДекорацияПредупреждение = ЭлементыФормы.Добавить("Архивирование_ДекорацияПредупреждение", Тип("ДекорацияФормы"), Родитель);
		ДекорацияПредупреждение.Вид = ВидДекорацииФормы.Картинка;
		ДекорацияПредупреждение.Ширина = 4;
		ДекорацияПредупреждение.Высота = 2;
		
		ДекорацияНадпись = ЭлементыФормы.Добавить("Архивирование_ДекорацияНадпись", Тип("ДекорацияФормы"), Родитель);
		ДекорацияНадпись.Вид = ВидДекорацииФормы.Надпись;
		ДекорацияНадпись.Заголовок = СообщенияПользователю.Получить("Архивирование_ЗаголовокДекорацииСтатуса");
		ДекорацияНадпись.Ширина = 25;
		ДекорацияНадпись.Высота = 2;
		
		Если ОбщегоНазначения.ПодсистемаСуществует("ФункциональныеПодсистемы.ОформленияВидовОбъектов") Тогда
			МодульОформлений = ОбщегоНазначения.ОбщийМодуль("ОформленияВидовОбъектов");
			МодульОформлений.НастроитьУсловноеОформление(
														Форма,
														"Объект." + Архивирование.ИмяРеквизита(),
														"Архивирование_ДекорацияНадпись,Архивирование_ДекорацияПредупреждение",
														"АрхивированиеЭлемент"
												);
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

////
 // Процедура: ИнициализироватьАктуальностьНоменклатуры
 //   устанавливает отрицание значения недоступности.
 //
  ///
Процедура ИнициализироватьДоступность() Экспорт
	
	ЗначениеНедоступности = Архивирование.ЗначениеНедоступности();
	Если ЗначениеНедоступности = Истина Тогда
		Возврат;
	КонецЕсли;
	ИмяРеквизита = Архивирование.ИмяРеквизита();
	
	МассивОбъектовПодсистемы = АрхивированиеПовтИсп.ПолучитьМассивОбъектовПодсистемы();
	
	Для каждого ОбъектПодсистемы Из МассивОбъектовПодсистемы Цикл
	
		Запрос = Новый Запрос;
		Запрос.Текст =
		"ВЫБРАТЬ
		|	Ссылка,
		|	" + ИмяРеквизита + "
		|ИЗ
		|	" + ОбъектПодсистемы + "
		|УПОРЯДОЧИТЬ ПО
		|	" + ИмяРеквизита + "
		|ИТОГИ ПО
		|	" + ИмяРеквизита;
		
		Результат = Запрос.Выполнить();
		Если Результат.Пустой() Тогда
			Продолжить;
		КонецЕсли;
		Выгрузка = Результат.Выгрузить();
		Выборка = Результат.Выбрать(ОбходРезультатаЗапроса.ПоГруппировкам);
		
		КоличествоИстина = 0;
		КоличествоЛожь = 0;
		Пока Выборка.Следующий() Цикл
			Если Выборка[ИмяРеквизита] =  Истина Тогда
				ВыборкаИстина = Выборка.Выбрать();
				КоличествоИстина = ВыборкаИстина.Количество();
			Иначе
				ВыборкаЛожь = Выборка.Выбрать();
				КоличествоЛожь = ВыборкаЛожь.Количество();
			КонецЕсли;
		КонецЦикла;
		
		Если КоличествоИстина = 0 И КоличествоЛожь > 0 Тогда
			
			НачатьТранзакцию();
			
			Пока ВыборкаЛожь.Следующий() Цикл
				Попытка
					
					ЭлементОбъект = ВыборкаЛожь.Ссылка.ПолучитьОбъект();
					ЭлементОбъект.ДополнительныеСвойства.Вставить("СлужебнаяЗапись");
					ЭлементОбъект[ИмяРеквизита] = НЕ Архивирование.ЗначениеНедоступности();
					ОбновлениеИнформационнойБазы.ЗаписатьДанные(ЭлементОбъект, Ложь, Ложь);
					
				Исключение
					
					ОтменитьТранзакцию();
					
					ВызватьИсключение НСтр("ru='Не удалось обновить реквизит " + ИмяРеквизита + " у элемента'");
					
				КонецПопытки;
			КонецЦикла;
			
			Если ТранзакцияАктивна() Тогда
				ЗафиксироватьТранзакцию();
			КонецЕсли;
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

// иницирует обработчики подсистемы
Процедура ОбновлениеНаКаждуюВерсию() Экспорт
	
	ИнициализироватьДоступность();
	
КонецПроцедуры

////
 // Функция: НайтиПоЗависимымСсылкам
 //   запускается при создании формы элемента на сервере.
 //
 // Параметры:
 //   ПолноеИмяМетаданных - Строка
 //     путь к метаданным.
 //   МассивПоискаСсылок_ - Массив
 //     Список ссылок, на которые ищем зависимости.
 //
 // Возврат:
 //    Массив - коллекция зависимых объектов.
///
Функция НайтиПоЗависимымСсылкам(Знач ПолноеИмяМетаданных, МассивПоискаСсылок_) Экспорт
	
	СписокЗависимыхОбъектов = Новый Массив;
	
	МенеджерОбъекта = ОбщегоНазначения.МенеджерОбъектаПоПолномуИмени(ПолноеИмяМетаданных);
	
	Попытка
		МенеджерОбъекта.СписокЗависимыхОбъектов(СписокЗависимыхОбъектов);
	Исключение
		ТЗ = Новый ТаблицаЗначений;
		ТЗ.Колонки.Добавить("Отказ",);
		ТЗ.Колонки.Добавить("Ссылка");
		ТЗ.Колонки.Добавить("ТипЗначения");
		Возврат ТЗ;
	КонецПопытки;
	
	Запрос = Новый Запрос;
	Запрос.Текст = "";
	Для каждого ЗависимыйОбъект Из СписокЗависимыхОбъектов Цикл
		ИмяБезТабличныеЧасти = СтрЗаменить(ЗависимыйОбъект.ПолноеИмя, "ТабличнаяЧасть.", "");
		
		ЧастиИмени = СтрРазделить(ЗависимыйОбъект.ПолноеИмя, ".");
		
		КлассОМ = ЧастиИмени[0];
		ИмяОМ   = ЧастиИмени[1];
		
		ИмяБазовыхМетаданных = КлассОМ + "." + ИмяОМ;
	
		ЭтоРегистр = (КлассОМ = "РегистрСведений" ИЛИ КлассОМ = "РегистрНакопления");
		ЭтоТабличнаяЧасть = (ЭтоРегистр = Ложь И ЧастиИмени.Количество() > 2);
		
		ИмяРеквизитаСсылка = "Ссылка";
		ИмяРеквизитаПометкаУдаления = "ПометкаУдаления";
		ЗначениеПометкаУдаления = Ложь;
		ИмяПараметраТипЗначения_ = "ТИПЗНАЧЕНИЯ(Ссылка)";
		
		Текст = "ВЫБРАТЬ РАЗЛИЧНЫЕ
				|	&РеквизитСсылка КАК Ссылка,
				|	&ПараметрТипЗначения КАК ТипЗначения,
				|	&ПараметрОтказ КАК Отказ
				|ИЗ
				|	" + ИмяБезТабличныеЧасти + "
				|ГДЕ
				|	&РеквизитПометкаУдаления = &ЗначениеПометкаУдаления
				|	И " + ЗависимыйОбъект.РеквизитЗначения + " В (&Ссылки)";
				
		Если ЭтоРегистр = Истина Тогда
			ИмяРеквизитаСсылка = ЗависимыйОбъект.РеквизитЗначения;
			ИмяРеквизитаПометкаУдаления = "Активность";
			ИмяПараметраТипЗначения_ = """" + ИмяОМ + """";
			ЗначениеПометкаУдаления = Истина;
		ИначеЕсли ЭтоТабличнаяЧасть = Истина Тогда
			ИмяРеквизитаПометкаУдаления = "Ссылка.ПометкаУдаления";
		КонецЕсли;
		
		Текст = СтрЗаменить(Текст, "&РеквизитСсылка", ИмяРеквизитаСсылка);
		Текст = СтрЗаменить(Текст, "&РеквизитПометкаУдаления", ИмяРеквизитаПометкаУдаления);
		Текст = СтрЗаменить(Текст, "&ПараметрТипЗначения", ИмяПараметраТипЗначения_);
		
		Если АрхивированиеПовтИсп.МетаданныеИспользуютсяПодсистемой(ИмяБазовыхМетаданных) Тогда
			Текст = Текст + "
						|	И " + ?(ЭтоТабличнаяЧасть, "Ссылка.", "") +
						Архивирование.ИмяРеквизита() + 
						" = &Доступность";
		КонецЕсли;
		
		Если НЕ ПустаяСтрока(Запрос.Текст) Тогда
			Текст = "
					|
					| ОБЪЕДИНИТЬ ВСЕ
					|
					|" + Текст;
		КонецЕсли;
		
		ИмяПараметраОтказа_ = "Отказ" + Формат(СписокЗависимыхОбъектов.Найти(ЗависимыйОбъект), "ЧГ=");
		Текст = СтрЗаменить(Текст, "ПараметрОтказ", ИмяПараметраОтказа_);
		
		Запрос.Текст = Запрос.Текст + Текст;
		Запрос.УстановитьПараметр(ИмяПараметраОтказа_, ЗависимыйОбъект.Отказ);
		Запрос.УстановитьПараметр("ЗначениеПометкаУдаления", ЗначениеПометкаУдаления);
		
	КонецЦикла; 
	Запрос.УстановитьПараметр("Ссылки", МассивПоискаСсылок_);
	Запрос.УстановитьПараметр("Доступность", НЕ Архивирование.ЗначениеНедоступности());
	
	Возврат Запрос.Выполнить().Выгрузить();
	
КонецФункции

///
//  Процедура АрхивированиеПередЗаписью
//      определяет обработчик ПередЗаписью для подсистемы Архивирование
///
Процедура АрхивированиеПередЗаписью(Источник, Отказ) Экспорт
	
	ИмяРеквизита_ = Архивирование.ИмяРеквизита();
	
	ПолноеИмяМетаданных = Источник.Метаданные().ПолноеИмя();
	Если АрхивированиеПовтИсп.МетаданныеИспользуютсяПодсистемой(ПолноеИмяМетаданных) = Ложь Тогда
		Возврат;
	КонецЕсли;
	
	// принудительная запись сообщения
	Если Источник.ДополнительныеСвойства.Свойство("СлужебнаяЗапись") ИЛИ Источник.ЭтоГруппа Тогда
		Возврат;
	КонецЕсли;
	
	ЗначениеНедоступности = Архивирование.ЗначениеНедоступности();
	// для новых установим по умолчанию значение
	Если Источник.ЭтоНовый() Тогда
		Источник[ИмяРеквизита_] = НЕ ЗначениеНедоступности;
		Возврат;
	КонецЕсли;
	
	Если Источник.ОбменДанными.Загрузка ИЛИ Отказ Тогда
		Возврат;
	КонецЕсли;
	
	// проверки на установку архивности. на снятие они не нужны
	Если Источник[ИмяРеквизита_] = ЗначениеНедоступности И
		Источник[ИмяРеквизита_] <> ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Источник.Ссылка, ИмяРеквизита_)
	Тогда
		МассивПоискаСсылок_ = Новый Массив();
		МассивПоискаСсылок_.Добавить(Источник.Ссылка);
		НайденныеСсылки_ = АрхивированиеСервер.НайтиПоЗависимымСсылкам(ПолноеИмяМетаданных, МассивПоискаСсылок_);
		Если НайденныеСсылки_.Количество() = 0 Тогда
			Возврат;
		КонецЕсли;
		НайденныеСсылки_.Сортировать("Отказ Убыв");
		Для каждого НайденнаяСсылка Из НайденныеСсылки_ Цикл
			ПараметрыСообщения_ = Новый Структура("Элемент1,Элемент2,Элемент1Метаданные,Элемент2Метаданные", 
								Строка(Источник.Ссылка),
								Строка(НайденнаяСсылка.Ссылка),
								Строка(ТипЗнч(Источник.Ссылка)),
								НайденнаяСсылка.ТипЗначения
							);
			
			Если НайденнаяСсылка.Отказ Тогда
				СообщенияПользователю.Показать("Архивирование_ЗапретПомещенияВАрхив", ПараметрыСообщения_);
				Отказ = Истина;
			ИначеЕсли Отказ = Ложь Тогда
				СообщенияПользователю.Показать("Архивирование_ИнформацияОЗависимостях", ПараметрыСообщения_);
			КонецЕсли;
			
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры

///
//  Процедура АрхивированиеОбработкаПолученияДанныхВыбора
//      определяет обработчик ОбработкаПолученияДанныхВыбора для подсистемы Архивирование.
///
Процедура АрхивированиеОбработкаПолученияДанныхВыбора(Источник, ДанныеВыбора, Параметры, СтандартнаяОбработка) Экспорт
	
	МетаданныеОбработки_ = Источник.ПустаяСсылка().Метаданные().ПолноеИмя();
	// Требует переноса в функциональные подсистемы
	Если АрхивированиеПовтИсп.МетаданныеИспользуютсяПодсистемой(МетаданныеОбработки_) Тогда
		Если Параметры.Свойство("ВыборГруппИЭлементов") Тогда
			// точно иерархические
			Если Параметры.ВыборГруппИЭлементов = ИспользованиеГруппИЭлементов.Элементы Тогда
				// у всех элементов есть реквизит
				Параметры.Отбор.Вставить(Архивирование.ИмяРеквизита(), НЕ Архивирование.ЗначениеНедоступности());
			ИначеЕсли Параметры.ВыборГруппИЭлементов = ИспользованиеГруппИЭлементов.ГруппыИЭлементы Тогда
				
				// TODO: данный код вызывает крах платформы. Дамп выслан в отдел платформы
				// оставлен стандартный выбор элементов платформой
				
				// отберем либо группы, либо элементы актуальные
				//СтандартнаяОбработка = Ложь;
				//ДанныеВыбора = Новый СписокЗначений;
				//Запрос = Новый Запрос;
				//Запрос.Текст =
				//"ВЫБРАТЬ
				//|	Данные.Ссылка,
				//|	Данные.ПометкаУдаления
				//|ИЗ
				//|	" + МетаданныеОбработки_ + " КАК Данные
				//|ГДЕ
				//|	(НЕ &ОтборПоСтроке ИЛИ Данные.Наименование ПОДОБНО &Наименование)
				//|	И Данные.ПометкаУдаления = ЛОЖЬ
				//|	И (Данные.ЭтоГруппа = ИСТИНА ИЛИ Данные." + Архивирование.ИмяРеквизита() + " = &ЗначениеАктуальности)";
				//
				//Запрос.УстановитьПараметр("ОтборПоСтроке", ЗначениеЗаполнено(Параметры.СтрокаПоиска));
				//Если ЗначениеЗаполнено(Параметры.СтрокаПоиска) Тогда
				//	Если Параметры.СпособПоискаСтроки = СпособПоискаСтрокиПриВводеПоСтроке.ЛюбаяЧасть Тогда
				//		Запрос.УстановитьПараметр("Наименование", "%" + Параметры.СтрокаПоиска + "%");
				//	Иначе
				//		Запрос.УстановитьПараметр("Наименование", Параметры.СтрокаПоиска + "%");
				//	КонецЕсли;
				//Иначе
				//	Запрос.УстановитьПараметр("Наименование", "");
				//КонецЕсли;
				//Запрос.УстановитьПараметр("ЗначениеАктуальности", НЕ Архивирование.ЗначениеНедоступности());
				//
				//Выборка = Запрос.Выполнить().Выбрать();
				//Пока Выборка.Следующий() Цикл
				//	ДанныеВыбора.Добавить(Выборка.Ссылка, Выборка.Ссылка);
				//КонецЦикла;
				
			КонецЕсли;
		Иначе
			// нет иерархии
			Параметры.Отбор.Вставить(Архивирование.ИмяРеквизита(), НЕ Архивирование.ЗначениеНедоступности());
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти