﻿
#Область ПрограммныйИнтерфейс


// Возвращает информацию в какие списки входит номенклатура
//
// Параметры
//	Номенклатура - СправочникСсылка.Номенклатура
//
// Возвращаемое значение
//	Структура
//
Функция ПолучитьПринадлежностьКСпискам(Номенклатура) Экспорт
	
	ПринадлежностьКСпискам = Новый Структура;
	
	МенеджерЗаписи = РегистрыСведений.ПринадлежностьЛекарственныхСредствКСпискам.СоздатьМенеджерЗаписи();
	МенеджерЗаписи.Номенклатура = Номенклатура;
	МенеджерЗаписи.Прочитать();
	
	ПринадлежностьКСпискам.Вставить("БезрецептурныйОтпуск", МенеджерЗаписи.БезрецептурныйОтпуск);
	ПринадлежностьКСпискам.Вставить("ДЛО", МенеджерЗаписи.ДЛО);
	ПринадлежностьКСпискам.Вставить("ЖНиВЛП", МенеджерЗаписи.ЖНиВЛП);
	ПринадлежностьКСпискам.Вставить("Наркотические", МенеджерЗаписи.Наркотические);
	ПринадлежностьКСпискам.Вставить("СильнодействующиеИЯды", МенеджерЗаписи.СильнодействующиеИЯды);
	ПринадлежностьКСпискам.Вставить("СпискиАиБ", МенеджерЗаписи.СпискиАиБ);
	
	Возврат ПринадлежностьКСпискам;
	
КонецФункции

// Возвращает описание элементов формы для отображения информации о принадлежности к спискам.
//
// Возвращаемое значение
//	Строка
//
Функция ТекстовоеОписаниеЭлементовФормыПринадлежностьКСпискам() Экспорт
		
	Возврат
	"	<ГруппаФормы
	|			Имя='ГруппаПринадлежностьКСпискам'
	|			Вид='ОбычнаяГруппа'
	|			ОтображатьЗаголовок='Ложь'
	|			Отображение='Нет'
	|			Группировка='Вертикальная'>
	|		<ДекорацияФормы
	|				Имя='ИзменитьПринадлежностьКСпискам'
	|				Вид='Надпись'
	|				Заголовок='Изменить принадлежность к спискам...'
	|				Гиперссылка='Истина'
	|				Обработчики='Нажатие:Подключаемый_ДекорацияФормыНажатие,
	|								Клиент:СпискиЛекарственныхСредствКлиент.ИзменитьПринадлежностьКСпискам'/>
	|		<ГруппаФормы
	|				Имя='ГруппаКартинкиПринадлежностьКСпискам'
	|				Вид='ОбычнаяГруппа'
	|				ОтображатьЗаголовок='Ложь'
	|				Отображение='Нет'
	|				Группировка='Горизонтальная'>
	|			<ДекорацияФормы
	|					Имя='БезрецептурныйОтпуск'
	|					Вид='Картинка'
	|					Картинка='Безрецептурный'
	|					Ширина='5'
	|					Высота='2'
	|					Подсказка='Препарат безрецептурного отпуска.'/>
	|			<ДекорацияФормы
	|					Имя='ДЛО'
	|					Вид='Картинка'
	|					Картинка='ДЛО'
	|					Ширина='5'
	|					Высота='2'
	|					Подсказка='Отпускается по рецепту врача при оказании дополнительной бесплатной медицинской помощи отдельным категориям граждан.'/>
	|			<ДекорацияФормы
	|					Имя='ЖНиВЛПБезКонтроляЦены'
	|					Вид='Картинка'
	|					Картинка='ЖНиВЛПБезКонтроляЦены'
	|					Ширина='5'
	|					Высота='2'
	|					Подсказка='Включено в перечень жизненно-необходимых и важнейших лекарственных средств (контроль цен не осуществляется).'/>
	|			<ДекорацияФормы
	|					Имя='ЖНиВЛПСКонтролемЦен'
	|					Вид='Картинка'
	|					Картинка='ЖНиВЛПСКонтролемЦен'
	|					Ширина='5'
	|					Высота='2'
	|					Подсказка='Включено в перечень жизненно-необходимых и важнейших лекарственных средств (осуществляется контроль цен).'/>
	|			<ГруппаФормы
	|					Имя='ГруппаНаркотические'
	|					Вид='ОбычнаяГруппа'
	|					ОтображатьЗаголовок='Ложь'
	|					Отображение='Нет'>
	|				<ДекорацияФормы
	|						Имя='Наркотические'
	|						Вид='Картинка'
	|						Картинка='Наркотические'
	|						Ширина='7'
	|						Высота='2'
	|						Подсказка='Включено в перечень наркотических средств, психотропных веществ и их прекурсоров.'/>
	|				<ДекорацияФормы
	|						Имя='НаркотическиеТекст'
	|						Вид='Надпись'
	|						Заголовок='Список IV'
	|						ГоризонтальноеПоложение='Центр'
	|						Ширина='7'/>
	|
	|			</ГруппаФормы>
	|			<ГруппаФормы
	|					Имя='ГруппаСильнодействующиеИЯды'
	|					Вид='ОбычнаяГруппа'
	|					ОтображатьЗаголовок='Ложь'
	|					Отображение='Нет'>
	|				<ДекорацияФормы
	|						Имя='СильнодействующиеИЯды'
	|						Вид='Картинка'
	|						Картинка='Яд'
	|						Ширина='14'
	|						Высота='2'/>
	|				<ДекорацияФормы
	|						Имя='СильнодействующиеИЯдыТекст'
	|						Вид='Надпись'
	|						Заголовок='Сильнодействующее'
	|						ГоризонтальноеПоложение='Центр'
	|						Ширина='14'/>
	|
	|			</ГруппаФормы>
	|			<ГруппаФормы
	|					Имя='ГруппаСпискиАиБ'
	|					Вид='ОбычнаяГруппа'
	|					ОтображатьЗаголовок='Ложь'
	|					Отображение='Нет'>
	|				<ДекорацияФормы
	|						Имя='СпискиАиБ'
	|						Вид='Картинка'
	|						Картинка='СпискиАиБ'
	|						Ширина='6'
	|						Высота='2'/>
	|				<ДекорацияФормы
	|						Имя='СпискиАиБТекст'
	|						Вид='Надпись'
	|						Заголовок='Список А'
	|						Ширина='6'/>
	|
	|			</ГруппаФормы>
	|
	|		</ГруппаФормы>
	|
	|	</ГруппаФормы>
	|";
	
КонецФункции


#КонецОбласти