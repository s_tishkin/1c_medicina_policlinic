﻿////////////////////////////////////////////////////////////////////////////////
// Подсистема "Журнал событий печати".
// Внутренние процедуры и функции для работы с журналом печати.
//  
////////////////////////////////////////////////////////////////////////////////

#Область ПрограммныйИнтерфейс

// Процедура обобщения действий после печати
//
// Параметры:
//  ПараметрыФормыПечати  - Структура() - данные
//                 необходимые для печати.
//
Процедура ПослеПечати(Знач ПараметрыФормыПечати) Экспорт
	
	// вызываем все подписки
	РегистрируемыеОбъекты = Новый Массив;
	
	ЖурналСобытийПечатиВызовСервера.ПередРегистрациейПечати(РегистрируемыеОбъекты, ПараметрыФормыПечати);
	
	Если РегистрируемыеОбъекты.Количество() > 0 Тогда
		ЖурналСобытийПечатиВызовСервера.РегистрацияСобытияПечати(РегистрируемыеОбъекты);
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти