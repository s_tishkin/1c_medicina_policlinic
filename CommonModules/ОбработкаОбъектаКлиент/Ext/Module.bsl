﻿
#Область ПрограммныйИнтерфейс

////
// Процедура ВыполнитьДействияНадОбъектом
// Параметры:
///
Процедура ВыполнитьДействияНадОбъектом(Объект, СтруктураДействий, КэшированныеЗначения) Экспорт
	
	ОбработкаОбъектаКлиентСервер.ИнициализироватьКэшируемыеЗначения(КэшированныеЗначения, СтруктураДействий);
	
	Если Объект = Неопределено Тогда
		Возврат;
	КонецЕсли;
	
	Если НеобходимВызовСервера(Объект, СтруктураДействий, КэшированныеЗначения) Тогда
		
		ПредставлениеОбъекта = ПолучитьПредставлениеОбъекта(Объект, СтруктураДействий);
		ОбработкаОбъектаСервер.ВыполнитьДействияНадОбъектом(ПредставлениеОбъекта, СтруктураДействий, КэшированныеЗначения);
		ЗаполнитьЗначенияСвойств(Объект, ПредставлениеОбъекта);
		
		Возврат;
		
	КонецЕсли;
	
	ВыполнитьДействияНадОбъектомНаКлиенте(Объект, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры

Функция НеобходимВызовСервера(Объект, СтруктураДействий, КэшированныеЗначения)
	
	Если СтруктураДействий.Свойство("ПересчитатьКоличествоЕдиниц") 
	   И ЗначениеЗаполнено(Объект.Упаковка)
	   И КэшированныеЗначения.КоэффициентыУпаковок[Объект.Упаковка] = Неопределено Тогда
		Возврат Истина;
	КонецЕсли;

	Возврат Ложь;
	
КонецФункции

Функция ПолучитьПредставлениеОбъекта(Объект, СтруктураДействий)
	
	ПредставлениеОбъекта = Новый Структура;
	
	Если СтруктураДействий.Свойство("ПересчитатьКоличествоЕдиниц") Тогда
		ПредставлениеОбъекта.Вставить("Упаковка");
		ПредставлениеОбъекта.Вставить("КоличествоУпаковок", 0);
		ПредставлениеОбъекта.Вставить("Количество", 0);
	КонецЕсли;
	
	ЗаполнитьЗначенияСвойств(ПредставлениеОбъекта, Объект);
	
	Возврат ПредставлениеОбъекта;
	
КонецФункции


Процедура ВыполнитьДействияНадОбъектомНаКлиенте(Объект, СтруктураДействий, КэшированныеЗначения)
	
	ПересчитатьКоличествоЕдиниц(Объект, СтруктураДействий, КэшированныеЗначения);
	
КонецПроцедуры


Процедура ПересчитатьКоличествоЕдиниц(Объект, СтруктураДействий, КэшированныеЗначения)
	
	Если СтруктураДействий.Свойство("ПересчитатьКоличествоЕдиниц") Тогда
		Объект.Количество = Объект.КоличествоУпаковок
		   * ПолучитьКоэффициентУпаковки(Объект.Упаковка, КэшированныеЗначения);
	КонецЕсли;
	
КонецПроцедуры



Функция ПолучитьКоэффициентУпаковки(Упаковка, КэшированныеЗначения)

	Если ЗначениеЗаполнено(Упаковка) Тогда
		Коэффициент = КэшированныеЗначения.КоэффициентыУпаковок[Упаковка];
	Иначе
		Коэффициент = 1;
	КонецЕсли;

	Возврат Коэффициент;

КонецФункции


#КонецОбласти