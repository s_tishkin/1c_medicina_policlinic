﻿////////////////////////////////////////////////////////////////////////////////
// Подсистема "Запрет редактирования реквизитов объектов".
// 
////////////////////////////////////////////////////////////////////////////////

#Область ПрограммныйИнтерфейс

// Определить объекты метаданных, в модулях менеджеров которых ограничивается возможность редактирования реквизитов
// с помощью экспортной функции ПолучитьБлокируемыеРеквизитыОбъекта.
//
// Параметры:
//   Объекты - Соответствие - в качестве ключа указать полное имя объекта метаданных,
//                            подключенного к подсистеме "Запрет редактирования реквизитов объектов";
//                            В качестве значения - пустую строку.
//
// Пример: 
//   Объекты.Вставить(Метаданные.Документы.ЗаказПокупателя.ПолноеИмя(), "");
//
Процедура ПриОпределенииОбъектовСЗаблокированнымиРеквизитами(Объекты) Экспорт
	
	// МедицинаПоликлиника
	Объекты.Вставить(Метаданные.Справочники.БанковскиеСчетаОрганизаций.ПолноеИмя(), "ПолучитьБлокируемыеРеквизитыОбъекта");
	Объекты.Вставить(Метаданные.Справочники.БанковскиеСчетаПолучателей.ПолноеИмя(), "ПолучитьБлокируемыеРеквизитыОбъекта");
	Объекты.Вставить(Метаданные.Справочники.БонусныеПрограммыЛояльности.ПолноеИмя(), "ПолучитьБлокируемыеРеквизитыОбъекта");
	Объекты.Вставить(Метаданные.Справочники.ВидыКартЛояльности.ПолноеИмя(), "ПолучитьБлокируемыеРеквизитыОбъекта");
	Объекты.Вставить(Метаданные.Справочники.ВидыНоменклатуры.ПолноеИмя(), "ПолучитьБлокируемыеРеквизитыОбъекта");
	Объекты.Вставить(Метаданные.Справочники.ВидыЦен.ПолноеИмя(), "ПолучитьБлокируемыеРеквизитыОбъекта");
	Объекты.Вставить(Метаданные.Справочники.ГрафикиОплаты.ПолноеИмя(), "ПолучитьБлокируемыеРеквизитыОбъекта");
	Объекты.Вставить(Метаданные.Справочники.ДоговорыКонтрагентов.ПолноеИмя(), "ПолучитьБлокируемыеРеквизитыОбъекта");
	Объекты.Вставить(Метаданные.Справочники.КартыЛояльности.ПолноеИмя(), "ПолучитьБлокируемыеРеквизитыОбъекта");
	Объекты.Вставить(Метаданные.Справочники.Кассы.ПолноеИмя(), "ПолучитьБлокируемыеРеквизитыОбъекта");
	Объекты.Вставить(Метаданные.Справочники.КассыККМ.ПолноеИмя(), "ПолучитьБлокируемыеРеквизитыОбъекта");
	Объекты.Вставить(Метаданные.Справочники.Номенклатура.ПолноеИмя(), "ПолучитьБлокируемыеРеквизитыОбъекта");
	Объекты.Вставить(Метаданные.Справочники.Организации.ПолноеИмя(), "ПолучитьБлокируемыеРеквизитыОбъекта");
	Объекты.Вставить(Метаданные.Справочники.ПравилаНачисленияИСписанияБонусныхБаллов.ПолноеИмя(), "ПолучитьБлокируемыеРеквизитыОбъекта");
	Объекты.Вставить(Метаданные.Справочники.СегментыНоменклатуры.ПолноеИмя(), "ПолучитьБлокируемыеРеквизитыОбъекта");
	Объекты.Вставить(Метаданные.Справочники.СегментыПартнеров.ПолноеИмя(), "ПолучитьБлокируемыеРеквизитыОбъекта");
	Объекты.Вставить(Метаданные.Справочники.СкидкиНаценки.ПолноеИмя(), "ПолучитьБлокируемыеРеквизитыОбъекта");
	Объекты.Вставить(Метаданные.Справочники.УсловияПредоставленияСкидокНаценок.ПолноеИмя(), "ПолучитьБлокируемыеРеквизитыОбъекта");
	Объекты.Вставить(Метаданные.Справочники.ЭквайринговыеТерминалы.ПолноеИмя(), "ПолучитьБлокируемыеРеквизитыОбъекта");
	
	// МедицинскаяНСИ
	Объекты.Вставить(Метаданные.Справочники.ТорговыеНаименования.ПолноеИмя(), "ПолучитьБлокируемыеРеквизитыОбъекта");
	Объекты.Вставить(Метаданные.Справочники.УпаковкиНоменклатуры.ПолноеИмя(), "ПолучитьБлокируемыеРеквизитыОбъекта");
	Объекты.Вставить(Метаданные.Справочники.ФормыВыпуска.ПолноеИмя(), "ПолучитьБлокируемыеРеквизитыОбъекта");
	
КонецПроцедуры

#КонецОбласти
