﻿//// Подсистема "Криптография".
////  
//////////////////////////////////////////////////////////////////////////////////

#Область ПрограммныйИнтерфейс

// Выгружает указанный сертификат хранилища в строку base64.
//
// Параметры:
//  ОповещениеОЗавершении - ОписаниеОповещения - описание процедуры, принимающей результат.
//    Результат - Структура:
//      * Выполнено            - Булево - если Истина, то процедура успешно выполнена и получен результат, иначе см. ОписаниеОшибки.
//      * МенеджерКриптографии - AddIn  - объект используемый для работы с файлами. Работать напрямую с объектом запрещено.
//      * ОписаниеОшибки       - Строка - описание ошибки выполнения.
//      * СтрокаBase64         - Строка - содержимое сертификата в Base64.
//
//  Сертификат - Структура - описание сертификата.
//    * СерийныйНомер - Строка - серийный номер сертификата.
//    * Поставщик     - Строка - издатель сертификата.
//    * Отпечаток     - Строка - отпечаток сертификата. Используется для поиска сертификата если не указаны СерийныйНомер и Поставщик.
//
//  ВыводитьСообщения     - Булево - устанавливает признак необходимости выводить сообщения об ошибках.
//
//  МенеджерКриптографии  - AddIn  - объект используемый для работы с криптографией. Если не задан, то будет создан новый.
//
Процедура ЭкспортироватьСертификатВBase64(ОповещениеОЗавершении, Сертификат, ВыводитьСоообщения = Истина, МенеджерКриптографии = Неопределено) Экспорт
	
	КриптографияЭДКОСлужебныйКлиент.ЭкспортироватьСертификатВBase64(ОповещениеОЗавершении, Сертификат, ВыводитьСоообщения, МенеджерКриптографии);
	
КонецПроцедуры

// Выполняет поиск сертификата в хранилище по отпечатку сертификата.
//
// Параметры:
//  ОповещениеОЗавершении - ОписаниеОповещения - описание процедуры, принимающей результат.
//    Результат - Структура:
//      * Выполнено            - Булево - если Истина, то процедура успешно выполнена и получен результат, иначе см. ОписаниеОшибки.
//      * МенеджерКриптографии - AddIn  - объект используемый для работы с файлами. Работать напрямую с объектом запрещено.
//      * ОписаниеОшибки       - Строка - описание ошибки выполнения.
//      * СертификатНайден     - Булево - сертификат успешно найден.
//      * СвойстваСертификата  - ФиксированнаяСтруктура - описание сертификата.
//        ** Версия                    - Строка - версия сертификата.
//        ** Наименование              - Строка - представление сертификата.
//        ** СерийныйНомер             - Строка - серийный номер сертификата.
//        ** Поставщик                 - Строка - издатель сертификата.
//        ** Владелец                  - Строка - владелец сертификата.
//        ** Отпечаток                 - Строка - отпечаток сертификата.
//        ** ИспользоватьДляПодписи    - Булево - пригоден для подписывания.
//        ** ИспользоватьДляШифрования - Булево - пригоден для шифрования.
//        ** ДействителенС             - Дата   - начало срока действия.
//        ** ДействителенПо            - Дата   - конец срока дейстия.
//        ** Валиден                   - Булево - сертификат соответствует требованиям проверки.
//
//  Отпечаток             - Строка - отпечаток сертификата.
//
//  ХранилищеСертификатов - Строка - задает хранилище для поиска. MY, AddressBook, CA, ROOT.
//
//  ВыполнятьПроверку     - Булево - если Истина, то для найденых сертификатов будет выполняться проверка.
//
//  ВыводитьСообщения     - Булево - устанавливает признак необходимости выводить сообщения об ошибках.
//
//  МенеджерКриптографии  - AddIn  - объект используемый для работы с криптографией. Если не задан, то будет создан новый.
//
Процедура НайтиСертификатПоОтпечатку(ОповещениеОЗавершении, Отпечаток, ХранилищеСертификатов = Неопределено, 
									ВыполнятьПроверку = Ложь, ВыводитьСоообщения = Истина, МенеджерКриптографии = Неопределено) Экспорт
	
	КриптографияЭДКОСлужебныйКлиент.НайтиСертификатПоОтпечатку(
		ОповещениеОЗавершении, Отпечаток, ХранилищеСертификатов, ВыполнятьПроверку, ВыводитьСоообщения, МенеджерКриптографии);
	
КонецПроцедуры

// Создает файл подписи для указанного файла данных.
//
// Параметры:
//  ОповещениеОЗавершении  - ОписаниеОповещения - описание процедуры, принимающей результат.
//    Результат - Структура:
//      * Выполнено            - Булево - если Истина, то процедура успешно выполнена и получен результат, иначе см. ОписаниеОшибки.
//      * МенеджерКриптографии - AddIn  - объект используемый для работы с файлами. Работать напрямую с объектом запрещено.
//      * ОписаниеОшибки       - Строка - описание ошибки выполнения.
//      * ИмяФайлаПодписи      - Строка - имя файла, в который будет сохранена подпись.
//
//  Сертификат - Структура - описание сертификата подписанта.
//    * СерийныйНомер - Строка - серийный номер сертификата.
//    * Поставщик     - Строка - издатель сертификата.
//    * Отпечаток     - Строка - отпечаток сертификата. Используется для поиска сертификата если не указаны СерийныйНомер и Поставщик.
//
//  ИмяФайлаДанных        - Строка - файл, для которого необходимо сформировать подпись.
//
//  ПараметрыКриптографии - Структура - если Истина, то создает присоединенную подпись, иначе - отсоединенную.
//    * АлгоритмХеширования - Число - идентификатор алгоритма хеширования (см. ПолучитьАлгоритмы).
//
//  ИмяФайлаИлиРасширение  - Строка - имя файла, в который необходимо сохранить результат.
//                                   Также можно указать только расширение создаваемого файла - ".расширение".
//
//  ВыводитьСообщения     - Булево - устанавливает признак необходимости выводить сообщения об ошибках.
//
//  МенеджерКриптографии  - AddIn  - объект используемый для работы с криптографией. Если не задан, то будет создан новый.
//
Процедура Подписать(ОповещениеОЗавершении, Сертификат, ИмяФайлаДанных, ПараметрыКриптографии, ИмяФайлаИлиРасширение = Неопределено, ВыводитьСоообщения = Истина, МенеджерКриптографии = Неопределено) Экспорт
	
	КриптографияЭДКОСлужебныйКлиент.Подписать(
		ОповещениеОЗавершении, Сертификат, ИмяФайлаДанных, ПараметрыКриптографии, ИмяФайлаИлиРасширение, ВыводитьСоообщения, МенеджерКриптографии);
	
КонецПроцедуры

// Выполняет проверку файла для указанного файла данных.
//
// Параметры:
//  ОповещениеОЗавершении  - ОписаниеОповещения - описание процедуры, принимающей результат.
//    Результат - Структура:
//      * Выполнено            - Булево - если Истина, то процедура успешно выполнена и получен результат, иначе см. ОписаниеОшибки.
//      * МенеджерКриптографии - AddIn  - объект используемый для работы с файлами. Работать напрямую с объектом запрещено.
//      * ОписаниеОшибки       - Строка - описание ошибки выполнения.
//      * ПодписьВалидна       - Булево - результат проверки подписи.
//
//  Сертификат - Структура - описание сертификата подписанта.
//    * СерийныйНомер - Строка - серийный номер сертификата.
//    * Поставщик     - Строка - издатель сертификата.
//    * Отпечаток     - Строка - отпечаток сертификата. Используется для поиска сертификата если не указаны СерийныйНомер и Поставщик.
//
//  ИмяФайлаПодписи       - Строка - файл подписи, которую необходимо проверить.
//
//  ИмяФайлаДанных        - Строка - файл, для которого необходимо проверить подпись.
//
//  ПараметрыКриптографии - Структура - если Истина, то создает присоединенную подпись, иначе - отсоединенную.
//    * АлгоритмХеширования - Число - идентификатор алгоритма хеширования (см. ПолучитьАлгоритмы).
//
//  ВыводитьСообщения     - Булево - устанавливает признак необходимости выводить сообщения об ошибках.
//
//  МенеджерКриптографии  - AddIn  - объект используемый для работы с криптографией. Если не задан, то будет создан новый.
//
Процедура ПроверитьПодпись(ОповещениеОЗавершении, Сертификат, ИмяФайлаПодписи, ИмяФайлаДанных, ПараметрыКриптографии, ВыводитьСоообщения = Истина, МенеджерКриптографии = Неопределено) Экспорт
	
	КриптографияЭДКОСлужебныйКлиент.ПроверитьПодпись(
		ОповещениеОЗавершении, Сертификат, ИмяФайлаПодписи, ИмяФайлаДанных, ПараметрыКриптографии, ВыводитьСоообщения, МенеджерКриптографии);
	
КонецПроцедуры

// Шифрует данные по методу Диффи-Хелмана.
//
// Параметры:
//  ОповещениеОЗавершении  - ОписаниеОповещения - описание процедуры, принимающей результат.
//    Результат - Структура:
//      * Выполнено            - Булево - если Истина, то процедура успешно выполнена и получен результат, иначе см. ОписаниеОшибки.
//      * МенеджерКриптографии - AddIn  - объект используемый для работы с файлами. Работать напрямую с объектом запрещено.
//      * ОписаниеОшибки       - Строка - описание ошибки выполнения.
//      * КаталогРезультата    - Строка - каталог, в который будут сохранены файлы результата.
//
//  СертификатОтправителя - Структура - описание сертификата.
//    * СерийныйНомер - Строка - серийный номер сертификата.
//    * Поставщик     - Строка - издатель сертификата.
//    * Отпечаток     - Строка - отпечаток сертификата. Используется для поиска сертификата если не указаны СерийныйНомер и Поставщик.
//
//  ИмяФайлаДанных - Строка - шифруемый файл.
//
//  СертификатПолучателя - Структура - описание сертификата.
//    * СерийныйНомер - Строка - серийный номер сертификата.
//    * Поставщик     - Строка - издатель сертификата.
//    * Отпечаток     - Строка - отпечаток сертификата. Используется для поиска сертификата если не указаны СерийныйНомер и Поставщик.
//
//  ПараметрыКриптографии - Структура - если Истина, то создает присоединенную подпись, иначе - отсоединенную.
//    * АлгоритмШифрования - Число - алгоритм шифрования. Если параметр не задан, идентификатор алгоритма будет браться из сертификата.
//    * АлгоритмКлюча      - Число - алгоритм генерации сессионного ключа.
//    * Режим              - Число - 0 - используется режим по умолчанию. 
//                                   1 - Cipher block chaining
//                                   2 - Electronic code book
//                                   4 - Cipher feedback mode
//                                   5 - Ciphertext stealing mode
//
//  КаталогРезультата    - Строка - каталог, в который будут сохранены файлы результата.
//
//  ВыводитьСообщения    - Булево - устанавливает признак необходимости выводить сообщения об ошибках.
//
//  МенеджерКриптографии - AddIn  - объект используемый для работы с криптографией. Если не задан, то будет создан новый.
//
Процедура Зашифровать(ОповещениеОЗавершении, СертификатОтправителя = Неопределено, ИмяФайлаДанных, СертификатПолучателя, ПараметрыКриптографии, 
						КаталогРезультата = Неопределено, ВыводитьСоообщения = Истина, МенеджерКриптографии = Неопределено) Экспорт
	
	КриптографияЭДКОСлужебныйКлиент.Зашифровать(
		ОповещениеОЗавершении, СертификатОтправителя, ИмяФайлаДанных, СертификатПолучателя, 
		ПараметрыКриптографии, КаталогРезультата, ВыводитьСоообщения, МенеджерКриптографии);
	
КонецПроцедуры

// Расшифровывает данные методу Диффи-Хелмана в блочном режиме с обратной связью.
//
// Параметры:
//  ОповещениеОЗавершении  - ОписаниеОповещения - описание процедуры, принимающей результат.
//    Результат - Структура:
//      * Выполнено            - Булево - если Истина, то процедура успешно выполнена и получен результат, иначе см. ОписаниеОшибки.
//      * МенеджерКриптографии - AddIn  - объект используемый для работы с файлами. Работать напрямую с объектом запрещено.
//      * ОписаниеОшибки       - Строка - описание ошибки выполнения.
//      * РасшифрованныйФайл   - Строка - имя файла.
//
//  СертификатПолучателя - Структура - сертификат для расшифровки.
//    * СерийныйНомер    - Строка - серийный номер сертификата.
//    * Поставщик        - Строка - издатель сертификата.
//    * Отпечаток        - Строка - отпечаток сертификата. Используется для поиска сертификата если не указаны СерийныйНомер и Поставщик.
//
//  ИмяЗашифрованногоФайла - Строка - полное имя зашифрованного файла на клиенте.
//
//  ПубличныйКлючОтправителя - Строка - полное имя файла публичного ключа эфемерной ключевой пары отправителя на клиенте.
//
//  СессионныйКлюч - Строка - полное имя файла сессионного ключа на клиенте.
//
//  ИнициализационныйВектор - Строка - полное имя файла инициализационного вектора на клиенте.
//
//  ИмяФайлаИлиРасширение - Строка - имя файла, в который необходимо сохранить результат.
//                                   Также можно указать только расширение создаваемого файла - ".расширение".
//
//  ВыводитьСообщения      - Булево - устанавливает признак необходимости выводить сообщения об ошибках.
//
//  МенеджерКриптографии   - AddIn  - объект используемый для работы с криптографией. Если не задан, то будет создан новый.
//
Процедура РасшифроватьФайл(ОповещениеОЗавершении, СертификатПолучателя, ИмяЗашифрованногоФайла, ПубличныйКлючОтправителя, СессионныйКлюч,
						ИнициализационныйВектор, ИмяФайлаИлиРасширение = Неопределено, ВыводитьСоообщения = Истина, МенеджерКриптографии = Неопределено) Экспорт
	
	КриптографияЭДКОСлужебныйКлиент.РасшифроватьФайл(
		ОповещениеОЗавершении, СертификатПолучателя, ИмяЗашифрованногоФайла, ПубличныйКлючОтправителя, СессионныйКлюч,
		ИнициализационныйВектор, ИмяФайлаИлиРасширение, ВыводитьСоообщения, МенеджерКриптографии);
	
КонецПроцедуры

/// Хеширование данных по алгоритму ГОСТ Р 34.11-2012
//
// Параметры:
//   ОповещениеОЗавершении - ОписаниеОповещения -
//     Оповещение, которое вызовется после завершения хеширования.
//   ИмяФайлаДанных - Строка -
//     Имя файла хешируемых данных.
//   ВыводитьСоообщения - Булево -
//     Признак необходимости вывода сообщений внешней компоненты.
//   МенеджерКриптографии - ВнешняяКомпонента, Неопределено -
//     Используемая компонента криптографии.
//   УдалятьВременныйФайл - Булево -
//     Признак необходимости удалить файл данных после хеширования.
///
Процедура ХешироватьДанные // Экспорт, 5 параметров
(
	ОповещениеОЗавершении,
	ИмяФайлаДанных,
	ВыводитьСоообщения = Истина,
	МенеджерКриптографии = Неопределено,
	УдалятьВременныйФайл = Истина
)
Экспорт
	
	КриптографияЭДКОСлужебныйКлиент.ХешироватьДанные(
		ОповещениеОЗавершении, ИмяФайлаДанных, ВыводитьСоообщения, МенеджерКриптографии, УдалятьВременныйФайл
	);
	
КонецПроцедуры

#КонецОбласти
