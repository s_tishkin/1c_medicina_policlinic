﻿
#Область ПрограммныйИнтерфейс

////
 // Функция: xslt_html2xml
 //     Это функция используется для ускорения работы создания МД в обработках-ШМД. 
 //   Она возвращает объект для XSL преобразования XHTML документа в XML документ.
 //   Результирующий документ - это тэг "section" документа стандарта CDA HL 7 r.2
 //
 // Возврат:
 //   Объект ПреобразованиеXSL, для преобразования.
 ///
Функция xslt_html2xml() Экспорт
	Преобразование = Новый ПреобразованиеXSL;
	Преобразование.ЗагрузитьИзСтроки(
		Справочники.ВизуализаторыМедицинскихДокументов.ПолучитьМакет("html2xml").ПолучитьТекст()
	);
	Возврат Преобразование;
КонецФункции

/// Формирует префикс изображений.
//
Функция ПрефиксПерманентныхИзображений() Экспорт
	Возврат "Перманентная картинка ШМД: ";
КонецФункции

/// Проверяет режим запуска ШМД.
//
Функция БезопасныйРежимЗапускаШМД() Экспорт
	Возврат Константы.БезопасныйРежимЗапускаШМД.Получить();
КонецФункции


#КонецОбласти