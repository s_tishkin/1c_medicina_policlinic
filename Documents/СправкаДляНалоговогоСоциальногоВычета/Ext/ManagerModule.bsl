﻿
#Область ПрограммныйИнтерфейс

#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////
 // Процедура: Печать
 //   Формирует печатную форму документа.
 //
 // Параметры:
 //   МассивОбъектов
 //     Массив ссылок на объекты, для которых нужно сформировать печатную форму.
 //   ПараметрыПечати.
 //     Дополнительные параметры печати, переданные из модуля команды.
 //   КоллекцияПечатныйФорм.
 //     Имена макетов.
 //   ОбъектыПечати
 //     Объекты печати.
 //   ПараметрыВывода
 //     Через эту структуру обработчик печати может вернуть параметры.
 ///
Процедура Печать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, 
	ОбъектыПечати, ПараметрыВывода) Экспорт
	
	Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФорм, "ПФ_MXL_СправкаДляНалоговой")
	Тогда
	
		// Формируем табличный документ и добавляем его в коллекцию печатных форм
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(КоллекцияПечатныхФорм,
			"ПФ_MXL_СправкаДляНалоговой", "Справка для налоговой",
			ПечатьНалоговойСправки(МассивОбъектов, ОбъектыПечати)
		);
		
	КонецЕсли;
	
КонецПроцедуры

////
 // Функция: ПечатьНалоговойСправки
 //   Возвращает сформированный табличный документ.
 //
 // Параметры:
 //   МассивОбъектов
 //     Массив ссылок на документы.
 //   ОбъектПечати
 //     Объекты печати.
 //
 // Возврат:
 //   Сформированный табличный документ.
 ///
Функция ПечатьНалоговойСправки(МассивОбъектов, ОбъектыПечати)
	
	// Создаем табличный документ и устанавливаем имя параметров печати
	ТабличныйДокумент = Новый ТабличныйДокумент;
	//ТабличныйДокумент.ИмяПараметровПечати = "ПараметрыПечати_СправкаДляНалоговой";
	
	ПервыйДокумент = Истина;
	
	ПараметрыОбласти = Новый Структура(
		"Налогоплательщик,
		|ИНННалогоплательщика,
		|НомерКарты,
		|СтоимостьУслуг,
		|СтоимостьУслугПрописью,
		|ДатаОплаты,
		|ДатаВыдачи,
		|Номер,
		|ДанныеОрганизацииСтр1,
		|ДанныеОрганизацииСтр2,
		|НомерТелефона,
		|ВыдавшийСправку, 
		|Пациент, 
		|МедКарта"
	);
	
	Для Каждого Эл Из МассивОбъектов Цикл
		Если Не ПервыйДокумент Тогда
			// Все документы нужно выводить на разных страницах.
			ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();
		КонецЕсли;
		ПервыйДокумент = Ложь;
		// Запомним номер строки, с которой начали выводить текущий документ.
		НомерСтрокиНачало = ТабличныйДокумент.ВысотаТаблицы + 1;
		
		Макет = УправлениеПечатью.МакетПечатнойФормы(
			"Документ.СправкаДляНалоговогоСоциальногоВычета.ПФ_MXL_СправкаДляНалоговой"
		);
		
		ОбластьМакета = Макет.ПолучитьОбласть("Область1");
		
		ЗаполнитьЗначенияСвойств(ПараметрыОбласти, Эл);
		СтрНомерКарты = "";
		Для Каждого Стр Из Эл.МедицинскиеКарты Цикл
			ПолныйНомерКарты = Стр.МедицинскаяКарта.НомерКартыПредставление;
			СтрНомерКарты = СтрНомерКарты + ПолныйНомерКарты + "; ";
		КонецЦикла;
		// Удаляем последние точку с запятой и пробел
		Если Эл.МедицинскиеКарты.Количество() = 1 Тогда
			ПараметрыОбласти.МедКарта = Эл.МедицинскиеКарты[0].МедицинскаяКарта;
		КонецЕсли;
		Если СтрДлина(СтрНомерКарты) > 2 Тогда
			СтрНомерКарты = Лев(СтрНомерКарты, СтрДлина(СтрНомерКарты)-2);
		КонецЕсли;
		ПараметрыОбласти.НомерКарты = СтрНомерКарты;
		ПараметрыОбласти.Пациент = Эл.Пациент;
		
		ПрописьЧисла = ЧислоПрописью(Эл.СтоимостьУслуг, ,
			"рубль, рубля, рублей, м, копейка, копейки, копеек, ж, 2 знака"
		);
		ПараметрыОбласти.СтоимостьУслугПрописью = ПрописьЧисла;
		
		ПараметрыОбласти.СтоимостьУслугПрописью = ПрописьЧисла;
		ПараметрыОбласти.СтоимостьУслуг = Строка(Эл.СтоимостьУслуг);
		
		ПараметрыОбласти.ДатаОплаты = Формат(Эл.ДатаОплаты, "ДЛФ=DD");
		ПараметрыОбласти.ДатаВыдачи = Формат(Эл.ДатаВыдачи, "ДЛФ=DD");
		
		ПараметрыОбласти.ИНННалогоплательщика = Эл.ИНН;
		
		// Получаем данные по организации
		Запрос = Новый Запрос;
		Запрос.Текст = 
		 "ВЫБРАТЬ
		 |	РегНастроек.Значение КАК ЗначениеНастройки
		 |ИЗ
		 |	РегистрСведений.НастройкиСистемы.СрезПоследних(
		 |			&ТекДата,
		 |			Настройка = ЗНАЧЕНИЕ(ПланВидовХарактеристик.НастройкиПользователей.ОсновнаяОрганизация)) КАК РегНастроек"
		;
		Запрос.УстановитьПараметр("ТекДата", ТекущаяДатаСеанса());
		Выборка = Запрос.Выполнить().Выбрать();
		Если Выборка.Следующий() И ЗначениеЗаполнено(Выборка.ЗначениеНастройки) Тогда
			ОсновнаяОрганизация = Выборка.ЗначениеНастройки;
			НаименованиеПолное = СокрЛП(ОсновнаяОрганизация.НаименованиеПолное);
			Если ЗначениеЗаполнено(НаименованиеПолное) Тогда
				ПараметрыОбласти.ДанныеОрганизацииСтр1 = НаименованиеПолное;
			КонецЕсли;
			// получаем контактную информацию
			ТабЗн = ОсновнаяОрганизация.КонтактнаяИнформация.Выгрузить();
			НайдСтр = ТабЗн.Найти(Справочники.ВидыКонтактнойИнформации.ЮрАдресОрганизации,"Вид");
			Если НайдСтр <> Неопределено Тогда
				ПараметрыОбласти.ДанныеОрганизацииСтр2 = НайдСтр.Представление;
				Если ЗначениеЗаполнено(ОсновнаяОрганизация.ИНН) Тогда
					ПараметрыОбласти.ДанныеОрганизацииСтр2 = ПараметрыОбласти.ДанныеОрганизацииСтр2 + "; ";
				КонецЕсли;
			КонецЕсли;
			
			Если ЗначениеЗаполнено(ОсновнаяОрганизация.ИНН) Тогда
				ПараметрыОбласти.ДанныеОрганизацииСтр2 = ПараметрыОбласти.ДанныеОрганизацииСтр2 + 
					ОсновнаяОрганизация.ИНН + ";"
				;
			КонецЕсли;
			
			Если ЗначениеЗаполнено(ПараметрыОбласти.ДанныеОрганизацииСтр2) И 
				ЗначениеЗаполнено(ПараметрыОбласти.ДанныеОрганизацииСтр1) Тогда 
				ПараметрыОбласти.ДанныеОрганизацииСтр1 = ПараметрыОбласти.ДанныеОрганизацииСтр1 + ";";
			КонецЕсли;
			
			// телефон организации
			НайдСтр = ТабЗн.Найти(Справочники.ВидыКонтактнойИнформации.ТелефонОрганизации,"Вид");
			Если НайдСтр <> Неопределено Тогда
				ПараметрыОбласти.НомерТелефона = НайдСтр.Представление;
			КонецЕсли;

		КонецЕсли;
		
		// сотрудник, выдавший справку
		Запрос = Новый Запрос;
		Запрос.Текст =
			"ВЫБРАТЬ
			|	Рег.Сотрудник.ФизЛицо.Наименование КАК ФИО,
			|	Рег.Сотрудник.Должность КАК Должность
			|ИЗ
			|	РегистрСведений.СотрудникиПользователя КАК Рег
			|ГДЕ
			|	Рег.Пользователь = &ТекПользователь";
		
		Запрос.УстановитьПараметр("ТекПользователь", Пользователи.ТекущийПользователь());
		Выборка = Запрос.Выполнить().Выбрать();
		Если Выборка.Следующий() //И Выборка.Количество() = 1 
		Тогда
			СтрВыдалСправку = "" + Выборка.Должность + 
				?(ЗначениеЗаполнено(Выборка.Должность), " ","") + 
				Выборка.ФИО
			;
			ПараметрыОбласти.ВыдавшийСправку = СтрВыдалСправку;
		КонецЕсли;
		
		// Заполнение кода услуги в макете;
		Услуга_ = ?(Эл.МедицинскиеУслуги.Количество(), Эл.МедицинскиеУслуги[0], Неопределено);
		Артикул_ = ?(ЗначениеЗаполнено(Услуга_), Услуга_.Номенклатура.Артикул, "");
		ПараметрыОбласти.Вставить("КодУслуги", Артикул_);
		
		ОбластьМакета.Параметры.Заполнить(ПараметрыОбласти);
		
		ТабличныйДокумент.Вывести(ОбластьМакета);
		
		// В табличном документе необходимо задать имя области, в которую был
		// выведен объект. Нужно для возможности печати покомплектно.
		УправлениеПечатью.ЗадатьОбластьПечатиДокумента(ТабличныйДокумент,
			НомерСтрокиНачало, ОбъектыПечати, Эл.Ссылка
		);
		
	КонецЦикла;
	
	Возврат ТабличныйДокумент;
	
КонецФункции

#КонецЕсли

////
 // Функция: ЕстьЕщеСправкиПоДокументу
 //   Проверяет наличие справок, оформленных на основании документа, переданного в первом параметре.
 //
 // Параметры:
 //   Основание {ДокументСсылка.ЧекККМ, ДокументСсылка.ОперацияПоПлатежнойКарте, ДокументСсылка.ПоступлениеБезналичныхДенежныхСредств}
 //     Документ, для которого нужно проверить были ли по нему оформлены справки.
 //
 // Возврат: {Булево}
 //   Результат поиска: Истина - нашли, Ложь - не нашли.
 ///
Функция ЕстьЕщеСправкиПоДокументу(Основание, Справка = Неопределено) Экспорт
	Нашли = Ложь;
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	Справка.Ссылка
	|ИЗ
	|	Документ.СправкаДляНалоговогоСоциальногоВычета КАК Справка
	|ГДЕ
	|	Справка.Основание = &Основание";
	Запрос.УстановитьПараметр("Основание", Основание);
	Нашли = Не Запрос.Выполнить().Пустой();
	Возврат Нашли;
КонецФункции

 // Функция: ВсеУслугиЧекаВыполены
 //   Проверяет наличие невыполненных услуг в чеке ККМ.
 //
 // Параметры:
 //   Основание {ДокументСсылка.ЧекККМ}
 //     Документ, для которого нужно проверить есть ли в нем невыполненные услуги.
 //
 // Возврат: {Булево}
 //   Истина - все услуги имеют стутус "Выполнена", Ложь - есть невыполненные услуги.
 ///
Функция ВсеУслугиВыполены(МедицинскиеУслуги) Экспорт
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	РегистрСтатусы.УникальныйИдентификаторУслуги
	|ИЗ
	|	РегистрСведений.СтатусыУслуг КАК РегистрСтатусы
	|ГДЕ
	|	РегистрСтатусы.УникальныйИдентификаторУслуги В(&Массив)
	|	И РегистрСтатусы.СтатусУслуги <> ЗНАЧЕНИЕ(Перечисление.СтатусыУслуг.Выполнена)"
	;
	Запрос.УстановитьПараметр("Массив", МедицинскиеУслуги.ВыгрузитьКолонку("УникальныйИдентификаторУслуги"));
	Если Не Запрос.Выполнить().Пустой() Тогда
		Возврат Ложь;
	КонецЕсли;
	Возврат Истина;
КонецФункции

#КонецОбласти