﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Удаление движений происходит в ручном режиме, так как используется Записывать(Ложь)
Процедура ОбработкаПроведения(Отказ, Режим)
	ДвиженияПоПредставителю();
КонецПроцедуры

Процедура ДвиженияПоПредставителю()
	Перем УИД;
	Врем = ЭтотОбъект.Представители.Выгрузить();
	Врем.Очистить();
	СтрукПоиска = Новый Структура ("Представитель");
	СтрукПоискаУИД = Новый Структура("УИД");
	
	// Поиск представителей в БД
	// Поиск представителей в БД
	Спис = Новый СписокЗначений;
	стрГде = "(";
	Для каждого стр Из ЭтотОбъект.Представители Цикл
		Спис.Добавить(стр.УИД);
		стрГде = стрГде + 
			"ИЛИ УИД = &УИД" + Строка(Спис.Количество()) + " ";
	КонецЦикла;
	стрГде = СтрЗаменить(стрГде,"(ИЛИ","(");
	стрГде = стрГде + ")";
	
	Запрос = Новый Запрос(
		"ВЫБРАТЬ
		|	ЗаконныеПредставителиПациентаСрезПоследних.УИД,
		|	ЗаконныеПредставителиПациентаСрезПоследних.Представитель,
		|	ЗаконныеПредставителиПациентаСрезПоследних.СпециальныйПредставитель
		|ПОМЕСТИТЬ ВТ_Представители
		|ИЗ
		|	РегистрСведений.ЗаконныеПредставителиПациента.СрезПоследних(&Дата, Пациент = &Пациент И &УИД) КАК ЗаконныеПредставителиПациентаСрезПоследних
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВТ_Представители.УИД,
		|	ВТ_Представители.Представитель
		|ИЗ
		|	ВТ_Представители КАК ВТ_Представители
		|ГДЕ
		|	ВТ_Представители.СпециальныйПредставитель <> ЗНАЧЕНИЕ(Перечисление.СпециальныеЗаконныеПредставители.ПустаяСсылка)");

	Запрос.Текст = СтрЗаменить(Запрос.Текст, "&УИД", стрГде);
	
	Для Сч = 1 По Спис.Количество() Цикл
		Запрос.УстановитьПараметр("УИД" + Строка(Сч), Спис[Сч-1].Значение);
	КонецЦикла;  
	
	Запрос.УстановитьПараметр("Пациент", ЭтотОбъект.Пациент);
	Запрос.УстановитьПараметр("Дата", ТекущаяДатаСеанса());
	Запрос.УстановитьПараметр("СписокУИД", Спис);
	
	Рез = Запрос.Выполнить().Выгрузить();
	
	Движения.ЗаконныеПредставителиПациента.Записывать = Истина;
	
	Для Каждого стр из ЭтотОбъект.Представители Цикл
		// Добавление обычного представителя
		Если стр.СпециальныйПредставитель = Перечисления.СпециальныеЗаконныеПредставители.УказанРегистратором Тогда
			ЗаполнитьЗначенияСвойств(СтрукПоиска,стр);
			Если Врем.НайтиСтроки(СтрукПоиска).Количество() = 0 Тогда
				Если Рез.НайтиСтроки(СтрукПоиска).Количество() = 0 Тогда
					// Новый представитель
					УИД = стр.УИД;
				Иначе
					// Активный представитель
					// Удаляем действия активного представителя
					Движение = Движения.ЗаконныеПредставителиПациента.Добавить();
					Движение.Период = Дата;
					Движение.УИД = стр.УИД;
					Движение.Пациент = Пациент;
					Движение.Представитель = Справочники.Картотека.ПустаяСсылка();
					Движение.СпециальныйПредставитель = Перечисления.СпециальныеЗаконныеПредставители.ПустаяСсылка();
					Движение.ПримечаниеПредставителя = "";
					Движение.Основной = Ложь;
					УИД = Новый УникальныйИдентификатор;
				КонецЕсли;
				
				Если стр.СрокДействия = Дата (1,1,1) И НЕ стр.БезСрокаДействия  Тогда
					// Дополнительно ничего проводить нет необходимости. 
					// Это удаление представителя
					Продолжить;
				КонецЕсли;
				
				// Начало действия представителя.
				Движение = Движения.ЗаконныеПредставителиПациента.Добавить();
				Движение.Период = Дата;
				Движение.Пациент = Пациент;
				Движение.УИД = УИД;
				Движение.Представитель = стр.Представитель;
				Движение.СпециальныйПредставитель = стр.СпециальныйПредставитель;
				Движение.ПримечаниеПредставителя = стр.ПримечаниеПредставителя;
				Движение.Основной = стр.Основной;
				стрНовая = Врем.Добавить();
				ЗаполнитьЗначенияСвойств(стрНовая,стр);
							
				// Окончание действия представителя.
				Если стр.БезСрокаДействия Тогда
					Продолжить; //Удаление представителя отменяется
				КонецЕсли;
				
				Движение = Движения.ЗаконныеПредставителиПациента.Добавить();
				Движение.Период = стр.СрокДействия;
				Движение.Пациент = Пациент;
				Движение.УИД = УИД;
				Движение.Представитель = Справочники.Картотека.ПустаяСсылка();
				Движение.СпециальныйПредставитель = Перечисления.СпециальныеЗаконныеПредставители.ПустаяСсылка();
				Движение.ПримечаниеПредставителя = "";
				Движение.Основной = Ложь;
			КонецЕсли;
		
		// Добавление Специального представителя
		Иначе
			// Начало действия представителя.
			Движение = Движения.ЗаконныеПредставителиПациента.Добавить();
			Движение.Период = Дата;
			Движение.Пациент = Пациент;
			Движение.УИД = стр.УИД;
			Движение.Представитель = Справочники.Картотека.ПустаяСсылка();
			Движение.СпециальныйПредставитель = ?(стр.СрокДействия = Дата (1,1,1),
				Перечисления.СпециальныеЗаконныеПредставители.ПустаяСсылка(), стр.СпециальныйПредставитель);
			Движение.ПримечаниеПредставителя = стр.ПримечаниеПредставителя;
			Движение.Основной = стр.Основной;
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	ДокументОснование = ДанныеЗаполнения;
КонецПроцедуры

#КонецОбласти

#КонецЕсли