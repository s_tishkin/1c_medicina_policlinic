﻿
#Область ПрограммныйИнтерфейс


&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	Если ПараметрКоманды = Неопределено Тогда
		Сообщение_ = Новый СообщениеПользователю;
		Сообщение_.Текст = "Не выбран листок нетрудоспособности";
		Сообщение_.Сообщить();
		Возврат;
	КонецЕсли;
	
	// получим список полей для печати
	ДополнительныеПараметры = Новый Структура("ПараметрКоманды, Источник", ПараметрКоманды, ПараметрыВыполненияКоманды.Источник);
	ОписаниеОповещения_ = Новый ОписаниеОповещения("ВыборПолейПечатиЗавершение", ЭтотОбъект, ДополнительныеПараметры);
	Режим = РежимОткрытияОкнаФормы.БлокироватьВесьИнтерфейс;
	ПараметрыОткрытия = Новый Структура("ПараметрСсылкаЛН", ПараметрКоманды);
	ОткрытьФорму("Документ.ЛистокНетрудоспособности.Форма.ФормаВыбораПолейПечати",
				ПараметрыОткрытия,
				ПараметрыВыполненияКоманды.Источник,
				,,,
				ОписаниеОповещения_, Режим
			);
	

КонецПроцедуры

// Процедура: ВыборПолейПечатиЗавершение
//   обработчик завершения выбора полей печати.
//
// Параметры:
//   РезультатОткрытия - список значений, содержащий значения полей печати.
//
// Возвращаемое значение:
//   Параметры - структура с дополнительными параметрами.
//
&НаКлиенте
Процедура ВыборПолейПечатиЗавершение(РезультатОткрытия, Параметры) Экспорт
	
	Если ТипЗнч(РезультатОткрытия) = Тип("СписокЗначений") И РезультатОткрытия.Количество() > 0 Тогда
		ПараметрыПечати = Новый Структура("СтрокиПечати", РезультатОткрытия.ВыгрузитьЗначения());
		// Печать множественная и обработка идет множественная, сделаем имитацию.
		МассивОбъектов = Новый Массив();
		МассивОбъектов.Добавить(Параметры.ПараметрКоманды);
		УправлениеПечатьюКлиент.ВыполнитьКомандуПечати(
				"Документ.ЛистокНетрудоспособности",
				"ПФ_MXL_ЛистокНетрудоспособности", 
				МассивОбъектов, 
				Параметры.Источник,
				ПараметрыПечати);
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти