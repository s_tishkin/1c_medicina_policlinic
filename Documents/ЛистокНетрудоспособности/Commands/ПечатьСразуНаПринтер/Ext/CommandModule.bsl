﻿
&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	УправлениеПечатьюКлиент.ВыполнитьКомандуПечатиНаПринтер(
		"Документ.ЛистокНетрудоспособности",
		"ПФ_MXL_ЛистокНетрудоспособности", 
		ПараметрКоманды
	);
	
КонецПроцедуры
