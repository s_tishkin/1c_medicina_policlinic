﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура Форма_ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// МедицинаПоликлиника
	Форма_ПриСозданииНаСервереПоликлиника(Отказ, СтандартнаяОбработка);
	// Конец МедицинаПоликлиника
	
	// ПодключаемоеОборудование
	ПодсистемаПодключаемоеОборудование.НастроитьПодключаемоеОборудование(ЭтотОбъект);
	// Конец ПодключаемоеОборудование
	
	// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
	
	// Электронные больничные
	УстановитьВидимостьЭлементов();
	// Конец Электронные больничные
	
КонецПроцедуры

&НаКлиенте
Процедура Форма_ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	// ПодключаемоеОборудование
	Если МенеджерОборудованияКлиентПоликлиника.ОповещениеСканераШтрихкода(ЭтотОбъект, ИмяСобытия, Параметр, Источник) Тогда
		РезультатПоиска = Ложь;
		ОбъектПоиска = Строка(Тип("ДокументСсылка.ЛистокНетрудоспособности"));
		СсылкаНаОбъект_ = Неопределено;
		Штрихкод_ = МенеджерОборудованияКлиентПоликлиника.ПолучитьШтрихкодПоОповещению(Параметр);
		Если Не Штрихкод_ = Неопределено Тогда
			ЭтотОбъект.Активизировать();
			Штрихкод_ = СтрЗаменить(Штрихкод_," ","");
			Если ТипЗнч(Штрихкод_) = Тип("Строка") И СтрДлина(Штрихкод_) = 12 Тогда
				Попытка
					Штрихкод_ = Число(Штрихкод_);
				Исключение
					Возврат;
				КонецПопытки;
				СсылкаНаОбъект_ = ПолучитьЛНПоНомеру(Штрихкод_);
				Если СсылкаНаОбъект_ <> Неопределено Тогда
					РезультатПоиска = Истина;;
					ПоказатьЗначение(, СсылкаНаОбъект_);
				КонецЕсли;
			КонецЕсли;
		КонецЕсли;
		ШтрихкодированиеПереопределяемый.ЗаписатьИОбновитьВЖурналеРегистрацииИнформациюОСканировании(
															Штрихкод_,
															ОбъектПоиска,
															СсылкаНаОбъект_,
															РезультатПоиска
														);
	КонецЕсли;
	
	// Электронные больничные
	Если ИмяСобытия = "ПриИзмененииФОИспользоватьОбменСФСС" Тогда
		УстановитьВидимостьЭлементов(); 
	КонецЕсли; 
	// конец Электронные больничные
КонецПроцедуры

&НаКлиенте
Процедура Форма_ПриОткрытии(Отказ)
	
	// ПодключаемоеОборудование
	МенеджерОборудованияКлиентПоликлиника.НачатьПодключениеОборудованиеПриОткрытииФормы(ЭтотОбъект, "СканерШтрихкода");
	// Конец ПодключаемоеОборудование
	
КонецПроцедуры

&НаКлиенте
Процедура Форма_ПриЗакрытии(ЗавершениеРаботы)
	
	Если ЗавершениеРаботы Тогда
		Возврат;
	КонецЕсли;
	
	// ПодключаемоеОборудование
	МенеджерОборудованияКлиентПоликлиника.НачатьОтключениеОборудованиеПриЗакрытииФормы(ЭтотОбъект);
	// Конец ПодключаемоеОборудование
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура Список_ПриАктивизацииСтроки(Элемент)
	ТекСтр = Элементы.Список.ТекущиеДанные;
	Если ТекСтр <> Неопределено И ТекСтр.Свойство("Ссылка") Тогда
		ПодчиненныйСписок.Параметры.УстановитьЗначениеПараметра("ЛН", ТекСтр.Ссылка);
	иначе 
		ПодчиненныйСписок.Параметры.УстановитьЗначениеПараметра("ЛН", ДокументПустаяСсылка);
	КонецЕсли;
	
	// МедицинаПоликлиника
	Список_ПриАктивизацииСтрокиПоликлиника(Элемент);
	// Конец МедицинаПоликлиника
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура УстановитьСтатусПодготовка_Выполнить(Команда)
	
	УстановитьСтатус_Выполнить("Подготовка");
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьСтатусВыдан_Выполнить(Команда)
	
	УстановитьСтатус_Выполнить("Выдан");
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьСтатусИспорчен_Выполнить(Команда)
	
	УстановитьСтатус_Выполнить("Испорчен");
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьСтатусЗакрыт_Выполнить(Команда)
	
	УстановитьСтатус_Выполнить("Закрыт");
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьСтатусУтерян_Выполнить(Команда)
	
	УстановитьСтатус_Выполнить("Утерян");
	
КонецПроцедуры

&НаКлиенте
Процедура УстановитьСтатус_Выполнить(Статус)

	Если ТипЗнч(ЭтотОбъект.ТекущийЭлемент) <> Тип("ТаблицаФормы") Тогда
		Возврат;
	КонецЕсли;
	ЭлементСписок = ?(ЭтотОбъект.ТекущийЭлемент.Имя = "ПодчиненныйСписок", 
		Элементы.ПодчиненныйСписок, 
		Элементы.Список
	);
	КоличествоОбработанных = УстановитьСтатусЛН(ЭлементСписок.ВыделенныеСтроки, Статус);
	ОповеститьПользователяОбУстановкеСтатуса(КоличествоОбработанных, ЭлементСписок, Статус);

КонецПроцедуры

////
 // Процедура: ПоискПоРеквизитам
 //   Обработчик нажатия кнопки поиска по реквизитам.
 //   Выполняет поиск документа по реквизитам.
 //
 // Параметры:
 //   Команда
 //     Команда, которая инициировала событие.
 ///
&НаКлиенте
Процедура ПоискПоРеквизитам(Команда)
	
	ОписаниеОповещения_ = Новый ОписаниеОповещения("ПоискЛисткаПоРеквизитамЗавершение", ЭтотОбъект);
	Режим = РежимОткрытияОкнаФормы.БлокироватьВесьИнтерфейс;
	ОткрытьФорму("Документ.ЛистокНетрудоспособности.Форма.ФормаПоискаЛистка",
								, ЭтотОбъект,,,, ОписаниеОповещения_, Режим);
	
КонецПроцедуры

&НаКлиенте
Процедура ПоискПоШтрихкоду(Команда)
	
	ОткрытьФорму("ОбщаяФорма.УпрПоискПоШтрихкоду",, ЭтотОбъект,,,,, РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	
КонецПроцедуры

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтотОбъект, Элементы.Список);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

#КонецОбласти

#Область ВспомогательныеПроцедурыИФункции

/////////////////////////////////////////////////////////////////////////
// Вспомогательные процедуры и функции

&НаКлиенте
Процедура ПоискЛисткаПоРеквизитамЗавершение(Результат, Параметры) Экспорт
	
	Если ТипЗнч(Результат) = Тип("Структура") Тогда
		// поиск
		РезультатыПоиска = ПоискЛисткаНаСервере(Результат);
		Если ЗначениеЗаполнено(РезультатыПоиска.ПервичныйЛисток) Тогда
			Элементы.Список.ТекущаяСтрока = РезультатыПоиска.ПервичныйЛисток;
			Элементы.ПодчиненныйСписок.ТекущаяСтрока = РезультатыПоиска.НайденныйЛисток;
		Иначе
			СообщенияПользователюКлиент.ВывестиПредупреждение("Больничные_ДокументНеНайден");
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция УстановитьСтатусЛН(Знач ЛисткиНетрудоспособности, Знач СтрокаСтатуса)

	Статус = Неопределено;
	
	Если СтрокаСтатуса = "Подготовка" Тогда
		Статус = Перечисления.СтатусыЛистковНетрудоспособности.Подготовка;
	ИначеЕсли СтрокаСтатуса = "Выдан" Тогда
		Статус = Перечисления.СтатусыЛистковНетрудоспособности.Выдан;
	ИначеЕсли СтрокаСтатуса = "Испорчен" Тогда
		Статус = Перечисления.СтатусыЛистковНетрудоспособности.Испорчен;
	ИначеЕсли СтрокаСтатуса = "Утерян" Тогда
		Статус = Перечисления.СтатусыЛистковНетрудоспособности.Утерян;
	ИначеЕсли СтрокаСтатуса = "Закрыт" Тогда
		Статус = Перечисления.СтатусыЛистковНетрудоспособности.Закрыт;
	КонецЕсли; 

	Возврат Документы.ЛистокНетрудоспособности.УстановитьСтатус(ЛисткиНетрудоспособности, Статус);

КонецФункции

// Процедура показывает оповещение после обработки выделенных в списке строк.
//
// Параметры
// КоличествоОбработанных - Число - количество успешно обработанных соглашений с клиентами.
// СписокДокументов       - ДинамическийСписок - Элемент формы
// Статус                 - Установленный статус
//
&НаКлиенте
Процедура ОповеститьПользователяОбУстановкеСтатуса(КоличествоОбработанных, СписокДокументов, Статус)
	
	Если КоличествоОбработанных > 0 Тогда
		
		Если ТипЗнч(СписокДокументов) <> Тип("Структура") Тогда
			СписокДокументов.Обновить();
		КонецЕсли;
		
		ТекстСообщения = СообщенияПользователю.Получить(
							"Больничные_СтатусУстановлен", 
							Новый Структура("КоличествоОбработанных,КоличествоВсего,Статус",
											 КоличествоОбработанных, СписокДокументов.ВыделенныеСтроки.Количество(), Статус)
						);
		ТекстЗаголовка = СообщенияПользователю.Получить(
							"Больничные_ЗаголовокСтатусУстановлен", 
							Новый Структура("Статус", Статус)
						);
		ПоказатьОповещениеПользователя(ТекстЗаголовка,, ТекстСообщения, БиблиотекаКартинок.Информация32);
		
	Иначе
		
		ТекстСообщения = СообщенияПользователю.Получить("Больничные_СтатусНеУстановлен", 
							Новый Структура("Статус", Статус)
						);
		ТекстЗаголовка = СообщенияПользователю.Получить("Больничные_ЗаголовокСтатусНеУстановлен", 
							Новый Структура("Статус", Статус)
						);
		ПоказатьОповещениеПользователя(ТекстЗаголовка,, ТекстСообщения, БиблиотекаКартинок.Информация32);
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ПолучитьЛНПоНомеру(Штрихкод_)
	Запрос = Новый Запрос;
	Запрос.Текст =
	 "ВЫБРАТЬ
	 |	ЛН.Ссылка
	 |ИЗ
	 |	Документ.ЛистокНетрудоспособности КАК ЛН
	 |ГДЕ
	 |	ЛН.Номер = &Шаблон"
	;
	Запрос.УстановитьПараметр("Шаблон", Штрихкод_);
	Выборка = Запрос.Выполнить().Выбрать();
	Возврат ?(Выборка.Следующий(), Выборка.Ссылка, Неопределено);
КонецФункции

////
 // Процедура: ПоискЛисткаНаСервере
 //   Ищет листок по параметрам поиска.
 //
 // Параметры:
 //   ПараметрыПоиска {Структура}
 //     Параметры поиска.
 ///
&НаСервереБезКонтекста
Функция ПоискЛисткаНаСервере(ПараметрыПоиска)
	РезультатыПоиска = Новый Структура("ПервичныйЛисток, НайденныйЛисток");	
	Запрос = Новый Запрос;
	ТекстЗапроса =
	 "ВЫБРАТЬ
	 |	ЛистокНетрудоспособности.Ссылка КАК НайденныйЛисток,
	 |	ЛистокНетрудоспособности.ПервичныйЛисток
	 |ИЗ
	 |	Документ.ЛистокНетрудоспособности КАК ЛистокНетрудоспособности
	 |ГДЕ
	 |	ВЫБОР
	 |			КОГДА ТИПЗНАЧЕНИЯ(ЛистокНетрудоспособности.ПервичныйЛисток) = ТИП(Документ.ЛистокНетрудоспособности)
	 |				ТОГДА ЛистокНетрудоспособности.ПервичныйЛисток <> ЗНАЧЕНИЕ(Документ.ЛистокНетрудоспособности.ПустаяСсылка)
	 |			КОГДА ТИПЗНАЧЕНИЯ(ЛистокНетрудоспособности.ПервичныйЛисток) = ТИП(ЧИСЛО)
	 |				ТОГДА ЛистокНетрудоспособности.ПервичныйЛисток <> 0
	 |			ИНАЧЕ ЛОЖЬ
	 |		КОНЕЦ
	 |	И (&_Условие_)
	 |
	 |УПОРЯДОЧИТЬ ПО
	 |	ЛистокНетрудоспособности.Номер"
	;
	УсловиеСтр = "";
	Счетчик = 1;
	ПараметрыЗапроса = Новый Структура;
	Для Каждого ПараметрПоиска Из ПараметрыПоиска Цикл
		Если Не ЗначениеЗаполнено(ПараметрПоиска.Значение) Тогда
			Продолжить;
		КонецЕсли;
		Если СтрДлина(УсловиеСтр) > 0 Тогда
			УсловиеСтр = УсловиеСтр + " И ";
		КонецЕсли;
		Если ПараметрПоиска.Ключ = "Фамилия" ИЛИ ПараметрПоиска.Ключ = "Имя" ИЛИ ПараметрПоиска.Ключ = "Отчество" Тогда
			ОперандСравнения = "Подобно";
		Иначе 
			ОперандСравнения = "=";
		КонецЕсли;
		НазваниеПараметра = "Параметр" + Счетчик;
		УсловиеСтр = УсловиеСтр + ПараметрПоиска.Ключ + " " + ОперандСравнения + " &" + НазваниеПараметра;
		ПараметрыЗапроса.Вставить(НазваниеПараметра);
		ПараметрыЗапроса[НазваниеПараметра] = ПараметрПоиска.Значение;
		Счетчик = Счетчик + 1;
	КонецЦикла;
	Если Не ЗначениеЗаполнено(УсловиеСтр) Тогда 
		Возврат РезультатыПоиска;
	КонецЕсли;
	
	ТекстЗапроса = СтрЗаменить(ТекстЗапроса, "&_Условие_", УсловиеСтр);
	Запрос.Текст = ТекстЗапроса;
	// заполняем параметры запроса
	Для Каждого ПараметрЗапроса Из ПараметрыЗапроса Цикл
		Запрос.УстановитьПараметр(ПараметрЗапроса.Ключ, ПараметрЗапроса.Значение);
	КонецЦикла;
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда
		ЗаполнитьЗначенияСвойств(РезультатыПоиска, Выборка);
	КонецЕсли;
	
	Возврат РезультатыПоиска;
КонецФункции

// Электронные больничные
&НаСервере
Процедура УстановитьВидимостьЭлементов()

	ИспользоватьОбменСФСС_ = Константы.ИспользоватьОбменСФСС.Получить();
	
	Элементы.КартинкаЭлектронныйБольничный.Видимость = ИспользоватьОбменСФСС_;
	Элементы.ПодчиненныйСписокКартинкаЭлектронныйБольничный.Видимость = ИспользоватьОбменСФСС_;
	
	Элементы.СтатусОтправкиВФСС.Видимость = ИспользоватьОбменСФСС_;
	Элементы.ПодчиненныйСписокСтатусОтправкиВФСС.Видимость = ИспользоватьОбменСФСС_;

КонецПроцедуры
// Конец Электронные больничные

#КонецОбласти

#Область МедицинаПоликлиника

&НаКлиенте
Процедура Список_ПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа)
	Если Не Копирование Тогда
		Отказ = Истина;
		ОткрытьФорму("Документ.ЛистокНетрудоспособности.ФормаОбъекта",
			Новый Структура("МедицинскаяКарта, МедицинскийДокумент,ОтборПоКарте",
				МедицинскаяКарта, МедицинскийДокумент, ЭтотОбъект.ОтборПоКарте)
		);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура Создать(Команда)
	
	ОткрытьФормуИдентификацииПациента();
	
КонецПроцедуры

&НаКлиенте
Процедура СоздатьЛистокНетрудоспособностиПоУходу(Команда)
	
	Элементы.ФормаСоздатьЛистокНетрудоспособностиПоУходу.Пометка = Истина;
	ОткрытьФормуИдентификацииПациента(Истина);
	
КонецПроцедуры

// Открывает форму идентификации пациента.
//
&НаКлиенте
Процедура ОткрытьФормуИдентификацииПациента(ПоУходу = Ложь)

	Если Не ЗначениеЗаполнено(ЭтотОбъект.МедицинскаяКартаСеанса) Тогда
	
		Оповещение_ = Новый ОписаниеОповещения("ОбработкаВыбораПациентаКарты", ЭтотОбъект);
		РегистратураКлиент.ВыбратьПациентаКарту(Оповещение_, ЭтотОбъект, Ложь, Истина);
	
	Иначе
		
		Если Не ПоУходу Тогда
			ПараметрыОткрытия_ = Новый Структура(
				"МедицинскаяКарта, МедицинскийДокумент",
				ЭтотОбъект.МедицинскаяКарта, ЭтотОбъект.МедицинскийДокумент
			);
			ОткрытьФорму("Документ.ЛистокНетрудоспособности.ФормаОбъекта", ПараметрыОткрытия_);
		Иначе
			ДополнительныеПараметры_ = Новый Структура("ПоУходу", ПоУходу);
			ОписаниеОповещения_ = Новый ОписаниеОповещения("ПослеВыбораВФормеВыбораЗаконногоПредставителя", ЭтотОбъект);
			ПараметрыФормы_ = Новый Структура("Пациент", ЭтотОбъект.Пациент);
			ОткрытьФорму("Документ.ЛистокНетрудоспособности.Форма.ФормаВыбораЗаконногоПредставителя",
							ПараметрыФормы_,,,,, 
							ОписаниеОповещения_,
							РежимОткрытияОкнаФормы.БлокироватьОкноВладельца
						);
		КонецЕсли;
		
	КонецЕсли;

КонецПроцедуры

// Обработка выбора пациента, медицинской карты.
&НаКлиенте
Процедура ОбработкаВыбораПациентаКарты(ПациентИлиКарта, ДополнительныеПараметры) Экспорт
	Если ПациентИлиКарта <> Неопределено Тогда
		Если ТипЗнч(ПациентИлиКарта) = Тип("СправочникСсылка.МедицинскиеКарты") Тогда
			ЭтотОбъект.МедицинскаяКарта = ПациентИлиКарта;
		КонецЕсли;
		
		СоздатьЛНПоУходу_ = Элементы.ФормаСоздатьЛистокНетрудоспособностиПоУходу.Пометка;
		
		Если Не СоздатьЛНПоУходу_ = Истина Тогда
			ПараметрыОткрытия_ = Новый Структура(
				"МедицинскаяКарта, МедицинскийДокумент",
				ЭтотОбъект.МедицинскаяКарта, ЭтотОбъект.МедицинскийДокумент
			);
			ОткрытьФорму("Документ.ЛистокНетрудоспособности.ФормаОбъекта", ПараметрыОткрытия_);
		Иначе
			ОписаниеОповещения_ = Новый ОписаниеОповещения("ПослеВыбораВФормеВыбораЗаконногоПредставителя", ЭтотОбъект);
			ПараметрыФормы_ = Новый Структура("Пациент", ЭтотОбъект.Пациент);
			ОткрытьФорму("Документ.ЛистокНетрудоспособности.Форма.ФормаВыбораЗаконногоПредставителя",
							ПараметрыФормы_,,,,, 
							ОписаниеОповещения_,
							РежимОткрытияОкнаФормы.БлокироватьОкноВладельца
						);
		КонецЕсли; 
	КонецЕсли;
	Элементы.ФормаСоздатьЛистокНетрудоспособностиПоУходу.Пометка = Ложь;
КонецПроцедуры

// Обработчик завершения выбора значения из формы выбора представителей.
&НаКлиенте
Процедура ПослеВыбораВФормеВыбораЗаконногоПредставителя(Результат, ДополнительныеПараметры) Экспорт

	ВыбранныйЭлемент = Результат;
	
	ПараметрыФормы = Новый Структура();
	ПараметрыФормы.Вставить("ЛНПоУходу", Истина);
	ПараметрыФормы.Вставить("Представитель", ?(ЗначениеЗаполнено(ВыбранныйЭлемент), ВыбранныйЭлемент, Неопределено));
	ПараметрыФормы.Вставить("МедицинскаяКарта", ЭтотОбъект.МедицинскаяКарта);
	ПараметрыФормы.Вставить("МедицинскийДокумент", ЭтотОбъект.МедицинскийДокумент);
	ОткрытьФорму("Документ.ЛистокНетрудоспособности.ФормаОбъекта", ПараметрыФормы);
	
КонецПроцедуры

&НаСервере
Процедура Форма_ПриСозданииНаСервереПоликлиника(Отказ, СтандартнаяОбработка)

	ЭтотОбъект.ОтборПоКарте = ЭтотОбъект.Параметры.ОтборПоКарте;
	
	Если ЗначениеЗаполнено(Параметры.МедицинскийДокумент) Тогда
		ЭтотОбъект.МедицинскийДокумент = Параметры.МедицинскийДокумент;
		ЭтотОбъект.МедицинскаяКарта = ЭтотОбъект.МедицинскийДокумент.МедицинскаяКарта;
		ЭтотОбъект.МедицинскаяКартаСеанса = ЭтотОбъект.МедицинскаяКарта;
		ЭтотОбъект.ОтборПоКарте = Истина;
	ИначеЕсли Параметры.Свойство("Медицинскаякарта") Тогда
		ЭтотОбъект.МедицинскаяКарта = Параметры.МедицинскаяКарта;
		ЭтотОбъект.МедицинскаяКартаСеанса = Параметры.МедицинскаяКарта;
	КонецЕсли;
	Если Не ЗначениеЗаполнено(ЭтотОбъект.МедицинскаяКарта) И ЗначениеЗаполнено(ЭтотОбъект.МедицинскаяКартаСеанса) Тогда
		ЭтотОбъект.МедицинскаяКарта = ЭтотОбъект.МедицинскаяКартаСеанса;
	КонецЕсли;
	Если ЭтотОбъект.ОтборПоКарте Тогда
		Если ЗначениеЗаполнено(МедицинскаяКарта) Тогда
			ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(
				Список, "МедицинскаяКарта", МедицинскаяКарта, ЗначениеЗаполнено(МедицинскаяКарта)
			);
			Элементы.ФормаПоискПоШтрихкоду.Видимость = Ложь;
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура Список_ПриАктивизацииСтрокиПоликлиника(Элемент)
	
	ТекущиеДанные_ = Элементы.Список.ТекущиеДанные;
	
	Если ТекущиеДанные_ <> Неопределено И ТекущиеДанные_.Свойство("Ссылка") Тогда
		
		ИспользоватьОбменФССТекущаяСтрока(ТекущиеДанные_);
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ИспользоватьОбменФССТекущаяСтрока(ТекущиеДанные)
	
	ИспользоватьОбменСФСС_ = Константы.ИспользоватьОбменСФСС.Получить();
	
	ТекущиееСогласие = РегистрыСведений.СогласияНаОформлениеЭлектронныхЛистковНетрудоспособности.ДействующееСогласиеНаОформлениеЭЛН(ТекущиеДанные.Пациент,
																						ТекущиеДанные.Организация
																					);
	
	ЗначениеДоступности = (ИспользоватьОбменСФСС_ И ТекущиееСогласие <> Неопределено);
	
	МассивЭлементыФСС = Новый Массив;
	МассивЭлементыФСС.Добавить("ФормаДокументЛистокНетрудоспособностиОтправитьДанныеЛНВФСС");
	МассивЭлементыФСС.Добавить("ФормаДокументЛистокНетрудоспособностиПолучитьНомерЛНЧерезФСС");
	МассивЭлементыФСС.Добавить("ФормаДокументЛистокНетрудоспособностиПрекратитьДействиеЛН");
	МассивЭлементыФСС.Добавить("ФормаОбновитьЛНЧерезФСС");
	МассивЭлементыФСС.Добавить("ПодчиненныйСписокФСС");
	
	ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементовФормы(Элементы, МассивЭлементыФСС, "Доступность", ЗначениеДоступности);
	
КонецПроцедуры

#КонецОбласти
