﻿
#Область ПрограммныйИнтерфейс


////
 // Процедура: ПриСозданииНаСервере
 //   Обработчик события формы ПриСозданииНаСервере.
 //   Инициализируем реквизит формы.
 //
 // Параметры:
 //   Отказ 
 //     Признак отказа от создания формы.
 //   СтандартнаяОбработка.
 //     Признак выполнения системной обработки.
 ///
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	Если Не ЗначениеЗаполнено(ЭтотОбъект.ПараметрПоиска) Тогда
		ЭтотОбъект.ПараметрПоиска = "Номер листка";
	КонецЕсли;
КонецПроцедуры

////
 // Процедура: ПриОткрытии
 //   Обработчик события формы ПриОткрытии.
 //   Изменяет видимость элементов формы.
 //
 // Параметры:
 //   Отказ 
 //     Признак отказа от открытия формы.
 ///
&НаКлиенте
Процедура ПриОткрытии(Отказ)
	ПараметрПоиска_ПриИзменении(ЭтотОбъект);
КонецПроцедуры

////
 // Процедура: ПараметрПоиска_ПриИзменении
 //   Обработчик события ПриИзменении элемента "Параметр формы".
 //   Изменяет видимость элементов формы.
 //
 // Параметры:
 //   Элемент 
 //     Элемент формы, который инициировал событие.
 ///
&НаКлиенте
Процедура ПараметрПоиска_ПриИзменении(Элемент)
	ПоискПоФИО = ПараметрПоиска = "ФИО";
	Элементы.ГруппаФИО.Видимость = ПоискПоФИО;
	Элементы.ЗначениеПараметра.Видимость = Не ПоискПоФИО;
КонецПроцедуры

////
 // Процедура: КомандаОК
 //   Обработчик нажатия кнопки "Поиск".
 //   Закрывает форму и передает параметры поиска.
 //
 // Параметры:
 //   Команда
 //     Команда, которая инициировала событие.
 ///
&НаКлиенте
Процедура КомандаОК(Команда)
	Параметры_ = Новый Структура("Фамилия, Имя, Отчество, Номер, НомерЛистка");
	Если ПараметрПоиска = "ФИО" Тогда
		Параметры_.Фамилия = СокрЛП(ЭтотОбъект.Фамилия) + ?(ЗначениеЗаполнено(ЭтотОбъект.Фамилия), "%", "");
		Параметры_.Имя = СокрЛП(ЭтотОбъект.Имя) + ?(ЗначениеЗаполнено(ЭтотОбъект.Имя), "%", "");
		Параметры_.Отчество = СокрЛП(ЭтотОбъект.Отчество) + ?(ЗначениеЗаполнено(ЭтотОбъект.Отчество), "%", "");
	ИначеЕсли ПараметрПоиска = "Номер документа" Тогда
		Параметры_.Номер = Число(ПреобразоватьКЧислу(ЭтотОбъект.ЗначениеПараметра));
	ИначеЕсли ПараметрПоиска = "Номер листка" Тогда
		Параметры_.НомерЛистка = Число(ПреобразоватьКЧислу(ЭтотОбъект.ЗначениеПараметра));
	КонецЕсли; 
	Закрыть(Параметры_);
КонецПроцедуры

Функция ПреобразоватьКЧислу(Значение)

	Знач_ = 0;
	Попытка
		Знач_ = Число(Значение);
	Исключение
	КонецПопытки;
	
	Возврат Знач_

КонецФункции // ()


#КонецОбласти