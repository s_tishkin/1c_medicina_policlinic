﻿#Область ПрограммныйИнтерфейс

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Список_ = ОбменДаннымиФСС.ПолучитьСписокПричинПрекращенияДействияЭЛН();
	СписокПричин.ЗагрузитьЗначения(Список_.ВыгрузитьЗначения());
	
	Элементы.Причина.СписокВыбора.ЗагрузитьЗначения(Список_.ВыгрузитьЗначения());
	
	Причина = Список_.Получить(0);
	
КонецПроцедуры

&НаКлиенте
Процедура КомандаОК(Команда)
	
	Массив_ = СтроковыеФункцииКлиентСервер.РазложитьСтрокуВМассивПодстрок(Причина, "-",, Истина);
	
	Закрыть(Новый Структура("КодПричины, Причина", Массив_[0], Массив_[1]));
	
КонецПроцедуры

#КонецОбласти