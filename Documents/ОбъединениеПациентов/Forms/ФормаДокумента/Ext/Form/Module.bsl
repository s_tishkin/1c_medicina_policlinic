﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	УстановитьДоступностьИзмененияДанных(ТекущийОбъект);
	УстановитьДоступностьПереносаДанных(ЭтотОбъект);
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	УстановитьДоступностьИзмененияДанных(ТекущийОбъект);
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура УдаляемыйПациентПриИзменении(Элемент)
	ОчиститьОбрабатываемыеДанные();
	УстановитьДоступностьПереносаДанных(ЭтотОбъект);
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ЗаполнитьОбрабатываемыеДанные(Команда)
	ЗаполнитьОбрабатываемыеДанныеНаСервере();
КонецПроцедуры

#КонецОбласти

#Область ВспомогательныеМетоды

&НаСервере
Процедура УстановитьДоступностьИзмененияДанных(ТекущийОбъект)
	Элементы.ГруппаНомерДатаОтветственный.ТолькоПросмотр = ТекущийОбъект.Проведен;
	Элементы.Пациент.ТолькоПросмотр = ТекущийОбъект.Проведен;
	Элементы.УдаляемыйПациент.ТолькоПросмотр = ТекущийОбъект.Проведен;
	Элементы.СтраницыДанные.ТолькоПросмотр = ТекущийОбъект.Проведен;
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьОбрабатываемыеДанныеНаСервере()
	Объект_ = РеквизитФормыВЗначение("Объект");
	Объект_.ЗаполнитьОбрабатываемыеДанные();
	ЗначениеВРеквизитФормы(Объект_, "Объект");
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура УстановитьДоступностьПереносаДанных(ТекущаяФорма)
	Если ЗначениеЗаполнено(ТекущаяФорма.Объект.УдаляемыйПациент) Тогда
		Наличие_ = ПолучитьНаличиеПереносимыхДанных(
			ТекущаяФорма.Объект.УдаляемыйПациент, ТекущаяФорма.Объект.Дата
		);	
		ТекущаяФорма.Элементы.ПереноситьСтраховойНомерПФР.Доступность = Наличие_.СтраховойНомерПФР;
		ТекущаяФорма.Элементы.ПереноситьДУЛ.Доступность = Наличие_.ДУЛ;
		ТекущаяФорма.Элементы.ПереноситьПолисы.Доступность = Наличие_.Полисы;
		ТекущаяФорма.Элементы.ПереноситьИнвалидность.Доступность = Наличие_.Инвалидность;
	Иначе
		ТекущаяФорма.Элементы.ПереноситьСтраховойНомерПФР.Доступность = Ложь;
		ТекущаяФорма.Элементы.ПереноситьДУЛ.Доступность = Ложь;
		ТекущаяФорма.Элементы.ПереноситьПолисы.Доступность = Ложь;
		ТекущаяФорма.Элементы.ПереноситьИнвалидность.Доступность = Ложь;
	КонецЕсли;
КонецПроцедуры

&НаСервереБезКонтекста
Функция ПолучитьНаличиеПереносимыхДанных(Знач Пациент, Знач Дата)
	НаличиеПереносимыхДанных_ = Новый Структура("СтраховойНомерПФР, ДУЛ, Полисы, Инвалидность", Ложь, Ложь, Ложь, Ложь);
	Дата = ?(ЗначениеЗаполнено(Дата), Дата, ТекущаяДатаСеанса());
	Запрос_ = Новый Запрос(
		"ВЫБРАТЬ
		|	Картотека.СтраховойНомерПФР
		|ИЗ
		|	Справочник.Картотека КАК Картотека
		|ГДЕ
		|	Картотека.Ссылка = &Пациент
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ПаспортныеДанныеПациентовСрезПоследних.Регистратор
		|ИЗ
		|	РегистрСведений.ПаспортныеДанныеПациентов.СрезПоследних(&Дата, Пациент = &Пациент) КАК ПаспортныеДанныеПациентовСрезПоследних
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ПолисыСтрахованияПациентовСрезПоследних.Регистратор
		|ИЗ
		|	РегистрСведений.ПолисыСтрахованияПациентов.СрезПоследних(&Дата, Пациент = &Пациент) КАК ПолисыСтрахованияПациентовСрезПоследних
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ГруппыИнвалидностиПациентовСрезПоследних.Регистратор
		|ИЗ
		|	РегистрСведений.ГруппыИнвалидностиПациентов.СрезПоследних(&Дата, Пациент = &Пациент) КАК ГруппыИнвалидностиПациентовСрезПоследних"
	);
	Запрос_.УстановитьПараметр("Дата", Дата);	
	Запрос_.УстановитьПараметр("Пациент", Пациент);
	Результат_ = Запрос_.ВыполнитьПакет();
	Выборка_ = Результат_[0].Выбрать();
	Если Выборка_.Следующий() Тогда
		НаличиеПереносимыхДанных_.СтраховойНомерПФР = ЗначениеЗаполнено(СокрЛП(Выборка_.СтраховойНомерПФР));
	КонецЕсли;
	Выборка_ = Результат_[1].Выбрать();
	НаличиеПереносимыхДанных_.ДУЛ = Выборка_.Следующий();
	Выборка_ = Результат_[2].Выбрать();
	НаличиеПереносимыхДанных_.Полисы = Выборка_.Следующий();
	Выборка_ = Результат_[3].Выбрать();
	НаличиеПереносимыхДанных_.Инвалидность = Выборка_.Следующий();
	Возврат НаличиеПереносимыхДанных_;
КонецФункции

&НаКлиенте
Процедура ОчиститьОбрабатываемыеДанные()
	Объект.ПереноситьСтраховойНомерПФР = Ложь;		
	Объект.ПереноситьДУЛ = Ложь;		
	Объект.ПереноситьПолисы = Ложь;		
	Объект.ПереноситьИнвалидность = Ложь;		
	Объект.СтраховойНомерПФРПациента = Неопределено;		
	Объект.СтраховойНомерПФРУдаляемогоПациента = Неопределено;
	Объект.РегистраторДУЛ = Неопределено;
	Объект.РегистраторИнвалидности = Неопределено;
	Объект.РегистраторыПолисов.Очистить();
	Объект.ЗаписиРегистров.Очистить();
	Объект.Документы.Очистить();
	Объект.СоответствияМедицинскихКарт.Очистить();
КонецПроцедуры

#КонецОбласти
