﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если Параметры.Свойство("ТекущиеЛьготы") Тогда
		ТекущиеКатегории = ИспользуемыеКатегорииЛьгот(Параметры.ТекущиеЛьготы);
		ДоступныеЛьготы = Регистратура.ДоступныеКатегорииЛьгот(ТекущиеКатегории);
		Если Параметры.Свойство("ТекущаяСтрока") Тогда
			ТекущаяЛьготы = Параметры.ТекущиеЛьготы.Получить(Параметры.ТекущаяСтрока);
			ЗаполнитьЗначенияСвойств(ЭтотОбъект, ТекущаяЛьготы);
			Если Параметры.Свойство("Режим") и Параметры.Режим = "Изменение" Тогда
				ДоступныеЛьготы.Добавить(ТекущаяЛьготы.КатегорияЛьготы);
			КонецЕсли;
		КонецЕсли;
	Иначе
		ДоступныеЛьготы = Регистратура.ДоступныеКатегорииЛьгот();
	КонецЕсли;
	
	Элементы.КатегорияЛьготы.СписокВыбора.ЗагрузитьЗначения(ДоступныеЛьготы);
	
	КатегорияЛьготыПриИзмененииНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ОК(Команда)
	Закрыть(УпаковатьРеквизиты());
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура КатегорияЛьготыПриИзменении(Элемент)
	КатегорияЛьготыПриИзмененииНаСервере();
КонецПроцедуры

#КонецОбласти

#Область ВспомогательныеМетоды

&НаСервере
Функция УпаковатьРеквизиты()
	
	РеквизитыФормы_ = Новый Структура;
	Для каждого РеквизитФормы_ Из ЭтотОбъект.ПолучитьРеквизиты() Цикл
	
		РеквизитыФормы_.Вставить(РеквизитФормы_.Имя, ЭтотОбъект[РеквизитФормы_.Имя]);
	
	КонецЦикла;
	
	Возврат РеквизитыФормы_;
	
КонецФункции

&НаСервере
Функция ИспользуемыеКатегорииЛьгот(Льготы)
	
	Результат = Новый Массив;
	
	Для каждого Льгота Из Льготы Цикл
		Результат.Добавить(Льгота.КатегорияЛьготы);
	КонецЦикла;
	
	Возврат Результат;
	
КонецФункции

&НаСервере
Процедура КатегорияЛьготыПриИзмененииНаСервере()
	
	Элементы.ГруппаРегистр.Доступность = ЭтотОбъект.КатегорияЛьготы.ПравоВключенияВРегистрНаЛЛО;
	Элементы.ГруппаНСУ.Доступность =  ЭтотОбъект.КатегорияЛьготы.ПравоПолученияНСУ;
	
КонецПроцедуры

#КонецОбласти


