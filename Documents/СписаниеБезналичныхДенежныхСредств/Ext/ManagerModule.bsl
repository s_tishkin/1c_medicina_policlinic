﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Заполняет список команд печати.
// 
// Параметры:
//   КомандыПечати - ТаблицаЗначений - состав полей см. в функции УправлениеПечатью.СоздатьКоллекциюКомандПечати.
//
Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт

	Если ПравоДоступа("Изменение", Метаданные.Документы.СписаниеБезналичныхДенежныхСредств) Тогда
		// Платежное поручение
		КомандаПечати = КомандыПечати.Добавить();
		КомандаПечати.Идентификатор = "ПлатежноеПоручение";
		КомандаПечати.Представление = НСтр("ru = 'Платежное поручение'");
		КомандаПечати.ПроверкаПроведенияПередПечатью = Истина;
	КонецЕсли;
	
КонецПроцедуры

// Процедура формирует и выводит печатную форму документа по указанному макету.
//
Функция СформироватьПлатежноеПоручение(МассивОбъектов, ОбъектыПечати)
	
	ТабличныйДокумент = Новый ТабличныйДокумент;
	Макет = УправлениеПечатью.МакетПечатнойФормы("Документ.СписаниеБезналичныхДенежныхСредств.ПФ_MXL_ПлатежноеПоручение");

	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	БанковскиеСчета.Ссылка КАК Ссылка,
	|	БанковскиеСчета.Владелец КАК Владелец,
	|	БанковскиеСчета.НомерСчета КАК НомерСчета,
	|	ПРЕДСТАВЛЕНИЕ(БанковскиеСчета.Банк) КАК НаименованиеБанка,
	|	БанковскиеСчета.Банк.Код КАК БИК,
	|	БанковскиеСчета.Банк.КоррСчет КАК КоррСчет,
	|	БанковскиеСчета.Банк.Город КАК Город,
	|	БанковскиеСчета.ТекстКорреспондента КАК ТекстКорреспондента
	|
	|ПОМЕСТИТЬ БанковскиеСчетаКонтрагентов
	|ИЗ
	|	Справочник.БанковскиеСчетаПолучателей КАК БанковскиеСчета
	|ГДЕ
	|	БанковскиеСчета.БанкДляРасчетов = ЗНАЧЕНИЕ(Справочник.КлассификаторБанковРФ.ПустаяСсылка)
	|	И БанковскиеСчета.Ссылка В (
	|		ВЫБРАТЬ
	|			Документ.БанковскийСчетКонтрагента КАК БанковскийСчетКонтрагента
	|		ИЗ
	|			Документ.СписаниеБезналичныхДенежныхСредств КАК Документ
	|		ГДЕ
	|			Документ.Ссылка В (&МассивДокументов)
	|		)
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	БанковскиеСчета.Ссылка КАК Ссылка,
	|	БанковскиеСчета.Владелец КАК Владелец,
	|	БанковскиеСчета.Банк.КоррСчет КАК НомерСчета,
	|	ПРЕДСТАВЛЕНИЕ(БанковскиеСчета.БанкДляРасчетов) КАК НаименованиеБанка,
	|	БанковскиеСчета.БанкДляРасчетов.Код КАК БИК,
	|	БанковскиеСчета.БанкДляРасчетов.КоррСчет КАК КоррСчет,
	|	БанковскиеСчета.БанкДляРасчетов.Город КАК Город,
	|	БанковскиеСчета.ТекстКорреспондента КАК ТекстКорреспондента
	|ИЗ
	|	Справочник.БанковскиеСчетаПолучателей КАК БанковскиеСчета
	|ГДЕ
	|	БанковскиеСчета.БанкДляРасчетов <> ЗНАЧЕНИЕ(Справочник.КлассификаторБанковРФ.ПустаяСсылка)
	|	И БанковскиеСчета.Ссылка В (
	|		ВЫБРАТЬ
	|			Документ.БанковскийСчетКонтрагента КАК БанковскийСчетКонтрагента
	|		ИЗ
	|			Документ.СписаниеБезналичныхДенежныхСредств КАК Документ
	|		ГДЕ
	|			Документ.Ссылка В (&МассивДокументов)
	|		)
	|;
	|/////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	БанковскиеСчета.Ссылка КАК Ссылка,
	|	БанковскиеСчета.Владелец КАК Владелец,
	|	БанковскиеСчета.НомерСчета КАК НомерСчета,
	|	ПРЕДСТАВЛЕНИЕ(БанковскиеСчета.Банк) КАК НаименованиеБанка,
	|	БанковскиеСчета.Банк.Код КАК БИК,
	|	БанковскиеСчета.Банк.КоррСчет КАК КоррСчет,
	|	БанковскиеСчета.Банк.Город КАК Город
	|
	|ПОМЕСТИТЬ БанковскиеСчетаОрганизаций
	|ИЗ
	|	Справочник.БанковскиеСчетаОрганизаций КАК БанковскиеСчета
	|ГДЕ
	|	БанковскиеСчета.БанкДляРасчетов = ЗНАЧЕНИЕ(Справочник.КлассификаторБанковРФ.ПустаяСсылка)
	|	И БанковскиеСчета.Ссылка В (
	|		ВЫБРАТЬ
	|			Документ.БанковскийСчет КАК БанковскийСчет
	|		ИЗ
	|			Документ.СписаниеБезналичныхДенежныхСредств КАК Документ
	|		ГДЕ
	|			Документ.Ссылка В (&МассивДокументов)
	|		)
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	БанковскиеСчета.Ссылка КАК Ссылка,
	|	БанковскиеСчета.Владелец КАК Владелец,
	|	БанковскиеСчета.Банк.КоррСчет КАК НомерСчета,
	|	ПРЕДСТАВЛЕНИЕ(БанковскиеСчета.БанкДляРасчетов) КАК НаименованиеБанка,
	|	БанковскиеСчета.БанкДляРасчетов.Код КАК БИК,
	|	БанковскиеСчета.БанкДляРасчетов.КоррСчет КАК КоррСчет,
	|	БанковскиеСчета.БанкДляРасчетов.Город КАК Город
	|ИЗ
	|	Справочник.БанковскиеСчетаОрганизаций КАК БанковскиеСчета
	|ГДЕ
	|	БанковскиеСчета.БанкДляРасчетов <> ЗНАЧЕНИЕ(Справочник.КлассификаторБанковРФ.ПустаяСсылка)
	|	И БанковскиеСчета.Ссылка В (
	|		ВЫБРАТЬ
	|			Документ.БанковскийСчет КАК БанковскийСчет
	|		ИЗ
	|			Документ.СписаниеБезналичныхДенежныхСредств КАК Документ
	|		ГДЕ
	|			Документ.Ссылка В (&МассивДокументов)
	|		)
	|;
	|/////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	Документ.Ссылка КАК Ссылка,
	|	Документ.Номер КАК Номер,
	|	Документ.Дата КАК ДатаДокумента,
	|	Документ.Организация КАК Организация,
	|	Документ.Организация.Префикс КАК Префикс,
	|
	|	Документ.Организация.НаименованиеСокращенное КАК ПлательщикНаименование,
	|	ВЫРАЗИТЬ(Документ.БанковскийСчет.ТекстКорреспондента КАК Строка(1000)) КАК ТекстПлательщика,
	|	Документ.Организация.ИНН КАК ИННПлательщика,
	|	Документ.Организация.КПП КАК КПППлательщика,
	|
	|	БанковскиеСчетаКонтрагентов.Владелец.Наименование КАК ПолучательНаименование,
	|	БанковскиеСчетаКонтрагентов.Владелец.НаименованиеПолное КАК ПолучательНаименованиеПолное,
	|	ВЫРАЗИТЬ(БанковскиеСчетаКонтрагентов.ТекстКорреспондента КАК Строка(1000)) КАК ТекстПолучателя,
	|	БанковскиеСчетаКонтрагентов.Владелец КАК Получатель,
	|	БанковскиеСчетаКонтрагентов.Владелец.ИНН КАК ИННПолучателя,
	|	БанковскиеСчетаКонтрагентов.Владелец.КПП КАК КПППолучателя,
	|
	|	Документ.НазначениеПлатежа КАК НазначениеПлатежа,
	|	Документ.ВидПлатежа КАК ВидПлатежа,
	|	Документ.ОчередностьПлатежа КАК Очередность,
	|	
	|	Документ.СуммаДокумента КАК СуммаДокумента,
	|	Документ.ТипПлатежногоДокумента КАК ТипПлатежногоДокумента,
	|
	|	БанковскиеСчетаКонтрагентов.НомерСчета КАК НомерСчетаПолучателя,
	|	БанковскиеСчетаКонтрагентов.НаименованиеБанка КАК НаименованиеБанкаПолучателя,
	|	БанковскиеСчетаКонтрагентов.БИК КАК БИКБанкаПолучателя,
	|	БанковскиеСчетаКонтрагентов.КоррСчет КАК СчетБанкаПолучателя,
	|	БанковскиеСчетаКонтрагентов.Город КАК ГородБанкаПолучателя,
	|	
	|	Документ.БанковскийСчет.ВариантВыводаМесяца КАК ВариантВыводаМесяца,
	|	Документ.БанковскийСчет.ВыводитьСуммуБезКопеек КАК ВыводитьСуммуБезКопеек,
	|	Документ.БанковскийСчет.ВалютаДенежныхСредств КАК ВалютаДенежныхСредств,
	|
	|	БанковскиеСчетаОрганизаций.НомерСчета КАК НомерСчетаПлательщика,
	|	БанковскиеСчетаОрганизаций.НаименованиеБанка КАК НаименованиеБанкаПлательщика,
	|	БанковскиеСчетаОрганизаций.БИК КАК БИКБанкаПлательщика,
	|	БанковскиеСчетаОрганизаций.КоррСчет КАК СчетБанкаПлательщика,
	|	БанковскиеСчетаОрганизаций.Город КАК ГородБанкаПлательщика
	|ИЗ
	|	Документ.СписаниеБезналичныхДенежныхСредств КАК Документ
	|
	|	ЛЕВОЕ СОЕДИНЕНИЕ
	|		БанковскиеСчетаКонтрагентов КАК БанковскиеСчетаКонтрагентов
	|	ПО
	|		Документ.БанковскийСчетКонтрагента = БанковскиеСчетаКонтрагентов.Ссылка
	|
	|	ЛЕВОЕ СОЕДИНЕНИЕ
	|		БанковскиеСчетаОрганизаций КАК БанковскиеСчетаОрганизаций
	|	ПО
	|		Документ.БанковскийСчет = БанковскиеСчетаОрганизаций.Ссылка
	|ГДЕ
	|	Документ.Ссылка В (&МассивДокументов)
	|	И Документ.Проведен
	|
	|УПОРЯДОЧИТЬ ПО
	|	Номер
	|");
	Запрос.УстановитьПараметр("МассивДокументов", МассивОбъектов);
		
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		
		НомерСтрокиНачало = ТабличныйДокумент.ВысотаТаблицы + 1;
		
		Если Выборка.ТипПлатежногоДокумента <> Перечисления.ТипыПлатежныхДокументов.ПлатежноеПоручение Тогда
			Текст = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
				НСтр("ru = 'Платежное поручение не формируется для типа документа: %1'"),
				Выборка.ТипПлатежногоДокумента
			);
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
				Текст,
				Выборка.Ссылка.ПолучитьОбъект(),
				"ТипПлатежногоДокумента",
				, // ПутьКДанным
				// Отказ
			);
		Иначе
		
			ОбластьМакета = Макет.ПолучитьОбласть("Заголовок");
			ОбластьМакета.Параметры.Заполнить(Выборка);
			
			Если Прав(СокрЛП(Выборка.Номер), 3) = "000" Тогда
				Текст = НСтр("ru = 'Номер платежного поручения не может оканчиваться на ""000""!
				|(Приложение 4 к Положению Банка России ""О безналичных расчетах в Российской Федерации"" 
				|от 3 октября 2002 г. No. 2-П в ред. Указания ЦБ РФ от 03.03.2003 No. 1256-У)'");
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
					Текст,
					Выборка.Ссылка.ПолучитьОбъект(),
					"Номер"
				);
			КонецЕсли;
			
			// Заполним текст плательщика.
			Если Не ПустаяСтрока(Выборка.ТекстПлательщика) Тогда
				ТекстПлательщика = Выборка.ТекстПлательщика;
			Иначе
				ТекстПлательщика = Выборка.ПлательщикНаименование;
			КонецЕсли;
			ОбластьМакета.Параметры.ТекстПлательщика = ТекстПлательщика;
			
			Если ПустаяСтрока(ТекстПлательщика) Тогда
				Текст = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
					НСтр("ru = 'У организации %1 не заполнено поле ""Сокращенное наименование""'"),
					Выборка.Организация
					);
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
					Текст,
					Выборка.Организация.ПолучитьОбъект(),
					"НаименованиеСокращенное"
				);
			КонецЕсли;
			
			// Заполним текст получателя.
			Если Не ПустаяСтрока(Выборка.ТекстПолучателя) Тогда
				ТекстПолучателя = Выборка.ТекстПолучателя;
			ИначеЕсли ТипЗнч(Выборка.Получатель) = Тип("СправочникСсылка.ФизическиеЛица")
						ИЛИ ТипЗнч(Выборка.Получатель) = Тип("СправочникСсылка.Картотека") Тогда
				ТекстПолучателя = Выборка.ПолучательНаименование;
			Иначе
				ТекстПолучателя = Выборка.ПолучательНаименованиеПолное;
			КонецЕсли;
			ОбластьМакета.Параметры.ТекстПолучателя = ТекстПолучателя;
			
			Если ПустаяСтрока(ТекстПолучателя) И ЗначениеЗаполнено(Выборка.Получатель) Тогда
				Если ТипЗнч(Выборка.Получатель) = Тип("СправочникСсылка.Организации") Тогда
					ИмяПоля = "НаименованиеСокращенное";
					Текст = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
						НСтр("ru = 'У организации-получателя %1 не заполнено поле ""Сокращенное наименование""'"),
						Выборка.Получатель
						);
				ИначеЕсли ТипЗнч(Выборка.Получатель) = Тип("СправочникСсылка.ФизическиеЛица") Тогда
					ИмяПоля = "ФИО";
					Текст = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
						НСтр("ru = 'У физического лица %1 не заполнено поле ""ФИО""'"),
						Выборка.Получатель
						);
				Иначе
					ИмяПоля = "НаименованиеПолное";
					Текст = СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
						НСтр("ru = 'У контрагента %1 не заполнено поле ""Сокращенное наименование""'"),
						Выборка.Получатель
						);
				КонецЕсли; 
				ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
					Текст,
					Выборка.Получатель.ПолучитьОбъект(),
					ИмяПоля
				);
			КонецЕсли;
			
			ОбластьМакета.Параметры.НаименованиеБанкаПлательщика = Выборка.НаименованиеБанкаПлательщика + " " + Выборка.ГородБанкаПлательщика;
			ОбластьМакета.Параметры.НаименованиеБанкаПолучателя = Выборка.НаименованиеБанкаПолучателя + " " + Выборка.ГородБанкаПолучателя;
			
			// Заполним ИНН и КПП.
			ОбластьМакета.Параметры.ИННПлательщика = "ИНН " + ?(ПустаяСтрока(Выборка.ИННПлательщика), "0", Выборка.ИННПлательщика);
			ОбластьМакета.Параметры.КПППлательщика = "КПП " + ?(ПустаяСтрока(Выборка.КПППлательщика), "0", Выборка.КПППлательщика);
			ОбластьМакета.Параметры.ИННПолучателя = "ИНН " + ?(ПустаяСтрока(Выборка.ИННПолучателя), "0", Выборка.ИННПолучателя);
			ОбластьМакета.Параметры.КПППолучателя = "КПП " + ?(ПустаяСтрока(Выборка.КПППолучателя), "0", Выборка.КПППолучателя);
			
			НомерДокументаДляПечати = ПрефиксацияОбъектовКлиентСервер.ПолучитьНомерНаПечать(
				Выборка.Номер,
				Истина, // УдалитьПрефиксИнформационнойБазы
				Истина // УдалитьПользовательскийПрефикс
			);
			ОбластьМакета.Параметры.НаименованиеНомер = "ПЛАТЕЖНОЕ ПОРУЧЕНИЕ № " + НомерДокументаДляПечати;
			ОбластьМакета.Параметры.СуммаЧислом = ФормированиеПечатныхФорм.СуммаПлатежногоДокумента(
				Выборка.СуммаДокумента,
				Выборка.ВыводитьСуммуБезКопеек
			);
			ОбластьМакета.Параметры.СуммаПрописью = РаботаСКурсамиВалют.СформироватьСуммуПрописью(
				Выборка.СуммаДокумента,
				Выборка.ВалютаДенежныхСредств,
				Выборка.ВыводитьСуммуБезКопеек
			);
			Если Выборка.ВариантВыводаМесяца = Перечисления.ВариантыВыводаМесяцаВДатеДокумента.Прописью Тогда
				ФорматДаты = "ДФ='дд ММММ гггг'";
			Иначе
				ФорматДаты = "ДФ='дд.ММ.гггг'";
			КонецЕсли;
			ОбластьМакета.Параметры.ДатаДокумента = Формат(Выборка.ДатаДокумента, ФорматДаты);
				
			ТабличныйДокумент.Вывести(ОбластьМакета);
				
			ТабличныйДокумент.ВывестиГоризонтальныйРазделительСтраниц();

			УправлениеПечатью.ЗадатьОбластьПечатиДокумента(
				ТабличныйДокумент,
				НомерСтрокиНачало,
				ОбъектыПечати,
				Выборка.Ссылка
			);
			
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат ТабличныйДокумент;
	
КонецФункции // СформироватьПлатежноеПоручение()

// Процедура печати документа.
//
Процедура Печать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт

	Если УправлениеПечатью.НужноПечататьМакет(КоллекцияПечатныхФорм, "ПлатежноеПоручение") Тогда
		УправлениеПечатью.ВывестиТабличныйДокументВКоллекцию(
			КоллекцияПечатныхФорм,
			"ПлатежноеПоручение",
			"Платежное поручение",
		СформироватьПлатежноеПоручение(МассивОбъектов, ОбъектыПечати));
	КонецЕсли;

КонецПроцедуры // Печать()

///////////////////////////////////////////////////////////////////////////////

// Функция получает реквизиты документа для обмена с конфигурацией "Бухгалтерия предприятия".
//
// Параметры:
//	ДокументСсылка - Документ, для которого необходимо получить реквизиты.
//
// Возвращаемое значение:
//	Структура - Структура реквизитов документа.
//
Функция РеквизитыДокументаДляОбменаСБухгалтерией(ДокументСсылка) Экспорт
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ ПЕРВЫЕ 1
	|	ДанныеДокумента.СтатьяДвиженияДенежныхСредств.КорреспондирующийСчет КАК КорреспондирующийСчет,
	|	ДанныеДокумента.Ссылка.ХозяйственнаяОперация КАК ХозяйственнаяОперация
	|ИЗ
	|	Документ.СписаниеБезналичныхДенежныхСредств.РасшифровкаПлатежа КАК ДанныеДокумента
	|ГДЕ
	|	ДанныеДокумента.Ссылка = &Ссылка
	|	И ДанныеДокумента.СтатьяДвиженияДенежныхСредств.КорреспондирующийСчет <> """"
	|;
	|/////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ ПЕРВЫЕ 1
	|	ДанныеДокумента.СтатьяРасходов.КорреспондирующийСчет КАК КорреспондирующийСчет,
	|	ДанныеДокумента.СтатьяРасходов КАК СтатьяРасходов
	|ИЗ
	|	Документ.СписаниеБезналичныхДенежныхСредств.РасшифровкаПлатежа КАК ДанныеДокумента
	|ГДЕ
	|	ДанныеДокумента.Ссылка = &Ссылка
	|	И ДанныеДокумента.СтатьяРасходов.КорреспондирующийСчет <> """"
	|	И ДанныеДокумента.Ссылка.ХозяйственнаяОперация В (
	|		ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ПрочиеРасходы),
	|		ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ПеречислениеВБюджет)
	|	)
	|
	|УПОРЯДОЧИТЬ ПО
	|	ДанныеДокумента.НомерСтроки
	|");
	Запрос.УстановитьПараметр("Ссылка", ДокументСсылка);
	МассивРезультатов = Запрос.ВыполнитьПакет();
	
	Выборка = МассивРезультатов[0].Выбрать();
	Если Выборка.Следующий() Тогда
		КорреспондирующийСчет = Выборка.КорреспондирующийСчет;
		ХозяйственнаяОперация = Выборка.ХозяйственнаяОперация;
	Иначе
		КорреспондирующийСчет = "";
		ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ПустаяСсылка();
	КонецЕсли;
	
	Выборка = МассивРезультатов[1].Выбрать();
	Если Выборка.Следующий() Тогда
		КорреспондирующийСчет = Выборка.КорреспондирующийСчет;
		СтатьяРасходов = Выборка.СтатьяРасходов;
	Иначе
		СтатьяРасходов = Неопределено;
	КонецЕсли;
	
	ВидОперации = "ПрочееСписание";
	
	Если ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ПрочиеРасходы Тогда
		
		Если Найти(КорреспондирующийСчет, "70") <> 0 Тогда
			ВидОперации = "ПеречислениеЗП";	
		КонецЕсли;
	
	ИначеЕсли ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ПрочаяВыдачаДенежныхСредств Тогда
		
		Если Найти(КорреспондирующийСчет, "66") <> 0
		 ИЛИ Найти(КорреспондирующийСчет, "67") <> 0 Тогда
			ВидОперации = "РасчетыПоКредитамИЗаймам";
			
		ИначеЕсли Найти(КорреспондирующийСчет, "76") <> 0
		 ИЛИ Найти(КорреспондирующийСчет, "60") <> 0
		 ИЛИ Найти(КорреспондирующийСчет, "62") <> 0 Тогда
			ВидОперации = "ПрочиеРасчетыСКонтрагентами";
			
		ИначеЕсли Найти(КорреспондирующийСчет, "70") <> 0 Тогда
			ВидОперации = "ПеречислениеЗП";	
			
		КонецЕсли;
		
	КонецЕсли;
	
	СтруктураРеквизитов = Новый Структура("ВидОперации, Счет, СтатьяРасходов",
		ВидОперации,
		КорреспондирующийСчет,
		СтатьяРасходов
	);
	
	Возврат СтруктураРеквизитов;
		
КонецФункции // РеквизитыДокументаДляОбменаСБухгалтерией()

// Процедура формирует таблицу реквизитов, зависимых от хозяйственной операции документа.
//
Процедура ПолучитьМассивыРеквизитов(ХозяйственнаяОперация, ПеречислениеВБюджет, МассивВсехРеквизитов, МассивРеквизитовОперации) Экспорт
	
	МассивВсехРеквизитов = Новый Массив;
	МассивВсехРеквизитов.Добавить("ЗаявкаНаРасходованиеДенежныхСредств");
	МассивВсехРеквизитов.Добавить("Контрагент");
	МассивВсехРеквизитов.Добавить("Пациент");
	МассивВсехРеквизитов.Добавить("Партнер");
	МассивВсехРеквизитов.Добавить("БанковскийСчетКонтрагента");
	МассивВсехРеквизитов.Добавить("РасшифровкаПлатежа");
	МассивВсехРеквизитов.Добавить("РасшифровкаПлатежа.СтатьяДвиженияДенежныхСредств");
	МассивВсехРеквизитов.Добавить("РасшифровкаПлатежа.Заказ");
	МассивВсехРеквизитов.Добавить("РасшифровкаПлатежа.СуммаВзаиморасчетов");
	МассивВсехРеквизитов.Добавить("РасшифровкаПлатежа.ВалютаВзаиморасчетов");
	МассивВсехРеквизитов.Добавить("РасшифровкаПлатежа.СтатьяРасходов");
	МассивВсехРеквизитов.Добавить("РасшифровкаПлатежа.АналитикаРасходов");
	МассивВсехРеквизитов.Добавить("РасшифровкаПлатежа.ПериодВозникновенияРасхода");
	
	МассивРеквизитовОперации = Новый Массив;
	Если ХозяйственнаяОперация = Перечисления.ХозяйственныеОперации.ВозвратОплатыКлиенту Тогда
		
		МассивРеквизитовОперации.Добавить("ЗаявкаНаРасходованиеДенежныхСредств");
		МассивРеквизитовОперации.Добавить("Контрагент");
		МассивРеквизитовОперации.Добавить("Партнер");
		МассивРеквизитовОперации.Добавить("Пациент");
		МассивРеквизитовОперации.Добавить("БанковскийСчетКонтрагента");
		МассивРеквизитовОперации.Добавить("РасшифровкаПлатежа");
		МассивРеквизитовОперации.Добавить("РасшифровкаПлатежа.СтатьяДвиженияДенежныхСредств");
		МассивРеквизитовОперации.Добавить("РасшифровкаПлатежа.Заказ");
		МассивРеквизитовОперации.Добавить("РасшифровкаПлатежа.СуммаВзаиморасчетов");
		МассивРеквизитовОперации.Добавить("РасшифровкаПлатежа.ВалютаВзаиморасчетов");
		
	КонецЕсли;
		
КонецПроцедуры // ПолучитьМассивыРеквизитов() 

///////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ И ФУНКЦИИ ФОРМИРОВАНИЯ ТАБЛИЦ ДВИЖЕНИЙ

// Функция формирует текст запроса для таблицы денежных средств к выплате.
//
// Возвращаемое значение:
//	Строка - Текст запроса
//
Функция ТекстЗапросаТаблицаДенежныеСредстваКВыплате()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ТаблицаРасшифровкаПлатежа.НомерСтроки,
	|	ДанныеДокумента.Дата КАК Период,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход) КАК ВидДвижения,
	|	ДанныеДокумента.Организация КАК Организация,
	|	ДанныеДокумента.ЗаявкаНаРасходованиеДенежныхСредств КАК ЗаявкаНаРасходованиеДенежныхСредств,
	|	ДанныеДокумента.ХозяйственнаяОперация КАК ХозяйственнаяОперация,
	|	&АналитикаУчетаПоПартнерам КАК АналитикаУчетаПоПартнерам,
	|
	|	ТаблицаРасшифровкаПлатежа.СтатьяДвиженияДенежныхСредств КАК СтатьяДвиженияДенежныхСредств,
	|	ТаблицаРасшифровкаПлатежа.Заказ КАК Заказ,
	|	ТаблицаРасшифровкаПлатежа.СтатьяРасходов КАК СтатьяРасходов,
	|	ТаблицаРасшифровкаПлатежа.АналитикаРасходов КАК АналитикаРасходов,
	|	ВЫБОР КОГДА ТаблицаРасшифровкаПлатежа.Сумма ЕСТЬ NULL ТОГДА
	|		ДанныеДокумента.СуммаДокумента
	|	КОГДА &Валюта = &ВалютаЗаявки ТОГДА
	|		ЕСТЬNULL(ТаблицаРасшифровкаПлатежа.Сумма, ДанныеДокумента.СуммаДокумента)
	|	ИНАЧЕ
	|		ВЫРАЗИТЬ(
	|			ЕСТЬNULL(ТаблицаРасшифровкаПлатежа.Сумма, ДанныеДокумента.СуммаДокумента)
	|			* &КоэффициентПересчетаВВалютуЗаявки КАК ЧИСЛО(15,2)
	|		)
	|	КОНЕЦ КАК Сумма
	|	
	|ИЗ
	|	Документ.СписаниеБезналичныхДенежныхСредств КАК ДанныеДокумента
	|	
	|	ЛЕВОЕ СОЕДИНЕНИЕ
	|		Документ.РасходныйКассовыйОрдер.РасшифровкаПлатежа КАК ТаблицаРасшифровкаПлатежа
	|	ПО
	|		ТаблицаРасшифровкаПлатежа.Ссылка = &Ссылка
	|ГДЕ
	|	ДанныеДокумента.Ссылка = &Ссылка
	|
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки
	|	
	|;
	|/////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции // ТекстЗапросаТаблицаДенежныеСредстваКВыплате()

// Функция формирует текст запроса для таблицы расчетов с клиентами.
//
// Возвращаемое значение:
//	Строка - Текст запроса
//
Функция ТекстЗапросаТаблицаРасчетыСКлиентами()
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ТаблицаРасшифровкаПлатежа.НомерСтроки КАК НомерСтроки,
	|	&Период КАК Период,
	|	&Период КАК ДатаРегистратора,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) КАК ВидДвижения,
	|	&АналитикаУчетаПоПартнерам КАК АналитикаУчетаПоПартнерам,
	|	ВЫБОР
	|		КОГДА ТаблицаРасшифровкаПлатежа.УникальныйИдентификаторУслуги = &ПустойУИД
	|			ТОГДА ТаблицаРасшифровкаПлатежа.Заказ
	|		ИНАЧЕ ПРЕДСТАВЛЕНИЕ(ТаблицаРасшифровкаПлатежа.УникальныйИдентификаторУслуги)
	|	КОНЕЦ КАК ЗаказКлиента,
	|	ТаблицаРасшифровкаПлатежа.Ссылка КАК РасчетныйДокумент,
	|	ТаблицаРасшифровкаПлатежа.ВалютаВзаиморасчетов КАК Валюта,	
	|	&ХозяйственнаяОперация КАК ХозяйственнаяОперация,
	|	ЗНАЧЕНИЕ(Перечисление.ФормыОплаты.Безналичная) КАК ФормаОплаты,
	|	ТаблицаРасшифровкаПлатежа.СтатьяДвиженияДенежныхСредств КАК СтатьяДвиженияДенежныхСредств,
	|	
	|	- ТаблицаРасшифровкаПлатежа.СуммаВзаиморасчетов КАК Сумма,
	|	- ТаблицаРасшифровкаПлатежа.СуммаВзаиморасчетов КАК Предоплата,
	|	- ТаблицаРасшифровкаПлатежа.СуммаВзаиморасчетов КАК КОплате,
	|	ВЫРАЗИТЬ(ТаблицаРасшифровкаПлатежа.ОснованиеПлатежа КАК Документ.СчетНаОплатуКлиенту) КАК СчетНаОплату
	|ИЗ
	|	Документ.СписаниеБезналичныхДенежныхСредств.РасшифровкаПлатежа КАК ТаблицаРасшифровкаПлатежа
	|ГДЕ
	|	ТаблицаРасшифровкаПлатежа.Ссылка = &Ссылка
	|	И &ХозяйственнаяОперация В (ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ВозвратОплатыКлиенту))
	|
	|
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки
	|;
	|/////////////////////////////////////////////////////////////////////////////
	|";
	
	Возврат ТекстЗапроса;
	
КонецФункции // ТекстЗапросаТаблицаРасчетыСКлиентами()

// Инициализирует таблицы значений, содержащие данные табличных частей документа.
// Таблицы значений сохраняет в свойствах структуры "ДополнительныеСвойства".
Процедура ИнициализироватьДанныеДокумента(ДокументСсылка, СтруктураДополнительныеСвойства) Экспорт
	
	Запрос = Новый Запрос("
	|ВЫБРАТЬ
	|	ДанныеДокумента.Дата КАК Период,
	|	ДанныеДокумента.Валюта КАК Валюта,
	|	ДанныеДокумента.ЗаявкаНаРасходованиеДенежныхСредств.Валюта КАК ВалютаЗаявки,
	|	ДанныеДокумента.ХозяйственнаяОперация КАК ХозяйственнаяОперация,
	|	ДанныеДокумента.Организация КАК Организация,
	|	ДанныеДокумента.Партнер КАК Партнер,
	|	ДанныеДокумента.Контрагент КАК Контрагент
	|ИЗ
	|	Документ.СписаниеБезналичныхДенежныхСредств КАК ДанныеДокумента
	|ГДЕ
	|	ДанныеДокумента.Ссылка = &Ссылка
	|");
	Запрос.УстановитьПараметр("Ссылка", ДокументСсылка);
	Реквизиты = Запрос.Выполнить().Выбрать();
	Реквизиты.Следующий();
	
	// Определим ключ аналитики учета по партнерам.
	АналитикаУчетаПоПартнерам = РегистрыСведений.АналитикаУчетаПоПартнерам.ЗначениеКлючаАналитики(Реквизиты);
	АналитикаУчетаПоПартнерамПолучатель = Неопределено;
	
	Коэффициенты = МодульВалютногоУчета.ПолучитьКоэффициентыПересчетаВалюты(
		Реквизиты.Валюта, 
		Реквизиты.ВалютаЗаявки, 
		Реквизиты.Период
	);
	ТекстЗапроса = ТекстЗапросаТаблицаДенежныеСредстваКВыплате()
		+ ТекстЗапросаТаблицаРасчетыСКлиентами()
		;
	
	Запрос.Текст = ТекстЗапроса;
	Запрос.УстановитьПараметр("Период", Реквизиты.Период);
	Запрос.УстановитьПараметр("Валюта", Реквизиты.Валюта);
	Запрос.УстановитьПараметр("ВалютаЗаявки", Реквизиты.ВалютаЗаявки);
	Запрос.УстановитьПараметр("ХозяйственнаяОперация", Реквизиты.ХозяйственнаяОперация);
	Запрос.УстановитьПараметр("АналитикаУчетаПоПартнерам", АналитикаУчетаПоПартнерам);
	Запрос.УстановитьПараметр("КоэффициентПересчетаВВалютуЗаявки", Коэффициенты.КоэффициентПересчетаВВалютуВзаиморасчетов);
	Запрос.УстановитьПараметр("СтатьяДвиженияДенежныхСредств", ЗначениеНастроекПовтИсп.ПредопределеннаяСтатьяДвиженияДенежныхСредств(Реквизиты.ХозяйственнаяОперация));
	Запрос.УстановитьПараметр("ПустойУИД", Новый УникальныйИдентификатор("00000000-0000-0000-0000-000000000000"));

	МассивРезультатов = Запрос.ВыполнитьПакет();
	
	// МассивРезультатов[0] - ВременнаяТаблицаРасшифровкиПлатежа
	СтруктураДополнительныеСвойства.ТаблицыДляДвижений.Вставить("ТаблицаДенежныеСредстваКВыплате", МассивРезультатов[0].Выгрузить());
	СтруктураДополнительныеСвойства.ТаблицыДляДвижений.Вставить("ТаблицаРасчетыСКлиентами",        МассивРезультатов[1].Выгрузить());
	
КонецПроцедуры // ИнициализироватьДанныеДокумента()

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// СтандартныеПодсистемы.ВерсионированиеОбъектов

// Определяет настройки объекта для подсистемы ВерсионированиеОбъектов.
//
// Параметры:
//  Настройки - Структура - настройки подсистемы.
Процедура ПриОпределенииНастроекВерсионированияОбъектов(Настройки) Экспорт

КонецПроцедуры

// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов

#КонецОбласти

#КонецЕсли