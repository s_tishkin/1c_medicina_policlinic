﻿
#Область ПрограммныйИнтерфейс


&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ЭтотОбъект.Соглашение = Параметры.Соглашение;
	ЭтотОбъект.Договор = Параметры.Договор;
	ЭтотОбъект.Пациент = Параметры.Пациент;
	
	Если ЗначениеЗаполнено(ЭтотОбъект.Соглашение) Или ЗначениеЗаполнено(ЭтотОбъект.Договор) Тогда
		ОбновитьСписок();
	КонецЕсли;
	
	Если Не ЗначениеЗаполнено(ЭтотОбъект.Соглашение) Тогда
		Элементы.Соглашение.Видимость = Ложь;
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ОбновитьСписок()
	
	ЭтотОбъект.Список.Очистить();
	
	Если ЗначениеЗаполнено(ЭтотОбъект.Соглашение) Тогда
		ТЗ_ = КвотыПолныеПрава.ПолучитьСостояниеКвотПоСоглашению(ЭтотОбъект.Соглашение, ЭтотОбъект.Пациент);
	КонецЕсли;

	Если ЗначениеЗаполнено(ЭтотОбъект.Договор) Тогда
		ТЗ_ = КвотыПолныеПрава.ПолучитьСостояниеКвотПоДоговору(ЭтотОбъект.Договор, ЭтотОбъект.Пациент);
	КонецЕсли;
	
	Если ТЗ_ <> Неопределено Тогда
		Для Каждого строкаТЗ Из ТЗ_ Цикл
			Если Не ЗначениеЗаполнено(ЭтотОбъект.Пациент) Или Не ЗначениеЗаполнено(строкаТЗ.Пациент)
				Или ЭтотОбъект.Пациент = строкаТЗ.Пациент
			Тогда
				НоваяСтрокаТЗ = ЭтотОбъект.Список.Добавить();
				ЗаполнитьЗначенияСвойств(НоваяСтрокаТЗ, строкаТЗ);
				НоваяСтрокаТЗ.Период = Новый СтандартныйПериод(НоваяСтрокаТЗ.ДатаНачала,НоваяСтрокаТЗ.ДатаОкончания);
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	ЭтотОбъект.Список.Сортировать("ДатаНачала,ДатаОкончания,ДенежнаяКвота,КвотируемыйОбъект");
	
КонецПроцедуры

&НаКлиенте
Процедура Обновить(Команда)
	
	ОбновитьСписок();
	
КонецПроцедуры


#КонецОбласти