﻿
#Область ПрограммныйИнтерфейс


&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Параметры.Отбор.Свойство("Соглашение", ЭтотОбъект.Соглашение);
	Параметры.Отбор.Свойство("Договор", ЭтотОбъект.Договор);
	
КонецПроцедуры



&НаКлиенте
Процедура Создать(Команда)
	П_ = Новый Структура("Основание", ВыбратьЗаполненное(ЭтотОбъект.Соглашение,ЭтотОбъект.Договор));
	ФормаДокумента = ПолучитьФорму(
		"Документ.УстановкаКвот.ФормаОбъекта", П_);

	ФормаДокумента.Открыть();
КонецПроцедуры


#КонецОбласти