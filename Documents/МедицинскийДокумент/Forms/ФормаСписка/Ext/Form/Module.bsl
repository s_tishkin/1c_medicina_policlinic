﻿
#Область ПрограммныйИнтерфейс

////
 // Функция: ПолучитьСсылкуВизуализатора
 //   Возвращает ссылку на визуализатор медицинского документа.
 //
 // Параметры:
 //   СсылкаМД {ДокументСсылка.МедицинскийДокумент}
 //     Ссылка на медицинский документ.
 //
 // Возврат: {СправочникСсылка.ВизуализаторыМедицискихДокументов}
 //   Визуализатор, наиболее подходящий для данного документа.
 ///
&НаСервере
Функция ПолучитьСсылкуВизуализатора(СсылкаМД)
	Если Истина
		И ТипЗнч(СсылкаМД) = Тип("ДокументСсылка.МедицинскийДокумент")
		И ЗначениеЗаполнено(СсылкаМД)
	Тогда
		ШаблонМедицинскогоДокумента_ = Документы.МедицинскийДокумент.ПолучитьШаблонМедицинскогоДокумента(СсылкаМД);
	КонецЕсли;
	Возврат ШаблоныМедицинскихДокументов.ПолучитьСсылкуВизуализатора(ШаблонМедицинскогоДокумента_);
КонецФункции

////
 // Функция: ПолучитьТипВизуализатора
 //   Возвращает тип визуализатора медицинского документа.
 //
 // Параметры:
 //   СсылкаВизуализатор {СправочникСсылка.ВизуализаторыМедицинскихДокументов}
 //     Ссылка на визуализатор.
 //
 // Возврат: {Строка}
 //   Тип визуализатора
 ///
&НаСервере
Функция ПолучитьТипВизуализатора(СсылкаВизуализатор)
	Возврат Справочники.ВизуализаторыМедицинскихДокументов.ПолучитьТипВизуализатора(СсылкаВизуализатор);
КонецФункции

&НаКлиенте
Процедура ПоказатьМД(Команда)
	Если ЭтотОбъект.Элементы.Список.ТекущаяСтрока <> Неопределено Тогда
		СсылкаМД = ЭтотОбъект.Элементы.Список.ТекущиеДанные.Ссылка;
		ШаблоныМедицинскихДокументовКлиент.ПоказатьМД(
			,
			ПолучитьТелоМедицинскогоДокумента(СсылкаМД),
			СсылкаМД,
			ЭтотОбъект
		);
	КонецЕсли;
КонецПроцедуры

&НаСервере
Функция ПолучитьТелоМедицинскогоДокумента(СсылкаМД) 
	CDAДокумент_ = Документы.МедицинскийДокумент.ПолучитьCDAДокумент(СсылкаМД);
	Возврат Документы.МедицинскийДокумент.ПолучитьТелоМД(CDAДокумент_);
КонецФункции

&НаСервере
Функция ПолучитьШМДПоСсылкеМД(СсылкаМД)
	Возврат Документы.МедицинскийДокумент.ПолучитьШаблонМедицинскогоДокумента(СсылкаМД);
КонецФункции

&НаКлиенте
Функция ПолучитьШМД(СсылкаМД) 
	Перем СсылкаШМД;
	СсылкаШМД = ПолучитьШМДПоСсылкеМД(СсылкаМД);
	Если СсылкаШМД <> Неопределено Тогда
		Возврат СсылкаШМД;
	Иначе
		
	КонецЕсли;	
КонецФункции

&НаКлиенте
Процедура РедактироватьМД(Команда)
	Перем СсылкаМД;
	
	Если ЭтотОбъект.Элементы.Список.ТекущаяСтрока <> Неопределено Тогда
		СсылкаМД = ЭтотОбъект.Элементы.Список.ТекущиеДанные.Ссылка;
		ШаблоныМедицинскихДокументовКлиент.ЗапуститьШМД(
			ПолучитьШМД(СсылкаМД),
			ПолучитьТелоМедицинскогоДокумента(СсылкаМД),
			СсылкаМД,
			ЭтотОбъект
		);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ТекстМД(Команда)
	Если ЭтотОбъект.Элементы.Список.ТекущаяСтрока <> Неопределено Тогда		
		СсылкаМД = ЭтотОбъект.Элементы.Список.ТекущиеДанные.Ссылка;		
		ОткрытьФорму(
			"ОбщаяФорма.ФормаРедактированияМД",
			Новый Структура("ТекстовоеСодержимое, ИдентификаторДокумента",
				ПолучитьТелоМедицинскогоДокумента(СсылкаМД),
				СсылкаМД
			)
		);
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура СброситьЗначениеШМД(Ссылка) 
	НачатьТранзакцию();
	Объект = Ссылка.ПолучитьОбъект();
	
	Для Каждого строкаТЧ_ Из Объект.CDAДокументы Цикл
		строкаТЧ_.Актуально = Ложь;
	КонецЦикла;
	
	Объект.Записать();
	ЗафиксироватьТранзакцию();
КонецПроцедуры

&НаКлиенте
Процедура СброситьШМД(Команда)
	Если ЭтотОбъект.Элементы.Список.ТекущаяСтрока <> Неопределено Тогда		
		СброситьЗначениеШМД(ЭтотОбъект.Элементы.Список.ТекущаяСтрока);
	КонецЕсли;	
КонецПроцедуры

&НаСервере
Функция ПолучитьДокументПроведен(СсылкаМД)
	Возврат СсылкаМД.Проведен;
КонецФункции

&НаСервере
Функция ПолучитьТабличныйДокумент(СсылкаМД)
	Возврат ВизуализаторCDA.СформироватьТабличныйДокумент(СсылкаМД);
КонецФункции

#Если Не ВебКлиент Тогда
&НаКлиенте
Процедура СохранитьPDF(Команда)
	Если ЭтотОбъект.Элементы.Список.ТекущаяСтрока <> Неопределено Тогда		
		СсылкаМД = ЭтотОбъект.Элементы.Список.ТекущиеДанные.Ссылка;	
		Визуализатор_ = ПолучитьСсылкуВизуализатора(СсылкаМД);
		
		Если ПолучитьТипВизуализатора(Визуализатор_) = "xslt" Тогда
			АдресФайла_ = ШаблоныМедицинскихДокументов.HTML2PDF(
				ШаблоныМедицинскихДокументов.CDA2HTML(
					ПолучитьТелоМедицинскогоДокумента(СсылкаМД),
					ПолучитьСсылкуВизуализатора(СсылкаМД),
					ШаблоныМедицинскихДокументовПереопределяемый.ОпределитьБазовыйАдрес(СсылкаМД),
					ПолучитьДокументПроведен(СсылкаМД),
					Истина
				)
			);
		Иначе
			ТабДок_ = ПолучитьТабличныйДокумент(СсылкаМД);
			Если ТабДок_ = Неопределено Тогда 
				Возврат;
			КонецЕсли;
			ТабДок_.Защита = Истина;
			ВремФайл_ = ПолучитьИмяВременногоФайла("pdf");
			ТабДок_.Записать(ВремФайл_, ТипФайлаТабличногоДокумента.PDF);
			ДДанные_ = Новый ДвоичныеДанные(ВремФайл_);
			АдресФайла_ = ПоместитьВоВременноеХранилище(ДДанные_);
		КонецЕсли;
		Если АдресФайла_ <> Неопределено Тогда
			ПолучитьФайл(
				АдресФайла_,
				"medical.pdf"
			);			
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры
#КонецЕсли

&НаКлиенте
Процедура СохранитьXML(Команда)
	Если ЭтотОбъект.Элементы.Список.ТекущаяСтрока <> Неопределено Тогда		
		СсылкаМД = ЭтотОбъект.Элементы.Список.ТекущиеДанные.Ссылка;		
		НачатьПолучениеФайлов( ,
			ШаблоныМедицинскихДокументов.МассивФайловXML(
				ПолучитьТелоМедицинскогоДокумента(СсылкаМД),
				ПолучитьСсылкуВизуализатора(СсылкаМД)
			)
		);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ПечатьПакета(Команда)
	Если ЭтотОбъект.Элементы.Список.ВыделенныеСтроки.Количество() > 0 Тогда
		Тексты = Новый Массив;
		Для каждого Строка Из ЭтотОбъект.Элементы.Список.ВыделенныеСтроки Цикл
			Тексты.Добавить(ПолучитьТелоМедицинскогоДокумента(Строка));
		КонецЦикла;
		
		ШаблоныМедицинскихДокументовКлиент.ПечататьМД(Новый Массив(), Тексты, Истина, , , , ЭтотОбъект.Элементы.Список.ВыделенныеСтроки);
		
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.Печать
	
	// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
	
КонецПроцедуры

#Область ОбработчикиКомандФормы

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтотОбъект, ЭтотОбъект.Элементы.Список);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

#КонецОбласти


#КонецОбласти