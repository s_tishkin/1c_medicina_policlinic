﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ИнтерфейсОбработчиковСобытийДокумента

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	ЗаполнитьВредности(Отказ);
	
	РезервированиеНомеровКарт(Отказ, РежимЗаписи);
	
КонецПроцедуры

Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	
	Если ЭтотОбъект.ПрикрепленныеПациенты.Количество() > 0 Тогда
		ДвижениеПоПрикреплениям(Отказ, РежимПроведения);
	ИначеЕсли ЭтотОбъект.ПланируемыйКонтингент.Количество() > 0 Тогда
		ДвижениеПоКонтингенту();
	Иначе
		Отказ = Истина;
		// TODO: СообщенияПользователю.
		Сообщить(НСтр("ru='В документе отсутствуют данные.'"));
	КонецЕсли;
	
КонецПроцедуры

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, ТекстЗаполнения, СтандартнаяОбработка)
	
	Ответственный = Пользователи.ТекущийПользователь();
	
	Если Тип(ДанныеЗаполнения) = Тип("Структура") Тогда
		Если ДанныеЗаполнения.Свойство("ПланируемыйКонтингент") И НЕ ДанныеЗаполнения.ПланируемыйКонтингент = Неопределено Тогда
			ЭтотОбъект.ПланируемыйКонтингент.Загрузить(ДанныеЗаполнения.ПланируемыйКонтингент.Выгрузить());
		КонецЕсли;
		Если ДанныеЗаполнения.Свойство("ПрикрепленныеПациенты") И НЕ ДанныеЗаполнения.ПрикрепленныеПациенты = Неопределено Тогда
			ЭтотОбъект.ПрикрепленныеПациенты.Загрузить(ДанныеЗаполнения.ПрикрепленныеПациенты.Выгрузить());
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

Процедура ОбработкаУдаленияПроведения(Отказ)
	
	// удалить записи с текущим документом и без документа открепления
	УдалитьДвижениеПоПрикреплениям(Отказ);
	
	// удалить записи с движений по контингенту
	//УдалитьДвижениеПоКонтингенту(Отказ);
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

Процедура ДвижениеПоПрикреплениям(Отказ, РежимПроведения)
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	Запрос.Текст = 
	"ВЫБРАТЬ
	|	*
	|ПОМЕСТИТЬ ПрикрепленияДокумента
	|ИЗ
	|	&ТаблицаПрикрепленныеПациенты КАК ПрикрепленияДокумента
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ЗагружаемыеДанные.*,
	|	&Период КАК Период,
	|	&ДокументПрикрепления КАК ДокументПрикрепления,
	|	ВЫБОР КОГДА РегистрПрикреплений.Соглашение ЕСТЬ NULL И &ЭтоОткрепление = Ложь ТОГДА
	|			ИСТИНА
	|		ИНАЧЕ
	|			ЛОЖЬ
	|	КОНЕЦ КАК ЭтоНоваяЗапись,
	|	ВЫБОР КОГДА ЗагружаемыеДанные.Соглашение ЕСТЬ NULL И &ЭтоОткрепление = Ложь ТОГДА
	|			ИСТИНА
	|		ИНАЧЕ
	|			ЛОЖЬ
	|	КОНЕЦ КАК ЭтоУдаление,
	|	ВЫБОР КОГДА (ЗагружаемыеДанные.ДатаПрикрепления = РегистрПрикреплений.ДатаПрикрепления И
	|			 ЗагружаемыеДанные.ДатаОткрепления = РегистрПрикреплений.ДатаОткрепления И
	|			 ЗагружаемыеДанные.Пол = РегистрПрикреплений.Пол И
	|			 ЗагружаемыеДанные.ТипКарты = РегистрПрикреплений.ТипКарты И
	|			 ЗагружаемыеДанные.ПолисВид = РегистрПрикреплений.ПолисВид И
	|			 ЗагружаемыеДанные.ПолисНомер = РегистрПрикреплений.ПолисНомер И
	|			 ЗагружаемыеДанные.ПолисСрокДействия = РегистрПрикреплений.ПолисСрокДействия И
	|			 ЗагружаемыеДанные.Телефон = РегистрПрикреплений.Телефон И
	|			 ЗагружаемыеДанные.Адрес = РегистрПрикреплений.Адрес И
	|			 ЗагружаемыеДанные.МестоРаботы = РегистрПрикреплений.МестоРаботы И
	|			 ЗагружаемыеДанные.Должность = РегистрПрикреплений.Должность И
	|			 ЗагружаемыеДанные.ГруппаИнвалидности = РегистрПрикреплений.ГруппаИнвалидности И
	|			 ЗагружаемыеДанные.СтраховаяПрограмма = РегистрПрикреплений.СтраховаяПрограмма И
	|			 ЗагружаемыеДанные.Госпитализация = РегистрПрикреплений.Госпитализация И
	|			 ЗагружаемыеДанные.Комментарий = РегистрПрикреплений.Комментарий)
	|	ТОГДА
	|		ЛОЖЬ
	|	ИНАЧЕ
	|		ИСТИНА
	|	КОНЕЦ КАК ТребуетсяОбновление
	|ИЗ
	|	ПрикрепленияДокумента КАК ЗагружаемыеДанные
	|		ПОЛНОЕ СОЕДИНЕНИЕ РегистрСведений.ПрикрепленныеПациентыСоглашений.СрезПоследних(&ДатаСреза) КАК РегистрПрикреплений
	|		ПО &ДокументПрикрепления = РегистрПрикреплений.ДокументПрикрепления И
	|			ЗагружаемыеДанные.Соглашение = РегистрПрикреплений.Соглашение И
	|			ЗагружаемыеДанные.Фамилия = РегистрПрикреплений.Фамилия И
	|			ЗагружаемыеДанные.Имя = РегистрПрикреплений.Имя И
	|			ЗагружаемыеДанные.Отчество = РегистрПрикреплений.Отчество И
	|			ЗагружаемыеДанные.ДатаРождения = РегистрПрикреплений.ДатаРождения
	|УПОРЯДОЧИТЬ ПО
	|	ЗагружаемыеДанные.Соглашение
	|";
	
	Запрос.УстановитьПараметр("ТаблицаПрикрепленныеПациенты", ЭтотОбъект.ПрикрепленныеПациенты.Выгрузить());
	Запрос.УстановитьПараметр("ДатаСреза",                    КонецДня(ТекущаяДатаСеанса()));
	Запрос.УстановитьПараметр("ДокументПрикрепления",         ЭтотОбъект.Ссылка);
	Запрос.УстановитьПараметр("Период",                       ЭтотОбъект.Дата);
	Запрос.УстановитьПараметр("ЭтоОткрепление",               ЭтотОбъект.ЭтоОткрепление);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	РегистрПрикреплений = РегистрыСведений.ПрикрепленныеПациентыСоглашений;
	// открепления не меняют вредностей
	ИспользоватьВредность = ПолучитьФункциональнуюОпцию("ИспользоватьВредныеФакторы") И ЭтотОбъект.ЭтоОткрепление = Ложь;
	НовыйНаборВредностей = РегистрыСведений.ВредныеФакторыПрикрепленных.СоздатьНаборЗаписей();
	НовыйНаборПрикреплений = РегистрПрикреплений.СоздатьНаборЗаписей();
	
	ПредыдущееСоглашение = Неопределено;
	ТаблицаПрикреплений = Неопределено;
	ТаблицаВредностей = Неопределено;
	
	Пока Выборка.Следующий() Цикл
		
		Если Выборка.Загружать = Ложь ИЛИ Выборка.ЕстьОшибка = Истина Тогда
			Продолжить;
		КонецЕсли;
		
		// запишем изменения при переходе на новое соглашение и прочитаем новый набор
		Если ПредыдущееСоглашение <> Выборка.Соглашение Тогда
			ПредыдущееСоглашение = Выборка.Соглашение;
			
			// если закончили модифицировать данные прикреплений
			Если НЕ ТаблицаПрикреплений = Неопределено Тогда
				НовыйНаборПрикреплений.Загрузить(ТаблицаПрикреплений);
				НовыйНаборПрикреплений.Записать(Истина);
			КонецЕсли;
			
			// если закончили модифицировать данные прикреплений
			Если НЕ ТаблицаВредностей = Неопределено И ИспользоватьВредность = Истина Тогда
				НовыйНаборВредностей.Загрузить(ТаблицаВредностей);
				НовыйНаборВредностей.Записать(Истина);
			КонецЕсли;
			
			// так как набор один, надо его чистить
			НовыйНаборПрикреплений.Очистить();
			НовыйНаборПрикреплений.Отбор.Соглашение.Установить(Выборка.Соглашение);
			НовыйНаборПрикреплений.Прочитать();
			ТаблицаПрикреплений = НовыйНаборПрикреплений.Выгрузить();
			
			// так как набор один, надо его чистить
			НовыйНаборВредностей.Очистить();
			НовыйНаборВредностей.Отбор.Соглашение.Установить(Выборка.Соглашение);
			ТаблицаВредностей = НовыйНаборВредностей.Выгрузить();
		КонецЕсли;
		
		ОтборДляПоиска = Новый Структура("ДокументПрикрепления, Соглашение, Фамилия, Имя, Отчество, ДатаРождения");
		ЗаполнитьЗначенияСвойств(ОтборДляПоиска, Выборка);
		// Запись от этого документа ещё не побывала в регистре
		Если Выборка.ЭтоНоваяЗапись = Истина Тогда
			
			НовоеПрикрепление = ТаблицаПрикреплений.Добавить();
			ЗаполнитьЗначенияСвойств(НовоеПрикрепление, Выборка);
			
		// Запись исчезла с документа, но есть в регистре
		ИначеЕсли Выборка.ЭтоУдаление = Истина Тогда
			
			УдаляемыеСтроки = ТаблицаПрикреплений.НайтиСтроки(ОтборДляПоиска);
			Для каждого УдаляемаяСтрока Из УдаляемыеСтроки Цикл
				ТаблицаПрикреплений.Удалить(УдаляемаяСтрока);
			КонецЦикла;
			
		// Обновление тоже самое что открепление, разница лишь в полях
		ИначеЕсли Выборка.ТребуетсяОбновление = Истина Тогда
			
			ОбновляемыеСтроки = ТаблицаПрикреплений.НайтиСтроки(ОтборДляПоиска);
			Для каждого ОбновляемаяСтрока Из ОбновляемыеСтроки Цикл
				Если ЭтотОбъект.ЭтоОткрепление = Истина Тогда
					ЗаполнитьЗначенияСвойств(ОбновляемаяСтрока, Выборка, "ДатаОткрепления");
				Иначе
					ЗаполнитьЗначенияСвойств(ОбновляемаяСтрока, Выборка);
				КонецЕсли;
			КонецЦикла;
			
		КонецЕсли;
		
		Если ИспользоватьВредность = Истина Тогда
			// заполним вредные факторы и работы, которые заполнили перед записью.
			Вредности_ = ЭтотОбъект.ВредныеФакторыРаботы.НайтиСтроки(Новый Структура("ИдентификаторСтрокиЗагрузки", Выборка.ИдентификаторСтрокиЗагрузки));
			Для каждого НоваяВредность Из Вредности_ Цикл
				НоваяСтрокаНабора = ТаблицаВредностей.Добавить();
				ЗаполнитьЗначенияСвойств(НоваяСтрокаНабора, Выборка);
				НоваяСтрокаНабора.ВредныйФактор = НоваяВредность.Вредность;
			КонецЦикла;
		КонецЕсли;
		
	КонецЦикла;
	
	// последний набор запишем отдельно
	Если НЕ ТаблицаПрикреплений = Неопределено Тогда
		НовыйНаборПрикреплений.Загрузить(ТаблицаПрикреплений);
		НовыйНаборПрикреплений.Записать(Истина);
	КонецЕсли;
	
	// последний набор запишем отдельно
	Если НЕ ТаблицаВредностей = Неопределено И ИспользоватьВредность = Истина Тогда
		НовыйНаборВредностей.Загрузить(ТаблицаВредностей);
		НовыйНаборВредностей.Записать(Истина);
	КонецЕсли;
	
КонецПроцедуры

Процедура УдалитьДвижениеПоПрикреплениям(Отказ)
	
	ИспользоватьВредность = ПолучитьФункциональнуюОпцию("ИспользоватьВредныеФакторы") И ЭтотОбъект.ЭтоОткрепление = Ложь;
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	ПрикрепленныеПациентыСоглашений.Соглашение,
		|	ПрикрепленныеПациентыСоглашений.Период,
		|	ПрикрепленныеПациентыСоглашений.Фамилия,
		|	ПрикрепленныеПациентыСоглашений.Имя,
		|	ПрикрепленныеПациентыСоглашений.Отчество,
		|	ПрикрепленныеПациентыСоглашений.ДатаРождения,
		|	ПрикрепленныеПациентыСоглашений.ДокументПрикрепления,
		|	ПрикрепленныеПациентыСоглашений.ДокументОткрепления
		|ИЗ
		|	РегистрСведений.ПрикрепленныеПациентыСоглашений КАК ПрикрепленныеПациентыСоглашений
		|ГДЕ
		|	(&ЭтоОткрепление = ЛОЖЬ И ПрикрепленныеПациентыСоглашений.ДокументПрикрепления = &ДокументПрикрепления) ИЛИ
		|	(&ЭтоОткрепление = ИСТИНА И ПрикрепленныеПациентыСоглашений.ДокументОткрепления = &ДокументОткрепления)
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ВредныеФакторыПрикрепленных.Соглашение,
		|	ВредныеФакторыПрикрепленных.Фамилия,
		|	ВредныеФакторыПрикрепленных.Имя,
		|	ВредныеФакторыПрикрепленных.Отчество,
		|	ВредныеФакторыПрикрепленных.ДатаРождения
		|ИЗ
		|	РегистрСведений.ВредныеФакторыПрикрепленных КАК ВредныеФакторыПрикрепленных
		|ГДЕ
		|	ВредныеФакторыПрикрепленных.ДокументПрикрепления = &ДокументПрикрепления";
	
	Запрос.УстановитьПараметр("ДокументПрикрепления", ЭтотОбъект.Ссылка);
	Запрос.УстановитьПараметр("ЭтоОткрепление", ЭтотОбъект.ЭтоОткрепление);
	Запрос.УстановитьПараметр("ДокументОткрепления", ЭтотОбъект.Ссылка);
	
	РезультатЗапроса = Запрос.ВыполнитьПакет();
	
	// удалим прикрепления, созданные этим документом
	ВыборкаПрикрепления = РезультатЗапроса[0].Выбрать();
	
	Пока ВыборкаПрикрепления.Следующий() Цикл
		
		НаборЗаписей_ = РегистрыСведений.ПрикрепленныеПациентыСоглашений.СоздатьНаборЗаписей();
		НаборЗаписей_.Отбор.Соглашение.Установить(ВыборкаПрикрепления.Соглашение);
		НаборЗаписей_.Отбор.Фамилия.Установить(ВыборкаПрикрепления.Фамилия);
		НаборЗаписей_.Отбор.Имя.Установить(ВыборкаПрикрепления.Имя);
		НаборЗаписей_.Отбор.Отчество.Установить(ВыборкаПрикрепления.Отчество);
		НаборЗаписей_.Отбор.ДатаРождения.Установить(ВыборкаПрикрепления.ДатаРождения);
		
		Если ЭтотОбъект.ЭтоОткрепление = Истина Тогда
			НаборЗаписей_.Прочитать();
			Для каждого Запись_ Из НаборЗаписей_ Цикл
				Запись_.ДатаОткрепления = Неопределено;
				Запись_.ДокументОткрепления = Неопределено;
			КонецЦикла;
		ИначеЕсли ЗначениеЗаполнено(ВыборкаПрикрепления.ДокументОткрепления) Тогда
			Отказ = Истина;
			//TODO: СообщенияПользователю
			Сообщить(НСтр("ru='Отмена проведения невозможна, так как заполнен документ открепления.'"));
			Возврат;
		КонецЕсли;
		
		НаборЗаписей_.Записать();
		
	КонецЦикла;
	
	Если ИспользоватьВредность = Истина Тогда
		// удалим вредности, созданные этим документом
		ВыборкаВредности = РезультатЗапроса[1].Выбрать();
		
		Пока ВыборкаВредности.Следующий() Цикл
			
			НаборЗаписей_ = РегистрыСведений.ВредныеФакторыПрикрепленных.СоздатьНаборЗаписей();
			НаборЗаписей_.Отбор.Соглашение.Установить(ВыборкаВредности.Соглашение);
			НаборЗаписей_.Отбор.Фамилия.Установить(ВыборкаВредности.Фамилия);
			НаборЗаписей_.Отбор.Имя.Установить(ВыборкаВредности.Имя);
			НаборЗаписей_.Отбор.Отчество.Установить(ВыборкаВредности.Отчество);
			НаборЗаписей_.Отбор.ДатаРождения.Установить(ВыборкаВредности.ДатаРождения);
			НаборЗаписей_.Записать();
			
		КонецЦикла;
	КонецЕсли;
	
КонецПроцедуры

Процедура ДвижениеПоКонтингенту()
	
	Движения.ПрикрепленныйКонтингентСоглашений.Записывать = Истина;
	Для каждого СтрокаКонтингент Из ЭтотОбъект.ПланируемыйКонтингент Цикл
		Если СтрокаКонтингент.Загружать = Истина И СтрокаКонтингент.ЕстьОшибка = Ложь Тогда
			НовоеДвижение = Движения.ПрикрепленныйКонтингентСоглашений.Добавить();
			ЗаполнитьЗначенияСвойств(НовоеДвижение, СтрокаКонтингент);
			НовоеДвижение.Регистратор = ЭтотОбъект.Ссылка;
			НовоеДвижение.Соглашение = ЭтотОбъект.Соглашение;
			НовоеДвижение.Контрагент = ЭтотОбъект.Контрагент;
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

Процедура ЗаполнитьВредности(Отказ)
	
	// открепления не меняют вредностей
	ИспользоватьВредность = ПолучитьФункциональнуюОпцию("ИспользоватьВредныеФакторы") И ЭтотОбъект.ЭтоОткрепление = Ложь;
	
	// Заполним вредности
	Если ИспользоватьВредность = Истина Тогда
		// всегда перезаполняем
		ЭтотОбъект.ВредныеФакторыРаботы.Очистить();
		
		// компануем вредности прикрепленных и контингента, так как конвертируются они одинаково.
		ВыгружаемыеКолонки = "Загружать,ЕстьОшибка,ВредныйФактор,ВреднаяРабота,ИдентификаторСтрокиЗагрузки";
		ТаблицаВредностей = ЭтотОбъект.ПрикрепленныеПациенты.ВыгрузитьКолонки(ВыгружаемыеКолонки);
		
		ТаблицаВредностейПрикрепленных = ЭтотОбъект.ПрикрепленныеПациенты.Выгрузить(,ВыгружаемыеКолонки);
		ТаблицаВредностейКонтингентах = ЭтотОбъект.ПланируемыйКонтингент.Выгрузить(,ВыгружаемыеКолонки);
		
		ОбщегоНазначенияКлиентСервер.ДополнитьТаблицу(ТаблицаВредностейПрикрепленных, ТаблицаВредностей);
		ОбщегоНазначенияКлиентСервер.ДополнитьТаблицу(ТаблицаВредностейКонтингентах, ТаблицаВредностей);
		
		Для каждого ПрикрепленныйПациент Из ТаблицаВредностей Цикл
			
			Если ПрикрепленныйПациент.Загружать = Ложь ИЛИ ПрикрепленныйПациент.ЕстьОшибка = Истина Тогда
				Продолжить;
			КонецЕсли;
			
			Если ЗначениеЗаполнено(ПрикрепленныйПациент.ВредныйФактор) Тогда
				МассивВредныхФакторов = Справочники.ВредныеФакторы.ВредностиПоКодам(ПрикрепленныйПациент.ВредныйФактор, Ложь);
				Для каждого ВредныйФактор Из МассивВредныхФакторов.Ссылки Цикл
					Отбор = Новый Структура("ИдентификаторСтрокиЗагрузки,Вредность", ПрикрепленныйПациент.ИдентификаторСтрокиЗагрузки, ВредныйФактор);
					НайденныеСтроки = ЭтотОбъект.ВредныеФакторыРаботы.НайтиСтроки(Отбор);
					Если НайденныеСтроки.Количество() = 0 Тогда
						НоваяВредность_ = ЭтотОбъект.ВредныеФакторыРаботы.Добавить();
						ЗаполнитьЗначенияСвойств(НоваяВредность_, Отбор);
					КонецЕсли;
				КонецЦикла;
			КонецЕсли;
			
			Если ЗначениеЗаполнено(ПрикрепленныйПациент.ВреднаяРабота) Тогда
				МассивВредныхРабот = Справочники.ВредныеФакторы.ВредностиПоКодам(ПрикрепленныйПациент.ВреднаяРабота, Истина);
				Для каждого ВреднаяРабота Из МассивВредныхРабот.Ссылки Цикл
					Отбор = Новый Структура("ИдентификаторСтрокиЗагрузки,Вредность", ПрикрепленныйПациент.ИдентификаторСтрокиЗагрузки, ВреднаяРабота);
					НайденныеСтроки = ЭтотОбъект.ВредныеФакторыРаботы.НайтиСтроки(Отбор);
					Если НайденныеСтроки.Количество() = 0 Тогда
						НоваяВредность_ = ЭтотОбъект.ВредныеФакторыРаботы.Добавить();
						ЗаполнитьЗначенияСвойств(НоваяВредность_, Отбор);
					КонецЕсли;
				КонецЦикла;
			КонецЕсли;
			
		КонецЦикла;
		
		
	КонецЕсли;
	
КонецПроцедуры

Процедура РезервированиеНомеровКарт(Отказ, РежимЗаписи)
	
	// резервируем номеров карт.
	ДанныеПоКартам = ЭтотОбъект.ПрикрепленныеПациенты.ВыгрузитьКолонки("ТипКарты,НомерКарты");
	// получим массив различных Типов карт.
	ДанныеПоТипамКарт = ОбщегоНазначенияКлиентСервер.СвернутьМассив(ДанныеПоКартам.ВыгрузитьКолонку("ТипКарты"));
	
	Для каждого стрТипКарты Из ДанныеПоТипамКарт Цикл
		// отберем номера карт текущего типа карты
		НомераТипаКарты = ДанныеПоКартам.Скопировать(Новый Структура("ТипКарты", стрТипКарты.ТипКарты), "НомерКарты");
		// выгрузим номера в массив
		МассивНомеровКарты = НомераТипаКарты.ВыгрузитьКолонку("НомерКарты");
		// уберем пустые номера карт с массива
		ОбщегоНазначенияКлиентСервер.УдалитьВсеВхожденияЗначенияИзМассива(МассивНомеровКарты, 0);
		
		Если ЗначениеЗаполнено(стрТипКарты.ТипКарты) И МассивНомеровКарты.Количество() > 0 Тогда
			Успех_ = Истина;
			Если РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
				// TODO: СообщенияПользователю
				Успех_ = НумерацияОбъектов.ЗарезервироватьНомераОбъектов(
					стрТипКарты.ТипКарты,
					,
					ЭтотОбъект.Дата,
					НомераТипаКарты.ВыгрузитьКолонку("НомерКарты"),
					НСтр("ru='Резервирование номеров для прикреплений пациентов'")
				);
				// TODO: СообщенияПользователю
				СообщениеОшибкиРезервирования = НСтр("ru='Не удалось зарезервировать номера карт'");
			ИначеЕсли РежимЗаписи = РежимЗаписиДокумента.ОтменаПроведения Тогда
				// TODO: СообщенияПользователю
				Успех_ = НумерацияОбъектов.СнятьРезервированиеНомеровОбъектов(
					стрТипКарты.ТипКарты,
					,
					ЭтотОбъект.Дата,
					НомераТипаКарты.ВыгрузитьКолонку("НомерКарты"),
					НСтр("ru='Отмена проведения прикреплений пациентов'")
				);
				// TODO: СообщенияПользователю
				СообщениеОшибкиРезервирования = НСтр("ru='Не удалось отменить зарезервирование номеров карт'");
			КонецЕсли;
			Если Успех_ = Ложь Тогда
				Отказ = Истина;
				// TODO: СообщенияПользователю
				Сообщить(СообщениеОшибкиРезервирования);
				Прервать;
			КонецЕсли;
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли