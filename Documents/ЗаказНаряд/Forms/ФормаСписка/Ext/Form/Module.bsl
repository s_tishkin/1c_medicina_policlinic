﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.Печать

	ОформленияВидовОбъектов.НастроитьУсловноеОформление(
		ЭтотОбъект, "Список.СтатусУслуги", "Список", "СменноеЗаданиеДополнительно"
	);
КонецПроцедуры

&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	Если ИмяСобытия = ОповещенияФорм.ПриИзмененииЗаказНаряда() Тогда
		ЭтотОбъект.Элементы.Список.Обновить();
		СписокПриАктивизацииСтроки(ЭтотОбъект.Элементы.Список);
	КонецЕсли;
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийКомандФормы

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтотОбъект, Элементы.Список);
	
КонецПроцедуры

// Конец СтандартныеПодсистемы.Печать

&НаКлиенте
Процедура СписокПриАктивизацииСтроки(Элемент)
	Если Элемент.ТекущиеДанные = Неопределено Тогда
		Возврат;
	КонецЕсли;

	ЭтотОбъект.Элементы.ФормаДокументЗаказНарядОтменитьЗакрытиеЗаказНаряда.Доступность = 
		ЗначениеЗаполнено(Элемент.ТекущиеДанные.ДатаЗакрытия)
	;
	ЭтотОбъект.Элементы.ФормаДокументЗаказНарядЗакрытьЗаказНаряд.Доступность = 
		НЕ ЗначениеЗаполнено(Элемент.ТекущиеДанные.ДатаЗакрытия)
	;
КонецПроцедуры

#КонецОбласти