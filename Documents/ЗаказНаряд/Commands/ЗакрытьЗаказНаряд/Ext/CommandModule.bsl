﻿
&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	ОповещениеОтвет = Новый ОписаниеОповещения("ПослеОтветаНаВопрос", ЭтотОбъект, ПараметрКоманды);
	ПоказатьВопрос(ОповещениеОтвет, "Вы уверены, что хотите закрыть заказ-наряд?", РежимДиалогаВопрос.ДаНет, ,КодВозвратаДиалога.Да);
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеОтветаНаВопрос(Ответ, ПараметрКоманды) Экспорт
	
	Если Ответ = КодВозвратаДиалога.Да Тогда 
		ЗакрытьЗаказНарядСервер(ПараметрКоманды);
		Оповестить(ОповещенияФорм.ПриИзмененииЗаказНаряда());
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ЗакрытьЗаказНарядСервер(ПараметрКоманды)
	Документы.ЗаказНаряд.ЗакрытьЗаказНаряд(ПараметрКоманды)
КонецПроцедуры