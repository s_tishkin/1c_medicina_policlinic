﻿
#Область ПрограммныйИнтерфейс


&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	Отбор_ = Новый Структура("Соглашение", ПараметрКоманды);
	ПараметрыФормы_ = Новый Структура("Отбор", Отбор_);
	ОткрытьФорму(
		"Документ.Направление.ФормаСписка", 
		ПараметрыФормы_, 
		ПараметрыВыполненияКоманды.Источник, 
		ПараметрыВыполненияКоманды.Уникальность, 
		ПараметрыВыполненияКоманды.Окно, 
		ПараметрыВыполненияКоманды.НавигационнаяСсылка
	);
КонецПроцедуры


#КонецОбласти