﻿#Область ПрограммныйИнтерфейс

////
 // Процедура: Форма_ПриСозданииНаСервере
 //   Обработчик события формы ПриСозданииНаСервере.
 //   Устанавливает отбор по мед. карте, если передан МД и в МД заполнена карта, 
 //   иначе - показывает только сообщение.
 //
 // Параметры:
 //   Отказ {Булево}
 //     Признак отказа от создания формы.
 //   СтандартнаяОбработка {Булево}
 //     Признак выполнения системной обработки.
 ///
&НаСервере
Процедура Форма_ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ЭтотОбъект.РежимРаботыВОтделении = Параметры.РежимРаботыВОтделении;
	
	Если ЗначениеЗаполнено(Параметры.МедицинскийДокумент) Тогда
		МедицинскийДокумент = Параметры.МедицинскийДокумент;
		МедицинскаяКарта = МедицинскийДокумент.МедицинскаяКарта;
		
		Если ЗначениеЗаполнено(МедицинскаяКарта) Тогда
			ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(
					Список, 
					"МедицинскаяКарта", 
					МедицинскаяКарта, 
					ЗначениеЗаполнено(МедицинскаяКарта)
			);
		Иначе
			ЭтотОбъект.Доступность = Ложь;
			ЭтотОбъект.ТекстОшибки = СообщенияПользователю.Получить("МедицинскийДокумент_ФормаДоступнаТолькоДляМедКарты");
		КонецЕсли;
		
	КонецЕсли;
	
	ЭтотОбъект.Список.Параметры.УстановитьЗначениеПараметра("ТекущаяДата", ТекущаяДатаСеанса());
	
	// Оформления видов объектов
	ОформленияВидовОбъектов.НастроитьУсловноеОформление(
		ЭтотОбъект, "Список.ОформлениеКомментария", "Комментарий", "Комментарии"
	);
	// КОНЕЦ Оформления видов объектов
	
	Параметры.Отбор.Свойство("КемНаправлен", ЭтотОбъект.КемНаправленОтбор);
	
	УправлениеВидимостьюЭлементовПоРежиму();
	
КонецПроцедуры

////
 // Процедура: Список_ПередНачаломДобавления
 //   Обработчик события списка ПередНачаломДобавления.
 //   Открывает форму документа и передает ей параметром медицинский документ,
 //   по которому должно быть выполнено автоматическое заполнение.
 //
 // Параметры:
 //   Элемент
 //     Элемент, который инициировал событие.
 //   Отказ {Булево}
 //     Признак отказа от добавления документа.
 //   Копирование {Булево}
 //     Признак добавления документа копированием.
 //   Родитель.
 //     Ссылка на родителя.
 //   ЭтоГруппа {Булево}
 //     Признак добавления группы.
 ///
&НаКлиенте
Процедура Список_ПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа)
	Отказ = Истина;

	П_ = Новый Структура("МедицинскийДокумент", МедицинскийДокумент);

	Если ЗначениеЗаполнено(ЭтотОбъект.КемНаправленОтбор) Тогда
		ДанныеЗаполнения_ = Новый Структура("КемНаправлен", ЭтотОбъект.КемНаправленОтбор);
		П_.Вставить("Основание", ДанныеЗаполнения_);
	КонецЕсли;

	ОткрытьФорму("Документ.Направление.ФормаОбъекта", П_);
КонецПроцедуры

////
 // Процедура: Список_ПередНачаломИзменения
 //   Обработчик события ПередНачаломИзменения таблицы "Список".
 //   Отменяет стандартные действия, открывает форму направления, при этом передает ей МД.
 //
 // Параметры:
 //   Элемент
 //     Элемент формы, который инициировал событие.
 //   Отказ {Булево}
 //     Признак отказа от изменения документа.
 ///
&НаКлиенте
Процедура Список_ПередНачаломИзменения(Элемент, Отказ)
	Отказ = Истина;
	ТекущиеДанные = Элементы.Список.ТекущиеДанные;
	Если ТекущиеДанные <> Неопределено Тогда
		ОткрытьФорму("Документ.Направление.ФормаОбъекта",
			Новый Структура("МедицинскийДокумент, Ключ", МедицинскийДокумент, ТекущиеДанные.Ссылка)
		);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура КомандаКомментарий(Команда)

	Элемент = ЭтотОбъект.Элементы.Список;
	Если Элемент.ТекущиеДанные <> Неопределено Тогда
		МеханизмыКомментариев.НажатиеКомментарияВТЧФормы(
			"Комментарий", 
			"ОформлениеКомментария",
			Элемент, 
			Элемент.ТекущиеДанные.Пациент,
			,
			Строка(Элемент.ТекущиеДанные.Пациент),
			ПредопределенноеЗначение("Перечисление.ТипыКомментариев.Пациент")
		);
		Элемент.Обновить();
	КонецЕсли;

КонецПроцедуры

&НаСервере
////
 // Процедура: УправлениеВидимостьюЭлементовПоРежиму
 //   По режиму работы (ФМД, КИ, больница зав отделения, пост отделения)
 //   устанавливает видимость колонок.
 //
 // Параметры:
///
Процедура УправлениеВидимостьюЭлементовПоРежиму()
	Если ЗначениеЗаполнено(ЭтотОбъект.МедицинскийДокумент) Тогда
	
		МассивЭлементов_ = Новый Массив();
		МассивЭлементов_.Добавить("Регион");
		МассивЭлементов_.Добавить("Комментарий");
		МассивЭлементов_.Добавить("ИсточникФинансирования");
		МассивЭлементов_.Добавить("КемНаправлен");
		МассивЭлементов_.Добавить("ПациентОповещен");
		МассивЭлементов_.Добавить("ФормаПациентОповещен");
		МассивЭлементов_.Добавить("Лупа");

		ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементовФормы(
					Элементы, МассивЭлементов_, "Видимость", Ложь);
	Иначе
		Если ЭтотОбъект.РежимРаботыВОтделении Тогда
			
			ИзменениеДанныхГоспитализации_ = РольДоступна("ИзменениеДанныхГоспитализацииНаправления") Или
				РольДоступна("ПолныеПрава");
				
			Если ИзменениеДанныхГоспитализации_ Тогда
				// Зав отделением
				
				МассивЭлементов_ = Новый Массив();
				МассивЭлементов_.Добавить("ФормаПациентОповещен");

				ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементовФормы(
							Элементы, МассивЭлементов_, "Видимость", Ложь);
			Иначе
				МассивЭлементов_ = Новый Массив();
				МассивЭлементов_.Добавить("Регион");
				МассивЭлементов_.Добавить("МКБ10");
				МассивЭлементов_.Добавить("КемНаправлен");
				МассивЭлементов_.Добавить("СтатусСогласования");
				МассивЭлементов_.Добавить("Комментарий");
				МассивЭлементов_.Добавить("Отделение");
				МассивЭлементов_.Добавить("ИсточникФинансирования");

				ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементовФормы(
							Элементы, МассивЭлементов_, "Видимость", Ложь);
			КонецЕсли;
			
		Иначе
			МассивЭлементов_ = Новый Массив();
			МассивЭлементов_.Добавить("Регион");
			МассивЭлементов_.Добавить("МКБ10");
			МассивЭлементов_.Добавить("КемНаправлен");
			МассивЭлементов_.Добавить("ПациентОповещен");
			МассивЭлементов_.Добавить("ФормаПациентОповещен");
			МассивЭлементов_.Добавить("Лупа");

			ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементовФормы(
						Элементы, МассивЭлементов_, "Видимость", Ложь);
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры


&НаКлиенте
Процедура УстановитьПациентОповещен(Команда)

	Если Элементы.Список.ТекущиеДанные <> Неопределено
		И Не Элементы.Список.ТекущиеДанные.ПациентОповещен
	Тогда
		УстановитьПациентОповещенНаСервере(Элементы.Список.ТекущиеДанные.Ссылка);
		ОповеститьОбИзменении(Элементы.Список.ТекущиеДанные.Ссылка);
	КонецЕсли;

КонецПроцедуры

&НаСервере
Процедура УстановитьПациентОповещенНаСервере(ДокументСсылка)

	ДокументОбъект_ = ДокументСсылка.ПолучитьОбъект();
	ДокументОбъект_.ПациентОповещен = Истина;
	ДокументОбъект_.Записать();

КонецПроцедуры

&НаКлиенте
Процедура СписокВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	
	Если Поле.Имя = "Лупа" Тогда
		СтандартнаяОбработка = Ложь;
		
		ПараметрыОткрытия_ = Новый Структура("Ключ", Элементы.Список.ТекущиеДанные.Пациент);
		ОткрытьФорму(
			"Справочник.Картотека.ФормаОбъекта",
			ПараметрыОткрытия_,
			ЭтотОбъект,,,,,
			РежимОткрытияОкнаФормы.БлокироватьОкноВладельца
		);
		
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	Если ЗначениеЗаполнено(ЭтотОбъект.ТекстОшибки) Тогда
		ПоказатьПредупреждение(, ЭтотОбъект.ТекстОшибки);
	КонецЕсли;
КонецПроцедуры

#КонецОбласти