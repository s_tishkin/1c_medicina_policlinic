﻿#Область ПрограммныйИнтерфейс

#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// СтандартныеПодсистемы.ВерсионированиеОбъектов
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ВерсионированиеОбъектов
	
КонецПроцедуры

#КонецОбласти

&НаКлиенте
Процедура УслугиКИсключениюПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа, Параметр)
	Отказ = Истина;
КонецПроцедуры

&НаКлиенте
Процедура ПодборУслуг(Команда)

	Если ЗначениеЗаполнено(Объект.Контрагент) Тогда
		ПараметрыОткрытия_ = Новый Структура(
			"Отбор",
			Новый Структура("Контрагент", Объект.Контрагент)
		);
		
		ОписаниеОповещения_ = Новый ОписаниеОповещения("ПослеВыбораУслуг", ЭтотОбъект);
		
		ОткрытьФорму("Документ.КорректировкаРеестраОказаннойПомощи.Форма.ФормаВыбораУслуг",
					ПараметрыОткрытия_,
					ЭтотОбъект,,,,
					ОписаниеОповещения_,
					РежимОткрытияОкнаФормы.БлокироватьОкноВладельца
				);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
// Вызывается после закрытия формы выбора услуг
//
// Параметры:
//  <Параметр1>  - <Тип.Вид> - <описание параметра>
//                 <продолжение описания параметра>
//  <Параметр2>  - <Тип.Вид> - <описание параметра>
//                 <продолжение описания параметра>
//
Процедура ПослеВыбораУслуг(Результат, ДополнительныеПараметры) Экспорт
	
	Если ТипЗнч(Результат) = Тип("Массив") И Результат.Количество() > 0 Тогда
		ЗаполнитьПоСпискуУИД(Результат);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьПоСпискуУИД(СписокУИД)
	ДокументОбъект_ = ДанныеФормыВЗначение(Объект, Тип("ДокументОбъект.КорректировкаРеестраОказаннойПомощи"));
	ДокументОбъект_.Заполнить(СписокУИД);
	
	Для Каждого строкаТЧ_ Из ДокументОбъект_.УслугиКИсключению Цикл
		Найденные_ = Объект.УслугиКИсключению.НайтиСтроки(
						Новый Структура("УникальныйИдентификаторУслуги", строкаТЧ_.УникальныйИдентификаторУслуги)
					);
		Если Найденные_.Количество() = 0 Тогда
			НоваяСтрокаТЧ_ = Объект.УслугиКИсключению.Добавить();
			ЗаполнитьЗначенияСвойств(НоваяСтрокаТЧ_, строкаТЧ_);
		КонецЕсли;
	КонецЦикла;
	
	ЗаполнитьЗначенияСвойств(Объект, ДокументОбъект_, "Партнер,Контрагент,Организация");
	
	ПересчитатьСуммы();
КонецПроцедуры

&НаСервере
Процедура ПересчитатьСуммы()
	ДокументОбъект_ = ДанныеФормыВЗначение(Объект, Тип("ДокументОбъект.КорректировкаРеестраОказаннойПомощи"));
	ДокументОбъект_.ПересчитатьСуммы();
	ЗначениеВДанныеФормы(ДокументОбъект_, Объект);
КонецПроцедуры

&НаКлиенте
Процедура УслугиКИсключениюПослеУдаления(Элемент)
	ПересчитатьСуммы();
КонецПроцедуры

&НаКлиенте
Процедура УслугиКИсключениюСуммаПринятаяПриИзменении(Элемент)
	ПересчитатьСуммы();
КонецПроцедуры

&НаКлиенте
Процедура ПартнерПриИзменении(Элемент)
	ПартнерПриИзмененииСервер();
КонецПроцедуры

// Процедура - обработчик события "ПриИзменении" поля "Партнер".
//
&НаСервере
Процедура ПартнерПриИзмененииСервер()
	
	Если ЗначениеЗаполнено(Объект.Партнер) Тогда
	
		КонтрагентПоУмолчанию = ПартнерыИКонтрагенты.ПолучитьКонтрагентаПартнераПоУмолчанию(Объект.Партнер);
		Если КонтрагентПоУмолчанию <> Неопределено Тогда
			Объект.Контрагент = КонтрагентПоУмолчанию;
		КонецЕсли;
		
	КонецЕсли;
	
	ДенежныеСредстваСервер.УстановитьПараметрыВыбораКонтрагента(Объект, Элементы.Контрагент);
	
	Объект.Договор = Неопределено;
	Объект.УслугиКИсключению.Очистить();
	
КонецПроцедуры // ПартнерПриИзмененииСервер()

&НаКлиенте
Процедура КонтрагентПриИзменении(Элемент)
	
	Если ЗначениеЗаполнено(Объект.Контрагент) Тогда
	
		Объект.Партнер = ДенежныеСредстваСервер.ПолучитьПартнераПоКонтрагенту(Объект.Контрагент);

	КонецЕсли;
	
	Объект.Договор = Неопределено;
	Объект.УслугиКИсключению.Очистить();
	
КонецПроцедуры

&НаКлиенте
Процедура ДоговорНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	МассивПараметров = Новый Массив();
	Если ЗначениеЗаполнено(Объект.Организация) Тогда
		МассивПараметров.Добавить(Новый ПараметрВыбора("Отбор.Организация", Объект.Организация));
	КонецЕсли;
	Если ЗначениеЗаполнено(Объект.Контрагент) Тогда
		МассивПараметров.Добавить(Новый ПараметрВыбора("Отбор.Контрагент", Объект.Контрагент));
	КонецЕсли;
	Если ЗначениеЗаполнено(Объект.Партнер) Тогда
		МассивПараметров.Добавить(Новый ПараметрВыбора("Отбор.Партнер", Объект.Партнер));
	КонецЕсли;
	
	НовыеПараметры = Новый ФиксированныйМассив(МассивПараметров);
	Элемент.ПараметрыВыбора = НовыеПараметры;
КонецПроцедуры

&НаКлиенте
Процедура ДоговорПриИзменении(Элемент)

	Если ЗначениеЗаполнено(Объект.Договор) Тогда
		Реквизиты_ = ОбщиеМеханизмы.ЗначенияРеквизитовОбъекта(Объект.Договор,
					Новый Структура("Организация,Контрагент,Партнер")
				);

		ЗаполнитьЗначенияСвойств(Объект, Реквизиты_);

		Объект.УслугиКИсключению.Очистить();
	КонецЕсли;
КонецПроцедуры

#КонецОбласти