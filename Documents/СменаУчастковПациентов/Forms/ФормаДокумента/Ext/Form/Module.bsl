﻿#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	УправлениеВидимостьюЭлементов();
	Если ЗначениеЗаполнено(Объект.ДокументОснование)  Тогда
		ЭтотОбъект.ТипМедицинскойКарты = Объект.ДокументОснование.ТипМедицинскойКарты;
	КонецЕсли;
	
	ЭтотОбъект.ПричинаСменыЗаявление = Перечисления.ПричиныИзмененияУчасткаПациента.ЗаявлениеНаСменуУчастка;
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура УчасткиУчастокНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	СтрокаИзменения = Объект.Участки.НайтиПоИдентификатору(Элементы.Участки.ТекущаяСтрока);
	ОписаниеОповещения_ = Новый ОписаниеОповещения("ПослеВыбораУчастка", ЭтотОбъект);
	ПараметрыОткрытия = Новый Структура("ТипУчастка,ФПДП,ИсключатьПустыеУчастки",
										СтрокаИзменения.ТипУчастка, Истина, Истина
									);
	ОткрытьФорму("Справочник.ВрачебныеУчастки.Форма.ФормаВыбора",
					ПараметрыОткрытия,
					ЭтотОбъект,,,,
					ОписаниеОповещения_,
					РежимОткрытияОкнаФормы.БлокироватьВесьИнтерфейс
				);
	
КонецПроцедуры

&НаКлиенте
Процедура ФильтрТипУчасткаПриИзменении(Элемент)
	ПрименитьФильтры();
КонецПроцедуры

&НаКлиенте
Процедура ФильтрПричинаСменыПриИзменении(Элемент)
	ПрименитьФильтры();
КонецПроцедуры

&НаКлиенте
Процедура ФильтрПредыдущийУчастокПриИзменении(Элемент)
	ПрименитьФильтры();
КонецПроцедуры

&НаКлиенте
Процедура ФильтрНовыйУчастокПриИзменении(Элемент)
		ПрименитьФильтры();
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийКомандФормы

&НаКлиенте
Процедура СоотнестиУчасткиСПациентами(Команда)
	ЭтотОбъект.УчасткиБезФильтра.Очистить();
	ЭтотОбъект.Элементы.УчасткиПредыдущийУчасток.Видимость = Истина;
	ЭтотОбъект.Элементы.УчасткиУчасток.Заголовок = "Новый участок";
	// получаем удаленные участки
	СоотнестиУдаленныеУчастки();
	// получаем новые и измененные участки
	СоотнестиНовыеИИзмененныеУчастки();	
	// заполнить ТЧ по фильтрам
	ПрименитьФильтры();
КонецПроцедуры

&НаКлиенте
Процедура ДобавитьУчасток(Команда)
	
	ОписаниеОповещения_ = Новый ОписаниеОповещения("ПослеДобавленияУчастка", ЭтотОбъект);
	ОткрытьФорму("Справочник.ВрачебныеУчастки.Форма.ФормаВыбора",
				Новый Структура("ФПДП,ИсключатьПустыеУчастки", Истина,Истина),
				ЭтотОбъект,,,,
				ОписаниеОповещения_,
				РежимОткрытияОкнаФормы.БлокироватьОкноВладельца
			);
	
КонецПроцедуры

&НаКлиенте
Процедура УдалитьУчасток(Команда)
	Если ЭтотОбъект.Элементы.Участки.ТекущаяСтрока<>Неопределено Тогда
		Эл=Объект.Участки.НайтиПоИдентификатору(Элементы.Участки.ТекущаяСтрока);
		Объект.Участки.Удалить(Эл);
	КонецЕсли;
КонецПроцедуры

#КонецОбласти

#Область ВспомогательныеМетоды

&НаСервере
Функция СоотнестиУдаленныеУчастки()
	УдУчастки = РаботаСУчастками.УчасткиБезСостава();
	ДанныеПациентовУдУчастков = РаботаСУчастками.ДанныеОПациентахУчастков(УдУчастки);
	Для каждого уд из ДанныеПациентовУдУчастков Цикл
		НоваяЗапись = УчасткиБезФильтра.Добавить();
		НоваяЗапись.Пациент = уд.Пациент;
		НоваяЗапись.ПредыдущийУчасток = уд.Участок;
		НоваяЗапись.ТипУчастка = уд.ТипУчастка;
		НоваяЗапись.ПричинаСмены = Перечисления.ПричиныИзмененияУчасткаПациента.УдалениеУчастка;
	КонецЦикла;
КонецФункции

&НаСервере
Функция СоотнестиНовыеИИзмененныеУчастки()
	НовыеУчастки = РаботаСУчастками.УчасткиБезПациентов();
	ПричиныСмены = Новый СписокЗначений;
	ПричиныСмены.Добавить(Перечисления.ПричиныИзмененияУчасткаПациента.ИзменениеСоставаУчастка);
	ПричиныСмены.Добавить(Перечисления.ПричиныИзмененияУчасткаПациента.ПоМестуРегистрацииПациента);
	ПричиныСмены.Добавить(Перечисления.ПричиныИзмененияУчасткаПациента.УдалениеУчастка);
	// получаем пациента с адресом. Временным или Регистрации
	АдресаПациентов = Справочники.ВрачебныеУчастки.ПолучитьВрИлиРегАдресаПациентов();
	Для каждого АдресПациента из АдресаПациентов Цикл
		МассивУчастковПоАдресу = РаботаСУчастками.УчасткиПоАдресу(АдресПациента);
		ТекущиеУчасткиПациента = РаботаСУчастками.УчасткиПациента(АдресПациента.Пациент);
		Для каждого ТекУчасток из ТекущиеУчасткиПациента Цикл
			Ответ = Ложь;
			УчНовый = Ложь;
			Подобранный = "";
			// проверяем сохранился ли участок
			Для каждого АдрУч из МассивУчастковПоАдресу Цикл
				// если участок не изменился
				Если ТекУчасток.Участок = АдрУч.Участок Тогда
					Ответ = Истина;
					Прервать;
				КонецЕсли;
				// подбираем участок пациенту
				Если ТекУчасток.ТипУчастка = АдрУч.Участок.ТипУчастка И Подобранный = "" Тогда
					Подобранный = АдрУч.Участок;
				КонецЕсли;
				// проверяем есть ли нужный участок в новых
				Для каждого НовУч из НовыеУчастки Цикл
					Если НовУч.Участок = АдрУч.Участок И НовУч.ТипУчастка = ТекУчасток.ТипУчастка 
						и НЕ ТекУчасток.ПричинаСмены = Перечисления.ПричиныИзмененияУчасткаПациента.ЗаявлениеНаСменуУчастка
					Тогда
						УчНовый = Истина;
						Прервать;
					КонецЕсли;
				КонецЦикла;
				Если УчНовый = Истина Тогда
					Прервать;
				КонецЕсли;
			КонецЦикла;
			Если Ответ Тогда
				Продолжить;
			КонецЕсли;
			// участок изменился
			СтрПоиска = Новый Структура("Пациент,ТипУчастка");
			СтрПоиска.Пациент = АдресПациента.Пациент;
			СтрПоиска.ТипУчастка = ТекУчасток.ТипУчастка;
			КоличествоДубликатов = УчасткиБезФильтра.НайтиСтроки(СтрПоиска).Количество();
			// если добавляемая запись уникальна
			Если КоличествоДубликатов = 0 И НЕ ТекУчасток.ПричинаСмены = Перечисления.ПричиныИзмененияУчасткаПациента.ЗаявлениеНаСменуУчастка Тогда
				НоваяЗапись = УчасткиБезФильтра.Добавить();
				НоваяЗапись.Пациент = АдресПациента.Пациент;
				// для нового участка добавляем значение реквизита участок
				Если УчНовый ИЛИ ЗначениеЗаполнено(Подобранный) Тогда
					НоваяЗапись.НовыйУчасток = Подобранный;
				КонецЕсли;
				НоваяЗапись.ПредыдущийУчасток = ТекУчасток.Участок;
				НоваяЗапись.ТипУчастка = ТекУчасток.ТипУчастка;
				НоваяЗапись.ПричинаСмены = Перечисления.ПричиныИзмененияУчасткаПациента.ИзменениеСоставаУчастка;
				Продолжить;
			ИначеЕсли НЕ ТекУчасток.ПричинаСмены = Перечисления.ПричиныИзмененияУчасткаПациента.ЗаявлениеНаСменуУчастка Тогда
				// если новый участок уже есть в удаленных, то подставляем ему участок
				УчасткиБезФильтра.НайтиСтроки(СтрПоиска)[0].НовыйУчасток = Подобранный;
			КонецЕсли;
		КонецЦикла;
	КонецЦикла;
КонецФункции

&НаКлиенте
Процедура ПрименитьФильтры()
	Объект.Участки.Очистить();
	ТипУчастка = ?(ЗначениеЗаполнено(ЭтотОбъект.ФильтрТипУчастка), ЭтотОбъект.ФильтрТипУчастка, Неопределено);
	ПричинаСмены = ?(ЗначениеЗаполнено(ЭтотОбъект.ФильтрПричинаСмены), ЭтотОбъект.ФильтрПричинаСмены, Неопределено);
	ПредыдущийУчасток = ?(ЗначениеЗаполнено(ЭтотОбъект.ФильтрПредыдущийУчасток), ЭтотОбъект.ФильтрПредыдущийУчасток, Неопределено);
	НовыйУчасток = ?(ЗначениеЗаполнено(ЭтотОбъект.ФильтрНовыйУчасток), ЭтотОбъект.ФильтрНовыйУчасток, Неопределено);
	Для каждого УчастокБезФильтра из УчасткиБезФильтра Цикл
		Если (УчастокБезФильтра.ТипУчастка = ТипУчастка ИЛИ ТипУчастка = Неопределено)
			И (УчастокБезФильтра.ПредыдущийУчасток = ПредыдущийУчасток ИЛИ ПредыдущийУчасток = Неопределено)
			И (УчастокБезФильтра.НовыйУчасток = НовыйУчасток ИЛИ НовыйУчасток = Неопределено)
			И (УчастокБезФильтра.ПричинаСмены = ПричинаСмены ИЛИ ПричинаСмены = Неопределено)
		Тогда
			ОтфильтрованныйУчасток = Объект.Участки.Добавить();
			ОтфильтрованныйУчасток.ТипУчастка = УчастокБезФильтра.ТипУчастка;
			ОтфильтрованныйУчасток.ПричинаСмены = УчастокБезФильтра.ПричинаСмены;
			ОтфильтрованныйУчасток.ПредыдущийУчасток = УчастокБезФильтра.ПредыдущийУчасток;
			ОтфильтрованныйУчасток.Участок = УчастокБезФильтра.НовыйУчасток;
			ОтфильтрованныйУчасток.Пациент = УчастокБезФильтра.Пациент;
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

&НаСервере
Процедура УправлениеВидимостьюЭлементов()
	ЭтотОбъект.Элементы.СоотнестиУчасткиСПациентами.Видимость=НЕ ЗначениеЗаполнено(Объект.ДокументОснование);
    ЭтотОбъект.Элементы.Участки.КоманднаяПанель.Доступность=НЕ ЗначениеЗаполнено(Объект.ДокументОснование) и НЕ Объект.Проведен;	
	ЭтотОбъект.Элементы.Фильтры.Видимость = НЕ ЗначениеЗаполнено(Объект.ДокументОснование);
	Если Объект.Проведен Тогда
		ЭтотОбъект.ПоложениеКоманднойПанели=ПоложениеКоманднойПанелиФормы.Нет;
		ЭтотОбъект.Элементы.КомандыФормы.Видимость=Истина;
		ЭтотОбъект.Элементы.Фильтры.Видимость = Ложь;
		ЭтотОбъект.Элементы.ФормаЗакрыть.КнопкаПоУмолчанию=Истина;
		ЭтотОбъект.Элементы.ГруппаНомераИДаты.ТолькоПросмотр=Истина;
		ЭтотОбъект.Элементы.Основное.ТолькоПросмотр=Истина;	
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ПослеВыбораУчастка(Результат, ДополнительныеПараметры) Экспорт
	
	Если ЗначениеЗаполнено(Результат) Тогда
		СтрокаИзменения = Объект.Участки.НайтиПоИдентификатору(Элементы.Участки.ТекущаяСтрока);
		СтрокаИзменения.Участок = Результат;
		СтрокаИзменения.ПричинаСмены = ЭтотОбъект.ПричинаСменыЗаявление;
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеДобавленияУчастка(Результат, ДополнительныеПараметры) Экспорт
	
	Если НЕ Результат = Неопределено Тогда
		ТипУчастка = РаботаСУчастками.ТипУчастка(Результат);
		ТипУчасткаПоКарте = РаботаСУчастками.ТипУчастка(ТипМедицинскойКарты);
		Если ЗначениеЗаполнено(ТипУчасткаПоКарте) И ТипУчастка = ТипУчасткаПоКарте Тогда
			// при заданном типе участка у карты может быть лишь один участок
			Если Объект.Участки.Количество() > 0 Тогда
				СообщенияПользователю.Показать("Участки_ПовторТипаУчастка", Новый Структура("ТипУчастка",ТипУчастка));
				Возврат;
			КонецЕсли;
		ИначеЕсли ЗначениеЗаполнено(ТипУчасткаПоКарте) И ТипУчастка <> ТипУчасткаПоКарте Тогда
			СообщенияПользователю.Показать("Участки_ТипУчасткаНеСоответствуетКарте", Новый Структура("ТипУчастка",ТипУчасткаПоКарте));
			Возврат;
		КонецЕсли;
		стр = Новый Структура("Участок",Результат);
		Нашли = Объект.Участки.НайтиСтроки(стр);
		ОтменитьДобавление = Ложь;
		Если Нашли.Количество() > 0 Тогда
			СообщенияПользователю.Показать("Участки_УчастокУжеВыбран", Новый Структура("Участок",Результат));
			ОтменитьДобавление=Истина;
		КонецЕсли;
		Если НЕ ОтменитьДобавление Тогда
			НовыйУчасток = Объект.Участки.Добавить();
			НовыйУчасток.Участок = Результат;
			НовыйУчасток.ТипУчастка = ТипУчастка;
			НовыйУчасток.ПричинаСмены = ПредопределенноеЗначение("Перечисление.ПричиныИзмененияУчасткаПациента.ИзменениеСоставаУчастка");
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти