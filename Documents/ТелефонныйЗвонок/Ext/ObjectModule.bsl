﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

// Процедура формирует строки списка участников.
Процедура ЗаполнитьКонтакты(Контакты) Экспорт
	
	Если Не Взаимодействия.КонтактыЗаполнены(Контакты) Тогда
		Возврат;
	КонецЕсли;
	
	// В телефонный звонок переносим первого контакта.
	Параметр = Контакты[0];
	Если ТипЗнч(Параметр) = Тип("Структура") Тогда
		АбонентКонтакт       = Параметр.Контакт;
		АбонентКакСвязаться  = Параметр.Адрес;
		АбонентПредставление = Параметр.Представление;
		Если Параметр.Свойство("ДатаРождения") Тогда
			ЭтотОбъект.АбонентДатаРождения = Параметр.ДатаРождения;	
		КонецЕсли;
	Иначе
		АбонентКонтакт = Параметр;
	КонецЕсли;
	
	Взаимодействия.ДозаполнитьПоляКонтактов(АбонентКонтакт, АбонентПредставление, 
		АбонентКакСвязаться, Перечисления.ТипыКонтактнойИнформации.Телефон);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытий

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	// Медицина: Поликлиника
	Если ТипЗнч(ДанныеЗаполнения) = Тип("ДокументСсылка.ТелефонныйЗвонок") Тогда
		Свойства_ = "АбонентПредставление, АбонентКакСвязаться, АбонентКонтакт," + 
			"АбонентМедицинскаяКарта, АбонентДатаРождения";
		ЗаполнитьЗначенияСвойств(ЭтотОбъект, ОбщегоНазначения.ЗначенияРеквизитовОбъекта(ДанныеЗаполнения, Свойства_));
		ЭтотОбъект.Автор = Пользователи.ТекущийПользователь();
		ЭтотОбъект.Ответственный = ЭтотОбъект.Автор;
		Возврат;
	ИначеЕсли ТипЗнч(ДанныеЗаполнения) = Тип("СправочникСсылка.Картотека") Тогда
		ЭтотОбъект.АбонентКонтакт = ДанныеЗаполнения;
		Взаимодействия.ДозаполнитьПоляКонтактов(
			ЭтотОбъект.АбонентКонтакт, ЭтотОбъект.АбонентПредставление, ЭтотОбъект.АбонентКакСвязаться, 
			Перечисления.ТипыКонтактнойИнформации.Телефон
		);
		ЭтотОбъект.АбонентДатаРождения = Регистратура.ПолучитьДатуРожденияПациента(ЭтотОбъект.АбонентКонтакт);
		ЭтотОбъект.Автор = Пользователи.ТекущийПользователь();
		ЭтотОбъект.Ответственный = ЭтотОбъект.Автор;
		Возврат;
	ИначеЕсли ТипЗнч(ДанныеЗаполнения) = Тип("ДокументСсылка.ВизитВРегистратуру") Тогда
		Свойства_ = "Контакт, Пациент, МедицинскаяКарта, ДатаРождения,КакСвязаться";
		Реквизиты_ = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(ДанныеЗаполнения, Свойства_);
		ЭтотОбъект.АбонентПредставление = Реквизиты_.Контакт;
		ЭтотОбъект.АбонентКонтакт = Реквизиты_.Пациент;
		ЭтотОбъект.АбонентМедицинскаяКарта = Реквизиты_.МедицинскаяКарта;
		ЭтотОбъект.АбонентДатаРождения = Реквизиты_.ДатаРождения;
		ЭтотОбъект.АбонентКакСвязаться = Реквизиты_.КакСвязаться;
		ЭтотОбъект.Автор = Пользователи.ТекущийПользователь();
		ЭтотОбъект.Ответственный = ЭтотОбъект.Автор;
		Возврат;
	КонецЕсли;
	// Конец Медицина: Поликлиника
	
	Взаимодействия.ЗаполнитьРеквизитыПоУмолчанию(ЭтотОбъект, ДанныеЗаполнения);
	
КонецПроцедуры

Процедура ПередЗаписью(Отказ, РежимЗаписи, РежимПроведения)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	// Медицина: Поликлиника
	ПередЗаписьюДокумента();
	// Конец Медицина: Поликлиника
	
КонецПроцедуры

Процедура ПриКопировании(ОбъектКопирования)
	
	Ответственный    = Пользователи.ТекущийПользователь();
	Автор            = Пользователи.ТекущийПользователь();
	
КонецПроцедуры

Процедура ПриЗаписи(Отказ)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	Взаимодействия.ПриЗаписиДокумента(ЭтотОбъект);
	
	// Медицина: Поликлиника
	Если ЭтотОбъект.ПометкаУдаления = Истина Тогда
		ПриУдаленииДокумента();	
	Иначе
		ПриЗаписиДокумента();
	КонецЕсли;
	// Конец Медицина: Поликлиника
	
КонецПроцедуры

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	
	// Медицина: Поликлиника
	Если ЗначениеЗаполнено(ЭтотОбъект.ВидВзаимодействия) Тогда
		Если ЭтотОбъект.ВидВзаимодействия.Предопределенный = Ложь Тогда
			ПроверяемыеРеквизиты.Удалить(ПроверяемыеРеквизиты.Найти("АбонентКакСвязаться"));
			ПроверяемыеРеквизиты.Удалить(ПроверяемыеРеквизиты.Найти("АбонентПредставление"));
		КонецЕсли;
	КонецЕсли;
	
	Если ЭтотОбъект.ВидВзаимодействия = Справочники.ВидыВзаимодействий.СогласованиеУслуг Тогда
		Для Каждого СтрокаСогласования_ Из ЭтотОбъект.Согласования Цикл
			Если СтрокаСогласования_.СтатусСогласования = Перечисления.СтатусыСогласований.Согласовано И 
				 СтрокаСогласования_.КоличествоЗаказано > 0 И СтрокаСогласования_.КоличествоСогласовано = 0
			Тогда
				Отказ = Истина;
				Параметры_ = Новый Структура("Поле", "Количество согласовано");
				СообщенияПользователю.Показать("Общие_ПолеНеЗаполнено", Параметры_);
			КонецЕсли;
		КонецЦикла;
	КонецЕсли;
	// Конец Медицина: Поликлиника
	
КонецПроцедуры

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// См. описание в комментарии к одноименной процедуре в модуле УправлениеДоступом.
Процедура ЗаполнитьНаборыЗначенийДоступа(Таблица) Экспорт

	// Логика ограничения следующая: объект доступен если доступен  "Автор" или "Ответственный".
	// Логика "ИЛИ" реализуется через различные номера наборов.
	
	// Ограничение по "Автор".
	НомерНабора = 1;

	СтрокаТаблицы = Таблица.Добавить();
	СтрокаТаблицы.НомерНабора     = НомерНабора;
	СтрокаТаблицы.ЗначениеДоступа = Автор;

	// Ограничение по "Ответственный".
	НомерНабора = НомерНабора + 1;

	СтрокаТаблицы = Таблица.Добавить();
	СтрокаТаблицы.НомерНабора     = НомерНабора;
	СтрокаТаблицы.ЗначениеДоступа = Ответственный;

	Если ЗначениеЗаполнено(АбонентКонтакт) Тогда

		Если ТипЗнч(АбонентКонтакт) = Тип("СправочникСсылка.Партнеры") Тогда

			НомерНабора = НомерНабора + 1;

			СтрокаТаблицы = Таблица.Добавить();
			СтрокаТаблицы.НомерНабора     = НомерНабора;
			СтрокаТаблицы.ЗначениеДоступа = АбонентКонтакт;

		ИначеЕсли ТипЗнч(АбонентКонтакт) = Тип("СправочникСсылка.КонтактныеЛицаПартнеров") Тогда

			Запрос = Новый Запрос(
			"ВЫБРАТЬ РАЗЛИЧНЫЕ
			|	КонтактныеЛицаПартнеров.Владелец КАК Партнер
			|ИЗ
			|	Справочник.КонтактныеЛицаПартнеров КАК КонтактныеЛицаПартнеров
			|ГДЕ
			|	КонтактныеЛицаПартнеров.Ссылка =&АбонентКонтакт
			|");
			Запрос.УстановитьПараметр("АбонентКонтакт", АбонентКонтакт);
			Выборка = Запрос.Выполнить().Выбрать();

			Пока Выборка.Следующий() Цикл

				НомерНабора = НомерНабора + 1;

				СтрокаТаблицы = Таблица.Добавить();
				СтрокаТаблицы.НомерНабора     = НомерНабора;
				СтрокаТаблицы.ЗначениеДоступа = Выборка.Партнер;

			КонецЦикла;
		КонецЕсли;
	КонецЕсли;

КонецПроцедуры

Процедура ПередЗаписьюДокумента()
	ЭтотОбъект.ДополнительныеСвойства.Вставить(
		"ДатаЗаписи", ?(ЗначениеЗаполнено(ЭтотОбъект.Ссылка), ТекущаяДатаСеанса(), ЭтотОбъект.Дата)
	);	
	Если ЭтотОбъект.ВидВзаимодействия = Справочники.ВидыВзаимодействий.СогласованиеУслуг Тогда
		ЭтотОбъект.ДополнительныеСвойства.Вставить("КомментарииСогласований", Новый Соответствие);	
		ЗаполнитьПредыдущиеОтказыВСогласовании();
		ЗаполнитьПланированиеСогласований();
	КонецЕсли;
КонецПроцедуры

Процедура ПриЗаписиДокумента()
	Если ЭтотОбъект.ВидВзаимодействия = Справочники.ВидыВзаимодействий.СогласованиеУслуг Тогда
		ЗарегистроватьСогласования();
		ИзменитьСоглашенияОтказовВСогласовании();
		ОбработатьПросроченныеСогласования();
		ОбработатьКоличествоСогласований();
		ЗаписатьКомментарииСогласований();
	КонецЕсли;
КонецПроцедуры

Процедура ПриУдаленииДокумента()
	ВзаимодействияПоликлиника.ОтменитьРегистрациюСогласований(ЭтотОбъект);
КонецПроцедуры

Процедура ЗарегистроватьСогласования()
	__ПРОВЕРКА__(ЭтотОбъект.ДополнительныеСвойства.Свойство("ДатаЗаписи"), "Нет свойства - дата записи");
	ВзаимодействияПоликлиника.ОтменитьРегистрациюСогласований(ЭтотОбъект);
	Для Каждого Строка_ Из ЭтотОбъект.Согласования Цикл
		ВзаимодействияПоликлиника.ЗарегистрироватьСогласование(
			Строка_.УИД, 
			Строка_.Согласование, 
			Строка_.СтатусСогласования, 
			ЭтотОбъект, 
			ЭтотОбъект.ДополнительныеСвойства.ДатаЗаписи, 
			Строка_.ДатаОкончанияДействияСогласования, 
			Строка_.КоличествоЗаказано, 
			Строка_.КоличествоСогласовано
		);
	КонецЦикла;
КонецПроцедуры

Процедура ИзменитьСоглашенияОтказовВСогласовании()
	ПредыдущиеОтказы_ = ПолучитьПредыдущиеОтказыВСогласовании();
	ТекущиеОтказы_ = ПолучитьТекущиеОтказыВСогласовании();
	Индекс_ = ТекущиеОтказы_.Количество() - 1;
	Пока Индекс_ >= 0 Цикл
		СтрокаТекущегоОтказа_ = ТекущиеОтказы_[Индекс_];
		СтрокаПредыдущегоОтказа_ = ПредыдущиеОтказы_.Найти(СтрокаТекущегоОтказа_.УИД, "УИД");
		Если СтрокаПредыдущегоОтказа_ <> Неопределено Тогда
			ПредыдущиеОтказы_.Удалить(СтрокаПредыдущегоОтказа_);
		КонецЕсли;
		Индекс_ = Индекс_ - 1;
	КонецЦикла;
	Если ПредыдущиеОтказы_.Количество() > 0 Тогда
		ВосстановитьСоглашенияСогласований(ПредыдущиеОтказы_);	
	КонецЕсли;
	Если ТекущиеОтказы_.Количество() > 0 Тогда
		УстановитьПлатноеСоглашениеСогласований(ТекущиеОтказы_);	
	КонецЕсли;
КонецПроцедуры

Процедура ЗаполнитьПредыдущиеОтказыВСогласовании()
	Если ЗначениеЗаполнено(ЭтотОбъект.Ссылка) Тогда
		ПредыдущиеОтказы_ = ОбщиеМеханизмы.СоздатьТаблицу("УИД, Соглашение, Согласование");
		Отказано_ = Перечисления.СтатусыСогласований.Отказано;
		Для Каждого СтрокаСогласования_ Из ЭтотОбъект.Ссылка.Согласования Цикл
			Если СтрокаСогласования_.СтатусСогласования = Отказано_ Тогда
				ЗаполнитьЗначенияСвойств(ПредыдущиеОтказы_.Добавить(), СтрокаСогласования_);	
			КонецЕсли;
		КонецЦикла;
		Если ПредыдущиеОтказы_.Количество() > 0 Тогда
			ЭтотОбъект.ДополнительныеСвойства.Вставить("ПредыдущиеОтказыВСогласовании", ПредыдущиеОтказы_);	
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

Функция ПолучитьТекущиеОтказыВСогласовании()
	ТекущиеОтказы_ = ОбщиеМеханизмы.СоздатьТаблицу("УИД, Соглашение, Согласование");
	Отказано_ = Перечисления.СтатусыСогласований.Отказано;
	Для Каждого СтрокаСогласования_ Из ЭтотОбъект.Согласования Цикл
		Если СтрокаСогласования_.СтатусСогласования = Отказано_ Тогда
			ЗаполнитьЗначенияСвойств(ТекущиеОтказы_.Добавить(), СтрокаСогласования_);	
		КонецЕсли;
	КонецЦикла;
	Возврат ТекущиеОтказы_;
КонецФункции

Функция ПолучитьПредыдущиеОтказыВСогласовании()
	ПредыдущиеОтказы_ = Неопределено;
	Если ЭтотОбъект.ДополнительныеСвойства.Свойство("ПредыдущиеОтказыВСогласовании") Тогда
		ПредыдущиеОтказы_ = ЭтотОбъект.ДополнительныеСвойства.ПредыдущиеОтказыВСогласовании.Скопировать();
	Иначе
		ПредыдущиеОтказы_ = ОбщиеМеханизмы.СоздатьТаблицу("УИД, Соглашение, Согласование");
	КонецЕсли;
	Возврат ПредыдущиеОтказы_;
КонецФункции

Процедура ВосстановитьСоглашенияСогласований(Согласования)
	__ПРОВЕРКА__(ЭтотОбъект.ДополнительныеСвойства.Свойство("ДатаЗаписи"), "Нет свойства - дата записи");
	Дата_ = ЭтотОбъект.ДополнительныеСвойства.ДатаЗаписи + 1;
	Соглашения_ = ОбщегоНазначения.ВыгрузитьКолонку(Согласования, "Соглашение", Истина);
	Для Каждого Соглашение_ Из Соглашения_ Цикл
		Отбор_ = Новый Структура("Соглашение", Соглашение_);
		СтрокиСогласований_ = Согласования.НайтиСтроки(Отбор_);
		УИДы_ = ОбщегоНазначения.ВыгрузитьКолонку(СтрокиСогласований_, "УИД");
		УправлениеЗаказамиУслугами.ИзменитьСоглашенияУслуг(УИДы_, Соглашение_, Дата_);
	КонецЦикла;
КонецПроцедуры

Процедура УстановитьПлатноеСоглашениеСогласований(Согласования)
	__ПРОВЕРКА__(ЭтотОбъект.ДополнительныеСвойства.Свойство("ДатаЗаписи"), "Нет свойства - дата записи");
	__ПРОВЕРКА__(
		ЭтотОбъект.ДополнительныеСвойства.Свойство("ПланированиеСогласований"), 
		"Нет свойства - планирование согласований"
	);
	Дата_ = ЭтотОбъект.ДополнительныеСвойства.ДатаЗаписи + 1;
	ПланированиеСогласований_ = ЭтотОбъект.ДополнительныеСвойства.ПланированиеСогласований;
	ОтказВСогласовании_ = Справочники.ПричиныОтменыУслугЗаказаПациента.ОтменаУслугПриОтказеВСогласовании;
	Для Каждого СтрокаСогласования_ Из Согласования Цикл
		ПланированиеСогласования_ = ПланированиеСогласований_.Найти(СтрокаСогласования_.УИД, "УИД");
		РабочееМесто_ = ?(ПланированиеСогласования_ <> Неопределено, ПланированиеСогласования_.РабочееМесто, Неопределено);
		Соглашение_ = РаботаССоглашениями.ПолучитьПлатноеСоглашение(Дата_, РабочееМесто_);
		Если ЗначениеЗаполнено(Соглашение_) Тогда
			УправлениеЗаказамиУслугами.ИзменитьСоглашениеУслуги(СтрокаСогласования_.УИД, Соглашение_, Дата_);
		Иначе
			Обработки.ОтменаУслугЗаказа.ОтменитьУслугу(СтрокаСогласования_.УИД, ОтказВСогласовании_);
			Параметры_ = Новый Структура("Номенклатура", СтрокаСогласования_.Согласование);
			СообщенияПользователю.Показать("Согласования_УслугаОтмененаПриОтказеВСогласовании", Параметры_);
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

Процедура ЗаполнитьПланированиеСогласований()
	ПланированиеСогласований_ = ОбщиеМеханизмы.СоздатьТаблицу("УИД, РабочееМесто, ЗапланированноеВремя");
	УИДы_ = ЭтотОбъект.Согласования.ВыгрузитьКолонку("УИД");
	Планирования_ = ПланированиеУслуг.ПолучитьДанныеПланированияУслуг(УИДы_);
	Для Каждого Планирование_ Из Планирования_ Цикл
		Строка_ = ПланированиеСогласований_.Добавить();
		Строка_.УИД = Планирование_.УникальныйИдентификаторУслуги;
		Строка_.РабочееМесто = Планирование_.МедицинскоеРабочееМесто;
		Строка_.ЗапланированноеВремя = Планирование_.ЗапланированноеВремя;
	КонецЦикла;
	ЭтотОбъект.ДополнительныеСвойства.Вставить(
		"ПланированиеСогласований", ПланированиеСогласований_
	);	
КонецПроцедуры

Процедура ОбработатьПросроченныеСогласования()
	__ПРОВЕРКА__(
		ЭтотОбъект.ДополнительныеСвойства.Свойство("ПланированиеСогласований"), 
		"Нет свойства - планирование согласований"
	);
	Согласовано_ = Перечисления.СтатусыСогласований.Согласовано;
	ПланированиеСогласований_ = ЭтотОбъект.ДополнительныеСвойства.ПланированиеСогласований;
	ПросроченныеСогласования_ = Новый Массив;
	Для Каждого Строка_ Из ПланированиеСогласований_ Цикл
		Если ЗначениеЗаполнено(Строка_.ЗапланированноеВремя) Тогда
			СтрокаСогласования_ = ЭтотОбъект.Согласования.Найти(Строка_.УИД, "УИД");
			__ПРОВЕРКА__(СтрокаСогласования_ <> Неопределено, "Не найдена строка согласования");
			Если СтрокаСогласования_.СтатусСогласования = Согласовано_ И 
				 ЗначениеЗаполнено(СтрокаСогласования_.ДатаОкончанияДействияСогласования) 
			Тогда
				Если КонецДня(СтрокаСогласования_.ДатаОкончанияДействияСогласования) < 
						Строка_.ЗапланированноеВремя 
				Тогда
					ПросроченныеСогласования_.Добавить(СтрокаСогласования_);
					Параметры_ = Новый Структура("Номенклатура, ЗапланированнаяДата, СрокСогласования");
					Параметры_.Номенклатура = СтрокаСогласования_.Согласование;
					Параметры_.ЗапланированнаяДата = Формат(Строка_.ЗапланированноеВремя, "ДФ=dd.MM.yyyy");
					Параметры_.СрокСогласования = Формат(
						СтрокаСогласования_.ДатаОкончанияДействияСогласования, "ДФ=dd.MM.yyyy"
					);
					СообщенияПользователю.Показать(
						"Согласования_ЗапланированноеВремяУслугиНеВходитВСрокСогласования", Параметры_
					);
				КонецЕсли;
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
	Если ПросроченныеСогласования_.Количество() > 0 Тогда
		УстановитьПлатноеСоглашениеСогласований(ПросроченныеСогласования_);	
	КонецЕсли;
КонецПроцедуры

Процедура ОбработатьКоличествоСогласований()
	__ПРОВЕРКА__(
		ЭтотОбъект.ДополнительныеСвойства.Свойство("КомментарииСогласований"), 
		"Нет свойства - комментарии согласований"
	);
	КомментарииСогласований_ = ЭтотОбъект.ДополнительныеСвойства.КомментарииСогласований;
	Для Каждого СтрокаСогласования_ Из ЭтотОбъект.Согласования Цикл
		Если СтрокаСогласования_.СтатусСогласования = Перечисления.СтатусыСогласований.Согласовано И 
			 СтрокаСогласования_.КоличествоСогласовано <> СтрокаСогласования_.КоличествоЗаказано 
		Тогда
			УправлениеЗаказамиУслугами.ИзменитьКоличествоУслуги(
				СтрокаСогласования_.УИД, СтрокаСогласования_.КоличествоСогласовано
			);
			Параметры_ = Новый Структура(
				"КоличествоСогласовано, КоличествоЗаказано", 
				СтрокаСогласования_.КоличествоСогласовано, СтрокаСогласования_.КоличествоЗаказано
			);
			Комментарий_ = СообщенияПользователю.Получить(
				"Согласования_КоличествоСогласованныхУслуг", Параметры_
			);
			КомментарийСогласования_ = КомментарииСогласований_.Получить(СтрокаСогласования_.УИД);
			Если КомментарийСогласования_ = Неопределено Тогда
				КомментарииСогласований_.Вставить(СтрокаСогласования_.УИД, Комментарий_);
			Иначе
				КомментарииСогласований_.Вставить(
					СтрокаСогласования_.УИД, КомментарийСогласования_ + Символы.ПС + Комментарий_
				);
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

Процедура ЗаписатьКомментарииСогласований()
	__ПРОВЕРКА__(ЭтотОбъект.ДополнительныеСвойства.Свойство("ДатаЗаписи"), "Нет свойства - дата записи");
	__ПРОВЕРКА__(
		ЭтотОбъект.ДополнительныеСвойства.Свойство("КомментарииСогласований"), 
		"Нет свойства - комментарии согласований"
	);
	КомментарииСогласований_ = ЭтотОбъект.ДополнительныеСвойства.КомментарииСогласований;
	Для Каждого СтрокаСогласования_ Из ЭтотОбъект.Согласования Цикл
		Комментарий_ = СтрокаСогласования_.Комментарий;
		
		КомментарийСогласования_ = КомментарииСогласований_.Получить(СтрокаСогласования_.УИД);
		Если КомментарийСогласования_ <> Неопределено Тогда
			Комментарий_ = Комментарий_ + Символы.ПС + КомментарийСогласования_;	
		КонецЕсли;
		
		Комментарий_ = СокрЛП(Комментарий_);
		Если Не ПустаяСтрока(Комментарий_) Тогда
			МеханизмыКомментариев.ЗаписатьКомментарий(
				СтрокаСогласования_.УИД, 
				ЭтотОбъект.ДополнительныеСвойства.ДатаЗаписи, 
				Комментарий_,, 
				Перечисления.ТипыКомментариев.МедицинскаяУслуга
			);	
		КонецЕсли;
	КонецЦикла;
КонецПроцедуры

#КонецОбласти

#КонецЕсли