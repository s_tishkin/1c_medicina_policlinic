﻿
#Область ПрограммныйИнтерфейс


&НаСервере
Функция ПолучитьКонтакт(ПараметрКоманды)
	Контакт = Неопределено;
	Если ТипЗнч(ПараметрКоманды) = Тип("ДокументСсылка.ЭлектронноеПисьмоВходящее") Тогда		
		ОтправительПисьма = ПараметрКоманды.ОтправительКонтакт;
		Если ЗначениеЗаполнено(ОтправительПисьма) Тогда
			Если ТипЗнч(ОтправительПисьма) = Тип("СправочникСсылка.КонтактныеЛицаПартнеров") Тогда
				Контакт = ОтправительПисьма;
			ИначеЕсли ТипЗнч(ОтправительПисьма) = Тип("СправочникСсылка.Партнеры") Тогда
				Контакт = ОтправительПисьма;
			КонецЕсли;
		КонецЕсли;
	ИначеЕсли ТипЗнч(ПараметрКоманды) = Тип("СправочникСсылка.Партнеры") Тогда
		Контакт = ПараметрКоманды;
	ИначеЕсли ТипЗнч(ПараметрКоманды) = Тип("СправочникСсылка.КонтактныеЛицаПартнеров") Тогда
		Контакт = ПараметрКоманды;
	КонецЕсли;
	Возврат Контакт;	
КонецФункции // ПолучитьКонтакт()

&НаКлиенте
// Процедура - обработчик открытия формы настроек СМС
//
// Параметры:
//	Ответ					- КодВозвратаДиалога	- Ответ на вопрос.
//	ДополнительныеПараметры	- Структура				- Структура дополнительных параметров
//
Процедура ОткрытьФормуНастроекСМС(Ответ, ДополнительныеПараметры) Экспорт
	Если НЕ (Ответ = КодВозвратаДиалога.Да) Тогда Возврат; КонецЕсли;
	ОткрытьФорму("ОбщаяФорма.смсФормаНастроек");
КонецПроцедуры // ОткрытьФормуНастроекСМС()

&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	Если НЕ смсРаботаССообщениями.ИспользоватьСМС() Тогда
		CRM_ОбщегоНазначенияКлиент.ОткрытьФормуОшибкиПодключения("СМС");
		Возврат;
	ИначеЕсли НЕ смсРаботаССообщениями.ПроверитьЗаполнениеКонстант(Ложь) Тогда
		ТекстВопроса = НСтр("ru='Отправка SMS невозможна так как не установлены параметры подключения.
						|Открыть форму настроек?'");
		ДополнительныеПараметры = Новый Структура;
		ОповещениеОтвета = Новый ОписаниеОповещения("ОткрытьФормуНастроекСМС", ЭтотОбъект, ДополнительныеПараметры);
		ПоказатьВопрос(ОповещениеОтвета, ТекстВопроса, РежимДиалогаВопрос.ДаНет, , , "Внимание!");
		Возврат;
	КонецЕсли;	
	Контакт = ПолучитьКонтакт(ПараметрКоманды[0]);
	Если ЗначениеЗаполнено(Контакт) Тогда
		СписокКонтактов = Новый СписокЗначений;
		СписокКонтактов.Добавить(Контакт);
		СписокТелефонов = сфпСофтФонПроКлиент.сфпЗаполнитьСписокТелефонов(СписокКонтактов);
	Иначе
		СписокТелефонов = Новый СписокЗначений;
	КонецЕсли;
	Если СписокТелефонов.Количество() > 0 Тогда
		СтруктураСписка = Новый Структура("СписокТелефонов", СписокТелефонов);
		ДанныеЗаполнения = Новый Структура();
		ДанныеЗаполнения.Вставить("Основание", СтруктураСписка);
		ОткрытьФорму("Документ.смсСообщение.ФормаОбъекта", ДанныеЗаполнения, ПараметрыВыполненияКоманды.Источник, ПараметрыВыполненияКоманды.Уникальность);
	Иначе	
		ОткрытьФорму("Документ.смсСообщение.ФормаОбъекта", , ПараметрыВыполненияКоманды.Источник, ПараметрыВыполненияКоманды.Уникальность);
	КонецЕсли;
КонецПроцедуры


#КонецОбласти