﻿
#Область ОбработчикиСобытий

Процедура ОбработкаПолученияФормы(ВидФормы, Параметры, ВыбраннаяФорма, ДополнительнаяИнформация, СтандартнаяОбработка)
	
	Если ВидФормы = "ФормаОбъекта" Тогда
		
		Если (Параметры.Свойство("Основание")
			И ЗначениеЗаполнено(Параметры.Основание))
			ИЛИ (Параметры.Свойство("Ключ") И Не ЗначениеЗаполнено(Параметры.Ключ)) Тогда
			
			СтандартнаяОбработка = Ложь;
			ВыбраннаяФорма = "ФормаДокументаРМК";
			
		ИначеЕсли Параметры.Свойство("Ключ") И ЗначениеЗаполнено(Параметры.Ключ) Тогда
			
			СтандартнаяОбработка = Ложь;
			ВыбраннаяФорма = "ФормаДокумента";
			
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область СлужебныеПроцедурыИФункции

#Область Проведение

Функция ДополнительныеИсточникиДанныхДляДвижений(ИмяРегистра) Экспорт

	ИсточникиДанных = Новый Соответствие;

	Возврат ИсточникиДанных; 

КонецФункции

Процедура ИнициализироватьДанныеДокумента(ДокументСсылка, ДополнительныеСвойства, Регистры = Неопределено) Экспорт
	
	////////////////////////////////////////////////////////////////////////////
	// Создадим запрос инициализации движений
	
	Запрос = Новый Запрос;
	ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка);
	
	ИнициализироватьКлючиАналитикиУчетаПоПартнерам(Запрос, ДокументСсылка);
	
	////////////////////////////////////////////////////////////////////////////
	// Сформируем текст запроса
	ТекстыЗапроса = Новый СписокЗначений;
	ТекстЗапросаТаблицаДенежныеСредстваВКассахККМ(Запрос, ТекстыЗапроса, Регистры);
	ТекстЗапросаТаблицаРасчетыПоЭквайрингу(Запрос, ТекстыЗапроса, Регистры);
	ТекстЗапросаТаблицаРасчетыСКлиентами(Запрос, ТекстыЗапроса, Регистры);
	ТекстЗапросаТаблицаВыручкаОтРеализации(Запрос, ТекстыЗапроса, Регистры);
	ТекстЗапросаБонусныеБаллы(Запрос, ТекстыЗапроса, Регистры);
	
	ПроведениеСервер.ИницализироватьТаблицыДляДвижений(Запрос, ТекстыЗапроса, ДополнительныеСвойства.ТаблицыДляДвижений, Истина);
	
КонецПроцедуры

Процедура ЗаполнитьПараметрыИнициализации(Запрос, ДокументСсылка)
	
	Запрос.МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	Запрос.УстановитьПараметр("Ссылка", ДокументСсылка);
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ДанныеДокумента.Ссылка                                        КАК Ссылка,
	|	ДанныеДокумента.Архивный                                      КАК Архивный,
	|	ДанныеДокумента.КассаККМ.Владелец                             КАК Организация,
	|	ДанныеДокумента.Партнер                                       КАК Партнер,
	|	ЗНАЧЕНИЕ(Справочник.Контрагенты.ПустаяСсылка)                 КАК Контрагент,
	|	ДанныеДокумента.Дата                                          КАК Период,
	|	ДанныеДокумента.Валюта                                        КАК Валюта,
	|	ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ВозвратДеньВДень) КАК ХозяйственнаяОперация,
	|	ЗНАЧЕНИЕ(Справочник.СтатьиДвиженияДенежныхСредств.ВозвратОплатыКлиенту) КАК СтатьяДвиженияДенежныхСредств,
	|	ИСТИНА                                                        КАК ЦенаВключаетНДС,
	|	ДанныеДокумента.Статус                                        КАК Статус,
	|	ДанныеДокумента.ЧекККМ.Дата                                   КАК ДатаЧекККМ,
	|	ДанныеДокумента.ЧекККМ.КартаЛояльности.Владелец.БонуснаяПрограммаЛояльности КАК БонуснаяПрограммаЛояльности
	|ИЗ
	|	Документ.ЧекККМВозврат КАК ДанныеДокумента
	|ГДЕ
	|	ДанныеДокумента.Ссылка = &Ссылка";
	Реквизиты = Запрос.Выполнить().Выбрать();
	Реквизиты.Следующий();
	
	Запрос.УстановитьПараметр("Архивный",                                 Реквизиты.Архивный);
	Запрос.УстановитьПараметр("Ссылка",                                   ДокументСсылка);
	Запрос.УстановитьПараметр("Период",                                   Реквизиты.Период);
	Запрос.УстановитьПараметр("Валюта",                                   Реквизиты.Валюта);
	Запрос.УстановитьПараметр("Организация",                              Реквизиты.Организация);
	Запрос.УстановитьПараметр("ЦенаВключаетНДС",                          ?(Реквизиты.ЦенаВключаетНДС, 0, 1));
	Запрос.УстановитьПараметр("ХозяйственнаяОперация",                    Реквизиты.ХозяйственнаяОперация);
	Запрос.УстановитьПараметр("СтатьяДвиженияДенежныхСредств",            Реквизиты.СтатьяДвиженияДенежныхСредств);
	Запрос.УстановитьПараметр("ЧекПробит",                                Реквизиты.Статус = Перечисления.СтатусыЧековККМ.Пробит);
	Запрос.УстановитьПараметр("Партнер",                                  Реквизиты.Партнер);
	Запрос.УстановитьПараметр("БонуснаяПрограммаЛояльности",              Реквизиты.БонуснаяПрограммаЛояльности);
	ТипыНоменклатурыНеТребующиеОтметки = Новый Массив;
	ТипыНоменклатурыНеТребующиеОтметки.Добавить(Справочники.ТипыНоменклатуры.Товар);
	Запрос.УстановитьПараметр("ТипыНоменклатурыНеТребующиеОтметки", ТипыНоменклатурыНеТребующиеОтметки);
	
КонецПроцедуры

Функция ТекстЗапросаТаблицаДенежныеСредстваВКассахККМ(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "ДенежныеСредстваВКассахККМ";
	
	Если НЕ ПроведениеСервер.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат "";
	КонецЕсли;
	
	Если НЕ ПроведениеСервер.ЕстьТаблицаЗапроса("ВтТаблицаСуммаПлатежнымиКартами", ТекстыЗапроса) Тогда
		ТекстЗапросаВтТаблицаСуммаПлатежнымиКартами(Запрос, ТекстыЗапроса);
	КонецЕсли; 
	
	ТекстЗапроса = 
	"ВЫБРАТЬ
	|	ДанныеДокумента.Дата КАК Период,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) КАК ВидДвижения,
	|	ДанныеДокумента.КассаККМ.Владелец КАК Организация,
	|	ДанныеДокумента.КассаККМ КАК КассаККМ,
	|	ДанныеДокумента.СуммаДокумента - ЕСТЬNULL(ТаблицаСуммаПлатежнымиКартами.Сумма, 0) КАК Сумма,
	|	ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ВозвратОплатыКлиенту) КАК ХозяйственнаяОперация,
	|	&СтатьяДвиженияДенежныхСредств КАК СтатьяДвиженияДенежныхСредств
	|ИЗ
	|	Документ.ЧекККМВозврат КАК ДанныеДокумента
	|		ЛЕВОЕ СОЕДИНЕНИЕ ВтТаблицаСуммаПлатежнымиКартами КАК ТаблицаСуммаПлатежнымиКартами
	|		ПО (ИСТИНА)
	|ГДЕ
	|	ДанныеДокумента.СуммаДокумента - ЕСТЬNULL(ТаблицаСуммаПлатежнымиКартами.Сумма, 0) <> 0
	|	И ДанныеДокумента.Ссылка = &Ссылка
	|	И (НЕ &Архивный)
	|	И &ЧекПробит
	|";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаТаблицаРасчетыПоЭквайрингу(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "РасчетыПоЭквайрингу";
	
	Если НЕ ПроведениеСервер.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат "";
	КонецЕсли; 
	
	ТекстЗапроса = "
	|ВЫБРАТЬ
	|	ТаблицаПлатежей.НомерСтроки КАК НомерСтроки,
	|	&Период КАК Период,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) КАК ВидДвижения,
	|	ЗНАЧЕНИЕ(Перечисление.ТипыДенежныхСредствПоЭквайрингу.СписаниеПоПлатежнойКарте) КАК ТипДенежныхСредств,
	|	&Организация                          КАК Организация,
	|	&Валюта                               КАК Валюта,
	|	ТаблицаПлатежей.ЭквайринговыйТерминал КАК ЭквайринговыйТерминал,
	|	ТаблицаПлатежей.КодАвторизации        КАК КодАвторизации,
	|	ТаблицаПлатежей.НомерПлатежнойКарты   КАК НомерПлатежнойКарты,
	|	ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ВозвратОплатыКлиентуНаПлатежнуюКарту) КАК ХозяйственнаяОперация,
	|	&СтатьяДвиженияДенежныхСредств        КАК СтатьяДвиженияДенежныхСредств,
	|	&Период                               КАК ДатаПлатежа,
	|	ТаблицаПлатежей.Сумма                 КАК Сумма
	|ИЗ
	|	Документ.ЧекККМВозврат.ОплатаПлатежнымиКартами КАК ТаблицаПлатежей
	|ГДЕ
	|	ТаблицаПлатежей.ОплатаОтменена
	|	И ТаблицаПлатежей.Ссылка = &Ссылка
	|	И (НЕ &Архивный)
	|УПОРЯДОЧИТЬ ПО
	|	НомерСтроки
	|";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаБонусныеБаллы(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "БонусныеБаллы";
	
	Если НЕ ПроведениеСервер.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат "";
	КонецЕсли;
	
	Если НЕ ПроведениеСервер.ЕстьТаблицаЗапроса("ВтБонусныеБаллы", ТекстыЗапроса) Тогда
		ТекстЗапросаВтБонусныеБаллы(Запрос, ТекстыЗапроса);
	КонецЕсли; 
	
	ТекстЗапроса = 
	"ВЫБРАТЬ
	|	БонусныеБаллы.Период                      КАК Период,
	|	БонусныеБаллы.ВидДвижения                 КАК ВидДвижения,
	|	БонусныеБаллы.БонуснаяПрограммаЛояльности КАК БонуснаяПрограммаЛояльности,
	|	БонусныеБаллы.Партнер                     КАК Партнер,
	|	СУММА(БонусныеБаллы.Начислено)            КАК Начислено,
	|	СУММА(БонусныеБаллы.КСписанию)            КАК КСписанию
	|ИЗ
	|	ВтБонусныеБаллы КАК БонусныеБаллы
	|СГРУППИРОВАТЬ ПО
	|	БонусныеБаллы.БонуснаяПрограммаЛояльности,
	|	БонусныеБаллы.Партнер,
	|	БонусныеБаллы.Период,
	|	БонусныеБаллы.ВидДвижения
	|ИМЕЮЩИЕ
	|	СУММА(БонусныеБаллы.Начислено) <> 0 ИЛИ СУММА(БонусныеБаллы.КСписанию) <> 0
	|";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаВтТаблицаСуммаПлатежнымиКартами(Запрос, ТекстыЗапроса)
	
	ИмяРегистра = "ВтТаблицаСуммаПлатежнымиКартами";
	
	ТекстЗапроса = 
	"ВЫБРАТЬ
	|	СУММА(ТабличнаяЧасть.Сумма) КАК Сумма
	|ПОМЕСТИТЬ ВтТаблицаСуммаПлатежнымиКартами
	|ИЗ
	|	Документ.ЧекККМВозврат.ОплатаПлатежнымиКартами КАК ТабличнаяЧасть
	|ГДЕ
	|	ТабличнаяЧасть.Ссылка = &Ссылка
	|	И ТабличнаяЧасть.ОплатаОтменена
	|";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
	Возврат ТекстЗапроса;
	
КонецФункции

Функция ТекстЗапросаВтБонусныеБаллы(Запрос, ТекстыЗапроса)
	
	ИмяРегистра = "ВтБонусныеБаллы";
	
	ТекстЗапроса = 
	"ВЫБРАТЬ
	|	ТабличнаяЧасть.ДатаНачисления                            КАК Период,
	|	ВЫБОР
	|		КОГДА ТабличнаяЧасть.СуммаБонусныхБаллов > 0
	|			ТОГДА ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)
	|		ИНАЧЕ ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход)
	|	КОНЕЦ КАК ВидДвижения,
	|	ТабличнаяЧасть.БонуснаяПрограммаЛояльности               КАК БонуснаяПрограммаЛояльности,
	|	&Партнер                                                 КАК Партнер,
	|	ВЫБОР
	|		КОГДА ТабличнаяЧасть.СуммаБонусныхБаллов > 0
	|			ТОГДА -ТабличнаяЧасть.СуммаБонусныхБаллов
	|		ИНАЧЕ ТабличнаяЧасть.СуммаБонусныхБаллов
	|	КОНЕЦ КАК Начислено,
	|	0                                                        КАК КСписанию
	|ПОМЕСТИТЬ ВтБонусныеБаллы
	|ИЗ
	|	Документ.ЧекККМВозврат.БонусныеБаллы КАК ТабличнаяЧасть
	|ГДЕ
	|	ТабличнаяЧасть.Ссылка = &Ссылка
	|	И НЕ &Партнер = ЗНАЧЕНИЕ(Справочник.Партнеры.ФизЛицо)
	|	И &ЧекПробит
	|	И НЕ &Архивный
	|	И ТабличнаяЧасть.ДатаНачисления <> ДатаВремя(1,1,1)
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	ТабличнаяЧасть.ДатаСписания                              КАК Период,
	|	ВЫБОР
	|		КОГДА ТабличнаяЧасть.СуммаБонусныхБаллов > 0
	|			ТОГДА ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)
	|		ИНАЧЕ ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход)
	|	КОНЕЦ КАК ВидДвижения,
	|	ТабличнаяЧасть.БонуснаяПрограммаЛояльности               КАК БонуснаяПрограммаЛояльности,
	|	&Партнер                                                 КАК Партнер,
	|	0                                                        КАК Начислено,
	|	ВЫБОР
	|		КОГДА ТабличнаяЧасть.СуммаБонусныхБаллов > 0
	|			ТОГДА -ТабличнаяЧасть.СуммаБонусныхБаллов
	|		ИНАЧЕ ТабличнаяЧасть.СуммаБонусныхБаллов
	|	КОНЕЦ КАК КСписанию
	|ИЗ
	|	Документ.ЧекККМВозврат.БонусныеБаллы КАК ТабличнаяЧасть
	|ГДЕ
	|	ТабличнаяЧасть.Ссылка = &Ссылка
	|	И НЕ &Партнер = ЗНАЧЕНИЕ(Справочник.Партнеры.ФизЛицо)
	|	И &ЧекПробит
	|	И НЕ &Архивный
	|	И ТабличнаяЧасть.ДатаСписания <> ДатаВремя(1,1,1)
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	НачалоПериода(ЧекККМВозврат.Дата, ДЕНЬ) КАК Период,
	|	ВЫБОР
	|		КОГДА ЧекККМВозврат.СуммаБонусныхБалловКВозврату > 0
	|			ТОГДА ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход)
	|		ИНАЧЕ ЗНАЧЕНИЕ(ВидДвиженияНакопления.Приход)
	|	КОНЕЦ КАК ВидДвижения,
	|	&БонуснаяПрограммаЛояльности КАК БонуснаяПрограммаЛояльности,
	|	&Партнер КАК Партнер,
	|	ВЫБОР
	|		КОГДА ЧекККМВозврат.СуммаБонусныхБалловКВозврату > 0
	|			ТОГДА -ЧекККМВозврат.СуммаБонусныхБалловКВозврату
	|		ИНАЧЕ ЧекККМВозврат.СуммаБонусныхБалловКВозврату
	|	КОНЕЦ КАК Начислено,
	|	ВЫБОР
	|		КОГДА ЧекККМВозврат.СуммаБонусныхБалловКВозврату > 0
	|			ТОГДА -ЧекККМВозврат.СуммаБонусныхБалловКВозврату
	|		ИНАЧЕ ЧекККМВозврат.СуммаБонусныхБалловКВозврату
	|	КОНЕЦ КАК КСписанию
	|ИЗ
	|	Документ.ЧекККМВозврат КАК ЧекККМВозврат
	|ГДЕ
	|	ЧекККМВозврат.Ссылка = &Ссылка
	|	И НЕ &Партнер = ЗНАЧЕНИЕ(Справочник.Партнеры.ФизЛицо)
	|	И &ЧекПробит
	|	И НЕ &Архивный
	|";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
	Возврат ТекстЗапроса;
	
КонецФункции

// Функция формирует текст запроса для таблицы расчетов с клиентами.
//
// Возвращаемое значение:
//	Строка - Текст запроса
//
Функция ТекстЗапросаТаблицаРасчетыСКлиентами(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "РасчетыСКлиентами";
	
	Если НЕ ПроведениеСервер.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат "";
	КонецЕсли;
	
	ЕстьПланЛечения_ = Метаданные.Документы.Найти("ПланЛечения") <> Неопределено;

	ТекстЗапроса = 
	"ВЫБРАТЬ
	|	РасшифровкаПлатежа.Ссылка.Дата КАК Период,
	|	РасшифровкаПлатежа.Ссылка.Дата КАК ДатаРегистратора,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход) КАК ВидДвижения,
	|	РегистрАналитикаУчетаПоПартнерам.КлючАналитики КАК АналитикаУчетаПоПартнерам,
	|	РасшифровкаПлатежа.Соглашение КАК ЗаказКлиента,
	|	РасшифровкаПлатежа.Ссылка КАК РасчетныйДокумент,
	|	РасшифровкаПлатежа.Ссылка.Валюта КАК Валюта,
	|	ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ВозвратОплатыКлиенту) КАК ХозяйственнаяОперация,
	|	РасшифровкаПлатежа.Ссылка.ЧекККМ.ФормаОплаты КАК ФормаОплаты,
	|	-РасшифровкаПлатежа.Сумма КАК Сумма,
	|	-РасшифровкаПлатежа.Сумма КАК КОплате
	|ИЗ
	|	Документ.ЧекККМВозврат.ОплатаСоглашений КАК РасшифровкаПлатежа
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.АналитикаУчетаПоПартнерам КАК РегистрАналитикаУчетаПоПартнерам
	|		ПО РасшифровкаПлатежа.Ссылка.Организация = РегистрАналитикаУчетаПоПартнерам.Организация
	|			И (ЕСТЬNULL(РасшифровкаПлатежа.Соглашение.Партнер, ЗНАЧЕНИЕ(Справочник.Партнеры.ФизЛицо)) = РегистрАналитикаУчетаПоПартнерам.Партнер)
	|			И ЕСТЬNULL(РасшифровкаПлатежа.Соглашение.Контрагент, РасшифровкаПлатежа.Соглашение.Пациент) = РегистрАналитикаУчетаПоПартнерам.Контрагент
	|ГДЕ
	|	РасшифровкаПлатежа.Ссылка = &Ссылка
	|	И &ЧекПробит
	|	И НЕ &Архивный
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ
	|	РасшифровкаПлатежа.Ссылка.Дата,
	|	РасшифровкаПлатежа.Ссылка.Дата,
	|	ЗНАЧЕНИЕ(ВидДвиженияНакопления.Расход),
	|	РегистрАналитикаУчетаПоПартнерам.КлючАналитики,
	|	ПРЕДСТАВЛЕНИЕ(РасшифровкаПлатежа.УникальныйИдентификаторУслуги),
	|	РасшифровкаПлатежа.Ссылка,
	|	РасшифровкаПлатежа.Ссылка.Валюта,
	|	ЗНАЧЕНИЕ(Перечисление.ХозяйственныеОперации.ВозвратОплатыКлиенту),
	|	РасшифровкаПлатежа.Ссылка.ЧекККМ.ФормаОплаты,
	|	-РасшифровкаПлатежа.Сумма,
	|	-РасшифровкаПлатежа.Сумма
	|ИЗ
	|	Документ.ЧекККМВозврат.МедицинскиеУслуги КАК РасшифровкаПлатежа
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.СтатусыУслуг КАК СтатусыУслуг
	|		ПО РасшифровкаПлатежа.УникальныйИдентификаторУслуги = СтатусыУслуг.УникальныйИдентификаторУслуги
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.АналитикаУчетаПоПартнерам КАК РегистрАналитикаУчетаПоПартнерам
	|		ПО РасшифровкаПлатежа.Ссылка.Организация = РегистрАналитикаУчетаПоПартнерам.Организация
	|			И (ВЫБОР
	|				КОГДА СтатусыУслуг.Соглашение.Партнер <> ЗНАЧЕНИЕ(Справочник.Партнеры.ПустаяСсылка)
	|					ТОГДА СтатусыУслуг.Соглашение.Партнер
	|				ИНАЧЕ ЗНАЧЕНИЕ(Справочник.Партнеры.ФизЛицо)
	|			КОНЕЦ = РегистрАналитикаУчетаПоПартнерам.Партнер)
	|			И (ЕСТЬNULL(СтатусыУслуг.Пациент, НЕОПРЕДЕЛЕНО) = РегистрАналитикаУчетаПоПартнерам.Контрагент)
	|ГДЕ
	|	РасшифровкаПлатежа.Ссылка = &Ссылка
	|	И &ЧекПробит
	|	И НЕ &Архивный";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);

	Возврат ТекстЗапроса;
	
КонецФункции // ТекстЗапросаТаблицаРасчетыСКлиентами()

Функция ТекстЗапросаТаблицаВыручкаОтРеализации(Запрос, ТекстыЗапроса, Регистры)
	
	ИмяРегистра = "ВыручкаОтреализации";
	
	Если НЕ ПроведениеСервер.ТребуетсяТаблицаДляДвижений(ИмяРегистра, Регистры) Тогда
		Возврат "";
	КонецЕсли;
	
	ТекстЗапроса =
	"ВЫБРАТЬ
	|	ДанныеДокумента.НомерСтроки КАК НомерСтроки,
	|	&Период КАК Период,
	|	СтатусыУслуг.Номенклатура КАК Номенклатура,
	|	СтатусыУслуг.Заказ КАК ЗаказПациента,
	|	РегистрАналитикаУчетаПоПартнерам.КлючАналитики КАК АналитикаУчетаПоПартнерам,
	|	НазначенныеУслугиИсполнители.Исполнитель КАК НазначившийСотрудник,
	|	СтатусыУслуг.Заказ.НаправившаяОрганизация КАК НаправившаяОрганизация,
	|	СтатусыУслуг.Соглашение КАК Соглашение,
	|	-ДанныеДокумента.Сумма КАК Сумма,
	|	-ДанныеДокумента.Количество КАК Количество
	|ИЗ
	|	Документ.ЧекККМВозврат.МедицинскиеУслуги КАК ДанныеДокумента
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.СтатусыУслуг КАК СтатусыУслуг
	|		ПО ДанныеДокумента.УникальныйИдентификаторУслуги = СтатусыУслуг.УникальныйИдентификаторУслуги
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.АналитикаУчетаПоПартнерам КАК РегистрАналитикаУчетаПоПартнерам
	|		ПО ДанныеДокумента.Ссылка.Организация = РегистрАналитикаУчетаПоПартнерам.Организация
	|			И (ВЫБОР
	|				КОГДА СтатусыУслуг.Соглашение.Партнер <> ЗНАЧЕНИЕ(Справочник.Партнеры.ПустаяСсылка)
	|					ТОГДА СтатусыУслуг.Соглашение.Партнер
	|				ИНАЧЕ ЗНАЧЕНИЕ(Справочник.Партнеры.ФизЛицо)
	|			КОНЕЦ = РегистрАналитикаУчетаПоПартнерам.Партнер)
	|			И (ЕСТЬNULL(СтатусыУслуг.Пациент, НЕОПРЕДЕЛЕНО) = РегистрАналитикаУчетаПоПартнерам.Контрагент)
	|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.МедицинскийДокумент.Исполнители КАК НазначенныеУслугиИсполнители
	|		ПО (СтатусыУслуг.Назначение = НазначенныеУслугиИсполнители.Ссылка)
	|			И (НазначенныеУслугиИсполнители.Ссылка.Проведен)
	|			И (НазначенныеУслугиИсполнители.ОтветственныйЗаНазначение)
	|ГДЕ
	|	ДанныеДокумента.Ссылка = &Ссылка
	|	И (ДанныеДокумента.Номенклатура.НеТребуетОтметкиИсполнения
	|			ИЛИ ДанныеДокумента.Номенклатура.ТипНоменклатуры В (&ТипыНоменклатурыНеТребующиеОтметки))
	|	И &ЧекПробит
	|	И НЕ &Архивный";
	
	ТекстыЗапроса.Добавить(ТекстЗапроса, ИмяРегистра);
	
	Возврат ТекстЗапроса;
	
КонецФункции // ТекстЗапросаТаблицаРасчетыСКлиентами()

#КонецОбласти

#Область Печать

// Заполняет список команд печати.
// 
// Параметры:
//   КомандыПечати - ТаблицаЗначений - состав полей см. в функции УправлениеПечатью.СоздатьКоллекциюКомандПечати.
//
Процедура ДобавитьКомандыПечати(КомандыПечати) Экспорт
	
	// КМ-3
	КомандаПечати = КомандыПечати.Добавить();
	КомандаПечати.Обработчик = "УправлениеПечатьюПоликлиникаКлиент.ПечатьКМ3";
	КомандаПечати.МенеджерПечати = "Обработка.ПечатьКМ3";
	КомандаПечати.Идентификатор = "КМ3";
	КомандаПечати.Представление = НСтр("ru = 'КМ-3'");
	
КонецПроцедуры

// Процедура печати документа.
//
Процедура Печать(МассивОбъектов, ПараметрыПечати, КоллекцияПечатныхФорм, ОбъектыПечати, ПараметрыВывода) Экспорт
	
КонецПроцедуры

// Функция получает данные для формирования печатной формы КМ-3.
//
Функция ПолучитьДанныеДляПечатнойФормыКМ3(ПараметрыПечати, МассивОбъектов) Экспорт
	
	УстановитьПривилегированныйРежим(Истина);
	
	МенеджерВременныхТаблиц = Новый МенеджерВременныхТаблиц;
	ОтветственныеЛицаСервер.СформироватьВременнуюТаблицуОтветственныхЛицДокументов(МассивОбъектов, МенеджерВременныхТаблиц);
	
	Запрос = Новый Запрос;
	Запрос.МенеджерВременныхТаблиц = МенеджерВременныхТаблиц;
	
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ДокЧек.Ссылка КАК Ссылка,
	|	ДокЧек.Номер КАК Номер,
	|	ДокЧек.Дата КАК ДатаДокумента,
	|	ДокЧек.КассаККМ КАК КассаККМ,
	|	ДокЧек.КассаККМ.ТипКассы КАК ТипКассы,
	|	ДокЧек.КассаККМ.Представление КАК Покупатель,
	|	ДокЧек.Кассир.ФизическоеЛицо КАК КассирККМ,
	|	ДокЧек.Валюта КАК Валюта,
	|	ДокЧек.Организация КАК Организация,
	|	ТаблицаОтветственныеЛица.РуководительНаименование КАК Руководитель,
	|	ТаблицаОтветственныеЛица.РуководительДолжность КАК ДолжностьРуководителя,
	|	ТаблицаОтветственныеЛица.ГлавныйБухгалтерНаименование КАК ГлавныйБухгалтер,
	|	ДокЧек.Организация.Представление КАК Поставщик,
	|	ДокЧек.КассаККМ.СерийныйНомер КАК СерийныйНомерККМ,
	|	ДокЧек.КассаККМ.РегистрационныйНомер КАК РегистрационныйНомерККМ,
	|	ДокЧек.КассаККМ.Наименование КАК ПредставлениеККМ
	|ИЗ
	|	Документ.ЧекККМВозврат КАК ДокЧек
	|		ЛЕВОЕ СОЕДИНЕНИЕ ТаблицаОтветственныеЛица КАК ТаблицаОтветственныеЛица
	|		ПО ДокЧек.Ссылка = ТаблицаОтветственныеЛица.Ссылка
	|ГДЕ
	|	ДокЧек.Ссылка В(&МассивОбъектов)
	|
	|УПОРЯДОЧИТЬ ПО
	|	ДокЧек.Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ДокЧек.Ссылка              КАК Документ,
	|	ЖурналФискальныхОпераций.ФискальнаяОперацияНомерЧекаККМ КАК НомерЧека,
	|	ДокЧек.СуммаДокумента      КАК СуммаДокумента
	|ИЗ
	|	Документ.ЧекККМВозврат КАК ДокЧек
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЖурналФискальныхОпераций КАК ЖурналФискальныхОпераций
	|		ПО ЖурналФискальныхОпераций.ДокументОснование = ДокЧек.Ссылка
	|ГДЕ
	|	ДокЧек.Ссылка В(&МассивОбъектов)
	|
	|УПОРЯДОЧИТЬ ПО
	|	Документ
	|ИТОГИ ПО
	|	Документ";
	
	Запрос.УстановитьПараметр("МассивОбъектов", МассивОбъектов);
	
	РезультатЗапроса = Запрос.ВыполнитьПакет();
	
	СтруктураДанныхДляПечати    = Новый Структура("РезультатЗапроса, ЗаголовокДокумента",
		РезультатЗапроса, НСтр("ru = 'КМ-3'"));
	
	Если ПривилегированныйРежим() Тогда
		УстановитьПривилегированныйРежим(Ложь);
	КонецЕсли;
	
	Возврат СтруктураДанныхДляПечати;
	
КонецФункции

// Процедура создает ключи аналитики учета по партнерам.
//
// Параметры:
//	Реквизиты - Структура - Реквизиты шапки документа.
//	ДокументСсылка - Текущий документ
//
Процедура ИнициализироватьКлючиАналитикиУчетаПоПартнерам(Запрос, ДокументСсылка)

	ТекстЗапроса_ =
	"ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	РасшифровкаПлатежа.Соглашение.Организация КАК Организация,
	|	ЕСТЬNULL(РасшифровкаПлатежа.Соглашение.Партнер, ЗНАЧЕНИЕ(Справочник.Партнеры.ФизЛицо)) КАК Партнер,
	|	ЕСТЬNULL(РасшифровкаПлатежа.Соглашение.Контрагент, РасшифровкаПлатежа.Соглашение.Пациент) КАК Контрагент
	|ИЗ
	|	Документ.ЧекККМВозврат.ОплатаСоглашений КАК РасшифровкаПлатежа
	|ГДЕ
	|	РасшифровкаПлатежа.Ссылка = &Ссылка
	|	И &ЧекПробит
	|	И НЕ &Архивный
	|
	|ОБЪЕДИНИТЬ
	|
	|ВЫБРАТЬ
	|	РасшифровкаПлатежа.Ссылка.Организация,
	|	ВЫБОР
	|		КОГДА СтатусыУслуг.Соглашение.Партнер <> ЗНАЧЕНИЕ(Справочник.Партнеры.ПустаяСсылка)
	|			ТОГДА СтатусыУслуг.Соглашение.Партнер
	|		ИНАЧЕ ЗНАЧЕНИЕ(Справочник.Партнеры.ФизЛицо)
	|	КОНЕЦ,
	|	СтатусыУслуг.Пациент
	|ИЗ
	|	Документ.ЧекККМВозврат.МедицинскиеУслуги КАК РасшифровкаПлатежа
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.СтатусыУслуг КАК СтатусыУслуг
	|		ПО РасшифровкаПлатежа.УникальныйИдентификаторУслуги = СтатусыУслуг.УникальныйИдентификаторУслуги
	|ГДЕ
	|	РасшифровкаПлатежа.Ссылка = &Ссылка
	|	И &ЧекПробит
	|	И НЕ &Архивный";
	
	Запрос.Текст = ТекстЗапроса_;
	
	Выборка = Запрос.Выполнить().Выбрать();
	Пока Выборка.Следующий() Цикл
		РегистрыСведений.АналитикаУчетаПоПартнерам.ЗначениеКлючаАналитики(Выборка);
	КонецЦикла;

КонецПроцедуры

#КонецОбласти

#КонецОбласти

#КонецЕсли
