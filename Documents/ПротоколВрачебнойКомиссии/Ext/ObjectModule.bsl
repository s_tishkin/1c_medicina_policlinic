﻿
#Область ПрограммныйИнтерфейс

#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

////
 // Процедура: ОбработкаЗаполнения
 //   Обработчик события объекта ОбработкаЗаполнения.
 //   Инициализирует реквизиты документа.
 //
 // Параметры:
 //   ДанныеЗаполнения {Произвольный}
 //     Значения используются как основания для заполнения.
 //   СтандартнаяОбработка {Булево}
 //     Признак выполнения системной обработки события.
 ///
Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	
	ЭтотОбъект.Дата = ТекущаяДатаСеанса();
	ЭтотОбъект.Ответственный = Пользователи.ТекущийПользователь();
	
	Если ТипЗнч(ДанныеЗаполнения) = Тип("Структура") Тогда
		ЗаполнитьЗначенияСвойств(ЭтотОбъект, ДанныеЗаполнения);
	КонецЕсли;

КонецПроцедуры

#КонецЕсли

#КонецОбласти