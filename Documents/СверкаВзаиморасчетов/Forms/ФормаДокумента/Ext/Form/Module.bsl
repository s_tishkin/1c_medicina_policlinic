﻿
#Область ПрограммныйИнтерфейс


///////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

// Процедура заполнения табличной части по остаткам.
//
&НаСервере
Процедура ЗаполнитьПоОстаткамСервер()
	                                                 
	ДанныеДокумента = Новый Структура;
	ДанныеДокумента.Вставить("Дата",			   Объект.Дата);
	ДанныеДокумента.Вставить("Организация",		   Объект.Организация);
	ДанныеДокумента.Вставить("Контрагент",		   Объект.Контрагент);
	ДанныеДокумента.Вставить("КонецПериода", 	   Объект.КонецПериода);
	
	Документы.СверкаВзаиморасчетов.ЗаполнитьДанныеКонтрагента(ДанныеДокумента, Объект.ДанныеКонтрагента);

КонецПроцедуры // ЗаполнитьПоОстаткамСервер()

// Заполняет описание расчетного документа и валюту взаиморасчетов в табличной части.
//
// Параметры:
//    ДокументСсылка - ДокументСсылка - Ссылка на расчетный документ;
//    ОписаниеДокумента - Строка - Переменная, в которую будет передано описание расчетного документа;
//    ВалютаВзаиморасчетов - СправочникСсылка.Валюты - Переменная, в которую будет передано значение валюты взаиморасчетов.
//
&НаСервере
Процедура ЗаполнитьСтрокуПоРасчетномуДокументуСервер(ДокументСсылка, ОписаниеДокумента, ВалютаВзаиморасчетов)
	
	СведенияОбОрганизации = ФормированиеПечатныхФорм.СведенияОЮрФизЛице(Объект.Организация, Объект.Дата);
	СведенияОКонтрагенте  = ФормированиеПечатныхФорм.СведенияОЮрФизЛице(Объект.Контрагент,  Объект.Дата);
	
	Запрос = Новый Запрос;
	Запрос.Текст = "
	|ВЫБРАТЬ
	|	ДанныеРасчетногоДокумента.Номер  КАК Номер,
	|	ДанныеРасчетногоДокумента.Дата   КАК Дата,
	|	ДанныеРасчетногоДокумента.Валюта КАК ВалютаВзаиморасчетов
	|ИЗ
	|	Справочник.СоглашенияСКлиентами КАК ДанныеРасчетногоДокумента
	|ГДЕ
	|	ДанныеРасчетногоДокумента.Ссылка = &Ссылка";
	
	Запрос.УстановитьПараметр("Ссылка", ДокументСсылка);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Если Выборка.Следующий() Тогда
	
		ОписаниеДокумента = Документы.СверкаВзаиморасчетов.ПолучитьОписаниеРасчетногоДокумента(ДокументСсылка,
																								  Выборка.Номер,
																								  Выборка.Дата,
																								  СведенияОбОрганизации,
																								  СведенияОКонтрагенте);
		ВалютаВзаиморасчетов = Выборка.ВалютаВзаиморасчетов;
		
    КонецЕсли;
	
КонецПроцедуры // ЗаполнитьСтрокуПоРасчетномуДокументуСервер()

&НаСервереБезКонтекста
Функция ПартнерКонтрагента(Контрагент)
	
	Возврат ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Контрагент, "Партнер");
	
КонецФункции

// Настраивает доступность элементов формы в зависимости от статуса документа
//
&НаСервере
Процедура УстановитьДоступностьЭлементовПоСтатусуСервер()
	
	ТолькоПросмотрЭлементов = (Объект.Статус = Перечисления.СтатусыСверокВзаиморасчетов.Сверена);
	
	МассивЭлементов = Новый Массив();
	
	// Элементы управления шапки
	МассивЭлементов.Добавить("Дата");
	МассивЭлементов.Добавить("Организация");
	МассивЭлементов.Добавить("НачалоПериода");
	МассивЭлементов.Добавить("КонецПериода");
	МассивЭлементов.Добавить("Контрагент");
	
	// Группы элементов управления
	МассивЭлементов.Добавить("ГруппаСтраницы");

	ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементовФормы(Элементы, МассивЭлементов, "ТолькоПросмотр", ТолькоПросмотрЭлементов);
	
	МассивЭлементов = Новый Массив();             
	
	// Элементы управления шапки
	МассивЭлементов.Добавить("УстановитьИнтервал");
	МассивЭлементов.Добавить("РасчетыКомандаЗаполнитьПоОстаткам");
	
	ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементовФормы(Элементы, МассивЭлементов, "Доступность", НЕ ТолькоПросмотрЭлементов);

КонецПроцедуры // УстановитьДоступностьЭлементовПоСтатусуСервер()

// Заполняет данные контрагента для печати по последнему документу
//
&НаСервере
Процедура ЗаполнитьДанныеКонтрагентаДляПечатиСервер()
	
	Документы.СверкаВзаиморасчетов.ЗаполнитьДанныеКонтрагентаДляПечатиПоПоследнемуДокументу(Объект.Контрагент,
																							   Объект.ФИОРуководителяКонтрагента,
																							   Объект.ДолжностьРуководителяКонтрагента);
КонецПроцедуры // ЗаполнитьДанныеКонтрагентаДляПечатиСервер()

///////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ ЭЛЕМЕНТОВ ФОРМЫ

// Процедура - обработчик события "ПриИзменении" поля "Контрагент".
//
&НаКлиенте
Процедура КонтрагентПриИзменении(Элемент)
	
	Если ЗначениеЗаполнено(Объект.Контрагент)
		И (НЕ ЗначениеЗаполнено(Объект.ФИОРуководителяКонтрагента)
		ИЛИ НЕ ЗначениеЗаполнено(Объект.ДолжностьРуководителяКонтрагента)) Тогда

		ЗаполнитьДанныеКонтрагентаДляПечатиСервер();
		
	КонецЕсли;
	
КонецПроцедуры // КонтрагентПриИзменении()

// Процедура - обработчик события "ПриИзменении" поля "Статус".
//
&НаКлиенте
Процедура СтатусПриИзменении(Элемент)
	
	УстановитьДоступностьЭлементовПоСтатусуСервер()
	
КонецПроцедуры // СтатусПриИзменении()

// Процедура - обработчик события "ПриИзменении" поля "РасчетныйДокумент" таблицы "ДанныеКонтрагента".
//
&НаКлиенте
Процедура ДанныеКонтрагентаРасчетныйДокументПриИзменении(Элемент)

	ТекущиеДанные = Элементы.ДанныеКонтрагента.ТекущиеДанные;
	
	Если ЗначениеЗаполнено(ТекущиеДанные.РасчетныйДокумент) Тогда
		
		ЗаполнитьСтрокуПоРасчетномуДокументуСервер(ТекущиеДанные.РасчетныйДокумент,
												   ТекущиеДанные.ОписаниеДокумента,
												   ТекущиеДанные.ВалютаВзаиморасчетов);

	КонецЕсли;
	
КонецПроцедуры // ДанныеКонтрагентаРасчетныйДокументПриИзменении()

// Процедура - обработчик события "НачалоВыбора" поля "ФИОРуководителяКонтрагента".
//
&НаКлиенте
Процедура ФИОРуководителяКонтрагентаНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	СтандартнаяОбработка = Ложь;
	
	ФИОРуководителяКонтактноеЛицоНачалоВыбора();
	
КонецПроцедуры // ФИОРуководителяКонтрагентаНачалоВыбора()

&НаКлиенте
Функция ФИОРуководителяКонтактноеЛицоНачалоВыбора()
	
	Если ЗначениеЗаполнено(Объект.Контрагент) Тогда
		Отбор = Новый Структура("Владелец", ПартнерКонтрагента(Объект.Контрагент));
		ОткрытьФорму("Справочник.КонтактныеЛицаПартнеров.ФормаВыбора",
			Новый Структура("Отбор", Отбор),
			ЭтотОбъект);
	Иначе
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
			НСтр("ru = 'Поле ""Контрагент"" не заполнено'"),
			,
			"Контрагент",
			"Объект");
	КонецЕсли; 
	
КонецФункции


// Процедура - обработчик события "НачалоВыбора" поля "Комментарий".
//
&НаКлиенте
Процедура КомментарийНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ОбщегоНазначенияКлиент.ПоказатьФормуРедактированияКомментария(Элемент.ТекстРедактирования, ЭтотОбъект, "Объект.Комментарий");
	
КонецПроцедуры // КомментарийНачалоВыбора()

///////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

// Процедура - обработчик события формы "ПриЧтенииНаСервере".
//
&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	УстановитьДоступностьЭлементовПоСтатусуСервер();

КонецПроцедуры // ПриЧтенииНаСервере()

// Процедура - обработчик события формы "ПослеЗаписиНаСервере".
//
&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	УстановитьДоступностьЭлементовПоСтатусуСервер()
	
КонецПроцедуры // ПослеЗаписиНаСервере()

// Процедура - обработчик события формы "ОбработкаВыбора".
//
&НаКлиенте
Процедура ОбработкаВыбора(ВыбранноеЗначение, ИсточникВыбора)
	
	Если ТипЗнч(ВыбранноеЗначение) = Тип("СправочникСсылка.КонтактныеЛицаПартнеров") Тогда
		Объект.ФИОРуководителяКонтрагента = Строка(ВыбранноеЗначение);
	КонецЕсли;
	
КонецПроцедуры // ОбработкаВыбора()

//////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД

// Процедура - обработчик команды "КомандаЗаполнитьПоОстаткам".
//
&НаКлиенте
Процедура КомандаЗаполнитьПоОстаткам(Команда)
	
	СтруктураРеквизитов = Новый Структура;
	СтруктураРеквизитов.Вставить("Организация");
	СтруктураРеквизитов.Вставить("Контрагент");
	СтруктураРеквизитов.Вставить("КонецПериода");
	
//	Если ОбщегоНазначенияУТКлиент.ПроверитьВозможностьЗаполненияТабличнойЧасти(ЭтотОбъект, 
//	Объект.ДанныеКонтрагента, СтруктураРеквизитов) Тогда
//
//		ЗаполнитьПоОстаткамСервер();
//		
//	КонецЕсли;
	Оповещение_ = Новый ОписаниеОповещения("ЗаполнитьПоОстаткамЗавершение", ЭтотОбъект);
	ОбщегоНазначенияУТКлиент.ПроверитьВозможностьЗаполненияТабличнойЧасти(
		Оповещение_, 
		ЭтаФорма,
		Объект.ДанныеКонтрагента,
		СтруктураРеквизитов);

КонецПроцедуры // КомандаЗаполнитьПоОстаткам()

&НаКлиенте
Процедура ЗаполнитьПоОстаткамЗавершение(Результат, ДополнительныеПараметры) Экспорт 
	
	ЗаполнитьПоОстаткамСервер();
	
КонецПроцедуры

// Процедура - обработчик команды "УстановитьИнтервал".
//
&НаКлиенте
Процедура УстановитьИнтервал(Команда)
	
	Диалог = Новый ДиалогРедактированияСтандартногоПериода();
	Диалог.Период.ДатаНачала = Объект.НачалоПериода;
	Диалог.Период.ДатаОкончания = Объект.КонецПериода;
	
	Диалог.Показать(Новый ОписаниеОповещения("УстановитьИнтервалПослеЗакрытияДиалога", ЭтотОбъект, Диалог));
	
КонецПроцедуры // УстановитьИнтервал()

&НаКлиенте
////
 // Процедура: УстановитьИнтервалПослеЗакрытияДиалога
 //   Вызывается после закрытия диалога
 //
 // Параметры:
 //   П1 (Тип)
 //     Описание
 //   П2 (Тип)
 //     Описание
///
Процедура УстановитьИнтервалПослеЗакрытияДиалога(Результат, Диалог) Экспорт
	Если Результат <> Неопределено Тогда
		Объект.НачалоПериода = Результат.ДатаНачала;
		Объект.КонецПериода = Результат.ДатаОкончания;
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтотОбъект, Элементы.ПодменюПечать);
	// Конец СтандартныеПодсистемы.Печать
	
	// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
	
КонецПроцедуры

&НаКлиенте
Процедура ОрганизацияПриИзменении(Элемент)
	ОрганизацияПриИзмененииНаСервере();
КонецПроцедуры

&НаСервере
Процедура ОрганизацияПриИзмененииНаСервере()
	ОтветственныеЛицаСервер.ПриИзмененииСвязанныхРеквизитовДокумента(ЭтотОбъект.Объект.Ссылка);
КонецПроцедуры

#Область ОбработчикиКомандФормы

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтотОбъект, Объект);
	
КонецПроцедуры


// Конец СтандартныеПодсистемы.Печать

#КонецОбласти

#КонецОбласти