﻿
#Область ПрограммныйИнтерфейс

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ (НА СЕРВЕРЕ БЕЗ КОНТЕКСТА)

// Устанавливает статус "Создан" для массива документов
// 
// Параметры:
// МассивДокументов - Массив(ДокументСсылка.СверкаВзаиморасчетов) - массив документов для изменения статуса.
//
// Возвращаемое значение: Булево - результат выполнения функции
//
&НаСервереБезКонтекста
Функция УстановитьСтатусДокументовСоздан(Знач МассивДокументов)

	Возврат Документы.СверкаВзаиморасчетов.УстановитьСтатус(МассивДокументов, Перечисления.СтатусыСверокВзаиморасчетов.Создана);

КонецФункции // УстановитьСтатусДокументовСоздан()

// Устанавливает статус "На сверке" для массива документов
// 
// Параметры:
// МассивДокументов - Массив(ДокументСсылка.СверкаВзаиморасчетов) - массив документов для изменения статуса.
//
// Возвращаемое значение: Булево - результат выполнения функции
//
&НаСервереБезКонтекста
Функция УстановитьСтатусДокументовНаСверке(Знач МассивДокументов)

	Возврат Документы.СверкаВзаиморасчетов.УстановитьСтатус(МассивДокументов, Перечисления.СтатусыСверокВзаиморасчетов.НаСверке);

КонецФункции // УстановитьСтатусДокументовНаСверке()

// Устанавливает статус "Сверен" для массива документов
// 
// Параметры:
// МассивДокументов - Массив(ДокументСсылка.СверкаВзаиморасчетов) - массив документов для изменения статуса.
//
// Возвращаемое значение: Булево - результат выполнения функции
//
&НаСервереБезКонтекста
Функция УстановитьСтатусДокументовСверен(Знач МассивДокументов)

	Возврат Документы.СверкаВзаиморасчетов.УстановитьСтатус(МассивДокументов, Перечисления.СтатусыСверокВзаиморасчетов.Сверена);

КонецФункции // УстановитьСтатусДокументовСверен()

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ (НА КЛИЕНТЕ)

// Возвращает массив выделенных в списке документов
//
// Возвращаемое значение: массив
//
&НаКлиенте
Функция ПолучитьМассивДокументов()

	МассивДокументов = Новый Массив;
	Для Каждого Элемент Из Элементы.Список.ВыделенныеСтроки Цикл

		Если ТипЗнч(Элемент) = Тип("ДокументСсылка.СверкаВзаиморасчетов") Тогда
			МассивДокументов.Добавить(Элемент);
		КонецЕсли;

	КонецЦикла;

	Возврат МассивДокументов;

КонецФункции // ПолучитьМассивДокументов()

// Показывает оповещение после обработки выделенных в списке актов.
//
// Параметры:
// КоличествоОбработанных - Число - количество успешно обработанных заказов клиентов.
// КоличествоВыделенных   - Число - количество выделенных в списке актов
// Статус                 - Строка - Представление установленного статуса
//
&НаКлиенте
Процедура ОповеститьПользователяОбУстановкеСтатуса(КоличествоОбработанных, КоличествоВыделенных, Статус)

	Если КоличествоОбработанных > 0 Тогда

		Элементы.Список.Обновить();

		ТекстСообщения = НСтр("ru='Для %КоличествоОбработанных% из %КоличествоВсего% выделенных в списке документов установлен статус ""%Статус%""'");
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%КоличествоОбработанных%", КоличествоОбработанных);
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%КоличествоВсего%",        КоличествоВыделенных);
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Статус%",                 Статус);
		ТекстЗаголовка = НСтр("ru='Статус ""%Статус%"" установлен'");
		ТекстЗаголовка = СтрЗаменить(ТекстЗаголовка, "%Статус%", Статус);

	Иначе

		ТекстСообщения = НСтр("ru='Статус ""%Статус%"" не установлен ни для одного документа.'");
		ТекстСообщения = СтрЗаменить(ТекстСообщения, "%Статус%", Статус);
		ТекстЗаголовка = НСтр("ru='Статус ""%Статус%"" не установлен'");
		ТекстЗаголовка = СтрЗаменить(ТекстЗаголовка, "%Статус%", Статус);

	КонецЕсли;

	ПоказатьОповещениеПользователя(ТекстЗаголовка,, ТекстСообщения, БиблиотекаКартинок.Информация32);

КонецПроцедуры // ОповеститьПользователяОбУстановкеСтатуса()

///////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ ПОЛЕЙ ФОРМЫ

// Процедура - обработчик события "ПриИзменении" поля "МенеджерОтбор".
//
&НаКлиенте
Процедура МенеджерОтборПриИзменении(Элемент)
	
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список, "Менеджер", Менеджер, ЗначениеЗаполнено(Менеджер));
	
КонецПроцедуры // МенеджерОтборПриИзменении()

// Процедура - обработчик события "ПриИзменении" поля "СтатусОтбор".
//
&НаКлиенте
Процедура СтатусОтборПриИзменении(Элемент)
	
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список, "Статус", Статус, ЗначениеЗаполнено(Статус));
	
КонецПроцедуры // СтатусОтборПриИзменении()

///////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

// Процедура - обработчик события формы "ПриЗагрузкеДанныхИзНастроекНаСервере".
//
&НаСервере
Процедура ПриЗагрузкеДанныхИзНастроекНаСервере(Настройки)
	
	Менеджер = Настройки.Получить("Менеджер");
	Статус = Настройки.Получить("Статус");
	
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список, "Менеджер", Менеджер, ЗначениеЗаполнено(Менеджер));
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список, "Статус", Статус, ЗначениеЗаполнено(Статус));
	
КонецПроцедуры // ПриЗагрузкеДанныхИзНастроекНаСервере()

//////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД

// Процедура - обработчик команды "УстановитьСтатусСоздан".
//
&НаКлиенте
Процедура УстановитьСтатусСоздан(Команда)
	
	Если Не ПроверитьНаличиеВыделенныхВСпискеСтрок(Элементы.Список) Тогда
		Возврат;
	КонецЕсли;
	
	ПредставлениеСтатуса = НСтр("ru='Создан'");
	
	ТекстВопроса = НСтр("ru='У выделенных в списке документов будет установлен статус ""%ЗначениеСтатуса%"". Продолжить?'");
	ТекстВопроса = СтрЗаменить(ТекстВопроса, "%ЗначениеСтатуса%", ПредставлениеСтатуса);
	ПоказатьВопрос(
		Новый ОписаниеОповещения("УстановитьСтатусСозданПослеВопроса", ЭтотОбъект, ПредставлениеСтатуса),
		ТекстВопроса, РежимДиалогаВопрос.ДаНет
	);

КонецПроцедуры // УстановитьСтатусСоздан()

&НаКлиенте
////
 // Процедура: УстановитьСтатусСозданПослеВопроса
 //   Вызывается после ответа на вопрос пользователем
 //
 // Параметры:
 //   П1 (Тип)
 //     Описание
 //   П2 (Тип)
 //     Описание
///
Процедура УстановитьСтатусСозданПослеВопроса(Ответ, ПредставлениеСтатуса) Экспорт
	Если Ответ <> КодВозвратаДиалога.Да Тогда
		Возврат;
	КонецЕсли;

	МассивДокументов = ПолучитьМассивДокументов();
	Если МассивДокументов.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;

	КоличествоОбработанных = УстановитьСтатусДокументовСоздан(МассивДокументов);

	ОповеститьПользователяОбУстановкеСтатуса(КоличествоОбработанных, МассивДокументов.Количество(), ПредставлениеСтатуса);

КонецПроцедуры

// Процедура - обработчик команды "УстановитьСтатусНаСверке".
//
&НаКлиенте
Процедура УстановитьСтатусНаСверке(Команда)
	
	Если Не ПроверитьНаличиеВыделенныхВСпискеСтрок(Элементы.Список) Тогда
		Возврат;
	КонецЕсли;
	
	ПредставлениеСтатуса = НСтр("ru='На сверке'");
	
	ТекстВопроса = НСтр("ru='У выделенных в списке документов будет установлен статус ""%ЗначениеСтатуса%"". Продолжить?'");
	ТекстВопроса = СтрЗаменить(ТекстВопроса, "%ЗначениеСтатуса%", ПредставлениеСтатуса);
	ПоказатьВопрос(
		Новый ОписаниеОповещения("УстановитьСтатусНаСверкеПослеВопроса", ЭтотОбъект, ПредставлениеСтатуса),
		ТекстВопроса, РежимДиалогаВопрос.ДаНет
	);
	
КонецПроцедуры // УстановитьСтатусНаСверке()

&НаКлиенте
////
 // Процедура: УстановитьСтатусНаСверкеПослеВопроса
 //   Вызывается после ответа на вопрос пользователем
 //
 // Параметры:
 //   П1 (Тип)
 //     Описание
 //   П2 (Тип)
 //     Описание
///
Процедура УстановитьСтатусНаСверкеПослеВопроса(Ответ, ПредставлениеСтатуса) Экспорт
	Если Ответ <> КодВозвратаДиалога.Да Тогда
		Возврат;
	КонецЕсли;

	МассивДокументов = ПолучитьМассивДокументов();
	Если МассивДокументов.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;

	КоличествоОбработанных = УстановитьСтатусДокументовНаСверке(МассивДокументов);

	ОповеститьПользователяОбУстановкеСтатуса(КоличествоОбработанных, МассивДокументов.Количество(), ПредставлениеСтатуса);

КонецПроцедуры

// Процедура - обработчик команды "УстановитьСтатусСверен".
//
&НаКлиенте
Процедура УстановитьСтатусСверен(Команда)
	
	Если Не ПроверитьНаличиеВыделенныхВСпискеСтрок(Элементы.Список) Тогда
		Возврат;
	КонецЕсли;
	
	ПредставлениеСтатуса = НСтр("ru='Сверен'");

	ТекстВопроса = НСтр("ru='У выделенных в списке документов будет установлен статус ""%ЗначениеСтатуса%"". Продолжить?'");
	ТекстВопроса = СтрЗаменить(ТекстВопроса, "%ЗначениеСтатуса%", ПредставлениеСтатуса);
	ПоказатьВопрос(
		Новый ОписаниеОповещения("УстановитьСтатусСверенПослеВопроса", ЭтотОбъект, ПредставлениеСтатуса),
		ТекстВопроса, РежимДиалогаВопрос.ДаНет
	);

КонецПроцедуры // УстановитьСтатусСверен()

&НаКлиенте
////
 // Процедура: УстановитьСтатусСверенПослеВопроса
 //   Вызывается после ответа на вопрос пользователем
 //
 // Параметры:
 //   П1 (Тип)
 //     Описание
 //   П2 (Тип)
 //     Описание
///
Процедура УстановитьСтатусСверенПослеВопроса(Ответ, ПредставлениеСтатуса) Экспорт
	Если Ответ <> КодВозвратаДиалога.Да Тогда
		Возврат;
	КонецЕсли;

	МассивДокументов = ПолучитьМассивДокументов();
	Если МассивДокументов.Количество() = 0 Тогда
		Возврат;
	КонецЕсли;

	КоличествоОбработанных = УстановитьСтатусДокументовСверен(МассивДокументов);

	ОповеститьПользователяОбУстановкеСтатуса(КоличествоОбработанных, МассивДокументов.Количество(), ПредставлениеСтатуса);

КонецПроцедуры

// Процедура - обработчик команды "ПомощникСозданияАктов".
//
&НаКлиенте
Процедура ОткрытьПомощникСозданияАктов(Команда)
	
	ОткрытьФорму("Документ.СверкаВзаиморасчетов.Форма.ФормаПомощникаСоздания",
		,
		ЭтотОбъект);
	
КонецПроцедуры // ОткрытьПомощникСозданияАктов()

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтотОбъект, Элементы.ПодменюПечать);
	// Конец СтандартныеПодсистемы.Печать
	
	// СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтотОбъект);
	// Конец СтандартныеПодсистемы.ДополнительныеОтчетыИОбработки
	
КонецПроцедуры

// Вызывает диалог ввода даты. В случае ввода пустой даты - предлагает диалог ввода даты еще раз.
//
// Параметры:
// Список - ДинамическийСписок
//
// Возвращаемое значение:
// Булево - Ложь - если в списке нет выделенных строк, иначе Истина.
//
&НаКлиенте
Функция ПроверитьНаличиеВыделенныхВСпискеСтрок(Список) Экспорт

	Если Список.ВыделенныеСтроки.Количество() = 0 Тогда
		СообщенияПользователюКлиент.ВывестиПредупреждение("Общие_НетВыделенныхСтрок");
		Возврат Ложь;
	КонецЕсли;
	
	Возврат Истина;
	
КонецФункции // ПроверитьНаличиеВыделенныхВСпискеСтрок()

#Область ОбработчикиКомандФормы

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтотОбъект, Элементы.Список);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

#КонецОбласти

#КонецОбласти