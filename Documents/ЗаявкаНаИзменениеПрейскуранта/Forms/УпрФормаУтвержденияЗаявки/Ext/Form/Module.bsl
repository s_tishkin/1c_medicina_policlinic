﻿
#Область ПрограммныйИнтерфейс


&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	ПодразделениеЗаявок = Справочники.СтруктураПредприятия.ПустаяСсылка();
	ЭтотОбъект.Элементы.СтатусЗаявок.СписокВыбора.Очистить();
	ЭтотОбъект.Элементы.СтатусЗаявок.СписокВыбора.Добавить(Перечисления.СтатусЗаявкиНаИзменениеПрайса.НаСогласовании);
	ЭтотОбъект.Элементы.СтатусЗаявок.СписокВыбора.Добавить(Перечисления.СтатусЗаявкиНаИзменениеПрайса.Согласована);
	ЭтотОбъект.Элементы.СтатусЗаявок.СписокВыбора.Добавить(Перечисления.СтатусЗаявкиНаИзменениеПрайса.НеСогласована);
	СтатусЗаявок = Перечисления.СтатусЗаявкиНаИзменениеПрайса.НаСогласовании;
	Элементы.ФормаСогласовать.Доступность = Истина;
	Элементы.ФормаОтказатьВСогласовании.Доступность = Истина;
	
	
	Запрос_ = Новый Запрос;
	Запрос_.Текст = 
		"ВЫБРАТЬ
		|	ВерсияПрейскуранта.Ссылка
		|ИЗ
		|	Справочник.ВерсияПрейскуранта КАК ВерсияПрейскуранта
		|ГДЕ
		|	НЕ ВерсияПрейскуранта.ПометкаУдаления
		|УПОРЯДОЧИТЬ ПО
		|	ВерсияПрейскуранта.ДатаНачалаПодготовки УБЫВ";
	ВыборкаДетальныеЗаписи_ = Запрос_.Выполнить().Выбрать();
	Если ВыборкаДетальныеЗаписи_.Следующий() Тогда
		ВерсияПрейскурантаЗаявок = ВыборкаДетальныеЗаписи_.Ссылка;
	КонецЕсли;

	ОбновитьПараметрыСписка();
	
	// Оформления видов объектов
	ОформленияВидовОбъектов.НастроитьУсловноеОформление(
		ЭтотОбъект, "Список.ОформлениеКомментария", "Комментарий", "Комментарии"
	);
	// КОНЕЦ Оформления видов объектов

КонецПроцедуры

&НаСервере
Процедура ОбновитьПараметрыСписка()
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список, "Статус", СтатусЗаявок, ЗначениеЗаполнено(СтатусЗаявок));
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список, "Подразделение", ПодразделениеЗаявок, ЗначениеЗаполнено(ПодразделениеЗаявок));
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список, "ВерсияПрейскуранта", ВерсияПрейскурантаЗаявок, ЗначениеЗаполнено(ВерсияПрейскурантаЗаявок));
КонецПроцедуры

&НаКлиенте
Процедура СтатусЗаявокПриИзменении(Элемент)
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список, "Статус", СтатусЗаявок, ЗначениеЗаполнено(СтатусЗаявок));
КонецПроцедуры

&НаКлиенте
Процедура ПодразделениеЗаявокПриИзменении(Элемент)
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список, "Подразделение", ПодразделениеЗаявок, ЗначениеЗаполнено(ПодразделениеЗаявок));
КонецПроцедуры

&НаСервере
Процедура ОбновитьОтображениеКнопок(ДокументСсылка)
	Если ДокументСсылка.Статус = Перечисления.СтатусЗаявкиНаИзменениеПрайса.НаСогласовании
		ИЛИ СтатусЗаявок = Перечисления.СтатусЗаявкиНаИзменениеПрайса.НеСогласована Тогда
		Элементы.ФормаСогласовать.Доступность =	Истина;
	Иначе
		Элементы.ФормаСогласовать.Доступность =	Ложь;
	КонецЕсли;

	Если ДокументСсылка.Статус = Перечисления.СтатусЗаявкиНаИзменениеПрайса.НаСогласовании
		ИЛИ СтатусЗаявок = Перечисления.СтатусЗаявкиНаИзменениеПрайса.Согласована Тогда
		Элементы.ФормаОтказатьВСогласовании.Доступность =	Истина;
	Иначе
		Элементы.ФормаОтказатьВСогласовании.Доступность =	Ложь;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура Согласовать(Команда)
	Если Элементы.Список.ТекущиеДанные <> Неопределено Тогда
		СогласоватьНаСервере(Элементы.Список.ТекущиеДанные.Ссылка);
		ОповеститьОбИзменении(Элементы.Список.ТекущиеДанные.Ссылка);
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура СогласоватьНаСервере(ДокументСсылка)
	Если ДокументСсылка.Статус = Перечисления.СтатусЗаявкиНаИзменениеПрайса.НаСогласовании
		ИЛИ ДокументСсылка.Статус = Перечисления.СтатусЗаявкиНаИзменениеПрайса.НеСогласована Тогда
		ДокументОбъект_ = ДокументСсылка.ПолучитьОбъект();
		ДокументОбъект_.Статус = Перечисления.СтатусЗаявкиНаИзменениеПрайса.Согласована;
		ДокументОбъект_.Дата = ПолучитьОперативнуюОтметкуВремени();
		ДокументОбъект_.Записать(РежимЗаписиДокумента.Проведение, РежимПроведенияДокумента.Оперативный);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ОтказатьВСогласовании(Команда)
	Если Элементы.Список.ТекущиеДанные <> Неопределено Тогда
		ОтказатьВСогласованииНаСервере(Элементы.Список.ТекущиеДанные.Ссылка);
		ОповеститьОбИзменении(Элементы.Список.ТекущиеДанные.Ссылка);
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ОтказатьВСогласованииНаСервере(ДокументСсылка)
	Если ДокументСсылка.Статус = Перечисления.СтатусЗаявкиНаИзменениеПрайса.НаСогласовании
		ИЛИ ДокументСсылка.Статус = Перечисления.СтатусЗаявкиНаИзменениеПрайса.Согласована Тогда
		ДокументОбъект_ = ДокументСсылка.ПолучитьОбъект();
		ДокументОбъект_.Статус = Перечисления.СтатусЗаявкиНаИзменениеПрайса.НеСогласована;
		ДокументОбъект_.Дата = ПолучитьОперативнуюОтметкуВремени();
		ДокументОбъект_.Записать(РежимЗаписиДокумента.ОтменаПроведения, РежимПроведенияДокумента.Оперативный);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура СписокПриАктивизацииЯчейки(Элемент)
	Если ТипЗнч(ЭтотОбъект.ТекущийЭлемент) = Тип("ТаблицаФормы")
		И ТипЗнч(ЭтотОбъект.ТекущийЭлемент.ТекущаяСтрока) = Тип("ДокументСсылка.ЗаявкаНаИзменениеПрейскуранта") Тогда
		ОбновитьОтображениеКнопок(Элемент.ТекущаяСтрока);
	КонецЕсли;
КонецПроцедуры

&НаСервере
Процедура ПриЗагрузкеДанныхИзНастроекНаСервере(Настройки)
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список, "Статус", СтатусЗаявок, ЗначениеЗаполнено(СтатусЗаявок));
КонецПроцедуры

&НаКлиенте
Процедура Комментарий(Команда)
	Если ЭтотОбъект.Элементы.Список.ТекущиеДанные <> Неопределено Тогда
		ОткрытьКомментарий();
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьКомментарий()
	
	Заявка_ = ЭтотОбъект.Элементы.Список.ТекущиеДанные.Ссылка;
	
	МеханизмыКомментариев.НажатиеКомментарияВТЧФормы(
		"Комментарий", 
		"ОформлениеКомментария",
		Элементы.Список, 
		Заявка_,
		,
		Строка(Заявка_)
		,
		ПредопределенноеЗначение("Перечисление.ТипыКомментариев.КЗаявкеНаИзменениеПрейскуранта")
	);
	
	ОповеститьОбИзменении(Заявка_);
КонецПроцедуры


&НаКлиенте
Процедура ВерсияПрейскурантаЗаявокПриИзменении(Элемент)
	ОтборыСписковКлиентСервер.ИзменитьЭлементОтбораСписка(Список, "ВерсияПрейскуранта", ВерсияПрейскурантаЗаявок, ЗначениеЗаполнено(ВерсияПрейскурантаЗаявок));
КонецПроцедуры



#КонецОбласти