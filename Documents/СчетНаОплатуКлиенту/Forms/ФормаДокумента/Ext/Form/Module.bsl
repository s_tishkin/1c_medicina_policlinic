﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	УстановитьУсловноеОформление();
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	Если ТребуетсяОткрытиеПечатнойФормы Тогда
		Возврат;
	КонецЕсли;
	
	// Обработчик механизма "ВерсионированиеОбъектов"
	ВерсионированиеОбъектов.ПриСозданииНаСервере(ЭтотОбъект);
	
	// Обработчик подсистемы "Внешние обработки"
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтотОбъект);
	
	ДенежныеСредстваСервер.УстановитьВидимостьОплатыПлатежнойКартой(Элементы.ФормаОплаты);
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		
		ПриЧтенииСозданииНаСервере();
		Если Параметры.Свойство("Основание") Тогда
			Основание = Параметры.Основание;
		КонецЕсли;
		
	КонецЕсли;
	
	ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "ДокументОснование", "Видимость", ЗначениеЗаполнено(Объект.ДокументОснование));
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтотОбъект, Элементы.ПодменюПечать);
	// Конец СтандартныеПодсистемы.Печать

КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	Если Не ЗначениеЗаполнено(Объект.Ссылка) Тогда
		ФинансыКлиент.ПроверитьЗаполнениеДокументаНаОсновании(
			Объект,
			Основание);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПриЧтенииНаСервере(ТекущийОбъект)
	
	ПриЧтенииСозданииНаСервере();
	
	// СтандартныеПодсистемы.ДатыЗапретаИзменения
	ДатыЗапретаИзменения.ОбъектПриЧтенииНаСервере(ЭтотОбъект, ТекущийОбъект);
	// Конец СтандартныеПодсистемы.ДатыЗапретаИзменения
	
КонецПроцедуры

&НаКлиенте
Процедура ПередЗаписью(Отказ, ПараметрыЗаписи)
	
	Если ПараметрыЗаписи.РежимЗаписи = РежимЗаписиДокумента.Проведение Тогда
		
		Если ЭтотОбъект.Модифицированность Тогда
			ПроверитьЗаполнениеЭтаповОплаты(Отказ);
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПослеЗаписиНаСервере(ТекущийОбъект, ПараметрыЗаписи)
	
	Элементы.ГруппаКомментарий.Картинка = ОбщегоНазначенияКлиентСервер.КартинкаКомментария(Объект.Комментарий);
	
КонецПроцедуры

&НаКлиенте
Процедура ПерезаполнитьНазначениеПлатежа(Команда)
	ЗаполнитьНазначениеПлатежа();
КонецПроцедуры


#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура ФормаОплатыПриИзменении(Элемент)

	ПриИзмененииФормыОплатыСервер();
	
КонецПроцедуры


&НаКлиенте
Процедура СуммаДокументаПриИзменении(Элемент)
	
	ЦенообразованиеКлиентСервер.РаспределитьСуммуПоЭтапамГрафикаОплаты(Объект.ЭтапыГрафикаОплаты, Объект.СуммаДокумента);
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовТаблицыФормыЭтапыГрафикаОплаты

&НаКлиенте
Процедура ГрафикОплатыПриИзменении(Элемент)
	Если ЗначениеЗаполнено(Объект.ГрафикОплаты) Тогда
		ПриИзмененииГрафикаОплатыСервер();
	КонецЕсли;
КонецПроцедуры

// Заполняет реквизиты документа по умолчанию в зависимости от выбранного графика оплаты.
//
&НаСервере
Процедура ПриИзмененииГрафикаОплатыСервер()

	СуммаКРаспределению_ = Объект.СуммаДокумента;
	
	ПродажиСервер.ЗаполнитьЭтапыГрафикаОплаты(ЭтотОбъект.Объект, СуммаКРаспределению_);

КонецПроцедуры // ПриИзмененииГрафикаОплатыСервер()


&НаКлиенте
Процедура ЭтапыГрафикаОплатыПослеУдаления(Элемент)
	
	РассчитатьИтоговыеПоказателиСчетаНаОплатуКлиенту(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ЭтапыГрафикаОплатыПриОкончанииРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования)
	
	РассчитатьИтоговыеПоказателиСчетаНаОплатуКлиенту(ЭтотОбъект);
	
КонецПроцедуры

&НаКлиенте
Процедура ЭтапыГрафикаОплатыПроцентПлатежаПриИзменении(Элемент)
	
	ЦенообразованиеКлиент.ЭтапыГрафикаОплатыПроцентПлатежаПриИзменении(Элементы.ЭтапыГрафикаОплаты.ТекущиеДанные, Объект.ЭтапыГрафикаОплаты, Объект.СуммаДокумента);
	
КонецПроцедуры

&НаКлиенте
Процедура ЭтапыГрафикаОплатыСуммаПлатежаПриИзменении(Элемент)

	ЦенообразованиеКлиент.ЭтапыГрафикаОплатыСуммаПлатежаПриИзменении(Элементы.ЭтапыГрафикаОплаты.ТекущиеДанные, Объект.ЭтапыГрафикаОплаты, Объект.СуммаДокумента);
	
КонецПроцедуры

&НаКлиенте
Процедура ПроверитьЗаполнениеЭтаповОплаты(Отказ)
		
	СуммаДокумента = Объект.СуммаДокумента;
	
	СуммаЭтаповОплаты    = Объект.ЭтапыГрафикаОплаты.Итог("СуммаПлатежа");

	Если СуммаДокумента = 0  И Объект.ЭтапыГрафикаОплаты.Количество() > 0 Тогда
		
			РежимДиалога = РежимДиалогаВопрос.Ок;
			ТекстВопроса = НСтр("ru='Сумма оплаты нулевая. Таблица этапов оплаты будет очищена'");
			
			ПоказатьВопрос(
					Новый ОписаниеОповещения("ПослеВопросаСуммаОплатыНулевая", ЭтотОбъект),
					ТекстВопроса, РежимДиалога
				);

			Отказ = Истина;
			
	ИначеЕсли СуммаДокумента <> СуммаЭтаповОплаты Тогда
		
		Если Объект.ЭтапыГрафикаОплаты.Количество() = 0 Тогда
		
			Если ЗначениеЗаполнено(Объект.ГрафикОплаты) Тогда
				ТекстВопроса = НСтр("ru='Отсутствуют этапы графика оплаты. Заполнить этапы в соответствии с графиком?'");
			Иначе
				ТекстВопроса = НСтр("ru='Отсутствуют этапы графика оплаты. Добавить один этап с оплатой 100%?'");
			КонецЕсли;
				
			РежимДиалога = РежимДиалогаВопрос.ОкОтмена;
			
			ПоказатьВопрос(
					Новый ОписаниеОповещения("ПослеВопросаОтсутствуютЭтапыГрафикаОплаты", ЭтотОбъект),
					ТекстВопроса, РежимДиалога
				);
			
			Отказ = Истина;
				
		Иначе
			
			РежимДиалога = РежимДиалогаВопрос.ОкОтмена;
			ТекстВопроса = НСтр("ru='Сумма этапов графика оплаты не совпадает с суммой документа. Сумма этапов оплаты будет скорректирована.'");
			
			ПоказатьВопрос(
					Новый ОписаниеОповещения("ПослеВопросаСуммаЭтаповГрафикаОплатыНеСовпадаетССуммойЗаказанныхСтрок", ЭтотОбъект),
					ТекстВопроса, РежимДиалога
				);
			
			Отказ = Истина;

		КонецЕсли;
		
	КонецЕсли;
	

КонецПроцедуры

&НаКлиенте
////
 // Процедура: ПослеВопросаСуммаОплатыНулевая
 //   Вызывается после ответа на вопрос пользователем
 //
 // Параметры:
 //   П1 (Тип)
 //     Описание
 //   П2 (Тип)
 //     Описание
///
Процедура ПослеВопросаСуммаОплатыНулевая(Результат, Параметры) Экспорт
	Если Результат = КодВозвратаДиалога.Отмена Тогда
		Возврат;
	КонецЕсли;
	
	Объект.ЭтапыГрафикаОплаты.Очистить();
КонецПроцедуры

&НаКлиенте
////
 // Процедура: ПослеВопросаОтсутствуютЭтапыГрафикаОплаты
 //   Вызывается после ответа на вопрос пользователем
 //
 // Параметры:
 //   П1 (Тип)
 //     Описание
 //   П2 (Тип)
 //     Описание
///
Процедура ПослеВопросаОтсутствуютЭтапыГрафикаОплаты(Результат, Параметры) Экспорт
	Если Результат = КодВозвратаДиалога.Отмена Тогда
		Возврат;
	Иначе 
		СуммаДокумента = Объект.СуммаДокумента;
		Если ЗначениеЗаполнено(Объект.ГрафикОплаты) Тогда
			ЗаполнитьЭтапыОплатыПоГрафикуСервер(СуммаДокумента);
		Иначе
			
			ЦенообразованиеКлиент.ДобавитьЭтапОплатыПоУмолчанию(
				Объект.ЭтапыГрафикаОплаты,
				Неопределено,
				СуммаДокумента
			);
				
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
////
 // Процедура: ПослеВопросаСуммаЭтаповГрафикаОплатыНеСовпадаетССуммойЗаказанныхСтрок
 //   Вызывается после ответа на вопрос пользователем
 //
 // Параметры:
 //   П1 (Тип)
 //     Описание
 //   П2 (Тип)
 //     Описание
///
Процедура ПослеВопросаСуммаЭтаповГрафикаОплатыНеСовпадаетССуммойЗаказанныхСтрок(Результат, Параметры) Экспорт
	Если Результат = КодВозвратаДиалога.Отмена Тогда
		Возврат;
	Иначе
		СуммаДокумента = Объект.СуммаДокумента;
		ЦенообразованиеКлиентСервер.РаспределитьСуммуПоЭтапамГрафикаОплаты(Объект.ЭтапыГрафикаОплаты, СуммаДокумента);
	КонецЕсли;
КонецПроцедуры

// Заполняет этапы оплаты в соответствии с графиком
//
// Параметры:
// СуммаДокумента - сумма, которую необходимо распределить по этапам.
//
&НаСервере
Процедура ЗаполнитьЭтапыОплатыПоГрафикуСервер(Знач СуммаДокумента)
	
	ПродажиСервер.ЗаполнитьЭтапыГрафикаОплаты(Объект, СуммаДокумента);
	
КонецПроцедуры // ЗаполнитьЭтапыОплатыПоГрафикуСервер()

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформление()

	УсловноеОформление.Элементы.Очистить();

	//

	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.ЭтапыГрафикаОплатыДатаПлатежа.Имя);

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.ЭтапыГрафикаОплаты.ДатаЗаполненаНеВерно");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = Истина;

	Элемент.Оформление.УстановитьЗначениеПараметра("ЦветТекста", WebЦвета.FireBrick);

	//

	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.ЭтапыГрафикаОплатыПроцентПлатежа.Имя);

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.ЭтапыГрафикаОплаты.ПроцентЗаполненНеВерно");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = Истина;

	Элемент.Оформление.УстановитьЗначениеПараметра("ЦветТекста", WebЦвета.FireBrick);

	//

	Элемент = УсловноеОформление.Элементы.Добавить();

	ПолеЭлемента = Элемент.Поля.Элементы.Добавить();
	ПолеЭлемента.Поле = Новый ПолеКомпоновкиДанных(Элементы.ЭтапыГрафикаОплатыПроцентПлатежа.Имя);

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.ЭтапыГрафикаОплаты.ПроцентЗаполненНеВерно");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = Ложь;

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Объект.ЭтапыГрафикаОплаты.НомерСтроки");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.МеньшеИлиРавно;
	ОтборЭлемента.ПравоеЗначение = Новый ПолеКомпоновкиДанных("НомерСтрокиПолнойОплаты");

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("НомерСтрокиПолнойОплаты");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.НеРавно;
	ОтборЭлемента.ПравоеЗначение = 0;

	Элемент.Оформление.УстановитьЗначениеПараметра("ЦветТекста", WebЦвета.Seagreen);

КонецПроцедуры


#Область Прочее

&НаСервере
Процедура ПриЧтенииСозданииНаСервере()
	
	УправлениеЭлементамиФормы();
	РассчитатьИтоговыеПоказателиСчетаНаОплатуКлиенту(ЭтотОбъект);
	Элементы.ГруппаКомментарий.Картинка = ОбщегоНазначенияКлиентСервер.КартинкаКомментария(Объект.Комментарий);
	
КонецПроцедуры

&НаКлиентеНаСервереБезКонтекста
Процедура РассчитатьИтоговыеПоказателиСчетаНаОплатуКлиенту(Форма)
	
	ПроцентПлатежейОбщий = 0;
	ПредыдущееЗначениеДаты = Дата(1, 1, 1);
	Форма.НомерСтрокиПолнойОплаты = 0;
	Для Каждого ТекСтрока Из Форма.Объект.ЭтапыГрафикаОплаты Цикл
		ПроцентПлатежейОбщий = ПроцентПлатежейОбщий + ТекСтрока.ПроцентПлатежа;
		ТекСтрока.ПроцентЗаполненНеВерно = (ПроцентПлатежейОбщий > 100);
		ТекСтрока.ДатаЗаполненаНеВерно = (ПредыдущееЗначениеДаты > ТекСтрока.ДатаПлатежа);
		ПредыдущееЗначениеДаты = ТекСтрока.ДатаПлатежа;
		Если ПроцентПлатежейОбщий = 100 Тогда
			Форма.НомерСтрокиПолнойОплаты = ТекСтрока.НомерСтроки;
		КонецЕсли;
	КонецЦикла;
	
КонецПроцедуры

&НаСервере
Процедура УправлениеЭлементамиФормы()
	
	ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "Касса", "Видимость", Объект.ФормаОплаты = Перечисления.ФормыОплаты.Наличная);
	
	ОбщегоНазначенияУТКлиентСервер.УстановитьСвойствоЭлементаФормы(Элементы, "Договор", "Видимость", Истина);
	
КонецПроцедуры

&НаСервере
Процедура ПриИзмененииФормыОплатыСервер()
	
	УправлениеЭлементамиФормы();
	Объект.БанковскийСчет = ЗначениеНастроекПовтИсп.ПолучитьБанковскийСчетОрганизацииПоУмолчанию(Объект.Организация, , Объект.БанковскийСчет);
	
КонецПроцедуры

&НаСервере
Процедура ЗаполнитьНазначениеПлатежа()
	
	ДокументОснование = Объект.ДокументОснование;
	
	Если НЕ ПустаяСтрока(ДокументОснование) Тогда
		Объект.НазначениеПлатежа = Документы.СчетНаОплатуКлиенту.СформироватьНазначениеПлатежа(
			ДокументОснование.Номер,
			ДокументОснование);
	Иначе	
		Объект.НазначениеПлатежа = "По счету № " +	ПрефиксацияОбъектовКлиентСервер.ПолучитьНомерНаПечать(Объект.Номер) +
		" от " + Формат(Объект.Дата, "ДФ=dd.MM.yyyy");
		
	КонецЕсли;
	
КонецПроцедуры

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтотОбъект, Объект);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

&НаКлиенте
Процедура ПартнерПриИзменении(Элемент)
	ПартнерПриИзмененииСервер();
КонецПроцедуры


// Процедура - обработчик события "ПриИзменении" поля "Партнер".
//
&НаСервере
Процедура ПартнерПриИзмененииСервер()
	
	Если ЗначениеЗаполнено(Объект.Партнер) Тогда
	
		КонтрагентПоУмолчанию = ПартнерыИКонтрагенты.ПолучитьКонтрагентаПартнераПоУмолчанию(Объект.Партнер);
		Если КонтрагентПоУмолчанию <> Неопределено Тогда
			Объект.Контрагент = КонтрагентПоУмолчанию;
		КонецЕсли;
		
	КонецЕсли;
	
	ДенежныеСредстваСервер.УстановитьПараметрыВыбораКонтрагента(Объект, Элементы.Контрагент);
	
КонецПроцедуры // ПартнерПриИзмененииСервер()

&НаКлиенте
Процедура КонтрагентПриИзменении(Элемент)
	КонтрагентПриИзмененииСервер();
КонецПроцедуры

// Процедура - обработчик события "ПриИзменении" поля "Контрагент".
//
&НаСервере
Процедура КонтрагентПриИзмененииСервер()
	
	Если ЗначениеЗаполнено(Объект.Контрагент) Тогда
		
		Если Не ЗначениеЗаполнено(Объект.Партнер) Тогда
			Объект.Партнер = ДенежныеСредстваСервер.ПолучитьПартнераПоКонтрагенту(Объект.Контрагент);
		КонецЕсли;
		
	КонецЕсли;
	
КонецПроцедуры // КонтрагентПриИзмененииСервер()

&НаКлиенте
Процедура РасшифровкаПлатежаСуммаПриИзменении(Элемент)
	ПересчитатьСуммуДокумента();
КонецПроцедуры

&НаКлиенте
Процедура РасшифровкаПлатежаПослеУдаления(Элемент)
	ПересчитатьСуммуДокумента();
КонецПроцедуры

&НаКлиенте
Процедура РасшифровкаПлатежаПриОкончанииРедактирования(Элемент, НоваяСтрока, ОтменаРедактирования)
	ПересчитатьСуммуДокумента();
КонецПроцедуры

&НаКлиенте
Процедура ПересчитатьСуммуДокумента()
	Объект.СуммаДокумента = Объект.РасшифровкаПлатежа.Итог("Сумма");
	Если ЗначениеЗаполнено(Объект.ГрафикОплаты) Тогда
		ПриИзмененииГрафикаОплатыСервер();
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	Оповестить(ОповещенияФорм.ПриИзмененииСчетаНаОплату());
КонецПроцедуры

&НаКлиенте
Процедура ОрганизацияПриИзменении(Элемент)
	ОрганизацияПриИзмененииНаСервере();
КонецПроцедуры

&НаСервере
Процедура ОрганизацияПриИзмененииНаСервере()
	ОтветственныеЛицаСервер.ПриИзмененииСвязанныхРеквизитовДокумента(Объект);
КонецПроцедуры

#КонецОбласти

#КонецОбласти
