﻿
#Область ОбработчикиСобытийФормы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	УстановитьУсловноеОформление();
	
	Если Параметры.Свойство("АвтоТест") Тогда // Возврат при получении формы для анализа.
		Возврат;
	КонецЕсли;

	// Обработчик подсистемы "Внешние обработки"
	ДополнительныеОтчетыИОбработки.ПриСозданииНаСервере(ЭтотОбъект);
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Менеджер",  Менеджер, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(Менеджер));
	
	ОтборыСписковКлиентСервер.СкопироватьСписокВыбораОтбораПоМенеджеру(
		Элементы.Менеджер.СписокВыбора,
		ОбщегоНазначенияУТ.ПолучитьСписокПользователейПоМассивуРолей(Документы.СчетНаОплатуКлиенту.ИменаРолейСПравомДобавления()));
	
	// СтандартныеПодсистемы.Печать
	УправлениеПечатью.ПриСозданииНаСервере(ЭтотОбъект, Элементы.ПодменюПечать);
	// Конец СтандартныеПодсистемы.Печать


КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	МенеджерОборудованияКлиентПоликлиника.НачатьПодключениеОборудованиеПриОткрытииФормы(ЭтотОбъект, "СканерШтрихкода");
	
КонецПроцедуры

&НаКлиенте
Процедура ПриЗакрытии(ЗавершениеРаботы)
	
	Если ЗавершениеРаботы Тогда
		Возврат;
	КонецЕсли;
	
	// ПодключаемоеОборудование
	МенеджерОборудованияКлиентПоликлиника.НачатьОтключениеОборудованиеПриЗакрытииФормы(ЭтотОбъект);
	// Конец ПодключаемоеОборудование
	
КонецПроцедуры

&НаСервере
Процедура ПриЗагрузкеДанныхИзНастроекНаСервере(Настройки)
	
	Состояние = Настройки.Получить("Состояние");
	Менеджер  = Настройки.Получить("Менеджер");
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Состояние", Состояние, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(Состояние));
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Менеджер",  Менеджер, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(Менеджер));
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиСобытийЭлементовШапкиФормы

&НаКлиенте
Процедура МенеджерПриИзменении(Элемент)
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Менеджер",  Менеджер, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(Менеджер));

КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура Аннулировать(Команда)
	
	Если Не РаботаСДиалогамиКлиент.ПроверитьНаличиеВыделенныхВСпискеСтрок(Элементы.Список) Тогда
		Возврат;
	КонецЕсли;
	
	Параметры_ = Новый Структура("ВыделенныеСтроки", Элементы.Список.ВыделенныеСтроки);
	ТекстВопроса = НСтр("ru='Выделенные в списке счетов на оплату будут аннулированы. Продолжить?'");
	ПоказатьВопрос(
		Новый ОписаниеОповещения("АннулироватьПослеВопроса", ЭтотОбъект, Параметры_),
		ТекстВопроса,РежимДиалогаВопрос.ДаНет
	);
	
КонецПроцедуры

&НаКлиенте
////
 // Процедура: АннулироватьПослеВопроса
 //   Вызывается после ответа на вопрос пользователем
 //
 // Параметры:
 //   П1 (Тип)
 //     Описание
 //   П2 (Тип)
 //     Описание
///
Процедура АннулироватьПослеВопроса(Результат, Параметры) Экспорт
	Если Результат = КодВозвратаДиалога.Нет Тогда
		Возврат;
	КонецЕсли;
	
	КоличествоОбработанных = УстановитьПризнакАннулироваСервер(Параметры.ВыделенныеСтроки);
	
	Если КоличествоОбработанных > 0 Тогда
		
		Элементы.Список.Обновить();
		
		ТекстСообщения = НСтр("ru='%КоличествоОбработанных% из %КоличествоВсего% выделенных в списке счетов на оплату аннулированы.'");
		ТекстСообщения = СтрЗаменить(ТекстСообщения,"%КоличествоОбработанных%", КоличествоОбработанных);
		ТекстСообщения = СтрЗаменить(ТекстСообщения,"%КоличествоВсего%",        Параметры.ВыделенныеСтроки.Количество());
		ПоказатьОповещениеПользователя(НСтр("ru='Счета на оплату аннулированы'"),, ТекстСообщения, БиблиотекаКартинок.Информация32);
		
		Оповестить(ОповещенияФорм.ПриИзмененииСчетаНаОплату());
		
	Иначе
		
		ТекстСообщения = НСтр("ru='Не аннулирован ни один счет на оплату.'");
		ПоказатьОповещениеПользователя(НСтр("ru='Счета на оплату не аннулированы'"),, ТекстСообщения, БиблиотекаКартинок.Информация32);
		
	КонецЕсли;
КонецПроцедуры

// СтандартныеПодсистемы.Печать
&НаКлиенте
Процедура Подключаемый_ВыполнитьКомандуПечати(Команда)
	
	УправлениеПечатьюКлиент.ВыполнитьПодключаемуюКомандуПечати(Команда, ЭтотОбъект, Элементы.Список);
	
КонецПроцедуры
// Конец СтандартныеПодсистемы.Печать

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

&НаСервере
Процедура УстановитьУсловноеОформление()

	УсловноеОформление.Элементы.Очистить();

	//

	Элемент = УсловноеОформление.Элементы.Добавить();

	ОтборЭлемента = Элемент.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ОтборЭлемента.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Состояние");
	ОтборЭлемента.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ОтборЭлемента.ПравоеЗначение = НСтр("ru = 'Аннулирован'");

	Элемент.Оформление.УстановитьЗначениеПараметра("ЦветТекста", ЦветаСтиля.ЗакрытыйДокумент);

КонецПроцедуры

#Область ПриИзмененииРеквизитов

&НаКлиенте
Процедура ОтборСостояниеПриИзменении(Элемент)
	
	ОбщегоНазначенияКлиентСервер.УстановитьЭлементОтбораДинамическогоСписка(Список, "Состояние", Состояние, ВидСравненияКомпоновкиДанных.Равно,, ЗначениеЗаполнено(Состояние));
	
КонецПроцедуры

&НаКлиенте
Процедура СписокПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа)
	
	Отказ = Истина;
	
КонецПроцедуры

#КонецОбласти


#Область Прочее

&НаСервереБезКонтекста
Функция УстановитьПризнакАннулироваСервер (Знач СчетаНаОплату)
	
	Возврат Документы.СчетНаОплатуКлиенту.УстановитьПризнакАннулирован(СчетаНаОплату);
	
КонецФункции


#КонецОбласти

#КонецОбласти
