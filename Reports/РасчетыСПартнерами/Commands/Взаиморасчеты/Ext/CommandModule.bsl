﻿
#Область ПрограммныйИнтерфейс


&НаКлиенте
// Процедура - обработчик события "ОбработкаКоманды".
//
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	ПараметрыФормы = Новый Структура("Отбор, ФиксированныеНастройки, КлючНазначенияИспользования, КлючВарианта, СформироватьПриОткрытии");
	ПараметрыФормы.СформироватьПриОткрытии = Истина;
	
	Если ТипЗнч(ПараметрКоманды) = Тип("ДокументСсылка.СверкаВзаиморасчетов") Тогда
		ПараметрыФормы.Отбор = Новый Структура("СверкаВзаиморасчетов", ПараметрКоманды);
		ПараметрыФормы.КлючНазначенияИспользования = "СверкаВзаиморасчетов";
		ПараметрыФормы.КлючВарианта = "РасчетыСПартнерами";
	
	ИначеЕсли ТипЗнч(ПараметрКоманды) = Тип("СправочникСсылка.Контрагенты") Тогда
		ПараметрыФормы.Отбор = Новый Структура("Контрагент", ПараметрКоманды);
		ПараметрыФормы.КлючНазначенияИспользования = "Контрагент";
		ПараметрыФормы.КлючВарианта = "РасчетыСПартнерами";
		
	ИначеЕсли ТипЗнч(ПараметрКоманды) = Тип("СправочникСсылка.Партнеры") Тогда
		
		Если ПартнерыИКонтрагенты.ЕстьПодчиненныеПартнеры(ПараметрКоманды) Тогда
			ФиксированныеНастройки = Новый НастройкиКомпоновкиДанных();
			ЭлементОтбора = ФиксированныеНастройки.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
			ЭлементОтбора.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Партнер");
			ЭлементОтбора.ВидСравнения = ВидСравненияКомпоновкиДанных.ВИерархии;
			ЭлементОтбора.ПравоеЗначение = ПараметрКоманды;
			ПараметрыФормы.ФиксированныеНастройки = ФиксированныеНастройки;
			ПараметрыФормы.КлючНазначенияИспользования = "ГруппаПартнеров";
			
		Иначе
			ПараметрыФормы.Отбор = Новый Структура("Партнер", ПараметрКоманды);
			ПараметрыФормы.КлючНазначенияИспользования = "Партнер";
			
		КонецЕсли;
			
		ПараметрыФормы.КлючВарианта = "РасчетыСПартнерами";
		
	ИначеЕсли ТипЗнч(ПараметрКоманды) = Тип("СправочникСсылка.СегментыПартнеров") Тогда
		
		ФиксированныеНастройки = Новый НастройкиКомпоновкиДанных();
		ЭлементОтбора = ФиксированныеНастройки.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
		ЭлементОтбора.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Партнер");
		ЭлементОтбора.ВидСравнения = ВидСравненияКомпоновкиДанных.ВСписке;
		
		ЭлементОтбора.ПравоеЗначение = СегментыСервер.СписокЗначений(ПараметрКоманды);
		ПараметрыФормы.ФиксированныеНастройки = ФиксированныеНастройки;
		
		ПараметрыФормы.КлючНазначенияИспользования = "СегментПартнеров";
		ПараметрыФормы.КлючВарианта = "РасчетыСПартнерами";
		
	КонецЕсли;
		
	ОткрытьФорму("Отчет.РасчетыСПартнерами.Форма",
		ПараметрыФормы,
		ПараметрыВыполненияКоманды.Источник,
		ПараметрыВыполненияКоманды.Уникальность,
		ПараметрыВыполненияКоманды.Окно
	);
	
КонецПроцедуры // ОбработкаКоманды()


#КонецОбласти