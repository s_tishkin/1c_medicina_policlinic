﻿
#Область ПрограммныйИнтерфейс


&НаКлиенте
Процедура СформироватьОтчет_Выполнить(Команда)
	ФормированиеОтчета();
КонецПроцедуры

&НаСервере
Процедура ФормированиеОтчета()
	Перем ПорядковыйНомер;
	Перем КоличествоИспорченных;
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	 "ВЫБРАТЬ
	 |	ЛистокНетрудоспособности.Ссылка,
	 |	НАЧАЛОПЕРИОДА(ЛистокНетрудоспособности.ДатаВыдачи, ДЕНЬ) КАК ДатаВыдачи,
	 |	ЛистокНетрудоспособности.Запись1Врач.ФизЛицо КАК Врач,
	 |	ЛистокНетрудоспособности.НомерЛистка КАК НомерЛистка,
	 |	ЛистокНетрудоспособности.Статус
	 |ИЗ
	 |	Документ.ЛистокНетрудоспособности КАК ЛистокНетрудоспособности
	 |ГДЕ
	 |	ЛистокНетрудоспособности.Дата >= &ДатаНачала
	 |	И ЛистокНетрудоспособности.Дата <= &ДатаОкончания
	 |	И ЛистокНетрудоспособности.МедОрганизация = &МедОрганизация
	 |	И (ЛистокНетрудоспособности.Статус = ЗНАЧЕНИЕ(Перечисление.СтатусыЛистковНетрудоспособности.Испорчен)
	 |			ИЛИ ЛистокНетрудоспособности.Статус = ЗНАЧЕНИЕ(Перечисление.СтатусыЛистковНетрудоспособности.Утерян))
	 |
	 |УПОРЯДОЧИТЬ ПО
	 |	ДатаВыдачи"
	;

	Запрос.УстановитьПараметр("ДатаНачала", Период.ДатаНачала);
	Запрос.УстановитьПараметр("ДатаОкончания", Период.ДатаОкончания);
	Запрос.УстановитьПараметр("МедОрганизация", МедОрганизация.Ссылка);

	РезультатЗапроса = Запрос.Выполнить();
    
	Макет = Отчеты.КнигаУчетаИспорченныхЛН.ПолучитьМакет("Макет");
	ОбластьМакетаШапка = Макет.ПолучитьОбласть("Шапка");
	ОбластьМакетаШапкаТаблицы = Макет.ПолучитьОбласть("ШапкаТаблицы");
	ОбластьМакетаСтрокаТаблицы = Макет.ПолучитьОбласть("СтрокаТаблицы");
	ОбластьМакетаПодвал = Макет.ПолучитьОбласть("Подвал");
	
	ПараметрыМакетаШапка = Новый Структура("НаименованиеМО, ОГРН", МедОрганизация.Наименование, МедОрганизация.ОГРН);
	ОбластьМакетаШапка.Параметры.Заполнить(ПараметрыМакетаШапка);
	Результат.Очистить();
	Результат.Вывести(ОбластьМакетаШапка);
	Результат.Вывести(ОбластьМакетаШапкаТаблицы);
	Результат.НачатьАвтогруппировкуСтрок();

	ВыборкаДетальныеЗаписи = РезультатЗапроса.Выбрать();

	ПорядковыйНомер = 0;
	Испорченных_ = 0;
	Утерянных_ = 0;
	Пока ВыборкаДетальныеЗаписи.Следующий() Цикл
		ПорядковыйНомер = ПорядковыйНомер + 1;
		ОбластьМакетаСтрокаТаблицы.Параметры.Заполнить(ВыборкаДетальныеЗаписи);
		НомерЛистка_ = Формат(ВыборкаДетальныеЗаписи.НомерЛистка, "ЧЦ=12; ЧРГ=' '; ЧВН=");
		Если ВыборкаДетальныеЗаписи.Статус = Перечисления.СтатусыЛистковНетрудоспособности.Утерян Тогда
			ОбластьМакетаСтрокаТаблицы.Параметры.Утерян_НомерЛистка = НомерЛистка_;
			Утерянных_ = Утерянных_ + 1;
		Иначе
			ОбластьМакетаСтрокаТаблицы.Параметры.Испорчен_НомерЛистка = НомерЛистка_;
			Испорченных_ = Испорченных_ + 1;
		КонецЕсли; 
		ОбластьМакетаСтрокаТаблицы.Параметры.ПорядковыйНомер = ПорядковыйНомер;
		Результат.Вывести(ОбластьМакетаСтрокаТаблицы, ВыборкаДетальныеЗаписи.Уровень());
	КонецЦикла;
	ОбластьМакетаПодвал.Параметры.КоличествоИтого = ПорядковыйНомер;
	ОбластьМакетаПодвал.Параметры.КоличествоИспорченныхИтого = Испорченных_;
	ОбластьМакетаПодвал.Параметры.КоличествоУтерянныхИтого = Утерянных_;
	Результат.Вывести(ОбластьМакетаПодвал);
    
	Результат.ЗакончитьАвтогруппировкуСтрок();
		
КонецПроцедуры 


#КонецОбласти