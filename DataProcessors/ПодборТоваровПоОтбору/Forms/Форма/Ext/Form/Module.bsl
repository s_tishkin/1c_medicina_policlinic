﻿
#Область ПрограммныйИнтерфейс


Функция ПоместитьВоВременноеХранилищеНаСервере()
	
	Возврат ПоместитьВоВременноеХранилище(Объект.Товары.Выгрузить(), УникальныйИдентификатор);
	
КонецФункции

&НаКлиенте
Процедура ПеренестиВдокумент(Команда)
	
	Закрыть(ПоместитьВоВременноеХранилищеНаСервере());
	
КонецПроцедуры

// Процедура выполняет заполнение табличной части "Товары".
//
&НаСервере
Процедура ЗаполнитьТаблицуТоваровНаСервере(ПроверятьЗаполнение = Истина)
	
	// Поля необходимые для вывода в таблицу товаров на форме.
	СтруктураНастроек = Обработки.ПодборТоваровПоОтбору.ПолучитьПустуюСтруктуруНастроек();
	
	СтруктураНастроек.ОбязательныеПоля.Добавить("Номенклатура");
	
	СтруктураНастроек.КомпоновщикНастроек = КомпоновщикНастроек;
	СтруктураНастроек.ИмяМакетаСхемыКомпоновкиДанных = "Макет";
	
	Объект.Товары.Очистить();
	
	// Загрузка сформированного списка товаров.
	СтруктураРезультата = Обработки.ПодборТоваровПоОтбору.ПодготовитьСтруктуруДанных(СтруктураНастроек);
	Для Каждого СтрокаТЧ Из СтруктураРезультата.ТаблицаТоваров Цикл
		
		НоваяСтрока = Объект.Товары.Добавить();
		НоваяСтрока.Номенклатура         = СтрокаТЧ.Номенклатура;
		
	КонецЦикла;
	
	Элементы.Товары.Обновить();
	
КонецПроцедуры // ЗаполнитьТаблицуТоваровНаСервере()

// Процедура - обработчик команды "ЗаполнитьТаблицуТоваров".
//
&НаКлиенте
Процедура ЗаполнитьТаблицуТоваров(Команда)
	
	Если Объект.Товары.Количество() = 0 Тогда
		ТекстВопроса = СообщенияПользователю.Получить("Общие_ПриПерезаполненииВсеВведенныеВручнуюДанныеБудутПотеряны");
		ОбработкаОтвета = Новый ОписаниеОповещения("ПослеОтветаНаВопросОПерезаполнении", ЭтотОбъект);
		ПоказатьВопрос(ОбработкаОтвета, ТекстВопроса, РежимДиалогаВопрос.ДаНет,,КодВозвратаДиалога.Да);
	КонецЕсли;
	
КонецПроцедуры // ЗаполнитьТаблицуТоваров()

&НаКлиенте
Процедура ПослеОтветаНаВопросОПерезаполнении(Ответ, ДополнительныеПараметры) Экспорт
	
	Если Ответ = КодВозвратаДиалога.Да Тогда
		ЗаполнитьТаблицуТоваровНаСервере();
	КонецЕсли;
	
КонецПроцедуры


// Процедура выполняет загрузку настроек отбора из настроек по умолчанию.
//
&НаСервере
Процедура ЗагрузитьНастройкиОтбораПоУмолчанию()
	
	СхемаКомпоновкиДанных = Обработки.ПодборТоваровПоОтбору.ПолучитьМакет("Макет");
	КомпоновщикНастроек.Инициализировать(
		Новый ИсточникДоступныхНастроекКомпоновкиДанных(ПоместитьВоВременноеХранилище(СхемаКомпоновкиДанных, ЭтотОбъект.УникальныйИдентификатор))
	);
	КомпоновщикНастроек.ЗагрузитьНастройки(СхемаКомпоновкиДанных.НастройкиПоУмолчанию);
	КомпоновщикНастроек.Восстановить(СпособВосстановленияНастроекКомпоновкиДанных.ПроверятьДоступность);
	
КонецПроцедуры // ЗагрузитьНастройкиОтбораПоУмолчанию()

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ЗагрузитьНастройкиОтбораПоУмолчанию();
	
КонецПроцедуры


#КонецОбласти