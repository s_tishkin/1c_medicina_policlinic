﻿
Процедура ОбработкаПолученияФормы(ВидФормы, Параметры, ВыбраннаяФорма, ДополнительнаяИнформация, СтандартнаяОбработка)
	
	Если Не Параметры.Свойство("ПодключенноеОборудование") Тогда
		
		ПодключенноеОборудование = ПодсистемаПодключаемоеОборудованиеВызовСервера.ОборудованиеПодключенноеПоОрганизации(Параметры.Организация);
		Параметры.Вставить(
			"ПодключенноеОборудование",
			ПодключенноеОборудование);
		
		Если ПодключенноеОборудование.ККТ.Количество() = 0 Тогда
			СтандартнаяОбработка = Ложь;
			ВыбраннаяФорма = "ОшибкаПодключенияККТ";
		КонецЕсли;
		
	КонецЕсли;
	
	#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда
		
		ВалютаРегл = Константы.ВалютаРегламентированногоУчета.Получить();
		
		Реквизиты = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(Параметры.ДокументСсылка, "Валюта");
		
		Если Реквизиты.Валюта <> ВалютаРегл Тогда
			СтандартнаяОбработка = Ложь;
			ВыбраннаяФорма = "ОшибкаИспользуемойВалюты";
		КонецЕсли;
			
		
	#КонецЕсли
	
КонецПроцедуры
