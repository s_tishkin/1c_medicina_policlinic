﻿
#Область ПрограммныйИнтерфейс


&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	ОценкаПроизводительностиКлиентСервер.НачатьЗамерВремени("ОткрытиеЛекарственныхНазначений");
	
	ПараметрыФормы = Новый Структура("МедицинскийДокумент", ПараметрКоманды);
	ОткрытьФорму("Обработка.ФормированиеМедицинскихДокументов.Форма.УпрФормаЛекарственныхНазначений",
					ПараметрыФормы,
					ПараметрыВыполненияКоманды.Источник, ПараметрыВыполненияКоманды.Уникальность, 
					ПараметрыВыполненияКоманды.Окно);
КонецПроцедуры

#КонецОбласти