﻿
#Область ПрограммныйИнтерфейс

&НаКлиенте
Перем ПутьКДаннымНаКлиенте;

////////////////////////////////////////////////////////////////////////////////
// ПРОЦЕДУРЫ - ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ

// Обработчик события "ПриСозданииНаСервере" формы.
// Заполняет список выбора адресных объектов для загрузки.
//
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	ОбъектыПереданные = ?(Параметры.Свойство("Классификаторы"), Параметры.Классификаторы, Неопределено);
	
	ЗаполнитьТаблицуКлассификаторов(ОбъектыПереданные);
	
КонецПроцедуры

// Обработчик события "ПриОткрытии" формы
// Вызывает функциональность обновления интерфейса
//
&НаКлиенте
Процедура ПриОткрытии(Отказ)
	
	#Если ВебКлиент Тогда
		СообщенияПользователюКлиент.ВывестиПредупреждение("Классификаторы_ЗапретЗагрузкиВебКлиент",
													,,,,,Истина, "Классификаторы_ЗаголовокЗагрузкаКлассификаторовРЛС"
												);
		Закрыть();
	#КонецЕсли
	
	УстановитьИзмененияВИнтерфейсе();
	
КонецПроцедуры

// Процедура - обработчик события "ОбработкаОповещения" формы.
//
&НаКлиенте
Процедура ОбработкаОповещения(ИмяСобытия, Параметр, Источник)
	
	Если ИмяСобытия = "УМО_ПолученыФайлыСДискаИТС" И Источник = УникальныйИдентификатор Тогда
		
		Если ЗагрузитьКлассификаторы(Параметр) Тогда
			Состояние(СообщенияПользователю.Получить("Общие_Классификаторы_РЛС_УспешноЗагружены"));
			Закрыть(КодВозвратаДиалога.ОК);
		КонецЕсли;
		
		УстановитьСтатусЗагрузки("");
		
	КонецЕсли;
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ КОМАНД ФОРМЫ

// Обработчик нажатия на кнопку "Далее"
//
&НаКлиенте
Процедура КомандаДалее(Команда)
	
	ОчиститьСообщения();
	
	Если Элементы.СтраницыФормы.ТекущаяСтраница = Элементы.СтраницаВыбораКлассификаторов Тогда
		Если КоличествоОтмеченныхКлассификаторов() = 0 Тогда
			ОбщегоНазначенияКлиентСервер.СообщитьПользователю(СообщенияПользователю.Получить("Общие_ТребуетсяВыбратьХотяБыОдинКлассификатор"),,"КлассификаторыДляЗагрузки");
			Возврат;
		КонецЕсли;
		Элементы.СтраницыФормы.ТекущаяСтраница = Элементы.СтраницаЗагрузка;
	КонецЕсли;
	
	УстановитьИзмененияВИнтерфейсе();
	
КонецПроцедуры

// Обработчик нажатия на кнопку "Назад"
//
&НаКлиенте
Процедура КомандаНазад(Команда)
	
	Если Элементы.СтраницыФормы.ТекущаяСтраница = Элементы.СтраницаЗагрузка Тогда
		Элементы.СтраницыФормы.ТекущаяСтраница = Элементы.СтраницаВыбораКлассификаторов;
	КонецЕсли;
	
	УстановитьИзмененияВИнтерфейсе();
	
КонецПроцедуры

// Обработчик нажатия на "кнопку "ВыделитьВсе"
// командной панели элемента управления "АдресныеОбъектыДляЗагрузки"
// Выделяет все адресные объекты в списке адресных объектов для загрузки
//
&НаКлиенте
Процедура ВыделитьВсеВыполнить()
	
	Для Каждого ЭлементКлассификатор Из КлассификаторыДляЗагрузки Цикл
		ЭлементКлассификатор.Пометка = Истина;
	КонецЦикла;
	
КонецПроцедуры

// Обработчик нажатия на "кнопку "ОтменитьВыделитьВсе"
// командной панели элемента управления "АдресныеОбъектыДляЗагрузки"
// Снимает выделение со всех адресных объектов в списке
// адресных объектов для загрузки.
//
&НаКлиенте
Процедура ОтменитьВыделитьВсеВыполнить()
	
	Для Каждого ЭлементКлассификатор Из КлассификаторыДляЗагрузки Цикл
		ЭлементКлассификатор.Пометка = Ложь;
	КонецЦикла;
	
КонецПроцедуры

// Обработчик нажатия на кнопку "Загрузить" командной панели формы
//
&НаКлиенте
Процедура ЗагрузитьВыполнить()
	
	ОчиститьСообщения();
	
	ПолучитьКлассификаторы();
	
КонецПроцедуры


// Заполняет таблицу классификаторов с учетом переданных объектов.
//
&НаСервере
Процедура ЗаполнитьТаблицуКлассификаторов(ОбъектыПереданные)
	
	СправочникиРЛС = ИТСМедицинаКлиентСервер.ПолучитьСтруктуруСправочниковНаИТС();
	
	Для Каждого СправочникРЛС Из СправочникиРЛС Цикл
		
		Если Не СправочникРЛС.Значение.Свойство("ИмяМетаданных") Тогда
			Продолжить;
		КонецЕсли;
		
		СтрокаКлассификатор = КлассификаторыДляЗагрузки.Добавить();
		СтрокаКлассификатор.НаименованиеКлассификатора = СправочникРЛС.Значение.Наименование;
		СтрокаКлассификатор.НомерСправочника = СправочникРЛС.Значение.Номер;
		СтрокаКлассификатор.ИмяСправочника = СправочникРЛС.Ключ;
		
		Если Не ОбъектыПереданные = Неопределено Тогда
			
			Если Не ОбъектыПереданные.Найти(СправочникРЛС.Ключ) = Неопределено Тогда
				СтрокаКлассификатор.Пометка = Истина;
			КонецЕсли;
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры


// В зависимости от текущей страницы устанавливает доступность тех или иных полей для пользователя.
//
&НаКлиенте
Процедура УстановитьИзмененияВИнтерфейсе()
	
	Если Элементы.СтраницыФормы.ТекущаяСтраница = Элементы.СтраницаВыбораКлассификаторов Тогда
		Элементы.Назад.Доступность = Ложь;
		Элементы.Далее.Доступность = Истина;
	ИначеЕсли Элементы.СтраницыФормы.ТекущаяСтраница = Элементы.СтраницаЗагрузка Тогда
		Элементы.Назад.Доступность = Истина;
		Элементы.Далее.Доступность = Ложь;
	КонецЕсли;
	
КонецПроцедуры

// Возвращает количество помеченных адресных объектов
//
&НаКлиенте
Функция КоличествоОтмеченныхКлассификаторов()
	
	КоличествоОтмеченныхКлассификаторов = 0;
	
	Для Каждого ЭлементКлассификатор Из КлассификаторыДляЗагрузки Цикл
		Если ЭлементКлассификатор.Пометка Тогда
			КоличествоОтмеченныхКлассификаторов = КоличествоОтмеченныхКлассификаторов + 1;
		КонецЕсли;
	КонецЦикла;
	
	Возврат КоличествоОтмеченныхКлассификаторов;
	
КонецФункции

// Устанавливает текст статус загрузки
//
&НаКлиенте
Процедура УстановитьСтатусЗагрузки(знач Сообщение = "")
	
	СтатусЗагрузки = Сообщение;
	
	Если ПустаяСтрока(Сообщение) Тогда
		Элементы.СтраницыЗагрузки.ТекущаяСтраница = Элементы.ГруппаПустаяГруппа;
	Иначе
		Элементы.СтраницыЗагрузки.ТекущаяСтраница = Элементы.СтраницаСтатусЗагрузки;
	КонецЕсли;
	
	ОбновитьОтображениеДанных();
	
КонецПроцедуры

// Функция получает классификаторы с диска ИТС
//
&НаКлиенте
Процедура ПолучитьКлассификаторы()
	
	// Подготавливаем массив адресных объектов для загрузки
	Классификаторы = Новый Массив;
	
	ПутьКДаннымНаКлиенте = ИТСМедицинаКлиентСервер.ПолучитьИмяКаталога("rls_files");
	Для Каждого ЭлементКлассификатор Из КлассификаторыДляЗагрузки Цикл
		
		Если ЭлементКлассификатор.Пометка Тогда
			
			ЗапрашиваемыйКлассификатор = Новый Структура;
			ЗапрашиваемыйКлассификатор.Вставить("ИмяСправочника", ЭлементКлассификатор.ИмяСправочника);
			ЗапрашиваемыйКлассификатор.Вставить("ИмяФайла", ПутьКДаннымНаКлиенте + ЭлементКлассификатор.НомерСправочника + ".rls");
			
			Классификаторы.Добавить(ЗапрашиваемыйКлассификатор);
			
		КонецЕсли;
		
	КонецЦикла;
	
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("ЗапрашиваемыеФайлы", Классификаторы);
	ПараметрыФормы.Вставить("УникальныйИдентификаторИсточник", УникальныйИдентификатор);
	
	УстановитьСтатусЗагрузки(СообщенияПользователю.Получить("Общие_ЗагружаютсяКлассификаторы_РЛС"));
	
	ОписаниеОповещения_ = Новый ОписаниеОповещения("ПослеВыполненияЗагрузкиКлассификатора", ЭтотОбъект);
	ОткрытьФорму("Обработка.ЗагрузкаКлассификаторовРЛС.Форма.ПолучениеФайловСДискаИТС",
				ПараметрыФормы,
				ЭтотОбъект,,,,
				ОписаниеОповещения_,
				РежимОткрытияОкнаФормы.БлокироватьОкноВладельца
			);
	
КонецПроцедуры

&НаКлиенте
Процедура ПослеВыполненияЗагрузкиКлассификатора(Ответ, ДополнительныеПараметры) Экспорт
	
	Если Не Ответ = КодВозвратаДиалога.ОК Тогда
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(СообщенияПользователю.Получить("Общие_ЗагрузкаКлассификаторовБылаОтменена"));
		Каталог = Новый Файл(ПутьКДаннымНаКлиенте);
		Если Каталог.Существует() Тогда
			УдалитьФайлы(ПутьКДаннымНаКлиенте);
		КонецЕсли;
		УстановитьСтатусЗагрузки("");
	КонецЕсли;
	
КонецПроцедуры

// Релизация загрузки классификатора
//
&НаКлиенте
Функция ЗагрузитьКлассификаторы(МассивФайловДляЗагрузки)
	
	ИндикаторЗагрузки = 25;
	ОбновитьОтображениеДанных();
	
	// Первый этап - сжатие файлов перед передачей на сервер
	УстановитьСтатусЗагрузки(СообщенияПользователю.Получить("Общие_СжатиеФайловПередПередачейНаСервер"));
	
	Попытка
		
		ИмяФайлаАрхива = "rls_files.zip";
		ИТСМедицинаКлиент.СжатьФайлы(МассивФайловДляЗагрузки, ИмяФайлаАрхива, ПутьКДаннымНаКлиенте);
		
		ИндикаторЗагрузки = 35;
		ОбновитьОтображениеДанных();
		
		// Второй этап - передача файлов на сервер 1с:Предприятия
		УстановитьСтатусЗагрузки(СообщенияПользователю.Получить("Общие_ПередачаФайловНаСервер_1С_Предприятия"));
		
		ПутьКДаннымНаСервере = "";
		ИТСМедицинаКлиент.ПередатьФайлНаСервер(ПутьКДаннымНаКлиенте, ИмяФайлаАрхива, ПутьКДаннымНаСервере);
		
		ИндикаторЗагрузки = 50;
		ОбновитьОтображениеДанных();
		
		// Третий этап - загрузка классификаторов в справочники базы данных.
		// Предполагается что к текущему моменту на сервере в каталоге ПутьКДаннымНаСервере
		// содержатся все необходимые файлы данных.
		
		СтруктураСправочников = ИТСМедицинаКлиентСервер.ПолучитьСтруктуруСправочниковНаИТС();
		
		Для Каждого Классификатор Из МассивФайловДляЗагрузки Цикл
			
			Справочник = СтруктураСправочников[Классификатор.ИмяСправочника];
			
			УстановитьСтатусЗагрузки(СообщенияПользователю.Получить("Общие_ЗагружаютсяСведенияПоОбъекту",Новый Структура("Наименование",Справочник.Наименование)));
			
			ИТСМедицина.ЗагрузитьКлассификаторИзФайла(Справочник,
															   ПутьКДаннымНаСервере + Классификатор.НомерСправочника + ".rls");
			
			ИндикаторЗагрузки = 50 + (МассивФайловДляЗагрузки.Найти(Классификатор)+1) * 50 / МассивФайловДляЗагрузки.Количество();
			ОбновитьОтображениеДанных();
			
			ОповеститьОбИзменении(Тип("СправочникСсылка." + Справочник.ИмяМетаданных));
			
		КонецЦикла;
		
		ВозвращаемоеЗначение = Истина;
	
	Исключение
		ОбщегоНазначенияКлиентСервер.СообщитьПользователю(
				СообщенияПользователю.Получить("Общие_ОшибкаПриЗагрузкеКлассификаторов")
				+ КраткоеПредставлениеОшибки(ИнформацияОбОшибке()));
		ВозвращаемоеЗначение = Ложь;
	КонецПопытки;
	
	Каталог = Новый Файл(ПутьКДаннымНаКлиенте);
	Если Каталог.Существует() Тогда
		УдалитьФайлы(ПутьКДаннымНаКлиенте);
	КонецЕсли;
	ИТСМедицинаКлиентСервер.УдалитьФайлКаталог(ПутьКДаннымНаСервере);
	
	Возврат ВозвращаемоеЗначение;
	
КонецФункции

#КонецОбласти