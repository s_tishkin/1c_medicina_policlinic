﻿
#Область ПрограммныйИнтерфейс


&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)

	ЕстьПЛТ = Истина;

	ЭтотОбъект.ИсточникФинансирования = ЭтотОбъект.Параметры.ИсточникФинансирования;
	ЭтотОбъект.Соглашение = ЭтотОбъект.Параметры.Соглашение;
	ЭтотОбъект.МедицинскаяКартаИлиПациент = ЭтотОбъект.Параметры.МедицинскаяКартаИлиПациент;

	ТаблицаИстФинСоглашения_ = Неопределено;
	ЭтотОбъект.Элементы.ИсточникФинансирования.СписокВыбора.ЗагрузитьЗначения(
						ПолучитьСписокИстФинансирования(ЕстьПЛТ, ТаблицаИстФинСоглашения_)
	);

	ОбновитьСписокВыбораДоговора(ТаблицаИстФинСоглашения_);
КонецПроцедуры

////
 //	Функция: ПолучитьСписокИстФинансирования
 //     Служит для полуения списка доступных источников финансирования.
 //     
 //     
 //
 //	Параметры:
 //   ЕстьПЛТ
 //     Булево значение передаваемое по ссылке.
 //	    Устанавливается в Истина, если в списке есть ист. фин. ПЛТ
 // Возврат:
 //   СписокЗначений с источниками финансирования.
 ///
&НаСервере
Функция ПолучитьСписокИстФинансирования(ЕстьПЛТ,ТаблицаИстФинСоглашения)
	
	СписокИсточниковФинансирования = РаботаССоглашениями.ПолучитьСписокСоглашений(
											МедицинскаяКартаИлиПациент,,,Истина,ТаблицаИстФинСоглашения);
	
	Если СписокИсточниковФинансирования.НайтиПоЗначению(Справочники.ИсточникиФинансирования.ПЛТ) = Неопределено Тогда
		ЕстьПЛТ = Ложь;
	Иначе
		ЕстьПЛТ = Истина;
	КонецЕсли;
	
	МассивИсточникФинансирования = Новый Массив;
	Если СписокИсточниковФинансирования.Количество()=0 Тогда
		СообщенияПользователю.Показать("ОформлениеЗаказов_НетДоступныхИсточниковФинансирования");
	Иначе
		МассивИсточникФинансирования = СписокИсточниковФинансирования.ВыгрузитьЗначения();
	КонецЕСли;
	
	Возврат МассивИсточникФинансирования;
КонецФункции

////
 //	Процедура: ОбновитьСписокВыбораДоговора
 //     Служит для загрузки в список выбора соглашений
 //     доступных соглашений для текущего источника финансирования.
 //     
 //
 //	Параметры:
 //	
 ///
&НаСервере
Функция ОбновитьСписокВыбораДоговора(ТаблицаИстФинСоглашения = Неопределено)
	
	Элементы.Соглашение.СписокВыбора.Очистить();
	
	Если ТаблицаИстФинСоглашения = Неопределено Тогда
		СписокВыбора_ = РаботаССоглашениями.ПолучитьСписокСоглашений(
													МедицинскаяКартаИлиПациент,
													ИсточникФинансирования,
													ТекущаяДатаСеанса()
						);
		МассивДоговоров_ = СписокВыбора_.ВыгрузитьЗначения();
	Иначе
		Отбор_ = Новый Структура("Источник",ИсточникФинансирования);
		Результат_ = ТаблицаИстФинСоглашения.НайтиСтроки(Отбор_);
		МассивДоговоров_ = ТаблицаИстФинСоглашения.Скопировать(Результат_).ВыгрузитьКолонку("Соглашение");
	КонецЕсли;

	
	Элементы.Соглашение.СписокВыбора.ЗагрузитьЗначения(МассивДоговоров_);
	
	Если МассивДоговоров_.Количество() > 0 Тогда
		Возврат МассивДоговоров_[0];
	КонецЕсли;
	
КонецФункции

&НаКлиенте
Процедура ИсточникФинансированияПриИзменении(Элемент)
    СерверныйОбработчикИзмененияИстФина();
КонецПроцедуры

////
 //	Процедура: СерверныйОбработчикИзмененияИстФина
 //     Серверная процедура обработки события изменения источника финансирования.
 //     Вызывает процедуру загрузки соглашений.  Одно из этих соглашений делает текущим.
 //     Пересчитывает цены на услуги по новому соглашению.
 //
 //	Параметры:
 //	
 ///
&НаСервере
Процедура СерверныйОбработчикИзмененияИстФина()
	Первый = ОбновитьСписокВыбораДоговора();
    
    Соглашение = Первый;
КонецПроцедуры

&НаКлиенте
Процедура Выбрать(Команда)
    ЭтотОбъект.Закрыть(
            Новый Структура("Соглашение, ИсточникФинансирования",Соглашение,ИсточникФинансирования)
          );
КонецПроцедуры


#КонецОбласти