﻿
#Область ПрограммныйИнтерфейс


&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)

	ПараметрыФормы = Новый Структура("МедицинскаяКартаИлиПациент", ПараметрКоманды);

	ОткрытьФорму(
			"Обработка.ВыпискаИзМедицинскойКарты.Форма",
			ПараметрыФормы,
			ПараметрыВыполненияКоманды.Источник,
			ПараметрыВыполненияКоманды.Уникальность,
			ПараметрыВыполненияКоманды.Окно
	);

КонецПроцедуры


#КонецОбласти