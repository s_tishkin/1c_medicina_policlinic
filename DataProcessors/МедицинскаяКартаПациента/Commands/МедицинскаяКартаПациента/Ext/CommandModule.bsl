﻿#Область ПрограммныйИнтерфейс

&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	РегистратураКлиент.ОткрытьМедицинскуюКарту(ПараметрКоманды, Истина, ПараметрыВыполненияКоманды.Источник, ПараметрыВыполненияКоманды.Окно);
КонецПроцедуры

#КонецОбласти