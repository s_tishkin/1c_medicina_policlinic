﻿
#Область ПрограммныйИнтерфейс

///////////////////////////////////////////////////////////////
// Методы формы

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	ЭтотОбъект.МедицинскаяКарта = Параметры.МедицинскаяКарта;
	МедКарта = СтрЗаменить(Параметры.МедицинскаяКарта," ","");
	НовоеСлово = ОбщиеМеханизмыDICOM.ПолучитьТранслитерированноеСлово(МедКарта);
	ЭтотОбъект.МедицинскаяКарта = НовоеСлово;

	ИспользоватьОбщуюФормуПросмотраИзображений = (Метаданные.ОбщиеФормы.Найти("ФормаПросмотраИзображений") <> Неопределено);
	Элементы.ФормаКомандаОткрытьИзображение.Видимость = ИспользоватьОбщуюФормуПросмотраИзображений;
	ЭтотОбъект.ИспользоватьПроверкуПараметровФайла = Ложь;//= (Метаданные.ОбщиеМодули.Найти("ОбщиеМеханизмыКлиентСервер") <> Неопределено);
	Если Параметры.Свойство("Пациент") Тогда
		ЭтотОбъект.Пациент = Параметры.Пациент;
	КонецЕсли;
	
	НастройкиDICOM = ПодсистемаDICOMКлиентСервер.ПерсональныеНастройки(
												"ХранилищеDICOM_КаталогФайлов, ХранилищеDICOM_ИспользоватьКлиент"
											);
											
	Если НастройкиDICOM.ХранилищеDICOM_ИспользоватьКлиент = Истина Тогда
		НастройкиDICOM_КаталогФайлов = НастройкиDICOM.ХранилищеDICOM_КаталогФайлов;
	Иначе
		Попытка
			НастройкиDICOM_КаталогФайлов = Константы.ХранилищеDICOM_КаталогФайлов.Получить();
		Исключение
			НастройкиDICOM_КаталогФайлов = Неопределено;
		КонецПопытки;
	КонецЕсли;
	
	НастройкиDICOM_КаталогФайловНаКлиенте = НастройкиDICOM.ХранилищеDICOM_ИспользоватьКлиент;
	Если НастройкиDICOM_КаталогФайловНаКлиенте = Ложь Тогда
		Если ЗначениеЗаполнено(НастройкиDICOM_КаталогФайлов) Тогда
			СформироватьДеревоФайлов(ЭтотОбъект);
		Иначе
			Сообщение_ = Новый СообщениеПользователю;
			Сообщение_.Текст = НСтр("ru = ""Необходимо настроить каталог файлов хранилища DICOM. Обратитесь к администратору""");
			Сообщение_.Сообщить();
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	Если  НастройкиDICOM_КаталогФайловНаКлиенте = Истина Тогда
		Если ЗначениеЗаполнено(НастройкиDICOM_КаталогФайлов) Тогда
			СформироватьДеревоФайлов(ЭтотОбъект);
		Иначе
			Сообщение_ = Новый СообщениеПользователю;
			Сообщение_.Текст = НСтр("ru = ""Необходимо настроить каталог файлов хранилища DICOM. Обратитесь к администратору""");
			Сообщение_.Сообщить();
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

///////////////////////////////////////////////////////////////
// Методы элементов формы

 &НаКлиенте
Процедура ДеревоФайловПациентаПриАктивизацииСтроки(Элемент)
	ТекДанные =Элементы.ДеревоФайловПациента.ТекущиеДанные;
	Если ЭтотОбъект.ФлагПоказыватьПревью = Истина И ТекДанные <> Неопределено Тогда
		Если Не ТекДанные.УжеКонвертировали И НЕ ЗначениеЗаполнено(ТекДанные.АдресХранилища)
			И ЗначениеЗаполнено(ТекДанные.ПутьКФайлу)
		Тогда
			Ответ = Истина;
			Если ИспользоватьПроверкуПараметровФайла Тогда
				Ответ = ОбщиеМеханизмыКлиентСервер.ПолучитьРезультатПроверкиФайла(ТекДанные.Размер, ТекДанные.Расширение);
			КонецЕсли;
			Если Ответ = Истина Тогда
				ТекДанные.УжеКонвертировали = Истина;
				ПутьКФайлуJpeg_ = ОбщиеМеханизмыDICOMКлиентСервер.Dcm2Jpeg(ТекДанные.ПутьКФайлу);
				Если ЗначениеЗаполнено(ПутьКФайлуJpeg_) Тогда
					ТекДанные.АдресХранилища = ПоместитьВХранилище(Новый ДвоичныеДанные(ПутьКФайлуJpeg_));
					УдалитьФайлы(ПутьКФайлуJpeg_);
				КонецЕсли;
			КонецЕсли;
		Конецесли;
		ЭтотОбъект.ПревьюКартинки = ТекДанные.АдресХранилища;
	Иначе
		ЭтотОбъект.ПревьюКартинки = "";
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ФлагПоказыватьПревьюПриИзменении(Элемент)
	ДеревоФайловПациентаПриАктивизацииСтроки(Неопределено);
КонецПроцедуры

&НаКлиенте
Процедура ДеревоФайловПациентаВыбор(Элемент, ВыбраннаяСтрока, Поле, СтандартнаяОбработка)
	ОткрытьТекущееИзбражение();
КонецПроцедуры

&НаКлиенте
Процедура ДеревоФайловПациентаФлагПриИзменении(Элемент)
	УзелДерева = Элементы.ДеревоФайловПациента.ТекущиеДанные;
	Если НЕ УзелДерева = Неопределено Тогда
		УстановитьВыделениеУзла(УзелДерева, УзелДерева.Выбран);
	КонецЕсли;
КонецПроцедуры

///////////////////////////////////////////////////////////////
// Методы команд формы

&НаКлиенте
Процедура КомандаВыбратьВсе(Команда)
	УзелДерева = Неопределено;
	Пока АлгоритмыДляКоллекций.ОбходДерева_Следующий(ЭтотОбъект.ДеревоФайловПациента, УзелДерева) Цикл
		УзелДерева.Выбран = Истина;
	КонецЦикла;
КонецПроцедуры

&НаКлиенте
Процедура КомандаСнятьВыделение(Команда)
	
	УзелДерева = Неопределено;
	Пока АлгоритмыДляКоллекций.ОбходДерева_Следующий(ЭтотОбъект.ДеревоФайловПациента, УзелДерева) Цикл
		УзелДерева.Выбран = Ложь;
	КонецЦикла;
	
КонецПроцедуры


&НаКлиенте
Процедура КомандаВыбратьФайлы(Команда)
	ПараметрыВыбора = Новый Массив();
	УзелДерева = Неопределено;
	Пока АлгоритмыДляКоллекций.ОбходДерева_Следующий(ЭтотОбъект.ДеревоФайловПациента, УзелДерева) Цикл
		Если УзелДерева.Выбран = Истина И ЗначениеЗаполнено(УзелДерева.ПутьКФайлу) Тогда
			Если Не ЗначениеЗаполнено(УзелДерева.АдресХранилища) Тогда
				УзелДерева.УжеКонвертировали = Истина;
				ПутьКФайлуJpeg_ = ОбщиеМеханизмыDICOMКлиентСервер.Dcm2Jpeg(УзелДерева.ПутьКФайлу);
				Если ЗначениеЗаполнено(ПутьКФайлуJpeg_) Тогда
					УзелДерева.АдресХранилища = ПоместитьВХранилище(Новый ДвоичныеДанные(ПутьКФайлуJpeg_));
					УдалитьФайлы(ПутьКФайлуJpeg_);
				КонецЕсли;
			КонецЕсли;
			СтрПараметров = Новый Структура(
								"АдресХранилища, ПутьКФайлу, ПолноеИмяФайла,ИмяФайла,
								|Размер,Расширение,ВремяИзменения,УниверсальноеВремяИзменения"
							);
			ЗаполнитьЗначенияСвойств(СтрПараметров, УзелДерева);
			СтрПараметров.Расширение = "jpg";
			ПараметрыВыбора.Добавить(СтрПараметров);
		КонецЕсли;
	КонецЦикла;
	ЭтотОбъект.Закрыть(ПараметрыВыбора);
КонецПроцедуры

&НаКлиенте
Процедура КомандаПеречитатьДанные(Команда)
	Если  НастройкиDICOM_КаталогФайловНаКлиенте = Ложь Тогда
		Если ЗначениеЗаполнено(НастройкиDICOM_КаталогФайлов) Тогда
			СформироватьДеревоФайлов(ЭтотОбъект);
		Иначе
			Сообщение_ = Новый СообщениеПользователю;
			Сообщение_.Текст = НСтр("ru = ""Необходимо настроить каталог файлов хранилища DICOM. Обратитесь к администратору""");
			Сообщение_.Сообщить();
		КонецЕсли;
	Иначе
		СформироватьДеревоФайлов(ЭтотОбъект);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура КомандаОткрытьИзображение(Команда)
	ОткрытьТекущееИзбражение();
КонецПроцедуры

//////////////////////////////////////////////////////////////
// Вспомогательные методы

// формирует таблицу и строет по таблице дерево для отображения в форме
&НаКлиентеНаСервереБезКонтекста
Процедура СформироватьДеревоФайлов(Форма)
	УзелДерева = Неопределено;
	ЭлДерева = Форма.ДеревоФайловПациента.ПолучитьЭлементы();
	Пока АлгоритмыДляКоллекций.ОбходДерева_Следующий(Форма.ДеревоФайловПациента, УзелДерева) Цикл
		ЭлДерева.Удалить(УзелДерева);
	КонецЦикла;
	
	Форма.ТаблицаФайловПациента.Очистить();
	
	СчетчикУИ = 0;
	ПредтавлениеПациента = Форма.Пациент + "(" + Форма.МедицинскаяКарта+ ")";
	
	ПутьКФайлам = Форма.НастройкиDICOM_КаталогФайлов;
	
	НайденныеФайлы = НайтиФайлы(ПутьКФайлам, "*");
	// найдем все каталоги в каталоге хранения
	Для каждого НайденныйФайл Из НайденныеФайлы Цикл
		Если НайденныйФайл.ЭтоКаталог() = Истина Тогда
			// найдем файл с информацией
			ИнформационныйФайл = Новый Файл(НайденныйФайл.ПолноеИмя + "\" + "info.txt");
			Если ИнформационныйФайл.Существует() Тогда
				// извлечем и структурируем текст с информацией
				ОбъектЧтения = Новый ТекстовыйДокумент();
				ОбъектЧтения.Прочитать(ИнформационныйФайл.ПолноеИмя);
				ТекстОсновногоЗаголовка = ОбъектЧтения.ПолучитьТекст();
				
				ДанныеОсновногоЗаголовка = ДанныеDICOM.ПрочитатьЗаголовокDICOM(ТекстОсновногоЗаголовка);
				ДатаИсследования = ДанныеDICOM.ДатаИсследования(ДанныеОсновногоЗаголовка);
				// отберем данные нашего пациента по карте
				Если ДанныеОсновногоЗаголовка.Получить(ДанныеDICOM.КодPatientID()) = Форма.МедицинскаяКарта И 
					(НЕ ЗначениеЗаполнено(Форма.Период.ДатаНачала) ИЛИ 
						Форма.Период.ДатаНачала <= ДанныеDICOM.ДатаИсследования(ДанныеОсновногоЗаголовка)
					) И
					(НЕ ЗначениеЗаполнено(Форма.Период.ДатаОкончания) ИЛИ 
						Форма.Период.ДатаОкончания >= ДанныеDICOM.ДатаИсследования(ДанныеОсновногоЗаголовка)
					)
				Тогда
					// найдем вложенные папки серий
					НайденныеПапкиСерий = НайтиФайлы(НайденныйФайл.ПолноеИмя, "*");
					Для каждого ПапкаСерии Из НайденныеПапкиСерий Цикл
						// найдем файл с информацией о серии
						ИнформационныйФайлСерии = Новый Файл(ПапкаСерии.ПолноеИмя + "\" + "info.txt");
						Если ИнформационныйФайлСерии.Существует() Тогда
							ОбъектЧтения = Новый ТекстовыйДокумент();
							ОбъектЧтения.Прочитать(ИнформационныйФайлСерии.ПолноеИмя);
							ТекстЗаголовкаСерии = ОбъектЧтения.ПолучитьТекст();
							ДанныеЗаголовкаСерии = ДанныеDICOM.ПрочитатьЗаголовокDICOM(ТекстЗаголовкаСерии);
							// найдем вложенные файлы в папках серий
							НайденныеИзображенияВПапкахСерий = НайтиФайлы(ПапкаСерии.ПолноеИмя, "*.dcm");
							Для каждого ИзображениеВПапкеСерии Из НайденныеИзображенияВПапкахСерий Цикл
								НоваяСтрока = Форма.ТаблицаФайловПациента.Добавить();
								НоваяСтрока.Представление = ПредтавлениеПациента;
								НоваяСтрока.ДатаИсследования =ДанныеDICOM.ДатаИсследования(ДанныеОсновногоЗаголовка);
								НоваяСтрока.ВремяИсследования =ДанныеDICOM.ВремяИсследования(ДанныеОсновногоЗаголовка);
								НоваяСтрока.УИДИсследования = ДанныеОсновногоЗаголовка.Получить(ДанныеDICOM.КодStudyInstanceUID()); //УИ исследования
								НоваяСтрока.ОписаниеИсследования = ДанныеОсновногоЗаголовка.Получить(ДанныеDICOM.КодStudyDescription());
								НоваяСтрока.Комментарии = ДанныеОсновногоЗаголовка.Получить(ДанныеDICOM.КодPatientComments());
								НоваяСтрока.РегистрационныйНомер = ДанныеОсновногоЗаголовка.Получить(ДанныеDICOM.КодAccessionNumber());
								// информация по серии
								НоваяСтрока.ТипИсследования = ДанныеЗаголовкаСерии.Получить(ДанныеDICOM.КодModality());
								НоваяСтрока.УИДСерии =ДанныеЗаголовкаСерии.Получить(ДанныеDICOM.КодSeriesInstanceUID()); //УИ серии
								НоваяСтрока.НомерСерии = ДанныеЗаголовкаСерии.Получить(ДанныеDICOM.КодSeriesNumber()); //Номер серии
								// данные пациента
								НоваяСтрока.Пациент = ДанныеОсновногоЗаголовка.Получить(ДанныеDICOM.КодPatientName());
								НоваяСтрока.КодПациента = ДанныеОсновногоЗаголовка.Получить(ДанныеDICOM.КодPatientID());
								НоваяСтрока.Пол = ДанныеDICOM.ПолПациента(ДанныеОсновногоЗаголовка);
								НоваяСтрока.ДатаРождения =ДанныеDICOM.ДатаРожденияПациента(ДанныеОсновногоЗаголовка);
								// данные файла
								НоваяСтрока.ИмяФайла = ИзображениеВПапкеСерии.ИмяБезРасширения;
								НоваяСтрока.Расширение = ИзображениеВПапкеСерии.Расширение;
								НоваяСтрока.ПолноеИмяФайла = НоваяСтрока.ИмяФайла + НоваяСтрока.Расширение;
								НоваяСтрока.ПутьКФайлу = ИзображениеВПапкеСерии.ПолноеИмя;
								НоваяСтрока.Размер = ИзображениеВПапкеСерии.Размер();
								НоваяСтрока.УниверсальноеВремяИзменения = ИзображениеВПапкеСерии.ПолучитьУниверсальноеВремяИзменения();
								НоваяСтрока.ВремяИзменения = ИзображениеВПапкеСерии.ПолучитьВремяИзменения();
							КонецЦикла;
						КонецЕсли;
					КонецЦикла; 
				КонецЕсли;
			КонецЕсли;
			
		КонецЕсли;
	КонецЦикла; 
	
	
	Для каждого СтрокаТаблицы Из Форма.ТаблицаФайловПациента Цикл
	
		УзелПациента_ = АлгоритмыДляКоллекций.НайтиВДереве(
							Форма.ДеревоФайловПациента, Новый Структура("Пациент", СтрокаТаблицы.Пациент), , Истина
						);
		Если УзелПациента_ = Неопределено Тогда
			УзелПациента_ = Форма.ДеревоФайловПациента.ПолучитьЭлементы().Добавить();
			УзелПациента_.Пациент = СтрокаТаблицы.Пациент;
			УзелПациента_.КомментарииОписание = СтрокаТаблицы.Комментарии;
			УзелПациента_.ДатаРожденияРегистрационныйНомерКоличествоСнимков = СтрокаТаблицы.ДатаРождения;
			УзелПациента_.Пол = СтрокаТаблицы.Пол;
			УзелПациента_.КодПациентаТипИсследования = СтрокаТаблицы.КодПациента;
			УзелПациента_.Представление = СтрокаТаблицы.Пациент;
			УзелПациента_.КартинкаОтображения = БиблиотекаКартинок.Пользователь;
		КонецЕсли;
		
		УзелИсследования_ = АлгоритмыДляКоллекций.НайтиВДереве(
				Форма.ДеревоФайловПациента,  
				Новый Структура("УИДИсследования", СтрокаТаблицы.УИДИсследования), , Истина
			);
		Если УзелИсследования_ = Неопределено Тогда
			УзелИсследования_ = УзелПациента_.ПолучитьЭлементы().Добавить();
			УзелИсследования_.УИДИсследования = СтрокаТаблицы.УИДИсследования;
			УзелИсследования_.КомментарииОписание = СтрокаТаблицы.ОписаниеИсследования;
			УзелИсследования_.ДатаРожденияРегистрационныйНомерКоличествоСнимков = СтрокаТаблицы.РегистрационныйНомер;
			УзелИсследования_.Представление = СтрокаТаблицы.ДатаИсследования + 
					(СтрокаТаблицы.ВремяИсследования - '00010101');
			УзелИсследования_.КартинкаОтображения = БиблиотекаКартинок.Календарь;
		КонецЕсли;
		
		УзелСерии_ = АлгоритмыДляКоллекций.НайтиВДереве(
				Форма.ДеревоФайловПациента,  
				Новый Структура("УИДСерии", СтрокаТаблицы.УИДСерии), , Истина
			);
			
		Если УзелСерии_ = Неопределено Тогда
			УзелСерии_ = УзелИсследования_.ПолучитьЭлементы().Добавить();
			УзелСерии_.КодПациентаТипИсследования = СтрокаТаблицы.ТипИсследования;
			УзелСерии_.УИДСерии = СтрокаТаблицы.УИДСерии;
			УзелСерии_.Представление = СтрокаТаблицы.НомерСерии;
			УзелСерии_.КартинкаОтображения = БиблиотекаКартинок.ВыборКомпоновкиДанных;
		КонецЕсли;
		УзелСерии_.ДатаРожденияРегистрационныйНомерКоличествоСнимков = УзелСерии_.КоличествоСнимков + 1;
		
		УзелСнимок_ = УзелСерии_.ПолучитьЭлементы().Добавить();
		ЗаполнитьЗначенияСвойств(УзелСнимок_, СтрокаТаблицы);
		УзелСнимок_.УИДИсследования = "";
		УзелСнимок_.УИДСерии = "";
		УзелСнимок_.Пациент = "";
		УзелСнимок_.Пол = "";
		УзелСнимок_.Представление = СтрокаТаблицы.ИмяФайла;
		УзелСнимок_.КартинкаОтображения = БиблиотекаКартинок.Картинка;
	
	КонецЦикла;
	
КонецПроцедуры

// открывает изображение на просмотр
&НаКлиенте
Процедура ОткрытьТекущееИзбражение()
	
	ТекДанные =Элементы.ДеревоФайловПациента.ТекущиеДанные;
	Если ИспользоватьОбщуюФормуПросмотраИзображений И ТекДанные <> Неопределено Тогда
		ИмяОткрываемойФормы = "ОбщаяФорма.ФормаПросмотраИзображений";
		ПараметрыОткрытия = Новый Структура;
		СвойстваФайла = Новый Структура;
		СвойстваФайла.Вставить("ИмяФайла",ТекДанные.ИмяФайла);
		СвойстваФайла.Вставить("Расширение",ТекДанные.Расширение);
		Если НЕ ЗначениеЗаполнено(ТекДанные.АдресХранилища) И ЗначениеЗаполнено(ТекДанные.ПутьКФайлу) Тогда
			Ответ = Истина;
			Если ИспользоватьПроверкуПараметровФайла Тогда
				Ответ = ОбщиеМеханизмыКлиентСервер.ПолучитьРезультатПроверкиФайла(ТекДанные.Размер, ТекДанные.Расширение);
			КонецЕсли;
			Если Ответ = Истина Тогда
				ТекДанные.АдресХранилища = ПоместитьВХранилище(Новый ДвоичныеДанные(ТекДанные.ПутьКФайлу));
			КонецЕсли;
		КонецЕсли;
		ПараметрыОткрытия.Вставить("Ключ",ТекДанные.АдресХранилища);
		ПараметрыОткрытия.Вставить("СвойстваФайла",СвойстваФайла);
 		ОткрытьФорму(ИмяОткрываемойФормы,
					ПараметрыОткрытия,
					ЭтотОбъект,,,,,
					РежимОткрытияОкнаФормы.БлокироватьОкноВладельца
				);
	КонецЕсли;
	
КонецПроцедуры

// Устанавливает или снимает флаг Выбран у узела и всех его дочерних элементов
&НаКлиенте
Процедура УстановитьВыделениеУзла(Узел, Выбран)
	Узел.Выбран = Выбран;
	ЭлементыУзла = Узел.ПолучитьЭлементы();
	Если ЭлементыУзла.Количество() > 0 Тогда
		Для каждого Узе Из ЭлементыУзла Цикл
			УстановитьВыделениеУзла(Узе, Выбран);
		КонецЦикла;
	КонецЕсли;
КонецПроцедуры

&НаСервере
////
// Функция: ПоместитьВХранилище
//   Помещает файл в хранилище, используя путь к файлу и уникальный идентификатор.
//
// Параметры:
//   ДД
//	 Двоичные данные файла.
//
// Возврат:
//   Адрес файла в хранилище
///
Функция ПоместитьВХранилище(ДД) Экспорт
	Адрес_ = ПоместитьВоВременноеХранилище(ДД, Новый УникальныйИдентификатор());
	Возврат Адрес_;
КонецФункции


#КонецОбласти