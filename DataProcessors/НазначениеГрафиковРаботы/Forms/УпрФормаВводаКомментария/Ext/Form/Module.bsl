﻿
#Область ПрограммныйИнтерфейс


&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	ТекстСообщения = СообщенияПользователю.Получить("ГрафикиРабочихМест_КомментарийНеМожетБытьПустым");
КонецПроцедуры

&НаКлиенте
Процедура ОК_Выполнить(Команда)
	Если ПустаяСтрока(ЭтотОбъект.ТекстКомментария) Тогда
		Сообщение_ = Новый СообщениеПользователю;
		Сообщение_.Текст = ТекстСообщения;
		Сообщение_.Сообщить();
	Иначе
		ЭтотОбъект.Закрыть(ЭтотОбъект.ТекстКомментария);
	КонецЕсли;	
КонецПроцедуры

&НаКлиенте
Процедура Отмена_Выполнить(Команда)
	ЭтотОбъект.Закрыть();
КонецПроцедуры



#КонецОбласти