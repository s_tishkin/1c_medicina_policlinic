﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ПрограммныйИнтерфейс

////
 // Процедура: СоздатьИерархиюЭлементов
 //   Описание.
 //
 // Параметры:
 //   ВерсияПрейскуранта (СправочниСсылка.ВерсияПрейскуранта)
 //     Описание.
 //   СписокНоменклатуры (Массив)
 //     Описание
///
Процедура СоздатьИерархиюЭлементов(ВерсияПрейскуранта, СписокНоменклатуры, СозданПоЗаявке = Ложь) Экспорт

	ВидЦены_ = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ВерсияПрейскуранта, "ВидЦены");
	
	Запрос_ = Новый Запрос;
	Запрос_.Текст =
	"ВЫБРАТЬ
	|	СправочникНоменклатура.Ссылка КАК Номенклатура,
	|	ЦеныНоменклатурыСрезПоследних.Цена
	|ИЗ
	|	Справочник.Номенклатура КАК СправочникНоменклатура
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЦеныНоменклатуры.СрезПоследних(&ДатаСреза, ВидЦены = &ВидЦены И Номенклатура В (&СписокНоменклатуры)) КАК ЦеныНоменклатурыСрезПоследних
	|		ПО ЦеныНоменклатурыСрезПоследних.Номенклатура = СправочникНоменклатура.Ссылка
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ПроектПрейскуранта КАК СправочникПроектПрейскуранта
	|		ПО ЦеныНоменклатурыСрезПоследних.Номенклатура = СправочникПроектПрейскуранта.Номенклатура
	|			И (СправочникПроектПрейскуранта.ВерсияПрейскуранта = &ВерсияПрейскуранта)
	|			И (СправочникПроектПрейскуранта.ПометкаУдаления = ЛОЖЬ)
	|ГДЕ
	|	СправочникПроектПрейскуранта.Ссылка ЕСТЬ NULL 
	|	И СправочникНоменклатура.Ссылка В (&СписокНоменклатуры)
	|
	|УПОРЯДОЧИТЬ ПО
	|	СправочникНоменклатура.Артикул";
	
	Запрос_.УстановитьПараметр("ДатаСреза", ТекущаяДатаСеанса());
	Запрос_.УстановитьПараметр("ВерсияПрейскуранта", ВерсияПрейскуранта);
	Запрос_.УстановитьПараметр("ВидЦены", ВидЦены_);
	Запрос_.УстановитьПараметр("СписокНоменклатуры", СписокНоменклатуры);
	
	Выборка_ = Запрос_.Выполнить().Выбрать();
	
	НачатьТранзакцию();
	Пока Выборка_.Следующий() Цикл
		СоздатьЭлементПроекта(ВерсияПрейскуранта, Выборка_.Цена, Выборка_.Номенклатура, СозданПоЗаявке);
	КонецЦикла;
	ЗафиксироватьТранзакцию();
	
КонецПроцедуры

////
 // Функция: СоздатьЭлементПроекта
 //   Создает элемент справочника и всех необходимых предков
 //
 // Параметры:
 //   П1 (Тип)
 //     Описание
 //   П2 (Тип)
 //     Описание
 // Возврат:
 //   
///
Функция СоздатьЭлементПроекта(ВерсияПрейскуранта, Цена = Неопределено, Номенклатура, СозданПоЗаявке = Ложь) Экспорт
	
	ЭтоГруппа_ = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Номенклатура, "ЭтоГруппа");
	
	Если ЭтоГруппа_ Тогда
		НовыйЭлемент_ = Справочники.ПроектПрейскуранта.СоздатьГруппу();
	Иначе
		НовыйЭлемент_ = Справочники.ПроектПрейскуранта.СоздатьЭлемент();
	КонецЕсли;
	
	НовыйЭлемент_.Номенклатура = Номенклатура;
	НовыйЭлемент_.ВерсияПрейскуранта = ВерсияПрейскуранта;
	НовыйЭлемент_.Наименование = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Номенклатура, "Наименование");
	НовыйЭлемент_.Артикул = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Номенклатура, "Артикул");
	
	Если Не ЭтоГруппа_ Тогда
		НовыйЭлемент_.Цена = Цена;
		НовыйЭлемент_.СтавкаНДС = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Номенклатура, "СтавкаНДС");;
		НовыйЭлемент_.СозданПоЗаявке = СозданПоЗаявке;
	КонецЕсли;
	
	Родитель_ = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Номенклатура, "Родитель");
	Если ЗначениеЗаполнено(Родитель_) Тогда
		НовыйЭлемент_.Родитель = НайтиЭлементПроекта(ВерсияПрейскуранта, Родитель_);
		Если Не ЗначениеЗаполнено(НовыйЭлемент_.Родитель) Тогда
			НовыйЭлемент_.Родитель = СоздатьЭлементПроекта(ВерсияПрейскуранта,, Родитель_);
		КонецЕсли;
	КонецЕсли;
	
	НовыйЭлемент_.Записать();
	
	Возврат НовыйЭлемент_.Ссылка;
		
КонецФункции

////
 // Функция: НайтиЭлементПроекта
 //   Ищет элемент справочника по реквизиту Номенклатура и ВерсияПрейскуранта
 //
 // Параметры:
 //   П1 (Тип)
 //     Описание
 //   П2 (Тип)
 //     Описание
 // Возврат:
 //   
///
Функция НайтиЭлементПроекта(ВерсияПрейскуранта, Номенклатура) Экспорт
	Запрос_ = Новый Запрос;
	Запрос_.Текст =
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	ПроектПрейскуранта.Ссылка
	|ИЗ
	|	Справочник.ПроектПрейскуранта КАК ПроектПрейскуранта
	|ГДЕ
	|	ПроектПрейскуранта.Номенклатура = &Номенклатура
	|	И ПроектПрейскуранта.ВерсияПрейскуранта = &ВерсияПрейскуранта
	|	И НЕ ПроектПрейскуранта.ПометкаУдаления";
	
	Запрос_.УстановитьПараметр("Номенклатура", Номенклатура);
	Запрос_.УстановитьПараметр("ВерсияПрейскуранта", ВерсияПрейскуранта);
	Выборка_ = Запрос_.Выполнить().Выбрать();
	
	Если Выборка_.Следующий() Тогда
		Возврат Выборка_.Ссылка;
	Иначе
		Возврат Неопределено;
	КонецЕсли;
	
КонецФункции

////
 // Процедура: ВнестиИзмененияВСправочникНоменклатура
 //   Приводит структуру справочника Номенклатура в соответствие с проектом
 //
 // Параметры:
 //   ВерсияПрейскуранта (СправочниСсылка.ВерсияПрейскуранта)
 //     Описание.
///
Процедура ВнестиИзмененияВСправочникНоменклатура(ВерсияПрейскуранта) Экспорт
	// Переименуем элементы
	Запрос_ = Новый Запрос;
	Запрос_.Текст =
	"ВЫБРАТЬ
	|	ПроектПрейскуранта.Номенклатура,
	|	ПроектПрейскуранта.Наименование,
	|	ПроектПрейскуранта.Артикул,
	|	ПроектПрейскуранта.СтавкаНДС,
	|	ЕСТЬNULL(ПроектПрейскурантаРодитель.Номенклатура, ЗНАЧЕНИЕ(Справочник.Номенклатура.ПустаяСсылка)) КАК Родитель
	|ИЗ
	|	Справочник.ПроектПрейскуранта КАК ПроектПрейскуранта
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.Номенклатура КАК СправочникНоменклатура
	|		ПО ПроектПрейскуранта.Номенклатура = СправочникНоменклатура.Ссылка
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ПроектПрейскуранта КАК ПроектПрейскурантаРодитель
	|		ПО ПроектПрейскуранта.Родитель = ПроектПрейскурантаРодитель.Ссылка
	|ГДЕ
	|	НЕ ПроектПрейскуранта.УдалитьИзСправочника
	|	И ПроектПрейскуранта.ВерсияПрейскуранта = &ВерсияПрейскуранта
	|	И ПроектПрейскуранта.Номенклатура <> ЗНАЧЕНИЕ(Справочник.Номенклатура.ПустаяСсылка)
	|	И НЕ ПроектПрейскуранта.ПометкаУдаления
	|	И (ПроектПрейскуранта.Наименование <> СправочникНоменклатура.Наименование
	|			ИЛИ ПроектПрейскуранта.Артикул <> СправочникНоменклатура.Артикул
	|			ИЛИ СправочникНоменклатура.Родитель <> ЕСТЬNULL(ПроектПрейскурантаРодитель.Номенклатура, ЗНАЧЕНИЕ(Справочник.Номенклатура.ПустаяСсылка))
	|			ИЛИ ПроектПрейскуранта.СтавкаНДС <> СправочникНоменклатура.СтавкаНДС)";
	
	Запрос_.УстановитьПараметр("ВерсияПрейскуранта", ВерсияПрейскуранта);
	Выборка_ = Запрос_.Выполнить().Выбрать();
	
	Пока Выборка_.Следующий() Цикл
		НоменклатураОбъект_ = Выборка_.Номенклатура.ПолучитьОбъект();
		НоменклатураОбъект_.ДополнительныеСвойства.Вставить("НеПерезаписыватьСпецификацию", Истина);
		
		Если НоменклатураОбъект_.Наименование <> Выборка_.Наименование Тогда
			НоменклатураОбъект_.Наименование = Выборка_.Наименование;
			НоменклатураОбъект_.НаименованиеПолное = Выборка_.Наименование;
		КонецЕсли;

		Если НоменклатураОбъект_.СтавкаНДС <> Выборка_.СтавкаНДС Тогда
			НоменклатураОбъект_.СтавкаНДС = Выборка_.СтавкаНДС;
		КонецЕсли;
		
		НоменклатураОбъект_.Артикул = Выборка_.Артикул;
		
		Если НоменклатураОбъект_.Родитель <> Выборка_.Родитель Тогда
			НоменклатураОбъект_.Родитель = Выборка_.Родитель;
		КонецЕсли;
		
		НоменклатураОбъект_.Записать();
	КонецЦикла;
	
	// Создадим новые элементы
	Запрос_.Текст =
	"ВЫБРАТЬ
	|	ПроектПрейскуранта.Наименование,
	|	ПроектПрейскуранта.Артикул,
	|	ПроектПрейскуранта.СтавкаНДС,
	|	ПроектПрейскурантаРодитель.Номенклатура КАК Родитель,
	|	ПроектПрейскуранта.Ссылка,
	|	ПроектПрейскуранта.Биоматериал
	|ИЗ
	|	Справочник.ПроектПрейскуранта КАК ПроектПрейскуранта
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ПроектПрейскуранта КАК ПроектПрейскурантаРодитель
	|		ПО ПроектПрейскуранта.Родитель = ПроектПрейскурантаРодитель.Ссылка
	|ГДЕ
	|	НЕ ПроектПрейскуранта.УдалитьИзСправочника
	|	И ПроектПрейскуранта.ВерсияПрейскуранта = &ВерсияПрейскуранта
	|	И ПроектПрейскуранта.Номенклатура = ЗНАЧЕНИЕ(Справочник.Номенклатура.ПустаяСсылка)
	|	И НЕ ПроектПрейскуранта.ПометкаУдаления";
	
	Выборка_ = Запрос_.Выполнить().Выбрать();
	
	Пока Выборка_.Следующий() Цикл
		НачатьТранзакцию();
		НоменклатураСсылка_ = СоздатьЭлементНоменклатуры(Выборка_.Наименование, Выборка_.Артикул, Выборка_.СтавкаНДС, Выборка_.Родитель, Выборка_.Биоматериал);
		
		СправочникОбъект_ = Выборка_.Ссылка.ПолучитьОбъект();
		СправочникОбъект_.ДополнительныеСвойства.Вставить("НеПерезаписыватьСпецификацию", Истина);
		СправочникОбъект_.Номенклатура = НоменклатураСсылка_;
		СправочникОбъект_.Записать();
		ЗафиксироватьТранзакцию();
	КонецЦикла;
	
	// Поместим в архив номенклатуры удаленные из прайса и отсутствующие в других прайсах.
	ВидЦены_ = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ВерсияПрейскуранта, "ВидЦены");
	
	Запрос_.Текст =
	"ВЫБРАТЬ
	|	ПроектПрейскуранта.Номенклатура
	|ИЗ
	|	Справочник.ПроектПрейскуранта КАК ПроектПрейскуранта
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.Номенклатура КАК СправочникНоменклатура
	|		ПО ПроектПрейскуранта.Номенклатура = СправочникНоменклатура.Ссылка
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЦеныНоменклатуры КАК РегистрСведенийЦеныНоменклатуры
	|		ПО (РегистрСведенийЦеныНоменклатуры.Номенклатура = СправочникНоменклатура.Ссылка)
	|			И (РегистрСведенийЦеныНоменклатуры.ВидЦены <> &ВидЦены)
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЦеныНоменклатуры КАК РегистрСведенийЦеныНоменклатурыБудущее
	|		ПО (РегистрСведенийЦеныНоменклатуры.Номенклатура = СправочникНоменклатура.Ссылка)
	|			И (РегистрСведенийЦеныНоменклатуры.ВидЦены = РегистрСведенийЦеныНоменклатуры.ВидЦены)
	|			И (РегистрСведенийЦеныНоменклатурыБудущее.Период > РегистрСведенийЦеныНоменклатуры.Период)
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЦеныНоменклатурыПоСоглашениям КАК РегистрСведенийЦеныНоменклатурыПоСоглашениям
	|		ПО ПроектПрейскуранта.Номенклатура = РегистрСведенийЦеныНоменклатурыПоСоглашениям.Номенклатура
	|ГДЕ
	|	ПроектПрейскуранта.УдалитьИзСправочника
	|	И ПроектПрейскуранта.ВерсияПрейскуранта = &ВерсияПрейскуранта
	|	И НЕ ПроектПрейскуранта.ПометкаУдаления
	|	И СправочникНоменклатура.Актуальность
	|	И РегистрСведенийЦеныНоменклатурыБудущее.Период ЕСТЬ NULL 
	|	И ЕСТЬNULL(РегистрСведенийЦеныНоменклатуры.Цена,0) = 0
	|	И РегистрСведенийЦеныНоменклатурыПоСоглашениям.Номенклатура ЕСТЬ NULL ";
	
	Запрос_.УстановитьПараметр("ВидЦены", ВидЦены_);
	
	Выборка_ = Запрос_.Выполнить().Выбрать();
	
	Пока Выборка_.Следующий() Цикл
		Попытка
			НоменклатураОбъект_ = Выборка_.Номенклатура.ПолучитьОбъект();
			НоменклатураОбъект_.ДополнительныеСвойства.Вставить("НеПерезаписыватьСпецификацию", Истина);
			НоменклатураОбъект_.Актуальность = Ложь;
			НоменклатураОбъект_.Записать();
		Исключение
			// в модуле объекта при отказе выдастся предупреждение
			//Позаимствовано из ОбщиеМеханизмы.Архивность_УстановитьАрхивность
		КонецПопытки;
	КонецЦикла;

КонецПроцедуры

Функция СоздатьЭлементНоменклатуры(Наименование, Артикул, СтавкаНДС, Родитель, Биоматериал)

	// Найдем подходящий вид номенклатуры
	Запрос_ = Новый Запрос;
	Запрос_.Текст =
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	ВидыНоменклатуры.Ссылка,
	|	ВидыНоменклатуры.ГруппаДоступа
	|ИЗ
	|	Справочник.ВидыНоменклатуры КАК ВидыНоменклатуры
	|ГДЕ
	|	ВидыНоменклатуры.ТипНоменклатуры = &ТипНоменклатуры";
	
	Запрос_.УстановитьПараметр("ТипНоменклатуры", 
			ПредопределенноеЗначение("Справочник.ТипыНоменклатуры.МедицинскаяУслуга"));
	
	Выборка_ = Запрос_.Выполнить().Выбрать();
	
	Если Выборка_.Следующий() Тогда
		НовыйОбъект_ = Справочники.Номенклатура.СоздатьЭлемент();
		НовыйОбъект_.Наименование = Наименование;
		НовыйОбъект_.Артикул = Артикул;
		НовыйОбъект_.ВидНоменклатуры = Выборка_.Ссылка;
		НовыйОбъект_.Наименование = Наименование;
		НовыйОбъект_.НаименованиеПолное = Наименование;
		НовыйОбъект_.ВидМедицинскойУслуги = Перечисления.ВидыМедицинскихУслуг.Одноэтапная;
		НовыйОбъект_.ЕдиницаИзмерения = Справочники.ЕдиницыИзмерения.Услуга;
		НовыйОбъект_.ИсполнительМедицинскойУслуги = Перечисления.ИсполнителиМедицинскихУслуг.Врач;
		НовыйОбъект_.СтавкаНДС = СтавкаНДС;
		НовыйОбъект_.Биоматериал = Биоматериал;
		НовыйОбъект_.ГруппаДоступа = Выборка_.ГруппаДоступа;
		НовыйОбъект_.Родитель = Родитель;
		
		НовыйОбъект_.Записать();
		
		Возврат НовыйОбъект_.Ссылка;
	Иначе
		ВызватьИсключение "Не удалось найти вид номенклатуры для типа номенклатуры МедицинскаяУслуга";
	КонецЕсли;
КонецФункции

////
 // Процедура: СоздатьДокументУстановкаЦенНоменклатуры
 //   Создает и проводит документ установки цен номенклатуры
 //
 // Параметры:
 //   П1 (Тип)
 //     Описание
 //   П2 (Тип)
 //     Описание
///
Процедура СоздатьДокументУстановкаЦенНоменклатуры(ВерсияПрейскуранта) Экспорт
	
	ВидЦены_ = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ВерсияПрейскуранта, "ВидЦены");
	ДокументУстановкаЦенНоменклатуры_ = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ВерсияПрейскуранта, "ДокументУстановкаЦенНоменклатуры");
	
	Запрос_ = Новый Запрос;
	Запрос_.Текст =
	"ВЫБРАТЬ
	|	ПроектПрейскуранта.Номенклатура,
	|	ВЫБОР
	|		КОГДА ПроектПрейскуранта.УдалитьИзСправочника
	|			ТОГДА 0
	|		ИНАЧЕ ПроектПрейскуранта.Цена
	|	КОНЕЦ КАК Цена
	|ИЗ
	|	Справочник.ПроектПрейскуранта КАК ПроектПрейскуранта
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.ВерсияПрейскуранта КАК СправочникВерсияПрейскуранта
	|		ПО ПроектПрейскуранта.ВерсияПрейскуранта = СправочникВерсияПрейскуранта.Ссылка
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЦеныНоменклатуры КАК РегистрСведенийЦеныНоменклатуры
	|		ПО (СправочникВерсияПрейскуранта.ВидЦены = РегистрСведенийЦеныНоменклатуры.ВидЦены)
	|			И ПроектПрейскуранта.Номенклатура = РегистрСведенийЦеныНоменклатуры.Номенклатура
	|			И (РегистрСведенийЦеныНоменклатуры.Период <= &Дата)
	|			И РегистрСведенийЦеныНоменклатуры.Регистратор <> СправочникВерсияПрейскуранта.ДокументУстановкаЦенНоменклатуры
	|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ЦеныНоменклатуры КАК РегистрСведенийЦеныНоменклатурыБудущее
	|		ПО (СправочникВерсияПрейскуранта.ВидЦены = РегистрСведенийЦеныНоменклатурыБудущее.ВидЦены)
	|			И ПроектПрейскуранта.Номенклатура = РегистрСведенийЦеныНоменклатурыБудущее.Номенклатура
	|			И (РегистрСведенийЦеныНоменклатурыБудущее.Период <= &Дата)
	|			И (РегистрСведенийЦеныНоменклатуры.Период < РегистрСведенийЦеныНоменклатурыБудущее.Период)
	|			И РегистрСведенийЦеныНоменклатурыБудущее.Регистратор <> СправочникВерсияПрейскуранта.ДокументУстановкаЦенНоменклатуры
	|ГДЕ
	|	НЕ ПроектПрейскуранта.ЭтоГруппа
	|	И ПроектПрейскуранта.ВерсияПрейскуранта = &ВерсияПрейскуранта
	|	И НЕ ПроектПрейскуранта.ПометкаУдаления
	|	И РегистрСведенийЦеныНоменклатурыБудущее.Период ЕСТЬ NULL 
	|	И (ПроектПрейскуранта.Цена <> ЕСТЬNULL(РегистрСведенийЦеныНоменклатуры.Цена,0)
	|			ИЛИ ПроектПрейскуранта.УдалитьИзСправочника)";
	
	Запрос_.УстановитьПараметр("ВерсияПрейскуранта", ВерсияПрейскуранта);
	Запрос_.УстановитьПараметр("Дата", ТекущаяДатаСеанса());
	
	Выборка_ = Запрос_.Выполнить().Выбрать();
	
	Если Выборка_.Количество() Тогда
		Если ЗначениеЗаполнено(ДокументУстановкаЦенНоменклатуры_) Тогда
			ДокументОбъект_ = ДокументУстановкаЦенНоменклатуры_.ПолучитьОбъект();
		Иначе
			// Создаем документ
			ДокументОбъект_ = Документы.УстановкаЦенНоменклатуры.СоздатьДокумент();
			ДокументОбъект_.Дата = ТекущаяДатаСеанса();
			ДокументОбъект_.Ответственный = Пользователи.ТекущийПользователь();
			ДокументОбъект_.Согласован = Истина;
			ДокументОбъект_.Записать(РежимЗаписиДокумента.Запись);
			
			ВерсияПрейскурантаОбъект_ = ВерсияПрейскуранта.ПолучитьОбъект();
			ВерсияПрейскурантаОбъект_.ДокументУстановкаЦенНоменклатуры = ДокументОбъект_.Ссылка;
			ВерсияПрейскурантаОбъект_.Записать();
		КонецЕсли;
		
		ДокументОбъект_.Товары.Очистить();
		
		Пока Выборка_.Следующий() Цикл
			строкаТЧ_ = ДокументОбъект_.Товары.Добавить();
			строкаТЧ_.ВидЦены = ВидЦены_;
			строкаТЧ_.Номенклатура = Выборка_.Номенклатура;
			строкаТЧ_.Цена = Выборка_.Цена;
		КонецЦикла;
		
		ДокументОбъект_.Записать(РежимЗаписиДокумента.Проведение);
	КонецЕсли;
	
КонецПроцедуры

////
 // Функция: СформироватьАртикулДляЭлемента
 //   Формирует артикул для элемента по формуле: "номер группы ветки"+"."+порядковый номер в ветке.
 //
 // Параметры:
 //   П1 (Тип)
 //     Описание
 //   П2 (Тип)
 //     Описание
 // Возврат:
 //   
///
Функция СформироватьАртикулДляЭлемента(СправочникПроектПрейскурантаСсылка, КоличествоСимволов = Неопределено) Экспорт

	Реквизиты_ = ОбщегоНазначения.ЗначенияРеквизитовОбъекта(СправочникПроектПрейскурантаСсылка, "ВерсияПрейскуранта, Родитель");

	ВерсияПрейскуранта_ = Реквизиты_.ВерсияПрейскуранта;
	Родитель_ = Реквизиты_.Родитель;

	АртикулРодителя_ = Неопределено;
	Если ЗначениеЗаполнено(Родитель_) Тогда
		АртикулРодителя_ = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Родитель_, "Артикул");
	КонецЕсли;
	
	// Определим порядковый номер элемента в группе
	ПорядковыйНомер_ = ПолучитьПорядковыйНомерЭлемента(СправочникПроектПрейскурантаСсылка, ВерсияПрейскуранта_, КоличествоСимволов, АртикулРодителя_);
	
	ПорядковыйНомер_ = Формат(ПорядковыйНомер_,"ЧГ=0");
	
	Если ЗначениеЗаполнено(КоличествоСимволов) Тогда
		Длина_ = СтрДлина(ПорядковыйНомер_);
		Если Длина_ < КоличествоСимволов Тогда
			ПорядковыйНомер_ = ПолучитьСтрокуНулей(КоличествоСимволов - Длина_) + ПорядковыйНомер_;
		КонецЕсли;
	КонецЕсли;
	
	Если ЗначениеЗаполнено(АртикулРодителя_) Тогда
		Возврат СокрП(АртикулРодителя_) + "." + ПорядковыйНомер_;
	Иначе
		Возврат ПорядковыйНомер_;
	КонецЕсли;
	
КонецФункции

Функция ПолучитьСтрокуНулей(Количество)
	Возврат Сред("00000000000000000000",0,Количество);
КонецФункции

////
 // Функция: ПолучитьПорядковыйНомерЭлемента
 //   Получает порядковый номер элемента в группе для использования в артикуле
 //
 // Параметры:
 //   П1 (Тип)
 //     Описание
 //   П2 (Тип)
 //     Описание
 // Возврат:
 //   
///
Функция ПолучитьПорядковыйНомерЭлемента(Знач Ссылка, Знач ВерсияПрейскуранта, Знач КоличествоСимволов = Неопределено, Знач АртикулРодителя) Экспорт

	Артикул_ = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(Ссылка, "Артикул");
	
	Номер_ = ПолучитьНомерИзАртикула(Артикул_, АртикулРодителя, КоличествоСимволов);
	Если Номер_ = Неопределено Тогда
		Артикул_ = "";
	КонецЕсли;
	
	Запрос_ = Новый Запрос;
	ТекстЗапроса_ =
	"ВЫБРАТЬ
	|	ПроектПрейскуранта.Родитель КАК Ссылка
	|ПОМЕСТИТЬ втРодительПроект
	|ИЗ
	|	Справочник.ПроектПрейскуранта КАК ПроектПрейскуранта
	|ГДЕ
	|	ПроектПрейскуранта.Ссылка = &Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	ЕСТЬNULL(РодительПроектПрейскуранта.Номенклатура, ЗНАЧЕНИЕ(Справочник.Номенклатура.ПустаяСсылка)) КАК Ссылка
	|ПОМЕСТИТЬ втРодительНоменклатура
	|ИЗ
	|	Справочник.ПроектПрейскуранта КАК ПроектПрейскуранта
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.ПроектПрейскуранта КАК РодительПроектПрейскуранта
	|		ПО ПроектПрейскуранта.Родитель = РодительПроектПрейскуранта.Ссылка
	|ГДЕ
	|	ПроектПрейскуранта.Ссылка = &Ссылка
	|;
	|
	|////////////////////////////////////////////////////////////////////////////////
	|ВЫБРАТЬ
	|	СправочникНоменклатура.Артикул
	|ИЗ
	|	Справочник.Номенклатура КАК СправочникНоменклатура
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ втРодительНоменклатура КАК втРодительНоменклатура
	|		ПО СправочникНоменклатура.Родитель = втРодительНоменклатура.Ссылка
	|ГДЕ
	|	СправочникНоменклатура.Артикул <> """"
	|	И НЕ СправочникНоменклатура.ПометкаУдаления
	|	И НЕ СправочникНоменклатура.Ссылка В
	|				(ВЫБРАТЬ
	|					СправочникПроектПрейскуранта.Номенклатура
	|				ИЗ
	|					Справочник.ПроектПрейскуранта КАК СправочникПроектПрейскуранта
	|				ГДЕ
	|					СправочникПроектПрейскуранта.Ссылка = &Ссылка)
	|
	|ОБЪЕДИНИТЬ
	|
	|ВЫБРАТЬ
	|	СправочникПроектПрейскуранта.Артикул
	|ИЗ
	|	Справочник.ПроектПрейскуранта КАК СправочникПроектПрейскуранта
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ втРодительПроект КАК втРодительПроект
	|		ПО СправочникПроектПрейскуранта.Родитель = втРодительПроект.Ссылка
	|ГДЕ
	|	СправочникПроектПрейскуранта.ВерсияПрейскуранта = &ВерсияПрейскуранта
	|	И НЕ СправочникПроектПрейскуранта.ПометкаУдаления
	|	И СправочникПроектПрейскуранта.Артикул <> """"
	|	И СправочникПроектПрейскуранта.Ссылка <> &Ссылка";
	
	Запрос_.Текст = ТекстЗапроса_;
	
	Запрос_.УстановитьПараметр("ВерсияПрейскуранта", ВерсияПрейскуранта);
	Запрос_.УстановитьПараметр("Ссылка", Ссылка);
	
	ВсеАртикулы_ = Запрос_.Выполнить().Выгрузить();
	
	ВсеАртикулы_.Колонки.Добавить("НомерИзАртикула", Новый ОписаниеТипов("Число"));
	ВсеАртикулы_.Колонки.Добавить("МеньшийНомер", Новый ОписаниеТипов("Булево"));
	
	// Заполним таблицу номерами из артикулов
	Для Каждого строкаТЗ_ Из ВсеАртикулы_ Цикл
		Врем_ = ПолучитьНомерИзАртикула(строкаТЗ_.Артикул, АртикулРодителя, КоличествоСимволов);
		строкаТЗ_.НомерИзАртикула = ?(Врем_ = Неопределено, 0, Врем_);
		строкаТЗ_.МеньшийНомер = ?(Номер_ = Неопределено Или Врем_ <> Неопределено И Врем_ < Номер_, Истина, Ложь);
	КонецЦикла;
	
	ВсеАртикулы_.Сортировать("НомерИзАртикула УБЫВ");
	
	МеньшиеАртикулы_ = ВсеАртикулы_.НайтиСтроки(Новый Структура("МеньшийНомер", Истина));
	
	// Найдем наибольший номер среди меньших артикулов
	Результат_ = 1;
	Результат_ = НайтиВКоллекцииНаибольшийНомерАртикула(Результат_, МеньшиеАртикулы_);
	
	// Проверим, что найденный номер не занят
	БольшиеАртикулы_ = ВсеАртикулы_.НайтиСтроки(Новый Структура("МеньшийНомер", Ложь));
	Инд_ = БольшиеАртикулы_.Количество() - 1;
	Пока Инд_ >= 0 Цикл
		строкаТЗ_ = БольшиеАртикулы_[Инд_];
		Если строкаТЗ_.НомерИзАртикула > 0 Тогда
			Если строкаТЗ_.НомерИзАртикула = Результат_ Тогда
				Результат_ = НайтиВКоллекцииНаибольшийНомерАртикула(Результат_, БольшиеАртикулы_);
				Прервать;
			Иначе
				Прервать;
			КонецЕсли;
		КонецЕсли;
		Инд_ = Инд_ - 1;
	КонецЦикла;

	Возврат Результат_;
КонецФункции

Функция НайтиВКоллекцииНаибольшийНомерАртикула(Результат, Таблица)
	Результат_ = Результат;
	Для Каждого строкаТЗ_ Из Таблица Цикл
		Номер_ = строкаТЗ_.НомерИзАртикула;
		Если Номер_ > 0 Тогда
			Результат_ = Номер_ + 1;
			Прервать;
		КонецЕсли;
	КонецЦикла;
	
	Возврат Результат_;
КонецФункции

////
 // Функция: ПолучитьНомерИзАртикула
 //   Пытается разобрать строку артикула и получить порядковый номер из строки
 //
 // Параметры:
 //   П1 (Тип)
 //     Описание
 //   П2 (Тип)
 //     Описание
 // Возврат:
 //   
///
Функция ПолучитьНомерИзАртикула(Знач Артикул, Знач АртикулРодителя, КоличествоСимволов = Неопределено)
	Артикул = СокрЛП(Артикул);
	Длина_ = СтрДлина(Артикул);
	Инд_ = Длина_;
	Номер_ = "";
	
	Если ЗначениеЗаполнено(АртикулРодителя) И Найти(Артикул, СокрЛП(АртикулРодителя) + ".") = 0 Тогда
		// Артикул не по формату
		Возврат Неопределено;
	КонецЕсли;
	
	Пока Инд_ >= 1 Цикл
		Символ_ = Сред(Артикул, Инд_, 1);
		Если Символ_ >= "0" и Символ_ <= "9" Тогда
			Номер_ = Символ_ + Номер_;
		Иначе
			Если Символ_ <> "." Тогда
				// Артикул не по формату
				Возврат Неопределено;
			КонецЕсли;
			Прервать;
		КонецЕсли;
		Инд_ = Инд_ - 1;
	КонецЦикла;
	
	Если ЗначениеЗаполнено(Номер_) Тогда
		Если ЗначениеЗаполнено(КоличествоСимволов) И СтрДлина(Номер_) <> КоличествоСимволов Тогда
			// Артикул не по формату
			Возврат Неопределено;
		КонецЕсли;

		Если Не ЗначениеЗаполнено(КоличествоСимволов) И Сред(Номер_, 1, 1) = "0" Тогда
			// Артикул не по формату
			Возврат Неопределено;
		КонецЕсли;
		
		Возврат Число(Номер_);
	Иначе
		Возврат Неопределено;
	КонецЕсли;
КонецФункции

////
 // Процедура: ПринятиеПроектаПрейскурантаВРаботу
 //   Отбирает версии прейскуранта готовые для принятия в работу.
 //   В указанный день вносит изменения в услуги и их цены в соответствии с проектом.
 //
 // Параметры:
///
Процедура ПринятиеПроектаПрейскурантаВРаботу() Экспорт
	// Отберем проекты со статусом НаВыполнение и текущей датой 
	НачатьТранзакцию();
	Запрос_ = Новый Запрос;
	Запрос_.Текст =
	"ВЫБРАТЬ
	|	ВерсияПрейскуранта.Ссылка
	|ИЗ
	|	Справочник.ВерсияПрейскуранта КАК ВерсияПрейскуранта
	|ГДЕ
	|	НЕ ВерсияПрейскуранта.ПометкаУдаления
	|	И ВерсияПрейскуранта.Статус = ЗНАЧЕНИЕ(Перечисление.СтатусПрейскуранта.НаВыполнение)
	|	И НАЧАЛОПЕРИОДА(ВерсияПрейскуранта.ДатаПринятияВРаботу, ДЕНЬ) = &ДатаПринятияВРаботу";
	
	Запрос_.УстановитьПараметр("ДатаПринятияВРаботу", НачалоДня(ТекущаяДатаСеанса()));
	Выборка_ = Запрос_.Выполнить().Выбрать();
	ЗафиксироватьТранзакцию();
	
	Пока Выборка_.Следующий() Цикл
		Попытка
			ПринятьПроектВРаботу(Выборка_.Ссылка);
		Исключение
			ОбщиеМеханизмы.ОбработатьОшибку(ИнформацияОбОшибке());
		КонецПопытки
	КонецЦикла;
	
КонецПроцедуры

////
 // Процедура: ПринятьПроектВРаботу
 //   Вносит изменения в услуги и их цены в соответствии с переданным проектом.
 //
 // Параметры:
 //   П1 (Тип)
 //     Описание
 //   П2 (Тип)
 //     Описание
///
Процедура ПринятьПроектВРаботу(ВерсияПрейскуранта) Экспорт
	
	НачатьТранзакцию();
	Попытка
		ВерсияПрейскурантаОбъект_ = ВерсияПрейскуранта.ПолучитьОбъект();
		ВерсияПрейскурантаОбъект_.Заблокировать();
		
		Обработки.ФормированиеПроектаПрейскуранта.ВнестиИзмененияВСправочникНоменклатура(ВерсияПрейскуранта);
		
		Обработки.ФормированиеПроектаПрейскуранта.СоздатьДокументУстановкаЦенНоменклатуры(ВерсияПрейскуранта);
		
		// Изменим статусы
		ВерсияПрейскурантаОбъект_.Прочитать();
		ВерсияПрейскурантаОбъект_.Статус = Перечисления.СтатусПрейскуранта.Готов;
		ВерсияПрейскурантаОбъект_.Записать();
		
		// Заявкам
		Запрос_ = Новый Запрос;
		Запрос_.Текст =
		"ВЫБРАТЬ РАЗЛИЧНЫЕ
		|	ПроектПрейскуранта.Заявка
		|ИЗ
		|	Справочник.ПроектПрейскуранта КАК ПроектПрейскуранта
		|		ЛЕВОЕ СОЕДИНЕНИЕ Документ.ЗаявкаНаИзменениеПрейскуранта КАК ЗаявкаНаИзменениеПрейскуранта
		|		ПО ПроектПрейскуранта.Заявка = ЗаявкаНаИзменениеПрейскуранта.Ссылка
		|ГДЕ
		|	НЕ ПроектПрейскуранта.ПометкаУдаления
		|	И НЕ ПроектПрейскуранта.ЭтоГруппа
		|	И ПроектПрейскуранта.ВерсияПрейскуранта = &ВерсияПрейскуранта
		|	И ЗаявкаНаИзменениеПрейскуранта.Статус <> ЗНАЧЕНИЕ(Перечисление.СтатусЗаявкиНаИзменениеПрайса.Выполнена)";
		
		Запрос_.УстановитьПараметр("ВерсияПрейскуранта", ВерсияПрейскуранта);
		Выборка_ = Запрос_.Выполнить().Выбрать();
		
		Пока Выборка_.Следующий() Цикл
			ДокументОбъект_ = Выборка_.Заявка.ПолучитьОбъект();
			ДокументОбъект_.Статус = Перечисления.СтатусЗаявкиНаИзменениеПрайса.Выполнена;
			ДокументОбъект_.Записать(РежимЗаписиДокумента.Запись);
		КонецЦикла;
		
		ВерсияПрейскурантаОбъект_.Разблокировать();
		
		ЗафиксироватьТранзакцию();
		
	Исключение
		ОтменитьТранзакцию();
		ВызватьИсключение;
	КонецПопытки;

КонецПроцедуры

////
 // Процедура: ЗагрузитьНоменклатуруПоВидуЦены
 //   Загружает в иерархию номенклатуру из вида цены созданной версии прейскуранта
 //
 // Параметры:
 //   П1 (Тип)
 //     Описание
 //   П2 (Тип)
 //     Описание
///
Процедура ЗагрузитьНоменклатуруПоВидуЦены(ВерсияПрейскуранта) Экспорт
	
	ВидЦены_ = ОбщегоНазначения.ЗначениеРеквизитаОбъекта(ВерсияПрейскуранта, "ВидЦены");
	Запрос_ = Новый Запрос;
	Запрос_.Текст =
	"ВЫБРАТЬ
	|	ЦеныНоменклатурыСрезПоследних.Номенклатура
	|ИЗ
	|	РегистрСведений.ЦеныНоменклатуры.СрезПоследних(&ДатаСреза, ВидЦены = &ВидЦены) КАК ЦеныНоменклатурыСрезПоследних
	|		ВНУТРЕННЕЕ СОЕДИНЕНИЕ Справочник.Номенклатура КАК СправочникНоменклатура
	|		ПО ЦеныНоменклатурыСрезПоследних.Номенклатура = СправочникНоменклатура.Ссылка
	|ГДЕ
	|	СправочникНоменклатура.Актуальность
	|	И НЕ СправочникНоменклатура.ПометкаУдаления
	|	И ЦеныНоменклатурыСрезПоследних.Цена > 0";
	
	Запрос_.УстановитьПараметр("ВидЦены", ВидЦены_);
	Запрос_.УстановитьПараметр("ДатаСреза", ТекущаяДатаСеанса());
	СписокНоменклатуры_ = Запрос_.Выполнить().Выгрузить().ВыгрузитьКолонку("Номенклатура");
	
	СоздатьИерархиюЭлементов(ВерсияПрейскуранта, СписокНоменклатуры_);
КонецПроцедуры

#КонецОбласти

#КонецЕсли