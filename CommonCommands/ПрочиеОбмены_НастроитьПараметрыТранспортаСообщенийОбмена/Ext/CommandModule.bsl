﻿
#Область ПрограммныйИнтерфейс


&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	Отбор              = Новый Структура("Узел", ПараметрКоманды);
	ЗначенияЗаполнения = Новый Структура("Узел", ПараметрКоманды);
	
	ОбменДаннымиКлиент.ОткрытьФормуЗаписиРегистраСведенийПоОтбору(Отбор, 
		ЗначенияЗаполнения, 
		"ПрочиеОбмены_НастройкиТранспортаОбмена", 
		ПараметрыВыполненияКоманды.Источник);
	
КонецПроцедуры // ОбработкаКоманды()


#КонецОбласти