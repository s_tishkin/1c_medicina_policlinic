﻿
#Область ПрограммныйИнтерфейс


&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	АрхивированиеКлиент.КомандаИзменитьАрхивность(ПараметрыВыполненияКоманды.Источник, ПараметрКоманды, НЕ Архивирование.ЗначениеНедоступности());
	
КонецПроцедуры

 

#КонецОбласти