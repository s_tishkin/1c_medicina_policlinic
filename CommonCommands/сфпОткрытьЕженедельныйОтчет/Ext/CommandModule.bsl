﻿
&НаКлиенте
// Процедура - обработчик выполнения команды
//
// Параметры:
//	ПараметрКоманды				- Произвольный	- Параметр команды
//	ПараметрыВыполненияКоманды	- Структура		- Параметры выполнения команды
//
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	ПараметрыФормы = Новый Структура("ВариантОтчета",	"ЕженедельныйОтчет");
	ОткрытьФорму("Отчет.сфпИсторияЗвонков.ФормаОбъекта", ПараметрыФормы, ПараметрыВыполненияКоманды.Источник, ПараметрыВыполненияКоманды.Уникальность, ПараметрыВыполненияКоманды.Окно);
КонецПроцедуры // ОбработкаКоманды()
