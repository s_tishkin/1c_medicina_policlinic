﻿
#Область ПрограммныйИнтерфейс


&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	// вызов сервера
	ВидПравил = ПредопределенноеЗначение("Перечисление.ВидыПравилДляОбменаДанными.ПравилаКонвертацииОбъектов");
	
	Отбор              = Новый Структура("УзелОбмена, ВидПравил", ПараметрКоманды, ВидПравил);
	ЗначенияЗаполнения = Новый Структура("УзелОбмена, ВидПравил", ПараметрКоманды, ВидПравил);
	
	ОбменДаннымиКлиент.ОткрытьФормуЗаписиРегистраСведенийПоОтбору(Отбор, 
		ЗначенияЗаполнения, 
		"ПрочиеОбмены_ПравилаДляОбменаДанными", 
		ПараметрыВыполненияКоманды.Источник);
	
КонецПроцедуры // ОбработкаКоманды()


#КонецОбласти