﻿
#Область ОбработчикиСобытий

&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	
	ВариантыОтчетовКлиент.ПоказатьПанельОтчетов("КонтрольИсполнения", ПараметрыВыполненияКоманды)
	
КонецПроцедуры

#КонецОбласти